function plotREC(t, acc, folder, tolerance, tit)
% Plot
figure;
plot(t,acc);
grid on;
legend('REC');
title(tit);
xlabel('Tolerance');
ylabel('Accuracy');
format = 'png';
name = strcat(folder,'\_to2',num2str(tolerance),'.',format );
saveas(gcf, name,format);
end