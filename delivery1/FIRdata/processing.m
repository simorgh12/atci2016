folder = 'test12';

num_folds = 5;
maxtolerance = 2;
errors = zeros(num_folds,1);
t_folds = zeros(num_folds,100);
acc_folds = zeros(num_folds,100);


t_student = 2.015;

for i=1:1:num_folds
    num_s = num2str(i);
    fold = strcat(folder, '\fold', num_s, '.mat');
    display(fold);
    load(fold);
    [errors(i), t, acc] = REC2(real, prediccio, 2);
       
    t_folds(i,:) = t;
    acc_folds(i,:) = acc;    
end
n = length(real);

MAD = mean(errors, 1);
MAD_interval = (1/sqrt(n))*std(errors)*t_student;

acc_2 = mean(acc_folds);
acc_2_interval = (1/sqrt(n))*std(acc_folds)*t_student;
t_2 = mean(t_folds);


plotREC(t_2, acc_2, folder, 1, folder);
name = strcat(folder,'\errors');
save(name,'MAD', 'MAD_interval', 't_2', 'acc_2', 'acc_2_interval');
clearvars -except folder;
load(strcat(folder, '\errors.mat'));


