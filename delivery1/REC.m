function [MAD,t,acc] = REC(class, pred, maxTolerance)
% Input:
%   class = true class
%   pred = class predicted by the model
%   maxTolerance = max value for the x coordinate
% Output:
%   MAD = Mean Absolute Deviation
%   t = array of tolerance (x-axis)
%   acc = array of accuracies corresponding to the array t (y-axis)

N = length(class); % Number of instances
step = maxTolerance/100; % dx

% REC computation
for i = 1:100
    t(i) = i*step; % x-coordinate
    acc(i) = 0;
    for j = 1:N
        if class(j) - pred(j) < t(i)
            acc(i) = acc(i) + 1;
        end
    end
    acc(i) = acc(i)/N; % y-coordinate
end

% Plot
plot(t,acc);
grid on;
legend('REC');
xlabel('Tolerance');
ylabel('Accuracy');

% Mean Absolute Deviation
MAD = sum(abs(class - pred))/N;
end