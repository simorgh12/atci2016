algorithm = Summary of the algorithm, Classification probs.
inputData = "../results/MOGUL-C.wine/result0s0.tst" "../results/MOGUL-C.wine/result1s0.tst" "../results/MOGUL-C.wine/result2s0.tst" "../results/MOGUL-C.wine/result3s0.tst" "../results/MOGUL-C.wine/result4s0.tst" "../results/MOGUL-C.wine/result5s0.tst" "../results/MOGUL-C.wine/result6s0.tst" "../results/MOGUL-C.wine/result7s0.tst" "../results/MOGUL-C.wine/result8s0.tst" "../results/MOGUL-C.wine/result9s0.tst" "../results/MOGUL-C.wine/result0s0.tra" "../results/MOGUL-C.wine/result1s0.tra" "../results/MOGUL-C.wine/result2s0.tra" "../results/MOGUL-C.wine/result3s0.tra" "../results/MOGUL-C.wine/result4s0.tra" "../results/MOGUL-C.wine/result5s0.tra" "../results/MOGUL-C.wine/result6s0.tra" "../results/MOGUL-C.wine/result7s0.tra" "../results/MOGUL-C.wine/result8s0.tra" "../results/MOGUL-C.wine/result9s0.tra" 
outputDataTabular = "../results/Vis-Clas-General/TSTMOGUL-C/result0s0.stat" "../results/Vis-Clas-General/TSTMOGUL-C/pvalueMatrix0s0.stat" 

seed = 1343278692
subAlgorithm = StatGeneralCL
dataformat = keel
outlabel = CHK
