algorithm = Fuzzy Rule Learning, SLAVE Algorithm
inputData = "../datasets/wine/wine-10-3tra.dat" "../datasets/wine/wine-10-3tra.dat" "../datasets/wine/wine-10-3tst.dat" 
outputData = "../results/SLAVE-C.wine/result2s0.tra" "../results/SLAVE-C.wine/result2s0.tst" "../results/SLAVE-C.wine/result2s0e0.txt"  "../results/SLAVE-C.wine/result2s0e1.txt"  

seed = 1286082570
Number of Labels = 5
Population Size = 100
Number of Iterations Allowed without Change = 500
Mutation Probability = 0.01
Use Rule Weigths = Yes
