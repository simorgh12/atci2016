algorithm = Fuzzy Rule Learning, SLAVE Algorithm
inputData = "../datasets/wine/wine-10-2tra.dat" "../datasets/wine/wine-10-2tra.dat" "../datasets/wine/wine-10-2tst.dat" 
outputData = "../results/SLAVE-C.wine/result1s0.tra" "../results/SLAVE-C.wine/result1s0.tst" "../results/SLAVE-C.wine/result1s0e0.txt"  "../results/SLAVE-C.wine/result1s0e1.txt"  

seed = 1286082570
Number of Labels = 5
Population Size = 10
Number of Iterations Allowed without Change = 500
Mutation Probability = 0.01
Use Rule Weigths = Yes
