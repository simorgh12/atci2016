from lasagne import layers
from nolearn.lasagne import NeuralNet, BatchIterator
import numpy as np

def fit_model(X, Y, N, K, H):
    shape = np.shape(X)
    net = NeuralNet(
        layers = [
            ('input', layers.InputLayer),
            ('conv', layers.Conv2DLayer),
            ('hidden', layers.DenseLayer),
            ('output', layers.DenseLayer),
        ],
        input_shape = (None, shape[1], shape[2], shape[3]),
        conv_num_filters = N, conv_filter_size=(K[0], K[1]),
        hidden_num_units = H,
        output_num_units = 1,
        
        update_learning_rate = 0.01,
        update_momentum = 0.9,
        
        batch_iterator_train = BatchIterator(batch_size = 400),
        batch_iterator_test = BatchIterator(batch_size = 100),
        
        use_label_encoder = False,
        regression = True,
        max_epochs = 100,
        verbose = 1,
    )
    
    net.fit(X, Y)
    
    return net

def rmse(net,X,Y):
    pred = net.predict(X)
    rmse = pow(sum(pow(Y - pred,2))/len(Y),0.5)  
    return rmse