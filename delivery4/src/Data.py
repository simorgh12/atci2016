import openpyxl as px
import numpy as np

def load_data():
    FILE_PATH = 'C:\Users\Arkard\MasterUni\ATCI\BitBucket\\atci2016\delivery4\src\data\Divisas.xlsx'
    W = px.load_workbook(FILE_PATH, use_iterators = True)
    tags = W.get_sheet_names()
    e = dict.fromkeys(tags,[])
    
    for i in range(len(tags)):
        p = W.get_sheet_by_name(tags[i])
        m = []
        for row in p.iter_rows():
            for k in row:
                m.append(k.internal_value)
                
        m = np.resize(m,[p.max_row,p.max_column])
        e[tags[i]] = np.array(m[:,2],dtype = np.float64)
        print 'Reading: ' + str(i + 1) + ' of 16'

    lengths = []    
    for tag in tags:
        lengths.append(len(e[tag]))
    m = min(lengths)

    M = np.zeros([m,16],dtype = np.float64)
    for i in range(len(tags)):
        M[:,i] = e[tags[i]][0:m]
        if len(tags[i]) > 3:
            M[:,i] = 1./M[:,i]
            tags[i] = tags[i][0:3]
    M = np.flipud(M)
    
    return M, tags
                

def preprocess(M,t,tags,currency):
    N = len(M) - t + 1
    X = np.zeros([N - 1,1,t,np.shape(M)[1]],dtype = np.float64)
    Y = np.zeros([N - 1,1],dtype = np.float64)
    for idx in range(len(tags)):
        if tags[idx] == currency:
            break
    
    for i in range(N - 1):
        X[i,0] = M[i:i + t,:]
        Y[i] = M[i + t, idx]
        
    return X, Y
    
def split(X,Y,r):
    idx = int(len(X)*r)
    X_train = X[1:idx]
    Y_train = Y[1:idx]
    X_test = X[idx:]
    Y_test = Y[idx:]
    return X_train, Y_train, X_test, Y_test