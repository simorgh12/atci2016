import Data
import CNN
import numpy as np
import winsound

T = 15;
N = 10;
kernel = [T, 1]
H = 50;

M,tags = Data.load_data();
X, Y = Data.preprocess(M,T,tags,'EUR')
X_train, Y_train, X_test, Y_test = Data.split(X,Y,0.8)

rmse_train = np.zeros([10])
rmse_test = np.zeros([10])
rmse = np.zeros([10])
i = 0;
while i<10:
    net = CNN.fit_model(X_train,Y_train,N,kernel,H)
    pred_train = net.predict(X_train)
    pred_test = net.predict(X_test)
    pred = net.predict(X)
    rmse_train[i] = CNN.rmse(net,X_train,Y_train)
    rmse_test[i] = CNN.rmse(net,X_test,Y_test)
    rmse[i] = CNN.rmse(net,X,Y)
    if rmse_train[i] < 1:
        i = i + 1

print "---TRAINING---"
print "RMSE = " + str(np.mean(rmse_train))
print "STD = " + str(np.std(rmse_train))

print "---TEST---"
print "RMSE = " + str(np.mean(rmse_test))
print "STD = " + str(np.std(rmse_test))

winsound.Beep(600,2000)