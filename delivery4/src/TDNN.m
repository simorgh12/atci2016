%%
clear all
close all
clc

%% Set problem parameters
Objective = 'EUR'; % Predict this currency
Currency  = 'EUR'; % Using data of this currency (e.g. 'EUR','JPY','GBP')
%Currency = {'EUR','JPY','GBP','AUD','RUB','ARS','CAD'}; % Using data of this currency (e.g. 'EUR','JPY','GBP')

%% Load data
load divisas.mat

%% TDNN Parameters
Architecture = 10;%[5 10 25 50 100];
T_delay = 5;
%Architecture = [[50 25];[50 10];[100 50];[100 25]];
%T_delay = [5 15 25 50 100];

n = 10; % iterations per experiment
%for j=1:length(T_delay)
    %% prediction currencies
    for i = 1:length(tags)
        if strcmp(tags{i},Objective)
            T = M(:,i);
        end
        if strcmp(tags{i},Currency) % Choose objective values 
            X = M(:,i);
        end
    end
    
    %% Split train/test
    train_split = 0.8;
    s = round(length(X)*train_split);
    X_train = X(1:s,:);
    T_train = T(1:s,:);
    X_test = X(s + 1:end,:);
    T_test = T(s + 1:end,:);

    %% init experiment vectors
    rmse_train = zeros(n,1);
    rmse_test = zeros(n,1);
    rmse = zeros(n,1);
    i=1;
    while i <= 10
        disp(['Iteration ' num2str(i)])
        %% Create net
        net = timedelaynet(1:T_delay, Architecture);
        %% Train parameters
        net.divideFcn = '';
        net.trainParam.epochs = 50;
    
        %% prepare data
        [Xs, Xi, Ai, Ts] = preparets(net,num2cell(X_train'),num2cell(T_train'));

        %% Net train
        net = train(net,Xs,Ts,Xi,Ai);

        %% Net's performance
        % Training
        Y = net(Xs,Xi,Ai);
        E = gsubtract(Y,Ts);
        rmse_train(i) = sqrt(mse(E));

        % Testing
        [Xs_test, Xi_test, Ai_test, Ts_test] = preparets(net,num2cell(X_test'),num2cell(T_test'));
        Y_test = net(Xs_test, Xi_test, Ai_test);
        E_test = gsubtract(Y_test,Ts_test);
        rmse_test(i) = sqrt(mse(E_test));

        % Test with all dataset
        [Xs_all, Xi_all, Ai_all, Ts_all] = preparets(net,num2cell(X'),num2cell(T'));
        Y_all = net(Xs_all, Xi_all, Ai_all);
        E_all = gsubtract(Y_all,Ts_all);
        rmse(i) = sqrt(mse(E_all));

        if rmse_train(i) < 1
            i = i + 1;
        end
    end


    %% Show results
    disp(['--- Arch ' num2str(Architecture)])
    disp(['--- Delay ' num2str(T_delay) ])
    disp(['--- Currency ' Currency ])
    disp('--- Results')
    disp(['trn RMSE = ' num2str(mean(rmse_train)) ' +/- ' num2str(std(rmse_train))])
    disp(['tst RMSE = ' num2str(mean(rmse_test)) ' +/- ' num2str(std(rmse_test))])
    disp(['all RMSE = ' num2str(mean(rmse)) ' +/- ' num2str(std(rmse))])
    disp(' ')
    
    clear net;


















%% Training plots
figure(1);
plot(cell2mat(Ts));
hold on
plot(cell2mat(Y),'r');
grid on
title('Training');
legend('Ground Truth','Prediction');
xlabel('Timesteps (days)');
ylabel(Objective);

%figure(2)
%plotresponse(Ts,Y);

%% Test plots
figure(3);
plot(cell2mat(Ts_test));
hold on
plot(cell2mat(Y_test),'r');
grid on
title('Test');
legend('Ground Truth','Prediction');
xlabel('Timesteps (days)');
ylabel(Objective);

%figure(4)
%plotresponse(Ts_test,Y_test);

%% All plots
figure(5);
plot(cell2mat(Ts_all));
hold on
plot(cell2mat(Y_all),'r');
grid on
title('Total');
legend('Ground Truth','Prediction');
xlabel('Timesteps (days)');
ylabel(Objective);

%% Script-ending alarm
%load gong.mat;
%soundsc(y);