%%
clear all
close all

%% Set problem parameters
Objective = 'EUR'; % Predict this currency
Currency = 'EUR'; % Using data of this currency

%% Load data
load divisas.mat
% Choose objective values (e.g. 'EUR','JPY','GBP')
for i = 1:length(tags)
    if strcmp(tags{i},Objective)
        T = M(:,i);
    end
    if strcmp(tags{i},Currency)
        X = M(:,i);
    end
end

%% Split train/test
train_split = 0.8;
s = round(length(X)*train_split);
X_train = X(1:s,:);
T_train = T(1:s,:);
X_test = X(s + 1:end,:);
T_test = T(s + 1:end,:);

%% NARX Parameters
Tx_delay = 1:20;
Ty_delay = 1:20;
Architecture = 10;

% Create net and prepare data
net = narxnet(Tx_delay,Ty_delay,Architecture);
X_train = num2cell(X_train');
T_train = num2cell(T_train');
[Xs, Xi, Ai, Ts] = preparets(net,X_train,{},T_train);

% Train parameters
net.divideFcn = '';
net.trainParam.epochs = 100;

% Net train
net = train(net,Xs,Ts,Xi,Ai);

%% Net's performance
% Training
Y = sim(net,Xs,Xi);
perf = perform(net,Ts,Y);
E = gsubtract(Y,Ts);
RMSE = sqrt(mse(E));

% Testing
net = closeloop(net); % Testing on closed loop gives worse results
X_test = num2cell(X_test');
T_test = num2cell(T_test');
[Xs_test, Xi_test, Ai_test, Ts_test] = preparets(net,X_test,{},T_test);
Y_test = net(Xs_test, Xi_test, Ai_test);
perf_test = perform(net,Ts_test, Y_test);
E_test = gsubtract(Y_test,Ts_test);
RMSE_test = sqrt(mse(E_test));

%% Plots
% Training plots
figure(1);
plot(cell2mat(Ts));
hold on
plot(cell2mat(Y),'r');
grid on
title('Training');
legend('Ground Truth','Prediction');
xlabel('Timesteps (days)');
ylabel(Objective);

figure(2)
plotresponse(Ts,Y);

% Test plots
figure(3);
plot(cell2mat(Ts_test));
hold on
plot(cell2mat(Y_test),'r');
grid on
title('Test');
legend('Ground Truth','Prediction');
xlabel('Timesteps (days)');
ylabel(Objective);

figure(4)
plotresponse(Ts_test,Y_test);

%% Script-ending alarm
load gong.mat;
soundsc(y);