function data = load_data(file)
% Input:
% - file = string with the path of the file
%
% Output:
% - data = struct
%       .M = Nx5 matrix, useful value on first column. M(:,1)
%       .dates = Nx1 cell matrix with the date of each observation
%       .pair = string containing the corresponding pair of currencies

pair = {'EUR/USD','GBP/USD','USD/JPY','USD/CAD','USD/RUB','USD/SAR','USD/ARS','USD/AUD','USD/BRL','USD/CNY','USD/KRW','USD/INR','USD/IDR','USD/MXN','USD/ZAR','USD/TRY'};

for i = 1:length(pair)
    disp(['Reading: ', num2str(i), ' of 16']);
    [data(i).M, data(i).dates]  = xlsread(file,i);    
    data(i).pair = pair{i};
end
disp('Done!');
end