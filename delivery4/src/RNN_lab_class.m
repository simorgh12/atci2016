% Load laser time-series and select a part of it (to speed up examples)
s = laser_dataset;
s = s(1:600);

% Define a time-delay single-layer linear perceptron with 8 delays
lin_net = linearlayer([1:8]);
view(lin_net);   % size of inputs and outputs are not displayed yet

% Prepare inputs and targets for lin_net from the time-series in s
[x,Xi,Ai,t] = preparets(lin_net,s,s);
% where second and third arguments are input and output signals
% respectively (the same series in this case);
% x are shifted inputs, t are shifted targets (based on net delays);
% Xi are initial input delays and 
% Ai are initial delay states (empty in this case).

% In this case, the above preparets instruction is equivalent to:
x = s(9:end);
t = s(9:end);
Xi = s(1:8);
Ai = {};

% Set training method and no inner training data partition
lin_net.trainFcn = 'trainlm';  % Levenberg-Marquardt
lin_net.divideFcn = '';        % No validation and test inner subsets
% Train the network
[lin_net,tr] = train(lin_net,x,t,Xi);
% Compute the output of the linear network for the training input
lin_y = lin_net(x,Xi);
% Compute the root mean squared error of the linear network
lin_e = gsubtract(lin_y,t);
lin_rmse = sqrt(mse(lin_e))

% Define a time-delay two-layer feed-forward net with 8 input delays,
% 10 sigmoidal hidden units and linear output units (Focused TDNN)
ftdnn_net = timedelaynet([1:8],10);
view(ftdnn_net);   % size of inputs and outputs are not displayed yet
% The default choices of activation functions are
% (no need to write the instructions unless other functions are desired)
ftdnn_net.layers{1}.transferFcn = 'tansig';
ftdnn_net.layers{2}.transferFcn = 'purelin';

% Prepare inputs and targets for ftdnn_net from the time-series in s
[x,Xi,Ai,t] = preparets(ftdnn_net,s,s); % yields the same as for lin_net

% Set and do the training, compute output and training error 
% of focused TDNN
ftdnn_net.trainFcn = 'trainlm';  % Levenberg-Marquardt (by default here)
ftdnn_net.divideFcn = '';        % No validation and test inner subsets
[ftdnn_net,tr] = train(ftdnn_net,x,t,Xi);
ftdnn_y = ftdnn_net(x,Xi);
ftdnn_e = gsubtract(ftdnn_y,t);
ftdnn_rmse = sqrt(mse(ftdnn_e))

% Define a time-delay two-layer feed-forward net with 8 input delays,
% 3 hidden delays, 10 sigmoidal hidden units and linear output units 
% (Distributed TDNN, meaning that there are delays in every layer)
dtdnn_net = distdelaynet({1:8,0:3},10);
view(dtdnn_net);   % size of inputs and outputs are not displayed yet

% Prepare inputs and targets for dtdnn_net from the time-series in s
[x,Xi,Ai,t] = preparets(dtdnn_net,s,s); % Note that Ai is not empty now

% Set and do the training, compute output and training error 
% of distributed TDNN
dtdnn_net.divideFcn = '';        % No validation and test inner subsets
dtdnn_net.trainParam.epochs = 100;
[dtdnn_net,tr] = train(dtdnn_net,x,t,Xi,Ai); % Note additional Ai argument
dtdnn_y = dtdnn_net(x,Xi,Ai);    % Here also
dtdnn_e = gsubtract(dtdnn_y,t);
dtdnn_rmse = sqrt(mse(dtdnn_e))
plotresponse(t,dtdnn_y);  % Plots difference between target and output

% Up to now, we have seen different types of time-delay neural nets of
% increasing complexity
%
% Now we turn our attention to recurrent neural nets, starting by
% NARX networks.
%
% Load another dataset with different input and output signals

load magdata;     % produces row vectors u,y
u = mapminmax(u); % normalizes between -1 and 1
y = mapminmax(y);
y = con2seq(y);   % convert data format from double vector to cell sequence
u = con2seq(u);

% Define a (open-loop) narx net with 3 input delays, 2 output delays, 
% 10 sigmoidal hidden units and linear output units 
d1=[1:3];         % set the input delays
d2=[1:2];         % set the output delays
narx_net = narxnet(d1,d2,10);
view(narx_net);

% Prepare inputs and targets for narx_net from the time-series in u,y
% (note that fourth argument of preparets, feedback target, is used
% instead of the third one, non-feedback target, for narx networks)
[x,Xi,Ai,t] = preparets(narx_net,u,{},y);

% Set and do the training, compute output and training error 
% of open-loop NARX network
narx_net.divideFcn = '';
narx_net.trainParam.epochs = 100;
narx_net = train(narx_net,x,t,Xi);
narx_y = sim(narx_net,x,Xi);
narx_e = gsubtract(narx_y,t);
narx_rmse = sqrt(mse(narx_e))
% Display training error graphic
e = cell2mat(narx_e);
plot(e);

% convert open-loop NARX net into closed-loop NARX net
narx_net_closed = closeloop(narx_net);
view(narx_net_closed);
% perform iterated long-term prediction of 900 time steps
% using closed-loop NARX net and compute test error
y1 = y(1700:2600);
u1 = u(1700:2600);
[x1,Xi1,Ai1,t1] = preparets(narx_net_closed,u1,{},y1);
narx_y1 = sim(narx_net_closed,x1,Xi1,Ai1);
narx_e1 = gsubtract(narx_y1,t1);
narx_rmse1 = sqrt(mse(narx_e1))
% Show comparison of predicted output(blue) vs. target(green)
plot([cell2mat(narx_y1)' cell2mat(t1)']);

% It is possible to define directly a closed-loop NARX net
% prior to training:
closed_narx_net = narxnet(d1,d2,10,'closed');
view(closed_narx_net);
% and then train it in closed-loop (but it is much slower):
closed_narx_net.divideFcn = '';
closed_narx_net.trainParam.epochs = 20;
[x,Xi,Ai,t] = preparets(closed_narx_net,u,{},y);
closed_narx_net = train(closed_narx_net,x,t,Xi,Ai);
closed_narx_y = sim(closed_narx_net,x,Xi,Ai);
closed_narx_e = gsubtract(closed_narx_y,t);
closed_narx_rmse = sqrt(mse(closed_narx_e))

% Load a new dataset to test Elman network
load phoneme;
x = con2seq(y);
t = con2seq(t);
% Define and train an Elman net (with no delays and) with 
% 8 sigmoidal hidden units and linear output units 
elman_net = newlrn(x,t,8);
view(elman_net);
elman_net.divideFcn = '';
elman_net.trainParam.epochs = 50;
elman_net = train(elman_net,x,t);
elman_y = sim(elman_net,x);
plot([cell2mat(elman_y)' cell2mat(t)']);

% Now we try layer-recurrent networks, which are a generalization
% of Elman networks with feedback delays 
% (and one or more hidden layers; one in the example below)
[X,T] = simpleseries_dataset;
lrec_net = layrecnet(1:2,8);
view(lrec_net);
lrec_net.divideFcn = '';
lrec_net.trainParam.epochs = 50;
[x,Xi,Ai,t] = preparets(lrec_net,X,T);
lrec_net = train(lrec_net,x,t,Xi,Ai);
lrec_net_y = sim(lrec_net,x,Xi,Ai);
plot([cell2mat(lrec_net_y)' cell2mat(t)']);

% Finally, let us see the data format when we have more than 
% one input signal and more than one output signal in a 
% training pattern.
% Generate three random input signals of 100 items (or time-steps)
x = nndata(3,1,100);
% Generate two random target signals of 100 items (or time-steps)
t = nndata(2,1,100);
% Train and apply a time-delay NN (5 input delays and 10 hidden units) 
% to these data
ftdnn_net = timedelaynet(1:5,10);
view(ftdnn_net);
[p,Pi,Ai,ts] = preparets(ftdnn_net,x,t);
ftdnn_net.divideFcn = '';
ftdnn_net = train(ftdnn_net,p,ts,Pi);
yp = ftdnn_net(p,Pi);
% Compare the first output signal vs. the first target signal
yp1 = getelements(yp,1);
t1 = getelements(ts,1);
plot([cell2mat(yp1)' cell2mat(t1)']);

