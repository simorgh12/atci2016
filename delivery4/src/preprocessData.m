function [M, tags] = preprocessData(data)
% Reorganizes the data of the struct in a Matrix
% Input:
%   - data: output struct from 'load_data.m'
% Output:
%   - M: Nx16 matrix with the values of 16 currencies, from older to newer
%   data
%   - tags: cell array with strings indicating which currency corresponds
%   to each index

% Computation of minimum data series length
for i = 1:length(data)
    L(i) = length(data(i).M);
end
m = min(L);

% Initialize matrix with size equivalent to the minimum previously computed
M = zeros(m,length(data));
tags = cell(1,length(data));

% For each currency
for i = 1:length(data)
    M(:,i) = data(i).M(1:m,1); % Assigns the values to the matrix
    pair = strsplit(data(i).pair,'/'); % Pair of currencies ("USD/XXX" or "XXX/USD")
    if strcmp(pair{1},'USD') % If not in dollars, calculate inverse
        M(:,i) = 1./M(:,i);
        tags{i} = pair{2}; % Assign the correct tag
    else
        tags{i} = pair{1}; % Assign the correct tag
    end
end
M = flipud(M); % Reverse matrix so first index correspon to oldest value
end