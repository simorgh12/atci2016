ATCI - Optional practical project

This optional ATCI project, to be developed in groups of two people (or individually), will consist of applying TDNNs and RNNs to one or more datasets of a dynamic input/output task, such as sequence prediction, sequence classification or temporal association.

At least you should compare a TDNN architecture versus an RNN one, testing different network sizes and number of delays. You may also apply different types of TDNN (linear, non-linear focused, non-linear distributed) and RNNs (NARX, Elman, ...). It is not mandatory to use the Matlab NN software commented in class; you may use as well other frameworks or software packages including TDNNs and RNNs.

The same experimental procedure should be applied to the different networks compared,  trying to obtain the best results possible for each of the tested datasets. For each group undertaking this optional work, a short document with a summary of the experiments performed, obtained results and some discussion of them should be delivered by June 17th, 2016. Please send this document to alquezar@cs.upc.edu.

Ren�