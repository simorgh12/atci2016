# -*- coding: utf-8 -*-
"""
Created on Tue May 17 18:08:28 2016

@author: Vicent
"""
import sys
import os

def parseFiles(root,verbose=False):
    """
    returns a dictionary with the following format:
    -> filename string as a key.
    -> float 'tuple' (train_acc, test_acc, time); containing the parsed values.    
    i.e.
      d[file][0] := Train Accuracy
      d[file][1] := Test Accuracy
      d[file][2] := Elapsed time
    """
    d={} # init empty dictionary
    for folder, subdirs, files in os.walk(root):
        for fname in files:
            if fname.endswith('.txt'):
                rpath = os.path.join(folder,fname)
                f = open(rpath,'r')
                lines = f.readlines()

                train_acc = lines[-3].split()[-6][:-1]  # train accuracy token
                test_acc = lines[-3].split()[-1]        # test accuracy token
                time = lines[-2].split()[-2]            # elapsed time token 
                d[rpath] = (float(train_acc), float(test_acc), float(time))

                if(verbose):
                    print rpath
                    print '  Train Accuracy',train_acc
                    print '  Test Accuracy',test_acc
                    print '  Elapsed time',time,'\n'
    return d


def rank(d, top):
    return sorted(d.items(), key=lambda x: x[1][1], reverse=True)[:top]


print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)
d = parseFiles(sys.argv[1]); # order files by its accuracy
ranked = rank(d, 20) # show 20 best results

# show results
print "\nTop\t\tTest Accuracy\tTrain Accuracy\tElapsed Time\tFile"
print "-----\t\t--------------\t-------------\t-------------\t------"
for idx, val in enumerate(ranked):
    print idx+1,'\t\t',val[1][1],'\t\t',val[1][0],'\t\t',val[1][2],'\t',val[0]