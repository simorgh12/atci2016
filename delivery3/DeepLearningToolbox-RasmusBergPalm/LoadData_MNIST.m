
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Load MNIST data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DataSet = 'mnist_uint8.mat';
disp(['Loading data set: ' DataSet]);
load(DataSet)

train_x = double(train_x);  train_y = double(train_y);
val_x   = [];               val_y   = [];
test_x  = double(test_x);   test_y  = double(test_y);

train_x = train_x/255;
val_x = val_x/255;
test_x = test_x/255;
