
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Global parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Setting parameters']);

% Random seed and Random initialization
RandomSeed = sum(100*clock);             %%% sum(100*clock) / A fixed value allows to replicate exactly the same results
% 
rand('state',RandomSeed);
randn('state',RandomSeed);

% Data
ReducedData = 10000;                     %%% If ReducedData > 0, it uses 'ReducedData' traininig input vectors for training
BatchSize = 100;                         %%% Values in [10 100]

% Architecture
NHiddens = [200 200 200];                %%% [100] / [500 500] / etc

% Pre-training y/n
PreTrainSAE = true;                      %%% true: pre-training with Stacked Denoising AutoEncoders

% Pre-training parameters
PreTrainSAE_IniWRange = 1.0;             %%% Initial weights in Stacked Denoising AutoEncoders pre-training from IniVHRange * Gaussian(0,1) / sqrt(fanin)
PreTrainSAE_NumEpochs = 20;              %%% Number of epochs in Stacked Denoising AutoEncoders pre-training
PreTrainSAE_LearningRate = 0.1;          %%% Learning rate in Stacked Denoising AutoEncoders pre-training
PreTrainSAE_Scaling_learningRate = 1;    %%% Scaling factor for the learning rate (in each epoch) in Stacked Denoising AutoEncoders pre-training
PreTrainSAE_Momentum = 0.8;              %%% Values in [0,1] / Momentum in Stacked Denoising AutoEncoders pre-training
PreTrainSAE_WeightPenaltyL2 = 0;         %%% Weight decay penalty in Stacked Denoising AutoEncoders pre-training
PreTrainSAE_InputZeroFraction = 0.5;     %%% Values in [0,1] / Fraction of inputs set to zero in Stacked *DENOISING* AutoEncoders pre-training
PreTrainSAE_VisualizeWeights = false;    %%% Weight visualization in Stacked Denoising AutoEncoders pre-training

% Fine-tuning parameters
FineTuningBP_IniWRange = 1.0;            %%% Initial NOT PRETRAINED weights in BP fine-tuning from IniWRange * Gaussian(0,1) / sqrt(fanin)
FineTuningBP_NumEpochs = 20;             %%% Number of epochs in BP fine-tuning
FineTuningBP_LearningRate = 0.1;         %%% Learning rate in BP fine-tuning
FineTuningBP_Scaling_learningRate = 1;   %%% Scaling factor for the learning rate (in each epoch) in BP fine-tuning
FineTuningBP_Momentum = 0.8;             %%% Values in [0,1] / Momentum in BP fine-tuning
FineTuningBP_WeightPenaltyL2 = 0;        %%% Weight decay penalty in BP fine-tuning
FineTuningBP_DropoutFraction = 0;        %%% Values in [0,1] / Dropout level (http://www.cs.toronto.edu/~hinton/absps/dropout.pdf)
FineTuningBP_PlotError = 0;              %%% Plot the error curves (0/1)?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

return;
