
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DO NOT MODIFY FROM HERE !!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Start Training']);

%%% Reduced Data
if ReducedData > 0
  r = randperm(size(train_x,1));
  train_x = train_x(r(1:ReducedData),:);
  train_y = train_y(r(1:ReducedData),:);
end;
%%% Prepare data to CNN
train_x = double(reshape(train_x',28,28,size(train_x,1)))/255;
test_x = double(reshape(test_x',28,28,size(test_y,1)))/255;
train_y = double(train_y');
test_y = double(test_y');

%%% Auxiliar variables
NExamples = size(train_x,1);

%%% Show information
disp(['Number of training examples: ' num2str(NExamples) '  BatchSize: ' num2str(BatchSize)]);
disp(['Architecture: ']);
n = numel(CNNLayers);
for l = 2 : n
  if strcmp(CNNLayers{l}.type, 'c')
    disp([' Layer ' num2str(l-1) ': Convolutional layer with ' ...
          num2str(CNNLayers{l}.outputmaps) ' filters of size ' num2str(CNNLayers{l}.kernelsize) 'x' num2str(CNNLayers{l}.kernelsize)]);
  end;
  if strcmp(CNNLayers{l}.type, 's')
    disp([' Layer ' num2str(l-1) ': Mean-pooling layer of ' num2str(CNNLayers{l}.scale) 'x' num2str(CNNLayers{l}.scale) ' convolutions']);
  end;
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%% Train

tstart = tic;

CNNet.layers = CNNLayers;
CNNet = cnnsetup(CNNet, train_x, train_y);

CNNopts.batchsize = BatchSize;
CNNopts.numepochs = NumEpochs;
CNNopts.alpha = LearningRate;
CNNopts.plot       = PlotError;
CNNet = cnntrain(CNNet, train_x, train_y, CNNopts, val_x, val_y, test_x, test_y);

toc(tstart);

disp(['End Training']);

%%%%%%%%%%%%%%%%%%%%%%%%%%% End train

return;
