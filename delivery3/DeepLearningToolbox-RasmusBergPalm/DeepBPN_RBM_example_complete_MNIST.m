
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Training a Deep BackPropagation Network with (optionally) RBM pretraining:
%%%  Every layer is (optionally) pretrained as a Restricted Boltmann Machine
%%%   in a greedy layer-wise unsupervised way with Contrastive Divergence
%%%  Subsequently, the output layer is added and the whole network is trained
%%%   (finetuned) with supervised BackPropagation
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

%%% Recursive addpath
ToolboxPath = './ToolboxDeepLearning/';
addpath(genpath(ToolboxPath));
DataPath = './Data/';
addpath(genpath(DataPath));

DeepBPN_RBM_set_parameters_MNIST;

LoadData_MNIST;

DeepBPN_RBM_train_models;

PlotWeights_RBM_example_complete_MNIST;