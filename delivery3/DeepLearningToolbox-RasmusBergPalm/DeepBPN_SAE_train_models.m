
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DO NOT MODIFY FROM HERE !!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Start Training']);

%%% Reduced Data
if ReducedData > 0
  r = randperm(size(train_x,1));
  train_x = train_x(r(1:ReducedData),:);
  train_y = train_y(r(1:ReducedData),:);
end;

%%% Auxiliar variables
NInputs = size(train_x,2);
NOutputs = size(train_y,2);
NExamples = size(train_x,1);

%%% Show information
disp(['Number of training examples: ' num2str(NExamples) '  BatchSize: ' num2str(BatchSize)]);
disp(['Number of inputs: ' num2str(NInputs) '  Number of outputs: ' num2str(NOutputs)]);
NHiddenLayers = length(NHiddens);
disp(['Number of hidden layers: ' num2str(NHiddenLayers) ' (' num2str(NHiddens) ')']);

%%%%%%%%%%%%%%%%%%%%%%%%%%% Train

tstart = tic;

%%% Pre-training
if PreTrainSAE
  SAENopts.iniWRange = PreTrainSAE_IniWRange;    %%% PREVIOUS TO saesetup !!!
  SAENet = saesetup([NInputs NHiddens],SAENopts);
  for i = 1 : numel(NHiddens)
    SAENet.ae{i}.activation_function = 'sigm';
    SAENet.ae{i}.output = 'sigm';
    SAENet.ae{i}.learningRate = PreTrainSAE_LearningRate;
    SAENet.ae{i}.scaling_learningRate = PreTrainSAE_Scaling_learningRate;
    SAENet.ae{i}.momentum = PreTrainSAE_Momentum;
    SAENet.ae{i}.weightPenaltyL2 = PreTrainSAE_WeightPenaltyL2;
    SAENet.ae{i}.dropoutFraction = 0;
    SAENet.ae{i}.nonSparsityPenalty  = 0;      %%%  Fixed non sparsity penalty (sparsity not used)
    SAENet.ae{i}.sparsityTarget = 0.05;        %%%  Fixed sparsity target
    SAENet.ae{i}.inputZeroMaskedFraction = PreTrainSAE_InputZeroFraction;
  end;
  SAENopts.numepochs = PreTrainSAE_NumEpochs;
  SAENopts.batchsize = BatchSize;
  SAENet = saetrain(SAENet, train_x, SAENopts);
  if PreTrainSAE_VisualizeWeights
    visualize(SAENet.ae{1}.W{1}(:,2:end)')
  end;
  % Use the SDAE to initialize a FFNN
  BPNNopts.iniWRange = FineTuningBP_IniWRange;              %%% PREVIOUS TO nnsetup !!!
  BPNNet = nnsetup([NInputs NHiddens NOutputs], BPNNopts);
  for i = 1 : numel(NHiddens)
    BPNNet.W{i} = SAENet.ae{i}.W{1};
  end;
else
  BPNNopts.iniWRange = FineTuningBP_IniWRange;              %%% PREVIOUS TO nnsetup !!!
  BPNNet = nnsetup([NInputs NHiddens NOutputs], BPNNopts);
end;


%%% Fine-tuning
BPNNet.activation_function = 'sigm';
BPNNet.output = 'softmax';
BPNNet.learningRate = FineTuningBP_LearningRate;
BPNNet.scaling_learningRate = FineTuningBP_Scaling_learningRate;
BPNNet.momentum = FineTuningBP_Momentum;
BPNNet.weightPenaltyL2 = FineTuningBP_WeightPenaltyL2;
BPNNet.dropoutFraction = FineTuningBP_DropoutFraction;
BPNNet.nonSparsityPenalty  = 0;      %%%  Fixed non sparsity penalty (sparsity not used)
BPNNet.sparsityTarget = 0.05;        %%%  Fixed sparsity target
BPNNet.inputZeroMaskedFraction = 0;  %%%  Only used for Denoising AutoEncoders (percentage of masked 0s)
BPNNopts.numepochs  = FineTuningBP_NumEpochs;
BPNNopts.batchsize  = BatchSize;
BPNNopts.plot       = FineTuningBP_PlotError;
BPNNet = nntrain(BPNNet, train_x, train_y, BPNNopts, val_x, val_y, test_x, test_y);

toc(tstart);

disp(['End Training']);

%%%%%%%%%%%%%%%%%%%%%%%%%%% End train

return;
