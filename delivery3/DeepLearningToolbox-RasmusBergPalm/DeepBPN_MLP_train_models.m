
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DO NOT MODIFY FROM HERE !!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Start Training']);

%%% Reduced Data
if ReducedData > 0
  r = randperm(size(train_x,1));
  train_x = train_x(r(1:ReducedData),:);
  train_y = train_y(r(1:ReducedData),:);
end;

%%% Auxiliar variables
NInputs = size(train_x,2);
NOutputs = size(train_y,2);
NExamples = size(train_x,1);

%%% Show information
disp(['Number of training examples: ' num2str(NExamples) '  BatchSize: ' num2str(BatchSize)]);
disp(['Number of inputs: ' num2str(NInputs) '  Number of outputs: ' num2str(NOutputs)]);
NHiddenLayers = length(NHiddens);
disp(['Number of hidden layers: ' num2str(NHiddenLayers) ' (' num2str(NHiddens) ')']);

%%%%%%%%%%%%%%%%%%%%%%%%%%% Train

tstart = tic;

BPNNopts.iniWRange = TrainBP_IniWRange;    %%% PREVIOUS TO nnsetup !!!
BPNNet = nnsetup([NInputs NHiddens NOutputs], BPNNopts);

%%% Rest of parameters
BPNNet.activation_function = ActFunction;
BPNNet.output = 'softmax';
BPNNet.learningRate = TrainBP_LearningRate;
BPNNet.scaling_learningRate = TrainBP_Scaling_learningRate;
BPNNet.momentum = TrainBP_Momentum;
BPNNet.weightPenaltyL2 = TrainBP_WeightPenaltyL2;
BPNNet.dropoutFraction = TrainBP_DropoutFraction;
BPNNet.nonSparsityPenalty  = 0;      %%%  Fixed non sparsity penalty (sparsity not used)
BPNNet.sparsityTarget = 0.05;        %%%  Fixed sparsity target
BPNNet.inputZeroMaskedFraction = 0;  %%%  Only used for Denoising AutoEncoders (percentage of masked 0s)
BPNNopts.numepochs  = TrainBP_NumEpochs;
BPNNopts.batchsize  = BatchSize;
BPNNopts.plot       = TrainBP_PlotError;
BPNNet = nntrain(BPNNet, train_x, train_y, BPNNopts, val_x, val_y, test_x, test_y);

toc(tstart);

disp(['End Training']);
%%%%%%%%%%%%%%%%%%%%%%%%%%% End train

return;
