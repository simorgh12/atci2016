
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Training a Deep Convolutional Neural Network
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;

%%% Recursive addpath
ToolboxPath = './ToolboxDeepLearning/';
addpath(genpath(ToolboxPath));
DataPath = './Data/';
addpath(genpath(DataPath));

DeepCNN_set_parameters_MNIST;

LoadData_MNIST;

DeepCNN_train_models;
