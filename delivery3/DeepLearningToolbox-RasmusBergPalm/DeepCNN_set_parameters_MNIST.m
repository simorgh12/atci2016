
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Global parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Setting parameters']);

% Random seed and Random initialization
RandomSeed = sum(100*clock);   %%% sum(100*clock) / A fixed value allows to replicate exactly the same results
% 
rand('state',RandomSeed);
randn('state',RandomSeed);

% Data
ReducedData = 1000;            %%% If ReducedData > 0, it uses 'ReducedData' traininig input vectors for training
BatchSize = 100;               %%% Values in [10 100]

% Architecture
CNNLayers = {
    % Input layer
    struct('type', 'i')
    % Convolution layer with  6 filters (outputmaps) of kernelsize 5x5
    struct('type', 'c', 'outputmaps', 6, 'kernelsize', 5)
    % Subsampling layer with scale 2 (mean-pooling of 2x2 convolutions)
    struct('type', 's', 'scale', 2)
    % Convolution layer with 12 filters (outputmaps) of kernelsize 5x5
    struct('type', 'c', 'outputmaps', 12, 'kernelsize', 5)
    % Subsampling layer with scale 2 (mean-pooling of 2x2 convolutions)
    struct('type', 's', 'scale', 2)
};

% Learning parameters
NumEpochs = 10;            %%% Number of epochs
LearningRate = 0.2;        %%% Learning rate
PlotError = 0;             %%% Plot the error curves (0/1)?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

return;
