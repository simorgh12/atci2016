
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DO NOT MODIFY FROM HERE !!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Start Training']);

%%% Reduced Data
if ReducedData > 0
  r = randperm(size(train_x,1));
  train_x = train_x(r(1:ReducedData),:);
  train_y = train_y(r(1:ReducedData),:);
end;

%%% Auxiliar variables
NInputs = size(train_x,2);
NOutputs = size(train_y,2);
NExamples = size(train_x,1);

%%% Show information
disp(['Number of training examples: ' num2str(NExamples) '  BatchSize: ' num2str(BatchSize)]);
disp(['Number of inputs: ' num2str(NInputs) '  Number of outputs: ' num2str(NOutputs)]);
NHiddenLayers = length(NHiddens);
disp(['Number of hidden layers: ' num2str(NHiddenLayers) ' (' num2str(NHiddens) ')']);

%%%%%%%%%%%%%%%%%%%%%%%%%%% Train

tstart = tic;

%%% Needed in all cases
BPNNopts.iniWRange = FineTuningBP_IniWRange;               %%% PREVIOUS TO dbnunfoldtonn/nnsetup !!!

%%% Pre-training
if PreTrainRBM
  DBNNet.sizes = NHiddens;
  DBNNopts.alpha = PreTrainRBM_LearningRate;               %%% PREVIOUS TO dbnsetup !!!
  DBNNopts.momentum = PreTrainRBM_Momentum;                %%% PREVIOUS TO dbnsetup !!!
  DBNNopts.weightPenaltyL2 = PreTrainRBM_WeightPenaltyL2;  %%% PREVIOUS TO dbnsetup !!!
  DBNNopts.iniVHRange = PreTrainRBM_IniVHRange;            %%% PREVIOUS TO dbnsetup !!!
  DBNNopts.inputDataType = PreTrainRBM_InputDataType;      %%% PREVIOUS TO dbnsetup !!!
  DBNNet = dbnsetup(DBNNet, train_x, DBNNopts);
  DBNNopts.numepochs = PreTrainRBM_NumEpochs;
  DBNNopts.batchsize = BatchSize;
  DBNNet = dbntrain(DBNNet, train_x, DBNNopts);
  if PreTrainRBM_VisualizeWeights
    figure; visualize(DBNNet.rbm{1}.W');
  end;
  % Unfold DBNNet to BPNNet
  BPNNet = dbnunfoldtonn(DBNNet, NOutputs, BPNNopts);
else
  BPNNet = nnsetup([NInputs NHiddens NOutputs], BPNNopts);
end;

%%% Fine-tuning
BPNNet.activation_function = 'sigm';
BPNNet.output = 'softmax';
BPNNet.learningRate = FineTuningBP_LearningRate;
BPNNet.scaling_learningRate = FineTuningBP_Scaling_learningRate;
BPNNet.momentum = FineTuningBP_Momentum;
BPNNet.weightPenaltyL2 = FineTuningBP_WeightPenaltyL2;
BPNNet.dropoutFraction = FineTuningBP_DropoutFraction;
BPNNet.nonSparsityPenalty  = 0;      %%%  Fixed non sparsity penalty (sparsity not used)
BPNNet.sparsityTarget = 0.05;        %%%  Fixed sparsity target
BPNNet.inputZeroMaskedFraction = 0;  %%%  Only used for Denoising AutoEncoders (percentage of masked 0s)
BPNNopts.numepochs  = FineTuningBP_NumEpochs;
BPNNopts.batchsize  = BatchSize;
BPNNopts.plot       = FineTuningBP_PlotError;
BPNNet = nntrain(BPNNet, train_x, train_y, BPNNopts, val_x, val_y, test_x, test_y);

toc(tstart);

disp(['End Training']);

%%%%%%%%%%%%%%%%%%%%%%%%%%% End train

return;
