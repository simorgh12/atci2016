Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	2000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 2000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.10846 seconds. Average reconstruction error is: 52.395
 epoch 2/20. Took 0.10746 seconds. Average reconstruction error is: 28.6693
 epoch 3/20. Took 0.10616 seconds. Average reconstruction error is: 24.0801
 epoch 4/20. Took 0.10717 seconds. Average reconstruction error is: 21.543
 epoch 5/20. Took 0.10641 seconds. Average reconstruction error is: 19.8285
 epoch 6/20. Took 0.11232 seconds. Average reconstruction error is: 18.6049
 epoch 7/20. Took 0.10865 seconds. Average reconstruction error is: 17.7022
 epoch 8/20. Took 0.10619 seconds. Average reconstruction error is: 16.9159
 epoch 9/20. Took 0.10651 seconds. Average reconstruction error is: 16.2992
 epoch 10/20. Took 0.10566 seconds. Average reconstruction error is: 15.7733
 epoch 11/20. Took 0.10557 seconds. Average reconstruction error is: 15.3343
 epoch 12/20. Took 0.1076 seconds. Average reconstruction error is: 14.8151
 epoch 13/20. Took 0.10633 seconds. Average reconstruction error is: 14.4773
 epoch 14/20. Took 0.10746 seconds. Average reconstruction error is: 14.1357
 epoch 15/20. Took 0.10719 seconds. Average reconstruction error is: 13.8338
 epoch 16/20. Took 0.10671 seconds. Average reconstruction error is: 13.5245
 epoch 17/20. Took 0.10784 seconds. Average reconstruction error is: 13.2429
 epoch 18/20. Took 0.10612 seconds. Average reconstruction error is: 13.0358
 epoch 19/20. Took 0.10552 seconds. Average reconstruction error is: 12.7058
 epoch 20/20. Took 0.10879 seconds. Average reconstruction error is: 12.5826
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.035467 seconds. Average reconstruction error is: 18.8397
 epoch 2/20. Took 0.036685 seconds. Average reconstruction error is: 10.5865
 epoch 3/20. Took 0.036007 seconds. Average reconstruction error is: 8.2929
 epoch 4/20. Took 0.036176 seconds. Average reconstruction error is: 7.1746
 epoch 5/20. Took 0.035979 seconds. Average reconstruction error is: 6.4559
 epoch 6/20. Took 0.036548 seconds. Average reconstruction error is: 5.9419
 epoch 7/20. Took 0.036037 seconds. Average reconstruction error is: 5.6066
 epoch 8/20. Took 0.03579 seconds. Average reconstruction error is: 5.3394
 epoch 9/20. Took 0.036323 seconds. Average reconstruction error is: 5.1947
 epoch 10/20. Took 0.037161 seconds. Average reconstruction error is: 4.9253
 epoch 11/20. Took 0.036266 seconds. Average reconstruction error is: 4.7411
 epoch 12/20. Took 0.036768 seconds. Average reconstruction error is: 4.6481
 epoch 13/20. Took 0.036534 seconds. Average reconstruction error is: 4.5386
 epoch 14/20. Took 0.03657 seconds. Average reconstruction error is: 4.4502
 epoch 15/20. Took 0.036522 seconds. Average reconstruction error is: 4.4064
 epoch 16/20. Took 0.035876 seconds. Average reconstruction error is: 4.3225
 epoch 17/20. Took 0.036714 seconds. Average reconstruction error is: 4.1826
 epoch 18/20. Took 0.036301 seconds. Average reconstruction error is: 4.1664
 epoch 19/20. Took 0.03689 seconds. Average reconstruction error is: 4.0803
 epoch 20/20. Took 0.036737 seconds. Average reconstruction error is: 4.0527
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.036127 seconds. Average reconstruction error is: 17.1028
 epoch 2/20. Took 0.036852 seconds. Average reconstruction error is: 9.2392
 epoch 3/20. Took 0.036461 seconds. Average reconstruction error is: 7.2327
 epoch 4/20. Took 0.036767 seconds. Average reconstruction error is: 6.2438
 epoch 5/20. Took 0.037273 seconds. Average reconstruction error is: 5.6831
 epoch 6/20. Took 0.036676 seconds. Average reconstruction error is: 5.365
 epoch 7/20. Took 0.036291 seconds. Average reconstruction error is: 5.1281
 epoch 8/20. Took 0.036239 seconds. Average reconstruction error is: 4.9158
 epoch 9/20. Took 0.036827 seconds. Average reconstruction error is: 4.7161
 epoch 10/20. Took 0.036845 seconds. Average reconstruction error is: 4.6378
 epoch 11/20. Took 0.035686 seconds. Average reconstruction error is: 4.5426
 epoch 12/20. Took 0.036367 seconds. Average reconstruction error is: 4.5156
 epoch 13/20. Took 0.036655 seconds. Average reconstruction error is: 4.4217
 epoch 14/20. Took 0.036568 seconds. Average reconstruction error is: 4.3957
 epoch 15/20. Took 0.036747 seconds. Average reconstruction error is: 4.3325
 epoch 16/20. Took 0.036763 seconds. Average reconstruction error is: 4.3502
 epoch 17/20. Took 0.036294 seconds. Average reconstruction error is: 4.2519
 epoch 18/20. Took 0.036746 seconds. Average reconstruction error is: 4.2367
 epoch 19/20. Took 0.037247 seconds. Average reconstruction error is: 4.1792
 epoch 20/20. Took 0.03688 seconds. Average reconstruction error is: 4.1806
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.10645 seconds. 
  Full-batch training loss = 0.524852, test loss = 0.536045
  Training set accuracy = 0.854000, Test set accuracy = 0.848700
 epoch 2/100. Took 0.10867 seconds. 
  Full-batch training loss = 0.372365, test loss = 0.404443
  Training set accuracy = 0.902000, Test set accuracy = 0.888300
 epoch 3/100. Took 0.1086 seconds. 
  Full-batch training loss = 0.314945, test loss = 0.366011
  Training set accuracy = 0.913000, Test set accuracy = 0.893200
 epoch 4/100. Took 0.11 seconds. 
  Full-batch training loss = 0.271121, test loss = 0.334923
  Training set accuracy = 0.926500, Test set accuracy = 0.902000
 epoch 5/100. Took 0.11166 seconds. 
  Full-batch training loss = 0.243438, test loss = 0.321188
  Training set accuracy = 0.935500, Test set accuracy = 0.905400
 epoch 6/100. Took 0.10782 seconds. 
  Full-batch training loss = 0.217994, test loss = 0.308477
  Training set accuracy = 0.944000, Test set accuracy = 0.909100
 epoch 7/100. Took 0.10884 seconds. 
  Full-batch training loss = 0.196109, test loss = 0.298261
  Training set accuracy = 0.950000, Test set accuracy = 0.912400
 epoch 8/100. Took 0.10864 seconds. 
  Full-batch training loss = 0.180201, test loss = 0.290271
  Training set accuracy = 0.952500, Test set accuracy = 0.915900
 epoch 9/100. Took 0.10834 seconds. 
  Full-batch training loss = 0.164408, test loss = 0.285629
  Training set accuracy = 0.956500, Test set accuracy = 0.915400
 epoch 10/100. Took 0.10995 seconds. 
  Full-batch training loss = 0.153390, test loss = 0.283811
  Training set accuracy = 0.962500, Test set accuracy = 0.916500
 epoch 11/100. Took 0.10899 seconds. 
  Full-batch training loss = 0.138739, test loss = 0.279901
  Training set accuracy = 0.964000, Test set accuracy = 0.916600
 epoch 12/100. Took 0.10787 seconds. 
  Full-batch training loss = 0.130484, test loss = 0.279966
  Training set accuracy = 0.967000, Test set accuracy = 0.917300
 epoch 13/100. Took 0.10849 seconds. 
  Full-batch training loss = 0.117738, test loss = 0.272253
  Training set accuracy = 0.972000, Test set accuracy = 0.921400
 epoch 14/100. Took 0.10916 seconds. 
  Full-batch training loss = 0.112110, test loss = 0.277558
  Training set accuracy = 0.972000, Test set accuracy = 0.918100
 epoch 15/100. Took 0.11033 seconds. 
  Full-batch training loss = 0.100103, test loss = 0.269075
  Training set accuracy = 0.975000, Test set accuracy = 0.920600
 epoch 16/100. Took 0.11052 seconds. 
  Full-batch training loss = 0.092733, test loss = 0.269997
  Training set accuracy = 0.979000, Test set accuracy = 0.920800
 epoch 17/100. Took 0.1095 seconds. 
  Full-batch training loss = 0.086669, test loss = 0.268244
  Training set accuracy = 0.983000, Test set accuracy = 0.921300
 epoch 18/100. Took 0.10959 seconds. 
  Full-batch training loss = 0.079657, test loss = 0.267401
  Training set accuracy = 0.985000, Test set accuracy = 0.922400
 epoch 19/100. Took 0.10856 seconds. 
  Full-batch training loss = 0.073452, test loss = 0.267425
  Training set accuracy = 0.989000, Test set accuracy = 0.921400
 epoch 20/100. Took 0.10908 seconds. 
  Full-batch training loss = 0.068593, test loss = 0.266650
  Training set accuracy = 0.991000, Test set accuracy = 0.922200
 epoch 21/100. Took 0.10996 seconds. 
  Full-batch training loss = 0.063180, test loss = 0.264543
  Training set accuracy = 0.994500, Test set accuracy = 0.923100
 epoch 22/100. Took 0.10887 seconds. 
  Full-batch training loss = 0.059663, test loss = 0.264218
  Training set accuracy = 0.993500, Test set accuracy = 0.923300
 epoch 23/100. Took 0.10983 seconds. 
  Full-batch training loss = 0.056068, test loss = 0.266097
  Training set accuracy = 0.995000, Test set accuracy = 0.923500
 epoch 24/100. Took 0.10926 seconds. 
  Full-batch training loss = 0.051758, test loss = 0.264753
  Training set accuracy = 0.996000, Test set accuracy = 0.923600
 epoch 25/100. Took 0.10882 seconds. 
  Full-batch training loss = 0.048355, test loss = 0.264749
  Training set accuracy = 0.996000, Test set accuracy = 0.924300
 epoch 26/100. Took 0.10944 seconds. 
  Full-batch training loss = 0.045880, test loss = 0.267547
  Training set accuracy = 0.996500, Test set accuracy = 0.923500
 epoch 27/100. Took 0.10748 seconds. 
  Full-batch training loss = 0.042798, test loss = 0.264758
  Training set accuracy = 0.998000, Test set accuracy = 0.924500
 epoch 28/100. Took 0.10882 seconds. 
  Full-batch training loss = 0.040288, test loss = 0.264611
  Training set accuracy = 0.998500, Test set accuracy = 0.924500
 epoch 29/100. Took 0.10911 seconds. 
  Full-batch training loss = 0.038053, test loss = 0.265846
  Training set accuracy = 0.998500, Test set accuracy = 0.924400
 epoch 30/100. Took 0.10945 seconds. 
  Full-batch training loss = 0.036204, test loss = 0.266032
  Training set accuracy = 0.999000, Test set accuracy = 0.924400
 epoch 31/100. Took 0.10968 seconds. 
  Full-batch training loss = 0.034315, test loss = 0.266869
  Training set accuracy = 0.999000, Test set accuracy = 0.924600
 epoch 32/100. Took 0.10989 seconds. 
  Full-batch training loss = 0.032539, test loss = 0.267407
  Training set accuracy = 0.999000, Test set accuracy = 0.924400
 epoch 33/100. Took 0.10841 seconds. 
  Full-batch training loss = 0.030887, test loss = 0.267676
  Training set accuracy = 0.999000, Test set accuracy = 0.925400
 epoch 34/100. Took 0.11091 seconds. 
  Full-batch training loss = 0.029473, test loss = 0.268516
  Training set accuracy = 0.999000, Test set accuracy = 0.924500
 epoch 35/100. Took 0.10995 seconds. 
  Full-batch training loss = 0.028075, test loss = 0.268336
  Training set accuracy = 0.999000, Test set accuracy = 0.925700
 epoch 36/100. Took 0.11055 seconds. 
  Full-batch training loss = 0.026866, test loss = 0.268727
  Training set accuracy = 0.999000, Test set accuracy = 0.924800
 epoch 37/100. Took 0.1113 seconds. 
  Full-batch training loss = 0.025474, test loss = 0.269783
  Training set accuracy = 0.999000, Test set accuracy = 0.925400
 epoch 38/100. Took 0.11075 seconds. 
  Full-batch training loss = 0.024640, test loss = 0.271216
  Training set accuracy = 0.999500, Test set accuracy = 0.924700
 epoch 39/100. Took 0.10938 seconds. 
  Full-batch training loss = 0.023402, test loss = 0.270179
  Training set accuracy = 0.999500, Test set accuracy = 0.925300
 epoch 40/100. Took 0.10866 seconds. 
  Full-batch training loss = 0.022427, test loss = 0.270758
  Training set accuracy = 1.000000, Test set accuracy = 0.924800
 epoch 41/100. Took 0.11177 seconds. 
  Full-batch training loss = 0.021541, test loss = 0.271067
  Training set accuracy = 1.000000, Test set accuracy = 0.925300
 epoch 42/100. Took 0.10971 seconds. 
  Full-batch training loss = 0.020701, test loss = 0.271514
  Training set accuracy = 1.000000, Test set accuracy = 0.924800
 epoch 43/100. Took 0.10981 seconds. 
  Full-batch training loss = 0.019926, test loss = 0.272586
  Training set accuracy = 1.000000, Test set accuracy = 0.924900
 epoch 44/100. Took 0.10906 seconds. 
  Full-batch training loss = 0.019215, test loss = 0.272723
  Training set accuracy = 1.000000, Test set accuracy = 0.925000
 epoch 45/100. Took 0.11056 seconds. 
  Full-batch training loss = 0.018493, test loss = 0.273175
  Training set accuracy = 1.000000, Test set accuracy = 0.925300
 epoch 46/100. Took 0.1094 seconds. 
  Full-batch training loss = 0.017858, test loss = 0.273791
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 47/100. Took 0.11257 seconds. 
  Full-batch training loss = 0.017263, test loss = 0.274555
  Training set accuracy = 1.000000, Test set accuracy = 0.925000
 epoch 48/100. Took 0.10971 seconds. 
  Full-batch training loss = 0.016740, test loss = 0.274634
  Training set accuracy = 1.000000, Test set accuracy = 0.926000
 epoch 49/100. Took 0.11089 seconds. 
  Full-batch training loss = 0.016210, test loss = 0.275647
  Training set accuracy = 1.000000, Test set accuracy = 0.925200
 epoch 50/100. Took 0.10888 seconds. 
  Full-batch training loss = 0.015674, test loss = 0.275874
  Training set accuracy = 1.000000, Test set accuracy = 0.925500
 epoch 51/100. Took 0.10913 seconds. 
  Full-batch training loss = 0.015118, test loss = 0.276298
  Training set accuracy = 1.000000, Test set accuracy = 0.925100
 epoch 52/100. Took 0.10889 seconds. 
  Full-batch training loss = 0.014706, test loss = 0.277043
  Training set accuracy = 1.000000, Test set accuracy = 0.925700
 epoch 53/100. Took 0.10941 seconds. 
  Full-batch training loss = 0.014234, test loss = 0.277094
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 54/100. Took 0.10853 seconds. 
  Full-batch training loss = 0.013833, test loss = 0.277806
  Training set accuracy = 1.000000, Test set accuracy = 0.925500
 epoch 55/100. Took 0.10872 seconds. 
  Full-batch training loss = 0.013436, test loss = 0.278054
  Training set accuracy = 1.000000, Test set accuracy = 0.925600
 epoch 56/100. Took 0.10947 seconds. 
  Full-batch training loss = 0.013060, test loss = 0.278857
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 57/100. Took 0.11073 seconds. 
  Full-batch training loss = 0.012716, test loss = 0.279210
  Training set accuracy = 1.000000, Test set accuracy = 0.925600
 epoch 58/100. Took 0.10941 seconds. 
  Full-batch training loss = 0.012360, test loss = 0.279663
  Training set accuracy = 1.000000, Test set accuracy = 0.925600
 epoch 59/100. Took 0.10884 seconds. 
  Full-batch training loss = 0.012043, test loss = 0.279848
  Training set accuracy = 1.000000, Test set accuracy = 0.925600
 epoch 60/100. Took 0.11029 seconds. 
  Full-batch training loss = 0.011717, test loss = 0.280438
  Training set accuracy = 1.000000, Test set accuracy = 0.925900
 epoch 61/100. Took 0.11034 seconds. 
  Full-batch training loss = 0.011412, test loss = 0.280712
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 62/100. Took 0.10939 seconds. 
  Full-batch training loss = 0.011138, test loss = 0.281073
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 63/100. Took 0.10886 seconds. 
  Full-batch training loss = 0.010866, test loss = 0.281630
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 64/100. Took 0.10913 seconds. 
  Full-batch training loss = 0.010604, test loss = 0.282135
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 65/100. Took 0.10891 seconds. 
  Full-batch training loss = 0.010353, test loss = 0.282297
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 66/100. Took 0.1091 seconds. 
  Full-batch training loss = 0.010110, test loss = 0.282954
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 67/100. Took 0.10924 seconds. 
  Full-batch training loss = 0.009887, test loss = 0.283127
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 68/100. Took 0.1097 seconds. 
  Full-batch training loss = 0.009666, test loss = 0.283715
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 69/100. Took 0.11129 seconds. 
  Full-batch training loss = 0.009458, test loss = 0.284071
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 70/100. Took 0.11025 seconds. 
  Full-batch training loss = 0.009263, test loss = 0.284476
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 71/100. Took 0.10942 seconds. 
  Full-batch training loss = 0.009061, test loss = 0.285076
  Training set accuracy = 1.000000, Test set accuracy = 0.926200
 epoch 72/100. Took 0.10871 seconds. 
  Full-batch training loss = 0.008867, test loss = 0.285278
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 73/100. Took 0.11102 seconds. 
  Full-batch training loss = 0.008675, test loss = 0.285635
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 74/100. Took 0.10951 seconds. 
  Full-batch training loss = 0.008503, test loss = 0.285882
  Training set accuracy = 1.000000, Test set accuracy = 0.926800
 epoch 75/100. Took 0.10957 seconds. 
  Full-batch training loss = 0.008326, test loss = 0.286512
  Training set accuracy = 1.000000, Test set accuracy = 0.926800
 epoch 76/100. Took 0.10933 seconds. 
  Full-batch training loss = 0.008167, test loss = 0.286829
  Training set accuracy = 1.000000, Test set accuracy = 0.926900
 epoch 77/100. Took 0.11026 seconds. 
  Full-batch training loss = 0.008010, test loss = 0.286887
  Training set accuracy = 1.000000, Test set accuracy = 0.927400
 epoch 78/100. Took 0.10961 seconds. 
  Full-batch training loss = 0.007860, test loss = 0.287060
  Training set accuracy = 1.000000, Test set accuracy = 0.927300
 epoch 79/100. Took 0.11044 seconds. 
  Full-batch training loss = 0.007711, test loss = 0.287672
  Training set accuracy = 1.000000, Test set accuracy = 0.927600
 epoch 80/100. Took 0.10913 seconds. 
  Full-batch training loss = 0.007567, test loss = 0.288024
  Training set accuracy = 1.000000, Test set accuracy = 0.927200
 epoch 81/100. Took 0.10956 seconds. 
  Full-batch training loss = 0.007429, test loss = 0.288399
  Training set accuracy = 1.000000, Test set accuracy = 0.927600
 epoch 82/100. Took 0.10998 seconds. 
  Full-batch training loss = 0.007299, test loss = 0.288806
  Training set accuracy = 1.000000, Test set accuracy = 0.927300
 epoch 83/100. Took 0.10947 seconds. 
  Full-batch training loss = 0.007167, test loss = 0.289239
  Training set accuracy = 1.000000, Test set accuracy = 0.927700
 epoch 84/100. Took 0.10861 seconds. 
  Full-batch training loss = 0.007039, test loss = 0.289477
  Training set accuracy = 1.000000, Test set accuracy = 0.927600
 epoch 85/100. Took 0.11013 seconds. 
  Full-batch training loss = 0.006914, test loss = 0.289929
  Training set accuracy = 1.000000, Test set accuracy = 0.928000
 epoch 86/100. Took 0.10817 seconds. 
  Full-batch training loss = 0.006802, test loss = 0.290174
  Training set accuracy = 1.000000, Test set accuracy = 0.927400
 epoch 87/100. Took 0.1097 seconds. 
  Full-batch training loss = 0.006687, test loss = 0.290467
  Training set accuracy = 1.000000, Test set accuracy = 0.928100
 epoch 88/100. Took 0.10962 seconds. 
  Full-batch training loss = 0.006577, test loss = 0.290661
  Training set accuracy = 1.000000, Test set accuracy = 0.927300
 epoch 89/100. Took 0.10744 seconds. 
  Full-batch training loss = 0.006473, test loss = 0.291250
  Training set accuracy = 1.000000, Test set accuracy = 0.927600
 epoch 90/100. Took 0.10955 seconds. 
  Full-batch training loss = 0.006365, test loss = 0.291425
  Training set accuracy = 1.000000, Test set accuracy = 0.927700
 epoch 91/100. Took 0.10939 seconds. 
  Full-batch training loss = 0.006263, test loss = 0.291760
  Training set accuracy = 1.000000, Test set accuracy = 0.928100
 epoch 92/100. Took 0.11049 seconds. 
  Full-batch training loss = 0.006160, test loss = 0.291922
  Training set accuracy = 1.000000, Test set accuracy = 0.928200
 epoch 93/100. Took 0.11068 seconds. 
  Full-batch training loss = 0.006068, test loss = 0.292270
  Training set accuracy = 1.000000, Test set accuracy = 0.928400
 epoch 94/100. Took 0.10957 seconds. 
  Full-batch training loss = 0.005972, test loss = 0.292703
  Training set accuracy = 1.000000, Test set accuracy = 0.927900
 epoch 95/100. Took 0.11027 seconds. 
  Full-batch training loss = 0.005884, test loss = 0.292989
  Training set accuracy = 1.000000, Test set accuracy = 0.927800
 epoch 96/100. Took 0.10925 seconds. 
  Full-batch training loss = 0.005796, test loss = 0.293272
  Training set accuracy = 1.000000, Test set accuracy = 0.927700
 epoch 97/100. Took 0.10805 seconds. 
  Full-batch training loss = 0.005708, test loss = 0.293524
  Training set accuracy = 1.000000, Test set accuracy = 0.928400
 epoch 98/100. Took 0.10892 seconds. 
  Full-batch training loss = 0.005626, test loss = 0.293871
  Training set accuracy = 1.000000, Test set accuracy = 0.928300
 epoch 99/100. Took 0.1099 seconds. 
  Full-batch training loss = 0.005546, test loss = 0.294125
  Training set accuracy = 1.000000, Test set accuracy = 0.928100
 epoch 100/100. Took 0.109 seconds. 
  Full-batch training loss = 0.005465, test loss = 0.294393
  Training set accuracy = 1.000000, Test set accuracy = 0.928200
Elapsed time is 48.447803 seconds.
End Training
