Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	false
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.5387 seconds. 
  Full-batch training loss = 2.297989, test loss = 2.299221
  Training set accuracy = 0.101500, Test set accuracy = 0.103300
 epoch 2/100. Took 0.54215 seconds. 
  Full-batch training loss = 1.199467, test loss = 1.198838
  Training set accuracy = 0.557300, Test set accuracy = 0.557200
 epoch 3/100. Took 0.54639 seconds. 
  Full-batch training loss = 0.693936, test loss = 0.686422
  Training set accuracy = 0.767400, Test set accuracy = 0.767100
 epoch 4/100. Took 0.54674 seconds. 
  Full-batch training loss = 0.508046, test loss = 0.511191
  Training set accuracy = 0.847600, Test set accuracy = 0.845900
 epoch 5/100. Took 0.54388 seconds. 
  Full-batch training loss = 0.433685, test loss = 0.442855
  Training set accuracy = 0.871100, Test set accuracy = 0.865800
 epoch 6/100. Took 0.5498 seconds. 
  Full-batch training loss = 0.361296, test loss = 0.380000
  Training set accuracy = 0.895300, Test set accuracy = 0.886600
 epoch 7/100. Took 0.55017 seconds. 
  Full-batch training loss = 0.333125, test loss = 0.359226
  Training set accuracy = 0.899700, Test set accuracy = 0.891700
 epoch 8/100. Took 0.54676 seconds. 
  Full-batch training loss = 0.276998, test loss = 0.314813
  Training set accuracy = 0.917700, Test set accuracy = 0.907700
 epoch 9/100. Took 0.54864 seconds. 
  Full-batch training loss = 0.255695, test loss = 0.298463
  Training set accuracy = 0.921600, Test set accuracy = 0.911100
 epoch 10/100. Took 0.54728 seconds. 
  Full-batch training loss = 0.280397, test loss = 0.332602
  Training set accuracy = 0.909100, Test set accuracy = 0.897200
 epoch 11/100. Took 0.54647 seconds. 
  Full-batch training loss = 0.206254, test loss = 0.262458
  Training set accuracy = 0.939200, Test set accuracy = 0.922100
 epoch 12/100. Took 0.54652 seconds. 
  Full-batch training loss = 0.181559, test loss = 0.246543
  Training set accuracy = 0.944400, Test set accuracy = 0.924200
 epoch 13/100. Took 0.54934 seconds. 
  Full-batch training loss = 0.149484, test loss = 0.224401
  Training set accuracy = 0.956900, Test set accuracy = 0.932300
 epoch 14/100. Took 0.55121 seconds. 
  Full-batch training loss = 0.134088, test loss = 0.216057
  Training set accuracy = 0.960500, Test set accuracy = 0.935000
 epoch 15/100. Took 0.54924 seconds. 
  Full-batch training loss = 0.148338, test loss = 0.239898
  Training set accuracy = 0.952800, Test set accuracy = 0.926800
 epoch 16/100. Took 0.5481 seconds. 
  Full-batch training loss = 0.105817, test loss = 0.202833
  Training set accuracy = 0.969300, Test set accuracy = 0.940300
 epoch 17/100. Took 0.54746 seconds. 
  Full-batch training loss = 0.130115, test loss = 0.236952
  Training set accuracy = 0.955400, Test set accuracy = 0.931100
 epoch 18/100. Took 0.55106 seconds. 
  Full-batch training loss = 0.096167, test loss = 0.207375
  Training set accuracy = 0.970800, Test set accuracy = 0.938300
 epoch 19/100. Took 0.54705 seconds. 
  Full-batch training loss = 0.086412, test loss = 0.203431
  Training set accuracy = 0.973900, Test set accuracy = 0.939100
 epoch 20/100. Took 0.54699 seconds. 
  Full-batch training loss = 0.082929, test loss = 0.213161
  Training set accuracy = 0.974000, Test set accuracy = 0.939800
 epoch 21/100. Took 0.5455 seconds. 
  Full-batch training loss = 0.057800, test loss = 0.188445
  Training set accuracy = 0.985500, Test set accuracy = 0.947200
 epoch 22/100. Took 0.54932 seconds. 
  Full-batch training loss = 0.053542, test loss = 0.191856
  Training set accuracy = 0.986200, Test set accuracy = 0.946400
 epoch 23/100. Took 0.54555 seconds. 
  Full-batch training loss = 0.037377, test loss = 0.176365
  Training set accuracy = 0.991400, Test set accuracy = 0.950800
 epoch 24/100. Took 0.54543 seconds. 
  Full-batch training loss = 0.037147, test loss = 0.182740
  Training set accuracy = 0.990700, Test set accuracy = 0.950700
 epoch 25/100. Took 0.54869 seconds. 
  Full-batch training loss = 0.035559, test loss = 0.188869
  Training set accuracy = 0.991200, Test set accuracy = 0.951000
 epoch 26/100. Took 0.54871 seconds. 
  Full-batch training loss = 0.029960, test loss = 0.183669
  Training set accuracy = 0.992900, Test set accuracy = 0.951900
 epoch 27/100. Took 0.54859 seconds. 
  Full-batch training loss = 0.024243, test loss = 0.186476
  Training set accuracy = 0.995300, Test set accuracy = 0.952800
 epoch 28/100. Took 0.54652 seconds. 
  Full-batch training loss = 0.023101, test loss = 0.190270
  Training set accuracy = 0.995200, Test set accuracy = 0.951600
 epoch 29/100. Took 0.55078 seconds. 
  Full-batch training loss = 0.022780, test loss = 0.194317
  Training set accuracy = 0.995200, Test set accuracy = 0.949200
 epoch 30/100. Took 0.54804 seconds. 
  Full-batch training loss = 0.015107, test loss = 0.183299
  Training set accuracy = 0.998100, Test set accuracy = 0.955500
 epoch 31/100. Took 0.54672 seconds. 
  Full-batch training loss = 0.015271, test loss = 0.190724
  Training set accuracy = 0.998500, Test set accuracy = 0.953500
 epoch 32/100. Took 0.54945 seconds. 
  Full-batch training loss = 0.010846, test loss = 0.188091
  Training set accuracy = 0.999200, Test set accuracy = 0.955400
 epoch 33/100. Took 0.55269 seconds. 
  Full-batch training loss = 0.010988, test loss = 0.195315
  Training set accuracy = 0.999300, Test set accuracy = 0.953200
 epoch 34/100. Took 0.54797 seconds. 
  Full-batch training loss = 0.008214, test loss = 0.190623
  Training set accuracy = 0.999500, Test set accuracy = 0.954700
 epoch 35/100. Took 0.54654 seconds. 
  Full-batch training loss = 0.006008, test loss = 0.187144
  Training set accuracy = 0.999800, Test set accuracy = 0.955500
 epoch 36/100. Took 0.54938 seconds. 
  Full-batch training loss = 0.005943, test loss = 0.188619
  Training set accuracy = 0.999800, Test set accuracy = 0.955500
 epoch 37/100. Took 0.54406 seconds. 
  Full-batch training loss = 0.005229, test loss = 0.189731
  Training set accuracy = 0.999900, Test set accuracy = 0.954100
 epoch 38/100. Took 0.54707 seconds. 
  Full-batch training loss = 0.004725, test loss = 0.190966
  Training set accuracy = 0.999800, Test set accuracy = 0.956300
 epoch 39/100. Took 0.55004 seconds. 
  Full-batch training loss = 0.005147, test loss = 0.197033
  Training set accuracy = 0.999900, Test set accuracy = 0.955000
 epoch 40/100. Took 0.54642 seconds. 
  Full-batch training loss = 0.004753, test loss = 0.197907
  Training set accuracy = 0.999600, Test set accuracy = 0.956000
 epoch 41/100. Took 0.54726 seconds. 
  Full-batch training loss = 0.003651, test loss = 0.195626
  Training set accuracy = 0.999800, Test set accuracy = 0.955900
 epoch 42/100. Took 0.54766 seconds. 
  Full-batch training loss = 0.003277, test loss = 0.195752
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 43/100. Took 0.54431 seconds. 
  Full-batch training loss = 0.003053, test loss = 0.196573
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 44/100. Took 0.54743 seconds. 
  Full-batch training loss = 0.002841, test loss = 0.199321
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 45/100. Took 0.55557 seconds. 
  Full-batch training loss = 0.002791, test loss = 0.199287
  Training set accuracy = 1.000000, Test set accuracy = 0.956000
 epoch 46/100. Took 0.55176 seconds. 
  Full-batch training loss = 0.002542, test loss = 0.200737
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 47/100. Took 0.5437 seconds. 
  Full-batch training loss = 0.002493, test loss = 0.202324
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 48/100. Took 0.54601 seconds. 
  Full-batch training loss = 0.002314, test loss = 0.201946
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 49/100. Took 0.54726 seconds. 
  Full-batch training loss = 0.002154, test loss = 0.201937
  Training set accuracy = 1.000000, Test set accuracy = 0.956100
 epoch 50/100. Took 0.5479 seconds. 
  Full-batch training loss = 0.002271, test loss = 0.204656
  Training set accuracy = 1.000000, Test set accuracy = 0.956000
 epoch 51/100. Took 0.54652 seconds. 
  Full-batch training loss = 0.001977, test loss = 0.203904
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 52/100. Took 0.54794 seconds. 
  Full-batch training loss = 0.001858, test loss = 0.204460
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 53/100. Took 0.55037 seconds. 
  Full-batch training loss = 0.001928, test loss = 0.206108
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 54/100. Took 0.54655 seconds. 
  Full-batch training loss = 0.001750, test loss = 0.206290
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 55/100. Took 0.54811 seconds. 
  Full-batch training loss = 0.001644, test loss = 0.206898
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 56/100. Took 0.54742 seconds. 
  Full-batch training loss = 0.001559, test loss = 0.207416
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 57/100. Took 0.54723 seconds. 
  Full-batch training loss = 0.001513, test loss = 0.208271
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 58/100. Took 0.54911 seconds. 
  Full-batch training loss = 0.001475, test loss = 0.209273
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 59/100. Took 0.54603 seconds. 
  Full-batch training loss = 0.001436, test loss = 0.209669
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 60/100. Took 0.54496 seconds. 
  Full-batch training loss = 0.001365, test loss = 0.209886
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 61/100. Took 0.54957 seconds. 
  Full-batch training loss = 0.001295, test loss = 0.210057
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 62/100. Took 0.54779 seconds. 
  Full-batch training loss = 0.001249, test loss = 0.210863
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 63/100. Took 0.54518 seconds. 
  Full-batch training loss = 0.001247, test loss = 0.212539
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 64/100. Took 0.5466 seconds. 
  Full-batch training loss = 0.001186, test loss = 0.212528
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 65/100. Took 0.54648 seconds. 
  Full-batch training loss = 0.001149, test loss = 0.212256
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 66/100. Took 0.54942 seconds. 
  Full-batch training loss = 0.001111, test loss = 0.213449
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 67/100. Took 0.54598 seconds. 
  Full-batch training loss = 0.001100, test loss = 0.214910
  Training set accuracy = 1.000000, Test set accuracy = 0.956000
 epoch 68/100. Took 0.54652 seconds. 
  Full-batch training loss = 0.001077, test loss = 0.214663
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 69/100. Took 0.54503 seconds. 
  Full-batch training loss = 0.001040, test loss = 0.214964
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 70/100. Took 0.54393 seconds. 
  Full-batch training loss = 0.001023, test loss = 0.215580
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 71/100. Took 0.54474 seconds. 
  Full-batch training loss = 0.000974, test loss = 0.215357
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 72/100. Took 0.54805 seconds. 
  Full-batch training loss = 0.000953, test loss = 0.216105
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 73/100. Took 0.54555 seconds. 
  Full-batch training loss = 0.000940, test loss = 0.216709
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 74/100. Took 0.54532 seconds. 
  Full-batch training loss = 0.000906, test loss = 0.216850
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 75/100. Took 0.54675 seconds. 
  Full-batch training loss = 0.000878, test loss = 0.216991
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 76/100. Took 0.54575 seconds. 
  Full-batch training loss = 0.000871, test loss = 0.217785
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 77/100. Took 0.54907 seconds. 
  Full-batch training loss = 0.000859, test loss = 0.218325
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 78/100. Took 0.55047 seconds. 
  Full-batch training loss = 0.000823, test loss = 0.218412
  Training set accuracy = 1.000000, Test set accuracy = 0.956100
 epoch 79/100. Took 0.55004 seconds. 
  Full-batch training loss = 0.000811, test loss = 0.218759
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 80/100. Took 0.54782 seconds. 
  Full-batch training loss = 0.000790, test loss = 0.219037
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 81/100. Took 0.55018 seconds. 
  Full-batch training loss = 0.000782, test loss = 0.219762
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 82/100. Took 0.54799 seconds. 
  Full-batch training loss = 0.000759, test loss = 0.219736
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 83/100. Took 0.5479 seconds. 
  Full-batch training loss = 0.000743, test loss = 0.220107
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 84/100. Took 0.54567 seconds. 
  Full-batch training loss = 0.000723, test loss = 0.220380
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 85/100. Took 0.5479 seconds. 
  Full-batch training loss = 0.000710, test loss = 0.220795
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 86/100. Took 0.55004 seconds. 
  Full-batch training loss = 0.000700, test loss = 0.221007
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 87/100. Took 0.54601 seconds. 
  Full-batch training loss = 0.000688, test loss = 0.221904
  Training set accuracy = 1.000000, Test set accuracy = 0.956000
 epoch 88/100. Took 0.54513 seconds. 
  Full-batch training loss = 0.000674, test loss = 0.222015
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 89/100. Took 0.54396 seconds. 
  Full-batch training loss = 0.000665, test loss = 0.222551
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 90/100. Took 0.5487 seconds. 
  Full-batch training loss = 0.000649, test loss = 0.222245
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 91/100. Took 0.54477 seconds. 
  Full-batch training loss = 0.000635, test loss = 0.222858
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 92/100. Took 0.54919 seconds. 
  Full-batch training loss = 0.000625, test loss = 0.223159
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 93/100. Took 0.54637 seconds. 
  Full-batch training loss = 0.000625, test loss = 0.223341
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 94/100. Took 0.54562 seconds. 
  Full-batch training loss = 0.000600, test loss = 0.223541
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 95/100. Took 0.54711 seconds. 
  Full-batch training loss = 0.000595, test loss = 0.223742
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 96/100. Took 0.54587 seconds. 
  Full-batch training loss = 0.000583, test loss = 0.224246
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 97/100. Took 0.54718 seconds. 
  Full-batch training loss = 0.000575, test loss = 0.224587
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 98/100. Took 0.54737 seconds. 
  Full-batch training loss = 0.000561, test loss = 0.224813
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 99/100. Took 0.5481 seconds. 
  Full-batch training loss = 0.000554, test loss = 0.225106
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 100/100. Took 0.54572 seconds. 
  Full-batch training loss = 0.000545, test loss = 0.225464
  Training set accuracy = 1.000000, Test set accuracy = 0.956800
Elapsed time is 110.754853 seconds.
End Training
