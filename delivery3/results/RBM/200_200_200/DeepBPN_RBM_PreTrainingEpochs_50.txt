Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	50
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.5439 seconds. Average reconstruction error is: 29.8838
 epoch 2/50. Took 0.54974 seconds. Average reconstruction error is: 17.9418
 epoch 3/50. Took 0.54594 seconds. Average reconstruction error is: 15.5775
 epoch 4/50. Took 0.54516 seconds. Average reconstruction error is: 14.0928
 epoch 5/50. Took 0.54674 seconds. Average reconstruction error is: 13.0288
 epoch 6/50. Took 0.54535 seconds. Average reconstruction error is: 12.2859
 epoch 7/50. Took 0.54938 seconds. Average reconstruction error is: 11.7606
 epoch 8/50. Took 0.54966 seconds. Average reconstruction error is: 11.3469
 epoch 9/50. Took 0.5457 seconds. Average reconstruction error is: 11.0207
 epoch 10/50. Took 0.5454 seconds. Average reconstruction error is: 10.7678
 epoch 11/50. Took 0.54669 seconds. Average reconstruction error is: 10.536
 epoch 12/50. Took 0.54581 seconds. Average reconstruction error is: 10.305
 epoch 13/50. Took 0.54585 seconds. Average reconstruction error is: 10.2026
 epoch 14/50. Took 0.545 seconds. Average reconstruction error is: 10.0173
 epoch 15/50. Took 0.54501 seconds. Average reconstruction error is: 9.9005
 epoch 16/50. Took 0.54647 seconds. Average reconstruction error is: 9.7923
 epoch 17/50. Took 0.54732 seconds. Average reconstruction error is: 9.7132
 epoch 18/50. Took 0.55173 seconds. Average reconstruction error is: 9.6328
 epoch 19/50. Took 0.54926 seconds. Average reconstruction error is: 9.5882
 epoch 20/50. Took 0.54858 seconds. Average reconstruction error is: 9.4364
 epoch 21/50. Took 0.54813 seconds. Average reconstruction error is: 9.3745
 epoch 22/50. Took 0.54748 seconds. Average reconstruction error is: 9.3642
 epoch 23/50. Took 0.54561 seconds. Average reconstruction error is: 9.276
 epoch 24/50. Took 0.54473 seconds. Average reconstruction error is: 9.2235
 epoch 25/50. Took 0.54976 seconds. Average reconstruction error is: 9.1727
 epoch 26/50. Took 0.54593 seconds. Average reconstruction error is: 9.1243
 epoch 27/50. Took 0.54548 seconds. Average reconstruction error is: 9.108
 epoch 28/50. Took 0.54523 seconds. Average reconstruction error is: 8.9953
 epoch 29/50. Took 0.54693 seconds. Average reconstruction error is: 8.9788
 epoch 30/50. Took 0.54678 seconds. Average reconstruction error is: 8.9546
 epoch 31/50. Took 0.5452 seconds. Average reconstruction error is: 8.9168
 epoch 32/50. Took 0.54905 seconds. Average reconstruction error is: 8.9147
 epoch 33/50. Took 0.54729 seconds. Average reconstruction error is: 8.8077
 epoch 34/50. Took 0.54625 seconds. Average reconstruction error is: 8.8233
 epoch 35/50. Took 0.54464 seconds. Average reconstruction error is: 8.7587
 epoch 36/50. Took 0.54305 seconds. Average reconstruction error is: 8.7525
 epoch 37/50. Took 0.54754 seconds. Average reconstruction error is: 8.6894
 epoch 38/50. Took 0.54786 seconds. Average reconstruction error is: 8.6542
 epoch 39/50. Took 0.54796 seconds. Average reconstruction error is: 8.6358
 epoch 40/50. Took 0.54607 seconds. Average reconstruction error is: 8.67
 epoch 41/50. Took 0.54321 seconds. Average reconstruction error is: 8.5981
 epoch 42/50. Took 0.5476 seconds. Average reconstruction error is: 8.6022
 epoch 43/50. Took 0.54033 seconds. Average reconstruction error is: 8.5643
 epoch 44/50. Took 0.54293 seconds. Average reconstruction error is: 8.5372
 epoch 45/50. Took 0.55003 seconds. Average reconstruction error is: 8.5044
 epoch 46/50. Took 0.54667 seconds. Average reconstruction error is: 8.4983
 epoch 47/50. Took 0.54848 seconds. Average reconstruction error is: 8.4714
 epoch 48/50. Took 0.54951 seconds. Average reconstruction error is: 8.4569
 epoch 49/50. Took 0.5492 seconds. Average reconstruction error is: 8.4564
 epoch 50/50. Took 0.54942 seconds. Average reconstruction error is: 8.3964
Training binary-binary RBM in layer 2 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.19015 seconds. Average reconstruction error is: 15.2585
 epoch 2/50. Took 0.19271 seconds. Average reconstruction error is: 9.2661
 epoch 3/50. Took 0.1916 seconds. Average reconstruction error is: 8.181
 epoch 4/50. Took 0.19178 seconds. Average reconstruction error is: 7.6532
 epoch 5/50. Took 0.19179 seconds. Average reconstruction error is: 7.2683
 epoch 6/50. Took 0.1917 seconds. Average reconstruction error is: 7.0437
 epoch 7/50. Took 0.19323 seconds. Average reconstruction error is: 6.8158
 epoch 8/50. Took 0.19134 seconds. Average reconstruction error is: 6.6776
 epoch 9/50. Took 0.19128 seconds. Average reconstruction error is: 6.4931
 epoch 10/50. Took 0.19245 seconds. Average reconstruction error is: 6.388
 epoch 11/50. Took 0.19215 seconds. Average reconstruction error is: 6.3126
 epoch 12/50. Took 0.19328 seconds. Average reconstruction error is: 6.1891
 epoch 13/50. Took 0.1926 seconds. Average reconstruction error is: 6.0912
 epoch 14/50. Took 0.19076 seconds. Average reconstruction error is: 6.0048
 epoch 15/50. Took 0.19334 seconds. Average reconstruction error is: 5.9246
 epoch 16/50. Took 0.19337 seconds. Average reconstruction error is: 5.863
 epoch 17/50. Took 0.19355 seconds. Average reconstruction error is: 5.7952
 epoch 18/50. Took 0.19257 seconds. Average reconstruction error is: 5.7325
 epoch 19/50. Took 0.19281 seconds. Average reconstruction error is: 5.6715
 epoch 20/50. Took 0.19457 seconds. Average reconstruction error is: 5.6309
 epoch 21/50. Took 0.19195 seconds. Average reconstruction error is: 5.5762
 epoch 22/50. Took 0.19286 seconds. Average reconstruction error is: 5.5051
 epoch 23/50. Took 0.19248 seconds. Average reconstruction error is: 5.4608
 epoch 24/50. Took 0.19291 seconds. Average reconstruction error is: 5.4345
 epoch 25/50. Took 0.19366 seconds. Average reconstruction error is: 5.3886
 epoch 26/50. Took 0.19176 seconds. Average reconstruction error is: 5.3654
 epoch 27/50. Took 0.19327 seconds. Average reconstruction error is: 5.3256
 epoch 28/50. Took 0.19355 seconds. Average reconstruction error is: 5.2884
 epoch 29/50. Took 0.19404 seconds. Average reconstruction error is: 5.2497
 epoch 30/50. Took 0.19311 seconds. Average reconstruction error is: 5.2149
 epoch 31/50. Took 0.19247 seconds. Average reconstruction error is: 5.2031
 epoch 32/50. Took 0.1922 seconds. Average reconstruction error is: 5.1709
 epoch 33/50. Took 0.19391 seconds. Average reconstruction error is: 5.1432
 epoch 34/50. Took 0.19331 seconds. Average reconstruction error is: 5.1202
 epoch 35/50. Took 0.1932 seconds. Average reconstruction error is: 5.0882
 epoch 36/50. Took 0.19244 seconds. Average reconstruction error is: 5.0855
 epoch 37/50. Took 0.19191 seconds. Average reconstruction error is: 5.0279
 epoch 38/50. Took 0.19333 seconds. Average reconstruction error is: 5.0272
 epoch 39/50. Took 0.1936 seconds. Average reconstruction error is: 4.9764
 epoch 40/50. Took 0.19331 seconds. Average reconstruction error is: 4.9867
 epoch 41/50. Took 0.19343 seconds. Average reconstruction error is: 4.9473
 epoch 42/50. Took 0.19304 seconds. Average reconstruction error is: 4.9171
 epoch 43/50. Took 0.19189 seconds. Average reconstruction error is: 4.9115
 epoch 44/50. Took 0.1938 seconds. Average reconstruction error is: 4.8943
 epoch 45/50. Took 0.19271 seconds. Average reconstruction error is: 4.8839
 epoch 46/50. Took 0.19284 seconds. Average reconstruction error is: 4.8564
 epoch 47/50. Took 0.19436 seconds. Average reconstruction error is: 4.8463
 epoch 48/50. Took 0.19375 seconds. Average reconstruction error is: 4.8243
 epoch 49/50. Took 0.19264 seconds. Average reconstruction error is: 4.8194
 epoch 50/50. Took 0.19414 seconds. Average reconstruction error is: 4.7896
Training binary-binary RBM in layer 3 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.19043 seconds. Average reconstruction error is: 13.1406
 epoch 2/50. Took 0.19193 seconds. Average reconstruction error is: 7.6899
 epoch 3/50. Took 0.19302 seconds. Average reconstruction error is: 6.7383
 epoch 4/50. Took 0.19231 seconds. Average reconstruction error is: 6.3177
 epoch 5/50. Took 0.19206 seconds. Average reconstruction error is: 6.0524
 epoch 6/50. Took 0.19208 seconds. Average reconstruction error is: 5.8395
 epoch 7/50. Took 0.19068 seconds. Average reconstruction error is: 5.673
 epoch 8/50. Took 0.1926 seconds. Average reconstruction error is: 5.5524
 epoch 9/50. Took 0.19193 seconds. Average reconstruction error is: 5.4351
 epoch 10/50. Took 0.19114 seconds. Average reconstruction error is: 5.3577
 epoch 11/50. Took 0.19285 seconds. Average reconstruction error is: 5.3004
 epoch 12/50. Took 0.19151 seconds. Average reconstruction error is: 5.2235
 epoch 13/50. Took 0.19209 seconds. Average reconstruction error is: 5.1563
 epoch 14/50. Took 0.19274 seconds. Average reconstruction error is: 5.0954
 epoch 15/50. Took 0.19247 seconds. Average reconstruction error is: 5.0727
 epoch 16/50. Took 0.19068 seconds. Average reconstruction error is: 4.9886
 epoch 17/50. Took 0.19402 seconds. Average reconstruction error is: 4.97
 epoch 18/50. Took 0.19266 seconds. Average reconstruction error is: 4.9253
 epoch 19/50. Took 0.19237 seconds. Average reconstruction error is: 4.8574
 epoch 20/50. Took 0.19289 seconds. Average reconstruction error is: 4.8548
 epoch 21/50. Took 0.19302 seconds. Average reconstruction error is: 4.7804
 epoch 22/50. Took 0.19364 seconds. Average reconstruction error is: 4.7454
 epoch 23/50. Took 0.19177 seconds. Average reconstruction error is: 4.7403
 epoch 24/50. Took 0.19165 seconds. Average reconstruction error is: 4.6771
 epoch 25/50. Took 0.19207 seconds. Average reconstruction error is: 4.6738
 epoch 26/50. Took 0.19222 seconds. Average reconstruction error is: 4.6337
 epoch 27/50. Took 0.19198 seconds. Average reconstruction error is: 4.6069
 epoch 28/50. Took 0.19297 seconds. Average reconstruction error is: 4.5616
 epoch 29/50. Took 0.19348 seconds. Average reconstruction error is: 4.5371
 epoch 30/50. Took 0.19103 seconds. Average reconstruction error is: 4.4642
 epoch 31/50. Took 0.1926 seconds. Average reconstruction error is: 4.4653
 epoch 32/50. Took 0.19215 seconds. Average reconstruction error is: 4.4475
 epoch 33/50. Took 0.19262 seconds. Average reconstruction error is: 4.4207
 epoch 34/50. Took 0.19279 seconds. Average reconstruction error is: 4.3559
 epoch 35/50. Took 0.19199 seconds. Average reconstruction error is: 4.3672
 epoch 36/50. Took 0.19209 seconds. Average reconstruction error is: 4.3421
 epoch 37/50. Took 0.19289 seconds. Average reconstruction error is: 4.304
 epoch 38/50. Took 0.19253 seconds. Average reconstruction error is: 4.2716
 epoch 39/50. Took 0.19256 seconds. Average reconstruction error is: 4.2508
 epoch 40/50. Took 0.19324 seconds. Average reconstruction error is: 4.2014
 epoch 41/50. Took 0.19194 seconds. Average reconstruction error is: 4.2204
 epoch 42/50. Took 0.1931 seconds. Average reconstruction error is: 4.1697
 epoch 43/50. Took 0.19248 seconds. Average reconstruction error is: 4.144
 epoch 44/50. Took 0.19335 seconds. Average reconstruction error is: 4.1476
 epoch 45/50. Took 0.19242 seconds. Average reconstruction error is: 4.1299
 epoch 46/50. Took 0.19078 seconds. Average reconstruction error is: 4.1155
 epoch 47/50. Took 0.19166 seconds. Average reconstruction error is: 4.0916
 epoch 48/50. Took 0.19234 seconds. Average reconstruction error is: 4.0676
 epoch 49/50. Took 0.19084 seconds. Average reconstruction error is: 4.0946
 epoch 50/50. Took 0.19089 seconds. Average reconstruction error is: 4.0551
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.55408 seconds. 
  Full-batch training loss = 0.297992, test loss = 0.288854
  Training set accuracy = 0.918000, Test set accuracy = 0.919800
 epoch 2/100. Took 0.54731 seconds. 
  Full-batch training loss = 0.234297, test loss = 0.245877
  Training set accuracy = 0.931300, Test set accuracy = 0.930600
 epoch 3/100. Took 0.54776 seconds. 
  Full-batch training loss = 0.193747, test loss = 0.218388
  Training set accuracy = 0.945100, Test set accuracy = 0.935600
 epoch 4/100. Took 0.54588 seconds. 
  Full-batch training loss = 0.162810, test loss = 0.199891
  Training set accuracy = 0.955700, Test set accuracy = 0.941600
 epoch 5/100. Took 0.5491 seconds. 
  Full-batch training loss = 0.151608, test loss = 0.195515
  Training set accuracy = 0.957900, Test set accuracy = 0.943800
 epoch 6/100. Took 0.54636 seconds. 
  Full-batch training loss = 0.128611, test loss = 0.186146
  Training set accuracy = 0.965200, Test set accuracy = 0.945700
 epoch 7/100. Took 0.54552 seconds. 
  Full-batch training loss = 0.116721, test loss = 0.181877
  Training set accuracy = 0.970000, Test set accuracy = 0.945400
 epoch 8/100. Took 0.54548 seconds. 
  Full-batch training loss = 0.101791, test loss = 0.177729
  Training set accuracy = 0.975300, Test set accuracy = 0.946800
 epoch 9/100. Took 0.54767 seconds. 
  Full-batch training loss = 0.094995, test loss = 0.174017
  Training set accuracy = 0.976600, Test set accuracy = 0.948700
 epoch 10/100. Took 0.54805 seconds. 
  Full-batch training loss = 0.083290, test loss = 0.168410
  Training set accuracy = 0.982000, Test set accuracy = 0.947900
 epoch 11/100. Took 0.54818 seconds. 
  Full-batch training loss = 0.074547, test loss = 0.165477
  Training set accuracy = 0.984900, Test set accuracy = 0.950200
 epoch 12/100. Took 0.54709 seconds. 
  Full-batch training loss = 0.066612, test loss = 0.164003
  Training set accuracy = 0.987400, Test set accuracy = 0.950800
 epoch 13/100. Took 0.54904 seconds. 
  Full-batch training loss = 0.058983, test loss = 0.159732
  Training set accuracy = 0.989000, Test set accuracy = 0.953100
 epoch 14/100. Took 0.55186 seconds. 
  Full-batch training loss = 0.053781, test loss = 0.159662
  Training set accuracy = 0.990900, Test set accuracy = 0.952600
 epoch 15/100. Took 0.54929 seconds. 
  Full-batch training loss = 0.050365, test loss = 0.160517
  Training set accuracy = 0.991500, Test set accuracy = 0.952800
 epoch 16/100. Took 0.54651 seconds. 
  Full-batch training loss = 0.044780, test loss = 0.156606
  Training set accuracy = 0.992700, Test set accuracy = 0.954400
 epoch 17/100. Took 0.54678 seconds. 
  Full-batch training loss = 0.040646, test loss = 0.157821
  Training set accuracy = 0.994200, Test set accuracy = 0.954200
 epoch 18/100. Took 0.54612 seconds. 
  Full-batch training loss = 0.036919, test loss = 0.155623
  Training set accuracy = 0.994900, Test set accuracy = 0.955300
 epoch 19/100. Took 0.54906 seconds. 
  Full-batch training loss = 0.034127, test loss = 0.156398
  Training set accuracy = 0.995300, Test set accuracy = 0.954400
 epoch 20/100. Took 0.548 seconds. 
  Full-batch training loss = 0.031379, test loss = 0.156494
  Training set accuracy = 0.995600, Test set accuracy = 0.954900
 epoch 21/100. Took 0.54663 seconds. 
  Full-batch training loss = 0.029263, test loss = 0.156635
  Training set accuracy = 0.996200, Test set accuracy = 0.954200
 epoch 22/100. Took 0.5489 seconds. 
  Full-batch training loss = 0.026635, test loss = 0.156045
  Training set accuracy = 0.996900, Test set accuracy = 0.955500
 epoch 23/100. Took 0.54611 seconds. 
  Full-batch training loss = 0.024585, test loss = 0.155160
  Training set accuracy = 0.997300, Test set accuracy = 0.955500
 epoch 24/100. Took 0.54836 seconds. 
  Full-batch training loss = 0.022338, test loss = 0.155358
  Training set accuracy = 0.997600, Test set accuracy = 0.955600
 epoch 25/100. Took 0.54871 seconds. 
  Full-batch training loss = 0.021581, test loss = 0.157746
  Training set accuracy = 0.997600, Test set accuracy = 0.955600
 epoch 26/100. Took 0.5468 seconds. 
  Full-batch training loss = 0.019652, test loss = 0.155441
  Training set accuracy = 0.998100, Test set accuracy = 0.956600
 epoch 27/100. Took 0.54776 seconds. 
  Full-batch training loss = 0.018153, test loss = 0.156082
  Training set accuracy = 0.998800, Test set accuracy = 0.955500
 epoch 28/100. Took 0.55331 seconds. 
  Full-batch training loss = 0.017104, test loss = 0.155269
  Training set accuracy = 0.998700, Test set accuracy = 0.956700
 epoch 29/100. Took 0.54777 seconds. 
  Full-batch training loss = 0.015798, test loss = 0.155104
  Training set accuracy = 0.999300, Test set accuracy = 0.956500
 epoch 30/100. Took 0.55105 seconds. 
  Full-batch training loss = 0.014697, test loss = 0.154930
  Training set accuracy = 0.999400, Test set accuracy = 0.957400
 epoch 31/100. Took 0.54772 seconds. 
  Full-batch training loss = 0.013944, test loss = 0.156034
  Training set accuracy = 0.999500, Test set accuracy = 0.957100
 epoch 32/100. Took 0.54907 seconds. 
  Full-batch training loss = 0.013070, test loss = 0.155511
  Training set accuracy = 0.999600, Test set accuracy = 0.957300
 epoch 33/100. Took 0.54501 seconds. 
  Full-batch training loss = 0.012511, test loss = 0.154945
  Training set accuracy = 0.999700, Test set accuracy = 0.957600
 epoch 34/100. Took 0.54739 seconds. 
  Full-batch training loss = 0.011639, test loss = 0.155535
  Training set accuracy = 0.999700, Test set accuracy = 0.957800
 epoch 35/100. Took 0.5485 seconds. 
  Full-batch training loss = 0.011191, test loss = 0.155675
  Training set accuracy = 0.999700, Test set accuracy = 0.958400
 epoch 36/100. Took 0.54587 seconds. 
  Full-batch training loss = 0.010487, test loss = 0.156267
  Training set accuracy = 0.999700, Test set accuracy = 0.957800
 epoch 37/100. Took 0.54863 seconds. 
  Full-batch training loss = 0.009846, test loss = 0.156209
  Training set accuracy = 0.999800, Test set accuracy = 0.958400
 epoch 38/100. Took 0.5488 seconds. 
  Full-batch training loss = 0.009467, test loss = 0.157581
  Training set accuracy = 0.999800, Test set accuracy = 0.957500
 epoch 39/100. Took 0.54754 seconds. 
  Full-batch training loss = 0.008964, test loss = 0.157565
  Training set accuracy = 0.999800, Test set accuracy = 0.957700
 epoch 40/100. Took 0.54855 seconds. 
  Full-batch training loss = 0.008661, test loss = 0.157309
  Training set accuracy = 0.999800, Test set accuracy = 0.958600
 epoch 41/100. Took 0.5462 seconds. 
  Full-batch training loss = 0.008148, test loss = 0.157669
  Training set accuracy = 0.999800, Test set accuracy = 0.958100
 epoch 42/100. Took 0.54822 seconds. 
  Full-batch training loss = 0.007800, test loss = 0.158108
  Training set accuracy = 0.999900, Test set accuracy = 0.957800
 epoch 43/100. Took 0.54515 seconds. 
  Full-batch training loss = 0.007530, test loss = 0.158612
  Training set accuracy = 0.999900, Test set accuracy = 0.957800
 epoch 44/100. Took 0.5479 seconds. 
  Full-batch training loss = 0.007250, test loss = 0.159799
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 45/100. Took 0.5494 seconds. 
  Full-batch training loss = 0.006888, test loss = 0.158640
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 46/100. Took 0.54922 seconds. 
  Full-batch training loss = 0.006598, test loss = 0.159283
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 47/100. Took 0.55147 seconds. 
  Full-batch training loss = 0.006378, test loss = 0.159719
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 48/100. Took 0.54661 seconds. 
  Full-batch training loss = 0.006137, test loss = 0.159784
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 49/100. Took 0.55521 seconds. 
  Full-batch training loss = 0.005933, test loss = 0.160241
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 50/100. Took 0.55054 seconds. 
  Full-batch training loss = 0.005696, test loss = 0.159940
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 51/100. Took 0.5488 seconds. 
  Full-batch training loss = 0.005515, test loss = 0.160318
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 52/100. Took 0.5483 seconds. 
  Full-batch training loss = 0.005353, test loss = 0.160289
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 53/100. Took 0.55191 seconds. 
  Full-batch training loss = 0.005205, test loss = 0.161436
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 54/100. Took 0.54667 seconds. 
  Full-batch training loss = 0.005025, test loss = 0.160758
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 55/100. Took 0.54724 seconds. 
  Full-batch training loss = 0.004864, test loss = 0.160979
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 56/100. Took 0.54607 seconds. 
  Full-batch training loss = 0.004726, test loss = 0.161829
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 57/100. Took 0.55015 seconds. 
  Full-batch training loss = 0.004576, test loss = 0.161759
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 58/100. Took 0.54692 seconds. 
  Full-batch training loss = 0.004469, test loss = 0.161439
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 59/100. Took 0.54813 seconds. 
  Full-batch training loss = 0.004330, test loss = 0.162038
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 60/100. Took 0.54815 seconds. 
  Full-batch training loss = 0.004221, test loss = 0.162386
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 61/100. Took 0.54846 seconds. 
  Full-batch training loss = 0.004094, test loss = 0.162540
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 62/100. Took 0.55037 seconds. 
  Full-batch training loss = 0.004019, test loss = 0.163089
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 63/100. Took 0.54683 seconds. 
  Full-batch training loss = 0.003886, test loss = 0.162876
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 64/100. Took 0.54523 seconds. 
  Full-batch training loss = 0.003804, test loss = 0.163200
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 65/100. Took 0.54806 seconds. 
  Full-batch training loss = 0.003698, test loss = 0.163592
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 66/100. Took 0.54732 seconds. 
  Full-batch training loss = 0.003620, test loss = 0.163793
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 67/100. Took 0.55201 seconds. 
  Full-batch training loss = 0.003523, test loss = 0.163816
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 68/100. Took 0.54809 seconds. 
  Full-batch training loss = 0.003446, test loss = 0.164032
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 69/100. Took 0.54863 seconds. 
  Full-batch training loss = 0.003378, test loss = 0.164542
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 70/100. Took 0.5468 seconds. 
  Full-batch training loss = 0.003293, test loss = 0.164124
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 71/100. Took 0.54909 seconds. 
  Full-batch training loss = 0.003226, test loss = 0.164746
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 72/100. Took 0.54674 seconds. 
  Full-batch training loss = 0.003157, test loss = 0.164444
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 73/100. Took 0.54692 seconds. 
  Full-batch training loss = 0.003089, test loss = 0.164421
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 74/100. Took 0.54777 seconds. 
  Full-batch training loss = 0.003024, test loss = 0.164888
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 75/100. Took 0.55092 seconds. 
  Full-batch training loss = 0.002961, test loss = 0.165097
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 76/100. Took 0.54669 seconds. 
  Full-batch training loss = 0.002898, test loss = 0.165324
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 77/100. Took 0.54676 seconds. 
  Full-batch training loss = 0.002842, test loss = 0.165346
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 78/100. Took 0.54733 seconds. 
  Full-batch training loss = 0.002802, test loss = 0.165838
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 79/100. Took 0.54802 seconds. 
  Full-batch training loss = 0.002735, test loss = 0.165688
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 80/100. Took 0.54827 seconds. 
  Full-batch training loss = 0.002686, test loss = 0.166244
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 81/100. Took 0.54965 seconds. 
  Full-batch training loss = 0.002634, test loss = 0.166049
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 82/100. Took 0.54933 seconds. 
  Full-batch training loss = 0.002584, test loss = 0.166253
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 83/100. Took 0.54647 seconds. 
  Full-batch training loss = 0.002538, test loss = 0.166457
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 84/100. Took 0.56355 seconds. 
  Full-batch training loss = 0.002494, test loss = 0.166664
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 85/100. Took 0.54464 seconds. 
  Full-batch training loss = 0.002450, test loss = 0.166676
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 86/100. Took 0.54524 seconds. 
  Full-batch training loss = 0.002410, test loss = 0.166967
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 87/100. Took 0.54539 seconds. 
  Full-batch training loss = 0.002371, test loss = 0.167169
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 88/100. Took 0.5478 seconds. 
  Full-batch training loss = 0.002328, test loss = 0.167167
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 89/100. Took 0.54906 seconds. 
  Full-batch training loss = 0.002290, test loss = 0.167531
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 90/100. Took 0.55014 seconds. 
  Full-batch training loss = 0.002255, test loss = 0.167736
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 91/100. Took 0.54777 seconds. 
  Full-batch training loss = 0.002222, test loss = 0.167316
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 92/100. Took 0.54532 seconds. 
  Full-batch training loss = 0.002181, test loss = 0.167813
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 93/100. Took 0.54745 seconds. 
  Full-batch training loss = 0.002150, test loss = 0.167962
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 94/100. Took 0.54772 seconds. 
  Full-batch training loss = 0.002114, test loss = 0.168038
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 95/100. Took 0.54502 seconds. 
  Full-batch training loss = 0.002082, test loss = 0.168427
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 96/100. Took 0.54729 seconds. 
  Full-batch training loss = 0.002051, test loss = 0.168353
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 97/100. Took 0.54725 seconds. 
  Full-batch training loss = 0.002023, test loss = 0.168583
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 98/100. Took 0.54742 seconds. 
  Full-batch training loss = 0.001990, test loss = 0.168753
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 99/100. Took 0.55005 seconds. 
  Full-batch training loss = 0.001962, test loss = 0.168929
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 100/100. Took 0.54662 seconds. 
  Full-batch training loss = 0.001934, test loss = 0.168990
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
Elapsed time is 158.409239 seconds.
End Training
