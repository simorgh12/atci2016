Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.10
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (50  50)
Training binary-binary RBM in layer 1 (784   50) with CD1 for 20 epochs
 epoch 1/20. Took 0.67758 seconds. Average reconstruction error is: 39.0963
 epoch 2/20. Took 0.67061 seconds. Average reconstruction error is: 25.4595
 epoch 3/20. Took 0.68258 seconds. Average reconstruction error is: 23.4028
 epoch 4/20. Took 0.6469 seconds. Average reconstruction error is: 22.3266
 epoch 5/20. Took 0.69141 seconds. Average reconstruction error is: 21.6993
 epoch 6/20. Took 0.66869 seconds. Average reconstruction error is: 21.2722
 epoch 7/20. Took 0.65418 seconds. Average reconstruction error is: 21.0234
 epoch 8/20. Took 0.65925 seconds. Average reconstruction error is: 20.8343
 epoch 9/20. Took 0.67226 seconds. Average reconstruction error is: 20.6335
 epoch 10/20. Took 0.65738 seconds. Average reconstruction error is: 20.5045
 epoch 11/20. Took 0.6698 seconds. Average reconstruction error is: 20.4108
 epoch 12/20. Took 0.65805 seconds. Average reconstruction error is: 20.3147
 epoch 13/20. Took 0.67217 seconds. Average reconstruction error is: 20.228
 epoch 14/20. Took 0.66657 seconds. Average reconstruction error is: 20.1644
 epoch 15/20. Took 0.6745 seconds. Average reconstruction error is: 20.085
 epoch 16/20. Took 0.65726 seconds. Average reconstruction error is: 20.0101
 epoch 17/20. Took 0.66388 seconds. Average reconstruction error is: 19.9353
 epoch 18/20. Took 0.70299 seconds. Average reconstruction error is: 19.8826
 epoch 19/20. Took 0.68815 seconds. Average reconstruction error is: 19.8206
 epoch 20/20. Took 0.72075 seconds. Average reconstruction error is: 19.7632
Training binary-binary RBM in layer 2 (50  50) with CD1 for 20 epochs
 epoch 1/20. Took 0.11964 seconds. Average reconstruction error is: 5.4727
 epoch 2/20. Took 0.096927 seconds. Average reconstruction error is: 3.3284
 epoch 3/20. Took 0.10059 seconds. Average reconstruction error is: 2.7876
 epoch 4/20. Took 0.09702 seconds. Average reconstruction error is: 2.5095
 epoch 5/20. Took 0.094338 seconds. Average reconstruction error is: 2.3358
 epoch 6/20. Took 0.10271 seconds. Average reconstruction error is: 2.2256
 epoch 7/20. Took 0.1052 seconds. Average reconstruction error is: 2.1466
 epoch 8/20. Took 0.10634 seconds. Average reconstruction error is: 2.0751
 epoch 9/20. Took 0.094376 seconds. Average reconstruction error is: 2.0102
 epoch 10/20. Took 0.094221 seconds. Average reconstruction error is: 1.9711
 epoch 11/20. Took 0.098553 seconds. Average reconstruction error is: 1.9356
 epoch 12/20. Took 0.094451 seconds. Average reconstruction error is: 1.8934
 epoch 13/20. Took 0.094474 seconds. Average reconstruction error is: 1.8695
 epoch 14/20. Took 0.097602 seconds. Average reconstruction error is: 1.8296
 epoch 15/20. Took 0.094531 seconds. Average reconstruction error is: 1.8131
 epoch 16/20. Took 0.1001 seconds. Average reconstruction error is: 1.7946
 epoch 17/20. Took 0.10471 seconds. Average reconstruction error is: 1.7854
 epoch 18/20. Took 0.10736 seconds. Average reconstruction error is: 1.7645
 epoch 19/20. Took 0.10827 seconds. Average reconstruction error is: 1.7396
 epoch 20/20. Took 0.096808 seconds. Average reconstruction error is: 1.7367
Training NN  (784   50   50   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.45365 seconds. 
  Full-batch training loss = 0.451242, test loss = 0.446295
  Training set accuracy = 0.868100, Test set accuracy = 0.872400
 epoch 2/100. Took 0.42246 seconds. 
  Full-batch training loss = 0.378326, test loss = 0.386851
  Training set accuracy = 0.889400, Test set accuracy = 0.887100
 epoch 3/100. Took 0.40793 seconds. 
  Full-batch training loss = 0.341120, test loss = 0.358850
  Training set accuracy = 0.898400, Test set accuracy = 0.893200
 epoch 4/100. Took 0.41122 seconds. 
  Full-batch training loss = 0.321841, test loss = 0.353417
  Training set accuracy = 0.903300, Test set accuracy = 0.896300
 epoch 5/100. Took 0.41913 seconds. 
  Full-batch training loss = 0.294792, test loss = 0.334997
  Training set accuracy = 0.913300, Test set accuracy = 0.899600
 epoch 6/100. Took 0.43357 seconds. 
  Full-batch training loss = 0.271910, test loss = 0.320001
  Training set accuracy = 0.919500, Test set accuracy = 0.905300
 epoch 7/100. Took 0.43276 seconds. 
  Full-batch training loss = 0.259130, test loss = 0.316255
  Training set accuracy = 0.923900, Test set accuracy = 0.907800
 epoch 8/100. Took 0.43467 seconds. 
  Full-batch training loss = 0.240749, test loss = 0.304383
  Training set accuracy = 0.931600, Test set accuracy = 0.912000
 epoch 9/100. Took 0.43856 seconds. 
  Full-batch training loss = 0.229526, test loss = 0.297834
  Training set accuracy = 0.933500, Test set accuracy = 0.913900
 epoch 10/100. Took 0.41026 seconds. 
  Full-batch training loss = 0.218239, test loss = 0.297013
  Training set accuracy = 0.935900, Test set accuracy = 0.913500
 epoch 11/100. Took 0.45159 seconds. 
  Full-batch training loss = 0.206201, test loss = 0.290648
  Training set accuracy = 0.942400, Test set accuracy = 0.916700
 epoch 12/100. Took 0.43987 seconds. 
  Full-batch training loss = 0.199639, test loss = 0.290979
  Training set accuracy = 0.942400, Test set accuracy = 0.915100
 epoch 13/100. Took 0.39242 seconds. 
  Full-batch training loss = 0.184741, test loss = 0.283245
  Training set accuracy = 0.949600, Test set accuracy = 0.918500
 epoch 14/100. Took 0.47332 seconds. 
  Full-batch training loss = 0.176623, test loss = 0.280327
  Training set accuracy = 0.951500, Test set accuracy = 0.919300
 epoch 15/100. Took 0.44855 seconds. 
  Full-batch training loss = 0.168230, test loss = 0.273990
  Training set accuracy = 0.954600, Test set accuracy = 0.921500
 epoch 16/100. Took 0.41756 seconds. 
  Full-batch training loss = 0.162535, test loss = 0.274271
  Training set accuracy = 0.954300, Test set accuracy = 0.922100
 epoch 17/100. Took 0.42837 seconds. 
  Full-batch training loss = 0.155365, test loss = 0.270600
  Training set accuracy = 0.956900, Test set accuracy = 0.922200
 epoch 18/100. Took 0.40644 seconds. 
  Full-batch training loss = 0.149858, test loss = 0.269953
  Training set accuracy = 0.959200, Test set accuracy = 0.923600
 epoch 19/100. Took 0.42233 seconds. 
  Full-batch training loss = 0.143079, test loss = 0.269077
  Training set accuracy = 0.960600, Test set accuracy = 0.922900
 epoch 20/100. Took 0.46658 seconds. 
  Full-batch training loss = 0.136187, test loss = 0.268947
  Training set accuracy = 0.963200, Test set accuracy = 0.923200
 epoch 21/100. Took 0.42754 seconds. 
  Full-batch training loss = 0.130968, test loss = 0.265255
  Training set accuracy = 0.965100, Test set accuracy = 0.925400
 epoch 22/100. Took 0.3879 seconds. 
  Full-batch training loss = 0.126135, test loss = 0.263290
  Training set accuracy = 0.966600, Test set accuracy = 0.925500
 epoch 23/100. Took 0.47339 seconds. 
  Full-batch training loss = 0.122614, test loss = 0.263807
  Training set accuracy = 0.968000, Test set accuracy = 0.923500
 epoch 24/100. Took 0.43338 seconds. 
  Full-batch training loss = 0.117567, test loss = 0.266350
  Training set accuracy = 0.969800, Test set accuracy = 0.924300
 epoch 25/100. Took 0.46865 seconds. 
  Full-batch training loss = 0.112834, test loss = 0.264052
  Training set accuracy = 0.971000, Test set accuracy = 0.925900
 epoch 26/100. Took 0.49801 seconds. 
  Full-batch training loss = 0.108717, test loss = 0.263981
  Training set accuracy = 0.972200, Test set accuracy = 0.925500
 epoch 27/100. Took 0.40093 seconds. 
  Full-batch training loss = 0.108402, test loss = 0.266589
  Training set accuracy = 0.971900, Test set accuracy = 0.924200
 epoch 28/100. Took 0.4143 seconds. 
  Full-batch training loss = 0.102992, test loss = 0.264236
  Training set accuracy = 0.973800, Test set accuracy = 0.926300
 epoch 29/100. Took 0.41919 seconds. 
  Full-batch training loss = 0.097362, test loss = 0.261844
  Training set accuracy = 0.976500, Test set accuracy = 0.926800
 epoch 30/100. Took 0.4504 seconds. 
  Full-batch training loss = 0.094897, test loss = 0.262970
  Training set accuracy = 0.976700, Test set accuracy = 0.926500
 epoch 31/100. Took 0.49863 seconds. 
  Full-batch training loss = 0.093750, test loss = 0.269348
  Training set accuracy = 0.976800, Test set accuracy = 0.926200
 epoch 32/100. Took 0.46656 seconds. 
  Full-batch training loss = 0.089348, test loss = 0.265425
  Training set accuracy = 0.979400, Test set accuracy = 0.926700
 epoch 33/100. Took 0.48281 seconds. 
  Full-batch training loss = 0.088062, test loss = 0.268153
  Training set accuracy = 0.978800, Test set accuracy = 0.924700
 epoch 34/100. Took 0.48012 seconds. 
  Full-batch training loss = 0.084965, test loss = 0.267827
  Training set accuracy = 0.978300, Test set accuracy = 0.925400
 epoch 35/100. Took 0.40524 seconds. 
  Full-batch training loss = 0.080775, test loss = 0.264891
  Training set accuracy = 0.981400, Test set accuracy = 0.926900
 epoch 36/100. Took 0.43143 seconds. 
  Full-batch training loss = 0.078698, test loss = 0.264630
  Training set accuracy = 0.980900, Test set accuracy = 0.927300
 epoch 37/100. Took 0.4006 seconds. 
  Full-batch training loss = 0.075944, test loss = 0.266506
  Training set accuracy = 0.982300, Test set accuracy = 0.928200
 epoch 38/100. Took 0.41477 seconds. 
  Full-batch training loss = 0.072862, test loss = 0.266276
  Training set accuracy = 0.983100, Test set accuracy = 0.928100
 epoch 39/100. Took 0.3976 seconds. 
  Full-batch training loss = 0.070498, test loss = 0.265409
  Training set accuracy = 0.984100, Test set accuracy = 0.928000
 epoch 40/100. Took 0.40202 seconds. 
  Full-batch training loss = 0.069872, test loss = 0.268239
  Training set accuracy = 0.983700, Test set accuracy = 0.927500
 epoch 41/100. Took 0.45969 seconds. 
  Full-batch training loss = 0.066928, test loss = 0.268633
  Training set accuracy = 0.984700, Test set accuracy = 0.928800
 epoch 42/100. Took 0.45037 seconds. 
  Full-batch training loss = 0.064378, test loss = 0.268621
  Training set accuracy = 0.985300, Test set accuracy = 0.928200
 epoch 43/100. Took 0.39892 seconds. 
  Full-batch training loss = 0.062994, test loss = 0.270053
  Training set accuracy = 0.985400, Test set accuracy = 0.928800
 epoch 44/100. Took 0.45174 seconds. 
  Full-batch training loss = 0.060768, test loss = 0.270645
  Training set accuracy = 0.986400, Test set accuracy = 0.929000
 epoch 45/100. Took 0.40328 seconds. 
  Full-batch training loss = 0.062298, test loss = 0.277732
  Training set accuracy = 0.985500, Test set accuracy = 0.926400
 epoch 46/100. Took 0.45306 seconds. 
  Full-batch training loss = 0.059252, test loss = 0.272743
  Training set accuracy = 0.986900, Test set accuracy = 0.929100
 epoch 47/100. Took 0.39382 seconds. 
  Full-batch training loss = 0.055234, test loss = 0.272001
  Training set accuracy = 0.988100, Test set accuracy = 0.929200
 epoch 48/100. Took 0.39963 seconds. 
  Full-batch training loss = 0.054357, test loss = 0.274974
  Training set accuracy = 0.988500, Test set accuracy = 0.928700
 epoch 49/100. Took 0.43979 seconds. 
  Full-batch training loss = 0.052842, test loss = 0.274532
  Training set accuracy = 0.988700, Test set accuracy = 0.928100
 epoch 50/100. Took 0.41692 seconds. 
  Full-batch training loss = 0.051550, test loss = 0.277526
  Training set accuracy = 0.988600, Test set accuracy = 0.928300
 epoch 51/100. Took 0.40937 seconds. 
  Full-batch training loss = 0.050338, test loss = 0.275494
  Training set accuracy = 0.989700, Test set accuracy = 0.929700
 epoch 52/100. Took 0.43087 seconds. 
  Full-batch training loss = 0.047844, test loss = 0.276247
  Training set accuracy = 0.990200, Test set accuracy = 0.930100
 epoch 53/100. Took 0.48793 seconds. 
  Full-batch training loss = 0.046961, test loss = 0.278578
  Training set accuracy = 0.990700, Test set accuracy = 0.929500
 epoch 54/100. Took 0.4095 seconds. 
  Full-batch training loss = 0.045236, test loss = 0.277528
  Training set accuracy = 0.991500, Test set accuracy = 0.929800
 epoch 55/100. Took 0.39676 seconds. 
  Full-batch training loss = 0.044284, test loss = 0.278559
  Training set accuracy = 0.992100, Test set accuracy = 0.929100
 epoch 56/100. Took 0.42533 seconds. 
  Full-batch training loss = 0.043052, test loss = 0.279729
  Training set accuracy = 0.992200, Test set accuracy = 0.928600
 epoch 57/100. Took 0.4152 seconds. 
  Full-batch training loss = 0.042514, test loss = 0.279208
  Training set accuracy = 0.992200, Test set accuracy = 0.928400
 epoch 58/100. Took 0.40604 seconds. 
  Full-batch training loss = 0.041032, test loss = 0.282340
  Training set accuracy = 0.992700, Test set accuracy = 0.929100
 epoch 59/100. Took 0.40622 seconds. 
  Full-batch training loss = 0.040485, test loss = 0.284774
  Training set accuracy = 0.993400, Test set accuracy = 0.927100
 epoch 60/100. Took 0.39509 seconds. 
  Full-batch training loss = 0.038906, test loss = 0.280979
  Training set accuracy = 0.993100, Test set accuracy = 0.928200
 epoch 61/100. Took 0.42743 seconds. 
  Full-batch training loss = 0.037807, test loss = 0.284462
  Training set accuracy = 0.993800, Test set accuracy = 0.928200
 epoch 62/100. Took 0.43426 seconds. 
  Full-batch training loss = 0.036704, test loss = 0.284504
  Training set accuracy = 0.994200, Test set accuracy = 0.929200
 epoch 63/100. Took 0.44025 seconds. 
  Full-batch training loss = 0.035751, test loss = 0.285792
  Training set accuracy = 0.994800, Test set accuracy = 0.928700
 epoch 64/100. Took 0.48823 seconds. 
  Full-batch training loss = 0.035242, test loss = 0.288449
  Training set accuracy = 0.994500, Test set accuracy = 0.929500
 epoch 65/100. Took 0.41587 seconds. 
  Full-batch training loss = 0.034552, test loss = 0.288006
  Training set accuracy = 0.994800, Test set accuracy = 0.929200
 epoch 66/100. Took 0.42602 seconds. 
  Full-batch training loss = 0.033561, test loss = 0.287920
  Training set accuracy = 0.995300, Test set accuracy = 0.928900
 epoch 67/100. Took 0.45123 seconds. 
  Full-batch training loss = 0.032797, test loss = 0.289345
  Training set accuracy = 0.995300, Test set accuracy = 0.928500
 epoch 68/100. Took 0.44268 seconds. 
  Full-batch training loss = 0.032153, test loss = 0.290358
  Training set accuracy = 0.995400, Test set accuracy = 0.928900
 epoch 69/100. Took 0.42214 seconds. 
  Full-batch training loss = 0.030941, test loss = 0.289976
  Training set accuracy = 0.995900, Test set accuracy = 0.929700
 epoch 70/100. Took 0.44515 seconds. 
  Full-batch training loss = 0.030798, test loss = 0.290810
  Training set accuracy = 0.995600, Test set accuracy = 0.929000
 epoch 71/100. Took 0.39887 seconds. 
  Full-batch training loss = 0.029584, test loss = 0.292614
  Training set accuracy = 0.996500, Test set accuracy = 0.929200
 epoch 72/100. Took 0.43826 seconds. 
  Full-batch training loss = 0.030136, test loss = 0.294420
  Training set accuracy = 0.996100, Test set accuracy = 0.928000
 epoch 73/100. Took 0.39752 seconds. 
  Full-batch training loss = 0.028702, test loss = 0.295942
  Training set accuracy = 0.996400, Test set accuracy = 0.929100
 epoch 74/100. Took 0.41286 seconds. 
  Full-batch training loss = 0.028257, test loss = 0.294469
  Training set accuracy = 0.996400, Test set accuracy = 0.927500
 epoch 75/100. Took 0.42714 seconds. 
  Full-batch training loss = 0.027099, test loss = 0.294918
  Training set accuracy = 0.996600, Test set accuracy = 0.930000
 epoch 76/100. Took 0.41774 seconds. 
  Full-batch training loss = 0.026652, test loss = 0.297095
  Training set accuracy = 0.996900, Test set accuracy = 0.929000
 epoch 77/100. Took 0.43414 seconds. 
  Full-batch training loss = 0.026157, test loss = 0.299209
  Training set accuracy = 0.997000, Test set accuracy = 0.929800
 epoch 78/100. Took 0.42275 seconds. 
  Full-batch training loss = 0.025797, test loss = 0.297850
  Training set accuracy = 0.996900, Test set accuracy = 0.930000
 epoch 79/100. Took 0.42422 seconds. 
  Full-batch training loss = 0.024916, test loss = 0.298710
  Training set accuracy = 0.997200, Test set accuracy = 0.929900
 epoch 80/100. Took 0.39898 seconds. 
  Full-batch training loss = 0.024531, test loss = 0.298962
  Training set accuracy = 0.997300, Test set accuracy = 0.929100
 epoch 81/100. Took 0.45876 seconds. 
  Full-batch training loss = 0.024171, test loss = 0.300195
  Training set accuracy = 0.997200, Test set accuracy = 0.929600
 epoch 82/100. Took 0.44932 seconds. 
  Full-batch training loss = 0.023510, test loss = 0.301484
  Training set accuracy = 0.997600, Test set accuracy = 0.928500
 epoch 83/100. Took 0.41987 seconds. 
  Full-batch training loss = 0.023209, test loss = 0.300993
  Training set accuracy = 0.997800, Test set accuracy = 0.929000
 epoch 84/100. Took 0.4285 seconds. 
  Full-batch training loss = 0.022583, test loss = 0.303083
  Training set accuracy = 0.997700, Test set accuracy = 0.930200
 epoch 85/100. Took 0.40471 seconds. 
  Full-batch training loss = 0.022227, test loss = 0.304274
  Training set accuracy = 0.997700, Test set accuracy = 0.929000
 epoch 86/100. Took 0.42811 seconds. 
  Full-batch training loss = 0.021782, test loss = 0.304603
  Training set accuracy = 0.997600, Test set accuracy = 0.930200
 epoch 87/100. Took 0.43263 seconds. 
  Full-batch training loss = 0.021351, test loss = 0.306636
  Training set accuracy = 0.997700, Test set accuracy = 0.929600
 epoch 88/100. Took 0.39665 seconds. 
  Full-batch training loss = 0.020934, test loss = 0.305643
  Training set accuracy = 0.997800, Test set accuracy = 0.928800
 epoch 89/100. Took 0.41632 seconds. 
  Full-batch training loss = 0.020485, test loss = 0.305389
  Training set accuracy = 0.997900, Test set accuracy = 0.930000
 epoch 90/100. Took 0.45317 seconds. 
  Full-batch training loss = 0.020230, test loss = 0.308879
  Training set accuracy = 0.998000, Test set accuracy = 0.928900
 epoch 91/100. Took 0.42475 seconds. 
  Full-batch training loss = 0.019776, test loss = 0.308793
  Training set accuracy = 0.997900, Test set accuracy = 0.929400
 epoch 92/100. Took 0.40775 seconds. 
  Full-batch training loss = 0.019450, test loss = 0.306868
  Training set accuracy = 0.997900, Test set accuracy = 0.930000
 epoch 93/100. Took 0.43599 seconds. 
  Full-batch training loss = 0.018918, test loss = 0.308796
  Training set accuracy = 0.997800, Test set accuracy = 0.929900
 epoch 94/100. Took 0.40822 seconds. 
  Full-batch training loss = 0.018708, test loss = 0.311701
  Training set accuracy = 0.998200, Test set accuracy = 0.929900
 epoch 95/100. Took 0.43725 seconds. 
  Full-batch training loss = 0.018227, test loss = 0.309884
  Training set accuracy = 0.998100, Test set accuracy = 0.929600
 epoch 96/100. Took 0.42258 seconds. 
  Full-batch training loss = 0.018016, test loss = 0.312495
  Training set accuracy = 0.998000, Test set accuracy = 0.929500
 epoch 97/100. Took 0.42687 seconds. 
  Full-batch training loss = 0.017552, test loss = 0.312111
  Training set accuracy = 0.998100, Test set accuracy = 0.930200
 epoch 98/100. Took 0.43926 seconds. 
  Full-batch training loss = 0.017361, test loss = 0.311743
  Training set accuracy = 0.998100, Test set accuracy = 0.929800
 epoch 99/100. Took 0.4036 seconds. 
  Full-batch training loss = 0.017086, test loss = 0.312695
  Training set accuracy = 0.998100, Test set accuracy = 0.930200
 epoch 100/100. Took 0.40803 seconds. 
  Full-batch training loss = 0.016676, test loss = 0.314395
  Training set accuracy = 0.998000, Test set accuracy = 0.929700
Elapsed time is 106.658875 seconds.
End Training
