Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	100
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (50  50)
Training binary-binary RBM in layer 1 (784   50) with CD1 for 100 epochs
 epoch 1/100. Took 0.66005 seconds. Average reconstruction error is: 39.8059
 epoch 2/100. Took 0.66396 seconds. Average reconstruction error is: 28.014
 epoch 3/100. Took 0.64664 seconds. Average reconstruction error is: 26.097
 epoch 4/100. Took 0.64433 seconds. Average reconstruction error is: 25.2854
 epoch 5/100. Took 0.66727 seconds. Average reconstruction error is: 24.7389
 epoch 6/100. Took 0.66633 seconds. Average reconstruction error is: 24.3349
 epoch 7/100. Took 0.66071 seconds. Average reconstruction error is: 24.153
 epoch 8/100. Took 0.62738 seconds. Average reconstruction error is: 24.0124
 epoch 9/100. Took 0.64608 seconds. Average reconstruction error is: 23.9282
 epoch 10/100. Took 0.6888 seconds. Average reconstruction error is: 23.8072
 epoch 11/100. Took 0.6646 seconds. Average reconstruction error is: 23.5778
 epoch 12/100. Took 0.65539 seconds. Average reconstruction error is: 23.5314
 epoch 13/100. Took 0.66121 seconds. Average reconstruction error is: 23.4884
 epoch 14/100. Took 0.64324 seconds. Average reconstruction error is: 23.4964
 epoch 15/100. Took 0.68712 seconds. Average reconstruction error is: 23.5155
 epoch 16/100. Took 0.68919 seconds. Average reconstruction error is: 23.488
 epoch 17/100. Took 0.65998 seconds. Average reconstruction error is: 23.4683
 epoch 18/100. Took 0.68022 seconds. Average reconstruction error is: 23.2621
 epoch 19/100. Took 0.67113 seconds. Average reconstruction error is: 23.1529
 epoch 20/100. Took 0.66209 seconds. Average reconstruction error is: 23.0643
 epoch 21/100. Took 0.68925 seconds. Average reconstruction error is: 22.9764
 epoch 22/100. Took 0.64126 seconds. Average reconstruction error is: 22.9413
 epoch 23/100. Took 0.66755 seconds. Average reconstruction error is: 22.8633
 epoch 24/100. Took 0.6397 seconds. Average reconstruction error is: 22.7788
 epoch 25/100. Took 0.68557 seconds. Average reconstruction error is: 22.7081
 epoch 26/100. Took 0.68939 seconds. Average reconstruction error is: 22.635
 epoch 27/100. Took 0.62905 seconds. Average reconstruction error is: 22.5897
 epoch 28/100. Took 0.65023 seconds. Average reconstruction error is: 22.5821
 epoch 29/100. Took 0.66215 seconds. Average reconstruction error is: 22.496
 epoch 30/100. Took 0.63991 seconds. Average reconstruction error is: 22.463
 epoch 31/100. Took 0.67491 seconds. Average reconstruction error is: 22.4646
 epoch 32/100. Took 0.69582 seconds. Average reconstruction error is: 22.3967
 epoch 33/100. Took 0.63927 seconds. Average reconstruction error is: 22.3591
 epoch 34/100. Took 0.68695 seconds. Average reconstruction error is: 22.1951
 epoch 35/100. Took 0.67492 seconds. Average reconstruction error is: 22.1672
 epoch 36/100. Took 0.71119 seconds. Average reconstruction error is: 22.068
 epoch 37/100. Took 0.64133 seconds. Average reconstruction error is: 22.0167
 epoch 38/100. Took 0.69692 seconds. Average reconstruction error is: 21.969
 epoch 39/100. Took 0.65664 seconds. Average reconstruction error is: 21.88
 epoch 40/100. Took 0.63092 seconds. Average reconstruction error is: 21.8267
 epoch 41/100. Took 0.64453 seconds. Average reconstruction error is: 21.8091
 epoch 42/100. Took 0.70209 seconds. Average reconstruction error is: 21.7189
 epoch 43/100. Took 0.63736 seconds. Average reconstruction error is: 21.6216
 epoch 44/100. Took 0.6876 seconds. Average reconstruction error is: 21.5848
 epoch 45/100. Took 0.65252 seconds. Average reconstruction error is: 21.5433
 epoch 46/100. Took 0.63731 seconds. Average reconstruction error is: 21.4926
 epoch 47/100. Took 0.66048 seconds. Average reconstruction error is: 21.4785
 epoch 48/100. Took 0.64511 seconds. Average reconstruction error is: 21.4591
 epoch 49/100. Took 0.71383 seconds. Average reconstruction error is: 21.3649
 epoch 50/100. Took 0.67057 seconds. Average reconstruction error is: 21.433
 epoch 51/100. Took 0.67158 seconds. Average reconstruction error is: 21.4004
 epoch 52/100. Took 0.64557 seconds. Average reconstruction error is: 21.3939
 epoch 53/100. Took 0.67222 seconds. Average reconstruction error is: 21.3645
 epoch 54/100. Took 0.64255 seconds. Average reconstruction error is: 21.3779
 epoch 55/100. Took 0.67839 seconds. Average reconstruction error is: 21.3311
 epoch 56/100. Took 0.64623 seconds. Average reconstruction error is: 21.3136
 epoch 57/100. Took 0.63149 seconds. Average reconstruction error is: 21.2913
 epoch 58/100. Took 0.66447 seconds. Average reconstruction error is: 21.2802
 epoch 59/100. Took 0.63584 seconds. Average reconstruction error is: 21.217
 epoch 60/100. Took 0.64426 seconds. Average reconstruction error is: 21.1852
 epoch 61/100. Took 0.71687 seconds. Average reconstruction error is: 21.1487
 epoch 62/100. Took 0.6621 seconds. Average reconstruction error is: 21.123
 epoch 63/100. Took 0.64587 seconds. Average reconstruction error is: 21.0453
 epoch 64/100. Took 0.67608 seconds. Average reconstruction error is: 21.0067
 epoch 65/100. Took 0.66298 seconds. Average reconstruction error is: 21.0061
 epoch 66/100. Took 0.67282 seconds. Average reconstruction error is: 20.9627
 epoch 67/100. Took 0.66044 seconds. Average reconstruction error is: 20.9253
 epoch 68/100. Took 0.6487 seconds. Average reconstruction error is: 20.9421
 epoch 69/100. Took 0.6541 seconds. Average reconstruction error is: 20.8654
 epoch 70/100. Took 0.709 seconds. Average reconstruction error is: 20.8285
 epoch 71/100. Took 0.62522 seconds. Average reconstruction error is: 20.8104
 epoch 72/100. Took 0.63593 seconds. Average reconstruction error is: 20.7696
 epoch 73/100. Took 0.66115 seconds. Average reconstruction error is: 20.7599
 epoch 74/100. Took 0.659 seconds. Average reconstruction error is: 20.689
 epoch 75/100. Took 0.63991 seconds. Average reconstruction error is: 20.699
 epoch 76/100. Took 0.64207 seconds. Average reconstruction error is: 20.6531
 epoch 77/100. Took 0.64219 seconds. Average reconstruction error is: 20.563
 epoch 78/100. Took 0.68212 seconds. Average reconstruction error is: 20.5351
 epoch 79/100. Took 0.66636 seconds. Average reconstruction error is: 20.4975
 epoch 80/100. Took 0.6724 seconds. Average reconstruction error is: 20.4163
 epoch 81/100. Took 0.69814 seconds. Average reconstruction error is: 20.437
 epoch 82/100. Took 0.62511 seconds. Average reconstruction error is: 20.3693
 epoch 83/100. Took 0.68113 seconds. Average reconstruction error is: 20.3136
 epoch 84/100. Took 0.63735 seconds. Average reconstruction error is: 20.3628
 epoch 85/100. Took 0.68877 seconds. Average reconstruction error is: 20.3157
 epoch 86/100. Took 0.67637 seconds. Average reconstruction error is: 20.2674
 epoch 87/100. Took 0.69401 seconds. Average reconstruction error is: 20.2702
 epoch 88/100. Took 0.64041 seconds. Average reconstruction error is: 20.2354
 epoch 89/100. Took 0.67056 seconds. Average reconstruction error is: 20.2155
 epoch 90/100. Took 0.65035 seconds. Average reconstruction error is: 20.1949
 epoch 91/100. Took 0.6686 seconds. Average reconstruction error is: 20.1919
 epoch 92/100. Took 0.65688 seconds. Average reconstruction error is: 20.1653
 epoch 93/100. Took 0.70902 seconds. Average reconstruction error is: 20.1234
 epoch 94/100. Took 0.67052 seconds. Average reconstruction error is: 20.0914
 epoch 95/100. Took 0.65876 seconds. Average reconstruction error is: 20.1114
 epoch 96/100. Took 0.63139 seconds. Average reconstruction error is: 20.0897
 epoch 97/100. Took 0.63763 seconds. Average reconstruction error is: 20.046
 epoch 98/100. Took 0.63866 seconds. Average reconstruction error is: 20.0585
 epoch 99/100. Took 0.62942 seconds. Average reconstruction error is: 20.0056
 epoch 100/100. Took 0.63354 seconds. Average reconstruction error is: 20.0604
Training binary-binary RBM in layer 2 (50  50) with CD1 for 100 epochs
 epoch 1/100. Took 0.09714 seconds. Average reconstruction error is: 4.0491
 epoch 2/100. Took 0.095224 seconds. Average reconstruction error is: 2.2745
 epoch 3/100. Took 0.12141 seconds. Average reconstruction error is: 1.8818
 epoch 4/100. Took 0.12 seconds. Average reconstruction error is: 1.7148
 epoch 5/100. Took 0.10889 seconds. Average reconstruction error is: 1.5934
 epoch 6/100. Took 0.094476 seconds. Average reconstruction error is: 1.5046
 epoch 7/100. Took 0.10289 seconds. Average reconstruction error is: 1.4446
 epoch 8/100. Took 0.094192 seconds. Average reconstruction error is: 1.4084
 epoch 9/100. Took 0.098726 seconds. Average reconstruction error is: 1.3579
 epoch 10/100. Took 0.094475 seconds. Average reconstruction error is: 1.3295
 epoch 11/100. Took 0.09579 seconds. Average reconstruction error is: 1.3134
 epoch 12/100. Took 0.098756 seconds. Average reconstruction error is: 1.2886
 epoch 13/100. Took 0.094668 seconds. Average reconstruction error is: 1.2652
 epoch 14/100. Took 0.095357 seconds. Average reconstruction error is: 1.2602
 epoch 15/100. Took 0.10763 seconds. Average reconstruction error is: 1.2359
 epoch 16/100. Took 0.10164 seconds. Average reconstruction error is: 1.2347
 epoch 17/100. Took 0.1002 seconds. Average reconstruction error is: 1.2148
 epoch 18/100. Took 0.10934 seconds. Average reconstruction error is: 1.193
 epoch 19/100. Took 0.10377 seconds. Average reconstruction error is: 1.1847
 epoch 20/100. Took 0.11941 seconds. Average reconstruction error is: 1.1804
 epoch 21/100. Took 0.10434 seconds. Average reconstruction error is: 1.1683
 epoch 22/100. Took 0.099666 seconds. Average reconstruction error is: 1.1637
 epoch 23/100. Took 0.097575 seconds. Average reconstruction error is: 1.158
 epoch 24/100. Took 0.094658 seconds. Average reconstruction error is: 1.145
 epoch 25/100. Took 0.10067 seconds. Average reconstruction error is: 1.1392
 epoch 26/100. Took 0.11977 seconds. Average reconstruction error is: 1.1345
 epoch 27/100. Took 0.11073 seconds. Average reconstruction error is: 1.1411
 epoch 28/100. Took 0.10061 seconds. Average reconstruction error is: 1.1275
 epoch 29/100. Took 0.10685 seconds. Average reconstruction error is: 1.1225
 epoch 30/100. Took 0.10281 seconds. Average reconstruction error is: 1.1287
 epoch 31/100. Took 0.11611 seconds. Average reconstruction error is: 1.1118
 epoch 32/100. Took 0.097489 seconds. Average reconstruction error is: 1.1186
 epoch 33/100. Took 0.093254 seconds. Average reconstruction error is: 1.1054
 epoch 34/100. Took 0.10091 seconds. Average reconstruction error is: 1.1055
 epoch 35/100. Took 0.095608 seconds. Average reconstruction error is: 1.1074
 epoch 36/100. Took 0.1077 seconds. Average reconstruction error is: 1.1059
 epoch 37/100. Took 0.094651 seconds. Average reconstruction error is: 1.098
 epoch 38/100. Took 0.094618 seconds. Average reconstruction error is: 1.0834
 epoch 39/100. Took 0.097332 seconds. Average reconstruction error is: 1.0749
 epoch 40/100. Took 0.093426 seconds. Average reconstruction error is: 1.0881
 epoch 41/100. Took 0.094615 seconds. Average reconstruction error is: 1.077
 epoch 42/100. Took 0.10144 seconds. Average reconstruction error is: 1.075
 epoch 43/100. Took 0.097109 seconds. Average reconstruction error is: 1.0741
 epoch 44/100. Took 0.10019 seconds. Average reconstruction error is: 1.069
 epoch 45/100. Took 0.093483 seconds. Average reconstruction error is: 1.0735
 epoch 46/100. Took 0.10212 seconds. Average reconstruction error is: 1.0565
 epoch 47/100. Took 0.094591 seconds. Average reconstruction error is: 1.0718
 epoch 48/100. Took 0.094626 seconds. Average reconstruction error is: 1.0732
 epoch 49/100. Took 0.094627 seconds. Average reconstruction error is: 1.0668
 epoch 50/100. Took 0.094483 seconds. Average reconstruction error is: 1.0547
 epoch 51/100. Took 0.0983 seconds. Average reconstruction error is: 1.0513
 epoch 52/100. Took 0.097684 seconds. Average reconstruction error is: 1.0502
 epoch 53/100. Took 0.10829 seconds. Average reconstruction error is: 1.0443
 epoch 54/100. Took 0.093871 seconds. Average reconstruction error is: 1.0523
 epoch 55/100. Took 0.094188 seconds. Average reconstruction error is: 1.0335
 epoch 56/100. Took 0.093259 seconds. Average reconstruction error is: 1.042
 epoch 57/100. Took 0.095185 seconds. Average reconstruction error is: 1.0505
 epoch 58/100. Took 0.094896 seconds. Average reconstruction error is: 1.0342
 epoch 59/100. Took 0.097387 seconds. Average reconstruction error is: 1.0443
 epoch 60/100. Took 0.10705 seconds. Average reconstruction error is: 1.0352
 epoch 61/100. Took 0.10431 seconds. Average reconstruction error is: 1.0334
 epoch 62/100. Took 0.10252 seconds. Average reconstruction error is: 1.0264
 epoch 63/100. Took 0.10654 seconds. Average reconstruction error is: 1.0228
 epoch 64/100. Took 0.094608 seconds. Average reconstruction error is: 1.0213
 epoch 65/100. Took 0.098447 seconds. Average reconstruction error is: 1.0201
 epoch 66/100. Took 0.09601 seconds. Average reconstruction error is: 1.0077
 epoch 67/100. Took 0.092017 seconds. Average reconstruction error is: 1.0242
 epoch 68/100. Took 0.10023 seconds. Average reconstruction error is: 1.015
 epoch 69/100. Took 0.1031 seconds. Average reconstruction error is: 1.0075
 epoch 70/100. Took 0.094574 seconds. Average reconstruction error is: 1.0007
 epoch 71/100. Took 0.095187 seconds. Average reconstruction error is: 1.0138
 epoch 72/100. Took 0.10622 seconds. Average reconstruction error is: 1.007
 epoch 73/100. Took 0.098623 seconds. Average reconstruction error is: 1.005
 epoch 74/100. Took 0.094983 seconds. Average reconstruction error is: 0.99849
 epoch 75/100. Took 0.096017 seconds. Average reconstruction error is: 0.99457
 epoch 76/100. Took 0.10178 seconds. Average reconstruction error is: 0.98673
 epoch 77/100. Took 0.097199 seconds. Average reconstruction error is: 0.98137
 epoch 78/100. Took 0.094975 seconds. Average reconstruction error is: 0.98611
 epoch 79/100. Took 0.094704 seconds. Average reconstruction error is: 0.97515
 epoch 80/100. Took 0.10426 seconds. Average reconstruction error is: 0.97693
 epoch 81/100. Took 0.093755 seconds. Average reconstruction error is: 0.97087
 epoch 82/100. Took 0.10316 seconds. Average reconstruction error is: 0.96085
 epoch 83/100. Took 0.1094 seconds. Average reconstruction error is: 0.96879
 epoch 84/100. Took 0.10259 seconds. Average reconstruction error is: 0.96562
 epoch 85/100. Took 0.09458 seconds. Average reconstruction error is: 0.96468
 epoch 86/100. Took 0.097345 seconds. Average reconstruction error is: 0.95541
 epoch 87/100. Took 0.10049 seconds. Average reconstruction error is: 0.95157
 epoch 88/100. Took 0.091762 seconds. Average reconstruction error is: 0.95613
 epoch 89/100. Took 0.096636 seconds. Average reconstruction error is: 0.93841
 epoch 90/100. Took 0.10894 seconds. Average reconstruction error is: 0.93799
 epoch 91/100. Took 0.10738 seconds. Average reconstruction error is: 0.93549
 epoch 92/100. Took 0.10428 seconds. Average reconstruction error is: 0.94635
 epoch 93/100. Took 0.1028 seconds. Average reconstruction error is: 0.94928
 epoch 94/100. Took 0.095003 seconds. Average reconstruction error is: 0.94237
 epoch 95/100. Took 0.094195 seconds. Average reconstruction error is: 0.93475
 epoch 96/100. Took 0.097811 seconds. Average reconstruction error is: 0.93599
 epoch 97/100. Took 0.094186 seconds. Average reconstruction error is: 0.93868
 epoch 98/100. Took 0.10446 seconds. Average reconstruction error is: 0.93017
 epoch 99/100. Took 0.094553 seconds. Average reconstruction error is: 0.92599
 epoch 100/100. Took 0.10472 seconds. Average reconstruction error is: 0.93085
Training NN  (784   50   50   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.38786 seconds. 
  Full-batch training loss = 0.442737, test loss = 0.438708
  Training set accuracy = 0.870800, Test set accuracy = 0.870500
 epoch 2/100. Took 0.42173 seconds. 
  Full-batch training loss = 0.369253, test loss = 0.377685
  Training set accuracy = 0.894100, Test set accuracy = 0.885900
 epoch 3/100. Took 0.40415 seconds. 
  Full-batch training loss = 0.339294, test loss = 0.358723
  Training set accuracy = 0.899300, Test set accuracy = 0.892200
 epoch 4/100. Took 0.43056 seconds. 
  Full-batch training loss = 0.313495, test loss = 0.343354
  Training set accuracy = 0.909000, Test set accuracy = 0.895200
 epoch 5/100. Took 0.45526 seconds. 
  Full-batch training loss = 0.296912, test loss = 0.334423
  Training set accuracy = 0.915500, Test set accuracy = 0.898800
 epoch 6/100. Took 0.42495 seconds. 
  Full-batch training loss = 0.274357, test loss = 0.320146
  Training set accuracy = 0.920100, Test set accuracy = 0.902100
 epoch 7/100. Took 0.47297 seconds. 
  Full-batch training loss = 0.261558, test loss = 0.311865
  Training set accuracy = 0.925100, Test set accuracy = 0.903100
 epoch 8/100. Took 0.41629 seconds. 
  Full-batch training loss = 0.248336, test loss = 0.303557
  Training set accuracy = 0.930000, Test set accuracy = 0.907500
 epoch 9/100. Took 0.4371 seconds. 
  Full-batch training loss = 0.238733, test loss = 0.300344
  Training set accuracy = 0.932300, Test set accuracy = 0.908400
 epoch 10/100. Took 0.4176 seconds. 
  Full-batch training loss = 0.230173, test loss = 0.295773
  Training set accuracy = 0.936600, Test set accuracy = 0.910400
 epoch 11/100. Took 0.43265 seconds. 
  Full-batch training loss = 0.226250, test loss = 0.300714
  Training set accuracy = 0.936400, Test set accuracy = 0.908400
 epoch 12/100. Took 0.47316 seconds. 
  Full-batch training loss = 0.214732, test loss = 0.290934
  Training set accuracy = 0.939200, Test set accuracy = 0.912800
 epoch 13/100. Took 0.42532 seconds. 
  Full-batch training loss = 0.207723, test loss = 0.286877
  Training set accuracy = 0.941200, Test set accuracy = 0.912300
 epoch 14/100. Took 0.45786 seconds. 
  Full-batch training loss = 0.200520, test loss = 0.285681
  Training set accuracy = 0.944500, Test set accuracy = 0.915300
 epoch 15/100. Took 0.44755 seconds. 
  Full-batch training loss = 0.193480, test loss = 0.283437
  Training set accuracy = 0.947000, Test set accuracy = 0.915200
 epoch 16/100. Took 0.42091 seconds. 
  Full-batch training loss = 0.187089, test loss = 0.279180
  Training set accuracy = 0.948200, Test set accuracy = 0.915900
 epoch 17/100. Took 0.42551 seconds. 
  Full-batch training loss = 0.183466, test loss = 0.280869
  Training set accuracy = 0.949900, Test set accuracy = 0.917900
 epoch 18/100. Took 0.41572 seconds. 
  Full-batch training loss = 0.175816, test loss = 0.275898
  Training set accuracy = 0.952700, Test set accuracy = 0.916300
 epoch 19/100. Took 0.42309 seconds. 
  Full-batch training loss = 0.173498, test loss = 0.276045
  Training set accuracy = 0.951400, Test set accuracy = 0.916900
 epoch 20/100. Took 0.47483 seconds. 
  Full-batch training loss = 0.166470, test loss = 0.274660
  Training set accuracy = 0.954200, Test set accuracy = 0.916900
 epoch 21/100. Took 0.39672 seconds. 
  Full-batch training loss = 0.163649, test loss = 0.276406
  Training set accuracy = 0.955300, Test set accuracy = 0.917700
 epoch 22/100. Took 0.42223 seconds. 
  Full-batch training loss = 0.157053, test loss = 0.271141
  Training set accuracy = 0.957000, Test set accuracy = 0.921200
 epoch 23/100. Took 0.39301 seconds. 
  Full-batch training loss = 0.151365, test loss = 0.270201
  Training set accuracy = 0.959500, Test set accuracy = 0.919100
 epoch 24/100. Took 0.43922 seconds. 
  Full-batch training loss = 0.146277, test loss = 0.268186
  Training set accuracy = 0.961700, Test set accuracy = 0.920500
 epoch 25/100. Took 0.41148 seconds. 
  Full-batch training loss = 0.142486, test loss = 0.266401
  Training set accuracy = 0.963900, Test set accuracy = 0.921600
 epoch 26/100. Took 0.4244 seconds. 
  Full-batch training loss = 0.139060, test loss = 0.266874
  Training set accuracy = 0.962900, Test set accuracy = 0.922200
 epoch 27/100. Took 0.39964 seconds. 
  Full-batch training loss = 0.135680, test loss = 0.267677
  Training set accuracy = 0.964600, Test set accuracy = 0.921400
 epoch 28/100. Took 0.41539 seconds. 
  Full-batch training loss = 0.133313, test loss = 0.267667
  Training set accuracy = 0.965300, Test set accuracy = 0.921600
 epoch 29/100. Took 0.39547 seconds. 
  Full-batch training loss = 0.131408, test loss = 0.269269
  Training set accuracy = 0.965900, Test set accuracy = 0.922300
 epoch 30/100. Took 0.42701 seconds. 
  Full-batch training loss = 0.131534, test loss = 0.272838
  Training set accuracy = 0.965100, Test set accuracy = 0.920300
 epoch 31/100. Took 0.4078 seconds. 
  Full-batch training loss = 0.123790, test loss = 0.266332
  Training set accuracy = 0.969400, Test set accuracy = 0.924200
 epoch 32/100. Took 0.41895 seconds. 
  Full-batch training loss = 0.121273, test loss = 0.264776
  Training set accuracy = 0.968100, Test set accuracy = 0.923800
 epoch 33/100. Took 0.41869 seconds. 
  Full-batch training loss = 0.118491, test loss = 0.266235
  Training set accuracy = 0.969300, Test set accuracy = 0.924500
 epoch 34/100. Took 0.43176 seconds. 
  Full-batch training loss = 0.116192, test loss = 0.264437
  Training set accuracy = 0.971200, Test set accuracy = 0.924400
 epoch 35/100. Took 0.42579 seconds. 
  Full-batch training loss = 0.112392, test loss = 0.265341
  Training set accuracy = 0.971100, Test set accuracy = 0.924300
 epoch 36/100. Took 0.4217 seconds. 
  Full-batch training loss = 0.113219, test loss = 0.271112
  Training set accuracy = 0.971600, Test set accuracy = 0.924900
 epoch 37/100. Took 0.40888 seconds. 
  Full-batch training loss = 0.107045, test loss = 0.264725
  Training set accuracy = 0.973800, Test set accuracy = 0.924000
 epoch 38/100. Took 0.42044 seconds. 
  Full-batch training loss = 0.107350, test loss = 0.266878
  Training set accuracy = 0.972600, Test set accuracy = 0.924900
 epoch 39/100. Took 0.44076 seconds. 
  Full-batch training loss = 0.102575, test loss = 0.264842
  Training set accuracy = 0.975000, Test set accuracy = 0.925800
 epoch 40/100. Took 0.46418 seconds. 
  Full-batch training loss = 0.100467, test loss = 0.264137
  Training set accuracy = 0.975200, Test set accuracy = 0.925000
 epoch 41/100. Took 0.42051 seconds. 
  Full-batch training loss = 0.100549, test loss = 0.269335
  Training set accuracy = 0.974600, Test set accuracy = 0.924800
 epoch 42/100. Took 0.43417 seconds. 
  Full-batch training loss = 0.098013, test loss = 0.268062
  Training set accuracy = 0.975000, Test set accuracy = 0.925700
 epoch 43/100. Took 0.42895 seconds. 
  Full-batch training loss = 0.096330, test loss = 0.269278
  Training set accuracy = 0.976200, Test set accuracy = 0.925800
 epoch 44/100. Took 0.4095 seconds. 
  Full-batch training loss = 0.092965, test loss = 0.267581
  Training set accuracy = 0.977100, Test set accuracy = 0.925400
 epoch 45/100. Took 0.40913 seconds. 
  Full-batch training loss = 0.092569, test loss = 0.267663
  Training set accuracy = 0.976000, Test set accuracy = 0.925700
 epoch 46/100. Took 0.40619 seconds. 
  Full-batch training loss = 0.089211, test loss = 0.267079
  Training set accuracy = 0.977800, Test set accuracy = 0.925600
 epoch 47/100. Took 0.40452 seconds. 
  Full-batch training loss = 0.086751, test loss = 0.266694
  Training set accuracy = 0.977700, Test set accuracy = 0.926700
 epoch 48/100. Took 0.41484 seconds. 
  Full-batch training loss = 0.086160, test loss = 0.268421
  Training set accuracy = 0.978300, Test set accuracy = 0.926000
 epoch 49/100. Took 0.39669 seconds. 
  Full-batch training loss = 0.083639, test loss = 0.268087
  Training set accuracy = 0.979800, Test set accuracy = 0.927200
 epoch 50/100. Took 0.44558 seconds. 
  Full-batch training loss = 0.082995, test loss = 0.268534
  Training set accuracy = 0.979500, Test set accuracy = 0.926800
 epoch 51/100. Took 0.42426 seconds. 
  Full-batch training loss = 0.080501, test loss = 0.270447
  Training set accuracy = 0.980400, Test set accuracy = 0.925800
 epoch 52/100. Took 0.42102 seconds. 
  Full-batch training loss = 0.078441, test loss = 0.269451
  Training set accuracy = 0.981500, Test set accuracy = 0.927500
 epoch 53/100. Took 0.43667 seconds. 
  Full-batch training loss = 0.076982, test loss = 0.269889
  Training set accuracy = 0.981600, Test set accuracy = 0.926400
 epoch 54/100. Took 0.41108 seconds. 
  Full-batch training loss = 0.075504, test loss = 0.270577
  Training set accuracy = 0.982100, Test set accuracy = 0.927700
 epoch 55/100. Took 0.40494 seconds. 
  Full-batch training loss = 0.075413, test loss = 0.273167
  Training set accuracy = 0.983200, Test set accuracy = 0.926700
 epoch 56/100. Took 0.39987 seconds. 
  Full-batch training loss = 0.072522, test loss = 0.271379
  Training set accuracy = 0.982500, Test set accuracy = 0.928100
 epoch 57/100. Took 0.3981 seconds. 
  Full-batch training loss = 0.070887, test loss = 0.272320
  Training set accuracy = 0.983400, Test set accuracy = 0.927500
 epoch 58/100. Took 0.39847 seconds. 
  Full-batch training loss = 0.069078, test loss = 0.272057
  Training set accuracy = 0.983600, Test set accuracy = 0.927600
 epoch 59/100. Took 0.40934 seconds. 
  Full-batch training loss = 0.071276, test loss = 0.275813
  Training set accuracy = 0.982400, Test set accuracy = 0.926200
 epoch 60/100. Took 0.4268 seconds. 
  Full-batch training loss = 0.067543, test loss = 0.274781
  Training set accuracy = 0.983800, Test set accuracy = 0.927300
 epoch 61/100. Took 0.3975 seconds. 
  Full-batch training loss = 0.066696, test loss = 0.274710
  Training set accuracy = 0.984300, Test set accuracy = 0.926900
 epoch 62/100. Took 0.41468 seconds. 
  Full-batch training loss = 0.064377, test loss = 0.274499
  Training set accuracy = 0.985700, Test set accuracy = 0.927600
 epoch 63/100. Took 0.41092 seconds. 
  Full-batch training loss = 0.064336, test loss = 0.277812
  Training set accuracy = 0.985400, Test set accuracy = 0.927000
 epoch 64/100. Took 0.425 seconds. 
  Full-batch training loss = 0.062763, test loss = 0.275994
  Training set accuracy = 0.986100, Test set accuracy = 0.926700
 epoch 65/100. Took 0.39651 seconds. 
  Full-batch training loss = 0.061306, test loss = 0.276106
  Training set accuracy = 0.986400, Test set accuracy = 0.927500
 epoch 66/100. Took 0.44739 seconds. 
  Full-batch training loss = 0.060387, test loss = 0.276795
  Training set accuracy = 0.986500, Test set accuracy = 0.926800
 epoch 67/100. Took 0.39796 seconds. 
  Full-batch training loss = 0.058701, test loss = 0.277306
  Training set accuracy = 0.986800, Test set accuracy = 0.927700
 epoch 68/100. Took 0.44881 seconds. 
  Full-batch training loss = 0.058414, test loss = 0.277779
  Training set accuracy = 0.986800, Test set accuracy = 0.927300
 epoch 69/100. Took 0.44155 seconds. 
  Full-batch training loss = 0.057410, test loss = 0.279042
  Training set accuracy = 0.987400, Test set accuracy = 0.926700
 epoch 70/100. Took 0.47806 seconds. 
  Full-batch training loss = 0.056232, test loss = 0.279335
  Training set accuracy = 0.987800, Test set accuracy = 0.926400
 epoch 71/100. Took 0.44804 seconds. 
  Full-batch training loss = 0.055937, test loss = 0.279958
  Training set accuracy = 0.988700, Test set accuracy = 0.927800
 epoch 72/100. Took 0.41786 seconds. 
  Full-batch training loss = 0.055264, test loss = 0.281445
  Training set accuracy = 0.988100, Test set accuracy = 0.926400
 epoch 73/100. Took 0.40706 seconds. 
  Full-batch training loss = 0.053864, test loss = 0.281917
  Training set accuracy = 0.988500, Test set accuracy = 0.927400
 epoch 74/100. Took 0.40041 seconds. 
  Full-batch training loss = 0.052684, test loss = 0.282247
  Training set accuracy = 0.989300, Test set accuracy = 0.926700
 epoch 75/100. Took 0.46655 seconds. 
  Full-batch training loss = 0.051615, test loss = 0.282187
  Training set accuracy = 0.989500, Test set accuracy = 0.926900
 epoch 76/100. Took 0.42616 seconds. 
  Full-batch training loss = 0.050786, test loss = 0.281633
  Training set accuracy = 0.990200, Test set accuracy = 0.927600
 epoch 77/100. Took 0.44171 seconds. 
  Full-batch training loss = 0.051107, test loss = 0.284168
  Training set accuracy = 0.989700, Test set accuracy = 0.926900
 epoch 78/100. Took 0.39682 seconds. 
  Full-batch training loss = 0.049992, test loss = 0.285541
  Training set accuracy = 0.990000, Test set accuracy = 0.927000
 epoch 79/100. Took 0.42031 seconds. 
  Full-batch training loss = 0.048535, test loss = 0.285064
  Training set accuracy = 0.989900, Test set accuracy = 0.926900
 epoch 80/100. Took 0.40736 seconds. 
  Full-batch training loss = 0.048145, test loss = 0.286990
  Training set accuracy = 0.990800, Test set accuracy = 0.927300
 epoch 81/100. Took 0.45999 seconds. 
  Full-batch training loss = 0.046842, test loss = 0.285985
  Training set accuracy = 0.990700, Test set accuracy = 0.927200
 epoch 82/100. Took 0.43085 seconds. 
  Full-batch training loss = 0.046780, test loss = 0.287456
  Training set accuracy = 0.991100, Test set accuracy = 0.927600
 epoch 83/100. Took 0.47774 seconds. 
  Full-batch training loss = 0.046146, test loss = 0.287676
  Training set accuracy = 0.991400, Test set accuracy = 0.927000
 epoch 84/100. Took 0.3968 seconds. 
  Full-batch training loss = 0.045437, test loss = 0.288711
  Training set accuracy = 0.991100, Test set accuracy = 0.927800
 epoch 85/100. Took 0.42583 seconds. 
  Full-batch training loss = 0.044965, test loss = 0.291220
  Training set accuracy = 0.991200, Test set accuracy = 0.927500
 epoch 86/100. Took 0.39508 seconds. 
  Full-batch training loss = 0.044293, test loss = 0.290790
  Training set accuracy = 0.991700, Test set accuracy = 0.926500
 epoch 87/100. Took 0.39915 seconds. 
  Full-batch training loss = 0.043155, test loss = 0.289977
  Training set accuracy = 0.991700, Test set accuracy = 0.927800
 epoch 88/100. Took 0.43142 seconds. 
  Full-batch training loss = 0.042140, test loss = 0.290042
  Training set accuracy = 0.991800, Test set accuracy = 0.928100
 epoch 89/100. Took 0.39963 seconds. 
  Full-batch training loss = 0.042213, test loss = 0.291458
  Training set accuracy = 0.991800, Test set accuracy = 0.926300
 epoch 90/100. Took 0.41415 seconds. 
  Full-batch training loss = 0.040982, test loss = 0.290564
  Training set accuracy = 0.992100, Test set accuracy = 0.928100
 epoch 91/100. Took 0.42933 seconds. 
  Full-batch training loss = 0.040334, test loss = 0.292454
  Training set accuracy = 0.992600, Test set accuracy = 0.927600
 epoch 92/100. Took 0.41703 seconds. 
  Full-batch training loss = 0.040046, test loss = 0.292665
  Training set accuracy = 0.992800, Test set accuracy = 0.927000
 epoch 93/100. Took 0.39717 seconds. 
  Full-batch training loss = 0.039860, test loss = 0.293850
  Training set accuracy = 0.992600, Test set accuracy = 0.926900
 epoch 94/100. Took 0.43559 seconds. 
  Full-batch training loss = 0.038910, test loss = 0.293995
  Training set accuracy = 0.992900, Test set accuracy = 0.927500
 epoch 95/100. Took 0.44446 seconds. 
  Full-batch training loss = 0.038499, test loss = 0.294620
  Training set accuracy = 0.993100, Test set accuracy = 0.926200
 epoch 96/100. Took 0.41881 seconds. 
  Full-batch training loss = 0.037894, test loss = 0.296097
  Training set accuracy = 0.993500, Test set accuracy = 0.926200
 epoch 97/100. Took 0.43395 seconds. 
  Full-batch training loss = 0.037476, test loss = 0.297229
  Training set accuracy = 0.993500, Test set accuracy = 0.927300
 epoch 98/100. Took 0.43595 seconds. 
  Full-batch training loss = 0.037121, test loss = 0.296790
  Training set accuracy = 0.993400, Test set accuracy = 0.927200
 epoch 99/100. Took 0.44187 seconds. 
  Full-batch training loss = 0.036227, test loss = 0.297504
  Training set accuracy = 0.993500, Test set accuracy = 0.927800
 epoch 100/100. Took 0.42386 seconds. 
  Full-batch training loss = 0.035753, test loss = 0.299039
  Training set accuracy = 0.993900, Test set accuracy = 0.927600
Elapsed time is 166.400630 seconds.
End Training
