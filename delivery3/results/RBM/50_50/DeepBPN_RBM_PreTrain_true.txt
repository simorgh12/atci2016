Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (50  50)
Training binary-binary RBM in layer 1 (784   50) with CD1 for 20 epochs
 epoch 1/20. Took 0.66401 seconds. Average reconstruction error is: 39.7309
 epoch 2/20. Took 0.65315 seconds. Average reconstruction error is: 27.5618
 epoch 3/20. Took 0.64498 seconds. Average reconstruction error is: 25.4611
 epoch 4/20. Took 0.65444 seconds. Average reconstruction error is: 24.2916
 epoch 5/20. Took 0.63387 seconds. Average reconstruction error is: 23.6935
 epoch 6/20. Took 0.69328 seconds. Average reconstruction error is: 23.4012
 epoch 7/20. Took 0.74979 seconds. Average reconstruction error is: 23.2597
 epoch 8/20. Took 0.68226 seconds. Average reconstruction error is: 23.0755
 epoch 9/20. Took 0.67618 seconds. Average reconstruction error is: 22.9415
 epoch 10/20. Took 0.71326 seconds. Average reconstruction error is: 22.7834
 epoch 11/20. Took 0.63391 seconds. Average reconstruction error is: 22.6473
 epoch 12/20. Took 0.64564 seconds. Average reconstruction error is: 22.5228
 epoch 13/20. Took 0.69194 seconds. Average reconstruction error is: 22.4305
 epoch 14/20. Took 0.64615 seconds. Average reconstruction error is: 22.2952
 epoch 15/20. Took 0.67532 seconds. Average reconstruction error is: 22.1945
 epoch 16/20. Took 0.64684 seconds. Average reconstruction error is: 22.0881
 epoch 17/20. Took 0.68806 seconds. Average reconstruction error is: 22.0697
 epoch 18/20. Took 0.66491 seconds. Average reconstruction error is: 22.0108
 epoch 19/20. Took 0.68116 seconds. Average reconstruction error is: 21.962
 epoch 20/20. Took 0.67947 seconds. Average reconstruction error is: 21.9198
Training binary-binary RBM in layer 2 (50  50) with CD1 for 20 epochs
 epoch 1/20. Took 0.092404 seconds. Average reconstruction error is: 3.7519
 epoch 2/20. Took 0.092235 seconds. Average reconstruction error is: 2.0801
 epoch 3/20. Took 0.097112 seconds. Average reconstruction error is: 1.727
 epoch 4/20. Took 0.09216 seconds. Average reconstruction error is: 1.5381
 epoch 5/20. Took 0.10411 seconds. Average reconstruction error is: 1.4192
 epoch 6/20. Took 0.119 seconds. Average reconstruction error is: 1.3418
 epoch 7/20. Took 0.1041 seconds. Average reconstruction error is: 1.2935
 epoch 8/20. Took 0.11615 seconds. Average reconstruction error is: 1.2404
 epoch 9/20. Took 0.10437 seconds. Average reconstruction error is: 1.1918
 epoch 10/20. Took 0.099575 seconds. Average reconstruction error is: 1.1676
 epoch 11/20. Took 0.098158 seconds. Average reconstruction error is: 1.1503
 epoch 12/20. Took 0.10721 seconds. Average reconstruction error is: 1.1178
 epoch 13/20. Took 0.094416 seconds. Average reconstruction error is: 1.1239
 epoch 14/20. Took 0.099934 seconds. Average reconstruction error is: 1.0973
 epoch 15/20. Took 0.09544 seconds. Average reconstruction error is: 1.0798
 epoch 16/20. Took 0.11081 seconds. Average reconstruction error is: 1.0632
 epoch 17/20. Took 0.094438 seconds. Average reconstruction error is: 1.0522
 epoch 18/20. Took 0.10585 seconds. Average reconstruction error is: 1.0519
 epoch 19/20. Took 0.10317 seconds. Average reconstruction error is: 1.039
 epoch 20/20. Took 0.10722 seconds. Average reconstruction error is: 1.0314
Training NN  (784   50   50   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.39821 seconds. 
  Full-batch training loss = 0.475708, test loss = 0.464337
  Training set accuracy = 0.862900, Test set accuracy = 0.866600
 epoch 2/100. Took 0.39459 seconds. 
  Full-batch training loss = 0.409850, test loss = 0.413280
  Training set accuracy = 0.877400, Test set accuracy = 0.876200
 epoch 3/100. Took 0.41843 seconds. 
  Full-batch training loss = 0.369937, test loss = 0.379337
  Training set accuracy = 0.888400, Test set accuracy = 0.884700
 epoch 4/100. Took 0.42398 seconds. 
  Full-batch training loss = 0.338249, test loss = 0.360156
  Training set accuracy = 0.898400, Test set accuracy = 0.890500
 epoch 5/100. Took 0.49504 seconds. 
  Full-batch training loss = 0.317500, test loss = 0.345259
  Training set accuracy = 0.903800, Test set accuracy = 0.895500
 epoch 6/100. Took 0.46459 seconds. 
  Full-batch training loss = 0.303434, test loss = 0.342147
  Training set accuracy = 0.909600, Test set accuracy = 0.894300
 epoch 7/100. Took 0.42879 seconds. 
  Full-batch training loss = 0.286818, test loss = 0.334864
  Training set accuracy = 0.914200, Test set accuracy = 0.897300
 epoch 8/100. Took 0.4052 seconds. 
  Full-batch training loss = 0.268839, test loss = 0.319499
  Training set accuracy = 0.919900, Test set accuracy = 0.902900
 epoch 9/100. Took 0.39839 seconds. 
  Full-batch training loss = 0.257094, test loss = 0.315929
  Training set accuracy = 0.923500, Test set accuracy = 0.902800
 epoch 10/100. Took 0.39517 seconds. 
  Full-batch training loss = 0.250387, test loss = 0.314057
  Training set accuracy = 0.925200, Test set accuracy = 0.902800
 epoch 11/100. Took 0.47444 seconds. 
  Full-batch training loss = 0.233764, test loss = 0.303307
  Training set accuracy = 0.931400, Test set accuracy = 0.907300
 epoch 12/100. Took 0.40842 seconds. 
  Full-batch training loss = 0.229192, test loss = 0.303984
  Training set accuracy = 0.933000, Test set accuracy = 0.907800
 epoch 13/100. Took 0.46562 seconds. 
  Full-batch training loss = 0.219722, test loss = 0.300066
  Training set accuracy = 0.932800, Test set accuracy = 0.908300
 epoch 14/100. Took 0.4423 seconds. 
  Full-batch training loss = 0.212754, test loss = 0.294841
  Training set accuracy = 0.938700, Test set accuracy = 0.911900
 epoch 15/100. Took 0.39833 seconds. 
  Full-batch training loss = 0.206003, test loss = 0.292953
  Training set accuracy = 0.938900, Test set accuracy = 0.912200
 epoch 16/100. Took 0.45056 seconds. 
  Full-batch training loss = 0.196250, test loss = 0.288832
  Training set accuracy = 0.944000, Test set accuracy = 0.912300
 epoch 17/100. Took 0.40824 seconds. 
  Full-batch training loss = 0.192625, test loss = 0.289867
  Training set accuracy = 0.944300, Test set accuracy = 0.912300
 epoch 18/100. Took 0.40565 seconds. 
  Full-batch training loss = 0.183895, test loss = 0.285657
  Training set accuracy = 0.946000, Test set accuracy = 0.914500
 epoch 19/100. Took 0.46203 seconds. 
  Full-batch training loss = 0.178281, test loss = 0.284086
  Training set accuracy = 0.948100, Test set accuracy = 0.913800
 epoch 20/100. Took 0.43331 seconds. 
  Full-batch training loss = 0.171508, test loss = 0.280941
  Training set accuracy = 0.951600, Test set accuracy = 0.915200
 epoch 21/100. Took 0.41089 seconds. 
  Full-batch training loss = 0.170441, test loss = 0.284300
  Training set accuracy = 0.951400, Test set accuracy = 0.915200
 epoch 22/100. Took 0.4262 seconds. 
  Full-batch training loss = 0.162383, test loss = 0.278346
  Training set accuracy = 0.955300, Test set accuracy = 0.916800
 epoch 23/100. Took 0.42604 seconds. 
  Full-batch training loss = 0.164591, test loss = 0.285501
  Training set accuracy = 0.953700, Test set accuracy = 0.915700
 epoch 24/100. Took 0.39639 seconds. 
  Full-batch training loss = 0.155681, test loss = 0.278568
  Training set accuracy = 0.957000, Test set accuracy = 0.917600
 epoch 25/100. Took 0.42627 seconds. 
  Full-batch training loss = 0.150462, test loss = 0.275744
  Training set accuracy = 0.957900, Test set accuracy = 0.918600
 epoch 26/100. Took 0.42692 seconds. 
  Full-batch training loss = 0.147780, test loss = 0.278253
  Training set accuracy = 0.959200, Test set accuracy = 0.917100
 epoch 27/100. Took 0.39735 seconds. 
  Full-batch training loss = 0.145617, test loss = 0.278049
  Training set accuracy = 0.959600, Test set accuracy = 0.918700
 epoch 28/100. Took 0.50259 seconds. 
  Full-batch training loss = 0.140532, test loss = 0.276764
  Training set accuracy = 0.961800, Test set accuracy = 0.918000
 epoch 29/100. Took 0.44433 seconds. 
  Full-batch training loss = 0.140243, test loss = 0.280976
  Training set accuracy = 0.961000, Test set accuracy = 0.918000
 epoch 30/100. Took 0.43549 seconds. 
  Full-batch training loss = 0.132551, test loss = 0.274635
  Training set accuracy = 0.964400, Test set accuracy = 0.920800
 epoch 31/100. Took 0.45655 seconds. 
  Full-batch training loss = 0.131079, test loss = 0.277847
  Training set accuracy = 0.964100, Test set accuracy = 0.920800
 epoch 32/100. Took 0.41435 seconds. 
  Full-batch training loss = 0.125762, test loss = 0.273838
  Training set accuracy = 0.966300, Test set accuracy = 0.920500
 epoch 33/100. Took 0.42731 seconds. 
  Full-batch training loss = 0.124691, test loss = 0.276603
  Training set accuracy = 0.966400, Test set accuracy = 0.920500
 epoch 34/100. Took 0.41304 seconds. 
  Full-batch training loss = 0.120492, test loss = 0.274465
  Training set accuracy = 0.967900, Test set accuracy = 0.920300
 epoch 35/100. Took 0.45824 seconds. 
  Full-batch training loss = 0.117624, test loss = 0.275547
  Training set accuracy = 0.969300, Test set accuracy = 0.919900
 epoch 36/100. Took 0.41201 seconds. 
  Full-batch training loss = 0.117972, test loss = 0.278073
  Training set accuracy = 0.967700, Test set accuracy = 0.921800
 epoch 37/100. Took 0.43493 seconds. 
  Full-batch training loss = 0.112538, test loss = 0.274019
  Training set accuracy = 0.970400, Test set accuracy = 0.920700
 epoch 38/100. Took 0.40194 seconds. 
  Full-batch training loss = 0.111693, test loss = 0.275229
  Training set accuracy = 0.970200, Test set accuracy = 0.921400
 epoch 39/100. Took 0.42975 seconds. 
  Full-batch training loss = 0.108865, test loss = 0.277941
  Training set accuracy = 0.970300, Test set accuracy = 0.922000
 epoch 40/100. Took 0.45563 seconds. 
  Full-batch training loss = 0.107081, test loss = 0.276485
  Training set accuracy = 0.972400, Test set accuracy = 0.921800
 epoch 41/100. Took 0.41953 seconds. 
  Full-batch training loss = 0.104347, test loss = 0.275343
  Training set accuracy = 0.972800, Test set accuracy = 0.922700
 epoch 42/100. Took 0.4134 seconds. 
  Full-batch training loss = 0.101973, test loss = 0.276709
  Training set accuracy = 0.973900, Test set accuracy = 0.922700
 epoch 43/100. Took 0.41594 seconds. 
  Full-batch training loss = 0.100534, test loss = 0.278360
  Training set accuracy = 0.974400, Test set accuracy = 0.921500
 epoch 44/100. Took 0.44379 seconds. 
  Full-batch training loss = 0.098273, test loss = 0.278202
  Training set accuracy = 0.975000, Test set accuracy = 0.921700
 epoch 45/100. Took 0.45739 seconds. 
  Full-batch training loss = 0.097433, test loss = 0.279349
  Training set accuracy = 0.975000, Test set accuracy = 0.921600
 epoch 46/100. Took 0.40003 seconds. 
  Full-batch training loss = 0.094403, test loss = 0.278133
  Training set accuracy = 0.976100, Test set accuracy = 0.922700
 epoch 47/100. Took 0.4331 seconds. 
  Full-batch training loss = 0.092366, test loss = 0.279941
  Training set accuracy = 0.977400, Test set accuracy = 0.921700
 epoch 48/100. Took 0.43176 seconds. 
  Full-batch training loss = 0.092873, test loss = 0.284020
  Training set accuracy = 0.975200, Test set accuracy = 0.920100
 epoch 49/100. Took 0.40644 seconds. 
  Full-batch training loss = 0.089458, test loss = 0.282065
  Training set accuracy = 0.977600, Test set accuracy = 0.921700
 epoch 50/100. Took 0.44427 seconds. 
  Full-batch training loss = 0.087525, test loss = 0.280416
  Training set accuracy = 0.978400, Test set accuracy = 0.921500
 epoch 51/100. Took 0.49936 seconds. 
  Full-batch training loss = 0.086030, test loss = 0.282719
  Training set accuracy = 0.978400, Test set accuracy = 0.922400
 epoch 52/100. Took 0.46171 seconds. 
  Full-batch training loss = 0.083383, test loss = 0.281650
  Training set accuracy = 0.979300, Test set accuracy = 0.922800
 epoch 53/100. Took 0.43245 seconds. 
  Full-batch training loss = 0.082587, test loss = 0.281307
  Training set accuracy = 0.979900, Test set accuracy = 0.921900
 epoch 54/100. Took 0.3999 seconds. 
  Full-batch training loss = 0.080429, test loss = 0.282051
  Training set accuracy = 0.980600, Test set accuracy = 0.921600
 epoch 55/100. Took 0.40886 seconds. 
  Full-batch training loss = 0.080653, test loss = 0.286039
  Training set accuracy = 0.979500, Test set accuracy = 0.920800
 epoch 56/100. Took 0.46028 seconds. 
  Full-batch training loss = 0.079809, test loss = 0.289048
  Training set accuracy = 0.980200, Test set accuracy = 0.920800
 epoch 57/100. Took 0.40694 seconds. 
  Full-batch training loss = 0.075821, test loss = 0.284255
  Training set accuracy = 0.982000, Test set accuracy = 0.920800
 epoch 58/100. Took 0.42669 seconds. 
  Full-batch training loss = 0.075829, test loss = 0.287209
  Training set accuracy = 0.981900, Test set accuracy = 0.921700
 epoch 59/100. Took 0.41907 seconds. 
  Full-batch training loss = 0.074737, test loss = 0.290489
  Training set accuracy = 0.981400, Test set accuracy = 0.921300
 epoch 60/100. Took 0.41987 seconds. 
  Full-batch training loss = 0.071241, test loss = 0.284827
  Training set accuracy = 0.983600, Test set accuracy = 0.923000
 epoch 61/100. Took 0.4154 seconds. 
  Full-batch training loss = 0.071374, test loss = 0.290870
  Training set accuracy = 0.983400, Test set accuracy = 0.921200
 epoch 62/100. Took 0.42904 seconds. 
  Full-batch training loss = 0.069134, test loss = 0.288831
  Training set accuracy = 0.983800, Test set accuracy = 0.920300
 epoch 63/100. Took 0.39862 seconds. 
  Full-batch training loss = 0.068770, test loss = 0.290017
  Training set accuracy = 0.983300, Test set accuracy = 0.922300
 epoch 64/100. Took 0.42575 seconds. 
  Full-batch training loss = 0.066490, test loss = 0.287667
  Training set accuracy = 0.984600, Test set accuracy = 0.923300
 epoch 65/100. Took 0.44635 seconds. 
  Full-batch training loss = 0.066277, test loss = 0.288199
  Training set accuracy = 0.984400, Test set accuracy = 0.924000
 epoch 66/100. Took 0.50149 seconds. 
  Full-batch training loss = 0.065207, test loss = 0.290779
  Training set accuracy = 0.985200, Test set accuracy = 0.921500
 epoch 67/100. Took 0.44046 seconds. 
  Full-batch training loss = 0.063678, test loss = 0.291018
  Training set accuracy = 0.985400, Test set accuracy = 0.922100
 epoch 68/100. Took 0.39265 seconds. 
  Full-batch training loss = 0.062407, test loss = 0.290840
  Training set accuracy = 0.986100, Test set accuracy = 0.922400
 epoch 69/100. Took 0.41251 seconds. 
  Full-batch training loss = 0.061760, test loss = 0.292287
  Training set accuracy = 0.986600, Test set accuracy = 0.920900
 epoch 70/100. Took 0.39668 seconds. 
  Full-batch training loss = 0.059508, test loss = 0.291928
  Training set accuracy = 0.986700, Test set accuracy = 0.921800
 epoch 71/100. Took 0.41628 seconds. 
  Full-batch training loss = 0.060270, test loss = 0.295216
  Training set accuracy = 0.986600, Test set accuracy = 0.921500
 epoch 72/100. Took 0.4672 seconds. 
  Full-batch training loss = 0.058966, test loss = 0.295494
  Training set accuracy = 0.987000, Test set accuracy = 0.920000
 epoch 73/100. Took 0.44269 seconds. 
  Full-batch training loss = 0.056453, test loss = 0.292330
  Training set accuracy = 0.988100, Test set accuracy = 0.922000
 epoch 74/100. Took 0.51274 seconds. 
  Full-batch training loss = 0.058217, test loss = 0.297497
  Training set accuracy = 0.987000, Test set accuracy = 0.922400
 epoch 75/100. Took 0.42025 seconds. 
  Full-batch training loss = 0.055978, test loss = 0.296834
  Training set accuracy = 0.988100, Test set accuracy = 0.921500
 epoch 76/100. Took 0.41152 seconds. 
  Full-batch training loss = 0.053819, test loss = 0.295863
  Training set accuracy = 0.989300, Test set accuracy = 0.922500
 epoch 77/100. Took 0.40762 seconds. 
  Full-batch training loss = 0.053594, test loss = 0.297350
  Training set accuracy = 0.989000, Test set accuracy = 0.922100
 epoch 78/100. Took 0.42255 seconds. 
  Full-batch training loss = 0.053276, test loss = 0.298035
  Training set accuracy = 0.987900, Test set accuracy = 0.923300
 epoch 79/100. Took 0.40276 seconds. 
  Full-batch training loss = 0.052592, test loss = 0.300236
  Training set accuracy = 0.988700, Test set accuracy = 0.921200
 epoch 80/100. Took 0.43315 seconds. 
  Full-batch training loss = 0.050954, test loss = 0.298739
  Training set accuracy = 0.989800, Test set accuracy = 0.921900
 epoch 81/100. Took 0.4254 seconds. 
  Full-batch training loss = 0.050196, test loss = 0.299489
  Training set accuracy = 0.989400, Test set accuracy = 0.921300
 epoch 82/100. Took 0.43851 seconds. 
  Full-batch training loss = 0.049090, test loss = 0.298585
  Training set accuracy = 0.990100, Test set accuracy = 0.922000
 epoch 83/100. Took 0.49469 seconds. 
  Full-batch training loss = 0.048693, test loss = 0.299550
  Training set accuracy = 0.989700, Test set accuracy = 0.922500
 epoch 84/100. Took 0.41828 seconds. 
  Full-batch training loss = 0.048880, test loss = 0.301730
  Training set accuracy = 0.990200, Test set accuracy = 0.920800
 epoch 85/100. Took 0.44256 seconds. 
  Full-batch training loss = 0.046826, test loss = 0.298908
  Training set accuracy = 0.990900, Test set accuracy = 0.922800
 epoch 86/100. Took 0.42677 seconds. 
  Full-batch training loss = 0.046576, test loss = 0.302645
  Training set accuracy = 0.990800, Test set accuracy = 0.921100
 epoch 87/100. Took 0.45388 seconds. 
  Full-batch training loss = 0.046281, test loss = 0.304371
  Training set accuracy = 0.990600, Test set accuracy = 0.921900
 epoch 88/100. Took 0.46553 seconds. 
  Full-batch training loss = 0.044934, test loss = 0.303498
  Training set accuracy = 0.991500, Test set accuracy = 0.922000
 epoch 89/100. Took 0.42795 seconds. 
  Full-batch training loss = 0.044500, test loss = 0.304128
  Training set accuracy = 0.991500, Test set accuracy = 0.922200
 epoch 90/100. Took 0.45556 seconds. 
  Full-batch training loss = 0.043806, test loss = 0.304315
  Training set accuracy = 0.991600, Test set accuracy = 0.922000
 epoch 91/100. Took 0.47667 seconds. 
  Full-batch training loss = 0.044791, test loss = 0.307779
  Training set accuracy = 0.991500, Test set accuracy = 0.921300
 epoch 92/100. Took 0.43504 seconds. 
  Full-batch training loss = 0.043066, test loss = 0.305435
  Training set accuracy = 0.991900, Test set accuracy = 0.921900
 epoch 93/100. Took 0.41688 seconds. 
  Full-batch training loss = 0.042144, test loss = 0.305640
  Training set accuracy = 0.992000, Test set accuracy = 0.923200
 epoch 94/100. Took 0.48174 seconds. 
  Full-batch training loss = 0.042452, test loss = 0.311423
  Training set accuracy = 0.992200, Test set accuracy = 0.920700
 epoch 95/100. Took 0.40198 seconds. 
  Full-batch training loss = 0.041439, test loss = 0.307812
  Training set accuracy = 0.992200, Test set accuracy = 0.922500
 epoch 96/100. Took 0.42504 seconds. 
  Full-batch training loss = 0.040632, test loss = 0.308870
  Training set accuracy = 0.992300, Test set accuracy = 0.921700
 epoch 97/100. Took 0.46497 seconds. 
  Full-batch training loss = 0.041291, test loss = 0.310900
  Training set accuracy = 0.991500, Test set accuracy = 0.922000
 epoch 98/100. Took 0.48211 seconds. 
  Full-batch training loss = 0.040089, test loss = 0.311876
  Training set accuracy = 0.992900, Test set accuracy = 0.923200
 epoch 99/100. Took 0.44967 seconds. 
  Full-batch training loss = 0.038786, test loss = 0.312386
  Training set accuracy = 0.993400, Test set accuracy = 0.922000
 epoch 100/100. Took 0.48919 seconds. 
  Full-batch training loss = 0.038095, test loss = 0.311255
  Training set accuracy = 0.993700, Test set accuracy = 0.922000
Elapsed time is 108.903311 seconds.
End Training
