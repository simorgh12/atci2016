Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (50  50)
Training binary-binary RBM in layer 1 (784   50) with CD1 for 20 epochs
 epoch 1/20. Took 0.74143 seconds. Average reconstruction error is: 40.2293
 epoch 2/20. Took 0.6685 seconds. Average reconstruction error is: 27.8971
 epoch 3/20. Took 0.63524 seconds. Average reconstruction error is: 25.9054
 epoch 4/20. Took 0.64422 seconds. Average reconstruction error is: 25.1109
 epoch 5/20. Took 0.64025 seconds. Average reconstruction error is: 24.6596
 epoch 6/20. Took 0.67265 seconds. Average reconstruction error is: 24.3476
 epoch 7/20. Took 0.66397 seconds. Average reconstruction error is: 24.2311
 epoch 8/20. Took 0.63711 seconds. Average reconstruction error is: 24.093
 epoch 9/20. Took 0.68181 seconds. Average reconstruction error is: 23.7217
 epoch 10/20. Took 0.64527 seconds. Average reconstruction error is: 23.4978
 epoch 11/20. Took 0.6994 seconds. Average reconstruction error is: 23.3632
 epoch 12/20. Took 0.63745 seconds. Average reconstruction error is: 23.314
 epoch 13/20. Took 0.71167 seconds. Average reconstruction error is: 23.2528
 epoch 14/20. Took 0.66849 seconds. Average reconstruction error is: 23.2518
 epoch 15/20. Took 0.66818 seconds. Average reconstruction error is: 23.2146
 epoch 16/20. Took 0.6949 seconds. Average reconstruction error is: 23.1711
 epoch 17/20. Took 0.63646 seconds. Average reconstruction error is: 23.1328
 epoch 18/20. Took 0.63023 seconds. Average reconstruction error is: 23.0871
 epoch 19/20. Took 0.66368 seconds. Average reconstruction error is: 23.031
 epoch 20/20. Took 0.66293 seconds. Average reconstruction error is: 22.8913
Training binary-binary RBM in layer 2 (50  50) with CD1 for 20 epochs
 epoch 1/20. Took 0.10586 seconds. Average reconstruction error is: 3.538
 epoch 2/20. Took 0.10692 seconds. Average reconstruction error is: 1.9487
 epoch 3/20. Took 0.096153 seconds. Average reconstruction error is: 1.5918
 epoch 4/20. Took 0.096387 seconds. Average reconstruction error is: 1.426
 epoch 5/20. Took 0.096861 seconds. Average reconstruction error is: 1.3166
 epoch 6/20. Took 0.099103 seconds. Average reconstruction error is: 1.2504
 epoch 7/20. Took 0.094261 seconds. Average reconstruction error is: 1.1991
 epoch 8/20. Took 0.097717 seconds. Average reconstruction error is: 1.1507
 epoch 9/20. Took 0.095794 seconds. Average reconstruction error is: 1.1321
 epoch 10/20. Took 0.10519 seconds. Average reconstruction error is: 1.0992
 epoch 11/20. Took 0.094281 seconds. Average reconstruction error is: 1.0724
 epoch 12/20. Took 0.10104 seconds. Average reconstruction error is: 1.0459
 epoch 13/20. Took 0.10177 seconds. Average reconstruction error is: 1.0444
 epoch 14/20. Took 0.11143 seconds. Average reconstruction error is: 1.0226
 epoch 15/20. Took 0.09778 seconds. Average reconstruction error is: 1.0038
 epoch 16/20. Took 0.094457 seconds. Average reconstruction error is: 0.99429
 epoch 17/20. Took 0.10447 seconds. Average reconstruction error is: 0.98125
 epoch 18/20. Took 0.095844 seconds. Average reconstruction error is: 0.97878
 epoch 19/20. Took 0.1213 seconds. Average reconstruction error is: 0.97976
 epoch 20/20. Took 0.099928 seconds. Average reconstruction error is: 0.96915
Training NN  (784   50   50   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.41027 seconds. 
  Full-batch training loss = 0.470473, test loss = 0.475900
  Training set accuracy = 0.861800, Test set accuracy = 0.863500
 epoch 2/100. Took 0.44859 seconds. 
  Full-batch training loss = 0.403858, test loss = 0.420597
  Training set accuracy = 0.877500, Test set accuracy = 0.873300
 epoch 3/100. Took 0.39745 seconds. 
  Full-batch training loss = 0.367128, test loss = 0.396803
  Training set accuracy = 0.886100, Test set accuracy = 0.881800
 epoch 4/100. Took 0.4397 seconds. 
  Full-batch training loss = 0.333908, test loss = 0.371521
  Training set accuracy = 0.897700, Test set accuracy = 0.889200
 epoch 5/100. Took 0.40496 seconds. 
  Full-batch training loss = 0.322304, test loss = 0.371508
  Training set accuracy = 0.897900, Test set accuracy = 0.887300
 epoch 6/100. Took 0.47304 seconds. 
  Full-batch training loss = 0.301486, test loss = 0.357065
  Training set accuracy = 0.906900, Test set accuracy = 0.893800
 epoch 7/100. Took 0.41563 seconds. 
  Full-batch training loss = 0.282416, test loss = 0.342638
  Training set accuracy = 0.912900, Test set accuracy = 0.899000
 epoch 8/100. Took 0.40949 seconds. 
  Full-batch training loss = 0.266395, test loss = 0.334583
  Training set accuracy = 0.918000, Test set accuracy = 0.901200
 epoch 9/100. Took 0.3951 seconds. 
  Full-batch training loss = 0.260587, test loss = 0.337887
  Training set accuracy = 0.921500, Test set accuracy = 0.900900
 epoch 10/100. Took 0.40575 seconds. 
  Full-batch training loss = 0.245618, test loss = 0.325461
  Training set accuracy = 0.925600, Test set accuracy = 0.902900
 epoch 11/100. Took 0.39983 seconds. 
  Full-batch training loss = 0.235410, test loss = 0.322831
  Training set accuracy = 0.926300, Test set accuracy = 0.905500
 epoch 12/100. Took 0.52152 seconds. 
  Full-batch training loss = 0.229082, test loss = 0.322668
  Training set accuracy = 0.929500, Test set accuracy = 0.905700
 epoch 13/100. Took 0.39929 seconds. 
  Full-batch training loss = 0.219571, test loss = 0.316194
  Training set accuracy = 0.932600, Test set accuracy = 0.906800
 epoch 14/100. Took 0.43594 seconds. 
  Full-batch training loss = 0.215337, test loss = 0.318492
  Training set accuracy = 0.934400, Test set accuracy = 0.907100
 epoch 15/100. Took 0.42126 seconds. 
  Full-batch training loss = 0.212009, test loss = 0.317704
  Training set accuracy = 0.933200, Test set accuracy = 0.906000
 epoch 16/100. Took 0.4319 seconds. 
  Full-batch training loss = 0.199533, test loss = 0.312134
  Training set accuracy = 0.938500, Test set accuracy = 0.908900
 epoch 17/100. Took 0.43467 seconds. 
  Full-batch training loss = 0.194058, test loss = 0.310735
  Training set accuracy = 0.942300, Test set accuracy = 0.909400
 epoch 18/100. Took 0.41768 seconds. 
  Full-batch training loss = 0.186645, test loss = 0.306039
  Training set accuracy = 0.944800, Test set accuracy = 0.910000
 epoch 19/100. Took 0.41293 seconds. 
  Full-batch training loss = 0.179324, test loss = 0.304927
  Training set accuracy = 0.946500, Test set accuracy = 0.910400
 epoch 20/100. Took 0.39881 seconds. 
  Full-batch training loss = 0.176407, test loss = 0.306274
  Training set accuracy = 0.947200, Test set accuracy = 0.910500
 epoch 21/100. Took 0.41485 seconds. 
  Full-batch training loss = 0.170725, test loss = 0.303364
  Training set accuracy = 0.949400, Test set accuracy = 0.910700
 epoch 22/100. Took 0.43262 seconds. 
  Full-batch training loss = 0.163937, test loss = 0.300071
  Training set accuracy = 0.952100, Test set accuracy = 0.913600
 epoch 23/100. Took 0.41325 seconds. 
  Full-batch training loss = 0.161113, test loss = 0.300584
  Training set accuracy = 0.953700, Test set accuracy = 0.913300
 epoch 24/100. Took 0.45567 seconds. 
  Full-batch training loss = 0.155297, test loss = 0.298126
  Training set accuracy = 0.954500, Test set accuracy = 0.913600
 epoch 25/100. Took 0.40131 seconds. 
  Full-batch training loss = 0.151549, test loss = 0.299794
  Training set accuracy = 0.956800, Test set accuracy = 0.912500
 epoch 26/100. Took 0.41942 seconds. 
  Full-batch training loss = 0.145627, test loss = 0.297725
  Training set accuracy = 0.957700, Test set accuracy = 0.914400
 epoch 27/100. Took 0.39644 seconds. 
  Full-batch training loss = 0.143709, test loss = 0.298926
  Training set accuracy = 0.958700, Test set accuracy = 0.916200
 epoch 28/100. Took 0.39764 seconds. 
  Full-batch training loss = 0.138587, test loss = 0.296725
  Training set accuracy = 0.961700, Test set accuracy = 0.916700
 epoch 29/100. Took 0.42814 seconds. 
  Full-batch training loss = 0.136571, test loss = 0.297728
  Training set accuracy = 0.959900, Test set accuracy = 0.915700
 epoch 30/100. Took 0.43078 seconds. 
  Full-batch training loss = 0.132002, test loss = 0.297762
  Training set accuracy = 0.963700, Test set accuracy = 0.917400
 epoch 31/100. Took 0.39618 seconds. 
  Full-batch training loss = 0.128458, test loss = 0.296254
  Training set accuracy = 0.963500, Test set accuracy = 0.916000
 epoch 32/100. Took 0.45746 seconds. 
  Full-batch training loss = 0.126409, test loss = 0.297445
  Training set accuracy = 0.965200, Test set accuracy = 0.916700
 epoch 33/100. Took 0.41712 seconds. 
  Full-batch training loss = 0.126859, test loss = 0.302084
  Training set accuracy = 0.964200, Test set accuracy = 0.915800
 epoch 34/100. Took 0.46178 seconds. 
  Full-batch training loss = 0.120881, test loss = 0.298408
  Training set accuracy = 0.966400, Test set accuracy = 0.915000
 epoch 35/100. Took 0.4386 seconds. 
  Full-batch training loss = 0.117022, test loss = 0.296858
  Training set accuracy = 0.967100, Test set accuracy = 0.916700
 epoch 36/100. Took 0.40195 seconds. 
  Full-batch training loss = 0.115263, test loss = 0.297204
  Training set accuracy = 0.968300, Test set accuracy = 0.916100
 epoch 37/100. Took 0.48801 seconds. 
  Full-batch training loss = 0.112957, test loss = 0.299434
  Training set accuracy = 0.969300, Test set accuracy = 0.917700
 epoch 38/100. Took 0.40917 seconds. 
  Full-batch training loss = 0.112235, test loss = 0.301678
  Training set accuracy = 0.970400, Test set accuracy = 0.918000
 epoch 39/100. Took 0.44467 seconds. 
  Full-batch training loss = 0.107921, test loss = 0.297492
  Training set accuracy = 0.971300, Test set accuracy = 0.918800
 epoch 40/100. Took 0.42113 seconds. 
  Full-batch training loss = 0.105964, test loss = 0.297776
  Training set accuracy = 0.972200, Test set accuracy = 0.920300
 epoch 41/100. Took 0.39554 seconds. 
  Full-batch training loss = 0.102803, test loss = 0.297561
  Training set accuracy = 0.973100, Test set accuracy = 0.916700
 epoch 42/100. Took 0.42939 seconds. 
  Full-batch training loss = 0.099916, test loss = 0.296340
  Training set accuracy = 0.974300, Test set accuracy = 0.919000
 epoch 43/100. Took 0.43726 seconds. 
  Full-batch training loss = 0.098499, test loss = 0.298533
  Training set accuracy = 0.975000, Test set accuracy = 0.918600
 epoch 44/100. Took 0.40526 seconds. 
  Full-batch training loss = 0.098200, test loss = 0.301438
  Training set accuracy = 0.974100, Test set accuracy = 0.917500
 epoch 45/100. Took 0.40919 seconds. 
  Full-batch training loss = 0.094717, test loss = 0.299514
  Training set accuracy = 0.975500, Test set accuracy = 0.920400
 epoch 46/100. Took 0.45137 seconds. 
  Full-batch training loss = 0.094171, test loss = 0.301790
  Training set accuracy = 0.975500, Test set accuracy = 0.918900
 epoch 47/100. Took 0.45754 seconds. 
  Full-batch training loss = 0.094165, test loss = 0.304853
  Training set accuracy = 0.975900, Test set accuracy = 0.917200
 epoch 48/100. Took 0.41502 seconds. 
  Full-batch training loss = 0.090802, test loss = 0.303877
  Training set accuracy = 0.977300, Test set accuracy = 0.918500
 epoch 49/100. Took 0.42782 seconds. 
  Full-batch training loss = 0.088899, test loss = 0.304275
  Training set accuracy = 0.976800, Test set accuracy = 0.918600
 epoch 50/100. Took 0.42319 seconds. 
  Full-batch training loss = 0.086892, test loss = 0.302094
  Training set accuracy = 0.978500, Test set accuracy = 0.919300
 epoch 51/100. Took 0.40235 seconds. 
  Full-batch training loss = 0.083322, test loss = 0.301853
  Training set accuracy = 0.979100, Test set accuracy = 0.919800
 epoch 52/100. Took 0.40325 seconds. 
  Full-batch training loss = 0.083018, test loss = 0.303032
  Training set accuracy = 0.979500, Test set accuracy = 0.919800
 epoch 53/100. Took 0.44519 seconds. 
  Full-batch training loss = 0.080756, test loss = 0.302914
  Training set accuracy = 0.980600, Test set accuracy = 0.918500
 epoch 54/100. Took 0.47392 seconds. 
  Full-batch training loss = 0.078746, test loss = 0.302528
  Training set accuracy = 0.980400, Test set accuracy = 0.918600
 epoch 55/100. Took 0.40817 seconds. 
  Full-batch training loss = 0.077331, test loss = 0.303674
  Training set accuracy = 0.981700, Test set accuracy = 0.919000
 epoch 56/100. Took 0.42444 seconds. 
  Full-batch training loss = 0.075712, test loss = 0.305951
  Training set accuracy = 0.981100, Test set accuracy = 0.918900
 epoch 57/100. Took 0.40688 seconds. 
  Full-batch training loss = 0.074739, test loss = 0.306026
  Training set accuracy = 0.981700, Test set accuracy = 0.920200
 epoch 58/100. Took 0.39669 seconds. 
  Full-batch training loss = 0.074293, test loss = 0.307988
  Training set accuracy = 0.981700, Test set accuracy = 0.918700
 epoch 59/100. Took 0.44943 seconds. 
  Full-batch training loss = 0.071643, test loss = 0.306832
  Training set accuracy = 0.982100, Test set accuracy = 0.920000
 epoch 60/100. Took 0.41564 seconds. 
  Full-batch training loss = 0.070818, test loss = 0.307867
  Training set accuracy = 0.982700, Test set accuracy = 0.919000
 epoch 61/100. Took 0.43126 seconds. 
  Full-batch training loss = 0.069228, test loss = 0.308941
  Training set accuracy = 0.983500, Test set accuracy = 0.920300
 epoch 62/100. Took 0.409 seconds. 
  Full-batch training loss = 0.067618, test loss = 0.309023
  Training set accuracy = 0.984300, Test set accuracy = 0.920000
 epoch 63/100. Took 0.41347 seconds. 
  Full-batch training loss = 0.068790, test loss = 0.311274
  Training set accuracy = 0.983400, Test set accuracy = 0.917400
 epoch 64/100. Took 0.42977 seconds. 
  Full-batch training loss = 0.065176, test loss = 0.309964
  Training set accuracy = 0.985100, Test set accuracy = 0.920300
 epoch 65/100. Took 0.40603 seconds. 
  Full-batch training loss = 0.063544, test loss = 0.310232
  Training set accuracy = 0.986100, Test set accuracy = 0.919500
 epoch 66/100. Took 0.39786 seconds. 
  Full-batch training loss = 0.063401, test loss = 0.311621
  Training set accuracy = 0.985200, Test set accuracy = 0.921000
 epoch 67/100. Took 0.44398 seconds. 
  Full-batch training loss = 0.062197, test loss = 0.312228
  Training set accuracy = 0.985600, Test set accuracy = 0.920000
 epoch 68/100. Took 0.41971 seconds. 
  Full-batch training loss = 0.060326, test loss = 0.311573
  Training set accuracy = 0.986300, Test set accuracy = 0.920200
 epoch 69/100. Took 0.43965 seconds. 
  Full-batch training loss = 0.059706, test loss = 0.313411
  Training set accuracy = 0.986200, Test set accuracy = 0.920200
 epoch 70/100. Took 0.41759 seconds. 
  Full-batch training loss = 0.058214, test loss = 0.314491
  Training set accuracy = 0.987100, Test set accuracy = 0.921500
 epoch 71/100. Took 0.3992 seconds. 
  Full-batch training loss = 0.057180, test loss = 0.313294
  Training set accuracy = 0.987400, Test set accuracy = 0.920500
 epoch 72/100. Took 0.42469 seconds. 
  Full-batch training loss = 0.056525, test loss = 0.313692
  Training set accuracy = 0.987300, Test set accuracy = 0.920200
 epoch 73/100. Took 0.41167 seconds. 
  Full-batch training loss = 0.055310, test loss = 0.316068
  Training set accuracy = 0.987200, Test set accuracy = 0.919800
 epoch 74/100. Took 0.43379 seconds. 
  Full-batch training loss = 0.054507, test loss = 0.316366
  Training set accuracy = 0.987200, Test set accuracy = 0.919900
 epoch 75/100. Took 0.40591 seconds. 
  Full-batch training loss = 0.054622, test loss = 0.317939
  Training set accuracy = 0.987100, Test set accuracy = 0.918000
 epoch 76/100. Took 0.42537 seconds. 
  Full-batch training loss = 0.053526, test loss = 0.319902
  Training set accuracy = 0.987700, Test set accuracy = 0.919800
 epoch 77/100. Took 0.40775 seconds. 
  Full-batch training loss = 0.051816, test loss = 0.319388
  Training set accuracy = 0.988700, Test set accuracy = 0.919300
 epoch 78/100. Took 0.40031 seconds. 
  Full-batch training loss = 0.050905, test loss = 0.318178
  Training set accuracy = 0.988900, Test set accuracy = 0.920300
 epoch 79/100. Took 0.41465 seconds. 
  Full-batch training loss = 0.050089, test loss = 0.319713
  Training set accuracy = 0.989100, Test set accuracy = 0.919400
 epoch 80/100. Took 0.45009 seconds. 
  Full-batch training loss = 0.049646, test loss = 0.322104
  Training set accuracy = 0.988600, Test set accuracy = 0.920300
 epoch 81/100. Took 0.40925 seconds. 
  Full-batch training loss = 0.049569, test loss = 0.322451
  Training set accuracy = 0.989100, Test set accuracy = 0.920400
 epoch 82/100. Took 0.40039 seconds. 
  Full-batch training loss = 0.048217, test loss = 0.321586
  Training set accuracy = 0.989200, Test set accuracy = 0.919000
 epoch 83/100. Took 0.45921 seconds. 
  Full-batch training loss = 0.047562, test loss = 0.323226
  Training set accuracy = 0.989600, Test set accuracy = 0.920900
 epoch 84/100. Took 0.41948 seconds. 
  Full-batch training loss = 0.047333, test loss = 0.325309
  Training set accuracy = 0.989000, Test set accuracy = 0.919900
 epoch 85/100. Took 0.40638 seconds. 
  Full-batch training loss = 0.045745, test loss = 0.324545
  Training set accuracy = 0.990000, Test set accuracy = 0.920900
 epoch 86/100. Took 0.39353 seconds. 
  Full-batch training loss = 0.045017, test loss = 0.325458
  Training set accuracy = 0.989800, Test set accuracy = 0.921700
 epoch 87/100. Took 0.40035 seconds. 
  Full-batch training loss = 0.043908, test loss = 0.325380
  Training set accuracy = 0.990700, Test set accuracy = 0.921500
 epoch 88/100. Took 0.45028 seconds. 
  Full-batch training loss = 0.043522, test loss = 0.327056
  Training set accuracy = 0.990700, Test set accuracy = 0.920300
 epoch 89/100. Took 0.40153 seconds. 
  Full-batch training loss = 0.042821, test loss = 0.328281
  Training set accuracy = 0.990800, Test set accuracy = 0.920600
 epoch 90/100. Took 0.3935 seconds. 
  Full-batch training loss = 0.042286, test loss = 0.329160
  Training set accuracy = 0.991300, Test set accuracy = 0.919400
 epoch 91/100. Took 0.41853 seconds. 
  Full-batch training loss = 0.041240, test loss = 0.328951
  Training set accuracy = 0.991500, Test set accuracy = 0.921100
 epoch 92/100. Took 0.4594 seconds. 
  Full-batch training loss = 0.041620, test loss = 0.330808
  Training set accuracy = 0.991000, Test set accuracy = 0.921900
 epoch 93/100. Took 0.44749 seconds. 
  Full-batch training loss = 0.040004, test loss = 0.331048
  Training set accuracy = 0.991300, Test set accuracy = 0.921200
 epoch 94/100. Took 0.44977 seconds. 
  Full-batch training loss = 0.040265, test loss = 0.331734
  Training set accuracy = 0.991700, Test set accuracy = 0.919400
 epoch 95/100. Took 0.41351 seconds. 
  Full-batch training loss = 0.039054, test loss = 0.333038
  Training set accuracy = 0.991700, Test set accuracy = 0.921100
 epoch 96/100. Took 0.41344 seconds. 
  Full-batch training loss = 0.038998, test loss = 0.333374
  Training set accuracy = 0.991800, Test set accuracy = 0.921400
 epoch 97/100. Took 0.42067 seconds. 
  Full-batch training loss = 0.038043, test loss = 0.334418
  Training set accuracy = 0.992000, Test set accuracy = 0.919500
 epoch 98/100. Took 0.41691 seconds. 
  Full-batch training loss = 0.037448, test loss = 0.336141
  Training set accuracy = 0.992200, Test set accuracy = 0.920300
 epoch 99/100. Took 0.40148 seconds. 
  Full-batch training loss = 0.037008, test loss = 0.336039
  Training set accuracy = 0.992100, Test set accuracy = 0.920200
 epoch 100/100. Took 0.40399 seconds. 
  Full-batch training loss = 0.037668, test loss = 0.339426
  Training set accuracy = 0.992200, Test set accuracy = 0.919800
Elapsed time is 105.505766 seconds.
End Training
