Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.10
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.58293 seconds. Average reconstruction error is: 30.9298
 epoch 2/20. Took 0.5643 seconds. Average reconstruction error is: 18.5271
 epoch 3/20. Took 0.56549 seconds. Average reconstruction error is: 16.0593
 epoch 4/20. Took 0.56551 seconds. Average reconstruction error is: 14.5826
 epoch 5/20. Took 0.57704 seconds. Average reconstruction error is: 13.4955
 epoch 6/20. Took 0.5877 seconds. Average reconstruction error is: 12.6615
 epoch 7/20. Took 0.58914 seconds. Average reconstruction error is: 12.0822
 epoch 8/20. Took 0.58911 seconds. Average reconstruction error is: 11.6507
 epoch 9/20. Took 0.588 seconds. Average reconstruction error is: 11.2887
 epoch 10/20. Took 0.58656 seconds. Average reconstruction error is: 11.0279
 epoch 11/20. Took 0.59229 seconds. Average reconstruction error is: 10.7267
 epoch 12/20. Took 0.59208 seconds. Average reconstruction error is: 10.5303
 epoch 13/20. Took 0.59193 seconds. Average reconstruction error is: 10.3432
 epoch 14/20. Took 0.59192 seconds. Average reconstruction error is: 10.2049
 epoch 15/20. Took 0.59009 seconds. Average reconstruction error is: 10.0481
 epoch 16/20. Took 0.58817 seconds. Average reconstruction error is: 9.9335
 epoch 17/20. Took 0.57752 seconds. Average reconstruction error is: 9.8384
 epoch 18/20. Took 0.58601 seconds. Average reconstruction error is: 9.7443
 epoch 19/20. Took 0.59084 seconds. Average reconstruction error is: 9.6151
 epoch 20/20. Took 0.58815 seconds. Average reconstruction error is: 9.5862
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20204 seconds. Average reconstruction error is: 14.127
 epoch 2/20. Took 0.20522 seconds. Average reconstruction error is: 8.4016
 epoch 3/20. Took 0.20608 seconds. Average reconstruction error is: 7.3766
 epoch 4/20. Took 0.20571 seconds. Average reconstruction error is: 6.9265
 epoch 5/20. Took 0.20457 seconds. Average reconstruction error is: 6.5816
 epoch 6/20. Took 0.2051 seconds. Average reconstruction error is: 6.3416
 epoch 7/20. Took 0.20566 seconds. Average reconstruction error is: 6.1897
 epoch 8/20. Took 0.20519 seconds. Average reconstruction error is: 6.0378
 epoch 9/20. Took 0.20469 seconds. Average reconstruction error is: 5.8849
 epoch 10/20. Took 0.20534 seconds. Average reconstruction error is: 5.7995
 epoch 11/20. Took 0.20651 seconds. Average reconstruction error is: 5.6938
 epoch 12/20. Took 0.2068 seconds. Average reconstruction error is: 5.6058
 epoch 13/20. Took 0.20478 seconds. Average reconstruction error is: 5.5392
 epoch 14/20. Took 0.20531 seconds. Average reconstruction error is: 5.4544
 epoch 15/20. Took 0.20601 seconds. Average reconstruction error is: 5.3836
 epoch 16/20. Took 0.20516 seconds. Average reconstruction error is: 5.3787
 epoch 17/20. Took 0.20658 seconds. Average reconstruction error is: 5.2976
 epoch 18/20. Took 0.20524 seconds. Average reconstruction error is: 5.2458
 epoch 19/20. Took 0.20532 seconds. Average reconstruction error is: 5.188
 epoch 20/20. Took 0.20504 seconds. Average reconstruction error is: 5.152
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20122 seconds. Average reconstruction error is: 11.7333
 epoch 2/20. Took 0.20268 seconds. Average reconstruction error is: 6.8476
 epoch 3/20. Took 0.20335 seconds. Average reconstruction error is: 6.1436
 epoch 4/20. Took 0.20316 seconds. Average reconstruction error is: 5.8399
 epoch 5/20. Took 0.20571 seconds. Average reconstruction error is: 5.6187
 epoch 6/20. Took 0.20436 seconds. Average reconstruction error is: 5.4293
 epoch 7/20. Took 0.20406 seconds. Average reconstruction error is: 5.3333
 epoch 8/20. Took 0.20385 seconds. Average reconstruction error is: 5.2362
 epoch 9/20. Took 0.20434 seconds. Average reconstruction error is: 5.1333
 epoch 10/20. Took 0.20464 seconds. Average reconstruction error is: 5.0654
 epoch 11/20. Took 0.2038 seconds. Average reconstruction error is: 5.0169
 epoch 12/20. Took 0.20454 seconds. Average reconstruction error is: 4.9573
 epoch 13/20. Took 0.20571 seconds. Average reconstruction error is: 4.8916
 epoch 14/20. Took 0.20572 seconds. Average reconstruction error is: 4.8588
 epoch 15/20. Took 0.20514 seconds. Average reconstruction error is: 4.7829
 epoch 16/20. Took 0.2052 seconds. Average reconstruction error is: 4.7412
 epoch 17/20. Took 0.20463 seconds. Average reconstruction error is: 4.732
 epoch 18/20. Took 0.20391 seconds. Average reconstruction error is: 4.6642
 epoch 19/20. Took 0.20495 seconds. Average reconstruction error is: 4.6323
 epoch 20/20. Took 0.20503 seconds. Average reconstruction error is: 4.6076
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.56821 seconds. 
  Full-batch training loss = 0.339731, test loss = 0.341977
  Training set accuracy = 0.908300, Test set accuracy = 0.907000
 epoch 2/100. Took 0.57001 seconds. 
  Full-batch training loss = 0.271472, test loss = 0.285764
  Training set accuracy = 0.925300, Test set accuracy = 0.919600
 epoch 3/100. Took 0.57629 seconds. 
  Full-batch training loss = 0.233861, test loss = 0.257712
  Training set accuracy = 0.935600, Test set accuracy = 0.927700
 epoch 4/100. Took 0.57509 seconds. 
  Full-batch training loss = 0.211479, test loss = 0.240833
  Training set accuracy = 0.939700, Test set accuracy = 0.929000
 epoch 5/100. Took 0.56906 seconds. 
  Full-batch training loss = 0.191777, test loss = 0.229939
  Training set accuracy = 0.946800, Test set accuracy = 0.934400
 epoch 6/100. Took 0.57324 seconds. 
  Full-batch training loss = 0.175938, test loss = 0.217119
  Training set accuracy = 0.950000, Test set accuracy = 0.936400
 epoch 7/100. Took 0.57452 seconds. 
  Full-batch training loss = 0.163330, test loss = 0.210290
  Training set accuracy = 0.955300, Test set accuracy = 0.940300
 epoch 8/100. Took 0.57537 seconds. 
  Full-batch training loss = 0.153970, test loss = 0.206786
  Training set accuracy = 0.958600, Test set accuracy = 0.939500
 epoch 9/100. Took 0.56985 seconds. 
  Full-batch training loss = 0.141127, test loss = 0.199515
  Training set accuracy = 0.961800, Test set accuracy = 0.941100
 epoch 10/100. Took 0.5778 seconds. 
  Full-batch training loss = 0.134165, test loss = 0.196670
  Training set accuracy = 0.963700, Test set accuracy = 0.943000
 epoch 11/100. Took 0.57244 seconds. 
  Full-batch training loss = 0.125165, test loss = 0.192500
  Training set accuracy = 0.965900, Test set accuracy = 0.942800
 epoch 12/100. Took 0.57353 seconds. 
  Full-batch training loss = 0.119174, test loss = 0.190800
  Training set accuracy = 0.969200, Test set accuracy = 0.942200
 epoch 13/100. Took 0.57071 seconds. 
  Full-batch training loss = 0.109460, test loss = 0.184599
  Training set accuracy = 0.971800, Test set accuracy = 0.945700
 epoch 14/100. Took 0.57119 seconds. 
  Full-batch training loss = 0.103502, test loss = 0.182618
  Training set accuracy = 0.974400, Test set accuracy = 0.946500
 epoch 15/100. Took 0.57592 seconds. 
  Full-batch training loss = 0.099186, test loss = 0.182227
  Training set accuracy = 0.975100, Test set accuracy = 0.946600
 epoch 16/100. Took 0.5783 seconds. 
  Full-batch training loss = 0.093939, test loss = 0.180377
  Training set accuracy = 0.977100, Test set accuracy = 0.946300
 epoch 17/100. Took 0.57685 seconds. 
  Full-batch training loss = 0.087786, test loss = 0.177267
  Training set accuracy = 0.979200, Test set accuracy = 0.947400
 epoch 18/100. Took 0.57207 seconds. 
  Full-batch training loss = 0.084508, test loss = 0.176930
  Training set accuracy = 0.979600, Test set accuracy = 0.947600
 epoch 19/100. Took 0.57678 seconds. 
  Full-batch training loss = 0.079784, test loss = 0.175950
  Training set accuracy = 0.982600, Test set accuracy = 0.947300
 epoch 20/100. Took 0.57249 seconds. 
  Full-batch training loss = 0.074852, test loss = 0.173174
  Training set accuracy = 0.984300, Test set accuracy = 0.949100
 epoch 21/100. Took 0.5722 seconds. 
  Full-batch training loss = 0.071609, test loss = 0.172859
  Training set accuracy = 0.985000, Test set accuracy = 0.949000
 epoch 22/100. Took 0.57304 seconds. 
  Full-batch training loss = 0.068772, test loss = 0.173656
  Training set accuracy = 0.985400, Test set accuracy = 0.949200
 epoch 23/100. Took 0.57061 seconds. 
  Full-batch training loss = 0.064460, test loss = 0.171591
  Training set accuracy = 0.987000, Test set accuracy = 0.949200
 epoch 24/100. Took 0.56822 seconds. 
  Full-batch training loss = 0.061872, test loss = 0.171870
  Training set accuracy = 0.988000, Test set accuracy = 0.949500
 epoch 25/100. Took 0.56981 seconds. 
  Full-batch training loss = 0.058702, test loss = 0.170488
  Training set accuracy = 0.988500, Test set accuracy = 0.950000
 epoch 26/100. Took 0.57618 seconds. 
  Full-batch training loss = 0.056568, test loss = 0.171024
  Training set accuracy = 0.988000, Test set accuracy = 0.950500
 epoch 27/100. Took 0.56981 seconds. 
  Full-batch training loss = 0.052842, test loss = 0.169446
  Training set accuracy = 0.989100, Test set accuracy = 0.950400
 epoch 28/100. Took 0.57195 seconds. 
  Full-batch training loss = 0.051305, test loss = 0.168084
  Training set accuracy = 0.989700, Test set accuracy = 0.950900
 epoch 29/100. Took 0.57362 seconds. 
  Full-batch training loss = 0.048427, test loss = 0.168033
  Training set accuracy = 0.990800, Test set accuracy = 0.951100
 epoch 30/100. Took 0.57246 seconds. 
  Full-batch training loss = 0.046040, test loss = 0.168660
  Training set accuracy = 0.991500, Test set accuracy = 0.950900
 epoch 31/100. Took 0.57273 seconds. 
  Full-batch training loss = 0.043753, test loss = 0.168088
  Training set accuracy = 0.992000, Test set accuracy = 0.950800
 epoch 32/100. Took 0.56963 seconds. 
  Full-batch training loss = 0.042142, test loss = 0.167439
  Training set accuracy = 0.992700, Test set accuracy = 0.950900
 epoch 33/100. Took 0.57133 seconds. 
  Full-batch training loss = 0.040893, test loss = 0.167783
  Training set accuracy = 0.993400, Test set accuracy = 0.951300
 epoch 34/100. Took 0.57569 seconds. 
  Full-batch training loss = 0.038423, test loss = 0.167968
  Training set accuracy = 0.994200, Test set accuracy = 0.951400
 epoch 35/100. Took 0.57227 seconds. 
  Full-batch training loss = 0.036972, test loss = 0.168493
  Training set accuracy = 0.994600, Test set accuracy = 0.951900
 epoch 36/100. Took 0.57632 seconds. 
  Full-batch training loss = 0.035061, test loss = 0.167392
  Training set accuracy = 0.995000, Test set accuracy = 0.952000
 epoch 37/100. Took 0.57211 seconds. 
  Full-batch training loss = 0.033926, test loss = 0.168373
  Training set accuracy = 0.995400, Test set accuracy = 0.952000
 epoch 38/100. Took 0.56802 seconds. 
  Full-batch training loss = 0.032132, test loss = 0.168403
  Training set accuracy = 0.996400, Test set accuracy = 0.952100
 epoch 39/100. Took 0.57243 seconds. 
  Full-batch training loss = 0.030767, test loss = 0.167800
  Training set accuracy = 0.996000, Test set accuracy = 0.952500
 epoch 40/100. Took 0.57146 seconds. 
  Full-batch training loss = 0.029559, test loss = 0.168602
  Training set accuracy = 0.996500, Test set accuracy = 0.951800
 epoch 41/100. Took 0.57419 seconds. 
  Full-batch training loss = 0.028352, test loss = 0.168271
  Training set accuracy = 0.997000, Test set accuracy = 0.952700
 epoch 42/100. Took 0.56878 seconds. 
  Full-batch training loss = 0.027190, test loss = 0.168150
  Training set accuracy = 0.997000, Test set accuracy = 0.952000
 epoch 43/100. Took 0.57198 seconds. 
  Full-batch training loss = 0.025930, test loss = 0.168155
  Training set accuracy = 0.997200, Test set accuracy = 0.952100
 epoch 44/100. Took 0.5714 seconds. 
  Full-batch training loss = 0.025014, test loss = 0.168402
  Training set accuracy = 0.997400, Test set accuracy = 0.952000
 epoch 45/100. Took 0.59144 seconds. 
  Full-batch training loss = 0.023948, test loss = 0.168235
  Training set accuracy = 0.997600, Test set accuracy = 0.953100
 epoch 46/100. Took 0.5744 seconds. 
  Full-batch training loss = 0.023100, test loss = 0.168851
  Training set accuracy = 0.998000, Test set accuracy = 0.953300
 epoch 47/100. Took 0.56798 seconds. 
  Full-batch training loss = 0.022246, test loss = 0.168291
  Training set accuracy = 0.998100, Test set accuracy = 0.953400
 epoch 48/100. Took 0.5767 seconds. 
  Full-batch training loss = 0.021413, test loss = 0.168783
  Training set accuracy = 0.998200, Test set accuracy = 0.953600
 epoch 49/100. Took 0.57715 seconds. 
  Full-batch training loss = 0.020528, test loss = 0.169687
  Training set accuracy = 0.998200, Test set accuracy = 0.953200
 epoch 50/100. Took 0.57507 seconds. 
  Full-batch training loss = 0.019861, test loss = 0.170283
  Training set accuracy = 0.998200, Test set accuracy = 0.953300
 epoch 51/100. Took 0.57129 seconds. 
  Full-batch training loss = 0.019111, test loss = 0.169491
  Training set accuracy = 0.998400, Test set accuracy = 0.952400
 epoch 52/100. Took 0.57487 seconds. 
  Full-batch training loss = 0.018601, test loss = 0.169889
  Training set accuracy = 0.998300, Test set accuracy = 0.953200
 epoch 53/100. Took 0.57455 seconds. 
  Full-batch training loss = 0.017728, test loss = 0.169750
  Training set accuracy = 0.998500, Test set accuracy = 0.953300
 epoch 54/100. Took 0.57286 seconds. 
  Full-batch training loss = 0.017078, test loss = 0.170501
  Training set accuracy = 0.998400, Test set accuracy = 0.953300
 epoch 55/100. Took 0.56754 seconds. 
  Full-batch training loss = 0.016567, test loss = 0.171045
  Training set accuracy = 0.998600, Test set accuracy = 0.953400
 epoch 56/100. Took 0.5721 seconds. 
  Full-batch training loss = 0.015981, test loss = 0.171061
  Training set accuracy = 0.998700, Test set accuracy = 0.953200
 epoch 57/100. Took 0.57447 seconds. 
  Full-batch training loss = 0.015422, test loss = 0.171049
  Training set accuracy = 0.999000, Test set accuracy = 0.953500
 epoch 58/100. Took 0.57396 seconds. 
  Full-batch training loss = 0.015027, test loss = 0.171016
  Training set accuracy = 0.999200, Test set accuracy = 0.953600
 epoch 59/100. Took 0.57605 seconds. 
  Full-batch training loss = 0.014440, test loss = 0.171319
  Training set accuracy = 0.999200, Test set accuracy = 0.953800
 epoch 60/100. Took 0.57251 seconds. 
  Full-batch training loss = 0.013961, test loss = 0.171613
  Training set accuracy = 0.999200, Test set accuracy = 0.953300
 epoch 61/100. Took 0.57287 seconds. 
  Full-batch training loss = 0.013526, test loss = 0.172133
  Training set accuracy = 0.999300, Test set accuracy = 0.953700
 epoch 62/100. Took 0.57317 seconds. 
  Full-batch training loss = 0.013157, test loss = 0.172741
  Training set accuracy = 0.999400, Test set accuracy = 0.953400
 epoch 63/100. Took 0.57537 seconds. 
  Full-batch training loss = 0.012703, test loss = 0.172832
  Training set accuracy = 0.999500, Test set accuracy = 0.953500
 epoch 64/100. Took 0.57235 seconds. 
  Full-batch training loss = 0.012362, test loss = 0.173016
  Training set accuracy = 0.999500, Test set accuracy = 0.953300
 epoch 65/100. Took 0.57236 seconds. 
  Full-batch training loss = 0.011971, test loss = 0.172711
  Training set accuracy = 0.999500, Test set accuracy = 0.953800
 epoch 66/100. Took 0.56855 seconds. 
  Full-batch training loss = 0.011729, test loss = 0.173191
  Training set accuracy = 0.999500, Test set accuracy = 0.953900
 epoch 67/100. Took 0.57619 seconds. 
  Full-batch training loss = 0.011398, test loss = 0.173238
  Training set accuracy = 0.999500, Test set accuracy = 0.954200
 epoch 68/100. Took 0.5696 seconds. 
  Full-batch training loss = 0.011109, test loss = 0.174418
  Training set accuracy = 0.999700, Test set accuracy = 0.953300
 epoch 69/100. Took 0.56984 seconds. 
  Full-batch training loss = 0.010707, test loss = 0.173686
  Training set accuracy = 0.999600, Test set accuracy = 0.953900
 epoch 70/100. Took 0.57191 seconds. 
  Full-batch training loss = 0.010438, test loss = 0.174530
  Training set accuracy = 0.999600, Test set accuracy = 0.953600
 epoch 71/100. Took 0.5724 seconds. 
  Full-batch training loss = 0.010114, test loss = 0.174275
  Training set accuracy = 0.999800, Test set accuracy = 0.953900
 epoch 72/100. Took 0.56891 seconds. 
  Full-batch training loss = 0.009893, test loss = 0.174254
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 73/100. Took 0.57315 seconds. 
  Full-batch training loss = 0.009612, test loss = 0.174916
  Training set accuracy = 0.999900, Test set accuracy = 0.954000
 epoch 74/100. Took 0.57186 seconds. 
  Full-batch training loss = 0.009342, test loss = 0.175063
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 75/100. Took 0.5721 seconds. 
  Full-batch training loss = 0.009129, test loss = 0.175385
  Training set accuracy = 0.999900, Test set accuracy = 0.954300
 epoch 76/100. Took 0.57354 seconds. 
  Full-batch training loss = 0.008932, test loss = 0.175840
  Training set accuracy = 0.999900, Test set accuracy = 0.954100
 epoch 77/100. Took 0.57276 seconds. 
  Full-batch training loss = 0.008694, test loss = 0.175754
  Training set accuracy = 0.999900, Test set accuracy = 0.954300
 epoch 78/100. Took 0.57612 seconds. 
  Full-batch training loss = 0.008497, test loss = 0.176170
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 79/100. Took 0.57258 seconds. 
  Full-batch training loss = 0.008329, test loss = 0.175963
  Training set accuracy = 1.000000, Test set accuracy = 0.954500
 epoch 80/100. Took 0.57479 seconds. 
  Full-batch training loss = 0.008112, test loss = 0.176038
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 81/100. Took 0.57112 seconds. 
  Full-batch training loss = 0.007914, test loss = 0.176677
  Training set accuracy = 1.000000, Test set accuracy = 0.954200
 epoch 82/100. Took 0.56966 seconds. 
  Full-batch training loss = 0.007746, test loss = 0.176985
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 83/100. Took 0.57135 seconds. 
  Full-batch training loss = 0.007589, test loss = 0.177092
  Training set accuracy = 1.000000, Test set accuracy = 0.954200
 epoch 84/100. Took 0.57171 seconds. 
  Full-batch training loss = 0.007410, test loss = 0.177383
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 85/100. Took 0.57753 seconds. 
  Full-batch training loss = 0.007251, test loss = 0.177918
  Training set accuracy = 1.000000, Test set accuracy = 0.954200
 epoch 86/100. Took 0.573 seconds. 
  Full-batch training loss = 0.007106, test loss = 0.177623
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 87/100. Took 0.56994 seconds. 
  Full-batch training loss = 0.006993, test loss = 0.178130
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 88/100. Took 0.57663 seconds. 
  Full-batch training loss = 0.006817, test loss = 0.178173
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 89/100. Took 0.56732 seconds. 
  Full-batch training loss = 0.006676, test loss = 0.178417
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 90/100. Took 0.57286 seconds. 
  Full-batch training loss = 0.006554, test loss = 0.178413
  Training set accuracy = 1.000000, Test set accuracy = 0.954300
 epoch 91/100. Took 0.57106 seconds. 
  Full-batch training loss = 0.006413, test loss = 0.178497
  Training set accuracy = 1.000000, Test set accuracy = 0.954500
 epoch 92/100. Took 0.57386 seconds. 
  Full-batch training loss = 0.006308, test loss = 0.178642
  Training set accuracy = 1.000000, Test set accuracy = 0.954400
 epoch 93/100. Took 0.57111 seconds. 
  Full-batch training loss = 0.006171, test loss = 0.179164
  Training set accuracy = 1.000000, Test set accuracy = 0.954500
 epoch 94/100. Took 0.57272 seconds. 
  Full-batch training loss = 0.006064, test loss = 0.179379
  Training set accuracy = 1.000000, Test set accuracy = 0.954400
 epoch 95/100. Took 0.57733 seconds. 
  Full-batch training loss = 0.005950, test loss = 0.179499
  Training set accuracy = 1.000000, Test set accuracy = 0.954500
 epoch 96/100. Took 0.57553 seconds. 
  Full-batch training loss = 0.005837, test loss = 0.179608
  Training set accuracy = 1.000000, Test set accuracy = 0.954400
 epoch 97/100. Took 0.57193 seconds. 
  Full-batch training loss = 0.005739, test loss = 0.179942
  Training set accuracy = 1.000000, Test set accuracy = 0.954200
 epoch 98/100. Took 0.57318 seconds. 
  Full-batch training loss = 0.005640, test loss = 0.179897
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 99/100. Took 0.56975 seconds. 
  Full-batch training loss = 0.005548, test loss = 0.179940
  Training set accuracy = 1.000000, Test set accuracy = 0.954400
 epoch 100/100. Took 0.57117 seconds. 
  Full-batch training loss = 0.005442, test loss = 0.180387
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
Elapsed time is 134.917127 seconds.
End Training
