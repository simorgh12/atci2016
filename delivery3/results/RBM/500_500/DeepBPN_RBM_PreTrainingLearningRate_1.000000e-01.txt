Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.10
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 2.4113 seconds. Average reconstruction error is: 27.3314
 epoch 2/20. Took 2.4836 seconds. Average reconstruction error is: 14.9306
 epoch 3/20. Took 2.391 seconds. Average reconstruction error is: 12.6514
 epoch 4/20. Took 2.318 seconds. Average reconstruction error is: 11.4487
 epoch 5/20. Took 2.3733 seconds. Average reconstruction error is: 10.6402
 epoch 6/20. Took 2.3562 seconds. Average reconstruction error is: 10.0367
 epoch 7/20. Took 2.3539 seconds. Average reconstruction error is: 9.6282
 epoch 8/20. Took 2.3441 seconds. Average reconstruction error is: 9.2435
 epoch 9/20. Took 2.3474 seconds. Average reconstruction error is: 8.9813
 epoch 10/20. Took 2.4093 seconds. Average reconstruction error is: 8.7708
 epoch 11/20. Took 2.4132 seconds. Average reconstruction error is: 8.641
 epoch 12/20. Took 2.3237 seconds. Average reconstruction error is: 8.5042
 epoch 13/20. Took 2.3647 seconds. Average reconstruction error is: 8.3691
 epoch 14/20. Took 2.3479 seconds. Average reconstruction error is: 8.3006
 epoch 15/20. Took 2.3221 seconds. Average reconstruction error is: 8.2127
 epoch 16/20. Took 2.4364 seconds. Average reconstruction error is: 8.1217
 epoch 17/20. Took 2.3372 seconds. Average reconstruction error is: 8.053
 epoch 18/20. Took 2.3087 seconds. Average reconstruction error is: 7.9944
 epoch 19/20. Took 2.38 seconds. Average reconstruction error is: 7.9277
 epoch 20/20. Took 2.3819 seconds. Average reconstruction error is: 7.9466
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 1.6174 seconds. Average reconstruction error is: 26.1649
 epoch 2/20. Took 1.5803 seconds. Average reconstruction error is: 14.5744
 epoch 3/20. Took 1.6084 seconds. Average reconstruction error is: 12.4578
 epoch 4/20. Took 1.6684 seconds. Average reconstruction error is: 11.4565
 epoch 5/20. Took 1.579 seconds. Average reconstruction error is: 10.864
 epoch 6/20. Took 1.5789 seconds. Average reconstruction error is: 10.4287
 epoch 7/20. Took 1.6203 seconds. Average reconstruction error is: 10.11
 epoch 8/20. Took 1.5879 seconds. Average reconstruction error is: 9.8297
 epoch 9/20. Took 1.6123 seconds. Average reconstruction error is: 9.5982
 epoch 10/20. Took 1.5621 seconds. Average reconstruction error is: 9.3991
 epoch 11/20. Took 1.6225 seconds. Average reconstruction error is: 9.2344
 epoch 12/20. Took 1.6336 seconds. Average reconstruction error is: 9.0934
 epoch 13/20. Took 1.5604 seconds. Average reconstruction error is: 8.9092
 epoch 14/20. Took 1.6679 seconds. Average reconstruction error is: 8.8281
 epoch 15/20. Took 1.6556 seconds. Average reconstruction error is: 8.6971
 epoch 16/20. Took 1.6364 seconds. Average reconstruction error is: 8.5726
 epoch 17/20. Took 1.5913 seconds. Average reconstruction error is: 8.4594
 epoch 18/20. Took 1.5591 seconds. Average reconstruction error is: 8.3305
 epoch 19/20. Took 1.6361 seconds. Average reconstruction error is: 8.2548
 epoch 20/20. Took 1.5973 seconds. Average reconstruction error is: 8.1986
Training NN  (784  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 2.5916 seconds. 
  Full-batch training loss = 0.270749, test loss = 0.270749
  Training set accuracy = 0.923700, Test set accuracy = 0.926100
 epoch 2/100. Took 2.6187 seconds. 
  Full-batch training loss = 0.214948, test loss = 0.236251
  Training set accuracy = 0.940900, Test set accuracy = 0.932100
 epoch 3/100. Took 2.5847 seconds. 
  Full-batch training loss = 0.172133, test loss = 0.207153
  Training set accuracy = 0.952000, Test set accuracy = 0.940600
 epoch 4/100. Took 2.5744 seconds. 
  Full-batch training loss = 0.155506, test loss = 0.197459
  Training set accuracy = 0.955200, Test set accuracy = 0.941900
 epoch 5/100. Took 2.5837 seconds. 
  Full-batch training loss = 0.125484, test loss = 0.175845
  Training set accuracy = 0.965000, Test set accuracy = 0.948500
 epoch 6/100. Took 2.5477 seconds. 
  Full-batch training loss = 0.115684, test loss = 0.177397
  Training set accuracy = 0.968700, Test set accuracy = 0.947800
 epoch 7/100. Took 2.593 seconds. 
  Full-batch training loss = 0.095028, test loss = 0.165096
  Training set accuracy = 0.977000, Test set accuracy = 0.951300
 epoch 8/100. Took 2.7308 seconds. 
  Full-batch training loss = 0.084136, test loss = 0.159349
  Training set accuracy = 0.979800, Test set accuracy = 0.951700
 epoch 9/100. Took 2.5925 seconds. 
  Full-batch training loss = 0.074343, test loss = 0.158197
  Training set accuracy = 0.983700, Test set accuracy = 0.953100
 epoch 10/100. Took 2.5875 seconds. 
  Full-batch training loss = 0.066802, test loss = 0.156993
  Training set accuracy = 0.986000, Test set accuracy = 0.953700
 epoch 11/100. Took 2.5868 seconds. 
  Full-batch training loss = 0.061749, test loss = 0.155634
  Training set accuracy = 0.987000, Test set accuracy = 0.954500
 epoch 12/100. Took 2.678 seconds. 
  Full-batch training loss = 0.053189, test loss = 0.153216
  Training set accuracy = 0.990900, Test set accuracy = 0.955400
 epoch 13/100. Took 2.6258 seconds. 
  Full-batch training loss = 0.048124, test loss = 0.148840
  Training set accuracy = 0.991300, Test set accuracy = 0.955800
 epoch 14/100. Took 2.5884 seconds. 
  Full-batch training loss = 0.040646, test loss = 0.145001
  Training set accuracy = 0.994600, Test set accuracy = 0.956200
 epoch 15/100. Took 2.5707 seconds. 
  Full-batch training loss = 0.038556, test loss = 0.149105
  Training set accuracy = 0.995300, Test set accuracy = 0.955100
 epoch 16/100. Took 2.6124 seconds. 
  Full-batch training loss = 0.034328, test loss = 0.144502
  Training set accuracy = 0.995700, Test set accuracy = 0.956800
 epoch 17/100. Took 2.5944 seconds. 
  Full-batch training loss = 0.030153, test loss = 0.140317
  Training set accuracy = 0.996800, Test set accuracy = 0.959100
 epoch 18/100. Took 2.6116 seconds. 
  Full-batch training loss = 0.028729, test loss = 0.141445
  Training set accuracy = 0.996600, Test set accuracy = 0.956700
 epoch 19/100. Took 2.579 seconds. 
  Full-batch training loss = 0.025673, test loss = 0.142713
  Training set accuracy = 0.998000, Test set accuracy = 0.958500
 epoch 20/100. Took 2.8077 seconds. 
  Full-batch training loss = 0.023753, test loss = 0.141553
  Training set accuracy = 0.997800, Test set accuracy = 0.958300
 epoch 21/100. Took 2.6492 seconds. 
  Full-batch training loss = 0.021903, test loss = 0.141609
  Training set accuracy = 0.997700, Test set accuracy = 0.958800
 epoch 22/100. Took 2.6088 seconds. 
  Full-batch training loss = 0.018827, test loss = 0.139643
  Training set accuracy = 0.998800, Test set accuracy = 0.959300
 epoch 23/100. Took 2.5993 seconds. 
  Full-batch training loss = 0.017683, test loss = 0.139946
  Training set accuracy = 0.998800, Test set accuracy = 0.959100
 epoch 24/100. Took 2.5858 seconds. 
  Full-batch training loss = 0.016849, test loss = 0.139740
  Training set accuracy = 0.999100, Test set accuracy = 0.959100
 epoch 25/100. Took 2.6027 seconds. 
  Full-batch training loss = 0.015150, test loss = 0.137893
  Training set accuracy = 0.999400, Test set accuracy = 0.959600
 epoch 26/100. Took 2.5897 seconds. 
  Full-batch training loss = 0.014235, test loss = 0.139161
  Training set accuracy = 0.999400, Test set accuracy = 0.960300
 epoch 27/100. Took 2.5877 seconds. 
  Full-batch training loss = 0.013101, test loss = 0.139138
  Training set accuracy = 0.999700, Test set accuracy = 0.959000
 epoch 28/100. Took 2.6095 seconds. 
  Full-batch training loss = 0.012202, test loss = 0.139438
  Training set accuracy = 0.999800, Test set accuracy = 0.960200
 epoch 29/100. Took 2.6511 seconds. 
  Full-batch training loss = 0.011433, test loss = 0.138852
  Training set accuracy = 0.999900, Test set accuracy = 0.960600
 epoch 30/100. Took 2.5772 seconds. 
  Full-batch training loss = 0.010738, test loss = 0.137319
  Training set accuracy = 0.999900, Test set accuracy = 0.960900
 epoch 31/100. Took 2.6006 seconds. 
  Full-batch training loss = 0.010222, test loss = 0.139549
  Training set accuracy = 0.999900, Test set accuracy = 0.960700
 epoch 32/100. Took 2.5745 seconds. 
  Full-batch training loss = 0.009492, test loss = 0.139447
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 33/100. Took 2.6118 seconds. 
  Full-batch training loss = 0.009011, test loss = 0.139006
  Training set accuracy = 1.000000, Test set accuracy = 0.960800
 epoch 34/100. Took 2.607 seconds. 
  Full-batch training loss = 0.008609, test loss = 0.138250
  Training set accuracy = 1.000000, Test set accuracy = 0.961400
 epoch 35/100. Took 2.7487 seconds. 
  Full-batch training loss = 0.008098, test loss = 0.139495
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 36/100. Took 2.6139 seconds. 
  Full-batch training loss = 0.007809, test loss = 0.139182
  Training set accuracy = 1.000000, Test set accuracy = 0.961500
 epoch 37/100. Took 2.604 seconds. 
  Full-batch training loss = 0.007401, test loss = 0.139540
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 38/100. Took 2.6276 seconds. 
  Full-batch training loss = 0.007118, test loss = 0.138635
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 39/100. Took 2.7144 seconds. 
  Full-batch training loss = 0.006763, test loss = 0.139712
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 40/100. Took 2.6283 seconds. 
  Full-batch training loss = 0.006517, test loss = 0.139575
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 41/100. Took 2.5634 seconds. 
  Full-batch training loss = 0.006316, test loss = 0.140629
  Training set accuracy = 1.000000, Test set accuracy = 0.961300
 epoch 42/100. Took 2.5648 seconds. 
  Full-batch training loss = 0.006057, test loss = 0.139327
  Training set accuracy = 1.000000, Test set accuracy = 0.961600
 epoch 43/100. Took 2.5746 seconds. 
  Full-batch training loss = 0.005776, test loss = 0.140027
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 44/100. Took 2.5751 seconds. 
  Full-batch training loss = 0.005604, test loss = 0.140448
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 45/100. Took 2.6013 seconds. 
  Full-batch training loss = 0.005471, test loss = 0.141250
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 46/100. Took 2.6103 seconds. 
  Full-batch training loss = 0.005241, test loss = 0.139921
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 47/100. Took 2.5969 seconds. 
  Full-batch training loss = 0.005053, test loss = 0.141651
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 48/100. Took 2.5873 seconds. 
  Full-batch training loss = 0.004872, test loss = 0.141082
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 49/100. Took 2.5882 seconds. 
  Full-batch training loss = 0.004704, test loss = 0.140777
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 50/100. Took 2.5824 seconds. 
  Full-batch training loss = 0.004555, test loss = 0.140996
  Training set accuracy = 1.000000, Test set accuracy = 0.961600
 epoch 51/100. Took 2.6189 seconds. 
  Full-batch training loss = 0.004445, test loss = 0.141242
  Training set accuracy = 1.000000, Test set accuracy = 0.961600
 epoch 52/100. Took 2.6011 seconds. 
  Full-batch training loss = 0.004324, test loss = 0.141698
  Training set accuracy = 1.000000, Test set accuracy = 0.961700
 epoch 53/100. Took 2.5859 seconds. 
  Full-batch training loss = 0.004195, test loss = 0.140575
  Training set accuracy = 1.000000, Test set accuracy = 0.961800
 epoch 54/100. Took 2.7559 seconds. 
  Full-batch training loss = 0.004069, test loss = 0.141358
  Training set accuracy = 1.000000, Test set accuracy = 0.961500
 epoch 55/100. Took 2.6448 seconds. 
  Full-batch training loss = 0.004000, test loss = 0.141590
  Training set accuracy = 1.000000, Test set accuracy = 0.961900
 epoch 56/100. Took 2.7006 seconds. 
  Full-batch training loss = 0.003846, test loss = 0.141796
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 57/100. Took 2.5737 seconds. 
  Full-batch training loss = 0.003774, test loss = 0.142829
  Training set accuracy = 1.000000, Test set accuracy = 0.961800
 epoch 58/100. Took 2.6812 seconds. 
  Full-batch training loss = 0.003663, test loss = 0.141959
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 59/100. Took 2.6452 seconds. 
  Full-batch training loss = 0.003582, test loss = 0.142653
  Training set accuracy = 1.000000, Test set accuracy = 0.961900
 epoch 60/100. Took 2.6496 seconds. 
  Full-batch training loss = 0.003496, test loss = 0.142537
  Training set accuracy = 1.000000, Test set accuracy = 0.962400
 epoch 61/100. Took 2.6125 seconds. 
  Full-batch training loss = 0.003393, test loss = 0.142403
  Training set accuracy = 1.000000, Test set accuracy = 0.962100
 epoch 62/100. Took 2.629 seconds. 
  Full-batch training loss = 0.003326, test loss = 0.142663
  Training set accuracy = 1.000000, Test set accuracy = 0.962200
 epoch 63/100. Took 2.5789 seconds. 
  Full-batch training loss = 0.003242, test loss = 0.142396
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 64/100. Took 2.6182 seconds. 
  Full-batch training loss = 0.003179, test loss = 0.142243
  Training set accuracy = 1.000000, Test set accuracy = 0.962400
 epoch 65/100. Took 2.5694 seconds. 
  Full-batch training loss = 0.003117, test loss = 0.142680
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 66/100. Took 2.6923 seconds. 
  Full-batch training loss = 0.003033, test loss = 0.143452
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 67/100. Took 2.6198 seconds. 
  Full-batch training loss = 0.002977, test loss = 0.142841
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 68/100. Took 2.5885 seconds. 
  Full-batch training loss = 0.002902, test loss = 0.143149
  Training set accuracy = 1.000000, Test set accuracy = 0.962200
 epoch 69/100. Took 2.6479 seconds. 
  Full-batch training loss = 0.002848, test loss = 0.143261
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 70/100. Took 2.631 seconds. 
  Full-batch training loss = 0.002781, test loss = 0.143149
  Training set accuracy = 1.000000, Test set accuracy = 0.962200
 epoch 71/100. Took 2.5793 seconds. 
  Full-batch training loss = 0.002739, test loss = 0.143290
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 72/100. Took 2.6377 seconds. 
  Full-batch training loss = 0.002678, test loss = 0.143282
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 73/100. Took 2.6525 seconds. 
  Full-batch training loss = 0.002627, test loss = 0.143880
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 74/100. Took 2.5632 seconds. 
  Full-batch training loss = 0.002586, test loss = 0.144229
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 75/100. Took 2.6017 seconds. 
  Full-batch training loss = 0.002522, test loss = 0.143889
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 76/100. Took 2.5971 seconds. 
  Full-batch training loss = 0.002476, test loss = 0.144161
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 77/100. Took 2.6201 seconds. 
  Full-batch training loss = 0.002434, test loss = 0.144467
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 78/100. Took 2.6598 seconds. 
  Full-batch training loss = 0.002392, test loss = 0.144570
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 79/100. Took 2.6334 seconds. 
  Full-batch training loss = 0.002350, test loss = 0.144460
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 80/100. Took 2.6203 seconds. 
  Full-batch training loss = 0.002308, test loss = 0.144754
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 81/100. Took 2.6598 seconds. 
  Full-batch training loss = 0.002269, test loss = 0.144654
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 82/100. Took 2.6169 seconds. 
  Full-batch training loss = 0.002232, test loss = 0.144571
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 83/100. Took 2.5751 seconds. 
  Full-batch training loss = 0.002201, test loss = 0.145555
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 84/100. Took 2.598 seconds. 
  Full-batch training loss = 0.002159, test loss = 0.145123
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 85/100. Took 2.6076 seconds. 
  Full-batch training loss = 0.002141, test loss = 0.146042
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 86/100. Took 2.634 seconds. 
  Full-batch training loss = 0.002091, test loss = 0.145273
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 87/100. Took 2.6165 seconds. 
  Full-batch training loss = 0.002054, test loss = 0.145328
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 88/100. Took 2.5887 seconds. 
  Full-batch training loss = 0.002024, test loss = 0.145388
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 89/100. Took 2.5623 seconds. 
  Full-batch training loss = 0.001991, test loss = 0.145594
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 90/100. Took 2.9488 seconds. 
  Full-batch training loss = 0.001963, test loss = 0.145538
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 91/100. Took 2.6092 seconds. 
  Full-batch training loss = 0.001938, test loss = 0.145721
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 92/100. Took 2.6331 seconds. 
  Full-batch training loss = 0.001905, test loss = 0.145697
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 93/100. Took 2.6181 seconds. 
  Full-batch training loss = 0.001877, test loss = 0.145743
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 94/100. Took 2.6119 seconds. 
  Full-batch training loss = 0.001851, test loss = 0.145873
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 95/100. Took 2.5786 seconds. 
  Full-batch training loss = 0.001822, test loss = 0.146024
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 96/100. Took 2.6227 seconds. 
  Full-batch training loss = 0.001798, test loss = 0.146421
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 97/100. Took 2.5914 seconds. 
  Full-batch training loss = 0.001776, test loss = 0.146377
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 98/100. Took 2.6874 seconds. 
  Full-batch training loss = 0.001749, test loss = 0.146095
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 99/100. Took 2.737 seconds. 
  Full-batch training loss = 0.001726, test loss = 0.146568
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 100/100. Took 2.6324 seconds. 
  Full-batch training loss = 0.001705, test loss = 0.146584
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
Elapsed time is 572.227763 seconds.
End Training
