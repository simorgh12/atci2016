Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.6294 seconds. Average reconstruction error is: 29.0815
 epoch 2/20. Took 1.6526 seconds. Average reconstruction error is: 17.6879
 epoch 3/20. Took 1.5796 seconds. Average reconstruction error is: 15.3811
 epoch 4/20. Took 1.6294 seconds. Average reconstruction error is: 13.9761
 epoch 5/20. Took 1.6638 seconds. Average reconstruction error is: 13.0164
 epoch 6/20. Took 1.583 seconds. Average reconstruction error is: 12.3127
 epoch 7/20. Took 1.629 seconds. Average reconstruction error is: 11.7423
 epoch 8/20. Took 1.641 seconds. Average reconstruction error is: 11.2705
 epoch 9/20. Took 1.7078 seconds. Average reconstruction error is: 10.9187
 epoch 10/20. Took 1.7165 seconds. Average reconstruction error is: 10.6419
 epoch 11/20. Took 1.6871 seconds. Average reconstruction error is: 10.4335
 epoch 12/20. Took 1.6801 seconds. Average reconstruction error is: 10.2505
 epoch 13/20. Took 1.8208 seconds. Average reconstruction error is: 10.0975
 epoch 14/20. Took 1.6056 seconds. Average reconstruction error is: 9.9367
 epoch 15/20. Took 1.5681 seconds. Average reconstruction error is: 9.7705
 epoch 16/20. Took 1.6185 seconds. Average reconstruction error is: 9.7217
 epoch 17/20. Took 1.815 seconds. Average reconstruction error is: 9.6081
 epoch 18/20. Took 1.552 seconds. Average reconstruction error is: 9.5129
 epoch 19/20. Took 1.6108 seconds. Average reconstruction error is: 9.4667
 epoch 20/20. Took 1.7561 seconds. Average reconstruction error is: 9.3497
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.48157 seconds. Average reconstruction error is: 14.4996
 epoch 2/20. Took 0.46724 seconds. Average reconstruction error is: 8.7637
 epoch 3/20. Took 0.53275 seconds. Average reconstruction error is: 7.7153
 epoch 4/20. Took 0.51332 seconds. Average reconstruction error is: 7.2011
 epoch 5/20. Took 0.47419 seconds. Average reconstruction error is: 6.8495
 epoch 6/20. Took 0.53573 seconds. Average reconstruction error is: 6.595
 epoch 7/20. Took 0.54637 seconds. Average reconstruction error is: 6.4056
 epoch 8/20. Took 0.48465 seconds. Average reconstruction error is: 6.2601
 epoch 9/20. Took 0.50169 seconds. Average reconstruction error is: 6.1027
 epoch 10/20. Took 0.506 seconds. Average reconstruction error is: 6.0143
 epoch 11/20. Took 0.45187 seconds. Average reconstruction error is: 5.8904
 epoch 12/20. Took 0.48169 seconds. Average reconstruction error is: 5.8084
 epoch 13/20. Took 0.53731 seconds. Average reconstruction error is: 5.7008
 epoch 14/20. Took 0.54584 seconds. Average reconstruction error is: 5.6727
 epoch 15/20. Took 0.56246 seconds. Average reconstruction error is: 5.5749
 epoch 16/20. Took 0.50755 seconds. Average reconstruction error is: 5.5167
 epoch 17/20. Took 0.50987 seconds. Average reconstruction error is: 5.4458
 epoch 18/20. Took 0.49813 seconds. Average reconstruction error is: 5.4114
 epoch 19/20. Took 0.52702 seconds. Average reconstruction error is: 5.3407
 epoch 20/20. Took 0.49991 seconds. Average reconstruction error is: 5.3036
Training NN  (784  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 1.3061 seconds. 
  Full-batch training loss = 0.301846, test loss = 0.305306
  Training set accuracy = 0.913500, Test set accuracy = 0.914800
 epoch 2/100. Took 1.269 seconds. 
  Full-batch training loss = 0.245750, test loss = 0.262821
  Training set accuracy = 0.930000, Test set accuracy = 0.924800
 epoch 3/100. Took 1.2313 seconds. 
  Full-batch training loss = 0.214140, test loss = 0.245558
  Training set accuracy = 0.937600, Test set accuracy = 0.930400
 epoch 4/100. Took 1.2302 seconds. 
  Full-batch training loss = 0.188991, test loss = 0.229339
  Training set accuracy = 0.945800, Test set accuracy = 0.932800
 epoch 5/100. Took 1.27 seconds. 
  Full-batch training loss = 0.165032, test loss = 0.213776
  Training set accuracy = 0.953000, Test set accuracy = 0.938300
 epoch 6/100. Took 1.1708 seconds. 
  Full-batch training loss = 0.149201, test loss = 0.206191
  Training set accuracy = 0.960100, Test set accuracy = 0.941100
 epoch 7/100. Took 1.2224 seconds. 
  Full-batch training loss = 0.136105, test loss = 0.200175
  Training set accuracy = 0.964000, Test set accuracy = 0.942900
 epoch 8/100. Took 1.1163 seconds. 
  Full-batch training loss = 0.119296, test loss = 0.188937
  Training set accuracy = 0.970000, Test set accuracy = 0.945700
 epoch 9/100. Took 1.2101 seconds. 
  Full-batch training loss = 0.108133, test loss = 0.185974
  Training set accuracy = 0.972000, Test set accuracy = 0.945600
 epoch 10/100. Took 1.1805 seconds. 
  Full-batch training loss = 0.098333, test loss = 0.184742
  Training set accuracy = 0.976200, Test set accuracy = 0.947300
 epoch 11/100. Took 1.1266 seconds. 
  Full-batch training loss = 0.090218, test loss = 0.180091
  Training set accuracy = 0.979100, Test set accuracy = 0.948500
 epoch 12/100. Took 1.1967 seconds. 
  Full-batch training loss = 0.083404, test loss = 0.175539
  Training set accuracy = 0.980300, Test set accuracy = 0.948000
 epoch 13/100. Took 1.1452 seconds. 
  Full-batch training loss = 0.073256, test loss = 0.170219
  Training set accuracy = 0.985000, Test set accuracy = 0.950200
 epoch 14/100. Took 1.1587 seconds. 
  Full-batch training loss = 0.069937, test loss = 0.176437
  Training set accuracy = 0.985000, Test set accuracy = 0.950600
 epoch 15/100. Took 1.2705 seconds. 
  Full-batch training loss = 0.061576, test loss = 0.170836
  Training set accuracy = 0.988100, Test set accuracy = 0.950700
 epoch 16/100. Took 1.2847 seconds. 
  Full-batch training loss = 0.055962, test loss = 0.169843
  Training set accuracy = 0.989900, Test set accuracy = 0.952400
 epoch 17/100. Took 1.2172 seconds. 
  Full-batch training loss = 0.051830, test loss = 0.169890
  Training set accuracy = 0.990300, Test set accuracy = 0.951200
 epoch 18/100. Took 1.1346 seconds. 
  Full-batch training loss = 0.046951, test loss = 0.166407
  Training set accuracy = 0.992600, Test set accuracy = 0.952300
 epoch 19/100. Took 1.1197 seconds. 
  Full-batch training loss = 0.043575, test loss = 0.167954
  Training set accuracy = 0.993400, Test set accuracy = 0.951800
 epoch 20/100. Took 1.1348 seconds. 
  Full-batch training loss = 0.040816, test loss = 0.169115
  Training set accuracy = 0.994500, Test set accuracy = 0.951700
 epoch 21/100. Took 1.2447 seconds. 
  Full-batch training loss = 0.037129, test loss = 0.167338
  Training set accuracy = 0.995000, Test set accuracy = 0.952600
 epoch 22/100. Took 1.1451 seconds. 
  Full-batch training loss = 0.034237, test loss = 0.166486
  Training set accuracy = 0.996200, Test set accuracy = 0.953300
 epoch 23/100. Took 1.1977 seconds. 
  Full-batch training loss = 0.031385, test loss = 0.166873
  Training set accuracy = 0.996000, Test set accuracy = 0.952100
 epoch 24/100. Took 1.1205 seconds. 
  Full-batch training loss = 0.030233, test loss = 0.168307
  Training set accuracy = 0.996400, Test set accuracy = 0.952700
 epoch 25/100. Took 1.1724 seconds. 
  Full-batch training loss = 0.027586, test loss = 0.168039
  Training set accuracy = 0.996700, Test set accuracy = 0.952700
 epoch 26/100. Took 1.1796 seconds. 
  Full-batch training loss = 0.025712, test loss = 0.167528
  Training set accuracy = 0.997400, Test set accuracy = 0.952400
 epoch 27/100. Took 1.2349 seconds. 
  Full-batch training loss = 0.023731, test loss = 0.167387
  Training set accuracy = 0.997900, Test set accuracy = 0.953600
 epoch 28/100. Took 1.1914 seconds. 
  Full-batch training loss = 0.022548, test loss = 0.170232
  Training set accuracy = 0.998300, Test set accuracy = 0.952500
 epoch 29/100. Took 1.311 seconds. 
  Full-batch training loss = 0.021286, test loss = 0.172062
  Training set accuracy = 0.998400, Test set accuracy = 0.953600
 epoch 30/100. Took 1.1838 seconds. 
  Full-batch training loss = 0.019359, test loss = 0.167670
  Training set accuracy = 0.998600, Test set accuracy = 0.954200
 epoch 31/100. Took 1.1328 seconds. 
  Full-batch training loss = 0.018121, test loss = 0.168331
  Training set accuracy = 0.998700, Test set accuracy = 0.954300
 epoch 32/100. Took 1.1311 seconds. 
  Full-batch training loss = 0.016994, test loss = 0.169927
  Training set accuracy = 0.999300, Test set accuracy = 0.953800
 epoch 33/100. Took 1.2078 seconds. 
  Full-batch training loss = 0.016004, test loss = 0.168664
  Training set accuracy = 0.999400, Test set accuracy = 0.954700
 epoch 34/100. Took 1.2633 seconds. 
  Full-batch training loss = 0.015216, test loss = 0.170175
  Training set accuracy = 0.999400, Test set accuracy = 0.954100
 epoch 35/100. Took 1.1657 seconds. 
  Full-batch training loss = 0.014291, test loss = 0.169850
  Training set accuracy = 0.999500, Test set accuracy = 0.954100
 epoch 36/100. Took 1.1419 seconds. 
  Full-batch training loss = 0.013536, test loss = 0.168594
  Training set accuracy = 0.999700, Test set accuracy = 0.954100
 epoch 37/100. Took 1.1798 seconds. 
  Full-batch training loss = 0.012944, test loss = 0.170149
  Training set accuracy = 0.999900, Test set accuracy = 0.954500
 epoch 38/100. Took 1.157 seconds. 
  Full-batch training loss = 0.012155, test loss = 0.170699
  Training set accuracy = 0.999900, Test set accuracy = 0.954600
 epoch 39/100. Took 1.1261 seconds. 
  Full-batch training loss = 0.011897, test loss = 0.172592
  Training set accuracy = 0.999800, Test set accuracy = 0.953200
 epoch 40/100. Took 1.2487 seconds. 
  Full-batch training loss = 0.011209, test loss = 0.171381
  Training set accuracy = 0.999900, Test set accuracy = 0.954800
 epoch 41/100. Took 1.273 seconds. 
  Full-batch training loss = 0.010744, test loss = 0.170973
  Training set accuracy = 0.999900, Test set accuracy = 0.954600
 epoch 42/100. Took 1.1167 seconds. 
  Full-batch training loss = 0.010156, test loss = 0.172260
  Training set accuracy = 0.999900, Test set accuracy = 0.954800
 epoch 43/100. Took 1.169 seconds. 
  Full-batch training loss = 0.009832, test loss = 0.173092
  Training set accuracy = 0.999900, Test set accuracy = 0.955200
 epoch 44/100. Took 1.2222 seconds. 
  Full-batch training loss = 0.009347, test loss = 0.173121
  Training set accuracy = 0.999900, Test set accuracy = 0.954600
 epoch 45/100. Took 1.1579 seconds. 
  Full-batch training loss = 0.009020, test loss = 0.173914
  Training set accuracy = 0.999900, Test set accuracy = 0.954700
 epoch 46/100. Took 1.3079 seconds. 
  Full-batch training loss = 0.008628, test loss = 0.174442
  Training set accuracy = 0.999900, Test set accuracy = 0.954800
 epoch 47/100. Took 1.2217 seconds. 
  Full-batch training loss = 0.008304, test loss = 0.173958
  Training set accuracy = 0.999900, Test set accuracy = 0.954700
 epoch 48/100. Took 1.1363 seconds. 
  Full-batch training loss = 0.007996, test loss = 0.174041
  Training set accuracy = 0.999900, Test set accuracy = 0.954800
 epoch 49/100. Took 1.2031 seconds. 
  Full-batch training loss = 0.007712, test loss = 0.174785
  Training set accuracy = 0.999900, Test set accuracy = 0.955200
 epoch 50/100. Took 1.1863 seconds. 
  Full-batch training loss = 0.007443, test loss = 0.174209
  Training set accuracy = 0.999900, Test set accuracy = 0.955100
 epoch 51/100. Took 1.1957 seconds. 
  Full-batch training loss = 0.007231, test loss = 0.176078
  Training set accuracy = 0.999900, Test set accuracy = 0.955200
 epoch 52/100. Took 1.1081 seconds. 
  Full-batch training loss = 0.006973, test loss = 0.175309
  Training set accuracy = 0.999900, Test set accuracy = 0.955200
 epoch 53/100. Took 1.1369 seconds. 
  Full-batch training loss = 0.006744, test loss = 0.176574
  Training set accuracy = 0.999900, Test set accuracy = 0.955200
 epoch 54/100. Took 1.2003 seconds. 
  Full-batch training loss = 0.006550, test loss = 0.175847
  Training set accuracy = 0.999900, Test set accuracy = 0.955600
 epoch 55/100. Took 1.1914 seconds. 
  Full-batch training loss = 0.006365, test loss = 0.176655
  Training set accuracy = 0.999900, Test set accuracy = 0.955600
 epoch 56/100. Took 1.2234 seconds. 
  Full-batch training loss = 0.006153, test loss = 0.176396
  Training set accuracy = 0.999900, Test set accuracy = 0.954800
 epoch 57/100. Took 1.234 seconds. 
  Full-batch training loss = 0.005955, test loss = 0.176950
  Training set accuracy = 0.999900, Test set accuracy = 0.955000
 epoch 58/100. Took 1.2935 seconds. 
  Full-batch training loss = 0.005767, test loss = 0.177649
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 59/100. Took 1.2236 seconds. 
  Full-batch training loss = 0.005601, test loss = 0.177639
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 60/100. Took 1.2306 seconds. 
  Full-batch training loss = 0.005461, test loss = 0.177323
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 61/100. Took 1.1987 seconds. 
  Full-batch training loss = 0.005306, test loss = 0.177381
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 62/100. Took 1.1389 seconds. 
  Full-batch training loss = 0.005168, test loss = 0.178567
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 63/100. Took 1.2102 seconds. 
  Full-batch training loss = 0.005041, test loss = 0.178895
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 64/100. Took 1.2309 seconds. 
  Full-batch training loss = 0.004903, test loss = 0.179015
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 65/100. Took 1.2212 seconds. 
  Full-batch training loss = 0.004816, test loss = 0.179396
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 66/100. Took 1.0961 seconds. 
  Full-batch training loss = 0.004660, test loss = 0.179022
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 67/100. Took 1.2614 seconds. 
  Full-batch training loss = 0.004581, test loss = 0.179864
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 68/100. Took 1.2541 seconds. 
  Full-batch training loss = 0.004454, test loss = 0.179509
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 69/100. Took 1.1784 seconds. 
  Full-batch training loss = 0.004348, test loss = 0.179739
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 70/100. Took 1.3368 seconds. 
  Full-batch training loss = 0.004252, test loss = 0.179931
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 71/100. Took 1.2833 seconds. 
  Full-batch training loss = 0.004165, test loss = 0.179708
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 72/100. Took 1.1068 seconds. 
  Full-batch training loss = 0.004085, test loss = 0.181191
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 73/100. Took 1.1957 seconds. 
  Full-batch training loss = 0.003978, test loss = 0.180822
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 74/100. Took 1.2878 seconds. 
  Full-batch training loss = 0.003902, test loss = 0.181648
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 75/100. Took 1.1707 seconds. 
  Full-batch training loss = 0.003829, test loss = 0.181083
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 76/100. Took 1.2545 seconds. 
  Full-batch training loss = 0.003740, test loss = 0.181762
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 77/100. Took 1.1888 seconds. 
  Full-batch training loss = 0.003659, test loss = 0.181392
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 78/100. Took 1.2506 seconds. 
  Full-batch training loss = 0.003587, test loss = 0.181941
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 79/100. Took 1.1635 seconds. 
  Full-batch training loss = 0.003522, test loss = 0.181825
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 80/100. Took 1.1958 seconds. 
  Full-batch training loss = 0.003451, test loss = 0.182324
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 81/100. Took 1.1653 seconds. 
  Full-batch training loss = 0.003396, test loss = 0.181782
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 82/100. Took 1.198 seconds. 
  Full-batch training loss = 0.003327, test loss = 0.182555
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 83/100. Took 1.2104 seconds. 
  Full-batch training loss = 0.003268, test loss = 0.182790
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 84/100. Took 1.2869 seconds. 
  Full-batch training loss = 0.003209, test loss = 0.183378
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 85/100. Took 1.1819 seconds. 
  Full-batch training loss = 0.003154, test loss = 0.183373
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 86/100. Took 1.1906 seconds. 
  Full-batch training loss = 0.003098, test loss = 0.183148
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 87/100. Took 1.2218 seconds. 
  Full-batch training loss = 0.003043, test loss = 0.183555
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 88/100. Took 1.2453 seconds. 
  Full-batch training loss = 0.002999, test loss = 0.183753
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 89/100. Took 1.2539 seconds. 
  Full-batch training loss = 0.002946, test loss = 0.184449
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 90/100. Took 1.113 seconds. 
  Full-batch training loss = 0.002895, test loss = 0.184286
  Training set accuracy = 1.000000, Test set accuracy = 0.956100
 epoch 91/100. Took 1.224 seconds. 
  Full-batch training loss = 0.002850, test loss = 0.184523
  Training set accuracy = 1.000000, Test set accuracy = 0.956100
 epoch 92/100. Took 1.1822 seconds. 
  Full-batch training loss = 0.002803, test loss = 0.184654
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 93/100. Took 1.1805 seconds. 
  Full-batch training loss = 0.002758, test loss = 0.184603
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 94/100. Took 1.3224 seconds. 
  Full-batch training loss = 0.002714, test loss = 0.184829
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 95/100. Took 1.1355 seconds. 
  Full-batch training loss = 0.002676, test loss = 0.185000
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 96/100. Took 1.286 seconds. 
  Full-batch training loss = 0.002633, test loss = 0.185344
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 97/100. Took 1.2286 seconds. 
  Full-batch training loss = 0.002593, test loss = 0.185248
  Training set accuracy = 1.000000, Test set accuracy = 0.956000
 epoch 98/100. Took 1.2179 seconds. 
  Full-batch training loss = 0.002560, test loss = 0.185714
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 99/100. Took 1.2024 seconds. 
  Full-batch training loss = 0.002519, test loss = 0.186047
  Training set accuracy = 1.000000, Test set accuracy = 0.956000
 epoch 100/100. Took 1.1709 seconds. 
  Full-batch training loss = 0.002483, test loss = 0.185578
  Training set accuracy = 1.000000, Test set accuracy = 0.956000
Elapsed time is 305.822531 seconds.
End Training
