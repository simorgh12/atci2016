Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	200
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.566 seconds. Average reconstruction error is: 30.9956
 epoch 2/20. Took 1.6567 seconds. Average reconstruction error is: 18.8725
 epoch 3/20. Took 1.5878 seconds. Average reconstruction error is: 16.5808
 epoch 4/20. Took 1.7026 seconds. Average reconstruction error is: 15.2114
 epoch 5/20. Took 1.7557 seconds. Average reconstruction error is: 14.1226
 epoch 6/20. Took 1.626 seconds. Average reconstruction error is: 13.3073
 epoch 7/20. Took 1.6046 seconds. Average reconstruction error is: 12.6284
 epoch 8/20. Took 1.6107 seconds. Average reconstruction error is: 12.1528
 epoch 9/20. Took 1.6115 seconds. Average reconstruction error is: 11.7051
 epoch 10/20. Took 1.5948 seconds. Average reconstruction error is: 11.376
 epoch 11/20. Took 1.6231 seconds. Average reconstruction error is: 11.1103
 epoch 12/20. Took 1.6082 seconds. Average reconstruction error is: 10.8756
 epoch 13/20. Took 1.613 seconds. Average reconstruction error is: 10.7307
 epoch 14/20. Took 1.7029 seconds. Average reconstruction error is: 10.5623
 epoch 15/20. Took 1.638 seconds. Average reconstruction error is: 10.4181
 epoch 16/20. Took 1.6433 seconds. Average reconstruction error is: 10.2839
 epoch 17/20. Took 1.5846 seconds. Average reconstruction error is: 10.1606
 epoch 18/20. Took 1.7615 seconds. Average reconstruction error is: 10.0339
 epoch 19/20. Took 1.5908 seconds. Average reconstruction error is: 9.9457
 epoch 20/20. Took 1.5667 seconds. Average reconstruction error is: 9.8376
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.47618 seconds. Average reconstruction error is: 13.333
 epoch 2/20. Took 0.51624 seconds. Average reconstruction error is: 7.8485
 epoch 3/20. Took 0.57026 seconds. Average reconstruction error is: 6.8915
 epoch 4/20. Took 0.49655 seconds. Average reconstruction error is: 6.4161
 epoch 5/20. Took 0.53235 seconds. Average reconstruction error is: 6.111
 epoch 6/20. Took 0.53076 seconds. Average reconstruction error is: 5.9127
 epoch 7/20. Took 0.48584 seconds. Average reconstruction error is: 5.7513
 epoch 8/20. Took 0.47608 seconds. Average reconstruction error is: 5.6306
 epoch 9/20. Took 0.5141 seconds. Average reconstruction error is: 5.5105
 epoch 10/20. Took 0.48321 seconds. Average reconstruction error is: 5.4068
 epoch 11/20. Took 0.49767 seconds. Average reconstruction error is: 5.3377
 epoch 12/20. Took 0.52763 seconds. Average reconstruction error is: 5.2459
 epoch 13/20. Took 0.47474 seconds. Average reconstruction error is: 5.1825
 epoch 14/20. Took 0.47022 seconds. Average reconstruction error is: 5.1361
 epoch 15/20. Took 0.52474 seconds. Average reconstruction error is: 5.0794
 epoch 16/20. Took 0.50047 seconds. Average reconstruction error is: 5.0063
 epoch 17/20. Took 0.46659 seconds. Average reconstruction error is: 4.9842
 epoch 18/20. Took 0.52175 seconds. Average reconstruction error is: 4.9276
 epoch 19/20. Took 0.51086 seconds. Average reconstruction error is: 4.9104
 epoch 20/20. Took 0.50683 seconds. Average reconstruction error is: 4.8541
Training NN  (784  200  200   10) with BackPropagation for 200 epochs
 epoch 1/200. Took 1.1592 seconds. 
  Full-batch training loss = 0.306044, test loss = 0.304525
  Training set accuracy = 0.910000, Test set accuracy = 0.913100
 epoch 2/200. Took 1.2187 seconds. 
  Full-batch training loss = 0.244481, test loss = 0.255259
  Training set accuracy = 0.929600, Test set accuracy = 0.928400
 epoch 3/200. Took 1.194 seconds. 
  Full-batch training loss = 0.212079, test loss = 0.234367
  Training set accuracy = 0.938400, Test set accuracy = 0.930800
 epoch 4/200. Took 1.235 seconds. 
  Full-batch training loss = 0.183997, test loss = 0.219354
  Training set accuracy = 0.948700, Test set accuracy = 0.935700
 epoch 5/200. Took 1.1646 seconds. 
  Full-batch training loss = 0.163423, test loss = 0.205718
  Training set accuracy = 0.954900, Test set accuracy = 0.940100
 epoch 6/200. Took 1.1788 seconds. 
  Full-batch training loss = 0.146117, test loss = 0.199047
  Training set accuracy = 0.959400, Test set accuracy = 0.940500
 epoch 7/200. Took 1.2547 seconds. 
  Full-batch training loss = 0.129364, test loss = 0.190897
  Training set accuracy = 0.966300, Test set accuracy = 0.944700
 epoch 8/200. Took 1.1468 seconds. 
  Full-batch training loss = 0.122845, test loss = 0.191597
  Training set accuracy = 0.968100, Test set accuracy = 0.943000
 epoch 9/200. Took 1.2734 seconds. 
  Full-batch training loss = 0.107484, test loss = 0.181148
  Training set accuracy = 0.974300, Test set accuracy = 0.947700
 epoch 10/200. Took 1.2251 seconds. 
  Full-batch training loss = 0.095637, test loss = 0.176789
  Training set accuracy = 0.977900, Test set accuracy = 0.948600
 epoch 11/200. Took 1.2591 seconds. 
  Full-batch training loss = 0.089142, test loss = 0.177353
  Training set accuracy = 0.978600, Test set accuracy = 0.947200
 epoch 12/200. Took 1.1716 seconds. 
  Full-batch training loss = 0.078936, test loss = 0.170113
  Training set accuracy = 0.983100, Test set accuracy = 0.949300
 epoch 13/200. Took 1.237 seconds. 
  Full-batch training loss = 0.073481, test loss = 0.171936
  Training set accuracy = 0.985400, Test set accuracy = 0.950000
 epoch 14/200. Took 1.2855 seconds. 
  Full-batch training loss = 0.068920, test loss = 0.173772
  Training set accuracy = 0.986300, Test set accuracy = 0.948900
 epoch 15/200. Took 1.1532 seconds. 
  Full-batch training loss = 0.062295, test loss = 0.170645
  Training set accuracy = 0.988700, Test set accuracy = 0.951000
 epoch 16/200. Took 1.1949 seconds. 
  Full-batch training loss = 0.056070, test loss = 0.165804
  Training set accuracy = 0.990500, Test set accuracy = 0.950000
 epoch 17/200. Took 1.1703 seconds. 
  Full-batch training loss = 0.050949, test loss = 0.166308
  Training set accuracy = 0.991200, Test set accuracy = 0.951700
 epoch 18/200. Took 1.2621 seconds. 
  Full-batch training loss = 0.049359, test loss = 0.165515
  Training set accuracy = 0.991300, Test set accuracy = 0.950700
 epoch 19/200. Took 1.1922 seconds. 
  Full-batch training loss = 0.043712, test loss = 0.163753
  Training set accuracy = 0.993800, Test set accuracy = 0.953500
 epoch 20/200. Took 1.1836 seconds. 
  Full-batch training loss = 0.039539, test loss = 0.164046
  Training set accuracy = 0.994800, Test set accuracy = 0.953000
 epoch 21/200. Took 1.2869 seconds. 
  Full-batch training loss = 0.036539, test loss = 0.161892
  Training set accuracy = 0.994900, Test set accuracy = 0.953600
 epoch 22/200. Took 1.2337 seconds. 
  Full-batch training loss = 0.033722, test loss = 0.163397
  Training set accuracy = 0.995900, Test set accuracy = 0.953300
 epoch 23/200. Took 1.2723 seconds. 
  Full-batch training loss = 0.030965, test loss = 0.162116
  Training set accuracy = 0.996800, Test set accuracy = 0.953700
 epoch 24/200. Took 1.2771 seconds. 
  Full-batch training loss = 0.029131, test loss = 0.161191
  Training set accuracy = 0.997400, Test set accuracy = 0.954500
 epoch 25/200. Took 1.1878 seconds. 
  Full-batch training loss = 0.026894, test loss = 0.162303
  Training set accuracy = 0.997200, Test set accuracy = 0.954100
 epoch 26/200. Took 1.2473 seconds. 
  Full-batch training loss = 0.025063, test loss = 0.163436
  Training set accuracy = 0.997700, Test set accuracy = 0.953800
 epoch 27/200. Took 1.1783 seconds. 
  Full-batch training loss = 0.023816, test loss = 0.163944
  Training set accuracy = 0.998100, Test set accuracy = 0.953800
 epoch 28/200. Took 1.1905 seconds. 
  Full-batch training loss = 0.022481, test loss = 0.162687
  Training set accuracy = 0.998500, Test set accuracy = 0.953800
 epoch 29/200. Took 1.253 seconds. 
  Full-batch training loss = 0.020759, test loss = 0.162239
  Training set accuracy = 0.998900, Test set accuracy = 0.955500
 epoch 30/200. Took 1.2878 seconds. 
  Full-batch training loss = 0.019584, test loss = 0.162823
  Training set accuracy = 0.998800, Test set accuracy = 0.954600
 epoch 31/200. Took 1.1475 seconds. 
  Full-batch training loss = 0.018375, test loss = 0.163818
  Training set accuracy = 0.999300, Test set accuracy = 0.954400
 epoch 32/200. Took 1.2571 seconds. 
  Full-batch training loss = 0.017197, test loss = 0.162629
  Training set accuracy = 0.999500, Test set accuracy = 0.955200
 epoch 33/200. Took 1.2392 seconds. 
  Full-batch training loss = 0.016297, test loss = 0.164114
  Training set accuracy = 0.999500, Test set accuracy = 0.954400
 epoch 34/200. Took 1.1162 seconds. 
  Full-batch training loss = 0.015258, test loss = 0.163601
  Training set accuracy = 0.999600, Test set accuracy = 0.954700
 epoch 35/200. Took 1.1679 seconds. 
  Full-batch training loss = 0.014678, test loss = 0.164671
  Training set accuracy = 0.999600, Test set accuracy = 0.955200
 epoch 36/200. Took 1.1344 seconds. 
  Full-batch training loss = 0.013935, test loss = 0.163682
  Training set accuracy = 0.999600, Test set accuracy = 0.955500
 epoch 37/200. Took 1.1472 seconds. 
  Full-batch training loss = 0.013197, test loss = 0.165189
  Training set accuracy = 0.999600, Test set accuracy = 0.954700
 epoch 38/200. Took 1.2061 seconds. 
  Full-batch training loss = 0.012535, test loss = 0.165061
  Training set accuracy = 0.999700, Test set accuracy = 0.955500
 epoch 39/200. Took 1.2312 seconds. 
  Full-batch training loss = 0.012150, test loss = 0.164995
  Training set accuracy = 0.999800, Test set accuracy = 0.956200
 epoch 40/200. Took 1.1884 seconds. 
  Full-batch training loss = 0.011535, test loss = 0.165447
  Training set accuracy = 0.999800, Test set accuracy = 0.956200
 epoch 41/200. Took 1.3344 seconds. 
  Full-batch training loss = 0.011125, test loss = 0.166588
  Training set accuracy = 0.999900, Test set accuracy = 0.955200
 epoch 42/200. Took 1.1444 seconds. 
  Full-batch training loss = 0.010678, test loss = 0.166481
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 43/200. Took 1.1475 seconds. 
  Full-batch training loss = 0.010102, test loss = 0.166735
  Training set accuracy = 0.999900, Test set accuracy = 0.955400
 epoch 44/200. Took 1.2215 seconds. 
  Full-batch training loss = 0.009783, test loss = 0.167092
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 45/200. Took 1.2506 seconds. 
  Full-batch training loss = 0.009393, test loss = 0.166452
  Training set accuracy = 0.999900, Test set accuracy = 0.956900
 epoch 46/200. Took 1.2119 seconds. 
  Full-batch training loss = 0.009074, test loss = 0.167860
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 47/200. Took 1.2048 seconds. 
  Full-batch training loss = 0.008707, test loss = 0.167407
  Training set accuracy = 0.999900, Test set accuracy = 0.956900
 epoch 48/200. Took 1.1758 seconds. 
  Full-batch training loss = 0.008423, test loss = 0.167832
  Training set accuracy = 0.999900, Test set accuracy = 0.956900
 epoch 49/200. Took 1.1868 seconds. 
  Full-batch training loss = 0.008182, test loss = 0.168053
  Training set accuracy = 0.999900, Test set accuracy = 0.956600
 epoch 50/200. Took 1.1704 seconds. 
  Full-batch training loss = 0.007836, test loss = 0.169054
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 51/200. Took 1.232 seconds. 
  Full-batch training loss = 0.007655, test loss = 0.169968
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 52/200. Took 1.233 seconds. 
  Full-batch training loss = 0.007285, test loss = 0.169126
  Training set accuracy = 0.999900, Test set accuracy = 0.956800
 epoch 53/200. Took 1.1725 seconds. 
  Full-batch training loss = 0.007042, test loss = 0.169626
  Training set accuracy = 0.999900, Test set accuracy = 0.956800
 epoch 54/200. Took 1.1501 seconds. 
  Full-batch training loss = 0.006843, test loss = 0.170215
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 55/200. Took 1.2329 seconds. 
  Full-batch training loss = 0.006621, test loss = 0.170261
  Training set accuracy = 0.999900, Test set accuracy = 0.956600
 epoch 56/200. Took 1.2696 seconds. 
  Full-batch training loss = 0.006460, test loss = 0.169840
  Training set accuracy = 0.999900, Test set accuracy = 0.957700
 epoch 57/200. Took 1.1629 seconds. 
  Full-batch training loss = 0.006256, test loss = 0.170789
  Training set accuracy = 0.999900, Test set accuracy = 0.957300
 epoch 58/200. Took 1.1558 seconds. 
  Full-batch training loss = 0.006060, test loss = 0.171036
  Training set accuracy = 0.999900, Test set accuracy = 0.957200
 epoch 59/200. Took 1.2344 seconds. 
  Full-batch training loss = 0.005859, test loss = 0.170873
  Training set accuracy = 0.999900, Test set accuracy = 0.957400
 epoch 60/200. Took 1.2534 seconds. 
  Full-batch training loss = 0.005719, test loss = 0.171234
  Training set accuracy = 0.999900, Test set accuracy = 0.958200
 epoch 61/200. Took 1.1669 seconds. 
  Full-batch training loss = 0.005542, test loss = 0.171628
  Training set accuracy = 0.999900, Test set accuracy = 0.957000
 epoch 62/200. Took 1.1733 seconds. 
  Full-batch training loss = 0.005406, test loss = 0.171654
  Training set accuracy = 0.999900, Test set accuracy = 0.957300
 epoch 63/200. Took 1.1435 seconds. 
  Full-batch training loss = 0.005268, test loss = 0.172195
  Training set accuracy = 0.999900, Test set accuracy = 0.957300
 epoch 64/200. Took 1.1613 seconds. 
  Full-batch training loss = 0.005147, test loss = 0.172074
  Training set accuracy = 0.999900, Test set accuracy = 0.957800
 epoch 65/200. Took 1.2066 seconds. 
  Full-batch training loss = 0.005010, test loss = 0.172617
  Training set accuracy = 0.999900, Test set accuracy = 0.957700
 epoch 66/200. Took 1.2504 seconds. 
  Full-batch training loss = 0.004868, test loss = 0.172676
  Training set accuracy = 0.999900, Test set accuracy = 0.957500
 epoch 67/200. Took 1.2382 seconds. 
  Full-batch training loss = 0.004756, test loss = 0.173202
  Training set accuracy = 0.999900, Test set accuracy = 0.957100
 epoch 68/200. Took 1.1327 seconds. 
  Full-batch training loss = 0.004646, test loss = 0.173212
  Training set accuracy = 0.999900, Test set accuracy = 0.957400
 epoch 69/200. Took 1.2135 seconds. 
  Full-batch training loss = 0.004557, test loss = 0.173890
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 70/200. Took 1.1504 seconds. 
  Full-batch training loss = 0.004433, test loss = 0.174043
  Training set accuracy = 0.999900, Test set accuracy = 0.957700
 epoch 71/200. Took 1.1473 seconds. 
  Full-batch training loss = 0.004326, test loss = 0.174062
  Training set accuracy = 0.999900, Test set accuracy = 0.957500
 epoch 72/200. Took 1.2123 seconds. 
  Full-batch training loss = 0.004233, test loss = 0.174192
  Training set accuracy = 0.999900, Test set accuracy = 0.957300
 epoch 73/200. Took 1.2096 seconds. 
  Full-batch training loss = 0.004144, test loss = 0.174710
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 74/200. Took 1.1685 seconds. 
  Full-batch training loss = 0.004057, test loss = 0.174444
  Training set accuracy = 0.999900, Test set accuracy = 0.958100
 epoch 75/200. Took 1.1788 seconds. 
  Full-batch training loss = 0.003965, test loss = 0.174998
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 76/200. Took 1.1777 seconds. 
  Full-batch training loss = 0.003884, test loss = 0.175289
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 77/200. Took 1.2086 seconds. 
  Full-batch training loss = 0.003821, test loss = 0.175850
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 78/200. Took 1.135 seconds. 
  Full-batch training loss = 0.003730, test loss = 0.175570
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 79/200. Took 1.2228 seconds. 
  Full-batch training loss = 0.003651, test loss = 0.175683
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 80/200. Took 1.2278 seconds. 
  Full-batch training loss = 0.003581, test loss = 0.176141
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 81/200. Took 1.1556 seconds. 
  Full-batch training loss = 0.003515, test loss = 0.176301
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 82/200. Took 1.1614 seconds. 
  Full-batch training loss = 0.003456, test loss = 0.176568
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 83/200. Took 1.2646 seconds. 
  Full-batch training loss = 0.003392, test loss = 0.176833
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 84/200. Took 1.2745 seconds. 
  Full-batch training loss = 0.003330, test loss = 0.176906
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 85/200. Took 1.2349 seconds. 
  Full-batch training loss = 0.003267, test loss = 0.177089
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 86/200. Took 1.1557 seconds. 
  Full-batch training loss = 0.003221, test loss = 0.177316
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 87/200. Took 1.1944 seconds. 
  Full-batch training loss = 0.003157, test loss = 0.177313
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 88/200. Took 1.1629 seconds. 
  Full-batch training loss = 0.003106, test loss = 0.177595
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 89/200. Took 1.2022 seconds. 
  Full-batch training loss = 0.003049, test loss = 0.177660
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 90/200. Took 1.1327 seconds. 
  Full-batch training loss = 0.003001, test loss = 0.177824
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 91/200. Took 1.2301 seconds. 
  Full-batch training loss = 0.002948, test loss = 0.178011
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 92/200. Took 1.2339 seconds. 
  Full-batch training loss = 0.002903, test loss = 0.178545
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 93/200. Took 1.2221 seconds. 
  Full-batch training loss = 0.002859, test loss = 0.178478
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 94/200. Took 1.1032 seconds. 
  Full-batch training loss = 0.002812, test loss = 0.178659
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 95/200. Took 1.1653 seconds. 
  Full-batch training loss = 0.002775, test loss = 0.178913
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 96/200. Took 1.1906 seconds. 
  Full-batch training loss = 0.002725, test loss = 0.179069
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 97/200. Took 1.1999 seconds. 
  Full-batch training loss = 0.002687, test loss = 0.179506
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 98/200. Took 1.2041 seconds. 
  Full-batch training loss = 0.002654, test loss = 0.179548
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 99/200. Took 1.1059 seconds. 
  Full-batch training loss = 0.002608, test loss = 0.179508
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 100/200. Took 1.0911 seconds. 
  Full-batch training loss = 0.002569, test loss = 0.179935
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 101/200. Took 1.2633 seconds. 
  Full-batch training loss = 0.002534, test loss = 0.179890
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 102/200. Took 1.2128 seconds. 
  Full-batch training loss = 0.002494, test loss = 0.180143
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 103/200. Took 1.275 seconds. 
  Full-batch training loss = 0.002460, test loss = 0.180021
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 104/200. Took 1.1991 seconds. 
  Full-batch training loss = 0.002428, test loss = 0.180354
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 105/200. Took 1.1446 seconds. 
  Full-batch training loss = 0.002393, test loss = 0.180663
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 106/200. Took 1.1316 seconds. 
  Full-batch training loss = 0.002362, test loss = 0.180524
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 107/200. Took 1.1861 seconds. 
  Full-batch training loss = 0.002331, test loss = 0.180983
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 108/200. Took 1.2033 seconds. 
  Full-batch training loss = 0.002299, test loss = 0.181121
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 109/200. Took 1.1498 seconds. 
  Full-batch training loss = 0.002269, test loss = 0.181230
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 110/200. Took 1.1968 seconds. 
  Full-batch training loss = 0.002237, test loss = 0.181274
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 111/200. Took 1.211 seconds. 
  Full-batch training loss = 0.002212, test loss = 0.181408
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 112/200. Took 1.1954 seconds. 
  Full-batch training loss = 0.002183, test loss = 0.181602
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 113/200. Took 1.274 seconds. 
  Full-batch training loss = 0.002154, test loss = 0.181950
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 114/200. Took 1.2032 seconds. 
  Full-batch training loss = 0.002129, test loss = 0.181950
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 115/200. Took 1.2533 seconds. 
  Full-batch training loss = 0.002100, test loss = 0.182106
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 116/200. Took 1.1496 seconds. 
  Full-batch training loss = 0.002076, test loss = 0.182156
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 117/200. Took 1.1929 seconds. 
  Full-batch training loss = 0.002052, test loss = 0.182425
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 118/200. Took 1.2427 seconds. 
  Full-batch training loss = 0.002032, test loss = 0.182734
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 119/200. Took 1.3247 seconds. 
  Full-batch training loss = 0.002003, test loss = 0.182677
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 120/200. Took 1.1563 seconds. 
  Full-batch training loss = 0.001979, test loss = 0.182830
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 121/200. Took 1.2524 seconds. 
  Full-batch training loss = 0.001958, test loss = 0.182905
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 122/200. Took 1.1488 seconds. 
  Full-batch training loss = 0.001934, test loss = 0.183059
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 123/200. Took 1.3063 seconds. 
  Full-batch training loss = 0.001912, test loss = 0.183363
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 124/200. Took 1.1334 seconds. 
  Full-batch training loss = 0.001891, test loss = 0.183451
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 125/200. Took 1.3355 seconds. 
  Full-batch training loss = 0.001871, test loss = 0.183396
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 126/200. Took 1.171 seconds. 
  Full-batch training loss = 0.001849, test loss = 0.183612
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 127/200. Took 1.2142 seconds. 
  Full-batch training loss = 0.001831, test loss = 0.183768
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 128/200. Took 1.2829 seconds. 
  Full-batch training loss = 0.001809, test loss = 0.183987
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 129/200. Took 1.185 seconds. 
  Full-batch training loss = 0.001789, test loss = 0.184091
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 130/200. Took 1.1754 seconds. 
  Full-batch training loss = 0.001770, test loss = 0.184187
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 131/200. Took 1.1174 seconds. 
  Full-batch training loss = 0.001752, test loss = 0.184448
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 132/200. Took 1.1573 seconds. 
  Full-batch training loss = 0.001735, test loss = 0.184505
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 133/200. Took 1.2915 seconds. 
  Full-batch training loss = 0.001715, test loss = 0.184563
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 134/200. Took 1.1935 seconds. 
  Full-batch training loss = 0.001699, test loss = 0.184767
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 135/200. Took 1.1229 seconds. 
  Full-batch training loss = 0.001682, test loss = 0.184770
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 136/200. Took 1.2402 seconds. 
  Full-batch training loss = 0.001664, test loss = 0.184841
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 137/200. Took 1.3519 seconds. 
  Full-batch training loss = 0.001647, test loss = 0.185096
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 138/200. Took 1.2079 seconds. 
  Full-batch training loss = 0.001632, test loss = 0.185258
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 139/200. Took 1.189 seconds. 
  Full-batch training loss = 0.001615, test loss = 0.185470
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 140/200. Took 1.1327 seconds. 
  Full-batch training loss = 0.001600, test loss = 0.185423
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 141/200. Took 1.2225 seconds. 
  Full-batch training loss = 0.001584, test loss = 0.185505
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 142/200. Took 1.1983 seconds. 
  Full-batch training loss = 0.001569, test loss = 0.185661
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 143/200. Took 1.2462 seconds. 
  Full-batch training loss = 0.001555, test loss = 0.185825
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 144/200. Took 1.1108 seconds. 
  Full-batch training loss = 0.001539, test loss = 0.185984
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 145/200. Took 1.2912 seconds. 
  Full-batch training loss = 0.001525, test loss = 0.186023
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 146/200. Took 1.1531 seconds. 
  Full-batch training loss = 0.001511, test loss = 0.186207
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 147/200. Took 1.1397 seconds. 
  Full-batch training loss = 0.001499, test loss = 0.186104
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 148/200. Took 1.1929 seconds. 
  Full-batch training loss = 0.001484, test loss = 0.186468
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 149/200. Took 1.1843 seconds. 
  Full-batch training loss = 0.001470, test loss = 0.186466
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 150/200. Took 1.2659 seconds. 
  Full-batch training loss = 0.001456, test loss = 0.186716
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 151/200. Took 1.1899 seconds. 
  Full-batch training loss = 0.001444, test loss = 0.186752
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 152/200. Took 1.1162 seconds. 
  Full-batch training loss = 0.001430, test loss = 0.186872
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 153/200. Took 1.1605 seconds. 
  Full-batch training loss = 0.001418, test loss = 0.187090
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 154/200. Took 1.1771 seconds. 
  Full-batch training loss = 0.001406, test loss = 0.187173
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 155/200. Took 1.179 seconds. 
  Full-batch training loss = 0.001393, test loss = 0.187112
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 156/200. Took 1.2275 seconds. 
  Full-batch training loss = 0.001382, test loss = 0.187237
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 157/200. Took 1.1227 seconds. 
  Full-batch training loss = 0.001370, test loss = 0.187440
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 158/200. Took 1.2678 seconds. 
  Full-batch training loss = 0.001358, test loss = 0.187473
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 159/200. Took 1.1358 seconds. 
  Full-batch training loss = 0.001347, test loss = 0.187530
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 160/200. Took 1.1647 seconds. 
  Full-batch training loss = 0.001335, test loss = 0.187718
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 161/200. Took 1.2015 seconds. 
  Full-batch training loss = 0.001324, test loss = 0.187859
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 162/200. Took 1.2177 seconds. 
  Full-batch training loss = 0.001314, test loss = 0.187980
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 163/200. Took 1.1722 seconds. 
  Full-batch training loss = 0.001303, test loss = 0.188093
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 164/200. Took 1.1038 seconds. 
  Full-batch training loss = 0.001292, test loss = 0.188155
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 165/200. Took 1.2385 seconds. 
  Full-batch training loss = 0.001282, test loss = 0.188262
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 166/200. Took 1.182 seconds. 
  Full-batch training loss = 0.001272, test loss = 0.188317
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 167/200. Took 1.1382 seconds. 
  Full-batch training loss = 0.001261, test loss = 0.188415
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 168/200. Took 1.2971 seconds. 
  Full-batch training loss = 0.001251, test loss = 0.188509
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 169/200. Took 1.2846 seconds. 
  Full-batch training loss = 0.001241, test loss = 0.188653
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 170/200. Took 1.1739 seconds. 
  Full-batch training loss = 0.001232, test loss = 0.188693
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 171/200. Took 1.2954 seconds. 
  Full-batch training loss = 0.001222, test loss = 0.188795
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 172/200. Took 1.2388 seconds. 
  Full-batch training loss = 0.001212, test loss = 0.188953
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 173/200. Took 1.1103 seconds. 
  Full-batch training loss = 0.001203, test loss = 0.189012
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 174/200. Took 1.1527 seconds. 
  Full-batch training loss = 0.001194, test loss = 0.189198
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 175/200. Took 1.1199 seconds. 
  Full-batch training loss = 0.001185, test loss = 0.189196
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 176/200. Took 1.1367 seconds. 
  Full-batch training loss = 0.001176, test loss = 0.189355
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 177/200. Took 1.1697 seconds. 
  Full-batch training loss = 0.001167, test loss = 0.189455
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 178/200. Took 1.1855 seconds. 
  Full-batch training loss = 0.001159, test loss = 0.189502
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 179/200. Took 1.1602 seconds. 
  Full-batch training loss = 0.001150, test loss = 0.189604
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 180/200. Took 1.2012 seconds. 
  Full-batch training loss = 0.001142, test loss = 0.189668
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 181/200. Took 1.3133 seconds. 
  Full-batch training loss = 0.001134, test loss = 0.189834
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 182/200. Took 1.2271 seconds. 
  Full-batch training loss = 0.001125, test loss = 0.189857
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 183/200. Took 1.0985 seconds. 
  Full-batch training loss = 0.001117, test loss = 0.189946
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 184/200. Took 1.1545 seconds. 
  Full-batch training loss = 0.001109, test loss = 0.190050
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 185/200. Took 1.1629 seconds. 
  Full-batch training loss = 0.001102, test loss = 0.190220
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 186/200. Took 1.2768 seconds. 
  Full-batch training loss = 0.001094, test loss = 0.190257
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 187/200. Took 1.177 seconds. 
  Full-batch training loss = 0.001086, test loss = 0.190376
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 188/200. Took 1.1043 seconds. 
  Full-batch training loss = 0.001078, test loss = 0.190448
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 189/200. Took 1.1626 seconds. 
  Full-batch training loss = 0.001071, test loss = 0.190551
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 190/200. Took 1.1712 seconds. 
  Full-batch training loss = 0.001064, test loss = 0.190626
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 191/200. Took 1.1606 seconds. 
  Full-batch training loss = 0.001056, test loss = 0.190724
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 192/200. Took 1.3287 seconds. 
  Full-batch training loss = 0.001049, test loss = 0.190790
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 193/200. Took 1.2045 seconds. 
  Full-batch training loss = 0.001042, test loss = 0.190962
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 194/200. Took 1.2423 seconds. 
  Full-batch training loss = 0.001035, test loss = 0.190984
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 195/200. Took 1.1356 seconds. 
  Full-batch training loss = 0.001028, test loss = 0.191059
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 196/200. Took 1.2274 seconds. 
  Full-batch training loss = 0.001021, test loss = 0.191130
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 197/200. Took 1.1584 seconds. 
  Full-batch training loss = 0.001014, test loss = 0.191206
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 198/200. Took 1.1661 seconds. 
  Full-batch training loss = 0.001008, test loss = 0.191309
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 199/200. Took 1.2038 seconds. 
  Full-batch training loss = 0.001001, test loss = 0.191421
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 200/200. Took 1.1793 seconds. 
  Full-batch training loss = 0.000995, test loss = 0.191552
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
Elapsed time is 567.415692 seconds.
End Training
