Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	100
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 100 epochs
 epoch 1/100. Took 1.5787 seconds. Average reconstruction error is: 29.8601
 epoch 2/100. Took 1.6523 seconds. Average reconstruction error is: 18.2624
 epoch 3/100. Took 1.6311 seconds. Average reconstruction error is: 15.9916
 epoch 4/100. Took 1.5807 seconds. Average reconstruction error is: 14.6117
 epoch 5/100. Took 1.5961 seconds. Average reconstruction error is: 13.5328
 epoch 6/100. Took 1.551 seconds. Average reconstruction error is: 12.7026
 epoch 7/100. Took 1.8731 seconds. Average reconstruction error is: 12.1389
 epoch 8/100. Took 1.6144 seconds. Average reconstruction error is: 11.6277
 epoch 9/100. Took 1.7199 seconds. Average reconstruction error is: 11.2786
 epoch 10/100. Took 1.634 seconds. Average reconstruction error is: 10.9279
 epoch 11/100. Took 1.7084 seconds. Average reconstruction error is: 10.6772
 epoch 12/100. Took 1.6522 seconds. Average reconstruction error is: 10.4961
 epoch 13/100. Took 1.5856 seconds. Average reconstruction error is: 10.3011
 epoch 14/100. Took 1.6622 seconds. Average reconstruction error is: 10.1487
 epoch 15/100. Took 1.6185 seconds. Average reconstruction error is: 10.0176
 epoch 16/100. Took 1.63 seconds. Average reconstruction error is: 9.9066
 epoch 17/100. Took 1.6466 seconds. Average reconstruction error is: 9.7784
 epoch 18/100. Took 1.6153 seconds. Average reconstruction error is: 9.682
 epoch 19/100. Took 1.5785 seconds. Average reconstruction error is: 9.6143
 epoch 20/100. Took 1.6783 seconds. Average reconstruction error is: 9.5039
 epoch 21/100. Took 1.6274 seconds. Average reconstruction error is: 9.4735
 epoch 22/100. Took 1.625 seconds. Average reconstruction error is: 9.3938
 epoch 23/100. Took 1.6686 seconds. Average reconstruction error is: 9.3534
 epoch 24/100. Took 1.6378 seconds. Average reconstruction error is: 9.2869
 epoch 25/100. Took 1.6831 seconds. Average reconstruction error is: 9.2168
 epoch 26/100. Took 1.6881 seconds. Average reconstruction error is: 9.1639
 epoch 27/100. Took 1.5832 seconds. Average reconstruction error is: 9.0884
 epoch 28/100. Took 1.6151 seconds. Average reconstruction error is: 9.0438
 epoch 29/100. Took 1.603 seconds. Average reconstruction error is: 9.0163
 epoch 30/100. Took 1.6179 seconds. Average reconstruction error is: 8.9578
 epoch 31/100. Took 1.7267 seconds. Average reconstruction error is: 8.8963
 epoch 32/100. Took 1.6586 seconds. Average reconstruction error is: 8.913
 epoch 33/100. Took 1.6362 seconds. Average reconstruction error is: 8.8748
 epoch 34/100. Took 1.5725 seconds. Average reconstruction error is: 8.8323
 epoch 35/100. Took 1.684 seconds. Average reconstruction error is: 8.7837
 epoch 36/100. Took 1.5807 seconds. Average reconstruction error is: 8.7619
 epoch 37/100. Took 1.5831 seconds. Average reconstruction error is: 8.7449
 epoch 38/100. Took 1.6468 seconds. Average reconstruction error is: 8.7283
 epoch 39/100. Took 1.6057 seconds. Average reconstruction error is: 8.6975
 epoch 40/100. Took 1.6408 seconds. Average reconstruction error is: 8.6271
 epoch 41/100. Took 1.6258 seconds. Average reconstruction error is: 8.6264
 epoch 42/100. Took 1.8429 seconds. Average reconstruction error is: 8.6011
 epoch 43/100. Took 1.7158 seconds. Average reconstruction error is: 8.5814
 epoch 44/100. Took 1.6614 seconds. Average reconstruction error is: 8.5543
 epoch 45/100. Took 1.7635 seconds. Average reconstruction error is: 8.5554
 epoch 46/100. Took 1.5759 seconds. Average reconstruction error is: 8.5459
 epoch 47/100. Took 1.5533 seconds. Average reconstruction error is: 8.5228
 epoch 48/100. Took 1.6922 seconds. Average reconstruction error is: 8.5144
 epoch 49/100. Took 1.5993 seconds. Average reconstruction error is: 8.4921
 epoch 50/100. Took 1.62 seconds. Average reconstruction error is: 8.4624
 epoch 51/100. Took 1.5445 seconds. Average reconstruction error is: 8.4492
 epoch 52/100. Took 1.6531 seconds. Average reconstruction error is: 8.4187
 epoch 53/100. Took 1.6361 seconds. Average reconstruction error is: 8.4171
 epoch 54/100. Took 1.6398 seconds. Average reconstruction error is: 8.4275
 epoch 55/100. Took 1.6433 seconds. Average reconstruction error is: 8.4016
 epoch 56/100. Took 1.641 seconds. Average reconstruction error is: 8.3552
 epoch 57/100. Took 1.6468 seconds. Average reconstruction error is: 8.3525
 epoch 58/100. Took 1.6895 seconds. Average reconstruction error is: 8.334
 epoch 59/100. Took 1.5807 seconds. Average reconstruction error is: 8.3197
 epoch 60/100. Took 1.5714 seconds. Average reconstruction error is: 8.3063
 epoch 61/100. Took 1.68 seconds. Average reconstruction error is: 8.2949
 epoch 62/100. Took 1.5878 seconds. Average reconstruction error is: 8.2824
 epoch 63/100. Took 1.5077 seconds. Average reconstruction error is: 8.2662
 epoch 64/100. Took 1.6306 seconds. Average reconstruction error is: 8.2498
 epoch 65/100. Took 1.634 seconds. Average reconstruction error is: 8.2666
 epoch 66/100. Took 1.6154 seconds. Average reconstruction error is: 8.221
 epoch 67/100. Took 1.7158 seconds. Average reconstruction error is: 8.1986
 epoch 68/100. Took 1.6121 seconds. Average reconstruction error is: 8.181
 epoch 69/100. Took 1.6244 seconds. Average reconstruction error is: 8.2158
 epoch 70/100. Took 1.6077 seconds. Average reconstruction error is: 8.1832
 epoch 71/100. Took 1.6121 seconds. Average reconstruction error is: 8.1741
 epoch 72/100. Took 1.6688 seconds. Average reconstruction error is: 8.1624
 epoch 73/100. Took 1.6777 seconds. Average reconstruction error is: 8.152
 epoch 74/100. Took 1.5919 seconds. Average reconstruction error is: 8.1655
 epoch 75/100. Took 1.6901 seconds. Average reconstruction error is: 8.1426
 epoch 76/100. Took 1.5893 seconds. Average reconstruction error is: 8.1404
 epoch 77/100. Took 1.6681 seconds. Average reconstruction error is: 8.112
 epoch 78/100. Took 1.6413 seconds. Average reconstruction error is: 8.0909
 epoch 79/100. Took 1.7199 seconds. Average reconstruction error is: 8.0937
 epoch 80/100. Took 1.6318 seconds. Average reconstruction error is: 8.0897
 epoch 81/100. Took 1.6186 seconds. Average reconstruction error is: 8.0808
 epoch 82/100. Took 1.653 seconds. Average reconstruction error is: 8.0459
 epoch 83/100. Took 1.6755 seconds. Average reconstruction error is: 8.0774
 epoch 84/100. Took 1.7061 seconds. Average reconstruction error is: 8.0515
 epoch 85/100. Took 1.6144 seconds. Average reconstruction error is: 8.0259
 epoch 86/100. Took 1.6339 seconds. Average reconstruction error is: 8.0627
 epoch 87/100. Took 1.6838 seconds. Average reconstruction error is: 8.0125
 epoch 88/100. Took 1.6955 seconds. Average reconstruction error is: 8.0234
 epoch 89/100. Took 1.5914 seconds. Average reconstruction error is: 7.9928
 epoch 90/100. Took 1.6299 seconds. Average reconstruction error is: 7.984
 epoch 91/100. Took 1.554 seconds. Average reconstruction error is: 7.9766
 epoch 92/100. Took 1.6696 seconds. Average reconstruction error is: 7.9865
 epoch 93/100. Took 1.6568 seconds. Average reconstruction error is: 7.9791
 epoch 94/100. Took 1.6992 seconds. Average reconstruction error is: 7.9747
 epoch 95/100. Took 1.7101 seconds. Average reconstruction error is: 7.9506
 epoch 96/100. Took 1.6866 seconds. Average reconstruction error is: 7.9597
 epoch 97/100. Took 1.6234 seconds. Average reconstruction error is: 7.9652
 epoch 98/100. Took 1.6101 seconds. Average reconstruction error is: 7.947
 epoch 99/100. Took 1.5126 seconds. Average reconstruction error is: 7.9396
 epoch 100/100. Took 1.6302 seconds. Average reconstruction error is: 7.9425
Training binary-binary RBM in layer 2 (200  200) with CD1 for 100 epochs
 epoch 1/100. Took 0.5593 seconds. Average reconstruction error is: 15.3132
 epoch 2/100. Took 0.45587 seconds. Average reconstruction error is: 9.336
 epoch 3/100. Took 0.49117 seconds. Average reconstruction error is: 8.2477
 epoch 4/100. Took 0.4762 seconds. Average reconstruction error is: 7.7114
 epoch 5/100. Took 0.46629 seconds. Average reconstruction error is: 7.3508
 epoch 6/100. Took 0.47582 seconds. Average reconstruction error is: 7.117
 epoch 7/100. Took 0.52186 seconds. Average reconstruction error is: 6.8815
 epoch 8/100. Took 0.49222 seconds. Average reconstruction error is: 6.6713
 epoch 9/100. Took 0.53261 seconds. Average reconstruction error is: 6.5375
 epoch 10/100. Took 0.49378 seconds. Average reconstruction error is: 6.4048
 epoch 11/100. Took 0.51469 seconds. Average reconstruction error is: 6.3035
 epoch 12/100. Took 0.54779 seconds. Average reconstruction error is: 6.2203
 epoch 13/100. Took 0.50322 seconds. Average reconstruction error is: 6.1084
 epoch 14/100. Took 0.52848 seconds. Average reconstruction error is: 6.0145
 epoch 15/100. Took 0.49419 seconds. Average reconstruction error is: 5.9475
 epoch 16/100. Took 0.48399 seconds. Average reconstruction error is: 5.8698
 epoch 17/100. Took 0.56706 seconds. Average reconstruction error is: 5.7945
 epoch 18/100. Took 0.48881 seconds. Average reconstruction error is: 5.7546
 epoch 19/100. Took 0.55123 seconds. Average reconstruction error is: 5.7029
 epoch 20/100. Took 0.52182 seconds. Average reconstruction error is: 5.6139
 epoch 21/100. Took 0.48335 seconds. Average reconstruction error is: 5.5898
 epoch 22/100. Took 0.51492 seconds. Average reconstruction error is: 5.5033
 epoch 23/100. Took 0.49358 seconds. Average reconstruction error is: 5.5011
 epoch 24/100. Took 0.49945 seconds. Average reconstruction error is: 5.4527
 epoch 25/100. Took 0.50847 seconds. Average reconstruction error is: 5.3784
 epoch 26/100. Took 0.47992 seconds. Average reconstruction error is: 5.3558
 epoch 27/100. Took 0.47818 seconds. Average reconstruction error is: 5.3202
 epoch 28/100. Took 0.51102 seconds. Average reconstruction error is: 5.2897
 epoch 29/100. Took 0.46335 seconds. Average reconstruction error is: 5.252
 epoch 30/100. Took 0.49734 seconds. Average reconstruction error is: 5.2525
 epoch 31/100. Took 0.49635 seconds. Average reconstruction error is: 5.1802
 epoch 32/100. Took 0.4929 seconds. Average reconstruction error is: 5.1503
 epoch 33/100. Took 0.51451 seconds. Average reconstruction error is: 5.1219
 epoch 34/100. Took 0.51005 seconds. Average reconstruction error is: 5.1253
 epoch 35/100. Took 0.54351 seconds. Average reconstruction error is: 5.0873
 epoch 36/100. Took 0.49317 seconds. Average reconstruction error is: 5.0238
 epoch 37/100. Took 0.4829 seconds. Average reconstruction error is: 5.0221
 epoch 38/100. Took 0.49105 seconds. Average reconstruction error is: 4.9955
 epoch 39/100. Took 0.52648 seconds. Average reconstruction error is: 4.986
 epoch 40/100. Took 0.52849 seconds. Average reconstruction error is: 4.939
 epoch 41/100. Took 0.51701 seconds. Average reconstruction error is: 4.9029
 epoch 42/100. Took 0.57252 seconds. Average reconstruction error is: 4.8868
 epoch 43/100. Took 0.51441 seconds. Average reconstruction error is: 4.8718
 epoch 44/100. Took 0.49158 seconds. Average reconstruction error is: 4.8626
 epoch 45/100. Took 0.48434 seconds. Average reconstruction error is: 4.8189
 epoch 46/100. Took 0.50429 seconds. Average reconstruction error is: 4.8253
 epoch 47/100. Took 0.47931 seconds. Average reconstruction error is: 4.7866
 epoch 48/100. Took 0.50575 seconds. Average reconstruction error is: 4.7904
 epoch 49/100. Took 0.47566 seconds. Average reconstruction error is: 4.7512
 epoch 50/100. Took 0.51849 seconds. Average reconstruction error is: 4.7653
 epoch 51/100. Took 0.52353 seconds. Average reconstruction error is: 4.7718
 epoch 52/100. Took 0.49692 seconds. Average reconstruction error is: 4.747
 epoch 53/100. Took 0.4742 seconds. Average reconstruction error is: 4.7459
 epoch 54/100. Took 0.50947 seconds. Average reconstruction error is: 4.7254
 epoch 55/100. Took 0.53562 seconds. Average reconstruction error is: 4.699
 epoch 56/100. Took 0.5025 seconds. Average reconstruction error is: 4.6755
 epoch 57/100. Took 0.48373 seconds. Average reconstruction error is: 4.6832
 epoch 58/100. Took 0.47105 seconds. Average reconstruction error is: 4.6751
 epoch 59/100. Took 0.4843 seconds. Average reconstruction error is: 4.6362
 epoch 60/100. Took 0.48581 seconds. Average reconstruction error is: 4.6261
 epoch 61/100. Took 0.49162 seconds. Average reconstruction error is: 4.6416
 epoch 62/100. Took 0.50186 seconds. Average reconstruction error is: 4.6424
 epoch 63/100. Took 0.52998 seconds. Average reconstruction error is: 4.6167
 epoch 64/100. Took 0.52075 seconds. Average reconstruction error is: 4.5826
 epoch 65/100. Took 0.51095 seconds. Average reconstruction error is: 4.5945
 epoch 66/100. Took 0.51621 seconds. Average reconstruction error is: 4.5824
 epoch 67/100. Took 0.49939 seconds. Average reconstruction error is: 4.5621
 epoch 68/100. Took 0.50745 seconds. Average reconstruction error is: 4.5482
 epoch 69/100. Took 0.55922 seconds. Average reconstruction error is: 4.5209
 epoch 70/100. Took 0.50591 seconds. Average reconstruction error is: 4.5199
 epoch 71/100. Took 0.54976 seconds. Average reconstruction error is: 4.5029
 epoch 72/100. Took 0.54762 seconds. Average reconstruction error is: 4.4691
 epoch 73/100. Took 0.53624 seconds. Average reconstruction error is: 4.5078
 epoch 74/100. Took 0.48903 seconds. Average reconstruction error is: 4.463
 epoch 75/100. Took 0.50637 seconds. Average reconstruction error is: 4.4723
 epoch 76/100. Took 0.48208 seconds. Average reconstruction error is: 4.4489
 epoch 77/100. Took 0.4837 seconds. Average reconstruction error is: 4.4447
 epoch 78/100. Took 0.57748 seconds. Average reconstruction error is: 4.4352
 epoch 79/100. Took 0.50356 seconds. Average reconstruction error is: 4.4502
 epoch 80/100. Took 0.46613 seconds. Average reconstruction error is: 4.4345
 epoch 81/100. Took 0.50253 seconds. Average reconstruction error is: 4.4348
 epoch 82/100. Took 0.48316 seconds. Average reconstruction error is: 4.4544
 epoch 83/100. Took 0.52449 seconds. Average reconstruction error is: 4.4142
 epoch 84/100. Took 0.47763 seconds. Average reconstruction error is: 4.4221
 epoch 85/100. Took 0.49476 seconds. Average reconstruction error is: 4.4229
 epoch 86/100. Took 0.56032 seconds. Average reconstruction error is: 4.3973
 epoch 87/100. Took 0.47238 seconds. Average reconstruction error is: 4.4158
 epoch 88/100. Took 0.48488 seconds. Average reconstruction error is: 4.3985
 epoch 89/100. Took 0.47656 seconds. Average reconstruction error is: 4.3583
 epoch 90/100. Took 0.47024 seconds. Average reconstruction error is: 4.3992
 epoch 91/100. Took 0.4714 seconds. Average reconstruction error is: 4.3501
 epoch 92/100. Took 0.51414 seconds. Average reconstruction error is: 4.3659
 epoch 93/100. Took 0.50962 seconds. Average reconstruction error is: 4.3556
 epoch 94/100. Took 0.52073 seconds. Average reconstruction error is: 4.3579
 epoch 95/100. Took 0.48261 seconds. Average reconstruction error is: 4.3629
 epoch 96/100. Took 0.53796 seconds. Average reconstruction error is: 4.3429
 epoch 97/100. Took 0.46832 seconds. Average reconstruction error is: 4.3454
 epoch 98/100. Took 0.51403 seconds. Average reconstruction error is: 4.3356
 epoch 99/100. Took 0.49306 seconds. Average reconstruction error is: 4.3527
 epoch 100/100. Took 0.52831 seconds. Average reconstruction error is: 4.3334
Training NN  (784  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 1.1532 seconds. 
  Full-batch training loss = 0.308394, test loss = 0.302523
  Training set accuracy = 0.912000, Test set accuracy = 0.915500
 epoch 2/100. Took 1.1988 seconds. 
  Full-batch training loss = 0.261347, test loss = 0.268178
  Training set accuracy = 0.928500, Test set accuracy = 0.925000
 epoch 3/100. Took 1.1892 seconds. 
  Full-batch training loss = 0.219485, test loss = 0.241940
  Training set accuracy = 0.937000, Test set accuracy = 0.929900
 epoch 4/100. Took 1.2016 seconds. 
  Full-batch training loss = 0.202321, test loss = 0.240246
  Training set accuracy = 0.942100, Test set accuracy = 0.929600
 epoch 5/100. Took 1.2101 seconds. 
  Full-batch training loss = 0.178405, test loss = 0.221197
  Training set accuracy = 0.951500, Test set accuracy = 0.936600
 epoch 6/100. Took 1.2192 seconds. 
  Full-batch training loss = 0.147377, test loss = 0.200420
  Training set accuracy = 0.962400, Test set accuracy = 0.941000
 epoch 7/100. Took 1.2827 seconds. 
  Full-batch training loss = 0.133095, test loss = 0.195122
  Training set accuracy = 0.966800, Test set accuracy = 0.943400
 epoch 8/100. Took 1.1864 seconds. 
  Full-batch training loss = 0.129465, test loss = 0.194953
  Training set accuracy = 0.965500, Test set accuracy = 0.941700
 epoch 9/100. Took 1.2752 seconds. 
  Full-batch training loss = 0.110990, test loss = 0.187545
  Training set accuracy = 0.972800, Test set accuracy = 0.944900
 epoch 10/100. Took 1.1733 seconds. 
  Full-batch training loss = 0.103076, test loss = 0.181695
  Training set accuracy = 0.974200, Test set accuracy = 0.944300
 epoch 11/100. Took 1.1878 seconds. 
  Full-batch training loss = 0.090842, test loss = 0.177015
  Training set accuracy = 0.979100, Test set accuracy = 0.947300
 epoch 12/100. Took 1.2375 seconds. 
  Full-batch training loss = 0.085396, test loss = 0.176341
  Training set accuracy = 0.980600, Test set accuracy = 0.946000
 epoch 13/100. Took 1.1711 seconds. 
  Full-batch training loss = 0.077469, test loss = 0.175305
  Training set accuracy = 0.982700, Test set accuracy = 0.948700
 epoch 14/100. Took 1.2243 seconds. 
  Full-batch training loss = 0.073616, test loss = 0.177111
  Training set accuracy = 0.983400, Test set accuracy = 0.946200
 epoch 15/100. Took 1.173 seconds. 
  Full-batch training loss = 0.066460, test loss = 0.167656
  Training set accuracy = 0.985400, Test set accuracy = 0.950200
 epoch 16/100. Took 1.204 seconds. 
  Full-batch training loss = 0.060994, test loss = 0.173243
  Training set accuracy = 0.987900, Test set accuracy = 0.949500
 epoch 17/100. Took 1.144 seconds. 
  Full-batch training loss = 0.053900, test loss = 0.168480
  Training set accuracy = 0.990200, Test set accuracy = 0.949700
 epoch 18/100. Took 1.2512 seconds. 
  Full-batch training loss = 0.049947, test loss = 0.164373
  Training set accuracy = 0.991400, Test set accuracy = 0.951200
 epoch 19/100. Took 1.1684 seconds. 
  Full-batch training loss = 0.046209, test loss = 0.165644
  Training set accuracy = 0.992500, Test set accuracy = 0.951200
 epoch 20/100. Took 1.179 seconds. 
  Full-batch training loss = 0.040982, test loss = 0.160505
  Training set accuracy = 0.993500, Test set accuracy = 0.952700
 epoch 21/100. Took 1.2199 seconds. 
  Full-batch training loss = 0.037105, test loss = 0.160055
  Training set accuracy = 0.994800, Test set accuracy = 0.952200
 epoch 22/100. Took 1.1732 seconds. 
  Full-batch training loss = 0.034815, test loss = 0.161017
  Training set accuracy = 0.995500, Test set accuracy = 0.952900
 epoch 23/100. Took 1.1699 seconds. 
  Full-batch training loss = 0.032624, test loss = 0.160277
  Training set accuracy = 0.996100, Test set accuracy = 0.951700
 epoch 24/100. Took 1.183 seconds. 
  Full-batch training loss = 0.030382, test loss = 0.162836
  Training set accuracy = 0.996800, Test set accuracy = 0.951800
 epoch 25/100. Took 1.1688 seconds. 
  Full-batch training loss = 0.028036, test loss = 0.161409
  Training set accuracy = 0.997300, Test set accuracy = 0.951500
 epoch 26/100. Took 1.1554 seconds. 
  Full-batch training loss = 0.026131, test loss = 0.160238
  Training set accuracy = 0.997700, Test set accuracy = 0.952900
 epoch 27/100. Took 1.152 seconds. 
  Full-batch training loss = 0.024663, test loss = 0.162084
  Training set accuracy = 0.997700, Test set accuracy = 0.952500
 epoch 28/100. Took 1.1992 seconds. 
  Full-batch training loss = 0.022410, test loss = 0.158492
  Training set accuracy = 0.998400, Test set accuracy = 0.953700
 epoch 29/100. Took 1.2611 seconds. 
  Full-batch training loss = 0.020781, test loss = 0.159206
  Training set accuracy = 0.998500, Test set accuracy = 0.953500
 epoch 30/100. Took 1.2148 seconds. 
  Full-batch training loss = 0.019364, test loss = 0.159732
  Training set accuracy = 0.998900, Test set accuracy = 0.953400
 epoch 31/100. Took 1.1517 seconds. 
  Full-batch training loss = 0.017974, test loss = 0.159989
  Training set accuracy = 0.998900, Test set accuracy = 0.953100
 epoch 32/100. Took 1.1314 seconds. 
  Full-batch training loss = 0.017118, test loss = 0.159197
  Training set accuracy = 0.999100, Test set accuracy = 0.954500
 epoch 33/100. Took 1.2814 seconds. 
  Full-batch training loss = 0.016225, test loss = 0.160909
  Training set accuracy = 0.999100, Test set accuracy = 0.952300
 epoch 34/100. Took 1.1428 seconds. 
  Full-batch training loss = 0.015270, test loss = 0.159181
  Training set accuracy = 0.999500, Test set accuracy = 0.953900
 epoch 35/100. Took 1.235 seconds. 
  Full-batch training loss = 0.014371, test loss = 0.158922
  Training set accuracy = 0.999700, Test set accuracy = 0.954100
 epoch 36/100. Took 1.1209 seconds. 
  Full-batch training loss = 0.013677, test loss = 0.159603
  Training set accuracy = 0.999800, Test set accuracy = 0.954400
 epoch 37/100. Took 1.1324 seconds. 
  Full-batch training loss = 0.013221, test loss = 0.161504
  Training set accuracy = 0.999700, Test set accuracy = 0.953200
 epoch 38/100. Took 1.1986 seconds. 
  Full-batch training loss = 0.012204, test loss = 0.159944
  Training set accuracy = 0.999800, Test set accuracy = 0.954300
 epoch 39/100. Took 1.0967 seconds. 
  Full-batch training loss = 0.011809, test loss = 0.161814
  Training set accuracy = 0.999900, Test set accuracy = 0.953600
 epoch 40/100. Took 1.2324 seconds. 
  Full-batch training loss = 0.011227, test loss = 0.160505
  Training set accuracy = 0.999900, Test set accuracy = 0.953800
 epoch 41/100. Took 1.1269 seconds. 
  Full-batch training loss = 0.010607, test loss = 0.160749
  Training set accuracy = 0.999900, Test set accuracy = 0.954000
 epoch 42/100. Took 1.114 seconds. 
  Full-batch training loss = 0.010272, test loss = 0.162013
  Training set accuracy = 0.999900, Test set accuracy = 0.953900
 epoch 43/100. Took 1.105 seconds. 
  Full-batch training loss = 0.009797, test loss = 0.161482
  Training set accuracy = 0.999900, Test set accuracy = 0.953800
 epoch 44/100. Took 1.189 seconds. 
  Full-batch training loss = 0.009385, test loss = 0.161498
  Training set accuracy = 0.999900, Test set accuracy = 0.954400
 epoch 45/100. Took 1.1869 seconds. 
  Full-batch training loss = 0.009037, test loss = 0.160598
  Training set accuracy = 0.999900, Test set accuracy = 0.954800
 epoch 46/100. Took 1.2571 seconds. 
  Full-batch training loss = 0.008730, test loss = 0.161141
  Training set accuracy = 0.999900, Test set accuracy = 0.954000
 epoch 47/100. Took 1.1632 seconds. 
  Full-batch training loss = 0.008350, test loss = 0.161661
  Training set accuracy = 0.999900, Test set accuracy = 0.954600
 epoch 48/100. Took 1.1403 seconds. 
  Full-batch training loss = 0.008024, test loss = 0.161602
  Training set accuracy = 0.999900, Test set accuracy = 0.954500
 epoch 49/100. Took 1.1677 seconds. 
  Full-batch training loss = 0.007767, test loss = 0.161747
  Training set accuracy = 0.999900, Test set accuracy = 0.954600
 epoch 50/100. Took 1.2405 seconds. 
  Full-batch training loss = 0.007562, test loss = 0.163661
  Training set accuracy = 1.000000, Test set accuracy = 0.954300
 epoch 51/100. Took 1.1925 seconds. 
  Full-batch training loss = 0.007264, test loss = 0.162456
  Training set accuracy = 1.000000, Test set accuracy = 0.954300
 epoch 52/100. Took 1.1422 seconds. 
  Full-batch training loss = 0.007066, test loss = 0.163126
  Training set accuracy = 1.000000, Test set accuracy = 0.954400
 epoch 53/100. Took 1.1719 seconds. 
  Full-batch training loss = 0.006813, test loss = 0.163113
  Training set accuracy = 1.000000, Test set accuracy = 0.954400
 epoch 54/100. Took 1.1442 seconds. 
  Full-batch training loss = 0.006593, test loss = 0.162729
  Training set accuracy = 1.000000, Test set accuracy = 0.954800
 epoch 55/100. Took 1.2363 seconds. 
  Full-batch training loss = 0.006570, test loss = 0.162518
  Training set accuracy = 0.999900, Test set accuracy = 0.954800
 epoch 56/100. Took 1.183 seconds. 
  Full-batch training loss = 0.006248, test loss = 0.164021
  Training set accuracy = 1.000000, Test set accuracy = 0.954300
 epoch 57/100. Took 1.1173 seconds. 
  Full-batch training loss = 0.006029, test loss = 0.163917
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 58/100. Took 1.1788 seconds. 
  Full-batch training loss = 0.005878, test loss = 0.163432
  Training set accuracy = 1.000000, Test set accuracy = 0.954500
 epoch 59/100. Took 1.1099 seconds. 
  Full-batch training loss = 0.005714, test loss = 0.163579
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 60/100. Took 1.1594 seconds. 
  Full-batch training loss = 0.005557, test loss = 0.164624
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 61/100. Took 1.2068 seconds. 
  Full-batch training loss = 0.005418, test loss = 0.164805
  Training set accuracy = 1.000000, Test set accuracy = 0.954800
 epoch 62/100. Took 1.1097 seconds. 
  Full-batch training loss = 0.005264, test loss = 0.164114
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 63/100. Took 1.1997 seconds. 
  Full-batch training loss = 0.005137, test loss = 0.163855
  Training set accuracy = 1.000000, Test set accuracy = 0.954900
 epoch 64/100. Took 1.1684 seconds. 
  Full-batch training loss = 0.005023, test loss = 0.165292
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 65/100. Took 1.1277 seconds. 
  Full-batch training loss = 0.004885, test loss = 0.164811
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 66/100. Took 1.1944 seconds. 
  Full-batch training loss = 0.004775, test loss = 0.165245
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 67/100. Took 1.1395 seconds. 
  Full-batch training loss = 0.004662, test loss = 0.165958
  Training set accuracy = 1.000000, Test set accuracy = 0.954300
 epoch 68/100. Took 1.1462 seconds. 
  Full-batch training loss = 0.004551, test loss = 0.165321
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 69/100. Took 1.1878 seconds. 
  Full-batch training loss = 0.004456, test loss = 0.165914
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 70/100. Took 1.1557 seconds. 
  Full-batch training loss = 0.004347, test loss = 0.165890
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 71/100. Took 1.2369 seconds. 
  Full-batch training loss = 0.004269, test loss = 0.166228
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 72/100. Took 1.2061 seconds. 
  Full-batch training loss = 0.004183, test loss = 0.165655
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 73/100. Took 1.1575 seconds. 
  Full-batch training loss = 0.004085, test loss = 0.166735
  Training set accuracy = 1.000000, Test set accuracy = 0.954900
 epoch 74/100. Took 1.2111 seconds. 
  Full-batch training loss = 0.004013, test loss = 0.166586
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 75/100. Took 1.2322 seconds. 
  Full-batch training loss = 0.003925, test loss = 0.166658
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 76/100. Took 1.1444 seconds. 
  Full-batch training loss = 0.003845, test loss = 0.166940
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 77/100. Took 1.2959 seconds. 
  Full-batch training loss = 0.003774, test loss = 0.167741
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 78/100. Took 1.1999 seconds. 
  Full-batch training loss = 0.003693, test loss = 0.167249
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 79/100. Took 1.1756 seconds. 
  Full-batch training loss = 0.003623, test loss = 0.167334
  Training set accuracy = 1.000000, Test set accuracy = 0.954900
 epoch 80/100. Took 1.149 seconds. 
  Full-batch training loss = 0.003552, test loss = 0.167254
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 81/100. Took 1.1508 seconds. 
  Full-batch training loss = 0.003488, test loss = 0.167625
  Training set accuracy = 1.000000, Test set accuracy = 0.954900
 epoch 82/100. Took 1.127 seconds. 
  Full-batch training loss = 0.003424, test loss = 0.167795
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 83/100. Took 1.304 seconds. 
  Full-batch training loss = 0.003363, test loss = 0.167982
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 84/100. Took 1.2 seconds. 
  Full-batch training loss = 0.003304, test loss = 0.168159
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 85/100. Took 1.1756 seconds. 
  Full-batch training loss = 0.003252, test loss = 0.168052
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 86/100. Took 1.1693 seconds. 
  Full-batch training loss = 0.003193, test loss = 0.168662
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 87/100. Took 1.2068 seconds. 
  Full-batch training loss = 0.003137, test loss = 0.168226
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 88/100. Took 1.1305 seconds. 
  Full-batch training loss = 0.003092, test loss = 0.168833
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 89/100. Took 1.2796 seconds. 
  Full-batch training loss = 0.003035, test loss = 0.168657
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 90/100. Took 1.1844 seconds. 
  Full-batch training loss = 0.003000, test loss = 0.169211
  Training set accuracy = 1.000000, Test set accuracy = 0.954900
 epoch 91/100. Took 1.1785 seconds. 
  Full-batch training loss = 0.002939, test loss = 0.169091
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 92/100. Took 1.1941 seconds. 
  Full-batch training loss = 0.002892, test loss = 0.169001
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 93/100. Took 1.2898 seconds. 
  Full-batch training loss = 0.002856, test loss = 0.168937
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 94/100. Took 1.165 seconds. 
  Full-batch training loss = 0.002804, test loss = 0.169143
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 95/100. Took 1.2203 seconds. 
  Full-batch training loss = 0.002760, test loss = 0.169431
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 96/100. Took 1.1674 seconds. 
  Full-batch training loss = 0.002722, test loss = 0.169367
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 97/100. Took 1.1172 seconds. 
  Full-batch training loss = 0.002687, test loss = 0.170348
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 98/100. Took 1.1698 seconds. 
  Full-batch training loss = 0.002641, test loss = 0.169915
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 99/100. Took 1.1261 seconds. 
  Full-batch training loss = 0.002601, test loss = 0.170111
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 100/100. Took 1.3261 seconds. 
  Full-batch training loss = 0.002568, test loss = 0.169978
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
Elapsed time is 476.008502 seconds.
End Training
