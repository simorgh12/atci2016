Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.10
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.6552 seconds. Average reconstruction error is: 29.6775
 epoch 2/20. Took 1.5613 seconds. Average reconstruction error is: 18.1259
 epoch 3/20. Took 1.571 seconds. Average reconstruction error is: 15.8414
 epoch 4/20. Took 1.6194 seconds. Average reconstruction error is: 14.3708
 epoch 5/20. Took 1.552 seconds. Average reconstruction error is: 13.4159
 epoch 6/20. Took 1.5216 seconds. Average reconstruction error is: 12.6037
 epoch 7/20. Took 1.6578 seconds. Average reconstruction error is: 11.9486
 epoch 8/20. Took 1.6113 seconds. Average reconstruction error is: 11.4698
 epoch 9/20. Took 1.6696 seconds. Average reconstruction error is: 11.1319
 epoch 10/20. Took 1.5189 seconds. Average reconstruction error is: 10.8141
 epoch 11/20. Took 1.5943 seconds. Average reconstruction error is: 10.6124
 epoch 12/20. Took 1.6277 seconds. Average reconstruction error is: 10.3958
 epoch 13/20. Took 1.5825 seconds. Average reconstruction error is: 10.2383
 epoch 14/20. Took 1.6563 seconds. Average reconstruction error is: 10.0566
 epoch 15/20. Took 1.5975 seconds. Average reconstruction error is: 9.9222
 epoch 16/20. Took 1.6553 seconds. Average reconstruction error is: 9.817
 epoch 17/20. Took 1.5222 seconds. Average reconstruction error is: 9.707
 epoch 18/20. Took 1.5797 seconds. Average reconstruction error is: 9.5851
 epoch 19/20. Took 1.5906 seconds. Average reconstruction error is: 9.5251
 epoch 20/20. Took 1.6157 seconds. Average reconstruction error is: 9.431
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.51537 seconds. Average reconstruction error is: 13.968
 epoch 2/20. Took 0.54785 seconds. Average reconstruction error is: 8.2524
 epoch 3/20. Took 0.53823 seconds. Average reconstruction error is: 7.2624
 epoch 4/20. Took 0.47389 seconds. Average reconstruction error is: 6.7775
 epoch 5/20. Took 0.50719 seconds. Average reconstruction error is: 6.5157
 epoch 6/20. Took 0.50483 seconds. Average reconstruction error is: 6.2854
 epoch 7/20. Took 0.49662 seconds. Average reconstruction error is: 6.1173
 epoch 8/20. Took 0.53388 seconds. Average reconstruction error is: 5.9733
 epoch 9/20. Took 0.52188 seconds. Average reconstruction error is: 5.8687
 epoch 10/20. Took 0.4938 seconds. Average reconstruction error is: 5.7783
 epoch 11/20. Took 0.47679 seconds. Average reconstruction error is: 5.7074
 epoch 12/20. Took 0.50572 seconds. Average reconstruction error is: 5.5883
 epoch 13/20. Took 0.50397 seconds. Average reconstruction error is: 5.4986
 epoch 14/20. Took 0.49686 seconds. Average reconstruction error is: 5.426
 epoch 15/20. Took 0.56569 seconds. Average reconstruction error is: 5.37
 epoch 16/20. Took 0.49664 seconds. Average reconstruction error is: 5.2974
 epoch 17/20. Took 0.49314 seconds. Average reconstruction error is: 5.277
 epoch 18/20. Took 0.58347 seconds. Average reconstruction error is: 5.2382
 epoch 19/20. Took 0.49823 seconds. Average reconstruction error is: 5.1837
 epoch 20/20. Took 0.51236 seconds. Average reconstruction error is: 5.1117
Training NN  (784  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 1.126 seconds. 
  Full-batch training loss = 0.357111, test loss = 0.349457
  Training set accuracy = 0.903700, Test set accuracy = 0.907000
 epoch 2/100. Took 1.1496 seconds. 
  Full-batch training loss = 0.292961, test loss = 0.295654
  Training set accuracy = 0.919400, Test set accuracy = 0.918000
 epoch 3/100. Took 1.1303 seconds. 
  Full-batch training loss = 0.254984, test loss = 0.265536
  Training set accuracy = 0.928800, Test set accuracy = 0.924700
 epoch 4/100. Took 1.1298 seconds. 
  Full-batch training loss = 0.231274, test loss = 0.247880
  Training set accuracy = 0.935600, Test set accuracy = 0.929800
 epoch 5/100. Took 1.1501 seconds. 
  Full-batch training loss = 0.212025, test loss = 0.235947
  Training set accuracy = 0.940000, Test set accuracy = 0.932300
 epoch 6/100. Took 1.2169 seconds. 
  Full-batch training loss = 0.196056, test loss = 0.225737
  Training set accuracy = 0.945300, Test set accuracy = 0.936700
 epoch 7/100. Took 1.2121 seconds. 
  Full-batch training loss = 0.182899, test loss = 0.217625
  Training set accuracy = 0.949400, Test set accuracy = 0.938800
 epoch 8/100. Took 1.2408 seconds. 
  Full-batch training loss = 0.170869, test loss = 0.210068
  Training set accuracy = 0.953200, Test set accuracy = 0.940500
 epoch 9/100. Took 1.3014 seconds. 
  Full-batch training loss = 0.164311, test loss = 0.209051
  Training set accuracy = 0.955900, Test set accuracy = 0.941600
 epoch 10/100. Took 1.269 seconds. 
  Full-batch training loss = 0.152541, test loss = 0.201626
  Training set accuracy = 0.957200, Test set accuracy = 0.941800
 epoch 11/100. Took 1.1709 seconds. 
  Full-batch training loss = 0.144849, test loss = 0.197063
  Training set accuracy = 0.960500, Test set accuracy = 0.943700
 epoch 12/100. Took 1.2132 seconds. 
  Full-batch training loss = 0.140680, test loss = 0.198723
  Training set accuracy = 0.962400, Test set accuracy = 0.944700
 epoch 13/100. Took 1.1749 seconds. 
  Full-batch training loss = 0.128698, test loss = 0.189581
  Training set accuracy = 0.965800, Test set accuracy = 0.945300
 epoch 14/100. Took 1.1801 seconds. 
  Full-batch training loss = 0.123260, test loss = 0.188178
  Training set accuracy = 0.968100, Test set accuracy = 0.945700
 epoch 15/100. Took 1.1371 seconds. 
  Full-batch training loss = 0.117896, test loss = 0.186476
  Training set accuracy = 0.968400, Test set accuracy = 0.947600
 epoch 16/100. Took 1.2188 seconds. 
  Full-batch training loss = 0.111517, test loss = 0.183998
  Training set accuracy = 0.970000, Test set accuracy = 0.946700
 epoch 17/100. Took 1.1385 seconds. 
  Full-batch training loss = 0.105659, test loss = 0.181696
  Training set accuracy = 0.974100, Test set accuracy = 0.948000
 epoch 18/100. Took 1.197 seconds. 
  Full-batch training loss = 0.100263, test loss = 0.178399
  Training set accuracy = 0.975600, Test set accuracy = 0.947400
 epoch 19/100. Took 1.2106 seconds. 
  Full-batch training loss = 0.096413, test loss = 0.177379
  Training set accuracy = 0.977100, Test set accuracy = 0.948200
 epoch 20/100. Took 1.1666 seconds. 
  Full-batch training loss = 0.092004, test loss = 0.177226
  Training set accuracy = 0.978000, Test set accuracy = 0.949900
 epoch 21/100. Took 1.2205 seconds. 
  Full-batch training loss = 0.087297, test loss = 0.174761
  Training set accuracy = 0.980200, Test set accuracy = 0.949600
 epoch 22/100. Took 1.2252 seconds. 
  Full-batch training loss = 0.082923, test loss = 0.172102
  Training set accuracy = 0.981800, Test set accuracy = 0.950400
 epoch 23/100. Took 1.2022 seconds. 
  Full-batch training loss = 0.080335, test loss = 0.171897
  Training set accuracy = 0.981200, Test set accuracy = 0.950600
 epoch 24/100. Took 1.2297 seconds. 
  Full-batch training loss = 0.075527, test loss = 0.168831
  Training set accuracy = 0.983800, Test set accuracy = 0.951000
 epoch 25/100. Took 1.1929 seconds. 
  Full-batch training loss = 0.072495, test loss = 0.169913
  Training set accuracy = 0.985200, Test set accuracy = 0.950200
 epoch 26/100. Took 1.1374 seconds. 
  Full-batch training loss = 0.069175, test loss = 0.169701
  Training set accuracy = 0.985800, Test set accuracy = 0.950600
 epoch 27/100. Took 1.2316 seconds. 
  Full-batch training loss = 0.065511, test loss = 0.167360
  Training set accuracy = 0.987800, Test set accuracy = 0.951500
 epoch 28/100. Took 1.1872 seconds. 
  Full-batch training loss = 0.062610, test loss = 0.167187
  Training set accuracy = 0.988700, Test set accuracy = 0.951400
 epoch 29/100. Took 1.1471 seconds. 
  Full-batch training loss = 0.061132, test loss = 0.167551
  Training set accuracy = 0.988700, Test set accuracy = 0.951000
 epoch 30/100. Took 1.2018 seconds. 
  Full-batch training loss = 0.058704, test loss = 0.168130
  Training set accuracy = 0.989700, Test set accuracy = 0.951100
 epoch 31/100. Took 1.2152 seconds. 
  Full-batch training loss = 0.054971, test loss = 0.165352
  Training set accuracy = 0.990600, Test set accuracy = 0.951900
 epoch 32/100. Took 1.1381 seconds. 
  Full-batch training loss = 0.053598, test loss = 0.165748
  Training set accuracy = 0.991100, Test set accuracy = 0.952200
 epoch 33/100. Took 1.1242 seconds. 
  Full-batch training loss = 0.050080, test loss = 0.163820
  Training set accuracy = 0.992600, Test set accuracy = 0.952500
 epoch 34/100. Took 1.1178 seconds. 
  Full-batch training loss = 0.049442, test loss = 0.166344
  Training set accuracy = 0.991700, Test set accuracy = 0.950700
 epoch 35/100. Took 1.2451 seconds. 
  Full-batch training loss = 0.046336, test loss = 0.163786
  Training set accuracy = 0.993500, Test set accuracy = 0.951800
 epoch 36/100. Took 1.1565 seconds. 
  Full-batch training loss = 0.044074, test loss = 0.163320
  Training set accuracy = 0.994100, Test set accuracy = 0.953400
 epoch 37/100. Took 1.1212 seconds. 
  Full-batch training loss = 0.042774, test loss = 0.163506
  Training set accuracy = 0.993900, Test set accuracy = 0.953000
 epoch 38/100. Took 1.366 seconds. 
  Full-batch training loss = 0.041450, test loss = 0.164605
  Training set accuracy = 0.994700, Test set accuracy = 0.952500
 epoch 39/100. Took 1.2083 seconds. 
  Full-batch training loss = 0.039081, test loss = 0.162596
  Training set accuracy = 0.995100, Test set accuracy = 0.953000
 epoch 40/100. Took 1.1267 seconds. 
  Full-batch training loss = 0.037232, test loss = 0.162226
  Training set accuracy = 0.995500, Test set accuracy = 0.953700
 epoch 41/100. Took 1.2077 seconds. 
  Full-batch training loss = 0.037498, test loss = 0.164020
  Training set accuracy = 0.995400, Test set accuracy = 0.953100
 epoch 42/100. Took 1.2053 seconds. 
  Full-batch training loss = 0.034900, test loss = 0.163792
  Training set accuracy = 0.996500, Test set accuracy = 0.951800
 epoch 43/100. Took 1.2305 seconds. 
  Full-batch training loss = 0.033468, test loss = 0.163102
  Training set accuracy = 0.996200, Test set accuracy = 0.953700
 epoch 44/100. Took 1.2032 seconds. 
  Full-batch training loss = 0.032067, test loss = 0.161957
  Training set accuracy = 0.996400, Test set accuracy = 0.954200
 epoch 45/100. Took 1.1685 seconds. 
  Full-batch training loss = 0.030658, test loss = 0.162337
  Training set accuracy = 0.997200, Test set accuracy = 0.953900
 epoch 46/100. Took 1.2635 seconds. 
  Full-batch training loss = 0.030028, test loss = 0.161735
  Training set accuracy = 0.997600, Test set accuracy = 0.954200
 epoch 47/100. Took 1.1779 seconds. 
  Full-batch training loss = 0.028598, test loss = 0.162310
  Training set accuracy = 0.997600, Test set accuracy = 0.954100
 epoch 48/100. Took 1.1441 seconds. 
  Full-batch training loss = 0.027660, test loss = 0.162073
  Training set accuracy = 0.997800, Test set accuracy = 0.954400
 epoch 49/100. Took 1.1916 seconds. 
  Full-batch training loss = 0.026508, test loss = 0.161544
  Training set accuracy = 0.998300, Test set accuracy = 0.954100
 epoch 50/100. Took 1.2291 seconds. 
  Full-batch training loss = 0.026108, test loss = 0.161859
  Training set accuracy = 0.998200, Test set accuracy = 0.954100
 epoch 51/100. Took 1.3369 seconds. 
  Full-batch training loss = 0.024687, test loss = 0.162061
  Training set accuracy = 0.998900, Test set accuracy = 0.954100
 epoch 52/100. Took 1.2674 seconds. 
  Full-batch training loss = 0.023788, test loss = 0.162192
  Training set accuracy = 0.998800, Test set accuracy = 0.954500
 epoch 53/100. Took 1.1669 seconds. 
  Full-batch training loss = 0.023167, test loss = 0.163121
  Training set accuracy = 0.998800, Test set accuracy = 0.954600
 epoch 54/100. Took 1.2807 seconds. 
  Full-batch training loss = 0.022456, test loss = 0.163310
  Training set accuracy = 0.998900, Test set accuracy = 0.954600
 epoch 55/100. Took 1.2375 seconds. 
  Full-batch training loss = 0.021622, test loss = 0.162171
  Training set accuracy = 0.998900, Test set accuracy = 0.955200
 epoch 56/100. Took 1.168 seconds. 
  Full-batch training loss = 0.021107, test loss = 0.162765
  Training set accuracy = 0.999000, Test set accuracy = 0.954900
 epoch 57/100. Took 1.1657 seconds. 
  Full-batch training loss = 0.020204, test loss = 0.162468
  Training set accuracy = 0.999000, Test set accuracy = 0.954500
 epoch 58/100. Took 1.265 seconds. 
  Full-batch training loss = 0.019719, test loss = 0.163766
  Training set accuracy = 0.998900, Test set accuracy = 0.954800
 epoch 59/100. Took 1.2231 seconds. 
  Full-batch training loss = 0.018985, test loss = 0.162669
  Training set accuracy = 0.999000, Test set accuracy = 0.955300
 epoch 60/100. Took 1.1886 seconds. 
  Full-batch training loss = 0.018437, test loss = 0.162442
  Training set accuracy = 0.999100, Test set accuracy = 0.954400
 epoch 61/100. Took 1.1903 seconds. 
  Full-batch training loss = 0.017926, test loss = 0.162956
  Training set accuracy = 0.999000, Test set accuracy = 0.955500
 epoch 62/100. Took 1.2714 seconds. 
  Full-batch training loss = 0.017405, test loss = 0.163226
  Training set accuracy = 0.999000, Test set accuracy = 0.955300
 epoch 63/100. Took 1.2038 seconds. 
  Full-batch training loss = 0.016800, test loss = 0.162737
  Training set accuracy = 0.999200, Test set accuracy = 0.955100
 epoch 64/100. Took 1.181 seconds. 
  Full-batch training loss = 0.016312, test loss = 0.163504
  Training set accuracy = 0.999200, Test set accuracy = 0.954900
 epoch 65/100. Took 1.1668 seconds. 
  Full-batch training loss = 0.015915, test loss = 0.162950
  Training set accuracy = 0.999300, Test set accuracy = 0.955400
 epoch 66/100. Took 1.1842 seconds. 
  Full-batch training loss = 0.015411, test loss = 0.163256
  Training set accuracy = 0.999400, Test set accuracy = 0.954800
 epoch 67/100. Took 1.2676 seconds. 
  Full-batch training loss = 0.015038, test loss = 0.164157
  Training set accuracy = 0.999600, Test set accuracy = 0.955300
 epoch 68/100. Took 1.1655 seconds. 
  Full-batch training loss = 0.014560, test loss = 0.163580
  Training set accuracy = 0.999600, Test set accuracy = 0.955400
 epoch 69/100. Took 1.0977 seconds. 
  Full-batch training loss = 0.014222, test loss = 0.163734
  Training set accuracy = 0.999600, Test set accuracy = 0.955000
 epoch 70/100. Took 1.1875 seconds. 
  Full-batch training loss = 0.013851, test loss = 0.163660
  Training set accuracy = 0.999600, Test set accuracy = 0.955200
 epoch 71/100. Took 1.1669 seconds. 
  Full-batch training loss = 0.013554, test loss = 0.164880
  Training set accuracy = 0.999700, Test set accuracy = 0.955300
 epoch 72/100. Took 1.2459 seconds. 
  Full-batch training loss = 0.013161, test loss = 0.164462
  Training set accuracy = 0.999700, Test set accuracy = 0.955200
 epoch 73/100. Took 1.2637 seconds. 
  Full-batch training loss = 0.012863, test loss = 0.164462
  Training set accuracy = 0.999700, Test set accuracy = 0.955500
 epoch 74/100. Took 1.1336 seconds. 
  Full-batch training loss = 0.012524, test loss = 0.164610
  Training set accuracy = 0.999800, Test set accuracy = 0.955200
 epoch 75/100. Took 1.2385 seconds. 
  Full-batch training loss = 0.012253, test loss = 0.165161
  Training set accuracy = 0.999800, Test set accuracy = 0.956000
 epoch 76/100. Took 1.2055 seconds. 
  Full-batch training loss = 0.011917, test loss = 0.165032
  Training set accuracy = 0.999800, Test set accuracy = 0.955400
 epoch 77/100. Took 1.3526 seconds. 
  Full-batch training loss = 0.011629, test loss = 0.164669
  Training set accuracy = 0.999800, Test set accuracy = 0.955500
 epoch 78/100. Took 1.2232 seconds. 
  Full-batch training loss = 0.011373, test loss = 0.165163
  Training set accuracy = 0.999800, Test set accuracy = 0.955300
 epoch 79/100. Took 1.1527 seconds. 
  Full-batch training loss = 0.011139, test loss = 0.165280
  Training set accuracy = 0.999800, Test set accuracy = 0.956100
 epoch 80/100. Took 1.1776 seconds. 
  Full-batch training loss = 0.010893, test loss = 0.165430
  Training set accuracy = 0.999800, Test set accuracy = 0.955700
 epoch 81/100. Took 1.1081 seconds. 
  Full-batch training loss = 0.010726, test loss = 0.165382
  Training set accuracy = 0.999800, Test set accuracy = 0.955500
 epoch 82/100. Took 1.1528 seconds. 
  Full-batch training loss = 0.010460, test loss = 0.165916
  Training set accuracy = 0.999800, Test set accuracy = 0.955200
 epoch 83/100. Took 1.1914 seconds. 
  Full-batch training loss = 0.010172, test loss = 0.166022
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 84/100. Took 1.301 seconds. 
  Full-batch training loss = 0.010004, test loss = 0.166105
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 85/100. Took 1.2792 seconds. 
  Full-batch training loss = 0.009764, test loss = 0.166189
  Training set accuracy = 0.999900, Test set accuracy = 0.955800
 epoch 86/100. Took 1.1647 seconds. 
  Full-batch training loss = 0.009559, test loss = 0.166184
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 87/100. Took 1.1602 seconds. 
  Full-batch training loss = 0.009380, test loss = 0.166327
  Training set accuracy = 0.999900, Test set accuracy = 0.956400
 epoch 88/100. Took 1.2914 seconds. 
  Full-batch training loss = 0.009182, test loss = 0.166414
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 89/100. Took 1.1289 seconds. 
  Full-batch training loss = 0.008996, test loss = 0.166318
  Training set accuracy = 0.999900, Test set accuracy = 0.956400
 epoch 90/100. Took 1.1879 seconds. 
  Full-batch training loss = 0.008817, test loss = 0.166717
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 91/100. Took 1.1337 seconds. 
  Full-batch training loss = 0.008662, test loss = 0.166981
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 92/100. Took 1.2367 seconds. 
  Full-batch training loss = 0.008494, test loss = 0.167526
  Training set accuracy = 0.999900, Test set accuracy = 0.956000
 epoch 93/100. Took 1.1732 seconds. 
  Full-batch training loss = 0.008378, test loss = 0.167667
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 94/100. Took 1.1243 seconds. 
  Full-batch training loss = 0.008183, test loss = 0.167499
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 95/100. Took 1.2165 seconds. 
  Full-batch training loss = 0.008034, test loss = 0.167549
  Training set accuracy = 0.999900, Test set accuracy = 0.956800
 epoch 96/100. Took 1.2218 seconds. 
  Full-batch training loss = 0.007866, test loss = 0.167496
  Training set accuracy = 0.999900, Test set accuracy = 0.956400
 epoch 97/100. Took 1.1949 seconds. 
  Full-batch training loss = 0.007774, test loss = 0.167495
  Training set accuracy = 0.999900, Test set accuracy = 0.956700
 epoch 98/100. Took 1.1658 seconds. 
  Full-batch training loss = 0.007608, test loss = 0.168295
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 99/100. Took 1.2875 seconds. 
  Full-batch training loss = 0.007469, test loss = 0.167746
  Training set accuracy = 0.999900, Test set accuracy = 0.956700
 epoch 100/100. Took 1.2049 seconds. 
  Full-batch training loss = 0.007352, test loss = 0.168480
  Training set accuracy = 0.999900, Test set accuracy = 0.956400
Elapsed time is 306.190339 seconds.
End Training
