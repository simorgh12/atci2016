Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (200   50)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 2.0131 seconds. Average reconstruction error is: 30.4144
 epoch 2/20. Took 1.8322 seconds. Average reconstruction error is: 18.7712
 epoch 3/20. Took 1.6041 seconds. Average reconstruction error is: 16.4027
 epoch 4/20. Took 1.5914 seconds. Average reconstruction error is: 15.0071
 epoch 5/20. Took 1.5413 seconds. Average reconstruction error is: 13.9364
 epoch 6/20. Took 1.6102 seconds. Average reconstruction error is: 13.1192
 epoch 7/20. Took 1.5869 seconds. Average reconstruction error is: 12.4748
 epoch 8/20. Took 1.5572 seconds. Average reconstruction error is: 11.9792
 epoch 9/20. Took 1.6439 seconds. Average reconstruction error is: 11.5676
 epoch 10/20. Took 1.5601 seconds. Average reconstruction error is: 11.2027
 epoch 11/20. Took 1.5907 seconds. Average reconstruction error is: 10.9398
 epoch 12/20. Took 1.6314 seconds. Average reconstruction error is: 10.7321
 epoch 13/20. Took 1.5601 seconds. Average reconstruction error is: 10.5163
 epoch 14/20. Took 1.6456 seconds. Average reconstruction error is: 10.3662
 epoch 15/20. Took 1.7506 seconds. Average reconstruction error is: 10.2205
 epoch 16/20. Took 1.6077 seconds. Average reconstruction error is: 10.08
 epoch 17/20. Took 1.6005 seconds. Average reconstruction error is: 10.0175
 epoch 18/20. Took 1.6076 seconds. Average reconstruction error is: 9.841
 epoch 19/20. Took 1.5553 seconds. Average reconstruction error is: 9.7568
 epoch 20/20. Took 1.6518 seconds. Average reconstruction error is: 9.7118
Training binary-binary RBM in layer 2 (200   50) with CD1 for 20 epochs
 epoch 1/20. Took 0.21272 seconds. Average reconstruction error is: 16.0573
 epoch 2/20. Took 0.23088 seconds. Average reconstruction error is: 10.8774
 epoch 3/20. Took 0.20311 seconds. Average reconstruction error is: 10.0912
 epoch 4/20. Took 0.21303 seconds. Average reconstruction error is: 9.7299
 epoch 5/20. Took 0.20617 seconds. Average reconstruction error is: 9.4981
 epoch 6/20. Took 0.21131 seconds. Average reconstruction error is: 9.3651
 epoch 7/20. Took 0.20917 seconds. Average reconstruction error is: 9.2805
 epoch 8/20. Took 0.23508 seconds. Average reconstruction error is: 9.1889
 epoch 9/20. Took 0.21405 seconds. Average reconstruction error is: 9.105
 epoch 10/20. Took 0.21325 seconds. Average reconstruction error is: 9.0539
 epoch 11/20. Took 0.2063 seconds. Average reconstruction error is: 9.0007
 epoch 12/20. Took 0.2227 seconds. Average reconstruction error is: 8.9505
 epoch 13/20. Took 0.20717 seconds. Average reconstruction error is: 8.9076
 epoch 14/20. Took 0.21969 seconds. Average reconstruction error is: 8.8603
 epoch 15/20. Took 0.202 seconds. Average reconstruction error is: 8.8086
 epoch 16/20. Took 0.2279 seconds. Average reconstruction error is: 8.7808
 epoch 17/20. Took 0.21581 seconds. Average reconstruction error is: 8.7598
 epoch 18/20. Took 0.21362 seconds. Average reconstruction error is: 8.7275
 epoch 19/20. Took 0.21523 seconds. Average reconstruction error is: 8.6979
 epoch 20/20. Took 0.22386 seconds. Average reconstruction error is: 8.6923
Training NN  (784  200   50   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.94324 seconds. 
  Full-batch training loss = 0.384906, test loss = 0.381397
  Training set accuracy = 0.892200, Test set accuracy = 0.897400
 epoch 2/100. Took 0.96406 seconds. 
  Full-batch training loss = 0.312156, test loss = 0.322880
  Training set accuracy = 0.911500, Test set accuracy = 0.908600
 epoch 3/100. Took 1.0513 seconds. 
  Full-batch training loss = 0.268571, test loss = 0.291609
  Training set accuracy = 0.925000, Test set accuracy = 0.916500
 epoch 4/100. Took 0.99097 seconds. 
  Full-batch training loss = 0.239484, test loss = 0.272590
  Training set accuracy = 0.931500, Test set accuracy = 0.919500
 epoch 5/100. Took 0.94241 seconds. 
  Full-batch training loss = 0.218853, test loss = 0.261929
  Training set accuracy = 0.938400, Test set accuracy = 0.922000
 epoch 6/100. Took 1.0201 seconds. 
  Full-batch training loss = 0.202509, test loss = 0.257380
  Training set accuracy = 0.943900, Test set accuracy = 0.925000
 epoch 7/100. Took 1.0313 seconds. 
  Full-batch training loss = 0.180147, test loss = 0.240816
  Training set accuracy = 0.950700, Test set accuracy = 0.927600
 epoch 8/100. Took 0.98528 seconds. 
  Full-batch training loss = 0.170354, test loss = 0.239883
  Training set accuracy = 0.952600, Test set accuracy = 0.928100
 epoch 9/100. Took 0.95443 seconds. 
  Full-batch training loss = 0.153397, test loss = 0.229525
  Training set accuracy = 0.959100, Test set accuracy = 0.930600
 epoch 10/100. Took 0.95115 seconds. 
  Full-batch training loss = 0.143637, test loss = 0.227510
  Training set accuracy = 0.962100, Test set accuracy = 0.932700
 epoch 11/100. Took 1.0499 seconds. 
  Full-batch training loss = 0.134805, test loss = 0.222547
  Training set accuracy = 0.964300, Test set accuracy = 0.931800
 epoch 12/100. Took 1.0513 seconds. 
  Full-batch training loss = 0.123101, test loss = 0.217076
  Training set accuracy = 0.969000, Test set accuracy = 0.933900
 epoch 13/100. Took 1.0445 seconds. 
  Full-batch training loss = 0.115633, test loss = 0.217255
  Training set accuracy = 0.971900, Test set accuracy = 0.935100
 epoch 14/100. Took 0.9918 seconds. 
  Full-batch training loss = 0.109252, test loss = 0.213585
  Training set accuracy = 0.974400, Test set accuracy = 0.937000
 epoch 15/100. Took 1.0288 seconds. 
  Full-batch training loss = 0.102543, test loss = 0.211813
  Training set accuracy = 0.976300, Test set accuracy = 0.937800
 epoch 16/100. Took 1.1658 seconds. 
  Full-batch training loss = 0.096226, test loss = 0.210039
  Training set accuracy = 0.978500, Test set accuracy = 0.937300
 epoch 17/100. Took 0.96313 seconds. 
  Full-batch training loss = 0.091616, test loss = 0.210594
  Training set accuracy = 0.980200, Test set accuracy = 0.938500
 epoch 18/100. Took 0.99708 seconds. 
  Full-batch training loss = 0.086698, test loss = 0.208977
  Training set accuracy = 0.981600, Test set accuracy = 0.937600
 epoch 19/100. Took 1.021 seconds. 
  Full-batch training loss = 0.081095, test loss = 0.208011
  Training set accuracy = 0.983000, Test set accuracy = 0.938900
 epoch 20/100. Took 1.0297 seconds. 
  Full-batch training loss = 0.076533, test loss = 0.207496
  Training set accuracy = 0.984700, Test set accuracy = 0.939100
 epoch 21/100. Took 1.0002 seconds. 
  Full-batch training loss = 0.072950, test loss = 0.207809
  Training set accuracy = 0.985000, Test set accuracy = 0.940000
 epoch 22/100. Took 1.1019 seconds. 
  Full-batch training loss = 0.068877, test loss = 0.207834
  Training set accuracy = 0.985100, Test set accuracy = 0.939700
 epoch 23/100. Took 1.0325 seconds. 
  Full-batch training loss = 0.066775, test loss = 0.208644
  Training set accuracy = 0.985800, Test set accuracy = 0.940300
 epoch 24/100. Took 1.0101 seconds. 
  Full-batch training loss = 0.062288, test loss = 0.206277
  Training set accuracy = 0.987900, Test set accuracy = 0.940900
 epoch 25/100. Took 0.9683 seconds. 
  Full-batch training loss = 0.059077, test loss = 0.206193
  Training set accuracy = 0.988500, Test set accuracy = 0.939800
 epoch 26/100. Took 1.1457 seconds. 
  Full-batch training loss = 0.056044, test loss = 0.205977
  Training set accuracy = 0.989400, Test set accuracy = 0.940800
 epoch 27/100. Took 1.0262 seconds. 
  Full-batch training loss = 0.053459, test loss = 0.205785
  Training set accuracy = 0.990000, Test set accuracy = 0.941200
 epoch 28/100. Took 1.0748 seconds. 
  Full-batch training loss = 0.050735, test loss = 0.205856
  Training set accuracy = 0.990700, Test set accuracy = 0.941500
 epoch 29/100. Took 1.0524 seconds. 
  Full-batch training loss = 0.049018, test loss = 0.207893
  Training set accuracy = 0.991000, Test set accuracy = 0.940600
 epoch 30/100. Took 0.98162 seconds. 
  Full-batch training loss = 0.045873, test loss = 0.206040
  Training set accuracy = 0.991900, Test set accuracy = 0.941700
 epoch 31/100. Took 0.92466 seconds. 
  Full-batch training loss = 0.043661, test loss = 0.204967
  Training set accuracy = 0.992200, Test set accuracy = 0.941800
 epoch 32/100. Took 1.0289 seconds. 
  Full-batch training loss = 0.042397, test loss = 0.207097
  Training set accuracy = 0.992500, Test set accuracy = 0.941900
 epoch 33/100. Took 0.93015 seconds. 
  Full-batch training loss = 0.039861, test loss = 0.206197
  Training set accuracy = 0.992900, Test set accuracy = 0.942200
 epoch 34/100. Took 1.0757 seconds. 
  Full-batch training loss = 0.038362, test loss = 0.206429
  Training set accuracy = 0.993500, Test set accuracy = 0.942600
 epoch 35/100. Took 1.0368 seconds. 
  Full-batch training loss = 0.036957, test loss = 0.207189
  Training set accuracy = 0.993500, Test set accuracy = 0.943100
 epoch 36/100. Took 0.97933 seconds. 
  Full-batch training loss = 0.035093, test loss = 0.207954
  Training set accuracy = 0.994200, Test set accuracy = 0.942600
 epoch 37/100. Took 0.98769 seconds. 
  Full-batch training loss = 0.033451, test loss = 0.208255
  Training set accuracy = 0.994300, Test set accuracy = 0.942500
 epoch 38/100. Took 0.92789 seconds. 
  Full-batch training loss = 0.031838, test loss = 0.207157
  Training set accuracy = 0.994700, Test set accuracy = 0.942500
 epoch 39/100. Took 1.155 seconds. 
  Full-batch training loss = 0.030680, test loss = 0.208495
  Training set accuracy = 0.995000, Test set accuracy = 0.942300
 epoch 40/100. Took 0.96146 seconds. 
  Full-batch training loss = 0.029300, test loss = 0.209335
  Training set accuracy = 0.994900, Test set accuracy = 0.943100
 epoch 41/100. Took 0.93888 seconds. 
  Full-batch training loss = 0.028075, test loss = 0.209174
  Training set accuracy = 0.995700, Test set accuracy = 0.942300
 epoch 42/100. Took 1.0411 seconds. 
  Full-batch training loss = 0.026830, test loss = 0.210153
  Training set accuracy = 0.995900, Test set accuracy = 0.942600
 epoch 43/100. Took 0.93949 seconds. 
  Full-batch training loss = 0.025817, test loss = 0.210089
  Training set accuracy = 0.996100, Test set accuracy = 0.942300
 epoch 44/100. Took 0.97365 seconds. 
  Full-batch training loss = 0.024745, test loss = 0.209676
  Training set accuracy = 0.996100, Test set accuracy = 0.942700
 epoch 45/100. Took 0.94904 seconds. 
  Full-batch training loss = 0.023855, test loss = 0.210987
  Training set accuracy = 0.996500, Test set accuracy = 0.943200
 epoch 46/100. Took 1.0121 seconds. 
  Full-batch training loss = 0.023091, test loss = 0.212376
  Training set accuracy = 0.996800, Test set accuracy = 0.944000
 epoch 47/100. Took 1.0324 seconds. 
  Full-batch training loss = 0.022104, test loss = 0.211341
  Training set accuracy = 0.997300, Test set accuracy = 0.943300
 epoch 48/100. Took 1.0112 seconds. 
  Full-batch training loss = 0.021358, test loss = 0.211342
  Training set accuracy = 0.997700, Test set accuracy = 0.943900
 epoch 49/100. Took 0.99921 seconds. 
  Full-batch training loss = 0.020560, test loss = 0.212025
  Training set accuracy = 0.997800, Test set accuracy = 0.943400
 epoch 50/100. Took 0.95281 seconds. 
  Full-batch training loss = 0.019897, test loss = 0.212360
  Training set accuracy = 0.997900, Test set accuracy = 0.944000
 epoch 51/100. Took 0.95805 seconds. 
  Full-batch training loss = 0.019154, test loss = 0.212601
  Training set accuracy = 0.998000, Test set accuracy = 0.944100
 epoch 52/100. Took 0.96564 seconds. 
  Full-batch training loss = 0.018482, test loss = 0.212778
  Training set accuracy = 0.998400, Test set accuracy = 0.944500
 epoch 53/100. Took 1.0025 seconds. 
  Full-batch training loss = 0.018043, test loss = 0.212859
  Training set accuracy = 0.998300, Test set accuracy = 0.944400
 epoch 54/100. Took 1.0196 seconds. 
  Full-batch training loss = 0.017333, test loss = 0.212834
  Training set accuracy = 0.998400, Test set accuracy = 0.943800
 epoch 55/100. Took 1.105 seconds. 
  Full-batch training loss = 0.016798, test loss = 0.214148
  Training set accuracy = 0.998500, Test set accuracy = 0.943700
 epoch 56/100. Took 1.0459 seconds. 
  Full-batch training loss = 0.016156, test loss = 0.213563
  Training set accuracy = 0.998400, Test set accuracy = 0.944600
 epoch 57/100. Took 0.96663 seconds. 
  Full-batch training loss = 0.015680, test loss = 0.213943
  Training set accuracy = 0.998600, Test set accuracy = 0.944200
 epoch 58/100. Took 1.0726 seconds. 
  Full-batch training loss = 0.015173, test loss = 0.213861
  Training set accuracy = 0.998700, Test set accuracy = 0.944500
 epoch 59/100. Took 1.0173 seconds. 
  Full-batch training loss = 0.014790, test loss = 0.215330
  Training set accuracy = 0.998800, Test set accuracy = 0.945100
 epoch 60/100. Took 0.97033 seconds. 
  Full-batch training loss = 0.014360, test loss = 0.215741
  Training set accuracy = 0.998700, Test set accuracy = 0.944500
 epoch 61/100. Took 0.9581 seconds. 
  Full-batch training loss = 0.013948, test loss = 0.215911
  Training set accuracy = 0.998900, Test set accuracy = 0.944900
 epoch 62/100. Took 1.0031 seconds. 
  Full-batch training loss = 0.013583, test loss = 0.216068
  Training set accuracy = 0.998900, Test set accuracy = 0.944600
 epoch 63/100. Took 0.95341 seconds. 
  Full-batch training loss = 0.013145, test loss = 0.216368
  Training set accuracy = 0.998900, Test set accuracy = 0.944800
 epoch 64/100. Took 0.95615 seconds. 
  Full-batch training loss = 0.012907, test loss = 0.216358
  Training set accuracy = 0.999000, Test set accuracy = 0.945100
 epoch 65/100. Took 1.0769 seconds. 
  Full-batch training loss = 0.012445, test loss = 0.217143
  Training set accuracy = 0.998900, Test set accuracy = 0.944900
 epoch 66/100. Took 1.0965 seconds. 
  Full-batch training loss = 0.012192, test loss = 0.217510
  Training set accuracy = 0.998900, Test set accuracy = 0.945400
 epoch 67/100. Took 0.98624 seconds. 
  Full-batch training loss = 0.011780, test loss = 0.216964
  Training set accuracy = 0.998900, Test set accuracy = 0.945000
 epoch 68/100. Took 0.97417 seconds. 
  Full-batch training loss = 0.011543, test loss = 0.217583
  Training set accuracy = 0.999100, Test set accuracy = 0.944800
 epoch 69/100. Took 1.0939 seconds. 
  Full-batch training loss = 0.011177, test loss = 0.217842
  Training set accuracy = 0.999000, Test set accuracy = 0.945100
 epoch 70/100. Took 0.9393 seconds. 
  Full-batch training loss = 0.010888, test loss = 0.218385
  Training set accuracy = 0.998900, Test set accuracy = 0.944900
 epoch 71/100. Took 0.93308 seconds. 
  Full-batch training loss = 0.010620, test loss = 0.218594
  Training set accuracy = 0.999000, Test set accuracy = 0.945400
 epoch 72/100. Took 1.0263 seconds. 
  Full-batch training loss = 0.010403, test loss = 0.218712
  Training set accuracy = 0.999200, Test set accuracy = 0.944800
 epoch 73/100. Took 1.0146 seconds. 
  Full-batch training loss = 0.010137, test loss = 0.219618
  Training set accuracy = 0.999300, Test set accuracy = 0.945000
 epoch 74/100. Took 0.95014 seconds. 
  Full-batch training loss = 0.009978, test loss = 0.220108
  Training set accuracy = 0.999200, Test set accuracy = 0.945100
 epoch 75/100. Took 1.0142 seconds. 
  Full-batch training loss = 0.009654, test loss = 0.219358
  Training set accuracy = 0.999400, Test set accuracy = 0.945200
 epoch 76/100. Took 0.96565 seconds. 
  Full-batch training loss = 0.009425, test loss = 0.219749
  Training set accuracy = 0.999400, Test set accuracy = 0.944900
 epoch 77/100. Took 1.2185 seconds. 
  Full-batch training loss = 0.009226, test loss = 0.219713
  Training set accuracy = 0.999500, Test set accuracy = 0.944900
 epoch 78/100. Took 0.9465 seconds. 
  Full-batch training loss = 0.009036, test loss = 0.220068
  Training set accuracy = 0.999500, Test set accuracy = 0.945000
 epoch 79/100. Took 0.97342 seconds. 
  Full-batch training loss = 0.008811, test loss = 0.220713
  Training set accuracy = 0.999500, Test set accuracy = 0.945300
 epoch 80/100. Took 0.97778 seconds. 
  Full-batch training loss = 0.008656, test loss = 0.220723
  Training set accuracy = 0.999500, Test set accuracy = 0.945300
 epoch 81/100. Took 1.0137 seconds. 
  Full-batch training loss = 0.008424, test loss = 0.221723
  Training set accuracy = 0.999500, Test set accuracy = 0.945100
 epoch 82/100. Took 0.96236 seconds. 
  Full-batch training loss = 0.008280, test loss = 0.221291
  Training set accuracy = 0.999500, Test set accuracy = 0.945200
 epoch 83/100. Took 1.0121 seconds. 
  Full-batch training loss = 0.008079, test loss = 0.221634
  Training set accuracy = 0.999500, Test set accuracy = 0.945400
 epoch 84/100. Took 0.97699 seconds. 
  Full-batch training loss = 0.007908, test loss = 0.222102
  Training set accuracy = 0.999500, Test set accuracy = 0.945400
 epoch 85/100. Took 0.97256 seconds. 
  Full-batch training loss = 0.007758, test loss = 0.222401
  Training set accuracy = 0.999500, Test set accuracy = 0.945500
 epoch 86/100. Took 0.96929 seconds. 
  Full-batch training loss = 0.007589, test loss = 0.222207
  Training set accuracy = 0.999500, Test set accuracy = 0.946100
 epoch 87/100. Took 0.98648 seconds. 
  Full-batch training loss = 0.007441, test loss = 0.222627
  Training set accuracy = 0.999500, Test set accuracy = 0.945100
 epoch 88/100. Took 0.97915 seconds. 
  Full-batch training loss = 0.007324, test loss = 0.222477
  Training set accuracy = 0.999500, Test set accuracy = 0.945000
 epoch 89/100. Took 0.97101 seconds. 
  Full-batch training loss = 0.007130, test loss = 0.223261
  Training set accuracy = 0.999500, Test set accuracy = 0.945300
 epoch 90/100. Took 0.96034 seconds. 
  Full-batch training loss = 0.007001, test loss = 0.222779
  Training set accuracy = 0.999500, Test set accuracy = 0.945600
 epoch 91/100. Took 0.99988 seconds. 
  Full-batch training loss = 0.006886, test loss = 0.223183
  Training set accuracy = 0.999500, Test set accuracy = 0.945700
 epoch 92/100. Took 1.0298 seconds. 
  Full-batch training loss = 0.006758, test loss = 0.223208
  Training set accuracy = 0.999500, Test set accuracy = 0.945600
 epoch 93/100. Took 0.95639 seconds. 
  Full-batch training loss = 0.006632, test loss = 0.224118
  Training set accuracy = 0.999500, Test set accuracy = 0.945400
 epoch 94/100. Took 1.0501 seconds. 
  Full-batch training loss = 0.006488, test loss = 0.224583
  Training set accuracy = 0.999500, Test set accuracy = 0.945400
 epoch 95/100. Took 0.98675 seconds. 
  Full-batch training loss = 0.006365, test loss = 0.224690
  Training set accuracy = 0.999500, Test set accuracy = 0.945500
 epoch 96/100. Took 0.95873 seconds. 
  Full-batch training loss = 0.006252, test loss = 0.224965
  Training set accuracy = 0.999500, Test set accuracy = 0.945700
 epoch 97/100. Took 1.0476 seconds. 
  Full-batch training loss = 0.006130, test loss = 0.224937
  Training set accuracy = 0.999600, Test set accuracy = 0.946000
 epoch 98/100. Took 0.98908 seconds. 
  Full-batch training loss = 0.006026, test loss = 0.224753
  Training set accuracy = 0.999600, Test set accuracy = 0.945600
 epoch 99/100. Took 0.981 seconds. 
  Full-batch training loss = 0.005910, test loss = 0.225500
  Training set accuracy = 0.999700, Test set accuracy = 0.945900
 epoch 100/100. Took 1.0147 seconds. 
  Full-batch training loss = 0.005803, test loss = 0.225729
  Training set accuracy = 0.999700, Test set accuracy = 0.945800
Elapsed time is 249.077407 seconds.
End Training
