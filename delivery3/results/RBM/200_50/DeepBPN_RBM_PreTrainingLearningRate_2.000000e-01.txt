Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (200   50)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.5701 seconds. Average reconstruction error is: 29.7659
 epoch 2/20. Took 1.5708 seconds. Average reconstruction error is: 18.2576
 epoch 3/20. Took 1.7152 seconds. Average reconstruction error is: 15.9189
 epoch 4/20. Took 1.7022 seconds. Average reconstruction error is: 14.5258
 epoch 5/20. Took 1.6346 seconds. Average reconstruction error is: 13.4944
 epoch 6/20. Took 1.7136 seconds. Average reconstruction error is: 12.7277
 epoch 7/20. Took 1.661 seconds. Average reconstruction error is: 12.0808
 epoch 8/20. Took 1.5344 seconds. Average reconstruction error is: 11.6405
 epoch 9/20. Took 1.5891 seconds. Average reconstruction error is: 11.2761
 epoch 10/20. Took 1.5494 seconds. Average reconstruction error is: 10.9772
 epoch 11/20. Took 1.6334 seconds. Average reconstruction error is: 10.7454
 epoch 12/20. Took 1.6038 seconds. Average reconstruction error is: 10.5741
 epoch 13/20. Took 1.6238 seconds. Average reconstruction error is: 10.4054
 epoch 14/20. Took 1.5716 seconds. Average reconstruction error is: 10.2511
 epoch 15/20. Took 1.6263 seconds. Average reconstruction error is: 10.1591
 epoch 16/20. Took 1.697 seconds. Average reconstruction error is: 10.0343
 epoch 17/20. Took 1.6508 seconds. Average reconstruction error is: 9.9105
 epoch 18/20. Took 1.688 seconds. Average reconstruction error is: 9.8154
 epoch 19/20. Took 1.6559 seconds. Average reconstruction error is: 9.741
 epoch 20/20. Took 1.618 seconds. Average reconstruction error is: 9.6663
Training binary-binary RBM in layer 2 (200   50) with CD1 for 20 epochs
 epoch 1/20. Took 0.21406 seconds. Average reconstruction error is: 16.0791
 epoch 2/20. Took 0.23336 seconds. Average reconstruction error is: 11.0047
 epoch 3/20. Took 0.20707 seconds. Average reconstruction error is: 10.1999
 epoch 4/20. Took 0.21506 seconds. Average reconstruction error is: 9.8272
 epoch 5/20. Took 0.21592 seconds. Average reconstruction error is: 9.6091
 epoch 6/20. Took 0.21514 seconds. Average reconstruction error is: 9.4799
 epoch 7/20. Took 0.22306 seconds. Average reconstruction error is: 9.359
 epoch 8/20. Took 0.20808 seconds. Average reconstruction error is: 9.2956
 epoch 9/20. Took 0.2122 seconds. Average reconstruction error is: 9.2391
 epoch 10/20. Took 0.22069 seconds. Average reconstruction error is: 9.1717
 epoch 11/20. Took 0.2209 seconds. Average reconstruction error is: 9.1365
 epoch 12/20. Took 0.21265 seconds. Average reconstruction error is: 9.082
 epoch 13/20. Took 0.21263 seconds. Average reconstruction error is: 9.0613
 epoch 14/20. Took 0.20754 seconds. Average reconstruction error is: 9.0033
 epoch 15/20. Took 0.23618 seconds. Average reconstruction error is: 8.988
 epoch 16/20. Took 0.20713 seconds. Average reconstruction error is: 8.9542
 epoch 17/20. Took 0.20846 seconds. Average reconstruction error is: 8.9167
 epoch 18/20. Took 0.23818 seconds. Average reconstruction error is: 8.8743
 epoch 19/20. Took 0.20704 seconds. Average reconstruction error is: 8.8409
 epoch 20/20. Took 0.20777 seconds. Average reconstruction error is: 8.8137
Training NN  (784  200   50   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 1.0423 seconds. 
  Full-batch training loss = 0.397259, test loss = 0.397758
  Training set accuracy = 0.891400, Test set accuracy = 0.896500
 epoch 2/100. Took 0.95764 seconds. 
  Full-batch training loss = 0.320243, test loss = 0.330167
  Training set accuracy = 0.910200, Test set accuracy = 0.909100
 epoch 3/100. Took 1.0073 seconds. 
  Full-batch training loss = 0.280025, test loss = 0.300791
  Training set accuracy = 0.920800, Test set accuracy = 0.918600
 epoch 4/100. Took 0.94646 seconds. 
  Full-batch training loss = 0.253128, test loss = 0.282389
  Training set accuracy = 0.929900, Test set accuracy = 0.920600
 epoch 5/100. Took 0.94287 seconds. 
  Full-batch training loss = 0.228148, test loss = 0.267437
  Training set accuracy = 0.936200, Test set accuracy = 0.924000
 epoch 6/100. Took 0.96351 seconds. 
  Full-batch training loss = 0.209373, test loss = 0.261002
  Training set accuracy = 0.941800, Test set accuracy = 0.926400
 epoch 7/100. Took 1.0558 seconds. 
  Full-batch training loss = 0.193467, test loss = 0.253706
  Training set accuracy = 0.948400, Test set accuracy = 0.927800
 epoch 8/100. Took 0.99251 seconds. 
  Full-batch training loss = 0.180320, test loss = 0.249734
  Training set accuracy = 0.952100, Test set accuracy = 0.927800
 epoch 9/100. Took 0.96385 seconds. 
  Full-batch training loss = 0.165345, test loss = 0.238786
  Training set accuracy = 0.956600, Test set accuracy = 0.931800
 epoch 10/100. Took 1.0499 seconds. 
  Full-batch training loss = 0.156805, test loss = 0.236623
  Training set accuracy = 0.958300, Test set accuracy = 0.932200
 epoch 11/100. Took 0.94802 seconds. 
  Full-batch training loss = 0.146697, test loss = 0.236964
  Training set accuracy = 0.962400, Test set accuracy = 0.933000
 epoch 12/100. Took 1.101 seconds. 
  Full-batch training loss = 0.134322, test loss = 0.230133
  Training set accuracy = 0.965800, Test set accuracy = 0.934600
 epoch 13/100. Took 1.0317 seconds. 
  Full-batch training loss = 0.126487, test loss = 0.231459
  Training set accuracy = 0.968500, Test set accuracy = 0.935100
 epoch 14/100. Took 0.95402 seconds. 
  Full-batch training loss = 0.118305, test loss = 0.222498
  Training set accuracy = 0.971500, Test set accuracy = 0.936500
 epoch 15/100. Took 0.99538 seconds. 
  Full-batch training loss = 0.112001, test loss = 0.224270
  Training set accuracy = 0.970700, Test set accuracy = 0.936400
 epoch 16/100. Took 0.95043 seconds. 
  Full-batch training loss = 0.104944, test loss = 0.222776
  Training set accuracy = 0.973100, Test set accuracy = 0.937100
 epoch 17/100. Took 1.0217 seconds. 
  Full-batch training loss = 0.098291, test loss = 0.222584
  Training set accuracy = 0.976200, Test set accuracy = 0.938400
 epoch 18/100. Took 1.0648 seconds. 
  Full-batch training loss = 0.093625, test loss = 0.220814
  Training set accuracy = 0.977100, Test set accuracy = 0.939200
 epoch 19/100. Took 0.96118 seconds. 
  Full-batch training loss = 0.087080, test loss = 0.219035
  Training set accuracy = 0.980000, Test set accuracy = 0.939200
 epoch 20/100. Took 1.0169 seconds. 
  Full-batch training loss = 0.082574, test loss = 0.217812
  Training set accuracy = 0.980300, Test set accuracy = 0.939200
 epoch 21/100. Took 0.94514 seconds. 
  Full-batch training loss = 0.077798, test loss = 0.216202
  Training set accuracy = 0.982200, Test set accuracy = 0.939700
 epoch 22/100. Took 0.93052 seconds. 
  Full-batch training loss = 0.073509, test loss = 0.217374
  Training set accuracy = 0.983200, Test set accuracy = 0.939800
 epoch 23/100. Took 0.98983 seconds. 
  Full-batch training loss = 0.069456, test loss = 0.214214
  Training set accuracy = 0.985000, Test set accuracy = 0.941600
 epoch 24/100. Took 1.0157 seconds. 
  Full-batch training loss = 0.066092, test loss = 0.215354
  Training set accuracy = 0.985200, Test set accuracy = 0.940900
 epoch 25/100. Took 0.93627 seconds. 
  Full-batch training loss = 0.061851, test loss = 0.215077
  Training set accuracy = 0.986700, Test set accuracy = 0.941500
 epoch 26/100. Took 1.0181 seconds. 
  Full-batch training loss = 0.060130, test loss = 0.218910
  Training set accuracy = 0.987600, Test set accuracy = 0.941300
 epoch 27/100. Took 0.94212 seconds. 
  Full-batch training loss = 0.055909, test loss = 0.215050
  Training set accuracy = 0.988000, Test set accuracy = 0.941300
 epoch 28/100. Took 1.0331 seconds. 
  Full-batch training loss = 0.054076, test loss = 0.219794
  Training set accuracy = 0.989400, Test set accuracy = 0.941900
 epoch 29/100. Took 0.93008 seconds. 
  Full-batch training loss = 0.050554, test loss = 0.215241
  Training set accuracy = 0.989900, Test set accuracy = 0.942400
 epoch 30/100. Took 0.95463 seconds. 
  Full-batch training loss = 0.047884, test loss = 0.216045
  Training set accuracy = 0.990700, Test set accuracy = 0.942700
 epoch 31/100. Took 0.96956 seconds. 
  Full-batch training loss = 0.046140, test loss = 0.216949
  Training set accuracy = 0.991300, Test set accuracy = 0.943400
 epoch 32/100. Took 0.98801 seconds. 
  Full-batch training loss = 0.043937, test loss = 0.215057
  Training set accuracy = 0.991400, Test set accuracy = 0.943100
 epoch 33/100. Took 0.973 seconds. 
  Full-batch training loss = 0.042346, test loss = 0.220550
  Training set accuracy = 0.991700, Test set accuracy = 0.943000
 epoch 34/100. Took 0.95154 seconds. 
  Full-batch training loss = 0.039523, test loss = 0.217115
  Training set accuracy = 0.993200, Test set accuracy = 0.943600
 epoch 35/100. Took 0.98299 seconds. 
  Full-batch training loss = 0.037971, test loss = 0.215922
  Training set accuracy = 0.993200, Test set accuracy = 0.944200
 epoch 36/100. Took 1.0361 seconds. 
  Full-batch training loss = 0.036492, test loss = 0.216450
  Training set accuracy = 0.993700, Test set accuracy = 0.943400
 epoch 37/100. Took 0.97428 seconds. 
  Full-batch training loss = 0.034611, test loss = 0.217247
  Training set accuracy = 0.994300, Test set accuracy = 0.943400
 epoch 38/100. Took 0.95575 seconds. 
  Full-batch training loss = 0.033239, test loss = 0.217742
  Training set accuracy = 0.994900, Test set accuracy = 0.944000
 epoch 39/100. Took 0.92951 seconds. 
  Full-batch training loss = 0.032351, test loss = 0.220398
  Training set accuracy = 0.995200, Test set accuracy = 0.943800
 epoch 40/100. Took 0.98091 seconds. 
  Full-batch training loss = 0.030920, test loss = 0.220366
  Training set accuracy = 0.995600, Test set accuracy = 0.943200
 epoch 41/100. Took 1.0948 seconds. 
  Full-batch training loss = 0.029459, test loss = 0.219026
  Training set accuracy = 0.995700, Test set accuracy = 0.944000
 epoch 42/100. Took 0.94676 seconds. 
  Full-batch training loss = 0.028111, test loss = 0.219873
  Training set accuracy = 0.996100, Test set accuracy = 0.944500
 epoch 43/100. Took 0.96448 seconds. 
  Full-batch training loss = 0.027060, test loss = 0.219324
  Training set accuracy = 0.996400, Test set accuracy = 0.944600
 epoch 44/100. Took 1.0506 seconds. 
  Full-batch training loss = 0.026285, test loss = 0.222552
  Training set accuracy = 0.996200, Test set accuracy = 0.944600
 epoch 45/100. Took 0.9967 seconds. 
  Full-batch training loss = 0.025094, test loss = 0.221112
  Training set accuracy = 0.996700, Test set accuracy = 0.943800
 epoch 46/100. Took 1.03 seconds. 
  Full-batch training loss = 0.024223, test loss = 0.221551
  Training set accuracy = 0.996900, Test set accuracy = 0.944200
 epoch 47/100. Took 0.95654 seconds. 
  Full-batch training loss = 0.023328, test loss = 0.221139
  Training set accuracy = 0.997000, Test set accuracy = 0.945700
 epoch 48/100. Took 1.016 seconds. 
  Full-batch training loss = 0.022318, test loss = 0.221826
  Training set accuracy = 0.997200, Test set accuracy = 0.944900
 epoch 49/100. Took 0.9793 seconds. 
  Full-batch training loss = 0.021655, test loss = 0.223364
  Training set accuracy = 0.997600, Test set accuracy = 0.944400
 epoch 50/100. Took 1.0576 seconds. 
  Full-batch training loss = 0.020895, test loss = 0.223419
  Training set accuracy = 0.997600, Test set accuracy = 0.943900
 epoch 51/100. Took 0.95217 seconds. 
  Full-batch training loss = 0.020046, test loss = 0.224202
  Training set accuracy = 0.998000, Test set accuracy = 0.944700
 epoch 52/100. Took 0.97784 seconds. 
  Full-batch training loss = 0.019320, test loss = 0.224129
  Training set accuracy = 0.998100, Test set accuracy = 0.943900
 epoch 53/100. Took 0.98383 seconds. 
  Full-batch training loss = 0.018809, test loss = 0.224590
  Training set accuracy = 0.998200, Test set accuracy = 0.944700
 epoch 54/100. Took 0.94498 seconds. 
  Full-batch training loss = 0.017933, test loss = 0.224136
  Training set accuracy = 0.998200, Test set accuracy = 0.944800
 epoch 55/100. Took 0.96158 seconds. 
  Full-batch training loss = 0.017387, test loss = 0.225228
  Training set accuracy = 0.998200, Test set accuracy = 0.944800
 epoch 56/100. Took 0.97882 seconds. 
  Full-batch training loss = 0.016819, test loss = 0.226085
  Training set accuracy = 0.998600, Test set accuracy = 0.944800
 epoch 57/100. Took 0.95738 seconds. 
  Full-batch training loss = 0.016380, test loss = 0.225412
  Training set accuracy = 0.998300, Test set accuracy = 0.945000
 epoch 58/100. Took 1.0095 seconds. 
  Full-batch training loss = 0.015737, test loss = 0.225156
  Training set accuracy = 0.998600, Test set accuracy = 0.945400
 epoch 59/100. Took 1.0201 seconds. 
  Full-batch training loss = 0.015367, test loss = 0.227023
  Training set accuracy = 0.998800, Test set accuracy = 0.944700
 epoch 60/100. Took 1.0633 seconds. 
  Full-batch training loss = 0.014799, test loss = 0.226852
  Training set accuracy = 0.998800, Test set accuracy = 0.945900
 epoch 61/100. Took 0.92512 seconds. 
  Full-batch training loss = 0.014371, test loss = 0.227594
  Training set accuracy = 0.998900, Test set accuracy = 0.945500
 epoch 62/100. Took 1.0669 seconds. 
  Full-batch training loss = 0.014057, test loss = 0.229543
  Training set accuracy = 0.999100, Test set accuracy = 0.945600
 epoch 63/100. Took 0.95752 seconds. 
  Full-batch training loss = 0.013583, test loss = 0.227799
  Training set accuracy = 0.999200, Test set accuracy = 0.944900
 epoch 64/100. Took 0.94142 seconds. 
  Full-batch training loss = 0.013220, test loss = 0.228318
  Training set accuracy = 0.998900, Test set accuracy = 0.945400
 epoch 65/100. Took 0.99086 seconds. 
  Full-batch training loss = 0.012847, test loss = 0.230536
  Training set accuracy = 0.999200, Test set accuracy = 0.945700
 epoch 66/100. Took 0.93748 seconds. 
  Full-batch training loss = 0.012468, test loss = 0.230766
  Training set accuracy = 0.999300, Test set accuracy = 0.945200
 epoch 67/100. Took 1.0502 seconds. 
  Full-batch training loss = 0.012067, test loss = 0.229688
  Training set accuracy = 0.999300, Test set accuracy = 0.945800
 epoch 68/100. Took 0.9562 seconds. 
  Full-batch training loss = 0.011820, test loss = 0.231451
  Training set accuracy = 0.999300, Test set accuracy = 0.945300
 epoch 69/100. Took 0.95424 seconds. 
  Full-batch training loss = 0.011490, test loss = 0.230772
  Training set accuracy = 0.999400, Test set accuracy = 0.945500
 epoch 70/100. Took 0.96737 seconds. 
  Full-batch training loss = 0.011176, test loss = 0.231233
  Training set accuracy = 0.999500, Test set accuracy = 0.946100
 epoch 71/100. Took 0.94167 seconds. 
  Full-batch training loss = 0.010916, test loss = 0.232387
  Training set accuracy = 0.999400, Test set accuracy = 0.944900
 epoch 72/100. Took 1.0211 seconds. 
  Full-batch training loss = 0.010601, test loss = 0.231505
  Training set accuracy = 0.999500, Test set accuracy = 0.945600
 epoch 73/100. Took 0.96112 seconds. 
  Full-batch training loss = 0.010351, test loss = 0.232210
  Training set accuracy = 0.999300, Test set accuracy = 0.946100
 epoch 74/100. Took 1.0403 seconds. 
  Full-batch training loss = 0.010130, test loss = 0.233738
  Training set accuracy = 0.999500, Test set accuracy = 0.945500
 epoch 75/100. Took 0.97137 seconds. 
  Full-batch training loss = 0.009843, test loss = 0.232873
  Training set accuracy = 0.999500, Test set accuracy = 0.946000
 epoch 76/100. Took 0.99629 seconds. 
  Full-batch training loss = 0.009634, test loss = 0.233906
  Training set accuracy = 0.999500, Test set accuracy = 0.946100
 epoch 77/100. Took 0.96806 seconds. 
  Full-batch training loss = 0.009403, test loss = 0.234502
  Training set accuracy = 0.999600, Test set accuracy = 0.945900
 epoch 78/100. Took 0.98666 seconds. 
  Full-batch training loss = 0.009199, test loss = 0.234481
  Training set accuracy = 0.999600, Test set accuracy = 0.946200
 epoch 79/100. Took 1.129 seconds. 
  Full-batch training loss = 0.009005, test loss = 0.235179
  Training set accuracy = 0.999600, Test set accuracy = 0.945900
 epoch 80/100. Took 0.94651 seconds. 
  Full-batch training loss = 0.008769, test loss = 0.235207
  Training set accuracy = 0.999600, Test set accuracy = 0.946200
 epoch 81/100. Took 0.97907 seconds. 
  Full-batch training loss = 0.008594, test loss = 0.235930
  Training set accuracy = 0.999600, Test set accuracy = 0.946100
 epoch 82/100. Took 0.96139 seconds. 
  Full-batch training loss = 0.008401, test loss = 0.235873
  Training set accuracy = 0.999600, Test set accuracy = 0.946200
 epoch 83/100. Took 0.93151 seconds. 
  Full-batch training loss = 0.008218, test loss = 0.236497
  Training set accuracy = 0.999600, Test set accuracy = 0.946100
 epoch 84/100. Took 0.9666 seconds. 
  Full-batch training loss = 0.008041, test loss = 0.236807
  Training set accuracy = 0.999600, Test set accuracy = 0.946600
 epoch 85/100. Took 0.95061 seconds. 
  Full-batch training loss = 0.007893, test loss = 0.236317
  Training set accuracy = 0.999600, Test set accuracy = 0.946000
 epoch 86/100. Took 0.93631 seconds. 
  Full-batch training loss = 0.007728, test loss = 0.237611
  Training set accuracy = 0.999600, Test set accuracy = 0.946000
 epoch 87/100. Took 1.0362 seconds. 
  Full-batch training loss = 0.007578, test loss = 0.237261
  Training set accuracy = 0.999600, Test set accuracy = 0.946200
 epoch 88/100. Took 0.95872 seconds. 
  Full-batch training loss = 0.007434, test loss = 0.238164
  Training set accuracy = 0.999600, Test set accuracy = 0.946300
 epoch 89/100. Took 0.99588 seconds. 
  Full-batch training loss = 0.007284, test loss = 0.238557
  Training set accuracy = 0.999600, Test set accuracy = 0.946200
 epoch 90/100. Took 0.95186 seconds. 
  Full-batch training loss = 0.007180, test loss = 0.239132
  Training set accuracy = 0.999600, Test set accuracy = 0.945800
 epoch 91/100. Took 0.99137 seconds. 
  Full-batch training loss = 0.007004, test loss = 0.239097
  Training set accuracy = 0.999700, Test set accuracy = 0.946500
 epoch 92/100. Took 0.95274 seconds. 
  Full-batch training loss = 0.006874, test loss = 0.239382
  Training set accuracy = 0.999700, Test set accuracy = 0.945900
 epoch 93/100. Took 0.99689 seconds. 
  Full-batch training loss = 0.006747, test loss = 0.239560
  Training set accuracy = 0.999800, Test set accuracy = 0.946300
 epoch 94/100. Took 1.0264 seconds. 
  Full-batch training loss = 0.006633, test loss = 0.240135
  Training set accuracy = 0.999800, Test set accuracy = 0.946300
 epoch 95/100. Took 0.95714 seconds. 
  Full-batch training loss = 0.006497, test loss = 0.240543
  Training set accuracy = 0.999900, Test set accuracy = 0.945800
 epoch 96/100. Took 0.93181 seconds. 
  Full-batch training loss = 0.006386, test loss = 0.240884
  Training set accuracy = 0.999900, Test set accuracy = 0.946000
 epoch 97/100. Took 0.98685 seconds. 
  Full-batch training loss = 0.006293, test loss = 0.241156
  Training set accuracy = 0.999900, Test set accuracy = 0.945900
 epoch 98/100. Took 1.0496 seconds. 
  Full-batch training loss = 0.006161, test loss = 0.241516
  Training set accuracy = 0.999900, Test set accuracy = 0.946100
 epoch 99/100. Took 0.98843 seconds. 
  Full-batch training loss = 0.006071, test loss = 0.241758
  Training set accuracy = 0.999900, Test set accuracy = 0.946000
 epoch 100/100. Took 0.94645 seconds. 
  Full-batch training loss = 0.005958, test loss = 0.241920
  Training set accuracy = 0.999900, Test set accuracy = 0.945800
Elapsed time is 247.025650 seconds.
End Training
