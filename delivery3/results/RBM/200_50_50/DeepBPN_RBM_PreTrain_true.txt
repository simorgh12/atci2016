Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.60416 seconds. Average reconstruction error is: 29.4201
 epoch 2/20. Took 0.59698 seconds. Average reconstruction error is: 18.049
 epoch 3/20. Took 0.59947 seconds. Average reconstruction error is: 15.7333
 epoch 4/20. Took 0.56927 seconds. Average reconstruction error is: 14.3126
 epoch 5/20. Took 0.57732 seconds. Average reconstruction error is: 13.2724
 epoch 6/20. Took 0.60492 seconds. Average reconstruction error is: 12.4901
 epoch 7/20. Took 0.63934 seconds. Average reconstruction error is: 11.8234
 epoch 8/20. Took 0.59641 seconds. Average reconstruction error is: 11.3858
 epoch 9/20. Took 0.59612 seconds. Average reconstruction error is: 10.9993
 epoch 10/20. Took 0.58679 seconds. Average reconstruction error is: 10.6891
 epoch 11/20. Took 0.57954 seconds. Average reconstruction error is: 10.4629
 epoch 12/20. Took 0.56896 seconds. Average reconstruction error is: 10.2119
 epoch 13/20. Took 0.57285 seconds. Average reconstruction error is: 10.0671
 epoch 14/20. Took 0.60112 seconds. Average reconstruction error is: 9.9146
 epoch 15/20. Took 0.64516 seconds. Average reconstruction error is: 9.7978
 epoch 16/20. Took 0.65648 seconds. Average reconstruction error is: 9.6807
 epoch 17/20. Took 0.66534 seconds. Average reconstruction error is: 9.5794
 epoch 18/20. Took 0.71951 seconds. Average reconstruction error is: 9.5301
 epoch 19/20. Took 0.72854 seconds. Average reconstruction error is: 9.3729
 epoch 20/20. Took 0.71968 seconds. Average reconstruction error is: 9.2974
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.26151 seconds. Average reconstruction error is: 13.9046
 epoch 2/20. Took 0.2404 seconds. Average reconstruction error is: 8.3555
 epoch 3/20. Took 0.21737 seconds. Average reconstruction error is: 7.3904
 epoch 4/20. Took 0.21361 seconds. Average reconstruction error is: 6.8341
 epoch 5/20. Took 0.21093 seconds. Average reconstruction error is: 6.5483
 epoch 6/20. Took 0.20239 seconds. Average reconstruction error is: 6.3245
 epoch 7/20. Took 0.20399 seconds. Average reconstruction error is: 6.1785
 epoch 8/20. Took 0.20716 seconds. Average reconstruction error is: 6.0094
 epoch 9/20. Took 0.2078 seconds. Average reconstruction error is: 5.8853
 epoch 10/20. Took 0.21241 seconds. Average reconstruction error is: 5.8183
 epoch 11/20. Took 0.20496 seconds. Average reconstruction error is: 5.7095
 epoch 12/20. Took 0.19636 seconds. Average reconstruction error is: 5.6278
 epoch 13/20. Took 0.20811 seconds. Average reconstruction error is: 5.5949
 epoch 14/20. Took 0.20875 seconds. Average reconstruction error is: 5.5121
 epoch 15/20. Took 0.2003 seconds. Average reconstruction error is: 5.4362
 epoch 16/20. Took 0.2184 seconds. Average reconstruction error is: 5.3613
 epoch 17/20. Took 0.23408 seconds. Average reconstruction error is: 5.3414
 epoch 18/20. Took 0.21091 seconds. Average reconstruction error is: 5.277
 epoch 19/20. Took 0.22611 seconds. Average reconstruction error is: 5.2191
 epoch 20/20. Took 0.21068 seconds. Average reconstruction error is: 5.2001
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.22582 seconds. Average reconstruction error is: 12.2758
 epoch 2/20. Took 0.22111 seconds. Average reconstruction error is: 7.2291
 epoch 3/20. Took 0.19655 seconds. Average reconstruction error is: 6.4736
 epoch 4/20. Took 0.19477 seconds. Average reconstruction error is: 6.1004
 epoch 5/20. Took 0.19639 seconds. Average reconstruction error is: 5.869
 epoch 6/20. Took 0.21543 seconds. Average reconstruction error is: 5.7148
 epoch 7/20. Took 0.20581 seconds. Average reconstruction error is: 5.5839
 epoch 8/20. Took 0.20257 seconds. Average reconstruction error is: 5.4879
 epoch 9/20. Took 0.20603 seconds. Average reconstruction error is: 5.3796
 epoch 10/20. Took 0.20645 seconds. Average reconstruction error is: 5.3179
 epoch 11/20. Took 0.20746 seconds. Average reconstruction error is: 5.2456
 epoch 12/20. Took 0.19575 seconds. Average reconstruction error is: 5.1688
 epoch 13/20. Took 0.19747 seconds. Average reconstruction error is: 5.1306
 epoch 14/20. Took 0.2043 seconds. Average reconstruction error is: 5.0981
 epoch 15/20. Took 0.21717 seconds. Average reconstruction error is: 5.0482
 epoch 16/20. Took 0.22698 seconds. Average reconstruction error is: 4.9813
 epoch 17/20. Took 0.21519 seconds. Average reconstruction error is: 4.9934
 epoch 18/20. Took 0.21285 seconds. Average reconstruction error is: 4.953
 epoch 19/20. Took 0.19734 seconds. Average reconstruction error is: 4.9155
 epoch 20/20. Took 0.20433 seconds. Average reconstruction error is: 4.8731
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.63057 seconds. 
  Full-batch training loss = 0.272186, test loss = 0.281999
  Training set accuracy = 0.923300, Test set accuracy = 0.919400
 epoch 2/100. Took 0.62855 seconds. 
  Full-batch training loss = 0.213869, test loss = 0.236479
  Training set accuracy = 0.938300, Test set accuracy = 0.932000
 epoch 3/100. Took 0.65381 seconds. 
  Full-batch training loss = 0.170702, test loss = 0.211548
  Training set accuracy = 0.954600, Test set accuracy = 0.938800
 epoch 4/100. Took 0.71642 seconds. 
  Full-batch training loss = 0.151087, test loss = 0.203207
  Training set accuracy = 0.956300, Test set accuracy = 0.940300
 epoch 5/100. Took 0.65017 seconds. 
  Full-batch training loss = 0.125244, test loss = 0.186728
  Training set accuracy = 0.967000, Test set accuracy = 0.945000
 epoch 6/100. Took 0.62645 seconds. 
  Full-batch training loss = 0.112428, test loss = 0.181689
  Training set accuracy = 0.971600, Test set accuracy = 0.945300
 epoch 7/100. Took 0.61918 seconds. 
  Full-batch training loss = 0.100236, test loss = 0.174859
  Training set accuracy = 0.975800, Test set accuracy = 0.947800
 epoch 8/100. Took 0.62311 seconds. 
  Full-batch training loss = 0.087098, test loss = 0.169397
  Training set accuracy = 0.980200, Test set accuracy = 0.949400
 epoch 9/100. Took 0.61799 seconds. 
  Full-batch training loss = 0.080071, test loss = 0.166221
  Training set accuracy = 0.981100, Test set accuracy = 0.951800
 epoch 10/100. Took 0.62406 seconds. 
  Full-batch training loss = 0.070311, test loss = 0.162793
  Training set accuracy = 0.984500, Test set accuracy = 0.950800
 epoch 11/100. Took 0.61639 seconds. 
  Full-batch training loss = 0.062533, test loss = 0.159945
  Training set accuracy = 0.987100, Test set accuracy = 0.952700
 epoch 12/100. Took 0.69708 seconds. 
  Full-batch training loss = 0.059351, test loss = 0.164810
  Training set accuracy = 0.988100, Test set accuracy = 0.950800
 epoch 13/100. Took 0.62727 seconds. 
  Full-batch training loss = 0.051209, test loss = 0.158866
  Training set accuracy = 0.989600, Test set accuracy = 0.953500
 epoch 14/100. Took 0.63282 seconds. 
  Full-batch training loss = 0.045775, test loss = 0.155632
  Training set accuracy = 0.992300, Test set accuracy = 0.955100
 epoch 15/100. Took 0.62192 seconds. 
  Full-batch training loss = 0.041394, test loss = 0.154925
  Training set accuracy = 0.993400, Test set accuracy = 0.954300
 epoch 16/100. Took 0.59772 seconds. 
  Full-batch training loss = 0.038114, test loss = 0.154169
  Training set accuracy = 0.994400, Test set accuracy = 0.954800
 epoch 17/100. Took 0.6263 seconds. 
  Full-batch training loss = 0.034832, test loss = 0.154113
  Training set accuracy = 0.994800, Test set accuracy = 0.954400
 epoch 18/100. Took 0.58359 seconds. 
  Full-batch training loss = 0.031431, test loss = 0.153965
  Training set accuracy = 0.996000, Test set accuracy = 0.954200
 epoch 19/100. Took 0.57912 seconds. 
  Full-batch training loss = 0.028959, test loss = 0.155760
  Training set accuracy = 0.996600, Test set accuracy = 0.955400
 epoch 20/100. Took 0.58119 seconds. 
  Full-batch training loss = 0.027022, test loss = 0.153441
  Training set accuracy = 0.996800, Test set accuracy = 0.955300
 epoch 21/100. Took 0.57524 seconds. 
  Full-batch training loss = 0.024080, test loss = 0.153168
  Training set accuracy = 0.997500, Test set accuracy = 0.956100
 epoch 22/100. Took 0.57632 seconds. 
  Full-batch training loss = 0.022136, test loss = 0.152537
  Training set accuracy = 0.998100, Test set accuracy = 0.956700
 epoch 23/100. Took 0.63463 seconds. 
  Full-batch training loss = 0.020655, test loss = 0.153799
  Training set accuracy = 0.998300, Test set accuracy = 0.956000
 epoch 24/100. Took 0.60222 seconds. 
  Full-batch training loss = 0.019119, test loss = 0.153952
  Training set accuracy = 0.998500, Test set accuracy = 0.956000
 epoch 25/100. Took 0.6138 seconds. 
  Full-batch training loss = 0.017602, test loss = 0.153605
  Training set accuracy = 0.998600, Test set accuracy = 0.957300
 epoch 26/100. Took 0.65817 seconds. 
  Full-batch training loss = 0.016486, test loss = 0.154587
  Training set accuracy = 0.998900, Test set accuracy = 0.956200
 epoch 27/100. Took 0.59803 seconds. 
  Full-batch training loss = 0.015229, test loss = 0.155139
  Training set accuracy = 0.998800, Test set accuracy = 0.956700
 epoch 28/100. Took 0.58131 seconds. 
  Full-batch training loss = 0.014013, test loss = 0.153663
  Training set accuracy = 0.999200, Test set accuracy = 0.957400
 epoch 29/100. Took 0.5813 seconds. 
  Full-batch training loss = 0.013198, test loss = 0.154380
  Training set accuracy = 0.999300, Test set accuracy = 0.957200
 epoch 30/100. Took 0.58189 seconds. 
  Full-batch training loss = 0.012443, test loss = 0.154288
  Training set accuracy = 0.999300, Test set accuracy = 0.957100
 epoch 31/100. Took 0.57494 seconds. 
  Full-batch training loss = 0.011598, test loss = 0.154902
  Training set accuracy = 0.999600, Test set accuracy = 0.957100
 epoch 32/100. Took 0.57886 seconds. 
  Full-batch training loss = 0.011067, test loss = 0.155302
  Training set accuracy = 0.999800, Test set accuracy = 0.958000
 epoch 33/100. Took 0.57767 seconds. 
  Full-batch training loss = 0.010286, test loss = 0.155636
  Training set accuracy = 0.999800, Test set accuracy = 0.957800
 epoch 34/100. Took 0.60272 seconds. 
  Full-batch training loss = 0.009747, test loss = 0.155976
  Training set accuracy = 0.999800, Test set accuracy = 0.958400
 epoch 35/100. Took 0.57392 seconds. 
  Full-batch training loss = 0.009252, test loss = 0.156739
  Training set accuracy = 0.999800, Test set accuracy = 0.958300
 epoch 36/100. Took 0.5785 seconds. 
  Full-batch training loss = 0.008851, test loss = 0.157061
  Training set accuracy = 0.999900, Test set accuracy = 0.958200
 epoch 37/100. Took 0.59322 seconds. 
  Full-batch training loss = 0.008352, test loss = 0.156499
  Training set accuracy = 0.999900, Test set accuracy = 0.957600
 epoch 38/100. Took 0.63252 seconds. 
  Full-batch training loss = 0.007969, test loss = 0.157520
  Training set accuracy = 0.999900, Test set accuracy = 0.958600
 epoch 39/100. Took 0.57966 seconds. 
  Full-batch training loss = 0.007666, test loss = 0.158263
  Training set accuracy = 0.999800, Test set accuracy = 0.958200
 epoch 40/100. Took 0.57929 seconds. 
  Full-batch training loss = 0.007239, test loss = 0.158337
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 41/100. Took 0.57958 seconds. 
  Full-batch training loss = 0.006992, test loss = 0.159332
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 42/100. Took 0.58325 seconds. 
  Full-batch training loss = 0.006763, test loss = 0.159871
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 43/100. Took 0.5897 seconds. 
  Full-batch training loss = 0.006376, test loss = 0.159348
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 44/100. Took 0.60062 seconds. 
  Full-batch training loss = 0.006181, test loss = 0.160149
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 45/100. Took 0.59929 seconds. 
  Full-batch training loss = 0.005990, test loss = 0.160126
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 46/100. Took 0.667 seconds. 
  Full-batch training loss = 0.005704, test loss = 0.160511
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 47/100. Took 0.6622 seconds. 
  Full-batch training loss = 0.005536, test loss = 0.160545
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 48/100. Took 0.71943 seconds. 
  Full-batch training loss = 0.005320, test loss = 0.160776
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 49/100. Took 0.67628 seconds. 
  Full-batch training loss = 0.005118, test loss = 0.161453
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 50/100. Took 0.73729 seconds. 
  Full-batch training loss = 0.004995, test loss = 0.161513
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 51/100. Took 0.65975 seconds. 
  Full-batch training loss = 0.004782, test loss = 0.161707
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 52/100. Took 0.72052 seconds. 
  Full-batch training loss = 0.004639, test loss = 0.162171
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 53/100. Took 0.59745 seconds. 
  Full-batch training loss = 0.004496, test loss = 0.162564
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 54/100. Took 0.74255 seconds. 
  Full-batch training loss = 0.004362, test loss = 0.162992
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 55/100. Took 0.62139 seconds. 
  Full-batch training loss = 0.004229, test loss = 0.162866
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 56/100. Took 0.69044 seconds. 
  Full-batch training loss = 0.004107, test loss = 0.163169
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 57/100. Took 0.62093 seconds. 
  Full-batch training loss = 0.004008, test loss = 0.163276
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 58/100. Took 0.61289 seconds. 
  Full-batch training loss = 0.003883, test loss = 0.163894
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 59/100. Took 0.57776 seconds. 
  Full-batch training loss = 0.003783, test loss = 0.163751
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 60/100. Took 0.6235 seconds. 
  Full-batch training loss = 0.003682, test loss = 0.163958
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 61/100. Took 0.60693 seconds. 
  Full-batch training loss = 0.003591, test loss = 0.164232
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 62/100. Took 0.57431 seconds. 
  Full-batch training loss = 0.003493, test loss = 0.164404
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 63/100. Took 0.57799 seconds. 
  Full-batch training loss = 0.003411, test loss = 0.164750
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 64/100. Took 0.57508 seconds. 
  Full-batch training loss = 0.003336, test loss = 0.165178
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 65/100. Took 0.59619 seconds. 
  Full-batch training loss = 0.003246, test loss = 0.165175
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 66/100. Took 0.5731 seconds. 
  Full-batch training loss = 0.003173, test loss = 0.165742
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 67/100. Took 0.68852 seconds. 
  Full-batch training loss = 0.003108, test loss = 0.165518
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 68/100. Took 0.60867 seconds. 
  Full-batch training loss = 0.003030, test loss = 0.166383
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 69/100. Took 0.70809 seconds. 
  Full-batch training loss = 0.002960, test loss = 0.166412
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 70/100. Took 0.65195 seconds. 
  Full-batch training loss = 0.002908, test loss = 0.166407
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 71/100. Took 0.62871 seconds. 
  Full-batch training loss = 0.002838, test loss = 0.166616
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 72/100. Took 0.69063 seconds. 
  Full-batch training loss = 0.002784, test loss = 0.166941
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 73/100. Took 0.61849 seconds. 
  Full-batch training loss = 0.002722, test loss = 0.167130
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 74/100. Took 0.57644 seconds. 
  Full-batch training loss = 0.002665, test loss = 0.167292
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 75/100. Took 0.5761 seconds. 
  Full-batch training loss = 0.002609, test loss = 0.167535
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 76/100. Took 0.59215 seconds. 
  Full-batch training loss = 0.002559, test loss = 0.167766
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 77/100. Took 0.59378 seconds. 
  Full-batch training loss = 0.002516, test loss = 0.168119
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 78/100. Took 0.6639 seconds. 
  Full-batch training loss = 0.002461, test loss = 0.168272
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 79/100. Took 0.5752 seconds. 
  Full-batch training loss = 0.002418, test loss = 0.168442
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 80/100. Took 0.57556 seconds. 
  Full-batch training loss = 0.002372, test loss = 0.168884
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 81/100. Took 0.57919 seconds. 
  Full-batch training loss = 0.002328, test loss = 0.168921
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 82/100. Took 0.65113 seconds. 
  Full-batch training loss = 0.002286, test loss = 0.169164
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 83/100. Took 0.62834 seconds. 
  Full-batch training loss = 0.002248, test loss = 0.169305
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 84/100. Took 0.68825 seconds. 
  Full-batch training loss = 0.002207, test loss = 0.169367
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 85/100. Took 0.69038 seconds. 
  Full-batch training loss = 0.002169, test loss = 0.169933
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 86/100. Took 0.69427 seconds. 
  Full-batch training loss = 0.002137, test loss = 0.169982
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 87/100. Took 0.61978 seconds. 
  Full-batch training loss = 0.002101, test loss = 0.169920
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 88/100. Took 0.70473 seconds. 
  Full-batch training loss = 0.002061, test loss = 0.170263
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 89/100. Took 0.58212 seconds. 
  Full-batch training loss = 0.002029, test loss = 0.170159
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 90/100. Took 0.57531 seconds. 
  Full-batch training loss = 0.001997, test loss = 0.170525
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 91/100. Took 0.57736 seconds. 
  Full-batch training loss = 0.001964, test loss = 0.170686
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 92/100. Took 0.57894 seconds. 
  Full-batch training loss = 0.001938, test loss = 0.171091
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 93/100. Took 0.59079 seconds. 
  Full-batch training loss = 0.001904, test loss = 0.171032
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 94/100. Took 0.65298 seconds. 
  Full-batch training loss = 0.001875, test loss = 0.171246
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 95/100. Took 0.5843 seconds. 
  Full-batch training loss = 0.001847, test loss = 0.171277
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 96/100. Took 0.61751 seconds. 
  Full-batch training loss = 0.001820, test loss = 0.171654
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 97/100. Took 0.64233 seconds. 
  Full-batch training loss = 0.001793, test loss = 0.171934
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 98/100. Took 0.62952 seconds. 
  Full-batch training loss = 0.001766, test loss = 0.172005
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 99/100. Took 0.62036 seconds. 
  Full-batch training loss = 0.001741, test loss = 0.172126
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 100/100. Took 0.63262 seconds. 
  Full-batch training loss = 0.001717, test loss = 0.172202
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
Elapsed time is 143.359793 seconds.
End Training
