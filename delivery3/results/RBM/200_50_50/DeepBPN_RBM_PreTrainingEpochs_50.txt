Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	50
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.56498 seconds. Average reconstruction error is: 30.007
 epoch 2/50. Took 0.56657 seconds. Average reconstruction error is: 18.1141
 epoch 3/50. Took 0.5627 seconds. Average reconstruction error is: 15.8017
 epoch 4/50. Took 0.56534 seconds. Average reconstruction error is: 14.3618
 epoch 5/50. Took 0.56528 seconds. Average reconstruction error is: 13.3362
 epoch 6/50. Took 0.56411 seconds. Average reconstruction error is: 12.5509
 epoch 7/50. Took 0.56378 seconds. Average reconstruction error is: 11.9473
 epoch 8/50. Took 0.56685 seconds. Average reconstruction error is: 11.5269
 epoch 9/50. Took 0.56489 seconds. Average reconstruction error is: 11.1647
 epoch 10/50. Took 0.56729 seconds. Average reconstruction error is: 10.8338
 epoch 11/50. Took 0.56598 seconds. Average reconstruction error is: 10.5959
 epoch 12/50. Took 0.57246 seconds. Average reconstruction error is: 10.3947
 epoch 13/50. Took 0.59152 seconds. Average reconstruction error is: 10.1981
 epoch 14/50. Took 0.58839 seconds. Average reconstruction error is: 10.047
 epoch 15/50. Took 0.57532 seconds. Average reconstruction error is: 9.8958
 epoch 16/50. Took 0.56796 seconds. Average reconstruction error is: 9.8028
 epoch 17/50. Took 0.56552 seconds. Average reconstruction error is: 9.7076
 epoch 18/50. Took 0.56483 seconds. Average reconstruction error is: 9.601
 epoch 19/50. Took 0.56905 seconds. Average reconstruction error is: 9.5173
 epoch 20/50. Took 0.58122 seconds. Average reconstruction error is: 9.4096
 epoch 21/50. Took 0.59013 seconds. Average reconstruction error is: 9.3302
 epoch 22/50. Took 0.59192 seconds. Average reconstruction error is: 9.2483
 epoch 23/50. Took 0.58865 seconds. Average reconstruction error is: 9.1825
 epoch 24/50. Took 0.59009 seconds. Average reconstruction error is: 9.1294
 epoch 25/50. Took 0.58976 seconds. Average reconstruction error is: 9.0744
 epoch 26/50. Took 0.58919 seconds. Average reconstruction error is: 9.0274
 epoch 27/50. Took 0.57821 seconds. Average reconstruction error is: 9.0055
 epoch 28/50. Took 0.56869 seconds. Average reconstruction error is: 8.9347
 epoch 29/50. Took 0.56584 seconds. Average reconstruction error is: 8.8913
 epoch 30/50. Took 0.56687 seconds. Average reconstruction error is: 8.8454
 epoch 31/50. Took 0.56768 seconds. Average reconstruction error is: 8.8039
 epoch 32/50. Took 0.5671 seconds. Average reconstruction error is: 8.8004
 epoch 33/50. Took 0.57158 seconds. Average reconstruction error is: 8.7723
 epoch 34/50. Took 0.57033 seconds. Average reconstruction error is: 8.7133
 epoch 35/50. Took 0.57885 seconds. Average reconstruction error is: 8.6714
 epoch 36/50. Took 0.56514 seconds. Average reconstruction error is: 8.6569
 epoch 37/50. Took 0.56586 seconds. Average reconstruction error is: 8.6274
 epoch 38/50. Took 0.56786 seconds. Average reconstruction error is: 8.6053
 epoch 39/50. Took 0.56808 seconds. Average reconstruction error is: 8.5758
 epoch 40/50. Took 0.5695 seconds. Average reconstruction error is: 8.5547
 epoch 41/50. Took 0.56701 seconds. Average reconstruction error is: 8.5314
 epoch 42/50. Took 0.56452 seconds. Average reconstruction error is: 8.5196
 epoch 43/50. Took 0.56949 seconds. Average reconstruction error is: 8.5025
 epoch 44/50. Took 0.56723 seconds. Average reconstruction error is: 8.4639
 epoch 45/50. Took 0.56572 seconds. Average reconstruction error is: 8.4398
 epoch 46/50. Took 0.56719 seconds. Average reconstruction error is: 8.4221
 epoch 47/50. Took 0.56869 seconds. Average reconstruction error is: 8.4099
 epoch 48/50. Took 0.5661 seconds. Average reconstruction error is: 8.4055
 epoch 49/50. Took 0.5687 seconds. Average reconstruction error is: 8.3732
 epoch 50/50. Took 0.56928 seconds. Average reconstruction error is: 8.3322
Training binary-binary RBM in layer 2 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.19994 seconds. Average reconstruction error is: 15.2768
 epoch 2/50. Took 0.20208 seconds. Average reconstruction error is: 9.2728
 epoch 3/50. Took 0.20529 seconds. Average reconstruction error is: 8.1159
 epoch 4/50. Took 0.20471 seconds. Average reconstruction error is: 7.5389
 epoch 5/50. Took 0.20564 seconds. Average reconstruction error is: 7.1571
 epoch 6/50. Took 0.20604 seconds. Average reconstruction error is: 6.8899
 epoch 7/50. Took 0.20522 seconds. Average reconstruction error is: 6.714
 epoch 8/50. Took 0.20596 seconds. Average reconstruction error is: 6.5209
 epoch 9/50. Took 0.20438 seconds. Average reconstruction error is: 6.3547
 epoch 10/50. Took 0.20442 seconds. Average reconstruction error is: 6.2205
 epoch 11/50. Took 0.20365 seconds. Average reconstruction error is: 6.11
 epoch 12/50. Took 0.20546 seconds. Average reconstruction error is: 6.0043
 epoch 13/50. Took 0.2042 seconds. Average reconstruction error is: 5.926
 epoch 14/50. Took 0.20529 seconds. Average reconstruction error is: 5.8577
 epoch 15/50. Took 0.20472 seconds. Average reconstruction error is: 5.7749
 epoch 16/50. Took 0.20496 seconds. Average reconstruction error is: 5.6995
 epoch 17/50. Took 0.20473 seconds. Average reconstruction error is: 5.6594
 epoch 18/50. Took 0.20505 seconds. Average reconstruction error is: 5.5987
 epoch 19/50. Took 0.20489 seconds. Average reconstruction error is: 5.5332
 epoch 20/50. Took 0.20505 seconds. Average reconstruction error is: 5.4892
 epoch 21/50. Took 0.20492 seconds. Average reconstruction error is: 5.4079
 epoch 22/50. Took 0.20457 seconds. Average reconstruction error is: 5.3588
 epoch 23/50. Took 0.20466 seconds. Average reconstruction error is: 5.3309
 epoch 24/50. Took 0.20502 seconds. Average reconstruction error is: 5.2864
 epoch 25/50. Took 0.2052 seconds. Average reconstruction error is: 5.2189
 epoch 26/50. Took 0.20665 seconds. Average reconstruction error is: 5.205
 epoch 27/50. Took 0.20543 seconds. Average reconstruction error is: 5.1603
 epoch 28/50. Took 0.20507 seconds. Average reconstruction error is: 5.1023
 epoch 29/50. Took 0.20581 seconds. Average reconstruction error is: 5.087
 epoch 30/50. Took 0.20562 seconds. Average reconstruction error is: 5.0539
 epoch 31/50. Took 0.20589 seconds. Average reconstruction error is: 5.0352
 epoch 32/50. Took 0.20607 seconds. Average reconstruction error is: 4.9586
 epoch 33/50. Took 0.20658 seconds. Average reconstruction error is: 4.9722
 epoch 34/50. Took 0.20669 seconds. Average reconstruction error is: 4.9223
 epoch 35/50. Took 0.20573 seconds. Average reconstruction error is: 4.9052
 epoch 36/50. Took 0.20598 seconds. Average reconstruction error is: 4.8806
 epoch 37/50. Took 0.20573 seconds. Average reconstruction error is: 4.8298
 epoch 38/50. Took 0.20607 seconds. Average reconstruction error is: 4.8088
 epoch 39/50. Took 0.20601 seconds. Average reconstruction error is: 4.7898
 epoch 40/50. Took 0.20551 seconds. Average reconstruction error is: 4.7493
 epoch 41/50. Took 0.2065 seconds. Average reconstruction error is: 4.7529
 epoch 42/50. Took 0.20431 seconds. Average reconstruction error is: 4.7408
 epoch 43/50. Took 0.20655 seconds. Average reconstruction error is: 4.7073
 epoch 44/50. Took 0.2062 seconds. Average reconstruction error is: 4.6744
 epoch 45/50. Took 0.20432 seconds. Average reconstruction error is: 4.6499
 epoch 46/50. Took 0.20432 seconds. Average reconstruction error is: 4.6321
 epoch 47/50. Took 0.20469 seconds. Average reconstruction error is: 4.6124
 epoch 48/50. Took 0.20404 seconds. Average reconstruction error is: 4.6206
 epoch 49/50. Took 0.20538 seconds. Average reconstruction error is: 4.5897
 epoch 50/50. Took 0.20491 seconds. Average reconstruction error is: 4.5654
Training binary-binary RBM in layer 3 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.20261 seconds. Average reconstruction error is: 12.2991
 epoch 2/50. Took 0.20369 seconds. Average reconstruction error is: 7.0853
 epoch 3/50. Took 0.20317 seconds. Average reconstruction error is: 6.2289
 epoch 4/50. Took 0.20523 seconds. Average reconstruction error is: 5.8712
 epoch 5/50. Took 0.20476 seconds. Average reconstruction error is: 5.57
 epoch 6/50. Took 0.20527 seconds. Average reconstruction error is: 5.4352
 epoch 7/50. Took 0.20483 seconds. Average reconstruction error is: 5.3121
 epoch 8/50. Took 0.20576 seconds. Average reconstruction error is: 5.1974
 epoch 9/50. Took 0.20485 seconds. Average reconstruction error is: 5.0928
 epoch 10/50. Took 0.20612 seconds. Average reconstruction error is: 5.0222
 epoch 11/50. Took 0.20627 seconds. Average reconstruction error is: 4.9524
 epoch 12/50. Took 0.20494 seconds. Average reconstruction error is: 4.9022
 epoch 13/50. Took 0.20506 seconds. Average reconstruction error is: 4.8414
 epoch 14/50. Took 0.20563 seconds. Average reconstruction error is: 4.8114
 epoch 15/50. Took 0.20546 seconds. Average reconstruction error is: 4.7305
 epoch 16/50. Took 0.20514 seconds. Average reconstruction error is: 4.6976
 epoch 17/50. Took 0.20526 seconds. Average reconstruction error is: 4.6231
 epoch 18/50. Took 0.20578 seconds. Average reconstruction error is: 4.5766
 epoch 19/50. Took 0.20665 seconds. Average reconstruction error is: 4.5456
 epoch 20/50. Took 0.2057 seconds. Average reconstruction error is: 4.5209
 epoch 21/50. Took 0.20464 seconds. Average reconstruction error is: 4.471
 epoch 22/50. Took 0.20569 seconds. Average reconstruction error is: 4.4463
 epoch 23/50. Took 0.20446 seconds. Average reconstruction error is: 4.4312
 epoch 24/50. Took 0.20497 seconds. Average reconstruction error is: 4.4156
 epoch 25/50. Took 0.20599 seconds. Average reconstruction error is: 4.3801
 epoch 26/50. Took 0.20718 seconds. Average reconstruction error is: 4.3546
 epoch 27/50. Took 0.20565 seconds. Average reconstruction error is: 4.2986
 epoch 28/50. Took 0.20538 seconds. Average reconstruction error is: 4.2878
 epoch 29/50. Took 0.20656 seconds. Average reconstruction error is: 4.2685
 epoch 30/50. Took 0.20625 seconds. Average reconstruction error is: 4.2164
 epoch 31/50. Took 0.20655 seconds. Average reconstruction error is: 4.1967
 epoch 32/50. Took 0.20641 seconds. Average reconstruction error is: 4.2202
 epoch 33/50. Took 0.20608 seconds. Average reconstruction error is: 4.158
 epoch 34/50. Took 0.20511 seconds. Average reconstruction error is: 4.1436
 epoch 35/50. Took 0.20712 seconds. Average reconstruction error is: 4.1108
 epoch 36/50. Took 0.20637 seconds. Average reconstruction error is: 4.1104
 epoch 37/50. Took 0.20541 seconds. Average reconstruction error is: 4.0788
 epoch 38/50. Took 0.20495 seconds. Average reconstruction error is: 4.0706
 epoch 39/50. Took 0.20531 seconds. Average reconstruction error is: 4.058
 epoch 40/50. Took 0.20509 seconds. Average reconstruction error is: 4.0433
 epoch 41/50. Took 0.20476 seconds. Average reconstruction error is: 4.0204
 epoch 42/50. Took 0.20508 seconds. Average reconstruction error is: 4.014
 epoch 43/50. Took 0.20629 seconds. Average reconstruction error is: 3.9954
 epoch 44/50. Took 0.20608 seconds. Average reconstruction error is: 3.9642
 epoch 45/50. Took 0.20588 seconds. Average reconstruction error is: 3.9648
 epoch 46/50. Took 0.20539 seconds. Average reconstruction error is: 3.9423
 epoch 47/50. Took 0.20537 seconds. Average reconstruction error is: 3.9059
 epoch 48/50. Took 0.20614 seconds. Average reconstruction error is: 3.899
 epoch 49/50. Took 0.20611 seconds. Average reconstruction error is: 3.883
 epoch 50/50. Took 0.20543 seconds. Average reconstruction error is: 3.8366
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.56749 seconds. 
  Full-batch training loss = 0.292017, test loss = 0.292058
  Training set accuracy = 0.915900, Test set accuracy = 0.916400
 epoch 2/100. Took 0.57301 seconds. 
  Full-batch training loss = 0.223699, test loss = 0.239672
  Training set accuracy = 0.935400, Test set accuracy = 0.931900
 epoch 3/100. Took 0.57389 seconds. 
  Full-batch training loss = 0.183561, test loss = 0.209107
  Training set accuracy = 0.946300, Test set accuracy = 0.939700
 epoch 4/100. Took 0.57067 seconds. 
  Full-batch training loss = 0.164981, test loss = 0.204491
  Training set accuracy = 0.952400, Test set accuracy = 0.940100
 epoch 5/100. Took 0.56834 seconds. 
  Full-batch training loss = 0.140743, test loss = 0.188608
  Training set accuracy = 0.958900, Test set accuracy = 0.945100
 epoch 6/100. Took 0.57545 seconds. 
  Full-batch training loss = 0.121540, test loss = 0.177773
  Training set accuracy = 0.968100, Test set accuracy = 0.949500
 epoch 7/100. Took 0.57406 seconds. 
  Full-batch training loss = 0.107344, test loss = 0.171637
  Training set accuracy = 0.972900, Test set accuracy = 0.949800
 epoch 8/100. Took 0.57008 seconds. 
  Full-batch training loss = 0.098175, test loss = 0.170007
  Training set accuracy = 0.975600, Test set accuracy = 0.950800
 epoch 9/100. Took 0.57095 seconds. 
  Full-batch training loss = 0.086343, test loss = 0.163306
  Training set accuracy = 0.980500, Test set accuracy = 0.952800
 epoch 10/100. Took 0.57333 seconds. 
  Full-batch training loss = 0.079851, test loss = 0.161933
  Training set accuracy = 0.983000, Test set accuracy = 0.952500
 epoch 11/100. Took 0.5747 seconds. 
  Full-batch training loss = 0.071299, test loss = 0.158626
  Training set accuracy = 0.985300, Test set accuracy = 0.954600
 epoch 12/100. Took 0.57382 seconds. 
  Full-batch training loss = 0.066944, test loss = 0.159595
  Training set accuracy = 0.985700, Test set accuracy = 0.953800
 epoch 13/100. Took 0.57606 seconds. 
  Full-batch training loss = 0.059008, test loss = 0.156001
  Training set accuracy = 0.989300, Test set accuracy = 0.955400
 epoch 14/100. Took 0.57074 seconds. 
  Full-batch training loss = 0.052913, test loss = 0.153766
  Training set accuracy = 0.990300, Test set accuracy = 0.954400
 epoch 15/100. Took 0.57523 seconds. 
  Full-batch training loss = 0.048699, test loss = 0.152737
  Training set accuracy = 0.992200, Test set accuracy = 0.956400
 epoch 16/100. Took 0.57288 seconds. 
  Full-batch training loss = 0.047009, test loss = 0.154024
  Training set accuracy = 0.991700, Test set accuracy = 0.956900
 epoch 17/100. Took 0.57619 seconds. 
  Full-batch training loss = 0.041343, test loss = 0.151644
  Training set accuracy = 0.994100, Test set accuracy = 0.956500
 epoch 18/100. Took 0.57584 seconds. 
  Full-batch training loss = 0.039154, test loss = 0.151560
  Training set accuracy = 0.994200, Test set accuracy = 0.957700
 epoch 19/100. Took 0.57556 seconds. 
  Full-batch training loss = 0.034442, test loss = 0.149529
  Training set accuracy = 0.995500, Test set accuracy = 0.957900
 epoch 20/100. Took 0.57599 seconds. 
  Full-batch training loss = 0.032528, test loss = 0.151546
  Training set accuracy = 0.995900, Test set accuracy = 0.957200
 epoch 21/100. Took 0.57256 seconds. 
  Full-batch training loss = 0.029078, test loss = 0.149824
  Training set accuracy = 0.996500, Test set accuracy = 0.958900
 epoch 22/100. Took 0.56877 seconds. 
  Full-batch training loss = 0.027429, test loss = 0.149417
  Training set accuracy = 0.996900, Test set accuracy = 0.958200
 epoch 23/100. Took 0.57364 seconds. 
  Full-batch training loss = 0.024615, test loss = 0.148240
  Training set accuracy = 0.997700, Test set accuracy = 0.959400
 epoch 24/100. Took 0.57154 seconds. 
  Full-batch training loss = 0.023586, test loss = 0.149108
  Training set accuracy = 0.997700, Test set accuracy = 0.959600
 epoch 25/100. Took 0.56757 seconds. 
  Full-batch training loss = 0.021461, test loss = 0.148735
  Training set accuracy = 0.998000, Test set accuracy = 0.958300
 epoch 26/100. Took 0.57493 seconds. 
  Full-batch training loss = 0.019657, test loss = 0.148454
  Training set accuracy = 0.998400, Test set accuracy = 0.959400
 epoch 27/100. Took 0.56885 seconds. 
  Full-batch training loss = 0.018516, test loss = 0.148517
  Training set accuracy = 0.998700, Test set accuracy = 0.960400
 epoch 28/100. Took 0.5704 seconds. 
  Full-batch training loss = 0.017573, test loss = 0.149510
  Training set accuracy = 0.998900, Test set accuracy = 0.959300
 epoch 29/100. Took 0.57167 seconds. 
  Full-batch training loss = 0.016280, test loss = 0.149091
  Training set accuracy = 0.999100, Test set accuracy = 0.960100
 epoch 30/100. Took 0.56515 seconds. 
  Full-batch training loss = 0.015727, test loss = 0.149712
  Training set accuracy = 0.998900, Test set accuracy = 0.959000
 epoch 31/100. Took 0.56936 seconds. 
  Full-batch training loss = 0.014607, test loss = 0.150674
  Training set accuracy = 0.999400, Test set accuracy = 0.959600
 epoch 32/100. Took 0.56896 seconds. 
  Full-batch training loss = 0.013430, test loss = 0.149271
  Training set accuracy = 0.999500, Test set accuracy = 0.960400
 epoch 33/100. Took 0.57189 seconds. 
  Full-batch training loss = 0.012514, test loss = 0.149221
  Training set accuracy = 0.999600, Test set accuracy = 0.960500
 epoch 34/100. Took 0.57151 seconds. 
  Full-batch training loss = 0.011949, test loss = 0.149602
  Training set accuracy = 0.999700, Test set accuracy = 0.960300
 epoch 35/100. Took 0.57508 seconds. 
  Full-batch training loss = 0.011319, test loss = 0.149490
  Training set accuracy = 0.999800, Test set accuracy = 0.959900
 epoch 36/100. Took 0.57208 seconds. 
  Full-batch training loss = 0.010847, test loss = 0.150790
  Training set accuracy = 0.999800, Test set accuracy = 0.960400
 epoch 37/100. Took 0.56675 seconds. 
  Full-batch training loss = 0.010316, test loss = 0.150752
  Training set accuracy = 0.999700, Test set accuracy = 0.960500
 epoch 38/100. Took 0.57047 seconds. 
  Full-batch training loss = 0.009638, test loss = 0.150372
  Training set accuracy = 0.999800, Test set accuracy = 0.960300
 epoch 39/100. Took 0.57051 seconds. 
  Full-batch training loss = 0.009182, test loss = 0.150623
  Training set accuracy = 0.999800, Test set accuracy = 0.961100
 epoch 40/100. Took 0.57182 seconds. 
  Full-batch training loss = 0.008779, test loss = 0.151128
  Training set accuracy = 0.999700, Test set accuracy = 0.960600
 epoch 41/100. Took 0.56949 seconds. 
  Full-batch training loss = 0.008393, test loss = 0.151197
  Training set accuracy = 0.999800, Test set accuracy = 0.960400
 epoch 42/100. Took 0.57243 seconds. 
  Full-batch training loss = 0.008112, test loss = 0.151896
  Training set accuracy = 0.999900, Test set accuracy = 0.960000
 epoch 43/100. Took 0.57177 seconds. 
  Full-batch training loss = 0.007668, test loss = 0.151764
  Training set accuracy = 0.999900, Test set accuracy = 0.960300
 epoch 44/100. Took 0.57005 seconds. 
  Full-batch training loss = 0.007369, test loss = 0.152153
  Training set accuracy = 0.999900, Test set accuracy = 0.960400
 epoch 45/100. Took 0.56914 seconds. 
  Full-batch training loss = 0.007098, test loss = 0.152036
  Training set accuracy = 0.999900, Test set accuracy = 0.960800
 epoch 46/100. Took 0.57123 seconds. 
  Full-batch training loss = 0.006764, test loss = 0.152625
  Training set accuracy = 0.999900, Test set accuracy = 0.960500
 epoch 47/100. Took 0.56804 seconds. 
  Full-batch training loss = 0.006615, test loss = 0.153021
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 48/100. Took 0.57238 seconds. 
  Full-batch training loss = 0.006325, test loss = 0.152902
  Training set accuracy = 0.999900, Test set accuracy = 0.960400
 epoch 49/100. Took 0.57927 seconds. 
  Full-batch training loss = 0.006084, test loss = 0.153264
  Training set accuracy = 0.999900, Test set accuracy = 0.960400
 epoch 50/100. Took 0.57338 seconds. 
  Full-batch training loss = 0.005909, test loss = 0.153461
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 51/100. Took 0.5705 seconds. 
  Full-batch training loss = 0.005683, test loss = 0.153674
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 52/100. Took 0.57162 seconds. 
  Full-batch training loss = 0.005523, test loss = 0.154014
  Training set accuracy = 1.000000, Test set accuracy = 0.960700
 epoch 53/100. Took 0.57747 seconds. 
  Full-batch training loss = 0.005299, test loss = 0.154074
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 54/100. Took 0.57695 seconds. 
  Full-batch training loss = 0.005139, test loss = 0.154260
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 55/100. Took 0.57174 seconds. 
  Full-batch training loss = 0.005010, test loss = 0.154427
  Training set accuracy = 1.000000, Test set accuracy = 0.960800
 epoch 56/100. Took 0.57265 seconds. 
  Full-batch training loss = 0.004834, test loss = 0.154638
  Training set accuracy = 1.000000, Test set accuracy = 0.960800
 epoch 57/100. Took 0.57123 seconds. 
  Full-batch training loss = 0.004713, test loss = 0.155132
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 58/100. Took 0.57093 seconds. 
  Full-batch training loss = 0.004557, test loss = 0.155100
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 59/100. Took 0.57226 seconds. 
  Full-batch training loss = 0.004448, test loss = 0.155679
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 60/100. Took 0.57393 seconds. 
  Full-batch training loss = 0.004318, test loss = 0.155819
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 61/100. Took 0.57438 seconds. 
  Full-batch training loss = 0.004192, test loss = 0.155871
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 62/100. Took 0.56643 seconds. 
  Full-batch training loss = 0.004104, test loss = 0.156119
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 63/100. Took 0.57 seconds. 
  Full-batch training loss = 0.003987, test loss = 0.156321
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 64/100. Took 0.57481 seconds. 
  Full-batch training loss = 0.003882, test loss = 0.156669
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 65/100. Took 0.56806 seconds. 
  Full-batch training loss = 0.003798, test loss = 0.156926
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 66/100. Took 0.56672 seconds. 
  Full-batch training loss = 0.003696, test loss = 0.156967
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 67/100. Took 0.56969 seconds. 
  Full-batch training loss = 0.003609, test loss = 0.156929
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 68/100. Took 0.57286 seconds. 
  Full-batch training loss = 0.003544, test loss = 0.157526
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 69/100. Took 0.56923 seconds. 
  Full-batch training loss = 0.003449, test loss = 0.157786
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 70/100. Took 0.57515 seconds. 
  Full-batch training loss = 0.003370, test loss = 0.157953
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 71/100. Took 0.57231 seconds. 
  Full-batch training loss = 0.003298, test loss = 0.157764
  Training set accuracy = 1.000000, Test set accuracy = 0.961300
 epoch 72/100. Took 0.57544 seconds. 
  Full-batch training loss = 0.003226, test loss = 0.158181
  Training set accuracy = 1.000000, Test set accuracy = 0.960800
 epoch 73/100. Took 0.57312 seconds. 
  Full-batch training loss = 0.003161, test loss = 0.158259
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 74/100. Took 0.57292 seconds. 
  Full-batch training loss = 0.003099, test loss = 0.158674
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 75/100. Took 0.5754 seconds. 
  Full-batch training loss = 0.003023, test loss = 0.158828
  Training set accuracy = 1.000000, Test set accuracy = 0.961300
 epoch 76/100. Took 0.57061 seconds. 
  Full-batch training loss = 0.002966, test loss = 0.158974
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 77/100. Took 0.57413 seconds. 
  Full-batch training loss = 0.002911, test loss = 0.159162
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 78/100. Took 0.57327 seconds. 
  Full-batch training loss = 0.002849, test loss = 0.159428
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 79/100. Took 0.56816 seconds. 
  Full-batch training loss = 0.002797, test loss = 0.159543
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 80/100. Took 0.57709 seconds. 
  Full-batch training loss = 0.002741, test loss = 0.159651
  Training set accuracy = 1.000000, Test set accuracy = 0.961400
 epoch 81/100. Took 0.57555 seconds. 
  Full-batch training loss = 0.002691, test loss = 0.159913
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 82/100. Took 0.57141 seconds. 
  Full-batch training loss = 0.002643, test loss = 0.160070
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 83/100. Took 0.57289 seconds. 
  Full-batch training loss = 0.002596, test loss = 0.160350
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 84/100. Took 0.57486 seconds. 
  Full-batch training loss = 0.002547, test loss = 0.160393
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 85/100. Took 0.57259 seconds. 
  Full-batch training loss = 0.002501, test loss = 0.160496
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 86/100. Took 0.5696 seconds. 
  Full-batch training loss = 0.002462, test loss = 0.160696
  Training set accuracy = 1.000000, Test set accuracy = 0.961400
 epoch 87/100. Took 0.57368 seconds. 
  Full-batch training loss = 0.002418, test loss = 0.160851
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 88/100. Took 0.57672 seconds. 
  Full-batch training loss = 0.002376, test loss = 0.161007
  Training set accuracy = 1.000000, Test set accuracy = 0.961300
 epoch 89/100. Took 0.5715 seconds. 
  Full-batch training loss = 0.002336, test loss = 0.161253
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 90/100. Took 0.57584 seconds. 
  Full-batch training loss = 0.002300, test loss = 0.161320
  Training set accuracy = 1.000000, Test set accuracy = 0.961400
 epoch 91/100. Took 0.5711 seconds. 
  Full-batch training loss = 0.002263, test loss = 0.161526
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 92/100. Took 0.56731 seconds. 
  Full-batch training loss = 0.002231, test loss = 0.161703
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 93/100. Took 0.57226 seconds. 
  Full-batch training loss = 0.002190, test loss = 0.161849
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 94/100. Took 0.56985 seconds. 
  Full-batch training loss = 0.002156, test loss = 0.161966
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 95/100. Took 0.57421 seconds. 
  Full-batch training loss = 0.002126, test loss = 0.162295
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 96/100. Took 0.57053 seconds. 
  Full-batch training loss = 0.002092, test loss = 0.162358
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 97/100. Took 0.57463 seconds. 
  Full-batch training loss = 0.002062, test loss = 0.162464
  Training set accuracy = 1.000000, Test set accuracy = 0.961300
 epoch 98/100. Took 0.57673 seconds. 
  Full-batch training loss = 0.002029, test loss = 0.162525
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 99/100. Took 0.57088 seconds. 
  Full-batch training loss = 0.002002, test loss = 0.162714
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 100/100. Took 0.57298 seconds. 
  Full-batch training loss = 0.001971, test loss = 0.162835
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
Elapsed time is 164.037700 seconds.
End Training
