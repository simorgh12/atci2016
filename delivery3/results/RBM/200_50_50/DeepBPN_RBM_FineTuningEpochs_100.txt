Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.58453 seconds. Average reconstruction error is: 29.3736
 epoch 2/20. Took 0.5873 seconds. Average reconstruction error is: 17.6831
 epoch 3/20. Took 0.59325 seconds. Average reconstruction error is: 15.3752
 epoch 4/20. Took 0.58852 seconds. Average reconstruction error is: 14.0085
 epoch 5/20. Took 0.5871 seconds. Average reconstruction error is: 13.0088
 epoch 6/20. Took 0.57177 seconds. Average reconstruction error is: 12.2543
 epoch 7/20. Took 0.57645 seconds. Average reconstruction error is: 11.6929
 epoch 8/20. Took 0.58776 seconds. Average reconstruction error is: 11.2527
 epoch 9/20. Took 0.58821 seconds. Average reconstruction error is: 10.8933
 epoch 10/20. Took 0.58396 seconds. Average reconstruction error is: 10.6086
 epoch 11/20. Took 0.56998 seconds. Average reconstruction error is: 10.398
 epoch 12/20. Took 0.56937 seconds. Average reconstruction error is: 10.1893
 epoch 13/20. Took 0.57694 seconds. Average reconstruction error is: 10.0188
 epoch 14/20. Took 0.5914 seconds. Average reconstruction error is: 9.8839
 epoch 15/20. Took 0.58984 seconds. Average reconstruction error is: 9.8042
 epoch 16/20. Took 0.59172 seconds. Average reconstruction error is: 9.6795
 epoch 17/20. Took 0.59226 seconds. Average reconstruction error is: 9.571
 epoch 18/20. Took 0.59075 seconds. Average reconstruction error is: 9.46
 epoch 19/20. Took 0.57166 seconds. Average reconstruction error is: 9.3838
 epoch 20/20. Took 0.5673 seconds. Average reconstruction error is: 9.3103
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.19859 seconds. Average reconstruction error is: 13.9709
 epoch 2/20. Took 0.20426 seconds. Average reconstruction error is: 8.2784
 epoch 3/20. Took 0.20564 seconds. Average reconstruction error is: 7.2891
 epoch 4/20. Took 0.20458 seconds. Average reconstruction error is: 6.7883
 epoch 5/20. Took 0.2048 seconds. Average reconstruction error is: 6.5088
 epoch 6/20. Took 0.20494 seconds. Average reconstruction error is: 6.2586
 epoch 7/20. Took 0.20544 seconds. Average reconstruction error is: 6.0904
 epoch 8/20. Took 0.20508 seconds. Average reconstruction error is: 5.9547
 epoch 9/20. Took 0.20501 seconds. Average reconstruction error is: 5.7981
 epoch 10/20. Took 0.20566 seconds. Average reconstruction error is: 5.7135
 epoch 11/20. Took 0.20709 seconds. Average reconstruction error is: 5.6235
 epoch 12/20. Took 0.2063 seconds. Average reconstruction error is: 5.5281
 epoch 13/20. Took 0.20553 seconds. Average reconstruction error is: 5.4513
 epoch 14/20. Took 0.20528 seconds. Average reconstruction error is: 5.3771
 epoch 15/20. Took 0.20477 seconds. Average reconstruction error is: 5.3272
 epoch 16/20. Took 0.20538 seconds. Average reconstruction error is: 5.2528
 epoch 17/20. Took 0.20615 seconds. Average reconstruction error is: 5.1838
 epoch 18/20. Took 0.2052 seconds. Average reconstruction error is: 5.138
 epoch 19/20. Took 0.20602 seconds. Average reconstruction error is: 5.0997
 epoch 20/20. Took 0.20697 seconds. Average reconstruction error is: 5.0242
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20219 seconds. Average reconstruction error is: 12.3236
 epoch 2/20. Took 0.20341 seconds. Average reconstruction error is: 7.2447
 epoch 3/20. Took 0.20421 seconds. Average reconstruction error is: 6.4252
 epoch 4/20. Took 0.20404 seconds. Average reconstruction error is: 6.0486
 epoch 5/20. Took 0.20436 seconds. Average reconstruction error is: 5.8116
 epoch 6/20. Took 0.20459 seconds. Average reconstruction error is: 5.6504
 epoch 7/20. Took 0.2048 seconds. Average reconstruction error is: 5.5014
 epoch 8/20. Took 0.20546 seconds. Average reconstruction error is: 5.3863
 epoch 9/20. Took 0.20504 seconds. Average reconstruction error is: 5.321
 epoch 10/20. Took 0.20542 seconds. Average reconstruction error is: 5.2167
 epoch 11/20. Took 0.20585 seconds. Average reconstruction error is: 5.1339
 epoch 12/20. Took 0.20624 seconds. Average reconstruction error is: 5.0795
 epoch 13/20. Took 0.20523 seconds. Average reconstruction error is: 5.0116
 epoch 14/20. Took 0.20514 seconds. Average reconstruction error is: 4.9674
 epoch 15/20. Took 0.20461 seconds. Average reconstruction error is: 4.9432
 epoch 16/20. Took 0.2039 seconds. Average reconstruction error is: 4.8831
 epoch 17/20. Took 0.20567 seconds. Average reconstruction error is: 4.85
 epoch 18/20. Took 0.20494 seconds. Average reconstruction error is: 4.8005
 epoch 19/20. Took 0.2057 seconds. Average reconstruction error is: 4.7901
 epoch 20/20. Took 0.20464 seconds. Average reconstruction error is: 4.7516
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.57168 seconds. 
  Full-batch training loss = 0.285880, test loss = 0.290934
  Training set accuracy = 0.921500, Test set accuracy = 0.917700
 epoch 2/100. Took 0.57084 seconds. 
  Full-batch training loss = 0.235588, test loss = 0.256101
  Training set accuracy = 0.931800, Test set accuracy = 0.923200
 epoch 3/100. Took 0.57471 seconds. 
  Full-batch training loss = 0.186045, test loss = 0.216712
  Training set accuracy = 0.949400, Test set accuracy = 0.937500
 epoch 4/100. Took 0.57505 seconds. 
  Full-batch training loss = 0.155783, test loss = 0.197203
  Training set accuracy = 0.957900, Test set accuracy = 0.944500
 epoch 5/100. Took 0.57289 seconds. 
  Full-batch training loss = 0.137752, test loss = 0.186364
  Training set accuracy = 0.964900, Test set accuracy = 0.947800
 epoch 6/100. Took 0.57407 seconds. 
  Full-batch training loss = 0.123050, test loss = 0.181655
  Training set accuracy = 0.968300, Test set accuracy = 0.949300
 epoch 7/100. Took 0.57661 seconds. 
  Full-batch training loss = 0.109017, test loss = 0.174791
  Training set accuracy = 0.973300, Test set accuracy = 0.950500
 epoch 8/100. Took 0.57533 seconds. 
  Full-batch training loss = 0.100714, test loss = 0.173676
  Training set accuracy = 0.973300, Test set accuracy = 0.950100
 epoch 9/100. Took 0.57269 seconds. 
  Full-batch training loss = 0.085915, test loss = 0.164899
  Training set accuracy = 0.981000, Test set accuracy = 0.952700
 epoch 10/100. Took 0.57435 seconds. 
  Full-batch training loss = 0.080656, test loss = 0.164310
  Training set accuracy = 0.982800, Test set accuracy = 0.953300
 epoch 11/100. Took 0.57729 seconds. 
  Full-batch training loss = 0.070980, test loss = 0.160994
  Training set accuracy = 0.983600, Test set accuracy = 0.954100
 epoch 12/100. Took 0.57167 seconds. 
  Full-batch training loss = 0.062340, test loss = 0.156506
  Training set accuracy = 0.987400, Test set accuracy = 0.956400
 epoch 13/100. Took 0.57086 seconds. 
  Full-batch training loss = 0.059132, test loss = 0.158888
  Training set accuracy = 0.987900, Test set accuracy = 0.955400
 epoch 14/100. Took 0.57089 seconds. 
  Full-batch training loss = 0.052132, test loss = 0.154299
  Training set accuracy = 0.990000, Test set accuracy = 0.956400
 epoch 15/100. Took 0.56966 seconds. 
  Full-batch training loss = 0.046193, test loss = 0.152614
  Training set accuracy = 0.991400, Test set accuracy = 0.956800
 epoch 16/100. Took 0.56899 seconds. 
  Full-batch training loss = 0.043261, test loss = 0.152484
  Training set accuracy = 0.992400, Test set accuracy = 0.956500
 epoch 17/100. Took 0.57616 seconds. 
  Full-batch training loss = 0.039627, test loss = 0.152787
  Training set accuracy = 0.992900, Test set accuracy = 0.956200
 epoch 18/100. Took 0.56871 seconds. 
  Full-batch training loss = 0.036375, test loss = 0.151333
  Training set accuracy = 0.994400, Test set accuracy = 0.956700
 epoch 19/100. Took 0.57478 seconds. 
  Full-batch training loss = 0.032501, test loss = 0.150635
  Training set accuracy = 0.995400, Test set accuracy = 0.957700
 epoch 20/100. Took 0.57517 seconds. 
  Full-batch training loss = 0.029748, test loss = 0.150346
  Training set accuracy = 0.996200, Test set accuracy = 0.958400
 epoch 21/100. Took 0.57435 seconds. 
  Full-batch training loss = 0.027636, test loss = 0.150786
  Training set accuracy = 0.996400, Test set accuracy = 0.958300
 epoch 22/100. Took 0.57284 seconds. 
  Full-batch training loss = 0.025206, test loss = 0.149739
  Training set accuracy = 0.996800, Test set accuracy = 0.958600
 epoch 23/100. Took 0.57247 seconds. 
  Full-batch training loss = 0.024308, test loss = 0.151846
  Training set accuracy = 0.997500, Test set accuracy = 0.958100
 epoch 24/100. Took 0.57324 seconds. 
  Full-batch training loss = 0.021948, test loss = 0.150862
  Training set accuracy = 0.998100, Test set accuracy = 0.958000
 epoch 25/100. Took 0.57502 seconds. 
  Full-batch training loss = 0.020097, test loss = 0.150467
  Training set accuracy = 0.998500, Test set accuracy = 0.958500
 epoch 26/100. Took 0.56676 seconds. 
  Full-batch training loss = 0.018679, test loss = 0.150308
  Training set accuracy = 0.999000, Test set accuracy = 0.958600
 epoch 27/100. Took 0.57264 seconds. 
  Full-batch training loss = 0.017453, test loss = 0.151233
  Training set accuracy = 0.999100, Test set accuracy = 0.958700
 epoch 28/100. Took 0.57368 seconds. 
  Full-batch training loss = 0.016576, test loss = 0.152044
  Training set accuracy = 0.999200, Test set accuracy = 0.959000
 epoch 29/100. Took 0.57122 seconds. 
  Full-batch training loss = 0.015263, test loss = 0.151617
  Training set accuracy = 0.999300, Test set accuracy = 0.958700
 epoch 30/100. Took 0.57061 seconds. 
  Full-batch training loss = 0.014335, test loss = 0.151841
  Training set accuracy = 0.999300, Test set accuracy = 0.958600
 epoch 31/100. Took 0.57438 seconds. 
  Full-batch training loss = 0.013581, test loss = 0.152844
  Training set accuracy = 0.999600, Test set accuracy = 0.958900
 epoch 32/100. Took 0.57029 seconds. 
  Full-batch training loss = 0.012598, test loss = 0.152942
  Training set accuracy = 0.999500, Test set accuracy = 0.958800
 epoch 33/100. Took 0.57859 seconds. 
  Full-batch training loss = 0.011914, test loss = 0.153153
  Training set accuracy = 0.999600, Test set accuracy = 0.959200
 epoch 34/100. Took 0.57231 seconds. 
  Full-batch training loss = 0.011481, test loss = 0.153925
  Training set accuracy = 0.999600, Test set accuracy = 0.959200
 epoch 35/100. Took 0.57197 seconds. 
  Full-batch training loss = 0.010803, test loss = 0.154755
  Training set accuracy = 0.999700, Test set accuracy = 0.959100
 epoch 36/100. Took 0.5763 seconds. 
  Full-batch training loss = 0.010380, test loss = 0.155578
  Training set accuracy = 0.999700, Test set accuracy = 0.958800
 epoch 37/100. Took 0.57067 seconds. 
  Full-batch training loss = 0.009633, test loss = 0.154397
  Training set accuracy = 0.999700, Test set accuracy = 0.958800
 epoch 38/100. Took 0.57143 seconds. 
  Full-batch training loss = 0.009182, test loss = 0.154563
  Training set accuracy = 0.999800, Test set accuracy = 0.959400
 epoch 39/100. Took 0.57039 seconds. 
  Full-batch training loss = 0.008743, test loss = 0.155396
  Training set accuracy = 0.999800, Test set accuracy = 0.958900
 epoch 40/100. Took 0.57176 seconds. 
  Full-batch training loss = 0.008437, test loss = 0.156210
  Training set accuracy = 0.999800, Test set accuracy = 0.958900
 epoch 41/100. Took 0.57534 seconds. 
  Full-batch training loss = 0.008039, test loss = 0.155794
  Training set accuracy = 0.999900, Test set accuracy = 0.959200
 epoch 42/100. Took 0.57803 seconds. 
  Full-batch training loss = 0.007673, test loss = 0.156388
  Training set accuracy = 0.999900, Test set accuracy = 0.959000
 epoch 43/100. Took 0.57135 seconds. 
  Full-batch training loss = 0.007353, test loss = 0.156450
  Training set accuracy = 0.999900, Test set accuracy = 0.959100
 epoch 44/100. Took 0.56847 seconds. 
  Full-batch training loss = 0.007141, test loss = 0.156526
  Training set accuracy = 0.999900, Test set accuracy = 0.959000
 epoch 45/100. Took 0.56967 seconds. 
  Full-batch training loss = 0.006794, test loss = 0.157143
  Training set accuracy = 0.999900, Test set accuracy = 0.959100
 epoch 46/100. Took 0.57408 seconds. 
  Full-batch training loss = 0.006544, test loss = 0.157627
  Training set accuracy = 0.999900, Test set accuracy = 0.958900
 epoch 47/100. Took 0.56817 seconds. 
  Full-batch training loss = 0.006284, test loss = 0.157788
  Training set accuracy = 0.999900, Test set accuracy = 0.959200
 epoch 48/100. Took 0.57607 seconds. 
  Full-batch training loss = 0.006068, test loss = 0.158587
  Training set accuracy = 0.999900, Test set accuracy = 0.959000
 epoch 49/100. Took 0.56937 seconds. 
  Full-batch training loss = 0.005842, test loss = 0.158910
  Training set accuracy = 0.999900, Test set accuracy = 0.959300
 epoch 50/100. Took 0.57041 seconds. 
  Full-batch training loss = 0.005623, test loss = 0.158364
  Training set accuracy = 0.999900, Test set accuracy = 0.959200
 epoch 51/100. Took 0.5739 seconds. 
  Full-batch training loss = 0.005438, test loss = 0.158983
  Training set accuracy = 0.999900, Test set accuracy = 0.959100
 epoch 52/100. Took 0.57432 seconds. 
  Full-batch training loss = 0.005316, test loss = 0.159250
  Training set accuracy = 0.999900, Test set accuracy = 0.959200
 epoch 53/100. Took 0.57085 seconds. 
  Full-batch training loss = 0.005094, test loss = 0.159515
  Training set accuracy = 0.999900, Test set accuracy = 0.959400
 epoch 54/100. Took 0.5732 seconds. 
  Full-batch training loss = 0.004977, test loss = 0.160155
  Training set accuracy = 0.999900, Test set accuracy = 0.959200
 epoch 55/100. Took 0.57523 seconds. 
  Full-batch training loss = 0.004784, test loss = 0.160417
  Training set accuracy = 0.999900, Test set accuracy = 0.959900
 epoch 56/100. Took 0.57215 seconds. 
  Full-batch training loss = 0.004631, test loss = 0.160730
  Training set accuracy = 0.999900, Test set accuracy = 0.959500
 epoch 57/100. Took 0.57057 seconds. 
  Full-batch training loss = 0.004502, test loss = 0.160921
  Training set accuracy = 0.999900, Test set accuracy = 0.960000
 epoch 58/100. Took 0.5774 seconds. 
  Full-batch training loss = 0.004381, test loss = 0.161126
  Training set accuracy = 0.999900, Test set accuracy = 0.959500
 epoch 59/100. Took 0.57957 seconds. 
  Full-batch training loss = 0.004247, test loss = 0.161418
  Training set accuracy = 0.999900, Test set accuracy = 0.959300
 epoch 60/100. Took 0.57559 seconds. 
  Full-batch training loss = 0.004133, test loss = 0.161772
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 61/100. Took 0.57107 seconds. 
  Full-batch training loss = 0.004017, test loss = 0.161928
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 62/100. Took 0.57514 seconds. 
  Full-batch training loss = 0.003917, test loss = 0.161884
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 63/100. Took 0.57395 seconds. 
  Full-batch training loss = 0.003810, test loss = 0.162477
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 64/100. Took 0.61275 seconds. 
  Full-batch training loss = 0.003718, test loss = 0.162874
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 65/100. Took 0.57591 seconds. 
  Full-batch training loss = 0.003633, test loss = 0.163054
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 66/100. Took 0.57665 seconds. 
  Full-batch training loss = 0.003538, test loss = 0.163199
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 67/100. Took 0.57249 seconds. 
  Full-batch training loss = 0.003466, test loss = 0.163807
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 68/100. Took 0.57142 seconds. 
  Full-batch training loss = 0.003378, test loss = 0.163483
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 69/100. Took 0.57033 seconds. 
  Full-batch training loss = 0.003302, test loss = 0.163819
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 70/100. Took 0.57427 seconds. 
  Full-batch training loss = 0.003223, test loss = 0.164139
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 71/100. Took 0.57237 seconds. 
  Full-batch training loss = 0.003161, test loss = 0.164538
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 72/100. Took 0.57423 seconds. 
  Full-batch training loss = 0.003087, test loss = 0.164609
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 73/100. Took 0.57345 seconds. 
  Full-batch training loss = 0.003015, test loss = 0.164835
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 74/100. Took 0.57095 seconds. 
  Full-batch training loss = 0.002957, test loss = 0.165049
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 75/100. Took 0.5745 seconds. 
  Full-batch training loss = 0.002893, test loss = 0.165023
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 76/100. Took 0.568 seconds. 
  Full-batch training loss = 0.002834, test loss = 0.165569
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 77/100. Took 0.57078 seconds. 
  Full-batch training loss = 0.002784, test loss = 0.165516
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 78/100. Took 0.57105 seconds. 
  Full-batch training loss = 0.002724, test loss = 0.165724
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 79/100. Took 0.57443 seconds. 
  Full-batch training loss = 0.002674, test loss = 0.165951
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 80/100. Took 0.5758 seconds. 
  Full-batch training loss = 0.002625, test loss = 0.166195
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 81/100. Took 0.57233 seconds. 
  Full-batch training loss = 0.002582, test loss = 0.166381
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 82/100. Took 0.56992 seconds. 
  Full-batch training loss = 0.002528, test loss = 0.166659
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 83/100. Took 0.57059 seconds. 
  Full-batch training loss = 0.002481, test loss = 0.166961
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 84/100. Took 0.56852 seconds. 
  Full-batch training loss = 0.002440, test loss = 0.166951
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 85/100. Took 0.57345 seconds. 
  Full-batch training loss = 0.002395, test loss = 0.167065
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 86/100. Took 0.57908 seconds. 
  Full-batch training loss = 0.002354, test loss = 0.167654
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 87/100. Took 0.57266 seconds. 
  Full-batch training loss = 0.002319, test loss = 0.167468
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 88/100. Took 0.57297 seconds. 
  Full-batch training loss = 0.002276, test loss = 0.167846
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 89/100. Took 0.57678 seconds. 
  Full-batch training loss = 0.002236, test loss = 0.168028
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 90/100. Took 0.56802 seconds. 
  Full-batch training loss = 0.002205, test loss = 0.168048
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 91/100. Took 0.56947 seconds. 
  Full-batch training loss = 0.002170, test loss = 0.168178
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 92/100. Took 0.57318 seconds. 
  Full-batch training loss = 0.002134, test loss = 0.168339
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 93/100. Took 0.57154 seconds. 
  Full-batch training loss = 0.002096, test loss = 0.168691
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 94/100. Took 0.57226 seconds. 
  Full-batch training loss = 0.002065, test loss = 0.168875
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 95/100. Took 0.57209 seconds. 
  Full-batch training loss = 0.002033, test loss = 0.168882
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 96/100. Took 0.57268 seconds. 
  Full-batch training loss = 0.002001, test loss = 0.169251
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 97/100. Took 0.57256 seconds. 
  Full-batch training loss = 0.001973, test loss = 0.169085
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 98/100. Took 0.57941 seconds. 
  Full-batch training loss = 0.001943, test loss = 0.169461
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 99/100. Took 0.57317 seconds. 
  Full-batch training loss = 0.001914, test loss = 0.169626
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 100/100. Took 0.57008 seconds. 
  Full-batch training loss = 0.001887, test loss = 0.169650
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
Elapsed time is 135.192988 seconds.
End Training
