Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.05
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.54457 seconds. Average reconstruction error is: 32.95
 epoch 2/20. Took 0.5343 seconds. Average reconstruction error is: 18.8226
 epoch 3/20. Took 0.54413 seconds. Average reconstruction error is: 15.9767
 epoch 4/20. Took 0.54029 seconds. Average reconstruction error is: 14.4379
 epoch 5/20. Took 0.53902 seconds. Average reconstruction error is: 13.4667
 epoch 6/20. Took 0.54041 seconds. Average reconstruction error is: 12.7229
 epoch 7/20. Took 0.54164 seconds. Average reconstruction error is: 12.1458
 epoch 8/20. Took 0.5359 seconds. Average reconstruction error is: 11.6537
 epoch 9/20. Took 0.54019 seconds. Average reconstruction error is: 11.2345
 epoch 10/20. Took 0.53902 seconds. Average reconstruction error is: 10.8967
 epoch 11/20. Took 0.54276 seconds. Average reconstruction error is: 10.5968
 epoch 12/20. Took 0.53813 seconds. Average reconstruction error is: 10.3361
 epoch 13/20. Took 0.54131 seconds. Average reconstruction error is: 10.116
 epoch 14/20. Took 0.54078 seconds. Average reconstruction error is: 9.9308
 epoch 15/20. Took 0.5434 seconds. Average reconstruction error is: 9.7337
 epoch 16/20. Took 0.54454 seconds. Average reconstruction error is: 9.5652
 epoch 17/20. Took 0.54194 seconds. Average reconstruction error is: 9.4322
 epoch 18/20. Took 0.53948 seconds. Average reconstruction error is: 9.3088
 epoch 19/20. Took 0.54159 seconds. Average reconstruction error is: 9.1972
 epoch 20/20. Took 0.54435 seconds. Average reconstruction error is: 9.0847
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.19063 seconds. Average reconstruction error is: 19.2488
 epoch 2/20. Took 0.19369 seconds. Average reconstruction error is: 11.1474
 epoch 3/20. Took 0.19151 seconds. Average reconstruction error is: 9.3505
 epoch 4/20. Took 0.19184 seconds. Average reconstruction error is: 8.4838
 epoch 5/20. Took 0.19373 seconds. Average reconstruction error is: 7.9741
 epoch 6/20. Took 0.19258 seconds. Average reconstruction error is: 7.6186
 epoch 7/20. Took 0.19312 seconds. Average reconstruction error is: 7.3806
 epoch 8/20. Took 0.19221 seconds. Average reconstruction error is: 7.1568
 epoch 9/20. Took 0.19457 seconds. Average reconstruction error is: 7.0009
 epoch 10/20. Took 0.19256 seconds. Average reconstruction error is: 6.8579
 epoch 11/20. Took 0.19293 seconds. Average reconstruction error is: 6.7701
 epoch 12/20. Took 0.19431 seconds. Average reconstruction error is: 6.6694
 epoch 13/20. Took 0.194 seconds. Average reconstruction error is: 6.5822
 epoch 14/20. Took 0.1937 seconds. Average reconstruction error is: 6.4985
 epoch 15/20. Took 0.19316 seconds. Average reconstruction error is: 6.4271
 epoch 16/20. Took 0.1911 seconds. Average reconstruction error is: 6.3486
 epoch 17/20. Took 0.19202 seconds. Average reconstruction error is: 6.2905
 epoch 18/20. Took 0.19459 seconds. Average reconstruction error is: 6.2487
 epoch 19/20. Took 0.19421 seconds. Average reconstruction error is: 6.1792
 epoch 20/20. Took 0.19582 seconds. Average reconstruction error is: 6.1389
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.19232 seconds. Average reconstruction error is: 15.8044
 epoch 2/20. Took 0.19361 seconds. Average reconstruction error is: 8.3473
 epoch 3/20. Took 0.1948 seconds. Average reconstruction error is: 6.9488
 epoch 4/20. Took 0.19371 seconds. Average reconstruction error is: 6.3589
 epoch 5/20. Took 0.1944 seconds. Average reconstruction error is: 6.0279
 epoch 6/20. Took 0.1929 seconds. Average reconstruction error is: 5.7913
 epoch 7/20. Took 0.1933 seconds. Average reconstruction error is: 5.6335
 epoch 8/20. Took 0.1932 seconds. Average reconstruction error is: 5.5078
 epoch 9/20. Took 0.19401 seconds. Average reconstruction error is: 5.4334
 epoch 10/20. Took 0.19528 seconds. Average reconstruction error is: 5.3894
 epoch 11/20. Took 0.1945 seconds. Average reconstruction error is: 5.3054
 epoch 12/20. Took 0.1948 seconds. Average reconstruction error is: 5.276
 epoch 13/20. Took 0.19543 seconds. Average reconstruction error is: 5.2203
 epoch 14/20. Took 0.19532 seconds. Average reconstruction error is: 5.1654
 epoch 15/20. Took 0.19264 seconds. Average reconstruction error is: 5.1292
 epoch 16/20. Took 0.19074 seconds. Average reconstruction error is: 5.1156
 epoch 17/20. Took 0.19429 seconds. Average reconstruction error is: 5.1048
 epoch 18/20. Took 0.19424 seconds. Average reconstruction error is: 5.0514
 epoch 19/20. Took 0.19501 seconds. Average reconstruction error is: 5.0443
 epoch 20/20. Took 0.1938 seconds. Average reconstruction error is: 5.0204
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.54548 seconds. 
  Full-batch training loss = 0.258877, test loss = 0.270969
  Training set accuracy = 0.924800, Test set accuracy = 0.923700
 epoch 2/100. Took 0.55143 seconds. 
  Full-batch training loss = 0.212799, test loss = 0.239676
  Training set accuracy = 0.938500, Test set accuracy = 0.934000
 epoch 3/100. Took 0.55342 seconds. 
  Full-batch training loss = 0.167161, test loss = 0.213889
  Training set accuracy = 0.951300, Test set accuracy = 0.938900
 epoch 4/100. Took 0.54868 seconds. 
  Full-batch training loss = 0.139500, test loss = 0.190311
  Training set accuracy = 0.961500, Test set accuracy = 0.944400
 epoch 5/100. Took 0.55034 seconds. 
  Full-batch training loss = 0.128156, test loss = 0.187365
  Training set accuracy = 0.963400, Test set accuracy = 0.944400
 epoch 6/100. Took 0.55057 seconds. 
  Full-batch training loss = 0.109178, test loss = 0.181890
  Training set accuracy = 0.970300, Test set accuracy = 0.946700
 epoch 7/100. Took 0.54904 seconds. 
  Full-batch training loss = 0.089997, test loss = 0.171723
  Training set accuracy = 0.977100, Test set accuracy = 0.948300
 epoch 8/100. Took 0.55007 seconds. 
  Full-batch training loss = 0.080318, test loss = 0.168384
  Training set accuracy = 0.980400, Test set accuracy = 0.951600
 epoch 9/100. Took 0.54842 seconds. 
  Full-batch training loss = 0.071023, test loss = 0.166527
  Training set accuracy = 0.983500, Test set accuracy = 0.950500
 epoch 10/100. Took 0.54617 seconds. 
  Full-batch training loss = 0.060722, test loss = 0.158582
  Training set accuracy = 0.987200, Test set accuracy = 0.951100
 epoch 11/100. Took 0.54679 seconds. 
  Full-batch training loss = 0.052335, test loss = 0.154406
  Training set accuracy = 0.989500, Test set accuracy = 0.953900
 epoch 12/100. Took 0.54878 seconds. 
  Full-batch training loss = 0.049596, test loss = 0.164406
  Training set accuracy = 0.989700, Test set accuracy = 0.952300
 epoch 13/100. Took 0.5469 seconds. 
  Full-batch training loss = 0.040027, test loss = 0.151692
  Training set accuracy = 0.993100, Test set accuracy = 0.956200
 epoch 14/100. Took 0.55115 seconds. 
  Full-batch training loss = 0.037724, test loss = 0.150593
  Training set accuracy = 0.994400, Test set accuracy = 0.955600
 epoch 15/100. Took 0.54577 seconds. 
  Full-batch training loss = 0.032566, test loss = 0.145929
  Training set accuracy = 0.994200, Test set accuracy = 0.956700
 epoch 16/100. Took 0.54715 seconds. 
  Full-batch training loss = 0.029082, test loss = 0.147781
  Training set accuracy = 0.995500, Test set accuracy = 0.956400
 epoch 17/100. Took 0.54849 seconds. 
  Full-batch training loss = 0.024348, test loss = 0.145000
  Training set accuracy = 0.997100, Test set accuracy = 0.956900
 epoch 18/100. Took 0.54701 seconds. 
  Full-batch training loss = 0.023528, test loss = 0.144884
  Training set accuracy = 0.997800, Test set accuracy = 0.957300
 epoch 19/100. Took 0.54835 seconds. 
  Full-batch training loss = 0.020109, test loss = 0.144849
  Training set accuracy = 0.998400, Test set accuracy = 0.956700
 epoch 20/100. Took 0.54781 seconds. 
  Full-batch training loss = 0.019827, test loss = 0.150632
  Training set accuracy = 0.998100, Test set accuracy = 0.957500
 epoch 21/100. Took 0.54958 seconds. 
  Full-batch training loss = 0.016786, test loss = 0.146723
  Training set accuracy = 0.999000, Test set accuracy = 0.957500
 epoch 22/100. Took 0.5491 seconds. 
  Full-batch training loss = 0.015724, test loss = 0.146085
  Training set accuracy = 0.999100, Test set accuracy = 0.958600
 epoch 23/100. Took 0.5506 seconds. 
  Full-batch training loss = 0.014010, test loss = 0.145030
  Training set accuracy = 0.999300, Test set accuracy = 0.957500
 epoch 24/100. Took 0.54739 seconds. 
  Full-batch training loss = 0.012912, test loss = 0.147282
  Training set accuracy = 0.999300, Test set accuracy = 0.957000
 epoch 25/100. Took 0.55132 seconds. 
  Full-batch training loss = 0.012410, test loss = 0.146033
  Training set accuracy = 0.999400, Test set accuracy = 0.958400
 epoch 26/100. Took 0.54877 seconds. 
  Full-batch training loss = 0.010866, test loss = 0.147720
  Training set accuracy = 0.999900, Test set accuracy = 0.957800
 epoch 27/100. Took 0.54962 seconds. 
  Full-batch training loss = 0.010036, test loss = 0.148391
  Training set accuracy = 0.999900, Test set accuracy = 0.958300
 epoch 28/100. Took 0.55152 seconds. 
  Full-batch training loss = 0.009320, test loss = 0.147560
  Training set accuracy = 0.999900, Test set accuracy = 0.958600
 epoch 29/100. Took 0.54983 seconds. 
  Full-batch training loss = 0.008872, test loss = 0.149160
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 30/100. Took 0.55217 seconds. 
  Full-batch training loss = 0.008170, test loss = 0.147588
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 31/100. Took 0.54784 seconds. 
  Full-batch training loss = 0.007754, test loss = 0.147343
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 32/100. Took 0.55198 seconds. 
  Full-batch training loss = 0.007337, test loss = 0.149142
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 33/100. Took 0.54868 seconds. 
  Full-batch training loss = 0.006938, test loss = 0.149390
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 34/100. Took 0.54834 seconds. 
  Full-batch training loss = 0.006573, test loss = 0.149185
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 35/100. Took 0.54827 seconds. 
  Full-batch training loss = 0.006284, test loss = 0.150164
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 36/100. Took 0.54893 seconds. 
  Full-batch training loss = 0.005977, test loss = 0.150599
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 37/100. Took 0.54832 seconds. 
  Full-batch training loss = 0.005609, test loss = 0.150691
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 38/100. Took 0.55381 seconds. 
  Full-batch training loss = 0.005386, test loss = 0.151841
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 39/100. Took 0.54525 seconds. 
  Full-batch training loss = 0.005089, test loss = 0.151449
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 40/100. Took 0.54951 seconds. 
  Full-batch training loss = 0.004918, test loss = 0.151535
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 41/100. Took 0.55319 seconds. 
  Full-batch training loss = 0.004696, test loss = 0.152157
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 42/100. Took 0.54754 seconds. 
  Full-batch training loss = 0.004514, test loss = 0.151848
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 43/100. Took 0.54932 seconds. 
  Full-batch training loss = 0.004328, test loss = 0.152833
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 44/100. Took 0.54865 seconds. 
  Full-batch training loss = 0.004141, test loss = 0.152992
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 45/100. Took 0.54916 seconds. 
  Full-batch training loss = 0.004013, test loss = 0.153314
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 46/100. Took 0.54952 seconds. 
  Full-batch training loss = 0.003898, test loss = 0.153830
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 47/100. Took 0.54785 seconds. 
  Full-batch training loss = 0.003743, test loss = 0.153466
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 48/100. Took 0.54613 seconds. 
  Full-batch training loss = 0.003646, test loss = 0.154453
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 49/100. Took 0.54922 seconds. 
  Full-batch training loss = 0.003507, test loss = 0.155021
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 50/100. Took 0.54835 seconds. 
  Full-batch training loss = 0.003390, test loss = 0.154871
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 51/100. Took 0.54832 seconds. 
  Full-batch training loss = 0.003280, test loss = 0.155035
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 52/100. Took 0.54844 seconds. 
  Full-batch training loss = 0.003185, test loss = 0.155054
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 53/100. Took 0.5485 seconds. 
  Full-batch training loss = 0.003083, test loss = 0.155446
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 54/100. Took 0.54818 seconds. 
  Full-batch training loss = 0.003004, test loss = 0.155910
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 55/100. Took 0.54969 seconds. 
  Full-batch training loss = 0.002918, test loss = 0.156464
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 56/100. Took 0.55244 seconds. 
  Full-batch training loss = 0.002837, test loss = 0.156844
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 57/100. Took 0.54749 seconds. 
  Full-batch training loss = 0.002754, test loss = 0.156529
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 58/100. Took 0.54695 seconds. 
  Full-batch training loss = 0.002678, test loss = 0.156873
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 59/100. Took 0.54927 seconds. 
  Full-batch training loss = 0.002614, test loss = 0.157405
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 60/100. Took 0.54951 seconds. 
  Full-batch training loss = 0.002567, test loss = 0.157691
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 61/100. Took 0.54912 seconds. 
  Full-batch training loss = 0.002492, test loss = 0.157720
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 62/100. Took 0.54948 seconds. 
  Full-batch training loss = 0.002433, test loss = 0.157911
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 63/100. Took 0.55141 seconds. 
  Full-batch training loss = 0.002370, test loss = 0.158660
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 64/100. Took 0.54817 seconds. 
  Full-batch training loss = 0.002316, test loss = 0.158564
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 65/100. Took 0.55075 seconds. 
  Full-batch training loss = 0.002256, test loss = 0.158766
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 66/100. Took 0.55065 seconds. 
  Full-batch training loss = 0.002211, test loss = 0.158716
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 67/100. Took 0.55057 seconds. 
  Full-batch training loss = 0.002162, test loss = 0.159041
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 68/100. Took 0.55112 seconds. 
  Full-batch training loss = 0.002117, test loss = 0.159417
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 69/100. Took 0.61648 seconds. 
  Full-batch training loss = 0.002066, test loss = 0.159731
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 70/100. Took 0.54961 seconds. 
  Full-batch training loss = 0.002035, test loss = 0.159640
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 71/100. Took 0.54834 seconds. 
  Full-batch training loss = 0.001989, test loss = 0.159541
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 72/100. Took 0.55258 seconds. 
  Full-batch training loss = 0.001952, test loss = 0.160046
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 73/100. Took 0.54957 seconds. 
  Full-batch training loss = 0.001904, test loss = 0.160425
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 74/100. Took 0.5541 seconds. 
  Full-batch training loss = 0.001868, test loss = 0.160671
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 75/100. Took 0.55021 seconds. 
  Full-batch training loss = 0.001842, test loss = 0.161383
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 76/100. Took 0.54774 seconds. 
  Full-batch training loss = 0.001798, test loss = 0.161157
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 77/100. Took 0.54892 seconds. 
  Full-batch training loss = 0.001764, test loss = 0.161516
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 78/100. Took 0.54844 seconds. 
  Full-batch training loss = 0.001733, test loss = 0.161357
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 79/100. Took 0.54879 seconds. 
  Full-batch training loss = 0.001702, test loss = 0.161470
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 80/100. Took 0.57391 seconds. 
  Full-batch training loss = 0.001672, test loss = 0.161830
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 81/100. Took 0.55082 seconds. 
  Full-batch training loss = 0.001645, test loss = 0.162021
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 82/100. Took 0.54741 seconds. 
  Full-batch training loss = 0.001610, test loss = 0.162322
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 83/100. Took 0.54793 seconds. 
  Full-batch training loss = 0.001587, test loss = 0.162373
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 84/100. Took 0.54757 seconds. 
  Full-batch training loss = 0.001556, test loss = 0.162635
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 85/100. Took 0.549 seconds. 
  Full-batch training loss = 0.001534, test loss = 0.163370
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 86/100. Took 0.54801 seconds. 
  Full-batch training loss = 0.001509, test loss = 0.163551
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 87/100. Took 0.54723 seconds. 
  Full-batch training loss = 0.001480, test loss = 0.163105
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 88/100. Took 0.54949 seconds. 
  Full-batch training loss = 0.001463, test loss = 0.163420
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 89/100. Took 0.54938 seconds. 
  Full-batch training loss = 0.001435, test loss = 0.163372
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 90/100. Took 0.54981 seconds. 
  Full-batch training loss = 0.001415, test loss = 0.163698
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 91/100. Took 0.5506 seconds. 
  Full-batch training loss = 0.001391, test loss = 0.164070
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 92/100. Took 0.54827 seconds. 
  Full-batch training loss = 0.001372, test loss = 0.164083
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 93/100. Took 0.547 seconds. 
  Full-batch training loss = 0.001350, test loss = 0.164136
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 94/100. Took 0.54701 seconds. 
  Full-batch training loss = 0.001329, test loss = 0.164098
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 95/100. Took 0.54827 seconds. 
  Full-batch training loss = 0.001312, test loss = 0.164909
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 96/100. Took 0.54682 seconds. 
  Full-batch training loss = 0.001290, test loss = 0.164702
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 97/100. Took 0.54784 seconds. 
  Full-batch training loss = 0.001272, test loss = 0.164872
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 98/100. Took 0.55096 seconds. 
  Full-batch training loss = 0.001256, test loss = 0.165277
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 99/100. Took 0.54925 seconds. 
  Full-batch training loss = 0.001237, test loss = 0.165368
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 100/100. Took 0.546 seconds. 
  Full-batch training loss = 0.001219, test loss = 0.165335
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
Elapsed time is 130.893749 seconds.
End Training
