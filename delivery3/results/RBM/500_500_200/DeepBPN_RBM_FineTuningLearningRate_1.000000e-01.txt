Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.10
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.54688 seconds. Average reconstruction error is: 29.6803
 epoch 2/20. Took 0.54817 seconds. Average reconstruction error is: 17.9338
 epoch 3/20. Took 0.54772 seconds. Average reconstruction error is: 15.4982
 epoch 4/20. Took 0.545 seconds. Average reconstruction error is: 14.0634
 epoch 5/20. Took 0.54284 seconds. Average reconstruction error is: 13.0269
 epoch 6/20. Took 0.5458 seconds. Average reconstruction error is: 12.2399
 epoch 7/20. Took 0.54727 seconds. Average reconstruction error is: 11.6645
 epoch 8/20. Took 0.54603 seconds. Average reconstruction error is: 11.2363
 epoch 9/20. Took 0.54656 seconds. Average reconstruction error is: 10.9288
 epoch 10/20. Took 0.5429 seconds. Average reconstruction error is: 10.6664
 epoch 11/20. Took 0.5489 seconds. Average reconstruction error is: 10.4193
 epoch 12/20. Took 0.54481 seconds. Average reconstruction error is: 10.2421
 epoch 13/20. Took 0.5451 seconds. Average reconstruction error is: 10.0793
 epoch 14/20. Took 0.54813 seconds. Average reconstruction error is: 9.9476
 epoch 15/20. Took 0.54668 seconds. Average reconstruction error is: 9.7915
 epoch 16/20. Took 0.54733 seconds. Average reconstruction error is: 9.6714
 epoch 17/20. Took 0.54693 seconds. Average reconstruction error is: 9.5791
 epoch 18/20. Took 0.54795 seconds. Average reconstruction error is: 9.4811
 epoch 19/20. Took 0.55184 seconds. Average reconstruction error is: 9.4022
 epoch 20/20. Took 0.54781 seconds. Average reconstruction error is: 9.3365
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.18988 seconds. Average reconstruction error is: 14.5478
 epoch 2/20. Took 0.19088 seconds. Average reconstruction error is: 8.7876
 epoch 3/20. Took 0.19112 seconds. Average reconstruction error is: 7.7555
 epoch 4/20. Took 0.19161 seconds. Average reconstruction error is: 7.1909
 epoch 5/20. Took 0.19175 seconds. Average reconstruction error is: 6.9058
 epoch 6/20. Took 0.19195 seconds. Average reconstruction error is: 6.6694
 epoch 7/20. Took 0.19307 seconds. Average reconstruction error is: 6.4927
 epoch 8/20. Took 0.19313 seconds. Average reconstruction error is: 6.3434
 epoch 9/20. Took 0.19295 seconds. Average reconstruction error is: 6.2227
 epoch 10/20. Took 0.19224 seconds. Average reconstruction error is: 6.0987
 epoch 11/20. Took 0.19318 seconds. Average reconstruction error is: 6.0191
 epoch 12/20. Took 0.19062 seconds. Average reconstruction error is: 5.9236
 epoch 13/20. Took 0.19224 seconds. Average reconstruction error is: 5.8588
 epoch 14/20. Took 0.19111 seconds. Average reconstruction error is: 5.7988
 epoch 15/20. Took 0.18939 seconds. Average reconstruction error is: 5.6977
 epoch 16/20. Took 0.19231 seconds. Average reconstruction error is: 5.6534
 epoch 17/20. Took 0.19287 seconds. Average reconstruction error is: 5.5763
 epoch 18/20. Took 0.19153 seconds. Average reconstruction error is: 5.521
 epoch 19/20. Took 0.19223 seconds. Average reconstruction error is: 5.4769
 epoch 20/20. Took 0.19292 seconds. Average reconstruction error is: 5.4012
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.18969 seconds. Average reconstruction error is: 11.8283
 epoch 2/20. Took 0.19022 seconds. Average reconstruction error is: 6.8451
 epoch 3/20. Took 0.19051 seconds. Average reconstruction error is: 6.1249
 epoch 4/20. Took 0.19 seconds. Average reconstruction error is: 5.7588
 epoch 5/20. Took 0.19101 seconds. Average reconstruction error is: 5.5074
 epoch 6/20. Took 0.18968 seconds. Average reconstruction error is: 5.3738
 epoch 7/20. Took 0.19072 seconds. Average reconstruction error is: 5.2486
 epoch 8/20. Took 0.19067 seconds. Average reconstruction error is: 5.1605
 epoch 9/20. Took 0.18979 seconds. Average reconstruction error is: 5.0903
 epoch 10/20. Took 0.19043 seconds. Average reconstruction error is: 5.0319
 epoch 11/20. Took 0.19198 seconds. Average reconstruction error is: 4.9599
 epoch 12/20. Took 0.18979 seconds. Average reconstruction error is: 4.9043
 epoch 13/20. Took 0.18985 seconds. Average reconstruction error is: 4.8406
 epoch 14/20. Took 0.19287 seconds. Average reconstruction error is: 4.7864
 epoch 15/20. Took 0.19087 seconds. Average reconstruction error is: 4.7485
 epoch 16/20. Took 0.19134 seconds. Average reconstruction error is: 4.7104
 epoch 17/20. Took 0.19077 seconds. Average reconstruction error is: 4.6664
 epoch 18/20. Took 0.19192 seconds. Average reconstruction error is: 4.6387
 epoch 19/20. Took 0.19062 seconds. Average reconstruction error is: 4.5978
 epoch 20/20. Took 0.1907 seconds. Average reconstruction error is: 4.5845
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.54138 seconds. 
  Full-batch training loss = 0.347352, test loss = 0.338789
  Training set accuracy = 0.910200, Test set accuracy = 0.913700
 epoch 2/100. Took 0.54752 seconds. 
  Full-batch training loss = 0.279620, test loss = 0.279765
  Training set accuracy = 0.923500, Test set accuracy = 0.925700
 epoch 3/100. Took 0.54881 seconds. 
  Full-batch training loss = 0.244263, test loss = 0.253272
  Training set accuracy = 0.931600, Test set accuracy = 0.931800
 epoch 4/100. Took 0.54764 seconds. 
  Full-batch training loss = 0.214295, test loss = 0.233614
  Training set accuracy = 0.938600, Test set accuracy = 0.933100
 epoch 5/100. Took 0.54943 seconds. 
  Full-batch training loss = 0.199700, test loss = 0.224602
  Training set accuracy = 0.941900, Test set accuracy = 0.936800
 epoch 6/100. Took 0.55064 seconds. 
  Full-batch training loss = 0.180168, test loss = 0.213589
  Training set accuracy = 0.949700, Test set accuracy = 0.939200
 epoch 7/100. Took 0.54946 seconds. 
  Full-batch training loss = 0.167111, test loss = 0.204834
  Training set accuracy = 0.952900, Test set accuracy = 0.940900
 epoch 8/100. Took 0.5481 seconds. 
  Full-batch training loss = 0.155649, test loss = 0.198707
  Training set accuracy = 0.955900, Test set accuracy = 0.943000
 epoch 9/100. Took 0.54853 seconds. 
  Full-batch training loss = 0.147565, test loss = 0.197223
  Training set accuracy = 0.958800, Test set accuracy = 0.943600
 epoch 10/100. Took 0.54467 seconds. 
  Full-batch training loss = 0.136070, test loss = 0.192874
  Training set accuracy = 0.963100, Test set accuracy = 0.942900
 epoch 11/100. Took 0.54949 seconds. 
  Full-batch training loss = 0.126614, test loss = 0.186519
  Training set accuracy = 0.964300, Test set accuracy = 0.945600
 epoch 12/100. Took 0.55241 seconds. 
  Full-batch training loss = 0.117276, test loss = 0.180617
  Training set accuracy = 0.967900, Test set accuracy = 0.946700
 epoch 13/100. Took 0.54827 seconds. 
  Full-batch training loss = 0.110739, test loss = 0.177929
  Training set accuracy = 0.969500, Test set accuracy = 0.947100
 epoch 14/100. Took 0.54858 seconds. 
  Full-batch training loss = 0.104760, test loss = 0.177996
  Training set accuracy = 0.973000, Test set accuracy = 0.947000
 epoch 15/100. Took 0.5474 seconds. 
  Full-batch training loss = 0.098679, test loss = 0.173972
  Training set accuracy = 0.975300, Test set accuracy = 0.948200
 epoch 16/100. Took 0.54788 seconds. 
  Full-batch training loss = 0.092981, test loss = 0.172016
  Training set accuracy = 0.977000, Test set accuracy = 0.948200
 epoch 17/100. Took 0.55003 seconds. 
  Full-batch training loss = 0.086526, test loss = 0.169274
  Training set accuracy = 0.979000, Test set accuracy = 0.950200
 epoch 18/100. Took 0.54781 seconds. 
  Full-batch training loss = 0.082586, test loss = 0.168859
  Training set accuracy = 0.980800, Test set accuracy = 0.949700
 epoch 19/100. Took 0.54988 seconds. 
  Full-batch training loss = 0.077528, test loss = 0.166835
  Training set accuracy = 0.982500, Test set accuracy = 0.950800
 epoch 20/100. Took 0.54904 seconds. 
  Full-batch training loss = 0.073768, test loss = 0.167903
  Training set accuracy = 0.983600, Test set accuracy = 0.948700
 epoch 21/100. Took 0.54676 seconds. 
  Full-batch training loss = 0.070046, test loss = 0.166119
  Training set accuracy = 0.984300, Test set accuracy = 0.951600
 epoch 22/100. Took 0.54821 seconds. 
  Full-batch training loss = 0.065524, test loss = 0.163701
  Training set accuracy = 0.986400, Test set accuracy = 0.951100
 epoch 23/100. Took 0.54577 seconds. 
  Full-batch training loss = 0.062497, test loss = 0.161659
  Training set accuracy = 0.987200, Test set accuracy = 0.953000
 epoch 24/100. Took 0.54949 seconds. 
  Full-batch training loss = 0.059131, test loss = 0.162532
  Training set accuracy = 0.988400, Test set accuracy = 0.951700
 epoch 25/100. Took 0.54629 seconds. 
  Full-batch training loss = 0.055903, test loss = 0.161002
  Training set accuracy = 0.989600, Test set accuracy = 0.952100
 epoch 26/100. Took 0.54749 seconds. 
  Full-batch training loss = 0.053592, test loss = 0.159717
  Training set accuracy = 0.990200, Test set accuracy = 0.952300
 epoch 27/100. Took 0.5484 seconds. 
  Full-batch training loss = 0.050935, test loss = 0.160090
  Training set accuracy = 0.991600, Test set accuracy = 0.952300
 epoch 28/100. Took 0.5487 seconds. 
  Full-batch training loss = 0.047758, test loss = 0.159256
  Training set accuracy = 0.991900, Test set accuracy = 0.952900
 epoch 29/100. Took 0.54458 seconds. 
  Full-batch training loss = 0.045608, test loss = 0.158321
  Training set accuracy = 0.992600, Test set accuracy = 0.953500
 epoch 30/100. Took 0.54699 seconds. 
  Full-batch training loss = 0.043111, test loss = 0.158734
  Training set accuracy = 0.993400, Test set accuracy = 0.952400
 epoch 31/100. Took 0.54603 seconds. 
  Full-batch training loss = 0.041264, test loss = 0.160085
  Training set accuracy = 0.993500, Test set accuracy = 0.952100
 epoch 32/100. Took 0.54782 seconds. 
  Full-batch training loss = 0.039068, test loss = 0.158034
  Training set accuracy = 0.994000, Test set accuracy = 0.953600
 epoch 33/100. Took 0.54552 seconds. 
  Full-batch training loss = 0.037134, test loss = 0.157016
  Training set accuracy = 0.994400, Test set accuracy = 0.953400
 epoch 34/100. Took 0.54811 seconds. 
  Full-batch training loss = 0.035874, test loss = 0.159779
  Training set accuracy = 0.994500, Test set accuracy = 0.952100
 epoch 35/100. Took 0.54853 seconds. 
  Full-batch training loss = 0.034619, test loss = 0.159148
  Training set accuracy = 0.995000, Test set accuracy = 0.953500
 epoch 36/100. Took 0.54948 seconds. 
  Full-batch training loss = 0.032469, test loss = 0.158440
  Training set accuracy = 0.995600, Test set accuracy = 0.953500
 epoch 37/100. Took 0.54633 seconds. 
  Full-batch training loss = 0.030930, test loss = 0.158440
  Training set accuracy = 0.995900, Test set accuracy = 0.953100
 epoch 38/100. Took 0.54727 seconds. 
  Full-batch training loss = 0.029696, test loss = 0.157658
  Training set accuracy = 0.996000, Test set accuracy = 0.953900
 epoch 39/100. Took 0.55039 seconds. 
  Full-batch training loss = 0.028184, test loss = 0.157820
  Training set accuracy = 0.996900, Test set accuracy = 0.953400
 epoch 40/100. Took 0.54848 seconds. 
  Full-batch training loss = 0.027238, test loss = 0.158101
  Training set accuracy = 0.996700, Test set accuracy = 0.953000
 epoch 41/100. Took 0.54759 seconds. 
  Full-batch training loss = 0.026012, test loss = 0.157800
  Training set accuracy = 0.997300, Test set accuracy = 0.954000
 epoch 42/100. Took 0.54889 seconds. 
  Full-batch training loss = 0.024719, test loss = 0.156515
  Training set accuracy = 0.997500, Test set accuracy = 0.954200
 epoch 43/100. Took 0.54808 seconds. 
  Full-batch training loss = 0.023769, test loss = 0.158059
  Training set accuracy = 0.997500, Test set accuracy = 0.953300
 epoch 44/100. Took 0.5467 seconds. 
  Full-batch training loss = 0.022827, test loss = 0.157353
  Training set accuracy = 0.997900, Test set accuracy = 0.953600
 epoch 45/100. Took 0.54789 seconds. 
  Full-batch training loss = 0.021825, test loss = 0.157817
  Training set accuracy = 0.998000, Test set accuracy = 0.954000
 epoch 46/100. Took 0.54656 seconds. 
  Full-batch training loss = 0.020976, test loss = 0.157740
  Training set accuracy = 0.998400, Test set accuracy = 0.954100
 epoch 47/100. Took 0.54983 seconds. 
  Full-batch training loss = 0.020228, test loss = 0.158200
  Training set accuracy = 0.998400, Test set accuracy = 0.954200
 epoch 48/100. Took 0.54519 seconds. 
  Full-batch training loss = 0.019683, test loss = 0.158292
  Training set accuracy = 0.998400, Test set accuracy = 0.954800
 epoch 49/100. Took 0.54713 seconds. 
  Full-batch training loss = 0.018591, test loss = 0.157903
  Training set accuracy = 0.998700, Test set accuracy = 0.954700
 epoch 50/100. Took 0.54668 seconds. 
  Full-batch training loss = 0.017977, test loss = 0.157964
  Training set accuracy = 0.998700, Test set accuracy = 0.954800
 epoch 51/100. Took 0.54825 seconds. 
  Full-batch training loss = 0.017324, test loss = 0.158052
  Training set accuracy = 0.999300, Test set accuracy = 0.954800
 epoch 52/100. Took 0.55047 seconds. 
  Full-batch training loss = 0.016625, test loss = 0.158327
  Training set accuracy = 0.999300, Test set accuracy = 0.954700
 epoch 53/100. Took 0.54586 seconds. 
  Full-batch training loss = 0.016058, test loss = 0.158156
  Training set accuracy = 0.999000, Test set accuracy = 0.955000
 epoch 54/100. Took 0.55118 seconds. 
  Full-batch training loss = 0.015581, test loss = 0.157904
  Training set accuracy = 0.999300, Test set accuracy = 0.954700
 epoch 55/100. Took 0.54549 seconds. 
  Full-batch training loss = 0.014972, test loss = 0.157613
  Training set accuracy = 0.999300, Test set accuracy = 0.955500
 epoch 56/100. Took 0.54927 seconds. 
  Full-batch training loss = 0.014517, test loss = 0.157723
  Training set accuracy = 0.999300, Test set accuracy = 0.955500
 epoch 57/100. Took 0.54769 seconds. 
  Full-batch training loss = 0.013957, test loss = 0.158606
  Training set accuracy = 0.999400, Test set accuracy = 0.955200
 epoch 58/100. Took 0.54642 seconds. 
  Full-batch training loss = 0.013550, test loss = 0.158970
  Training set accuracy = 0.999400, Test set accuracy = 0.955200
 epoch 59/100. Took 0.54909 seconds. 
  Full-batch training loss = 0.013162, test loss = 0.159867
  Training set accuracy = 0.999500, Test set accuracy = 0.954700
 epoch 60/100. Took 0.54684 seconds. 
  Full-batch training loss = 0.012694, test loss = 0.158877
  Training set accuracy = 0.999500, Test set accuracy = 0.955700
 epoch 61/100. Took 0.54758 seconds. 
  Full-batch training loss = 0.012264, test loss = 0.159006
  Training set accuracy = 0.999700, Test set accuracy = 0.955900
 epoch 62/100. Took 0.54786 seconds. 
  Full-batch training loss = 0.011898, test loss = 0.159178
  Training set accuracy = 0.999700, Test set accuracy = 0.955800
 epoch 63/100. Took 0.54698 seconds. 
  Full-batch training loss = 0.011565, test loss = 0.159423
  Training set accuracy = 0.999800, Test set accuracy = 0.955800
 epoch 64/100. Took 0.54757 seconds. 
  Full-batch training loss = 0.011257, test loss = 0.160006
  Training set accuracy = 0.999800, Test set accuracy = 0.955800
 epoch 65/100. Took 0.54521 seconds. 
  Full-batch training loss = 0.010883, test loss = 0.159990
  Training set accuracy = 0.999800, Test set accuracy = 0.956100
 epoch 66/100. Took 0.5474 seconds. 
  Full-batch training loss = 0.010647, test loss = 0.161004
  Training set accuracy = 0.999800, Test set accuracy = 0.955700
 epoch 67/100. Took 0.55016 seconds. 
  Full-batch training loss = 0.010345, test loss = 0.160770
  Training set accuracy = 0.999800, Test set accuracy = 0.956200
 epoch 68/100. Took 0.5491 seconds. 
  Full-batch training loss = 0.009979, test loss = 0.160965
  Training set accuracy = 0.999800, Test set accuracy = 0.955700
 epoch 69/100. Took 0.54715 seconds. 
  Full-batch training loss = 0.009788, test loss = 0.160112
  Training set accuracy = 0.999800, Test set accuracy = 0.956500
 epoch 70/100. Took 0.54498 seconds. 
  Full-batch training loss = 0.009455, test loss = 0.160849
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 71/100. Took 0.54992 seconds. 
  Full-batch training loss = 0.009230, test loss = 0.161405
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 72/100. Took 0.548 seconds. 
  Full-batch training loss = 0.008968, test loss = 0.161432
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 73/100. Took 0.54795 seconds. 
  Full-batch training loss = 0.008746, test loss = 0.162051
  Training set accuracy = 0.999900, Test set accuracy = 0.955800
 epoch 74/100. Took 0.54614 seconds. 
  Full-batch training loss = 0.008523, test loss = 0.161503
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 75/100. Took 0.54756 seconds. 
  Full-batch training loss = 0.008365, test loss = 0.162665
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 76/100. Took 0.55066 seconds. 
  Full-batch training loss = 0.008102, test loss = 0.162411
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 77/100. Took 0.5472 seconds. 
  Full-batch training loss = 0.007922, test loss = 0.163014
  Training set accuracy = 0.999900, Test set accuracy = 0.955600
 epoch 78/100. Took 0.54566 seconds. 
  Full-batch training loss = 0.007750, test loss = 0.162814
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 79/100. Took 0.54468 seconds. 
  Full-batch training loss = 0.007559, test loss = 0.162901
  Training set accuracy = 0.999900, Test set accuracy = 0.956000
 epoch 80/100. Took 0.54427 seconds. 
  Full-batch training loss = 0.007390, test loss = 0.163161
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 81/100. Took 0.54686 seconds. 
  Full-batch training loss = 0.007225, test loss = 0.163354
  Training set accuracy = 0.999900, Test set accuracy = 0.956000
 epoch 82/100. Took 0.54855 seconds. 
  Full-batch training loss = 0.007067, test loss = 0.163617
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 83/100. Took 0.55058 seconds. 
  Full-batch training loss = 0.006915, test loss = 0.163966
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 84/100. Took 0.54492 seconds. 
  Full-batch training loss = 0.006767, test loss = 0.164326
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 85/100. Took 0.54849 seconds. 
  Full-batch training loss = 0.006632, test loss = 0.164220
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 86/100. Took 0.54608 seconds. 
  Full-batch training loss = 0.006484, test loss = 0.164380
  Training set accuracy = 0.999900, Test set accuracy = 0.956400
 epoch 87/100. Took 0.54864 seconds. 
  Full-batch training loss = 0.006360, test loss = 0.164787
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 88/100. Took 0.5452 seconds. 
  Full-batch training loss = 0.006230, test loss = 0.164665
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 89/100. Took 0.54724 seconds. 
  Full-batch training loss = 0.006110, test loss = 0.164875
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 90/100. Took 0.54817 seconds. 
  Full-batch training loss = 0.005992, test loss = 0.164900
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 91/100. Took 0.54992 seconds. 
  Full-batch training loss = 0.005895, test loss = 0.165244
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 92/100. Took 0.5466 seconds. 
  Full-batch training loss = 0.005770, test loss = 0.165115
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 93/100. Took 0.54848 seconds. 
  Full-batch training loss = 0.005661, test loss = 0.165934
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 94/100. Took 0.54858 seconds. 
  Full-batch training loss = 0.005559, test loss = 0.165718
  Training set accuracy = 1.000000, Test set accuracy = 0.956800
 epoch 95/100. Took 0.54757 seconds. 
  Full-batch training loss = 0.005455, test loss = 0.166062
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 96/100. Took 0.54595 seconds. 
  Full-batch training loss = 0.005372, test loss = 0.165905
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 97/100. Took 0.54808 seconds. 
  Full-batch training loss = 0.005270, test loss = 0.166396
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 98/100. Took 0.54855 seconds. 
  Full-batch training loss = 0.005187, test loss = 0.166585
  Training set accuracy = 1.000000, Test set accuracy = 0.957100
 epoch 99/100. Took 0.54916 seconds. 
  Full-batch training loss = 0.005090, test loss = 0.166680
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 100/100. Took 0.54774 seconds. 
  Full-batch training loss = 0.005017, test loss = 0.167276
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
Elapsed time is 130.372120 seconds.
End Training
