Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.56073 seconds. Average reconstruction error is: 29.9816
 epoch 2/20. Took 0.55632 seconds. Average reconstruction error is: 18.3756
 epoch 3/20. Took 0.56352 seconds. Average reconstruction error is: 15.9616
 epoch 4/20. Took 0.56008 seconds. Average reconstruction error is: 14.5963
 epoch 5/20. Took 0.55445 seconds. Average reconstruction error is: 13.5691
 epoch 6/20. Took 0.5632 seconds. Average reconstruction error is: 12.8207
 epoch 7/20. Took 0.55582 seconds. Average reconstruction error is: 12.2084
 epoch 8/20. Took 0.56425 seconds. Average reconstruction error is: 11.7079
 epoch 9/20. Took 0.56135 seconds. Average reconstruction error is: 11.3745
 epoch 10/20. Took 0.54482 seconds. Average reconstruction error is: 11.0583
 epoch 11/20. Took 0.54255 seconds. Average reconstruction error is: 10.8217
 epoch 12/20. Took 0.55944 seconds. Average reconstruction error is: 10.5953
 epoch 13/20. Took 0.55028 seconds. Average reconstruction error is: 10.4498
 epoch 14/20. Took 0.54711 seconds. Average reconstruction error is: 10.2877
 epoch 15/20. Took 0.54275 seconds. Average reconstruction error is: 10.1724
 epoch 16/20. Took 0.54511 seconds. Average reconstruction error is: 10.0705
 epoch 17/20. Took 0.54324 seconds. Average reconstruction error is: 9.954
 epoch 18/20. Took 0.54796 seconds. Average reconstruction error is: 9.8424
 epoch 19/20. Took 0.54669 seconds. Average reconstruction error is: 9.7236
 epoch 20/20. Took 0.55442 seconds. Average reconstruction error is: 9.6492
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.19969 seconds. Average reconstruction error is: 13.7728
 epoch 2/20. Took 0.20067 seconds. Average reconstruction error is: 8.241
 epoch 3/20. Took 0.20368 seconds. Average reconstruction error is: 7.2068
 epoch 4/20. Took 0.20443 seconds. Average reconstruction error is: 6.6838
 epoch 5/20. Took 0.20182 seconds. Average reconstruction error is: 6.3648
 epoch 6/20. Took 0.20439 seconds. Average reconstruction error is: 6.1223
 epoch 7/20. Took 0.20201 seconds. Average reconstruction error is: 5.9589
 epoch 8/20. Took 0.20447 seconds. Average reconstruction error is: 5.7986
 epoch 9/20. Took 0.20321 seconds. Average reconstruction error is: 5.6923
 epoch 10/20. Took 0.20567 seconds. Average reconstruction error is: 5.5871
 epoch 11/20. Took 0.20434 seconds. Average reconstruction error is: 5.5123
 epoch 12/20. Took 0.20412 seconds. Average reconstruction error is: 5.4173
 epoch 13/20. Took 0.20424 seconds. Average reconstruction error is: 5.3369
 epoch 14/20. Took 0.20384 seconds. Average reconstruction error is: 5.2854
 epoch 15/20. Took 0.2041 seconds. Average reconstruction error is: 5.2121
 epoch 16/20. Took 0.20481 seconds. Average reconstruction error is: 5.174
 epoch 17/20. Took 0.20448 seconds. Average reconstruction error is: 5.1097
 epoch 18/20. Took 0.20243 seconds. Average reconstruction error is: 5.0982
 epoch 19/20. Took 0.20449 seconds. Average reconstruction error is: 5.0351
 epoch 20/20. Took 0.20389 seconds. Average reconstruction error is: 5.0009
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.19085 seconds. Average reconstruction error is: 12.2561
 epoch 2/20. Took 0.19162 seconds. Average reconstruction error is: 7.2446
 epoch 3/20. Took 0.19156 seconds. Average reconstruction error is: 6.4076
 epoch 4/20. Took 0.19285 seconds. Average reconstruction error is: 6.0358
 epoch 5/20. Took 0.19287 seconds. Average reconstruction error is: 5.8018
 epoch 6/20. Took 0.1919 seconds. Average reconstruction error is: 5.6315
 epoch 7/20. Took 0.19383 seconds. Average reconstruction error is: 5.505
 epoch 8/20. Took 0.19277 seconds. Average reconstruction error is: 5.3919
 epoch 9/20. Took 0.19309 seconds. Average reconstruction error is: 5.3033
 epoch 10/20. Took 0.19297 seconds. Average reconstruction error is: 5.2443
 epoch 11/20. Took 0.1935 seconds. Average reconstruction error is: 5.1856
 epoch 12/20. Took 0.19242 seconds. Average reconstruction error is: 5.0797
 epoch 13/20. Took 0.19279 seconds. Average reconstruction error is: 5.0507
 epoch 14/20. Took 0.19274 seconds. Average reconstruction error is: 5.0118
 epoch 15/20. Took 0.19232 seconds. Average reconstruction error is: 4.957
 epoch 16/20. Took 0.19302 seconds. Average reconstruction error is: 4.907
 epoch 17/20. Took 0.19332 seconds. Average reconstruction error is: 4.8703
 epoch 18/20. Took 0.19289 seconds. Average reconstruction error is: 4.8314
 epoch 19/20. Took 0.19326 seconds. Average reconstruction error is: 4.8104
 epoch 20/20. Took 0.19309 seconds. Average reconstruction error is: 4.7627
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.55006 seconds. 
  Full-batch training loss = 0.286647, test loss = 0.283307
  Training set accuracy = 0.916600, Test set accuracy = 0.920100
 epoch 2/100. Took 0.56029 seconds. 
  Full-batch training loss = 0.226950, test loss = 0.237989
  Training set accuracy = 0.936600, Test set accuracy = 0.933100
 epoch 3/100. Took 0.56035 seconds. 
  Full-batch training loss = 0.201662, test loss = 0.224219
  Training set accuracy = 0.943800, Test set accuracy = 0.936200
 epoch 4/100. Took 0.56435 seconds. 
  Full-batch training loss = 0.166735, test loss = 0.205744
  Training set accuracy = 0.956000, Test set accuracy = 0.941600
 epoch 5/100. Took 0.55843 seconds. 
  Full-batch training loss = 0.144981, test loss = 0.191031
  Training set accuracy = 0.959700, Test set accuracy = 0.943500
 epoch 6/100. Took 0.55897 seconds. 
  Full-batch training loss = 0.128749, test loss = 0.182146
  Training set accuracy = 0.966300, Test set accuracy = 0.947300
 epoch 7/100. Took 0.5568 seconds. 
  Full-batch training loss = 0.116880, test loss = 0.179674
  Training set accuracy = 0.968200, Test set accuracy = 0.947100
 epoch 8/100. Took 0.55949 seconds. 
  Full-batch training loss = 0.100821, test loss = 0.172669
  Training set accuracy = 0.975400, Test set accuracy = 0.950500
 epoch 9/100. Took 0.55248 seconds. 
  Full-batch training loss = 0.093832, test loss = 0.169547
  Training set accuracy = 0.977500, Test set accuracy = 0.950600
 epoch 10/100. Took 0.55402 seconds. 
  Full-batch training loss = 0.082191, test loss = 0.165800
  Training set accuracy = 0.980900, Test set accuracy = 0.951300
 epoch 11/100. Took 0.5594 seconds. 
  Full-batch training loss = 0.073660, test loss = 0.161680
  Training set accuracy = 0.983700, Test set accuracy = 0.952400
 epoch 12/100. Took 0.55693 seconds. 
  Full-batch training loss = 0.068793, test loss = 0.160000
  Training set accuracy = 0.985400, Test set accuracy = 0.953700
 epoch 13/100. Took 0.5571 seconds. 
  Full-batch training loss = 0.063105, test loss = 0.160437
  Training set accuracy = 0.987700, Test set accuracy = 0.953600
 epoch 14/100. Took 0.552 seconds. 
  Full-batch training loss = 0.055887, test loss = 0.158183
  Training set accuracy = 0.988600, Test set accuracy = 0.952500
 epoch 15/100. Took 0.55955 seconds. 
  Full-batch training loss = 0.054583, test loss = 0.163123
  Training set accuracy = 0.989200, Test set accuracy = 0.952700
 epoch 16/100. Took 0.55414 seconds. 
  Full-batch training loss = 0.047155, test loss = 0.157590
  Training set accuracy = 0.991900, Test set accuracy = 0.953600
 epoch 17/100. Took 0.55473 seconds. 
  Full-batch training loss = 0.041595, test loss = 0.154475
  Training set accuracy = 0.992900, Test set accuracy = 0.956400
 epoch 18/100. Took 0.55386 seconds. 
  Full-batch training loss = 0.038270, test loss = 0.153715
  Training set accuracy = 0.993700, Test set accuracy = 0.956300
 epoch 19/100. Took 0.55725 seconds. 
  Full-batch training loss = 0.036123, test loss = 0.156365
  Training set accuracy = 0.994300, Test set accuracy = 0.956000
 epoch 20/100. Took 0.5534 seconds. 
  Full-batch training loss = 0.032727, test loss = 0.153470
  Training set accuracy = 0.995000, Test set accuracy = 0.955500
 epoch 21/100. Took 0.55205 seconds. 
  Full-batch training loss = 0.029161, test loss = 0.151571
  Training set accuracy = 0.995700, Test set accuracy = 0.957100
 epoch 22/100. Took 0.56036 seconds. 
  Full-batch training loss = 0.026532, test loss = 0.152296
  Training set accuracy = 0.996500, Test set accuracy = 0.956900
 epoch 23/100. Took 0.55442 seconds. 
  Full-batch training loss = 0.025022, test loss = 0.153526
  Training set accuracy = 0.996800, Test set accuracy = 0.956500
 epoch 24/100. Took 0.557 seconds. 
  Full-batch training loss = 0.022883, test loss = 0.153374
  Training set accuracy = 0.997400, Test set accuracy = 0.956800
 epoch 25/100. Took 0.5568 seconds. 
  Full-batch training loss = 0.022161, test loss = 0.156531
  Training set accuracy = 0.997700, Test set accuracy = 0.955600
 epoch 26/100. Took 0.55673 seconds. 
  Full-batch training loss = 0.019850, test loss = 0.152812
  Training set accuracy = 0.998400, Test set accuracy = 0.956900
 epoch 27/100. Took 0.55142 seconds. 
  Full-batch training loss = 0.018136, test loss = 0.154185
  Training set accuracy = 0.998700, Test set accuracy = 0.957100
 epoch 28/100. Took 0.55834 seconds. 
  Full-batch training loss = 0.016868, test loss = 0.152866
  Training set accuracy = 0.998600, Test set accuracy = 0.957100
 epoch 29/100. Took 0.5544 seconds. 
  Full-batch training loss = 0.015844, test loss = 0.152415
  Training set accuracy = 0.998600, Test set accuracy = 0.958000
 epoch 30/100. Took 0.55527 seconds. 
  Full-batch training loss = 0.015204, test loss = 0.154633
  Training set accuracy = 0.999000, Test set accuracy = 0.956900
 epoch 31/100. Took 0.56106 seconds. 
  Full-batch training loss = 0.013954, test loss = 0.155709
  Training set accuracy = 0.999100, Test set accuracy = 0.957100
 epoch 32/100. Took 0.55459 seconds. 
  Full-batch training loss = 0.012964, test loss = 0.155073
  Training set accuracy = 0.999200, Test set accuracy = 0.956800
 epoch 33/100. Took 0.55258 seconds. 
  Full-batch training loss = 0.012158, test loss = 0.155038
  Training set accuracy = 0.999300, Test set accuracy = 0.956400
 epoch 34/100. Took 0.55741 seconds. 
  Full-batch training loss = 0.011599, test loss = 0.154926
  Training set accuracy = 0.999200, Test set accuracy = 0.958000
 epoch 35/100. Took 0.55346 seconds. 
  Full-batch training loss = 0.011076, test loss = 0.154754
  Training set accuracy = 0.999200, Test set accuracy = 0.957400
 epoch 36/100. Took 0.558 seconds. 
  Full-batch training loss = 0.010477, test loss = 0.156100
  Training set accuracy = 0.999700, Test set accuracy = 0.957300
 epoch 37/100. Took 0.55294 seconds. 
  Full-batch training loss = 0.009901, test loss = 0.155371
  Training set accuracy = 0.999700, Test set accuracy = 0.956800
 epoch 38/100. Took 0.5565 seconds. 
  Full-batch training loss = 0.009290, test loss = 0.156545
  Training set accuracy = 0.999800, Test set accuracy = 0.957600
 epoch 39/100. Took 0.55836 seconds. 
  Full-batch training loss = 0.008939, test loss = 0.155994
  Training set accuracy = 0.999700, Test set accuracy = 0.958200
 epoch 40/100. Took 0.55409 seconds. 
  Full-batch training loss = 0.008461, test loss = 0.156478
  Training set accuracy = 0.999900, Test set accuracy = 0.958600
 epoch 41/100. Took 0.55553 seconds. 
  Full-batch training loss = 0.008001, test loss = 0.157036
  Training set accuracy = 0.999900, Test set accuracy = 0.958200
 epoch 42/100. Took 0.55399 seconds. 
  Full-batch training loss = 0.007786, test loss = 0.157949
  Training set accuracy = 0.999900, Test set accuracy = 0.957300
 epoch 43/100. Took 0.55623 seconds. 
  Full-batch training loss = 0.007350, test loss = 0.157376
  Training set accuracy = 0.999900, Test set accuracy = 0.958600
 epoch 44/100. Took 0.56205 seconds. 
  Full-batch training loss = 0.007115, test loss = 0.157506
  Training set accuracy = 0.999900, Test set accuracy = 0.958400
 epoch 45/100. Took 0.6032 seconds. 
  Full-batch training loss = 0.006762, test loss = 0.158402
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 46/100. Took 0.56342 seconds. 
  Full-batch training loss = 0.006566, test loss = 0.158138
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 47/100. Took 0.56601 seconds. 
  Full-batch training loss = 0.006295, test loss = 0.158711
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 48/100. Took 0.58058 seconds. 
  Full-batch training loss = 0.006051, test loss = 0.159630
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 49/100. Took 0.56204 seconds. 
  Full-batch training loss = 0.005830, test loss = 0.159829
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 50/100. Took 0.57338 seconds. 
  Full-batch training loss = 0.005623, test loss = 0.160118
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 51/100. Took 0.56384 seconds. 
  Full-batch training loss = 0.005427, test loss = 0.160405
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 52/100. Took 0.56947 seconds. 
  Full-batch training loss = 0.005273, test loss = 0.160623
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 53/100. Took 0.56116 seconds. 
  Full-batch training loss = 0.005105, test loss = 0.161214
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 54/100. Took 0.5613 seconds. 
  Full-batch training loss = 0.004949, test loss = 0.161413
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 55/100. Took 0.59616 seconds. 
  Full-batch training loss = 0.004798, test loss = 0.161828
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 56/100. Took 0.63276 seconds. 
  Full-batch training loss = 0.004633, test loss = 0.161790
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 57/100. Took 0.65996 seconds. 
  Full-batch training loss = 0.004535, test loss = 0.161383
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 58/100. Took 0.63606 seconds. 
  Full-batch training loss = 0.004401, test loss = 0.161940
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 59/100. Took 0.60209 seconds. 
  Full-batch training loss = 0.004250, test loss = 0.162597
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 60/100. Took 0.60923 seconds. 
  Full-batch training loss = 0.004139, test loss = 0.162542
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 61/100. Took 0.68493 seconds. 
  Full-batch training loss = 0.004025, test loss = 0.162752
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 62/100. Took 0.59812 seconds. 
  Full-batch training loss = 0.003923, test loss = 0.163439
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 63/100. Took 0.65533 seconds. 
  Full-batch training loss = 0.003823, test loss = 0.164078
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 64/100. Took 0.67103 seconds. 
  Full-batch training loss = 0.003735, test loss = 0.163649
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 65/100. Took 0.67678 seconds. 
  Full-batch training loss = 0.003635, test loss = 0.164208
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 66/100. Took 0.6514 seconds. 
  Full-batch training loss = 0.003549, test loss = 0.164769
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 67/100. Took 0.6459 seconds. 
  Full-batch training loss = 0.003465, test loss = 0.164597
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 68/100. Took 0.61138 seconds. 
  Full-batch training loss = 0.003391, test loss = 0.164646
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 69/100. Took 0.62366 seconds. 
  Full-batch training loss = 0.003305, test loss = 0.164971
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 70/100. Took 0.62863 seconds. 
  Full-batch training loss = 0.003231, test loss = 0.165373
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 71/100. Took 0.6053 seconds. 
  Full-batch training loss = 0.003162, test loss = 0.165704
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 72/100. Took 0.6935 seconds. 
  Full-batch training loss = 0.003093, test loss = 0.165828
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 73/100. Took 0.61589 seconds. 
  Full-batch training loss = 0.003026, test loss = 0.166217
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 74/100. Took 0.59368 seconds. 
  Full-batch training loss = 0.002965, test loss = 0.166053
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 75/100. Took 0.61255 seconds. 
  Full-batch training loss = 0.002911, test loss = 0.166351
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 76/100. Took 0.57888 seconds. 
  Full-batch training loss = 0.002850, test loss = 0.167096
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 77/100. Took 0.59198 seconds. 
  Full-batch training loss = 0.002789, test loss = 0.167099
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 78/100. Took 0.58039 seconds. 
  Full-batch training loss = 0.002738, test loss = 0.167271
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 79/100. Took 0.57582 seconds. 
  Full-batch training loss = 0.002683, test loss = 0.167319
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 80/100. Took 0.58852 seconds. 
  Full-batch training loss = 0.002632, test loss = 0.167609
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 81/100. Took 0.62572 seconds. 
  Full-batch training loss = 0.002586, test loss = 0.167932
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 82/100. Took 0.61575 seconds. 
  Full-batch training loss = 0.002546, test loss = 0.168240
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 83/100. Took 0.63067 seconds. 
  Full-batch training loss = 0.002487, test loss = 0.168197
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 84/100. Took 0.6402 seconds. 
  Full-batch training loss = 0.002450, test loss = 0.168299
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 85/100. Took 0.61578 seconds. 
  Full-batch training loss = 0.002405, test loss = 0.168748
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 86/100. Took 0.60862 seconds. 
  Full-batch training loss = 0.002362, test loss = 0.169084
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 87/100. Took 0.63957 seconds. 
  Full-batch training loss = 0.002321, test loss = 0.169116
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 88/100. Took 0.6414 seconds. 
  Full-batch training loss = 0.002284, test loss = 0.169210
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 89/100. Took 0.62592 seconds. 
  Full-batch training loss = 0.002248, test loss = 0.169911
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 90/100. Took 0.63678 seconds. 
  Full-batch training loss = 0.002206, test loss = 0.169648
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 91/100. Took 0.60034 seconds. 
  Full-batch training loss = 0.002174, test loss = 0.169785
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 92/100. Took 0.58253 seconds. 
  Full-batch training loss = 0.002135, test loss = 0.169954
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 93/100. Took 0.58216 seconds. 
  Full-batch training loss = 0.002102, test loss = 0.169987
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 94/100. Took 0.60485 seconds. 
  Full-batch training loss = 0.002071, test loss = 0.170014
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 95/100. Took 0.59029 seconds. 
  Full-batch training loss = 0.002042, test loss = 0.170255
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 96/100. Took 0.61002 seconds. 
  Full-batch training loss = 0.002009, test loss = 0.170912
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 97/100. Took 0.59598 seconds. 
  Full-batch training loss = 0.001982, test loss = 0.170995
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 98/100. Took 0.59336 seconds. 
  Full-batch training loss = 0.001950, test loss = 0.171162
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 99/100. Took 0.58067 seconds. 
  Full-batch training loss = 0.001920, test loss = 0.171356
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 100/100. Took 0.57377 seconds. 
  Full-batch training loss = 0.001895, test loss = 0.171439
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
Elapsed time is 137.736872 seconds.
End Training
