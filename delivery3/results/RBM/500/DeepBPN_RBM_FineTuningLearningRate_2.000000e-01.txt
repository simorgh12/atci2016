Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.56127 seconds. Average reconstruction error is: 31.0662
 epoch 2/20. Took 0.5645 seconds. Average reconstruction error is: 18.8462
 epoch 3/20. Took 0.55606 seconds. Average reconstruction error is: 16.3412
 epoch 4/20. Took 0.56283 seconds. Average reconstruction error is: 14.8644
 epoch 5/20. Took 0.55786 seconds. Average reconstruction error is: 13.8169
 epoch 6/20. Took 0.5536 seconds. Average reconstruction error is: 12.969
 epoch 7/20. Took 0.55749 seconds. Average reconstruction error is: 12.2351
 epoch 8/20. Took 0.55578 seconds. Average reconstruction error is: 11.7493
 epoch 9/20. Took 0.55522 seconds. Average reconstruction error is: 11.3684
 epoch 10/20. Took 0.55666 seconds. Average reconstruction error is: 11.0679
 epoch 11/20. Took 0.55575 seconds. Average reconstruction error is: 10.8272
 epoch 12/20. Took 0.55359 seconds. Average reconstruction error is: 10.5713
 epoch 13/20. Took 0.55704 seconds. Average reconstruction error is: 10.3831
 epoch 14/20. Took 0.56752 seconds. Average reconstruction error is: 10.2621
 epoch 15/20. Took 0.55849 seconds. Average reconstruction error is: 10.1062
 epoch 16/20. Took 0.55843 seconds. Average reconstruction error is: 9.9819
 epoch 17/20. Took 0.55834 seconds. Average reconstruction error is: 9.8874
 epoch 18/20. Took 0.55793 seconds. Average reconstruction error is: 9.7848
 epoch 19/20. Took 0.55906 seconds. Average reconstruction error is: 9.6616
 epoch 20/20. Took 0.57812 seconds. Average reconstruction error is: 9.5693
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20087 seconds. Average reconstruction error is: 13.7265
 epoch 2/20. Took 0.20377 seconds. Average reconstruction error is: 8.1752
 epoch 3/20. Took 0.20239 seconds. Average reconstruction error is: 7.1709
 epoch 4/20. Took 0.2033 seconds. Average reconstruction error is: 6.7131
 epoch 5/20. Took 0.20349 seconds. Average reconstruction error is: 6.4083
 epoch 6/20. Took 0.20286 seconds. Average reconstruction error is: 6.2044
 epoch 7/20. Took 0.20479 seconds. Average reconstruction error is: 6.0305
 epoch 8/20. Took 0.20401 seconds. Average reconstruction error is: 5.8641
 epoch 9/20. Took 0.20518 seconds. Average reconstruction error is: 5.7427
 epoch 10/20. Took 0.20402 seconds. Average reconstruction error is: 5.6197
 epoch 11/20. Took 0.20562 seconds. Average reconstruction error is: 5.5486
 epoch 12/20. Took 0.20294 seconds. Average reconstruction error is: 5.4315
 epoch 13/20. Took 0.20303 seconds. Average reconstruction error is: 5.4035
 epoch 14/20. Took 0.20263 seconds. Average reconstruction error is: 5.3055
 epoch 15/20. Took 0.20378 seconds. Average reconstruction error is: 5.2657
 epoch 16/20. Took 0.20337 seconds. Average reconstruction error is: 5.1972
 epoch 17/20. Took 0.20416 seconds. Average reconstruction error is: 5.1678
 epoch 18/20. Took 0.20424 seconds. Average reconstruction error is: 5.0993
 epoch 19/20. Took 0.20377 seconds. Average reconstruction error is: 5.0519
 epoch 20/20. Took 0.20311 seconds. Average reconstruction error is: 5.0358
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20142 seconds. Average reconstruction error is: 11.8224
 epoch 2/20. Took 0.20205 seconds. Average reconstruction error is: 6.9466
 epoch 3/20. Took 0.20223 seconds. Average reconstruction error is: 6.2077
 epoch 4/20. Took 0.20252 seconds. Average reconstruction error is: 5.9005
 epoch 5/20. Took 0.20138 seconds. Average reconstruction error is: 5.6924
 epoch 6/20. Took 0.19658 seconds. Average reconstruction error is: 5.5211
 epoch 7/20. Took 0.1945 seconds. Average reconstruction error is: 5.3875
 epoch 8/20. Took 0.19536 seconds. Average reconstruction error is: 5.3292
 epoch 9/20. Took 0.19716 seconds. Average reconstruction error is: 5.2173
 epoch 10/20. Took 0.19544 seconds. Average reconstruction error is: 5.1789
 epoch 11/20. Took 0.19574 seconds. Average reconstruction error is: 5.1164
 epoch 12/20. Took 0.19683 seconds. Average reconstruction error is: 5.0415
 epoch 13/20. Took 0.19555 seconds. Average reconstruction error is: 4.9975
 epoch 14/20. Took 0.19564 seconds. Average reconstruction error is: 4.9375
 epoch 15/20. Took 0.19683 seconds. Average reconstruction error is: 4.9036
 epoch 16/20. Took 0.19662 seconds. Average reconstruction error is: 4.8466
 epoch 17/20. Took 0.19623 seconds. Average reconstruction error is: 4.8113
 epoch 18/20. Took 0.1967 seconds. Average reconstruction error is: 4.7536
 epoch 19/20. Took 0.19688 seconds. Average reconstruction error is: 4.7012
 epoch 20/20. Took 0.19631 seconds. Average reconstruction error is: 4.6772
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.58101 seconds. 
  Full-batch training loss = 0.297781, test loss = 0.287884
  Training set accuracy = 0.917600, Test set accuracy = 0.921900
 epoch 2/100. Took 0.5818 seconds. 
  Full-batch training loss = 0.234579, test loss = 0.242838
  Training set accuracy = 0.936400, Test set accuracy = 0.933300
 epoch 3/100. Took 0.58713 seconds. 
  Full-batch training loss = 0.203867, test loss = 0.226088
  Training set accuracy = 0.942000, Test set accuracy = 0.933300
 epoch 4/100. Took 0.58784 seconds. 
  Full-batch training loss = 0.167431, test loss = 0.201762
  Training set accuracy = 0.954500, Test set accuracy = 0.942100
 epoch 5/100. Took 0.58428 seconds. 
  Full-batch training loss = 0.147873, test loss = 0.190848
  Training set accuracy = 0.960300, Test set accuracy = 0.944000
 epoch 6/100. Took 0.57957 seconds. 
  Full-batch training loss = 0.135173, test loss = 0.185628
  Training set accuracy = 0.962700, Test set accuracy = 0.945000
 epoch 7/100. Took 0.58756 seconds. 
  Full-batch training loss = 0.118429, test loss = 0.180492
  Training set accuracy = 0.967500, Test set accuracy = 0.944500
 epoch 8/100. Took 0.59387 seconds. 
  Full-batch training loss = 0.108348, test loss = 0.175874
  Training set accuracy = 0.973900, Test set accuracy = 0.949000
 epoch 9/100. Took 0.58615 seconds. 
  Full-batch training loss = 0.093435, test loss = 0.168219
  Training set accuracy = 0.976100, Test set accuracy = 0.950900
 epoch 10/100. Took 0.58727 seconds. 
  Full-batch training loss = 0.082927, test loss = 0.164452
  Training set accuracy = 0.979100, Test set accuracy = 0.949400
 epoch 11/100. Took 0.59071 seconds. 
  Full-batch training loss = 0.077734, test loss = 0.166717
  Training set accuracy = 0.980700, Test set accuracy = 0.950900
 epoch 12/100. Took 0.58408 seconds. 
  Full-batch training loss = 0.070517, test loss = 0.162170
  Training set accuracy = 0.983100, Test set accuracy = 0.950400
 epoch 13/100. Took 0.58679 seconds. 
  Full-batch training loss = 0.061652, test loss = 0.158281
  Training set accuracy = 0.986300, Test set accuracy = 0.952900
 epoch 14/100. Took 0.58339 seconds. 
  Full-batch training loss = 0.055325, test loss = 0.157381
  Training set accuracy = 0.988800, Test set accuracy = 0.952600
 epoch 15/100. Took 0.58133 seconds. 
  Full-batch training loss = 0.053258, test loss = 0.158844
  Training set accuracy = 0.988900, Test set accuracy = 0.953700
 epoch 16/100. Took 0.58739 seconds. 
  Full-batch training loss = 0.046376, test loss = 0.154901
  Training set accuracy = 0.990800, Test set accuracy = 0.953500
 epoch 17/100. Took 0.58494 seconds. 
  Full-batch training loss = 0.041176, test loss = 0.152576
  Training set accuracy = 0.992800, Test set accuracy = 0.955700
 epoch 18/100. Took 0.59175 seconds. 
  Full-batch training loss = 0.038514, test loss = 0.155346
  Training set accuracy = 0.993300, Test set accuracy = 0.955000
 epoch 19/100. Took 0.59061 seconds. 
  Full-batch training loss = 0.037338, test loss = 0.155830
  Training set accuracy = 0.993100, Test set accuracy = 0.956200
 epoch 20/100. Took 0.58818 seconds. 
  Full-batch training loss = 0.031926, test loss = 0.152135
  Training set accuracy = 0.995000, Test set accuracy = 0.955100
 epoch 21/100. Took 0.59761 seconds. 
  Full-batch training loss = 0.030356, test loss = 0.154426
  Training set accuracy = 0.995100, Test set accuracy = 0.953100
 epoch 22/100. Took 0.59633 seconds. 
  Full-batch training loss = 0.027176, test loss = 0.153397
  Training set accuracy = 0.995800, Test set accuracy = 0.955400
 epoch 23/100. Took 0.59012 seconds. 
  Full-batch training loss = 0.024363, test loss = 0.152205
  Training set accuracy = 0.996600, Test set accuracy = 0.956800
 epoch 24/100. Took 0.58404 seconds. 
  Full-batch training loss = 0.022473, test loss = 0.152100
  Training set accuracy = 0.997500, Test set accuracy = 0.956900
 epoch 25/100. Took 0.5912 seconds. 
  Full-batch training loss = 0.020755, test loss = 0.151527
  Training set accuracy = 0.997400, Test set accuracy = 0.956500
 epoch 26/100. Took 0.58691 seconds. 
  Full-batch training loss = 0.019719, test loss = 0.153702
  Training set accuracy = 0.997900, Test set accuracy = 0.955900
 epoch 27/100. Took 0.58747 seconds. 
  Full-batch training loss = 0.018587, test loss = 0.153583
  Training set accuracy = 0.998600, Test set accuracy = 0.957300
 epoch 28/100. Took 0.59227 seconds. 
  Full-batch training loss = 0.016367, test loss = 0.152223
  Training set accuracy = 0.999100, Test set accuracy = 0.957000
 epoch 29/100. Took 0.58364 seconds. 
  Full-batch training loss = 0.015148, test loss = 0.153198
  Training set accuracy = 0.999200, Test set accuracy = 0.956200
 epoch 30/100. Took 0.58974 seconds. 
  Full-batch training loss = 0.014444, test loss = 0.153642
  Training set accuracy = 0.999300, Test set accuracy = 0.957400
 epoch 31/100. Took 0.59066 seconds. 
  Full-batch training loss = 0.013333, test loss = 0.153773
  Training set accuracy = 0.999400, Test set accuracy = 0.957100
 epoch 32/100. Took 0.59162 seconds. 
  Full-batch training loss = 0.012527, test loss = 0.153473
  Training set accuracy = 0.999500, Test set accuracy = 0.958000
 epoch 33/100. Took 0.60949 seconds. 
  Full-batch training loss = 0.011848, test loss = 0.153778
  Training set accuracy = 0.999600, Test set accuracy = 0.957800
 epoch 34/100. Took 0.65827 seconds. 
  Full-batch training loss = 0.011623, test loss = 0.155175
  Training set accuracy = 0.999800, Test set accuracy = 0.956300
 epoch 35/100. Took 0.62389 seconds. 
  Full-batch training loss = 0.010419, test loss = 0.154827
  Training set accuracy = 0.999800, Test set accuracy = 0.958000
 epoch 36/100. Took 0.58149 seconds. 
  Full-batch training loss = 0.009949, test loss = 0.155414
  Training set accuracy = 0.999800, Test set accuracy = 0.958000
 epoch 37/100. Took 0.62589 seconds. 
  Full-batch training loss = 0.009364, test loss = 0.156058
  Training set accuracy = 0.999800, Test set accuracy = 0.958100
 epoch 38/100. Took 0.68217 seconds. 
  Full-batch training loss = 0.008971, test loss = 0.156974
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 39/100. Took 0.59545 seconds. 
  Full-batch training loss = 0.008524, test loss = 0.156975
  Training set accuracy = 0.999800, Test set accuracy = 0.958000
 epoch 40/100. Took 0.62224 seconds. 
  Full-batch training loss = 0.008100, test loss = 0.156572
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 41/100. Took 0.63312 seconds. 
  Full-batch training loss = 0.007666, test loss = 0.157169
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 42/100. Took 0.63366 seconds. 
  Full-batch training loss = 0.007719, test loss = 0.158271
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 43/100. Took 0.71817 seconds. 
  Full-batch training loss = 0.007099, test loss = 0.158337
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 44/100. Took 0.71555 seconds. 
  Full-batch training loss = 0.006777, test loss = 0.158992
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 45/100. Took 0.70187 seconds. 
  Full-batch training loss = 0.006519, test loss = 0.159175
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 46/100. Took 0.68581 seconds. 
  Full-batch training loss = 0.006253, test loss = 0.159247
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 47/100. Took 0.66588 seconds. 
  Full-batch training loss = 0.006066, test loss = 0.160200
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 48/100. Took 0.67311 seconds. 
  Full-batch training loss = 0.005816, test loss = 0.160124
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 49/100. Took 0.66262 seconds. 
  Full-batch training loss = 0.005591, test loss = 0.160624
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 50/100. Took 0.66551 seconds. 
  Full-batch training loss = 0.005434, test loss = 0.160642
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 51/100. Took 0.64022 seconds. 
  Full-batch training loss = 0.005208, test loss = 0.160928
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 52/100. Took 0.64837 seconds. 
  Full-batch training loss = 0.005089, test loss = 0.161741
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 53/100. Took 0.6498 seconds. 
  Full-batch training loss = 0.004871, test loss = 0.161419
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 54/100. Took 0.63265 seconds. 
  Full-batch training loss = 0.004757, test loss = 0.161435
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 55/100. Took 0.63921 seconds. 
  Full-batch training loss = 0.004609, test loss = 0.161549
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 56/100. Took 0.61732 seconds. 
  Full-batch training loss = 0.004498, test loss = 0.162328
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 57/100. Took 0.64429 seconds. 
  Full-batch training loss = 0.004319, test loss = 0.163291
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 58/100. Took 0.72107 seconds. 
  Full-batch training loss = 0.004188, test loss = 0.163108
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 59/100. Took 0.6771 seconds. 
  Full-batch training loss = 0.004091, test loss = 0.163758
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 60/100. Took 0.63167 seconds. 
  Full-batch training loss = 0.003973, test loss = 0.163509
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 61/100. Took 0.63414 seconds. 
  Full-batch training loss = 0.003859, test loss = 0.164142
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 62/100. Took 0.62386 seconds. 
  Full-batch training loss = 0.003766, test loss = 0.164265
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 63/100. Took 0.63259 seconds. 
  Full-batch training loss = 0.003668, test loss = 0.164610
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 64/100. Took 0.65111 seconds. 
  Full-batch training loss = 0.003590, test loss = 0.165024
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 65/100. Took 0.60944 seconds. 
  Full-batch training loss = 0.003485, test loss = 0.165354
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 66/100. Took 0.61407 seconds. 
  Full-batch training loss = 0.003410, test loss = 0.165610
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 67/100. Took 0.6104 seconds. 
  Full-batch training loss = 0.003333, test loss = 0.165640
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 68/100. Took 0.61539 seconds. 
  Full-batch training loss = 0.003259, test loss = 0.165639
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 69/100. Took 0.6155 seconds. 
  Full-batch training loss = 0.003168, test loss = 0.165886
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 70/100. Took 0.61806 seconds. 
  Full-batch training loss = 0.003105, test loss = 0.166577
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 71/100. Took 0.67429 seconds. 
  Full-batch training loss = 0.003032, test loss = 0.166513
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 72/100. Took 0.64672 seconds. 
  Full-batch training loss = 0.002965, test loss = 0.166649
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 73/100. Took 0.6341 seconds. 
  Full-batch training loss = 0.002907, test loss = 0.167181
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 74/100. Took 0.617 seconds. 
  Full-batch training loss = 0.002843, test loss = 0.167348
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 75/100. Took 0.63997 seconds. 
  Full-batch training loss = 0.002784, test loss = 0.167467
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 76/100. Took 0.63665 seconds. 
  Full-batch training loss = 0.002727, test loss = 0.167602
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 77/100. Took 0.63826 seconds. 
  Full-batch training loss = 0.002670, test loss = 0.167853
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 78/100. Took 0.61075 seconds. 
  Full-batch training loss = 0.002618, test loss = 0.168275
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 79/100. Took 0.6547 seconds. 
  Full-batch training loss = 0.002569, test loss = 0.168427
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 80/100. Took 0.69153 seconds. 
  Full-batch training loss = 0.002522, test loss = 0.168618
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 81/100. Took 0.67048 seconds. 
  Full-batch training loss = 0.002476, test loss = 0.168990
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 82/100. Took 0.66052 seconds. 
  Full-batch training loss = 0.002429, test loss = 0.169082
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 83/100. Took 0.61953 seconds. 
  Full-batch training loss = 0.002385, test loss = 0.169160
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 84/100. Took 0.62365 seconds. 
  Full-batch training loss = 0.002345, test loss = 0.169406
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 85/100. Took 0.62023 seconds. 
  Full-batch training loss = 0.002303, test loss = 0.169460
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 86/100. Took 0.67603 seconds. 
  Full-batch training loss = 0.002262, test loss = 0.169720
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 87/100. Took 0.5989 seconds. 
  Full-batch training loss = 0.002222, test loss = 0.169965
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 88/100. Took 0.57895 seconds. 
  Full-batch training loss = 0.002189, test loss = 0.170082
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 89/100. Took 0.6076 seconds. 
  Full-batch training loss = 0.002149, test loss = 0.170370
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 90/100. Took 0.67434 seconds. 
  Full-batch training loss = 0.002113, test loss = 0.170426
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 91/100. Took 0.66651 seconds. 
  Full-batch training loss = 0.002080, test loss = 0.170836
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 92/100. Took 0.6338 seconds. 
  Full-batch training loss = 0.002045, test loss = 0.171018
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 93/100. Took 0.62792 seconds. 
  Full-batch training loss = 0.002013, test loss = 0.171268
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 94/100. Took 0.60242 seconds. 
  Full-batch training loss = 0.001980, test loss = 0.171409
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 95/100. Took 0.61009 seconds. 
  Full-batch training loss = 0.001949, test loss = 0.171737
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 96/100. Took 0.61373 seconds. 
  Full-batch training loss = 0.001920, test loss = 0.171780
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 97/100. Took 0.64821 seconds. 
  Full-batch training loss = 0.001891, test loss = 0.172000
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 98/100. Took 0.66845 seconds. 
  Full-batch training loss = 0.001863, test loss = 0.172181
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 99/100. Took 0.67419 seconds. 
  Full-batch training loss = 0.001836, test loss = 0.172366
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 100/100. Took 0.61425 seconds. 
  Full-batch training loss = 0.001809, test loss = 0.172510
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
Elapsed time is 143.555974 seconds.
End Training
