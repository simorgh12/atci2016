Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (500  200)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 4.5317 seconds. Average reconstruction error is: 27.2258
 epoch 2/20. Took 4.5436 seconds. Average reconstruction error is: 16.0029
 epoch 3/20. Took 4.6101 seconds. Average reconstruction error is: 13.805
 epoch 4/20. Took 4.511 seconds. Average reconstruction error is: 12.4873
 epoch 5/20. Took 4.5341 seconds. Average reconstruction error is: 11.5895
 epoch 6/20. Took 4.5433 seconds. Average reconstruction error is: 10.9242
 epoch 7/20. Took 4.4988 seconds. Average reconstruction error is: 10.4495
 epoch 8/20. Took 4.4761 seconds. Average reconstruction error is: 10.0777
 epoch 9/20. Took 4.5113 seconds. Average reconstruction error is: 9.8015
 epoch 10/20. Took 4.4484 seconds. Average reconstruction error is: 9.5857
 epoch 11/20. Took 4.5848 seconds. Average reconstruction error is: 9.3962
 epoch 12/20. Took 4.4661 seconds. Average reconstruction error is: 9.2093
 epoch 13/20. Took 4.5028 seconds. Average reconstruction error is: 9.1415
 epoch 14/20. Took 4.5598 seconds. Average reconstruction error is: 9.0091
 epoch 15/20. Took 4.5128 seconds. Average reconstruction error is: 8.9584
 epoch 16/20. Took 4.4543 seconds. Average reconstruction error is: 8.8157
 epoch 17/20. Took 4.5336 seconds. Average reconstruction error is: 8.7363
 epoch 18/20. Took 4.4818 seconds. Average reconstruction error is: 8.645
 epoch 19/20. Took 4.5196 seconds. Average reconstruction error is: 8.6118
 epoch 20/20. Took 4.5676 seconds. Average reconstruction error is: 8.5487
Training binary-binary RBM in layer 2 (500  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.4534 seconds. Average reconstruction error is: 19.2455
 epoch 2/20. Took 1.3874 seconds. Average reconstruction error is: 11.27
 epoch 3/20. Took 1.4197 seconds. Average reconstruction error is: 9.9866
 epoch 4/20. Took 1.3756 seconds. Average reconstruction error is: 9.3294
 epoch 5/20. Took 1.4018 seconds. Average reconstruction error is: 8.9207
 epoch 6/20. Took 1.4292 seconds. Average reconstruction error is: 8.5929
 epoch 7/20. Took 1.4193 seconds. Average reconstruction error is: 8.3591
 epoch 8/20. Took 1.4144 seconds. Average reconstruction error is: 8.1433
 epoch 9/20. Took 1.4548 seconds. Average reconstruction error is: 7.9932
 epoch 10/20. Took 1.439 seconds. Average reconstruction error is: 7.852
 epoch 11/20. Took 1.4493 seconds. Average reconstruction error is: 7.6536
 epoch 12/20. Took 1.5198 seconds. Average reconstruction error is: 7.5251
 epoch 13/20. Took 1.7569 seconds. Average reconstruction error is: 7.4292
 epoch 14/20. Took 1.4369 seconds. Average reconstruction error is: 7.319
 epoch 15/20. Took 1.3993 seconds. Average reconstruction error is: 7.2067
 epoch 16/20. Took 1.4005 seconds. Average reconstruction error is: 7.1315
 epoch 17/20. Took 1.3916 seconds. Average reconstruction error is: 7.0379
 epoch 18/20. Took 1.4365 seconds. Average reconstruction error is: 6.9558
 epoch 19/20. Took 1.4356 seconds. Average reconstruction error is: 6.873
 epoch 20/20. Took 1.4441 seconds. Average reconstruction error is: 6.7827
Training NN  (784  500  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 3.2998 seconds. 
  Full-batch training loss = 0.288177, test loss = 0.292829
  Training set accuracy = 0.920900, Test set accuracy = 0.917000
 epoch 2/100. Took 3.3286 seconds. 
  Full-batch training loss = 0.229638, test loss = 0.249944
  Training set accuracy = 0.934000, Test set accuracy = 0.928900
 epoch 3/100. Took 3.3567 seconds. 
  Full-batch training loss = 0.198096, test loss = 0.230946
  Training set accuracy = 0.945300, Test set accuracy = 0.933900
 epoch 4/100. Took 3.3404 seconds. 
  Full-batch training loss = 0.167600, test loss = 0.212505
  Training set accuracy = 0.953900, Test set accuracy = 0.939000
 epoch 5/100. Took 3.3703 seconds. 
  Full-batch training loss = 0.151428, test loss = 0.204353
  Training set accuracy = 0.958200, Test set accuracy = 0.940300
 epoch 6/100. Took 3.355 seconds. 
  Full-batch training loss = 0.134380, test loss = 0.196516
  Training set accuracy = 0.966900, Test set accuracy = 0.941700
 epoch 7/100. Took 3.3025 seconds. 
  Full-batch training loss = 0.116885, test loss = 0.189209
  Training set accuracy = 0.970100, Test set accuracy = 0.946800
 epoch 8/100. Took 3.2841 seconds. 
  Full-batch training loss = 0.104651, test loss = 0.179411
  Training set accuracy = 0.974300, Test set accuracy = 0.948100
 epoch 9/100. Took 3.3341 seconds. 
  Full-batch training loss = 0.093844, test loss = 0.177411
  Training set accuracy = 0.977900, Test set accuracy = 0.949300
 epoch 10/100. Took 3.2968 seconds. 
  Full-batch training loss = 0.086552, test loss = 0.176990
  Training set accuracy = 0.980300, Test set accuracy = 0.947200
 epoch 11/100. Took 3.2649 seconds. 
  Full-batch training loss = 0.076434, test loss = 0.171561
  Training set accuracy = 0.984100, Test set accuracy = 0.950800
 epoch 12/100. Took 3.3331 seconds. 
  Full-batch training loss = 0.069097, test loss = 0.168479
  Training set accuracy = 0.986300, Test set accuracy = 0.950800
 epoch 13/100. Took 3.3267 seconds. 
  Full-batch training loss = 0.064568, test loss = 0.167846
  Training set accuracy = 0.986900, Test set accuracy = 0.950500
 epoch 14/100. Took 3.3327 seconds. 
  Full-batch training loss = 0.057196, test loss = 0.164826
  Training set accuracy = 0.989300, Test set accuracy = 0.951400
 epoch 15/100. Took 3.4223 seconds. 
  Full-batch training loss = 0.054281, test loss = 0.164179
  Training set accuracy = 0.989200, Test set accuracy = 0.952500
 epoch 16/100. Took 3.2871 seconds. 
  Full-batch training loss = 0.050960, test loss = 0.163601
  Training set accuracy = 0.990700, Test set accuracy = 0.953300
 epoch 17/100. Took 3.2851 seconds. 
  Full-batch training loss = 0.043704, test loss = 0.159604
  Training set accuracy = 0.992700, Test set accuracy = 0.954900
 epoch 18/100. Took 3.2816 seconds. 
  Full-batch training loss = 0.039249, test loss = 0.158909
  Training set accuracy = 0.993900, Test set accuracy = 0.953100
 epoch 19/100. Took 3.2997 seconds. 
  Full-batch training loss = 0.036135, test loss = 0.159983
  Training set accuracy = 0.994900, Test set accuracy = 0.953100
 epoch 20/100. Took 3.3451 seconds. 
  Full-batch training loss = 0.033667, test loss = 0.159344
  Training set accuracy = 0.995100, Test set accuracy = 0.954200
 epoch 21/100. Took 3.3394 seconds. 
  Full-batch training loss = 0.030498, test loss = 0.157272
  Training set accuracy = 0.996000, Test set accuracy = 0.954800
 epoch 22/100. Took 3.3191 seconds. 
  Full-batch training loss = 0.028307, test loss = 0.157025
  Training set accuracy = 0.996600, Test set accuracy = 0.954500
 epoch 23/100. Took 3.3206 seconds. 
  Full-batch training loss = 0.026073, test loss = 0.157855
  Training set accuracy = 0.997100, Test set accuracy = 0.955700
 epoch 24/100. Took 3.3285 seconds. 
  Full-batch training loss = 0.024460, test loss = 0.159707
  Training set accuracy = 0.998100, Test set accuracy = 0.954400
 epoch 25/100. Took 3.4039 seconds. 
  Full-batch training loss = 0.022865, test loss = 0.156586
  Training set accuracy = 0.998100, Test set accuracy = 0.955000
 epoch 26/100. Took 3.3061 seconds. 
  Full-batch training loss = 0.021089, test loss = 0.157673
  Training set accuracy = 0.998800, Test set accuracy = 0.955100
 epoch 27/100. Took 3.332 seconds. 
  Full-batch training loss = 0.019508, test loss = 0.156067
  Training set accuracy = 0.998800, Test set accuracy = 0.955400
 epoch 28/100. Took 3.2817 seconds. 
  Full-batch training loss = 0.018926, test loss = 0.161121
  Training set accuracy = 0.999000, Test set accuracy = 0.955300
 epoch 29/100. Took 3.3689 seconds. 
  Full-batch training loss = 0.017371, test loss = 0.159622
  Training set accuracy = 0.999200, Test set accuracy = 0.955900
 epoch 30/100. Took 3.3062 seconds. 
  Full-batch training loss = 0.016342, test loss = 0.159263
  Training set accuracy = 0.999300, Test set accuracy = 0.956000
 epoch 31/100. Took 3.7481 seconds. 
  Full-batch training loss = 0.015176, test loss = 0.157643
  Training set accuracy = 0.999400, Test set accuracy = 0.955800
 epoch 32/100. Took 3.3394 seconds. 
  Full-batch training loss = 0.014373, test loss = 0.156841
  Training set accuracy = 0.999400, Test set accuracy = 0.956200
 epoch 33/100. Took 3.3265 seconds. 
  Full-batch training loss = 0.013312, test loss = 0.157162
  Training set accuracy = 0.999600, Test set accuracy = 0.956500
 epoch 34/100. Took 3.3071 seconds. 
  Full-batch training loss = 0.012644, test loss = 0.157045
  Training set accuracy = 0.999600, Test set accuracy = 0.956100
 epoch 35/100. Took 3.2852 seconds. 
  Full-batch training loss = 0.012341, test loss = 0.160244
  Training set accuracy = 0.999800, Test set accuracy = 0.956100
 epoch 36/100. Took 3.3316 seconds. 
  Full-batch training loss = 0.011357, test loss = 0.157620
  Training set accuracy = 0.999600, Test set accuracy = 0.956600
 epoch 37/100. Took 3.2019 seconds. 
  Full-batch training loss = 0.010748, test loss = 0.158130
  Training set accuracy = 0.999800, Test set accuracy = 0.957200
 epoch 38/100. Took 3.1791 seconds. 
  Full-batch training loss = 0.010252, test loss = 0.157800
  Training set accuracy = 0.999800, Test set accuracy = 0.957100
 epoch 39/100. Took 3.2022 seconds. 
  Full-batch training loss = 0.009809, test loss = 0.158157
  Training set accuracy = 0.999800, Test set accuracy = 0.956900
 epoch 40/100. Took 3.2533 seconds. 
  Full-batch training loss = 0.009377, test loss = 0.159237
  Training set accuracy = 0.999900, Test set accuracy = 0.956800
 epoch 41/100. Took 3.3322 seconds. 
  Full-batch training loss = 0.008955, test loss = 0.159131
  Training set accuracy = 0.999900, Test set accuracy = 0.956700
 epoch 42/100. Took 3.3108 seconds. 
  Full-batch training loss = 0.008636, test loss = 0.158914
  Training set accuracy = 0.999900, Test set accuracy = 0.956800
 epoch 43/100. Took 3.3371 seconds. 
  Full-batch training loss = 0.008219, test loss = 0.159839
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 44/100. Took 3.327 seconds. 
  Full-batch training loss = 0.007913, test loss = 0.159951
  Training set accuracy = 0.999900, Test set accuracy = 0.957400
 epoch 45/100. Took 3.2029 seconds. 
  Full-batch training loss = 0.007617, test loss = 0.159745
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 46/100. Took 3.3915 seconds. 
  Full-batch training loss = 0.007280, test loss = 0.160093
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 47/100. Took 3.3348 seconds. 
  Full-batch training loss = 0.007046, test loss = 0.159544
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 48/100. Took 3.2764 seconds. 
  Full-batch training loss = 0.006813, test loss = 0.160957
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 49/100. Took 3.1792 seconds. 
  Full-batch training loss = 0.006546, test loss = 0.161079
  Training set accuracy = 1.000000, Test set accuracy = 0.957000
 epoch 50/100. Took 3.1758 seconds. 
  Full-batch training loss = 0.006340, test loss = 0.160526
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 51/100. Took 3.2606 seconds. 
  Full-batch training loss = 0.006227, test loss = 0.160531
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 52/100. Took 3.3252 seconds. 
  Full-batch training loss = 0.005948, test loss = 0.161985
  Training set accuracy = 1.000000, Test set accuracy = 0.957000
 epoch 53/100. Took 3.3236 seconds. 
  Full-batch training loss = 0.005803, test loss = 0.162803
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 54/100. Took 3.2891 seconds. 
  Full-batch training loss = 0.005611, test loss = 0.161513
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 55/100. Took 3.3737 seconds. 
  Full-batch training loss = 0.005426, test loss = 0.161952
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 56/100. Took 3.3145 seconds. 
  Full-batch training loss = 0.005272, test loss = 0.161854
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 57/100. Took 3.2755 seconds. 
  Full-batch training loss = 0.005117, test loss = 0.162687
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 58/100. Took 3.3585 seconds. 
  Full-batch training loss = 0.005008, test loss = 0.162365
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 59/100. Took 3.3235 seconds. 
  Full-batch training loss = 0.004841, test loss = 0.162374
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 60/100. Took 3.3198 seconds. 
  Full-batch training loss = 0.004724, test loss = 0.163060
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 61/100. Took 3.3697 seconds. 
  Full-batch training loss = 0.004594, test loss = 0.163177
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 62/100. Took 3.2979 seconds. 
  Full-batch training loss = 0.004491, test loss = 0.163542
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 63/100. Took 3.3077 seconds. 
  Full-batch training loss = 0.004362, test loss = 0.163499
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 64/100. Took 3.3427 seconds. 
  Full-batch training loss = 0.004261, test loss = 0.163547
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 65/100. Took 3.3381 seconds. 
  Full-batch training loss = 0.004171, test loss = 0.164289
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 66/100. Took 3.3527 seconds. 
  Full-batch training loss = 0.004068, test loss = 0.163693
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 67/100. Took 3.2898 seconds. 
  Full-batch training loss = 0.003980, test loss = 0.164377
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 68/100. Took 3.3217 seconds. 
  Full-batch training loss = 0.003887, test loss = 0.164291
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 69/100. Took 3.3442 seconds. 
  Full-batch training loss = 0.003804, test loss = 0.164337
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 70/100. Took 3.3653 seconds. 
  Full-batch training loss = 0.003749, test loss = 0.165485
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 71/100. Took 3.3463 seconds. 
  Full-batch training loss = 0.003638, test loss = 0.164863
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 72/100. Took 3.3424 seconds. 
  Full-batch training loss = 0.003575, test loss = 0.164858
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 73/100. Took 3.3217 seconds. 
  Full-batch training loss = 0.003510, test loss = 0.164501
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 74/100. Took 3.2987 seconds. 
  Full-batch training loss = 0.003419, test loss = 0.165359
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 75/100. Took 3.2848 seconds. 
  Full-batch training loss = 0.003359, test loss = 0.165437
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 76/100. Took 3.3178 seconds. 
  Full-batch training loss = 0.003285, test loss = 0.165486
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 77/100. Took 3.3291 seconds. 
  Full-batch training loss = 0.003231, test loss = 0.165415
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 78/100. Took 3.3012 seconds. 
  Full-batch training loss = 0.003172, test loss = 0.165318
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 79/100. Took 3.4212 seconds. 
  Full-batch training loss = 0.003112, test loss = 0.165625
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 80/100. Took 3.3482 seconds. 
  Full-batch training loss = 0.003048, test loss = 0.166036
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 81/100. Took 3.3232 seconds. 
  Full-batch training loss = 0.002995, test loss = 0.165906
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 82/100. Took 3.3086 seconds. 
  Full-batch training loss = 0.002942, test loss = 0.166362
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 83/100. Took 3.3459 seconds. 
  Full-batch training loss = 0.002894, test loss = 0.166949
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 84/100. Took 3.3776 seconds. 
  Full-batch training loss = 0.002842, test loss = 0.166665
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 85/100. Took 3.3036 seconds. 
  Full-batch training loss = 0.002794, test loss = 0.166633
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 86/100. Took 3.3043 seconds. 
  Full-batch training loss = 0.002745, test loss = 0.166659
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 87/100. Took 3.3502 seconds. 
  Full-batch training loss = 0.002702, test loss = 0.166921
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 88/100. Took 3.2958 seconds. 
  Full-batch training loss = 0.002659, test loss = 0.167058
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 89/100. Took 3.3438 seconds. 
  Full-batch training loss = 0.002616, test loss = 0.167357
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 90/100. Took 3.3462 seconds. 
  Full-batch training loss = 0.002579, test loss = 0.167956
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 91/100. Took 3.3515 seconds. 
  Full-batch training loss = 0.002532, test loss = 0.167804
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 92/100. Took 3.3017 seconds. 
  Full-batch training loss = 0.002495, test loss = 0.167765
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 93/100. Took 3.3389 seconds. 
  Full-batch training loss = 0.002455, test loss = 0.167749
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 94/100. Took 3.3674 seconds. 
  Full-batch training loss = 0.002417, test loss = 0.168040
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 95/100. Took 3.3573 seconds. 
  Full-batch training loss = 0.002383, test loss = 0.168429
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 96/100. Took 3.3245 seconds. 
  Full-batch training loss = 0.002346, test loss = 0.168084
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 97/100. Took 3.407 seconds. 
  Full-batch training loss = 0.002317, test loss = 0.168294
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 98/100. Took 3.3883 seconds. 
  Full-batch training loss = 0.002280, test loss = 0.168541
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 99/100. Took 3.3416 seconds. 
  Full-batch training loss = 0.002251, test loss = 0.168277
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 100/100. Took 3.3771 seconds. 
  Full-batch training loss = 0.002218, test loss = 0.168728
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
Elapsed time is 834.738437 seconds.
End Training
