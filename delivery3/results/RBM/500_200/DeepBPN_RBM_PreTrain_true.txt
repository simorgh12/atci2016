Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (500  200)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 4.5927 seconds. Average reconstruction error is: 27.5047
 epoch 2/20. Took 4.5426 seconds. Average reconstruction error is: 16.0225
 epoch 3/20. Took 4.5008 seconds. Average reconstruction error is: 13.8647
 epoch 4/20. Took 4.4928 seconds. Average reconstruction error is: 12.5445
 epoch 5/20. Took 4.5907 seconds. Average reconstruction error is: 11.6788
 epoch 6/20. Took 4.5597 seconds. Average reconstruction error is: 10.996
 epoch 7/20. Took 4.5503 seconds. Average reconstruction error is: 10.5153
 epoch 8/20. Took 4.4857 seconds. Average reconstruction error is: 10.155
 epoch 9/20. Took 4.4717 seconds. Average reconstruction error is: 9.8742
 epoch 10/20. Took 4.5276 seconds. Average reconstruction error is: 9.6382
 epoch 11/20. Took 4.5372 seconds. Average reconstruction error is: 9.4562
 epoch 12/20. Took 4.554 seconds. Average reconstruction error is: 9.3508
 epoch 13/20. Took 4.7602 seconds. Average reconstruction error is: 9.1709
 epoch 14/20. Took 4.5412 seconds. Average reconstruction error is: 9.0476
 epoch 15/20. Took 4.4877 seconds. Average reconstruction error is: 8.9686
 epoch 16/20. Took 4.5194 seconds. Average reconstruction error is: 8.8562
 epoch 17/20. Took 4.4772 seconds. Average reconstruction error is: 8.79
 epoch 18/20. Took 4.4946 seconds. Average reconstruction error is: 8.7122
 epoch 19/20. Took 4.5465 seconds. Average reconstruction error is: 8.6438
 epoch 20/20. Took 4.6241 seconds. Average reconstruction error is: 8.6121
Training binary-binary RBM in layer 2 (500  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.4597 seconds. Average reconstruction error is: 19.3335
 epoch 2/20. Took 1.4573 seconds. Average reconstruction error is: 11.2404
 epoch 3/20. Took 1.471 seconds. Average reconstruction error is: 9.8857
 epoch 4/20. Took 1.4627 seconds. Average reconstruction error is: 9.2289
 epoch 5/20. Took 1.4296 seconds. Average reconstruction error is: 8.7905
 epoch 6/20. Took 1.5141 seconds. Average reconstruction error is: 8.4341
 epoch 7/20. Took 1.4198 seconds. Average reconstruction error is: 8.1288
 epoch 8/20. Took 1.7494 seconds. Average reconstruction error is: 7.9363
 epoch 9/20. Took 1.6741 seconds. Average reconstruction error is: 7.7735
 epoch 10/20. Took 1.4441 seconds. Average reconstruction error is: 7.6232
 epoch 11/20. Took 1.4547 seconds. Average reconstruction error is: 7.4687
 epoch 12/20. Took 1.518 seconds. Average reconstruction error is: 7.3303
 epoch 13/20. Took 1.4275 seconds. Average reconstruction error is: 7.2516
 epoch 14/20. Took 1.4272 seconds. Average reconstruction error is: 7.0967
 epoch 15/20. Took 1.4357 seconds. Average reconstruction error is: 7.0377
 epoch 16/20. Took 1.5463 seconds. Average reconstruction error is: 6.9694
 epoch 17/20. Took 1.7016 seconds. Average reconstruction error is: 6.9116
 epoch 18/20. Took 1.4612 seconds. Average reconstruction error is: 6.8041
 epoch 19/20. Took 1.4073 seconds. Average reconstruction error is: 6.7353
 epoch 20/20. Took 1.4346 seconds. Average reconstruction error is: 6.6641
Training NN  (784  500  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 3.2503 seconds. 
  Full-batch training loss = 0.281540, test loss = 0.289425
  Training set accuracy = 0.920300, Test set accuracy = 0.918100
 epoch 2/100. Took 3.3205 seconds. 
  Full-batch training loss = 0.228684, test loss = 0.253550
  Training set accuracy = 0.935900, Test set accuracy = 0.928600
 epoch 3/100. Took 3.3326 seconds. 
  Full-batch training loss = 0.194646, test loss = 0.230955
  Training set accuracy = 0.946400, Test set accuracy = 0.935400
 epoch 4/100. Took 3.3441 seconds. 
  Full-batch training loss = 0.165144, test loss = 0.214271
  Training set accuracy = 0.956300, Test set accuracy = 0.939100
 epoch 5/100. Took 3.3365 seconds. 
  Full-batch training loss = 0.146604, test loss = 0.203129
  Training set accuracy = 0.961700, Test set accuracy = 0.941400
 epoch 6/100. Took 3.342 seconds. 
  Full-batch training loss = 0.129183, test loss = 0.194717
  Training set accuracy = 0.966000, Test set accuracy = 0.943400
 epoch 7/100. Took 3.3445 seconds. 
  Full-batch training loss = 0.115271, test loss = 0.186976
  Training set accuracy = 0.970400, Test set accuracy = 0.946000
 epoch 8/100. Took 3.339 seconds. 
  Full-batch training loss = 0.101883, test loss = 0.181718
  Training set accuracy = 0.975200, Test set accuracy = 0.948100
 epoch 9/100. Took 3.3502 seconds. 
  Full-batch training loss = 0.091556, test loss = 0.177823
  Training set accuracy = 0.979200, Test set accuracy = 0.948600
 epoch 10/100. Took 3.3281 seconds. 
  Full-batch training loss = 0.082823, test loss = 0.172314
  Training set accuracy = 0.981400, Test set accuracy = 0.949600
 epoch 11/100. Took 3.2828 seconds. 
  Full-batch training loss = 0.074425, test loss = 0.171813
  Training set accuracy = 0.984000, Test set accuracy = 0.949900
 epoch 12/100. Took 3.3389 seconds. 
  Full-batch training loss = 0.067529, test loss = 0.168096
  Training set accuracy = 0.985900, Test set accuracy = 0.950600
 epoch 13/100. Took 3.2616 seconds. 
  Full-batch training loss = 0.063098, test loss = 0.169518
  Training set accuracy = 0.987300, Test set accuracy = 0.951600
 epoch 14/100. Took 3.354 seconds. 
  Full-batch training loss = 0.057212, test loss = 0.167681
  Training set accuracy = 0.990000, Test set accuracy = 0.951700
 epoch 15/100. Took 3.3493 seconds. 
  Full-batch training loss = 0.050578, test loss = 0.163936
  Training set accuracy = 0.991500, Test set accuracy = 0.953000
 epoch 16/100. Took 3.373 seconds. 
  Full-batch training loss = 0.046551, test loss = 0.161008
  Training set accuracy = 0.992600, Test set accuracy = 0.953200
 epoch 17/100. Took 3.3492 seconds. 
  Full-batch training loss = 0.042036, test loss = 0.160789
  Training set accuracy = 0.994100, Test set accuracy = 0.954200
 epoch 18/100. Took 3.318 seconds. 
  Full-batch training loss = 0.038033, test loss = 0.161170
  Training set accuracy = 0.994900, Test set accuracy = 0.953700
 epoch 19/100. Took 3.3011 seconds. 
  Full-batch training loss = 0.035615, test loss = 0.160738
  Training set accuracy = 0.995500, Test set accuracy = 0.954300
 epoch 20/100. Took 3.2256 seconds. 
  Full-batch training loss = 0.031667, test loss = 0.158608
  Training set accuracy = 0.996200, Test set accuracy = 0.955100
 epoch 21/100. Took 3.3273 seconds. 
  Full-batch training loss = 0.030218, test loss = 0.160684
  Training set accuracy = 0.996800, Test set accuracy = 0.954700
 epoch 22/100. Took 3.3268 seconds. 
  Full-batch training loss = 0.027465, test loss = 0.157973
  Training set accuracy = 0.997600, Test set accuracy = 0.955600
 epoch 23/100. Took 3.7107 seconds. 
  Full-batch training loss = 0.025731, test loss = 0.159076
  Training set accuracy = 0.997600, Test set accuracy = 0.956200
 epoch 24/100. Took 3.3384 seconds. 
  Full-batch training loss = 0.023422, test loss = 0.158243
  Training set accuracy = 0.998000, Test set accuracy = 0.956300
 epoch 25/100. Took 3.3133 seconds. 
  Full-batch training loss = 0.021650, test loss = 0.158877
  Training set accuracy = 0.998300, Test set accuracy = 0.956200
 epoch 26/100. Took 3.3429 seconds. 
  Full-batch training loss = 0.020393, test loss = 0.158401
  Training set accuracy = 0.998700, Test set accuracy = 0.956300
 epoch 27/100. Took 3.3287 seconds. 
  Full-batch training loss = 0.018981, test loss = 0.158889
  Training set accuracy = 0.998700, Test set accuracy = 0.956400
 epoch 28/100. Took 3.8225 seconds. 
  Full-batch training loss = 0.017533, test loss = 0.158964
  Training set accuracy = 0.999000, Test set accuracy = 0.955900
 epoch 29/100. Took 3.849 seconds. 
  Full-batch training loss = 0.016678, test loss = 0.158009
  Training set accuracy = 0.999100, Test set accuracy = 0.957200
 epoch 30/100. Took 3.8323 seconds. 
  Full-batch training loss = 0.015458, test loss = 0.158852
  Training set accuracy = 0.999300, Test set accuracy = 0.956800
 epoch 31/100. Took 3.7868 seconds. 
  Full-batch training loss = 0.014672, test loss = 0.160074
  Training set accuracy = 0.999400, Test set accuracy = 0.956100
 epoch 32/100. Took 3.8407 seconds. 
  Full-batch training loss = 0.013793, test loss = 0.159547
  Training set accuracy = 0.999500, Test set accuracy = 0.956700
 epoch 33/100. Took 3.8098 seconds. 
  Full-batch training loss = 0.013130, test loss = 0.159526
  Training set accuracy = 0.999400, Test set accuracy = 0.956700
 epoch 34/100. Took 3.8463 seconds. 
  Full-batch training loss = 0.012431, test loss = 0.159303
  Training set accuracy = 0.999600, Test set accuracy = 0.956700
 epoch 35/100. Took 3.8185 seconds. 
  Full-batch training loss = 0.011951, test loss = 0.159290
  Training set accuracy = 0.999600, Test set accuracy = 0.956400
 epoch 36/100. Took 3.8152 seconds. 
  Full-batch training loss = 0.011026, test loss = 0.159907
  Training set accuracy = 0.999700, Test set accuracy = 0.957200
 epoch 37/100. Took 3.9423 seconds. 
  Full-batch training loss = 0.010446, test loss = 0.159801
  Training set accuracy = 0.999800, Test set accuracy = 0.957300
 epoch 38/100. Took 3.8804 seconds. 
  Full-batch training loss = 0.009946, test loss = 0.159961
  Training set accuracy = 0.999800, Test set accuracy = 0.957200
 epoch 39/100. Took 3.8166 seconds. 
  Full-batch training loss = 0.009619, test loss = 0.160800
  Training set accuracy = 0.999900, Test set accuracy = 0.957100
 epoch 40/100. Took 3.8174 seconds. 
  Full-batch training loss = 0.009088, test loss = 0.160998
  Training set accuracy = 0.999900, Test set accuracy = 0.957000
 epoch 41/100. Took 3.7883 seconds. 
  Full-batch training loss = 0.008786, test loss = 0.160876
  Training set accuracy = 0.999900, Test set accuracy = 0.957000
 epoch 42/100. Took 3.7791 seconds. 
  Full-batch training loss = 0.008416, test loss = 0.161682
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 43/100. Took 3.8254 seconds. 
  Full-batch training loss = 0.008105, test loss = 0.161438
  Training set accuracy = 0.999900, Test set accuracy = 0.956800
 epoch 44/100. Took 3.8223 seconds. 
  Full-batch training loss = 0.007704, test loss = 0.161722
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 45/100. Took 3.8331 seconds. 
  Full-batch training loss = 0.007443, test loss = 0.162367
  Training set accuracy = 1.000000, Test set accuracy = 0.957000
 epoch 46/100. Took 3.8291 seconds. 
  Full-batch training loss = 0.007172, test loss = 0.162316
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 47/100. Took 3.8821 seconds. 
  Full-batch training loss = 0.006940, test loss = 0.162492
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 48/100. Took 3.8054 seconds. 
  Full-batch training loss = 0.006742, test loss = 0.163020
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 49/100. Took 3.7994 seconds. 
  Full-batch training loss = 0.006466, test loss = 0.162501
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 50/100. Took 3.7281 seconds. 
  Full-batch training loss = 0.006278, test loss = 0.163047
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 51/100. Took 3.6643 seconds. 
  Full-batch training loss = 0.006051, test loss = 0.164229
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 52/100. Took 3.7274 seconds. 
  Full-batch training loss = 0.005845, test loss = 0.163890
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 53/100. Took 3.7399 seconds. 
  Full-batch training loss = 0.005664, test loss = 0.163876
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 54/100. Took 3.738 seconds. 
  Full-batch training loss = 0.005517, test loss = 0.164155
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 55/100. Took 3.7119 seconds. 
  Full-batch training loss = 0.005353, test loss = 0.164359
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 56/100. Took 3.7779 seconds. 
  Full-batch training loss = 0.005228, test loss = 0.164706
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 57/100. Took 3.7668 seconds. 
  Full-batch training loss = 0.005064, test loss = 0.164526
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 58/100. Took 3.5956 seconds. 
  Full-batch training loss = 0.004916, test loss = 0.164927
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 59/100. Took 3.6056 seconds. 
  Full-batch training loss = 0.004795, test loss = 0.165315
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 60/100. Took 3.7937 seconds. 
  Full-batch training loss = 0.004680, test loss = 0.165533
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 61/100. Took 3.7521 seconds. 
  Full-batch training loss = 0.004562, test loss = 0.165429
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 62/100. Took 3.7672 seconds. 
  Full-batch training loss = 0.004460, test loss = 0.165320
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 63/100. Took 3.7571 seconds. 
  Full-batch training loss = 0.004333, test loss = 0.166162
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 64/100. Took 3.8683 seconds. 
  Full-batch training loss = 0.004254, test loss = 0.165893
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 65/100. Took 3.8135 seconds. 
  Full-batch training loss = 0.004136, test loss = 0.166544
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 66/100. Took 3.7263 seconds. 
  Full-batch training loss = 0.004039, test loss = 0.166358
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 67/100. Took 3.709 seconds. 
  Full-batch training loss = 0.003948, test loss = 0.166710
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 68/100. Took 3.7441 seconds. 
  Full-batch training loss = 0.003856, test loss = 0.167057
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 69/100. Took 3.7041 seconds. 
  Full-batch training loss = 0.003777, test loss = 0.166987
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 70/100. Took 3.6751 seconds. 
  Full-batch training loss = 0.003701, test loss = 0.167226
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 71/100. Took 3.7133 seconds. 
  Full-batch training loss = 0.003618, test loss = 0.167549
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 72/100. Took 3.7804 seconds. 
  Full-batch training loss = 0.003547, test loss = 0.167769
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 73/100. Took 3.707 seconds. 
  Full-batch training loss = 0.003487, test loss = 0.167562
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 74/100. Took 3.6981 seconds. 
  Full-batch training loss = 0.003406, test loss = 0.167903
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 75/100. Took 3.7053 seconds. 
  Full-batch training loss = 0.003340, test loss = 0.168087
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 76/100. Took 4.4584 seconds. 
  Full-batch training loss = 0.003275, test loss = 0.168302
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 77/100. Took 3.7077 seconds. 
  Full-batch training loss = 0.003217, test loss = 0.168614
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 78/100. Took 3.7336 seconds. 
  Full-batch training loss = 0.003157, test loss = 0.168513
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 79/100. Took 3.7197 seconds. 
  Full-batch training loss = 0.003106, test loss = 0.168832
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 80/100. Took 3.6781 seconds. 
  Full-batch training loss = 0.003042, test loss = 0.168799
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 81/100. Took 3.7186 seconds. 
  Full-batch training loss = 0.002988, test loss = 0.169243
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 82/100. Took 3.7516 seconds. 
  Full-batch training loss = 0.002938, test loss = 0.169380
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 83/100. Took 3.7369 seconds. 
  Full-batch training loss = 0.002880, test loss = 0.169172
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 84/100. Took 3.7025 seconds. 
  Full-batch training loss = 0.002832, test loss = 0.169499
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 85/100. Took 3.7184 seconds. 
  Full-batch training loss = 0.002785, test loss = 0.169684
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 86/100. Took 3.7094 seconds. 
  Full-batch training loss = 0.002744, test loss = 0.169537
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 87/100. Took 3.6838 seconds. 
  Full-batch training loss = 0.002694, test loss = 0.169837
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 88/100. Took 3.6869 seconds. 
  Full-batch training loss = 0.002653, test loss = 0.170287
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 89/100. Took 3.7047 seconds. 
  Full-batch training loss = 0.002609, test loss = 0.170252
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 90/100. Took 3.7366 seconds. 
  Full-batch training loss = 0.002568, test loss = 0.170585
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 91/100. Took 3.7709 seconds. 
  Full-batch training loss = 0.002528, test loss = 0.170481
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 92/100. Took 5.2706 seconds. 
  Full-batch training loss = 0.002489, test loss = 0.170715
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 93/100. Took 3.741 seconds. 
  Full-batch training loss = 0.002453, test loss = 0.170839
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 94/100. Took 3.7422 seconds. 
  Full-batch training loss = 0.002421, test loss = 0.171116
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 95/100. Took 3.7795 seconds. 
  Full-batch training loss = 0.002381, test loss = 0.170910
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 96/100. Took 3.7678 seconds. 
  Full-batch training loss = 0.002346, test loss = 0.171207
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 97/100. Took 3.773 seconds. 
  Full-batch training loss = 0.002311, test loss = 0.171498
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 98/100. Took 3.7625 seconds. 
  Full-batch training loss = 0.002279, test loss = 0.171564
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 99/100. Took 3.7442 seconds. 
  Full-batch training loss = 0.002253, test loss = 0.171620
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 100/100. Took 3.7409 seconds. 
  Full-batch training loss = 0.002219, test loss = 0.171788
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
Elapsed time is 905.861456 seconds.
End Training
