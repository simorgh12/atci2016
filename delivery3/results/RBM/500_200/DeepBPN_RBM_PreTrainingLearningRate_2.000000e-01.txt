Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (500  200)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 3.617 seconds. Average reconstruction error is: 27.2507
 epoch 2/20. Took 3.6616 seconds. Average reconstruction error is: 15.9435
 epoch 3/20. Took 3.6529 seconds. Average reconstruction error is: 13.7528
 epoch 4/20. Took 3.6652 seconds. Average reconstruction error is: 12.3939
 epoch 5/20. Took 3.6197 seconds. Average reconstruction error is: 11.5033
 epoch 6/20. Took 3.5865 seconds. Average reconstruction error is: 10.8597
 epoch 7/20. Took 3.6101 seconds. Average reconstruction error is: 10.3617
 epoch 8/20. Took 3.6305 seconds. Average reconstruction error is: 9.9951
 epoch 9/20. Took 3.6636 seconds. Average reconstruction error is: 9.763
 epoch 10/20. Took 3.6048 seconds. Average reconstruction error is: 9.5804
 epoch 11/20. Took 3.5799 seconds. Average reconstruction error is: 9.3644
 epoch 12/20. Took 3.7476 seconds. Average reconstruction error is: 9.2165
 epoch 13/20. Took 3.6336 seconds. Average reconstruction error is: 9.092
 epoch 14/20. Took 3.5958 seconds. Average reconstruction error is: 8.9745
 epoch 15/20. Took 3.5736 seconds. Average reconstruction error is: 8.8852
 epoch 16/20. Took 3.4895 seconds. Average reconstruction error is: 8.7921
 epoch 17/20. Took 3.5188 seconds. Average reconstruction error is: 8.6976
 epoch 18/20. Took 3.583 seconds. Average reconstruction error is: 8.6428
 epoch 19/20. Took 3.6379 seconds. Average reconstruction error is: 8.56
 epoch 20/20. Took 3.574 seconds. Average reconstruction error is: 8.5141
Training binary-binary RBM in layer 2 (500  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.1364 seconds. Average reconstruction error is: 19.5575
 epoch 2/20. Took 1.0828 seconds. Average reconstruction error is: 11.4946
 epoch 3/20. Took 1.1453 seconds. Average reconstruction error is: 10.1836
 epoch 4/20. Took 1.1422 seconds. Average reconstruction error is: 9.4679
 epoch 5/20. Took 1.1124 seconds. Average reconstruction error is: 9.029
 epoch 6/20. Took 1.1264 seconds. Average reconstruction error is: 8.7016
 epoch 7/20. Took 1.13 seconds. Average reconstruction error is: 8.4219
 epoch 8/20. Took 1.1125 seconds. Average reconstruction error is: 8.2275
 epoch 9/20. Took 1.0996 seconds. Average reconstruction error is: 8.0415
 epoch 10/20. Took 1.0662 seconds. Average reconstruction error is: 7.8821
 epoch 11/20. Took 1.155 seconds. Average reconstruction error is: 7.7527
 epoch 12/20. Took 1.1206 seconds. Average reconstruction error is: 7.6064
 epoch 13/20. Took 1.1358 seconds. Average reconstruction error is: 7.5115
 epoch 14/20. Took 1.1473 seconds. Average reconstruction error is: 7.4127
 epoch 15/20. Took 1.1562 seconds. Average reconstruction error is: 7.2951
 epoch 16/20. Took 1.1445 seconds. Average reconstruction error is: 7.2031
 epoch 17/20. Took 1.1429 seconds. Average reconstruction error is: 7.1447
 epoch 18/20. Took 1.1556 seconds. Average reconstruction error is: 7.0309
 epoch 19/20. Took 1.1492 seconds. Average reconstruction error is: 6.9813
 epoch 20/20. Took 1.1115 seconds. Average reconstruction error is: 6.9214
Training NN  (784  500  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 2.5 seconds. 
  Full-batch training loss = 0.295681, test loss = 0.300710
  Training set accuracy = 0.916400, Test set accuracy = 0.912900
 epoch 2/100. Took 2.5289 seconds. 
  Full-batch training loss = 0.238498, test loss = 0.259592
  Training set accuracy = 0.930300, Test set accuracy = 0.923600
 epoch 3/100. Took 2.5231 seconds. 
  Full-batch training loss = 0.197343, test loss = 0.230939
  Training set accuracy = 0.946600, Test set accuracy = 0.934400
 epoch 4/100. Took 2.4896 seconds. 
  Full-batch training loss = 0.179055, test loss = 0.225877
  Training set accuracy = 0.949500, Test set accuracy = 0.933200
 epoch 5/100. Took 2.4613 seconds. 
  Full-batch training loss = 0.154546, test loss = 0.207489
  Training set accuracy = 0.958900, Test set accuracy = 0.941500
 epoch 6/100. Took 2.5446 seconds. 
  Full-batch training loss = 0.132588, test loss = 0.191128
  Training set accuracy = 0.964900, Test set accuracy = 0.946100
 epoch 7/100. Took 2.5489 seconds. 
  Full-batch training loss = 0.118362, test loss = 0.185968
  Training set accuracy = 0.969300, Test set accuracy = 0.947100
 epoch 8/100. Took 2.5314 seconds. 
  Full-batch training loss = 0.112776, test loss = 0.187490
  Training set accuracy = 0.969700, Test set accuracy = 0.946900
 epoch 9/100. Took 2.5304 seconds. 
  Full-batch training loss = 0.095575, test loss = 0.174524
  Training set accuracy = 0.976800, Test set accuracy = 0.949600
 epoch 10/100. Took 2.4926 seconds. 
  Full-batch training loss = 0.085973, test loss = 0.172853
  Training set accuracy = 0.981400, Test set accuracy = 0.951000
 epoch 11/100. Took 2.4955 seconds. 
  Full-batch training loss = 0.076794, test loss = 0.166903
  Training set accuracy = 0.982900, Test set accuracy = 0.951500
 epoch 12/100. Took 2.5116 seconds. 
  Full-batch training loss = 0.068613, test loss = 0.164328
  Training set accuracy = 0.986200, Test set accuracy = 0.953000
 epoch 13/100. Took 2.5102 seconds. 
  Full-batch training loss = 0.064214, test loss = 0.165774
  Training set accuracy = 0.987100, Test set accuracy = 0.951400
 epoch 14/100. Took 2.5254 seconds. 
  Full-batch training loss = 0.057957, test loss = 0.162738
  Training set accuracy = 0.988400, Test set accuracy = 0.953500
 epoch 15/100. Took 2.5294 seconds. 
  Full-batch training loss = 0.052065, test loss = 0.160824
  Training set accuracy = 0.990800, Test set accuracy = 0.952600
 epoch 16/100. Took 2.4995 seconds. 
  Full-batch training loss = 0.047631, test loss = 0.160796
  Training set accuracy = 0.991500, Test set accuracy = 0.953400
 epoch 17/100. Took 2.5424 seconds. 
  Full-batch training loss = 0.042894, test loss = 0.157559
  Training set accuracy = 0.993700, Test set accuracy = 0.953800
 epoch 18/100. Took 2.5107 seconds. 
  Full-batch training loss = 0.038546, test loss = 0.155474
  Training set accuracy = 0.994600, Test set accuracy = 0.955100
 epoch 19/100. Took 2.54 seconds. 
  Full-batch training loss = 0.037816, test loss = 0.157360
  Training set accuracy = 0.995100, Test set accuracy = 0.955600
 epoch 20/100. Took 2.5281 seconds. 
  Full-batch training loss = 0.032890, test loss = 0.156664
  Training set accuracy = 0.996600, Test set accuracy = 0.956100
 epoch 21/100. Took 2.5459 seconds. 
  Full-batch training loss = 0.030362, test loss = 0.157785
  Training set accuracy = 0.997100, Test set accuracy = 0.954800
 epoch 22/100. Took 2.5837 seconds. 
  Full-batch training loss = 0.027603, test loss = 0.154725
  Training set accuracy = 0.997600, Test set accuracy = 0.955400
 epoch 23/100. Took 2.5066 seconds. 
  Full-batch training loss = 0.025469, test loss = 0.155630
  Training set accuracy = 0.997700, Test set accuracy = 0.956200
 epoch 24/100. Took 2.5164 seconds. 
  Full-batch training loss = 0.023525, test loss = 0.154549
  Training set accuracy = 0.998000, Test set accuracy = 0.955800
 epoch 25/100. Took 2.5649 seconds. 
  Full-batch training loss = 0.021560, test loss = 0.155393
  Training set accuracy = 0.998600, Test set accuracy = 0.956700
 epoch 26/100. Took 2.4874 seconds. 
  Full-batch training loss = 0.020177, test loss = 0.156253
  Training set accuracy = 0.999000, Test set accuracy = 0.955900
 epoch 27/100. Took 2.4777 seconds. 
  Full-batch training loss = 0.019033, test loss = 0.156543
  Training set accuracy = 0.999100, Test set accuracy = 0.955100
 epoch 28/100. Took 2.485 seconds. 
  Full-batch training loss = 0.017605, test loss = 0.157348
  Training set accuracy = 0.999300, Test set accuracy = 0.956400
 epoch 29/100. Took 2.5198 seconds. 
  Full-batch training loss = 0.016753, test loss = 0.155518
  Training set accuracy = 0.999100, Test set accuracy = 0.957000
 epoch 30/100. Took 2.5271 seconds. 
  Full-batch training loss = 0.015362, test loss = 0.155997
  Training set accuracy = 0.999400, Test set accuracy = 0.956100
 epoch 31/100. Took 2.5127 seconds. 
  Full-batch training loss = 0.014408, test loss = 0.156798
  Training set accuracy = 0.999500, Test set accuracy = 0.956400
 epoch 32/100. Took 2.496 seconds. 
  Full-batch training loss = 0.013685, test loss = 0.155784
  Training set accuracy = 0.999500, Test set accuracy = 0.957000
 epoch 33/100. Took 2.5517 seconds. 
  Full-batch training loss = 0.012891, test loss = 0.157168
  Training set accuracy = 0.999500, Test set accuracy = 0.956700
 epoch 34/100. Took 2.4828 seconds. 
  Full-batch training loss = 0.012176, test loss = 0.156862
  Training set accuracy = 0.999600, Test set accuracy = 0.957100
 epoch 35/100. Took 2.5286 seconds. 
  Full-batch training loss = 0.011506, test loss = 0.156794
  Training set accuracy = 0.999600, Test set accuracy = 0.956900
 epoch 36/100. Took 2.5399 seconds. 
  Full-batch training loss = 0.011065, test loss = 0.156754
  Training set accuracy = 0.999700, Test set accuracy = 0.956500
 epoch 37/100. Took 2.5903 seconds. 
  Full-batch training loss = 0.010476, test loss = 0.157755
  Training set accuracy = 0.999700, Test set accuracy = 0.955900
 epoch 38/100. Took 2.5795 seconds. 
  Full-batch training loss = 0.010122, test loss = 0.158397
  Training set accuracy = 0.999800, Test set accuracy = 0.957500
 epoch 39/100. Took 2.5761 seconds. 
  Full-batch training loss = 0.009659, test loss = 0.157531
  Training set accuracy = 0.999800, Test set accuracy = 0.956800
 epoch 40/100. Took 2.534 seconds. 
  Full-batch training loss = 0.009268, test loss = 0.159545
  Training set accuracy = 0.999900, Test set accuracy = 0.956400
 epoch 41/100. Took 2.5847 seconds. 
  Full-batch training loss = 0.008697, test loss = 0.158303
  Training set accuracy = 0.999800, Test set accuracy = 0.957400
 epoch 42/100. Took 2.5462 seconds. 
  Full-batch training loss = 0.008333, test loss = 0.158617
  Training set accuracy = 0.999800, Test set accuracy = 0.957300
 epoch 43/100. Took 2.5205 seconds. 
  Full-batch training loss = 0.008020, test loss = 0.158274
  Training set accuracy = 0.999900, Test set accuracy = 0.957800
 epoch 44/100. Took 2.5513 seconds. 
  Full-batch training loss = 0.007673, test loss = 0.158634
  Training set accuracy = 1.000000, Test set accuracy = 0.957100
 epoch 45/100. Took 2.5161 seconds. 
  Full-batch training loss = 0.007404, test loss = 0.159470
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 46/100. Took 2.5508 seconds. 
  Full-batch training loss = 0.007123, test loss = 0.159390
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 47/100. Took 2.5261 seconds. 
  Full-batch training loss = 0.006839, test loss = 0.159503
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 48/100. Took 2.5159 seconds. 
  Full-batch training loss = 0.006625, test loss = 0.160000
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 49/100. Took 2.5049 seconds. 
  Full-batch training loss = 0.006417, test loss = 0.160150
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 50/100. Took 2.5349 seconds. 
  Full-batch training loss = 0.006216, test loss = 0.160827
  Training set accuracy = 1.000000, Test set accuracy = 0.957200
 epoch 51/100. Took 2.5348 seconds. 
  Full-batch training loss = 0.005990, test loss = 0.160722
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 52/100. Took 2.5086 seconds. 
  Full-batch training loss = 0.005786, test loss = 0.160567
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 53/100. Took 2.5427 seconds. 
  Full-batch training loss = 0.005638, test loss = 0.161038
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 54/100. Took 2.485 seconds. 
  Full-batch training loss = 0.005445, test loss = 0.161299
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 55/100. Took 2.5036 seconds. 
  Full-batch training loss = 0.005293, test loss = 0.161274
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 56/100. Took 2.5528 seconds. 
  Full-batch training loss = 0.005159, test loss = 0.161885
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 57/100. Took 2.5938 seconds. 
  Full-batch training loss = 0.004989, test loss = 0.161552
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 58/100. Took 2.581 seconds. 
  Full-batch training loss = 0.004860, test loss = 0.162372
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 59/100. Took 2.5237 seconds. 
  Full-batch training loss = 0.004745, test loss = 0.162493
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 60/100. Took 2.517 seconds. 
  Full-batch training loss = 0.004615, test loss = 0.162130
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 61/100. Took 2.5613 seconds. 
  Full-batch training loss = 0.004490, test loss = 0.162517
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 62/100. Took 2.5333 seconds. 
  Full-batch training loss = 0.004381, test loss = 0.163074
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 63/100. Took 2.5316 seconds. 
  Full-batch training loss = 0.004262, test loss = 0.162895
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 64/100. Took 2.5125 seconds. 
  Full-batch training loss = 0.004160, test loss = 0.163071
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 65/100. Took 2.5008 seconds. 
  Full-batch training loss = 0.004070, test loss = 0.163524
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 66/100. Took 2.5445 seconds. 
  Full-batch training loss = 0.003978, test loss = 0.163871
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 67/100. Took 2.5097 seconds. 
  Full-batch training loss = 0.003880, test loss = 0.163687
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 68/100. Took 2.5103 seconds. 
  Full-batch training loss = 0.003796, test loss = 0.164164
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 69/100. Took 2.564 seconds. 
  Full-batch training loss = 0.003720, test loss = 0.164080
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 70/100. Took 2.5982 seconds. 
  Full-batch training loss = 0.003631, test loss = 0.164073
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 71/100. Took 2.555 seconds. 
  Full-batch training loss = 0.003555, test loss = 0.164118
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 72/100. Took 2.5554 seconds. 
  Full-batch training loss = 0.003475, test loss = 0.164411
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 73/100. Took 2.5329 seconds. 
  Full-batch training loss = 0.003411, test loss = 0.164717
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 74/100. Took 2.5286 seconds. 
  Full-batch training loss = 0.003343, test loss = 0.164912
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 75/100. Took 2.4667 seconds. 
  Full-batch training loss = 0.003274, test loss = 0.165313
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 76/100. Took 2.4963 seconds. 
  Full-batch training loss = 0.003209, test loss = 0.165365
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 77/100. Took 2.5348 seconds. 
  Full-batch training loss = 0.003150, test loss = 0.165354
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 78/100. Took 2.5629 seconds. 
  Full-batch training loss = 0.003096, test loss = 0.165630
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 79/100. Took 2.5156 seconds. 
  Full-batch training loss = 0.003033, test loss = 0.165830
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 80/100. Took 2.5135 seconds. 
  Full-batch training loss = 0.002981, test loss = 0.166084
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 81/100. Took 2.5127 seconds. 
  Full-batch training loss = 0.002938, test loss = 0.166007
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 82/100. Took 2.5458 seconds. 
  Full-batch training loss = 0.002874, test loss = 0.166056
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 83/100. Took 2.5435 seconds. 
  Full-batch training loss = 0.002821, test loss = 0.166441
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 84/100. Took 2.5246 seconds. 
  Full-batch training loss = 0.002775, test loss = 0.166335
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 85/100. Took 2.5815 seconds. 
  Full-batch training loss = 0.002727, test loss = 0.166924
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 86/100. Took 2.5323 seconds. 
  Full-batch training loss = 0.002687, test loss = 0.166879
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 87/100. Took 2.5129 seconds. 
  Full-batch training loss = 0.002636, test loss = 0.167097
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 88/100. Took 2.5194 seconds. 
  Full-batch training loss = 0.002595, test loss = 0.167289
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 89/100. Took 2.5136 seconds. 
  Full-batch training loss = 0.002553, test loss = 0.167384
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 90/100. Took 2.5773 seconds. 
  Full-batch training loss = 0.002512, test loss = 0.167391
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 91/100. Took 2.5301 seconds. 
  Full-batch training loss = 0.002470, test loss = 0.167416
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 92/100. Took 2.5408 seconds. 
  Full-batch training loss = 0.002433, test loss = 0.167727
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 93/100. Took 2.4991 seconds. 
  Full-batch training loss = 0.002395, test loss = 0.167805
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 94/100. Took 2.5435 seconds. 
  Full-batch training loss = 0.002359, test loss = 0.167983
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 95/100. Took 2.5325 seconds. 
  Full-batch training loss = 0.002328, test loss = 0.168139
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 96/100. Took 2.5415 seconds. 
  Full-batch training loss = 0.002290, test loss = 0.168277
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 97/100. Took 2.5283 seconds. 
  Full-batch training loss = 0.002258, test loss = 0.168441
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 98/100. Took 2.5743 seconds. 
  Full-batch training loss = 0.002226, test loss = 0.168531
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 99/100. Took 2.501 seconds. 
  Full-batch training loss = 0.002196, test loss = 0.168878
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 100/100. Took 2.5263 seconds. 
  Full-batch training loss = 0.002162, test loss = 0.168861
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
Elapsed time is 683.975496 seconds.
End Training
