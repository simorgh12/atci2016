Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (500  200)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 3.5909 seconds. Average reconstruction error is: 27.1053
 epoch 2/20. Took 3.6877 seconds. Average reconstruction error is: 15.9725
 epoch 3/20. Took 3.5727 seconds. Average reconstruction error is: 13.7984
 epoch 4/20. Took 3.5971 seconds. Average reconstruction error is: 12.5154
 epoch 5/20. Took 3.4466 seconds. Average reconstruction error is: 11.5621
 epoch 6/20. Took 3.6279 seconds. Average reconstruction error is: 10.9026
 epoch 7/20. Took 3.5986 seconds. Average reconstruction error is: 10.4434
 epoch 8/20. Took 3.5502 seconds. Average reconstruction error is: 10.1055
 epoch 9/20. Took 3.4528 seconds. Average reconstruction error is: 9.8209
 epoch 10/20. Took 3.4809 seconds. Average reconstruction error is: 9.5903
 epoch 11/20. Took 3.539 seconds. Average reconstruction error is: 9.3999
 epoch 12/20. Took 3.5827 seconds. Average reconstruction error is: 9.2475
 epoch 13/20. Took 3.5204 seconds. Average reconstruction error is: 9.1643
 epoch 14/20. Took 3.6883 seconds. Average reconstruction error is: 9.0155
 epoch 15/20. Took 3.4799 seconds. Average reconstruction error is: 8.8643
 epoch 16/20. Took 3.7183 seconds. Average reconstruction error is: 8.7976
 epoch 17/20. Took 3.6015 seconds. Average reconstruction error is: 8.7542
 epoch 18/20. Took 3.5648 seconds. Average reconstruction error is: 8.6797
 epoch 19/20. Took 3.5701 seconds. Average reconstruction error is: 8.6099
 epoch 20/20. Took 3.6127 seconds. Average reconstruction error is: 8.5421
Training binary-binary RBM in layer 2 (500  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.1013 seconds. Average reconstruction error is: 19.4359
 epoch 2/20. Took 1.0766 seconds. Average reconstruction error is: 11.3717
 epoch 3/20. Took 1.1105 seconds. Average reconstruction error is: 10.0392
 epoch 4/20. Took 1.0561 seconds. Average reconstruction error is: 9.4212
 epoch 5/20. Took 1.1071 seconds. Average reconstruction error is: 8.9735
 epoch 6/20. Took 1.1051 seconds. Average reconstruction error is: 8.6534
 epoch 7/20. Took 1.1265 seconds. Average reconstruction error is: 8.43
 epoch 8/20. Took 1.1238 seconds. Average reconstruction error is: 8.2022
 epoch 9/20. Took 1.1113 seconds. Average reconstruction error is: 8.0087
 epoch 10/20. Took 1.139 seconds. Average reconstruction error is: 7.8375
 epoch 11/20. Took 1.0635 seconds. Average reconstruction error is: 7.7059
 epoch 12/20. Took 1.1241 seconds. Average reconstruction error is: 7.5558
 epoch 13/20. Took 1.1926 seconds. Average reconstruction error is: 7.4279
 epoch 14/20. Took 1.1153 seconds. Average reconstruction error is: 7.3236
 epoch 15/20. Took 1.0805 seconds. Average reconstruction error is: 7.2139
 epoch 16/20. Took 1.169 seconds. Average reconstruction error is: 7.1083
 epoch 17/20. Took 1.0845 seconds. Average reconstruction error is: 7.0284
 epoch 18/20. Took 1.1109 seconds. Average reconstruction error is: 6.9255
 epoch 19/20. Took 1.1327 seconds. Average reconstruction error is: 6.8517
 epoch 20/20. Took 1.1588 seconds. Average reconstruction error is: 6.7702
Training NN  (784  500  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 2.5059 seconds. 
  Full-batch training loss = 0.300579, test loss = 0.300270
  Training set accuracy = 0.911900, Test set accuracy = 0.914800
 epoch 2/100. Took 2.4794 seconds. 
  Full-batch training loss = 0.231996, test loss = 0.247976
  Training set accuracy = 0.933900, Test set accuracy = 0.928500
 epoch 3/100. Took 2.5494 seconds. 
  Full-batch training loss = 0.195704, test loss = 0.229271
  Training set accuracy = 0.946300, Test set accuracy = 0.934600
 epoch 4/100. Took 2.4943 seconds. 
  Full-batch training loss = 0.166396, test loss = 0.210114
  Training set accuracy = 0.955500, Test set accuracy = 0.939200
 epoch 5/100. Took 2.5253 seconds. 
  Full-batch training loss = 0.146207, test loss = 0.200200
  Training set accuracy = 0.960600, Test set accuracy = 0.942900
 epoch 6/100. Took 2.5556 seconds. 
  Full-batch training loss = 0.134335, test loss = 0.198176
  Training set accuracy = 0.964200, Test set accuracy = 0.941400
 epoch 7/100. Took 2.5296 seconds. 
  Full-batch training loss = 0.116026, test loss = 0.185301
  Training set accuracy = 0.970400, Test set accuracy = 0.946300
 epoch 8/100. Took 2.514 seconds. 
  Full-batch training loss = 0.104296, test loss = 0.181328
  Training set accuracy = 0.976800, Test set accuracy = 0.946500
 epoch 9/100. Took 2.5326 seconds. 
  Full-batch training loss = 0.093166, test loss = 0.177609
  Training set accuracy = 0.978100, Test set accuracy = 0.948000
 epoch 10/100. Took 2.5715 seconds. 
  Full-batch training loss = 0.082410, test loss = 0.171634
  Training set accuracy = 0.982000, Test set accuracy = 0.950600
 epoch 11/100. Took 2.496 seconds. 
  Full-batch training loss = 0.075078, test loss = 0.168780
  Training set accuracy = 0.985300, Test set accuracy = 0.950200
 epoch 12/100. Took 2.5752 seconds. 
  Full-batch training loss = 0.067184, test loss = 0.165471
  Training set accuracy = 0.987400, Test set accuracy = 0.951800
 epoch 13/100. Took 2.535 seconds. 
  Full-batch training loss = 0.059831, test loss = 0.161081
  Training set accuracy = 0.989300, Test set accuracy = 0.953000
 epoch 14/100. Took 2.522 seconds. 
  Full-batch training loss = 0.055623, test loss = 0.161346
  Training set accuracy = 0.990400, Test set accuracy = 0.953400
 epoch 15/100. Took 2.5462 seconds. 
  Full-batch training loss = 0.048816, test loss = 0.159386
  Training set accuracy = 0.991700, Test set accuracy = 0.954200
 epoch 16/100. Took 2.5021 seconds. 
  Full-batch training loss = 0.044622, test loss = 0.156222
  Training set accuracy = 0.993400, Test set accuracy = 0.954600
 epoch 17/100. Took 2.5681 seconds. 
  Full-batch training loss = 0.041070, test loss = 0.155521
  Training set accuracy = 0.994100, Test set accuracy = 0.955600
 epoch 18/100. Took 2.4953 seconds. 
  Full-batch training loss = 0.036726, test loss = 0.155051
  Training set accuracy = 0.995100, Test set accuracy = 0.956700
 epoch 19/100. Took 2.5252 seconds. 
  Full-batch training loss = 0.033744, test loss = 0.153698
  Training set accuracy = 0.995600, Test set accuracy = 0.957100
 epoch 20/100. Took 2.5092 seconds. 
  Full-batch training loss = 0.031844, test loss = 0.155774
  Training set accuracy = 0.995900, Test set accuracy = 0.955700
 epoch 21/100. Took 2.5577 seconds. 
  Full-batch training loss = 0.028673, test loss = 0.152079
  Training set accuracy = 0.996900, Test set accuracy = 0.957900
 epoch 22/100. Took 2.5737 seconds. 
  Full-batch training loss = 0.026796, test loss = 0.154050
  Training set accuracy = 0.997200, Test set accuracy = 0.956300
 epoch 23/100. Took 2.4981 seconds. 
  Full-batch training loss = 0.023953, test loss = 0.152390
  Training set accuracy = 0.997400, Test set accuracy = 0.957100
 epoch 24/100. Took 2.4835 seconds. 
  Full-batch training loss = 0.022092, test loss = 0.151966
  Training set accuracy = 0.997900, Test set accuracy = 0.956600
 epoch 25/100. Took 2.5339 seconds. 
  Full-batch training loss = 0.020867, test loss = 0.152841
  Training set accuracy = 0.998200, Test set accuracy = 0.957500
 epoch 26/100. Took 2.4977 seconds. 
  Full-batch training loss = 0.019044, test loss = 0.151822
  Training set accuracy = 0.998900, Test set accuracy = 0.956900
 epoch 27/100. Took 2.6828 seconds. 
  Full-batch training loss = 0.017655, test loss = 0.151593
  Training set accuracy = 0.999000, Test set accuracy = 0.957300
 epoch 28/100. Took 2.5214 seconds. 
  Full-batch training loss = 0.016767, test loss = 0.153392
  Training set accuracy = 0.999400, Test set accuracy = 0.957700
 epoch 29/100. Took 2.4979 seconds. 
  Full-batch training loss = 0.015662, test loss = 0.152198
  Training set accuracy = 0.999400, Test set accuracy = 0.958800
 epoch 30/100. Took 2.5221 seconds. 
  Full-batch training loss = 0.014475, test loss = 0.152319
  Training set accuracy = 0.999500, Test set accuracy = 0.956500
 epoch 31/100. Took 2.4906 seconds. 
  Full-batch training loss = 0.013976, test loss = 0.151810
  Training set accuracy = 0.999700, Test set accuracy = 0.958900
 epoch 32/100. Took 2.5677 seconds. 
  Full-batch training loss = 0.013104, test loss = 0.153445
  Training set accuracy = 0.999600, Test set accuracy = 0.958600
 epoch 33/100. Took 2.5649 seconds. 
  Full-batch training loss = 0.012293, test loss = 0.153578
  Training set accuracy = 0.999800, Test set accuracy = 0.958100
 epoch 34/100. Took 2.5581 seconds. 
  Full-batch training loss = 0.011798, test loss = 0.152896
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 35/100. Took 2.6268 seconds. 
  Full-batch training loss = 0.010951, test loss = 0.152500
  Training set accuracy = 0.999900, Test set accuracy = 0.958000
 epoch 36/100. Took 2.5928 seconds. 
  Full-batch training loss = 0.010376, test loss = 0.152955
  Training set accuracy = 0.999900, Test set accuracy = 0.959200
 epoch 37/100. Took 2.6191 seconds. 
  Full-batch training loss = 0.009938, test loss = 0.153830
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 38/100. Took 2.5579 seconds. 
  Full-batch training loss = 0.009479, test loss = 0.154138
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 39/100. Took 2.5881 seconds. 
  Full-batch training loss = 0.009025, test loss = 0.153315
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 40/100. Took 2.5006 seconds. 
  Full-batch training loss = 0.008726, test loss = 0.153401
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 41/100. Took 2.5586 seconds. 
  Full-batch training loss = 0.008315, test loss = 0.154513
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 42/100. Took 2.5552 seconds. 
  Full-batch training loss = 0.008072, test loss = 0.153672
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 43/100. Took 2.5969 seconds. 
  Full-batch training loss = 0.007658, test loss = 0.154308
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 44/100. Took 2.5207 seconds. 
  Full-batch training loss = 0.007380, test loss = 0.154266
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 45/100. Took 2.5092 seconds. 
  Full-batch training loss = 0.007117, test loss = 0.154223
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 46/100. Took 2.5244 seconds. 
  Full-batch training loss = 0.006810, test loss = 0.154566
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 47/100. Took 2.5353 seconds. 
  Full-batch training loss = 0.006615, test loss = 0.154808
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 48/100. Took 2.5106 seconds. 
  Full-batch training loss = 0.006347, test loss = 0.154818
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 49/100. Took 2.5056 seconds. 
  Full-batch training loss = 0.006131, test loss = 0.154791
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 50/100. Took 2.5033 seconds. 
  Full-batch training loss = 0.005941, test loss = 0.155761
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 51/100. Took 2.527 seconds. 
  Full-batch training loss = 0.005785, test loss = 0.155527
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 52/100. Took 2.5597 seconds. 
  Full-batch training loss = 0.005588, test loss = 0.156188
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 53/100. Took 2.5472 seconds. 
  Full-batch training loss = 0.005436, test loss = 0.156373
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 54/100. Took 2.4973 seconds. 
  Full-batch training loss = 0.005253, test loss = 0.156245
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 55/100. Took 2.5139 seconds. 
  Full-batch training loss = 0.005104, test loss = 0.156547
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 56/100. Took 2.5812 seconds. 
  Full-batch training loss = 0.004952, test loss = 0.156165
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 57/100. Took 2.5036 seconds. 
  Full-batch training loss = 0.004828, test loss = 0.157183
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 58/100. Took 2.5484 seconds. 
  Full-batch training loss = 0.004686, test loss = 0.156926
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 59/100. Took 2.5811 seconds. 
  Full-batch training loss = 0.004588, test loss = 0.156574
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 60/100. Took 2.5684 seconds. 
  Full-batch training loss = 0.004473, test loss = 0.156956
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 61/100. Took 2.5398 seconds. 
  Full-batch training loss = 0.004346, test loss = 0.157332
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 62/100. Took 2.5068 seconds. 
  Full-batch training loss = 0.004242, test loss = 0.157247
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 63/100. Took 2.5769 seconds. 
  Full-batch training loss = 0.004136, test loss = 0.157583
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 64/100. Took 2.5324 seconds. 
  Full-batch training loss = 0.004048, test loss = 0.157524
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 65/100. Took 2.5442 seconds. 
  Full-batch training loss = 0.003940, test loss = 0.158026
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 66/100. Took 2.5223 seconds. 
  Full-batch training loss = 0.003856, test loss = 0.158406
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 67/100. Took 2.5276 seconds. 
  Full-batch training loss = 0.003764, test loss = 0.158236
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 68/100. Took 2.5375 seconds. 
  Full-batch training loss = 0.003687, test loss = 0.158542
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 69/100. Took 2.5118 seconds. 
  Full-batch training loss = 0.003624, test loss = 0.158959
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 70/100. Took 2.5207 seconds. 
  Full-batch training loss = 0.003537, test loss = 0.158914
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 71/100. Took 2.53 seconds. 
  Full-batch training loss = 0.003454, test loss = 0.159090
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 72/100. Took 2.531 seconds. 
  Full-batch training loss = 0.003387, test loss = 0.158916
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 73/100. Took 2.5523 seconds. 
  Full-batch training loss = 0.003320, test loss = 0.159202
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 74/100. Took 2.5166 seconds. 
  Full-batch training loss = 0.003250, test loss = 0.159507
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 75/100. Took 2.4989 seconds. 
  Full-batch training loss = 0.003191, test loss = 0.159471
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 76/100. Took 2.5823 seconds. 
  Full-batch training loss = 0.003134, test loss = 0.159687
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 77/100. Took 2.6131 seconds. 
  Full-batch training loss = 0.003069, test loss = 0.159754
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 78/100. Took 2.5155 seconds. 
  Full-batch training loss = 0.003013, test loss = 0.160037
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 79/100. Took 2.5199 seconds. 
  Full-batch training loss = 0.002957, test loss = 0.160089
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 80/100. Took 2.5756 seconds. 
  Full-batch training loss = 0.002904, test loss = 0.160256
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 81/100. Took 2.5251 seconds. 
  Full-batch training loss = 0.002851, test loss = 0.160411
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 82/100. Took 2.5165 seconds. 
  Full-batch training loss = 0.002805, test loss = 0.160512
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 83/100. Took 2.5176 seconds. 
  Full-batch training loss = 0.002753, test loss = 0.160764
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 84/100. Took 2.568 seconds. 
  Full-batch training loss = 0.002709, test loss = 0.160783
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 85/100. Took 2.5501 seconds. 
  Full-batch training loss = 0.002669, test loss = 0.161077
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 86/100. Took 2.5355 seconds. 
  Full-batch training loss = 0.002625, test loss = 0.161051
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 87/100. Took 2.5126 seconds. 
  Full-batch training loss = 0.002579, test loss = 0.160935
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 88/100. Took 2.5446 seconds. 
  Full-batch training loss = 0.002539, test loss = 0.161199
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 89/100. Took 2.5194 seconds. 
  Full-batch training loss = 0.002494, test loss = 0.161483
  Training set accuracy = 1.000000, Test set accuracy = 0.960700
 epoch 90/100. Took 2.4992 seconds. 
  Full-batch training loss = 0.002458, test loss = 0.161478
  Training set accuracy = 1.000000, Test set accuracy = 0.960800
 epoch 91/100. Took 2.606 seconds. 
  Full-batch training loss = 0.002419, test loss = 0.161838
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 92/100. Took 2.514 seconds. 
  Full-batch training loss = 0.002380, test loss = 0.161981
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 93/100. Took 2.507 seconds. 
  Full-batch training loss = 0.002345, test loss = 0.162088
  Training set accuracy = 1.000000, Test set accuracy = 0.960500
 epoch 94/100. Took 2.5147 seconds. 
  Full-batch training loss = 0.002312, test loss = 0.162143
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 95/100. Took 2.5398 seconds. 
  Full-batch training loss = 0.002278, test loss = 0.162353
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 96/100. Took 2.595 seconds. 
  Full-batch training loss = 0.002242, test loss = 0.162361
  Training set accuracy = 1.000000, Test set accuracy = 0.960600
 epoch 97/100. Took 2.5457 seconds. 
  Full-batch training loss = 0.002210, test loss = 0.162597
  Training set accuracy = 1.000000, Test set accuracy = 0.960800
 epoch 98/100. Took 2.5078 seconds. 
  Full-batch training loss = 0.002179, test loss = 0.162580
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 99/100. Took 2.5472 seconds. 
  Full-batch training loss = 0.002149, test loss = 0.162690
  Training set accuracy = 1.000000, Test set accuracy = 0.960900
 epoch 100/100. Took 2.5586 seconds. 
  Full-batch training loss = 0.002120, test loss = 0.162687
  Training set accuracy = 1.000000, Test set accuracy = 0.960700
Elapsed time is 684.622900 seconds.
End Training
