Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.10
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (500  200)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 3.5993 seconds. Average reconstruction error is: 26.8799
 epoch 2/20. Took 3.6904 seconds. Average reconstruction error is: 15.7688
 epoch 3/20. Took 3.79 seconds. Average reconstruction error is: 13.6647
 epoch 4/20. Took 3.7743 seconds. Average reconstruction error is: 12.4514
 epoch 5/20. Took 3.6826 seconds. Average reconstruction error is: 11.5574
 epoch 6/20. Took 3.5901 seconds. Average reconstruction error is: 10.9439
 epoch 7/20. Took 3.6504 seconds. Average reconstruction error is: 10.3956
 epoch 8/20. Took 3.5448 seconds. Average reconstruction error is: 10.0487
 epoch 9/20. Took 3.6426 seconds. Average reconstruction error is: 9.7626
 epoch 10/20. Took 3.6389 seconds. Average reconstruction error is: 9.5832
 epoch 11/20. Took 3.6481 seconds. Average reconstruction error is: 9.3944
 epoch 12/20. Took 3.5864 seconds. Average reconstruction error is: 9.2675
 epoch 13/20. Took 3.7324 seconds. Average reconstruction error is: 9.1297
 epoch 14/20. Took 3.6778 seconds. Average reconstruction error is: 9.0462
 epoch 15/20. Took 3.5736 seconds. Average reconstruction error is: 8.9044
 epoch 16/20. Took 3.5051 seconds. Average reconstruction error is: 8.8284
 epoch 17/20. Took 3.6024 seconds. Average reconstruction error is: 8.7641
 epoch 18/20. Took 3.7001 seconds. Average reconstruction error is: 8.6564
 epoch 19/20. Took 3.7042 seconds. Average reconstruction error is: 8.5814
 epoch 20/20. Took 3.539 seconds. Average reconstruction error is: 8.5373
Training binary-binary RBM in layer 2 (500  200) with CD1 for 20 epochs
 epoch 1/20. Took 1.1208 seconds. Average reconstruction error is: 19.038
 epoch 2/20. Took 1.1762 seconds. Average reconstruction error is: 11.1538
 epoch 3/20. Took 1.1389 seconds. Average reconstruction error is: 9.9412
 epoch 4/20. Took 1.1314 seconds. Average reconstruction error is: 9.304
 epoch 5/20. Took 1.1301 seconds. Average reconstruction error is: 8.881
 epoch 6/20. Took 1.1546 seconds. Average reconstruction error is: 8.5822
 epoch 7/20. Took 1.1428 seconds. Average reconstruction error is: 8.3141
 epoch 8/20. Took 1.1679 seconds. Average reconstruction error is: 8.1306
 epoch 9/20. Took 1.1552 seconds. Average reconstruction error is: 7.9602
 epoch 10/20. Took 1.1215 seconds. Average reconstruction error is: 7.7949
 epoch 11/20. Took 1.1521 seconds. Average reconstruction error is: 7.6672
 epoch 12/20. Took 1.1547 seconds. Average reconstruction error is: 7.5459
 epoch 13/20. Took 1.1415 seconds. Average reconstruction error is: 7.4123
 epoch 14/20. Took 1.1629 seconds. Average reconstruction error is: 7.3262
 epoch 15/20. Took 1.1416 seconds. Average reconstruction error is: 7.2323
 epoch 16/20. Took 1.1319 seconds. Average reconstruction error is: 7.1293
 epoch 17/20. Took 1.1542 seconds. Average reconstruction error is: 7.056
 epoch 18/20. Took 1.1745 seconds. Average reconstruction error is: 6.9539
 epoch 19/20. Took 1.1791 seconds. Average reconstruction error is: 6.8737
 epoch 20/20. Took 1.154 seconds. Average reconstruction error is: 6.8248
Training NN  (784  500  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 2.4842 seconds. 
  Full-batch training loss = 0.360697, test loss = 0.357727
  Training set accuracy = 0.899100, Test set accuracy = 0.902100
 epoch 2/100. Took 2.5898 seconds. 
  Full-batch training loss = 0.289043, test loss = 0.295892
  Training set accuracy = 0.918200, Test set accuracy = 0.917500
 epoch 3/100. Took 2.5091 seconds. 
  Full-batch training loss = 0.248588, test loss = 0.263612
  Training set accuracy = 0.927500, Test set accuracy = 0.924700
 epoch 4/100. Took 2.4861 seconds. 
  Full-batch training loss = 0.226987, test loss = 0.249271
  Training set accuracy = 0.936700, Test set accuracy = 0.928100
 epoch 5/100. Took 2.5403 seconds. 
  Full-batch training loss = 0.210917, test loss = 0.239790
  Training set accuracy = 0.939100, Test set accuracy = 0.930500
 epoch 6/100. Took 2.5236 seconds. 
  Full-batch training loss = 0.190078, test loss = 0.225447
  Training set accuracy = 0.944200, Test set accuracy = 0.933700
 epoch 7/100. Took 2.5688 seconds. 
  Full-batch training loss = 0.175893, test loss = 0.217957
  Training set accuracy = 0.949300, Test set accuracy = 0.936700
 epoch 8/100. Took 2.5573 seconds. 
  Full-batch training loss = 0.163640, test loss = 0.210510
  Training set accuracy = 0.952500, Test set accuracy = 0.938000
 epoch 9/100. Took 2.5678 seconds. 
  Full-batch training loss = 0.155211, test loss = 0.205767
  Training set accuracy = 0.956000, Test set accuracy = 0.938900
 epoch 10/100. Took 2.5277 seconds. 
  Full-batch training loss = 0.145476, test loss = 0.202102
  Training set accuracy = 0.960700, Test set accuracy = 0.940800
 epoch 11/100. Took 2.5292 seconds. 
  Full-batch training loss = 0.137528, test loss = 0.199018
  Training set accuracy = 0.962500, Test set accuracy = 0.941800
 epoch 12/100. Took 2.5176 seconds. 
  Full-batch training loss = 0.126674, test loss = 0.191877
  Training set accuracy = 0.966400, Test set accuracy = 0.943000
 epoch 13/100. Took 2.7603 seconds. 
  Full-batch training loss = 0.119874, test loss = 0.188222
  Training set accuracy = 0.968500, Test set accuracy = 0.944700
 epoch 14/100. Took 2.56 seconds. 
  Full-batch training loss = 0.114008, test loss = 0.187715
  Training set accuracy = 0.970300, Test set accuracy = 0.945000
 epoch 15/100. Took 2.5368 seconds. 
  Full-batch training loss = 0.107337, test loss = 0.182464
  Training set accuracy = 0.973100, Test set accuracy = 0.946300
 epoch 16/100. Took 2.5423 seconds. 
  Full-batch training loss = 0.102620, test loss = 0.181394
  Training set accuracy = 0.974200, Test set accuracy = 0.944400
 epoch 17/100. Took 2.527 seconds. 
  Full-batch training loss = 0.097260, test loss = 0.179460
  Training set accuracy = 0.976500, Test set accuracy = 0.945700
 epoch 18/100. Took 2.5704 seconds. 
  Full-batch training loss = 0.091432, test loss = 0.177601
  Training set accuracy = 0.978600, Test set accuracy = 0.948200
 epoch 19/100. Took 2.5106 seconds. 
  Full-batch training loss = 0.086684, test loss = 0.175006
  Training set accuracy = 0.980200, Test set accuracy = 0.948200
 epoch 20/100. Took 2.4946 seconds. 
  Full-batch training loss = 0.083536, test loss = 0.174633
  Training set accuracy = 0.981000, Test set accuracy = 0.948800
 epoch 21/100. Took 2.5906 seconds. 
  Full-batch training loss = 0.077396, test loss = 0.171075
  Training set accuracy = 0.983100, Test set accuracy = 0.948600
 epoch 22/100. Took 2.5298 seconds. 
  Full-batch training loss = 0.075868, test loss = 0.171199
  Training set accuracy = 0.982400, Test set accuracy = 0.948600
 epoch 23/100. Took 2.5444 seconds. 
  Full-batch training loss = 0.070475, test loss = 0.168718
  Training set accuracy = 0.985200, Test set accuracy = 0.950500
 epoch 24/100. Took 2.5526 seconds. 
  Full-batch training loss = 0.067293, test loss = 0.169260
  Training set accuracy = 0.986400, Test set accuracy = 0.950000
 epoch 25/100. Took 2.5056 seconds. 
  Full-batch training loss = 0.064191, test loss = 0.167962
  Training set accuracy = 0.986500, Test set accuracy = 0.949400
 epoch 26/100. Took 2.5328 seconds. 
  Full-batch training loss = 0.061122, test loss = 0.167979
  Training set accuracy = 0.988600, Test set accuracy = 0.950800
 epoch 27/100. Took 2.5273 seconds. 
  Full-batch training loss = 0.057917, test loss = 0.165442
  Training set accuracy = 0.989900, Test set accuracy = 0.951000
 epoch 28/100. Took 2.494 seconds. 
  Full-batch training loss = 0.055493, test loss = 0.165819
  Training set accuracy = 0.990500, Test set accuracy = 0.951100
 epoch 29/100. Took 2.5901 seconds. 
  Full-batch training loss = 0.052893, test loss = 0.164835
  Training set accuracy = 0.992100, Test set accuracy = 0.951500
 epoch 30/100. Took 2.5449 seconds. 
  Full-batch training loss = 0.050192, test loss = 0.164323
  Training set accuracy = 0.992700, Test set accuracy = 0.951100
 epoch 31/100. Took 2.5417 seconds. 
  Full-batch training loss = 0.047778, test loss = 0.163883
  Training set accuracy = 0.993700, Test set accuracy = 0.951500
 epoch 32/100. Took 2.5681 seconds. 
  Full-batch training loss = 0.045999, test loss = 0.163259
  Training set accuracy = 0.994100, Test set accuracy = 0.951800
 epoch 33/100. Took 2.5729 seconds. 
  Full-batch training loss = 0.045148, test loss = 0.163682
  Training set accuracy = 0.994000, Test set accuracy = 0.952200
 epoch 34/100. Took 2.5277 seconds. 
  Full-batch training loss = 0.041643, test loss = 0.161708
  Training set accuracy = 0.995100, Test set accuracy = 0.952000
 epoch 35/100. Took 2.5632 seconds. 
  Full-batch training loss = 0.040568, test loss = 0.163191
  Training set accuracy = 0.994900, Test set accuracy = 0.951400
 epoch 36/100. Took 2.5647 seconds. 
  Full-batch training loss = 0.038252, test loss = 0.161550
  Training set accuracy = 0.996100, Test set accuracy = 0.952700
 epoch 37/100. Took 2.5476 seconds. 
  Full-batch training loss = 0.036840, test loss = 0.161524
  Training set accuracy = 0.996100, Test set accuracy = 0.952600
 epoch 38/100. Took 2.5194 seconds. 
  Full-batch training loss = 0.035586, test loss = 0.162123
  Training set accuracy = 0.996300, Test set accuracy = 0.953100
 epoch 39/100. Took 2.4989 seconds. 
  Full-batch training loss = 0.033734, test loss = 0.161067
  Training set accuracy = 0.996700, Test set accuracy = 0.952800
 epoch 40/100. Took 2.5518 seconds. 
  Full-batch training loss = 0.032764, test loss = 0.161401
  Training set accuracy = 0.996700, Test set accuracy = 0.952900
 epoch 41/100. Took 2.4985 seconds. 
  Full-batch training loss = 0.031174, test loss = 0.161421
  Training set accuracy = 0.997200, Test set accuracy = 0.953000
 epoch 42/100. Took 2.5354 seconds. 
  Full-batch training loss = 0.029870, test loss = 0.160995
  Training set accuracy = 0.997300, Test set accuracy = 0.953700
 epoch 43/100. Took 2.5542 seconds. 
  Full-batch training loss = 0.028472, test loss = 0.160752
  Training set accuracy = 0.997400, Test set accuracy = 0.953500
 epoch 44/100. Took 2.5515 seconds. 
  Full-batch training loss = 0.027531, test loss = 0.160681
  Training set accuracy = 0.997400, Test set accuracy = 0.953300
 epoch 45/100. Took 2.5459 seconds. 
  Full-batch training loss = 0.026391, test loss = 0.160530
  Training set accuracy = 0.997800, Test set accuracy = 0.953600
 epoch 46/100. Took 2.5723 seconds. 
  Full-batch training loss = 0.025435, test loss = 0.160279
  Training set accuracy = 0.997700, Test set accuracy = 0.954100
 epoch 47/100. Took 2.5656 seconds. 
  Full-batch training loss = 0.024574, test loss = 0.160220
  Training set accuracy = 0.997800, Test set accuracy = 0.954500
 epoch 48/100. Took 2.5874 seconds. 
  Full-batch training loss = 0.023674, test loss = 0.160285
  Training set accuracy = 0.997900, Test set accuracy = 0.954400
 epoch 49/100. Took 2.5447 seconds. 
  Full-batch training loss = 0.022653, test loss = 0.160482
  Training set accuracy = 0.998200, Test set accuracy = 0.954200
 epoch 50/100. Took 2.5354 seconds. 
  Full-batch training loss = 0.021915, test loss = 0.160385
  Training set accuracy = 0.998200, Test set accuracy = 0.954900
 epoch 51/100. Took 2.5306 seconds. 
  Full-batch training loss = 0.021298, test loss = 0.160911
  Training set accuracy = 0.998500, Test set accuracy = 0.954600
 epoch 52/100. Took 2.5545 seconds. 
  Full-batch training loss = 0.020474, test loss = 0.161311
  Training set accuracy = 0.998600, Test set accuracy = 0.953900
 epoch 53/100. Took 2.5338 seconds. 
  Full-batch training loss = 0.019858, test loss = 0.161677
  Training set accuracy = 0.998600, Test set accuracy = 0.953900
 epoch 54/100. Took 2.5326 seconds. 
  Full-batch training loss = 0.019058, test loss = 0.160434
  Training set accuracy = 0.998600, Test set accuracy = 0.955200
 epoch 55/100. Took 2.462 seconds. 
  Full-batch training loss = 0.018360, test loss = 0.160935
  Training set accuracy = 0.999000, Test set accuracy = 0.954700
 epoch 56/100. Took 2.5386 seconds. 
  Full-batch training loss = 0.017763, test loss = 0.160874
  Training set accuracy = 0.998900, Test set accuracy = 0.954800
 epoch 57/100. Took 2.5028 seconds. 
  Full-batch training loss = 0.017216, test loss = 0.160599
  Training set accuracy = 0.999000, Test set accuracy = 0.955300
 epoch 58/100. Took 2.5477 seconds. 
  Full-batch training loss = 0.016671, test loss = 0.160758
  Training set accuracy = 0.999200, Test set accuracy = 0.955200
 epoch 59/100. Took 2.517 seconds. 
  Full-batch training loss = 0.016170, test loss = 0.160810
  Training set accuracy = 0.999500, Test set accuracy = 0.955100
 epoch 60/100. Took 2.5491 seconds. 
  Full-batch training loss = 0.015633, test loss = 0.161192
  Training set accuracy = 0.999300, Test set accuracy = 0.955300
 epoch 61/100. Took 2.6103 seconds. 
  Full-batch training loss = 0.015198, test loss = 0.161042
  Training set accuracy = 0.999600, Test set accuracy = 0.955000
 epoch 62/100. Took 2.5021 seconds. 
  Full-batch training loss = 0.014758, test loss = 0.161451
  Training set accuracy = 0.999600, Test set accuracy = 0.955400
 epoch 63/100. Took 2.5636 seconds. 
  Full-batch training loss = 0.014374, test loss = 0.161938
  Training set accuracy = 0.999700, Test set accuracy = 0.955600
 epoch 64/100. Took 2.5936 seconds. 
  Full-batch training loss = 0.013887, test loss = 0.161579
  Training set accuracy = 0.999600, Test set accuracy = 0.955500
 epoch 65/100. Took 2.5536 seconds. 
  Full-batch training loss = 0.013607, test loss = 0.162630
  Training set accuracy = 0.999800, Test set accuracy = 0.954800
 epoch 66/100. Took 2.5464 seconds. 
  Full-batch training loss = 0.013145, test loss = 0.161733
  Training set accuracy = 0.999700, Test set accuracy = 0.955600
 epoch 67/100. Took 2.5863 seconds. 
  Full-batch training loss = 0.012835, test loss = 0.162083
  Training set accuracy = 0.999800, Test set accuracy = 0.956100
 epoch 68/100. Took 2.5488 seconds. 
  Full-batch training loss = 0.012472, test loss = 0.161957
  Training set accuracy = 0.999800, Test set accuracy = 0.954900
 epoch 69/100. Took 2.5546 seconds. 
  Full-batch training loss = 0.012126, test loss = 0.161645
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 70/100. Took 2.5865 seconds. 
  Full-batch training loss = 0.011826, test loss = 0.162449
  Training set accuracy = 0.999900, Test set accuracy = 0.955800
 epoch 71/100. Took 2.5565 seconds. 
  Full-batch training loss = 0.011545, test loss = 0.162330
  Training set accuracy = 0.999900, Test set accuracy = 0.955900
 epoch 72/100. Took 2.5609 seconds. 
  Full-batch training loss = 0.011232, test loss = 0.162481
  Training set accuracy = 0.999900, Test set accuracy = 0.955800
 epoch 73/100. Took 2.5344 seconds. 
  Full-batch training loss = 0.010926, test loss = 0.162649
  Training set accuracy = 0.999900, Test set accuracy = 0.955800
 epoch 74/100. Took 2.5802 seconds. 
  Full-batch training loss = 0.010673, test loss = 0.162673
  Training set accuracy = 0.999900, Test set accuracy = 0.955500
 epoch 75/100. Took 2.5682 seconds. 
  Full-batch training loss = 0.010449, test loss = 0.163046
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 76/100. Took 2.5918 seconds. 
  Full-batch training loss = 0.010209, test loss = 0.162751
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 77/100. Took 2.5281 seconds. 
  Full-batch training loss = 0.009943, test loss = 0.162974
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 78/100. Took 2.5403 seconds. 
  Full-batch training loss = 0.009742, test loss = 0.163146
  Training set accuracy = 0.999900, Test set accuracy = 0.956200
 epoch 79/100. Took 2.5589 seconds. 
  Full-batch training loss = 0.009505, test loss = 0.163315
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 80/100. Took 2.5131 seconds. 
  Full-batch training loss = 0.009296, test loss = 0.163441
  Training set accuracy = 0.999900, Test set accuracy = 0.956000
 epoch 81/100. Took 2.5857 seconds. 
  Full-batch training loss = 0.009116, test loss = 0.163402
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 82/100. Took 2.5082 seconds. 
  Full-batch training loss = 0.008898, test loss = 0.163723
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 83/100. Took 2.5088 seconds. 
  Full-batch training loss = 0.008732, test loss = 0.163733
  Training set accuracy = 0.999900, Test set accuracy = 0.956600
 epoch 84/100. Took 2.5819 seconds. 
  Full-batch training loss = 0.008566, test loss = 0.164234
  Training set accuracy = 0.999900, Test set accuracy = 0.956000
 epoch 85/100. Took 2.5709 seconds. 
  Full-batch training loss = 0.008367, test loss = 0.163988
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 86/100. Took 2.5373 seconds. 
  Full-batch training loss = 0.008201, test loss = 0.164217
  Training set accuracy = 0.999900, Test set accuracy = 0.956300
 epoch 87/100. Took 2.534 seconds. 
  Full-batch training loss = 0.008054, test loss = 0.164169
  Training set accuracy = 0.999900, Test set accuracy = 0.956700
 epoch 88/100. Took 2.5631 seconds. 
  Full-batch training loss = 0.007898, test loss = 0.164593
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 89/100. Took 2.5068 seconds. 
  Full-batch training loss = 0.007752, test loss = 0.164555
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 90/100. Took 2.5142 seconds. 
  Full-batch training loss = 0.007589, test loss = 0.165029
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 91/100. Took 2.5152 seconds. 
  Full-batch training loss = 0.007476, test loss = 0.164944
  Training set accuracy = 0.999900, Test set accuracy = 0.956600
 epoch 92/100. Took 2.5522 seconds. 
  Full-batch training loss = 0.007314, test loss = 0.165004
  Training set accuracy = 0.999900, Test set accuracy = 0.956000
 epoch 93/100. Took 2.535 seconds. 
  Full-batch training loss = 0.007202, test loss = 0.164979
  Training set accuracy = 0.999900, Test set accuracy = 0.956100
 epoch 94/100. Took 2.5658 seconds. 
  Full-batch training loss = 0.007058, test loss = 0.165251
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 95/100. Took 2.603 seconds. 
  Full-batch training loss = 0.006937, test loss = 0.165643
  Training set accuracy = 0.999900, Test set accuracy = 0.956400
 epoch 96/100. Took 2.5505 seconds. 
  Full-batch training loss = 0.006832, test loss = 0.165631
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 97/100. Took 2.4989 seconds. 
  Full-batch training loss = 0.006692, test loss = 0.165556
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 98/100. Took 2.5611 seconds. 
  Full-batch training loss = 0.006586, test loss = 0.165549
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 99/100. Took 2.5361 seconds. 
  Full-batch training loss = 0.006482, test loss = 0.166084
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 100/100. Took 2.5099 seconds. 
  Full-batch training loss = 0.006366, test loss = 0.166143
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
Elapsed time is 686.075023 seconds.
End Training
