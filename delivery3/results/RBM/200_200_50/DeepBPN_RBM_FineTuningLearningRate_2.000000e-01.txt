Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.64485 seconds. Average reconstruction error is: 29.3087
 epoch 2/20. Took 0.64059 seconds. Average reconstruction error is: 18.0241
 epoch 3/20. Took 0.60579 seconds. Average reconstruction error is: 15.8086
 epoch 4/20. Took 0.60598 seconds. Average reconstruction error is: 14.4823
 epoch 5/20. Took 0.62761 seconds. Average reconstruction error is: 13.5835
 epoch 6/20. Took 0.60542 seconds. Average reconstruction error is: 12.837
 epoch 7/20. Took 0.61217 seconds. Average reconstruction error is: 12.2251
 epoch 8/20. Took 0.60743 seconds. Average reconstruction error is: 11.7417
 epoch 9/20. Took 0.61583 seconds. Average reconstruction error is: 11.3111
 epoch 10/20. Took 0.63543 seconds. Average reconstruction error is: 11.0222
 epoch 11/20. Took 0.63305 seconds. Average reconstruction error is: 10.7323
 epoch 12/20. Took 0.63157 seconds. Average reconstruction error is: 10.575
 epoch 13/20. Took 0.61917 seconds. Average reconstruction error is: 10.3951
 epoch 14/20. Took 0.64266 seconds. Average reconstruction error is: 10.2173
 epoch 15/20. Took 0.75189 seconds. Average reconstruction error is: 10.0966
 epoch 16/20. Took 0.62303 seconds. Average reconstruction error is: 9.9416
 epoch 17/20. Took 0.60764 seconds. Average reconstruction error is: 9.8274
 epoch 18/20. Took 0.60739 seconds. Average reconstruction error is: 9.7389
 epoch 19/20. Took 0.60407 seconds. Average reconstruction error is: 9.6806
 epoch 20/20. Took 0.62862 seconds. Average reconstruction error is: 9.5981
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20686 seconds. Average reconstruction error is: 13.7219
 epoch 2/20. Took 0.20614 seconds. Average reconstruction error is: 8.0618
 epoch 3/20. Took 0.19951 seconds. Average reconstruction error is: 7.1474
 epoch 4/20. Took 0.19972 seconds. Average reconstruction error is: 6.6704
 epoch 5/20. Took 0.20274 seconds. Average reconstruction error is: 6.3327
 epoch 6/20. Took 0.19987 seconds. Average reconstruction error is: 6.1305
 epoch 7/20. Took 0.20072 seconds. Average reconstruction error is: 5.956
 epoch 8/20. Took 0.19914 seconds. Average reconstruction error is: 5.7933
 epoch 9/20. Took 0.1989 seconds. Average reconstruction error is: 5.6777
 epoch 10/20. Took 0.20153 seconds. Average reconstruction error is: 5.5932
 epoch 11/20. Took 0.20132 seconds. Average reconstruction error is: 5.5046
 epoch 12/20. Took 0.20234 seconds. Average reconstruction error is: 5.4254
 epoch 13/20. Took 0.19885 seconds. Average reconstruction error is: 5.3393
 epoch 14/20. Took 0.2036 seconds. Average reconstruction error is: 5.273
 epoch 15/20. Took 0.20143 seconds. Average reconstruction error is: 5.22
 epoch 16/20. Took 0.20078 seconds. Average reconstruction error is: 5.1494
 epoch 17/20. Took 0.19983 seconds. Average reconstruction error is: 5.0984
 epoch 18/20. Took 0.20106 seconds. Average reconstruction error is: 5.0664
 epoch 19/20. Took 0.19938 seconds. Average reconstruction error is: 5.0051
 epoch 20/20. Took 0.20676 seconds. Average reconstruction error is: 4.9886
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20562 seconds. Average reconstruction error is: 12.0469
 epoch 2/20. Took 0.20021 seconds. Average reconstruction error is: 7.117
 epoch 3/20. Took 0.20179 seconds. Average reconstruction error is: 6.337
 epoch 4/20. Took 0.19821 seconds. Average reconstruction error is: 5.992
 epoch 5/20. Took 0.19981 seconds. Average reconstruction error is: 5.727
 epoch 6/20. Took 0.20491 seconds. Average reconstruction error is: 5.5685
 epoch 7/20. Took 0.22046 seconds. Average reconstruction error is: 5.4524
 epoch 8/20. Took 0.20021 seconds. Average reconstruction error is: 5.3473
 epoch 9/20. Took 0.2014 seconds. Average reconstruction error is: 5.2484
 epoch 10/20. Took 0.20031 seconds. Average reconstruction error is: 5.1529
 epoch 11/20. Took 0.2041 seconds. Average reconstruction error is: 5.0515
 epoch 12/20. Took 0.20043 seconds. Average reconstruction error is: 5.0149
 epoch 13/20. Took 0.2004 seconds. Average reconstruction error is: 4.9677
 epoch 14/20. Took 0.19974 seconds. Average reconstruction error is: 4.9332
 epoch 15/20. Took 0.19981 seconds. Average reconstruction error is: 4.8811
 epoch 16/20. Took 0.20203 seconds. Average reconstruction error is: 4.8163
 epoch 17/20. Took 0.19955 seconds. Average reconstruction error is: 4.7793
 epoch 18/20. Took 0.2012 seconds. Average reconstruction error is: 4.7073
 epoch 19/20. Took 0.20675 seconds. Average reconstruction error is: 4.6893
 epoch 20/20. Took 0.20388 seconds. Average reconstruction error is: 4.6456
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.6103 seconds. 
  Full-batch training loss = 0.279151, test loss = 0.287781
  Training set accuracy = 0.921800, Test set accuracy = 0.919400
 epoch 2/100. Took 0.64374 seconds. 
  Full-batch training loss = 0.229592, test loss = 0.255438
  Training set accuracy = 0.936700, Test set accuracy = 0.926500
 epoch 3/100. Took 0.68275 seconds. 
  Full-batch training loss = 0.184043, test loss = 0.218442
  Training set accuracy = 0.949000, Test set accuracy = 0.937200
 epoch 4/100. Took 0.64648 seconds. 
  Full-batch training loss = 0.161364, test loss = 0.211298
  Training set accuracy = 0.954600, Test set accuracy = 0.938400
 epoch 5/100. Took 0.6289 seconds. 
  Full-batch training loss = 0.140887, test loss = 0.198100
  Training set accuracy = 0.961600, Test set accuracy = 0.942100
 epoch 6/100. Took 0.68155 seconds. 
  Full-batch training loss = 0.124632, test loss = 0.193292
  Training set accuracy = 0.964400, Test set accuracy = 0.945000
 epoch 7/100. Took 0.64463 seconds. 
  Full-batch training loss = 0.111103, test loss = 0.181521
  Training set accuracy = 0.969900, Test set accuracy = 0.945900
 epoch 8/100. Took 0.6608 seconds. 
  Full-batch training loss = 0.100097, test loss = 0.180758
  Training set accuracy = 0.974800, Test set accuracy = 0.948100
 epoch 9/100. Took 0.64736 seconds. 
  Full-batch training loss = 0.088184, test loss = 0.172871
  Training set accuracy = 0.978500, Test set accuracy = 0.951000
 epoch 10/100. Took 0.62864 seconds. 
  Full-batch training loss = 0.078884, test loss = 0.169065
  Training set accuracy = 0.980800, Test set accuracy = 0.951400
 epoch 11/100. Took 0.65463 seconds. 
  Full-batch training loss = 0.071638, test loss = 0.166130
  Training set accuracy = 0.983000, Test set accuracy = 0.952200
 epoch 12/100. Took 0.67085 seconds. 
  Full-batch training loss = 0.066851, test loss = 0.167527
  Training set accuracy = 0.983900, Test set accuracy = 0.951500
 epoch 13/100. Took 0.64696 seconds. 
  Full-batch training loss = 0.059188, test loss = 0.163808
  Training set accuracy = 0.986700, Test set accuracy = 0.953500
 epoch 14/100. Took 0.63857 seconds. 
  Full-batch training loss = 0.054392, test loss = 0.163897
  Training set accuracy = 0.987900, Test set accuracy = 0.952000
 epoch 15/100. Took 0.62732 seconds. 
  Full-batch training loss = 0.049986, test loss = 0.160709
  Training set accuracy = 0.989300, Test set accuracy = 0.954300
 epoch 16/100. Took 0.6384 seconds. 
  Full-batch training loss = 0.044640, test loss = 0.161235
  Training set accuracy = 0.991200, Test set accuracy = 0.954100
 epoch 17/100. Took 0.6318 seconds. 
  Full-batch training loss = 0.040702, test loss = 0.160796
  Training set accuracy = 0.992300, Test set accuracy = 0.953700
 epoch 18/100. Took 0.63231 seconds. 
  Full-batch training loss = 0.037570, test loss = 0.157832
  Training set accuracy = 0.993600, Test set accuracy = 0.954600
 epoch 19/100. Took 0.64757 seconds. 
  Full-batch training loss = 0.034122, test loss = 0.160932
  Training set accuracy = 0.994700, Test set accuracy = 0.955000
 epoch 20/100. Took 0.65388 seconds. 
  Full-batch training loss = 0.031157, test loss = 0.158721
  Training set accuracy = 0.995500, Test set accuracy = 0.955900
 epoch 21/100. Took 0.64437 seconds. 
  Full-batch training loss = 0.028589, test loss = 0.158505
  Training set accuracy = 0.996200, Test set accuracy = 0.955200
 epoch 22/100. Took 0.64121 seconds. 
  Full-batch training loss = 0.027152, test loss = 0.157386
  Training set accuracy = 0.995600, Test set accuracy = 0.956100
 epoch 23/100. Took 0.66166 seconds. 
  Full-batch training loss = 0.024374, test loss = 0.157601
  Training set accuracy = 0.997300, Test set accuracy = 0.955100
 epoch 24/100. Took 0.6593 seconds. 
  Full-batch training loss = 0.023175, test loss = 0.157442
  Training set accuracy = 0.997600, Test set accuracy = 0.954900
 epoch 25/100. Took 0.65677 seconds. 
  Full-batch training loss = 0.020818, test loss = 0.159229
  Training set accuracy = 0.998100, Test set accuracy = 0.955600
 epoch 26/100. Took 0.63234 seconds. 
  Full-batch training loss = 0.019247, test loss = 0.158911
  Training set accuracy = 0.998500, Test set accuracy = 0.956000
 epoch 27/100. Took 0.63587 seconds. 
  Full-batch training loss = 0.018276, test loss = 0.159476
  Training set accuracy = 0.998800, Test set accuracy = 0.955000
 epoch 28/100. Took 0.62737 seconds. 
  Full-batch training loss = 0.016652, test loss = 0.158409
  Training set accuracy = 0.998900, Test set accuracy = 0.956300
 epoch 29/100. Took 0.72258 seconds. 
  Full-batch training loss = 0.015360, test loss = 0.158483
  Training set accuracy = 0.999000, Test set accuracy = 0.956000
 epoch 30/100. Took 0.62455 seconds. 
  Full-batch training loss = 0.014623, test loss = 0.158715
  Training set accuracy = 0.999100, Test set accuracy = 0.956000
 epoch 31/100. Took 0.63279 seconds. 
  Full-batch training loss = 0.013594, test loss = 0.159661
  Training set accuracy = 0.999200, Test set accuracy = 0.956600
 epoch 32/100. Took 0.71749 seconds. 
  Full-batch training loss = 0.012685, test loss = 0.159842
  Training set accuracy = 0.999500, Test set accuracy = 0.956300
 epoch 33/100. Took 0.65491 seconds. 
  Full-batch training loss = 0.012054, test loss = 0.160134
  Training set accuracy = 0.999700, Test set accuracy = 0.957400
 epoch 34/100. Took 0.72358 seconds. 
  Full-batch training loss = 0.011294, test loss = 0.160053
  Training set accuracy = 0.999900, Test set accuracy = 0.956500
 epoch 35/100. Took 0.66911 seconds. 
  Full-batch training loss = 0.010896, test loss = 0.161087
  Training set accuracy = 0.999800, Test set accuracy = 0.957400
 epoch 36/100. Took 0.65419 seconds. 
  Full-batch training loss = 0.010151, test loss = 0.159717
  Training set accuracy = 0.999900, Test set accuracy = 0.957400
 epoch 37/100. Took 0.70565 seconds. 
  Full-batch training loss = 0.009741, test loss = 0.161418
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 38/100. Took 0.7384 seconds. 
  Full-batch training loss = 0.009222, test loss = 0.161079
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 39/100. Took 0.64854 seconds. 
  Full-batch training loss = 0.008869, test loss = 0.161184
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 40/100. Took 0.64763 seconds. 
  Full-batch training loss = 0.008362, test loss = 0.161570
  Training set accuracy = 1.000000, Test set accuracy = 0.957100
 epoch 41/100. Took 0.69278 seconds. 
  Full-batch training loss = 0.007946, test loss = 0.163066
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 42/100. Took 0.69834 seconds. 
  Full-batch training loss = 0.007618, test loss = 0.162754
  Training set accuracy = 1.000000, Test set accuracy = 0.957100
 epoch 43/100. Took 0.70679 seconds. 
  Full-batch training loss = 0.007428, test loss = 0.164703
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 44/100. Took 0.71331 seconds. 
  Full-batch training loss = 0.007068, test loss = 0.163936
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 45/100. Took 0.636 seconds. 
  Full-batch training loss = 0.006736, test loss = 0.163903
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 46/100. Took 0.67795 seconds. 
  Full-batch training loss = 0.006516, test loss = 0.164095
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 47/100. Took 0.72642 seconds. 
  Full-batch training loss = 0.006282, test loss = 0.165598
  Training set accuracy = 1.000000, Test set accuracy = 0.957200
 epoch 48/100. Took 0.68811 seconds. 
  Full-batch training loss = 0.006034, test loss = 0.165000
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 49/100. Took 0.77004 seconds. 
  Full-batch training loss = 0.005826, test loss = 0.165384
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 50/100. Took 0.64287 seconds. 
  Full-batch training loss = 0.005622, test loss = 0.166232
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 51/100. Took 0.64693 seconds. 
  Full-batch training loss = 0.005436, test loss = 0.165707
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 52/100. Took 0.68619 seconds. 
  Full-batch training loss = 0.005269, test loss = 0.165860
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 53/100. Took 0.71676 seconds. 
  Full-batch training loss = 0.005086, test loss = 0.166399
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 54/100. Took 0.72974 seconds. 
  Full-batch training loss = 0.004927, test loss = 0.166506
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 55/100. Took 0.64334 seconds. 
  Full-batch training loss = 0.004772, test loss = 0.167265
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 56/100. Took 0.62703 seconds. 
  Full-batch training loss = 0.004641, test loss = 0.167263
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 57/100. Took 0.65146 seconds. 
  Full-batch training loss = 0.004503, test loss = 0.167525
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 58/100. Took 0.63431 seconds. 
  Full-batch training loss = 0.004384, test loss = 0.168169
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 59/100. Took 0.70044 seconds. 
  Full-batch training loss = 0.004302, test loss = 0.169068
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 60/100. Took 0.6814 seconds. 
  Full-batch training loss = 0.004165, test loss = 0.169358
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 61/100. Took 0.62842 seconds. 
  Full-batch training loss = 0.004036, test loss = 0.169064
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 62/100. Took 0.63796 seconds. 
  Full-batch training loss = 0.003942, test loss = 0.169063
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 63/100. Took 0.64438 seconds. 
  Full-batch training loss = 0.003845, test loss = 0.169626
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 64/100. Took 0.6644 seconds. 
  Full-batch training loss = 0.003737, test loss = 0.169565
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 65/100. Took 0.63436 seconds. 
  Full-batch training loss = 0.003654, test loss = 0.169907
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 66/100. Took 0.66867 seconds. 
  Full-batch training loss = 0.003568, test loss = 0.170116
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 67/100. Took 0.70274 seconds. 
  Full-batch training loss = 0.003479, test loss = 0.170577
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 68/100. Took 0.62964 seconds. 
  Full-batch training loss = 0.003397, test loss = 0.170666
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 69/100. Took 0.675 seconds. 
  Full-batch training loss = 0.003321, test loss = 0.171387
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 70/100. Took 0.62356 seconds. 
  Full-batch training loss = 0.003249, test loss = 0.171008
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 71/100. Took 0.70685 seconds. 
  Full-batch training loss = 0.003172, test loss = 0.171545
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 72/100. Took 0.66218 seconds. 
  Full-batch training loss = 0.003117, test loss = 0.171945
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 73/100. Took 0.62725 seconds. 
  Full-batch training loss = 0.003043, test loss = 0.172183
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 74/100. Took 0.68234 seconds. 
  Full-batch training loss = 0.002984, test loss = 0.172141
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 75/100. Took 0.64361 seconds. 
  Full-batch training loss = 0.002922, test loss = 0.172443
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 76/100. Took 0.63854 seconds. 
  Full-batch training loss = 0.002866, test loss = 0.172371
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 77/100. Took 0.62359 seconds. 
  Full-batch training loss = 0.002803, test loss = 0.173061
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 78/100. Took 0.6244 seconds. 
  Full-batch training loss = 0.002753, test loss = 0.173421
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 79/100. Took 0.62953 seconds. 
  Full-batch training loss = 0.002697, test loss = 0.173411
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 80/100. Took 0.62374 seconds. 
  Full-batch training loss = 0.002648, test loss = 0.173547
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 81/100. Took 0.63613 seconds. 
  Full-batch training loss = 0.002600, test loss = 0.173993
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 82/100. Took 0.6299 seconds. 
  Full-batch training loss = 0.002557, test loss = 0.174381
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 83/100. Took 0.62874 seconds. 
  Full-batch training loss = 0.002506, test loss = 0.174168
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 84/100. Took 0.63611 seconds. 
  Full-batch training loss = 0.002456, test loss = 0.174571
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 85/100. Took 0.65044 seconds. 
  Full-batch training loss = 0.002417, test loss = 0.174817
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 86/100. Took 0.64783 seconds. 
  Full-batch training loss = 0.002374, test loss = 0.174928
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 87/100. Took 0.68815 seconds. 
  Full-batch training loss = 0.002340, test loss = 0.175177
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 88/100. Took 0.69408 seconds. 
  Full-batch training loss = 0.002300, test loss = 0.175661
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 89/100. Took 0.64882 seconds. 
  Full-batch training loss = 0.002259, test loss = 0.175740
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 90/100. Took 0.61865 seconds. 
  Full-batch training loss = 0.002219, test loss = 0.175504
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 91/100. Took 0.6873 seconds. 
  Full-batch training loss = 0.002185, test loss = 0.176157
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 92/100. Took 0.73874 seconds. 
  Full-batch training loss = 0.002149, test loss = 0.175902
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 93/100. Took 0.82531 seconds. 
  Full-batch training loss = 0.002114, test loss = 0.176096
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 94/100. Took 0.74413 seconds. 
  Full-batch training loss = 0.002084, test loss = 0.176431
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 95/100. Took 0.65064 seconds. 
  Full-batch training loss = 0.002051, test loss = 0.176694
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 96/100. Took 0.65152 seconds. 
  Full-batch training loss = 0.002020, test loss = 0.176827
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 97/100. Took 0.62813 seconds. 
  Full-batch training loss = 0.001990, test loss = 0.177053
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 98/100. Took 0.63409 seconds. 
  Full-batch training loss = 0.001961, test loss = 0.177019
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 99/100. Took 0.67337 seconds. 
  Full-batch training loss = 0.001933, test loss = 0.177382
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 100/100. Took 0.68117 seconds. 
  Full-batch training loss = 0.001904, test loss = 0.177407
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
Elapsed time is 150.108792 seconds.
End Training
