Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.5912 seconds. Average reconstruction error is: 30.2627
 epoch 2/20. Took 0.58461 seconds. Average reconstruction error is: 18.1467
 epoch 3/20. Took 0.62731 seconds. Average reconstruction error is: 15.5609
 epoch 4/20. Took 0.59411 seconds. Average reconstruction error is: 14.1656
 epoch 5/20. Took 0.57281 seconds. Average reconstruction error is: 13.1528
 epoch 6/20. Took 0.57073 seconds. Average reconstruction error is: 12.3599
 epoch 7/20. Took 0.5742 seconds. Average reconstruction error is: 11.7685
 epoch 8/20. Took 0.61086 seconds. Average reconstruction error is: 11.3336
 epoch 9/20. Took 0.63871 seconds. Average reconstruction error is: 10.9756
 epoch 10/20. Took 0.59487 seconds. Average reconstruction error is: 10.6894
 epoch 11/20. Took 0.57087 seconds. Average reconstruction error is: 10.4735
 epoch 12/20. Took 0.61104 seconds. Average reconstruction error is: 10.2989
 epoch 13/20. Took 0.68418 seconds. Average reconstruction error is: 10.1222
 epoch 14/20. Took 0.6709 seconds. Average reconstruction error is: 9.982
 epoch 15/20. Took 0.58864 seconds. Average reconstruction error is: 9.8545
 epoch 16/20. Took 0.57209 seconds. Average reconstruction error is: 9.6985
 epoch 17/20. Took 0.56951 seconds. Average reconstruction error is: 9.6132
 epoch 18/20. Took 0.56934 seconds. Average reconstruction error is: 9.5234
 epoch 19/20. Took 0.57159 seconds. Average reconstruction error is: 9.4707
 epoch 20/20. Took 0.58134 seconds. Average reconstruction error is: 9.3432
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.1996 seconds. Average reconstruction error is: 14.5042
 epoch 2/20. Took 0.20204 seconds. Average reconstruction error is: 8.6443
 epoch 3/20. Took 0.20229 seconds. Average reconstruction error is: 7.6474
 epoch 4/20. Took 0.20062 seconds. Average reconstruction error is: 7.0803
 epoch 5/20. Took 0.20333 seconds. Average reconstruction error is: 6.7361
 epoch 6/20. Took 0.20376 seconds. Average reconstruction error is: 6.5055
 epoch 7/20. Took 0.19981 seconds. Average reconstruction error is: 6.3079
 epoch 8/20. Took 0.19737 seconds. Average reconstruction error is: 6.1529
 epoch 9/20. Took 0.19482 seconds. Average reconstruction error is: 6.0181
 epoch 10/20. Took 0.22834 seconds. Average reconstruction error is: 5.9112
 epoch 11/20. Took 0.23009 seconds. Average reconstruction error is: 5.7831
 epoch 12/20. Took 0.23266 seconds. Average reconstruction error is: 5.6913
 epoch 13/20. Took 0.21804 seconds. Average reconstruction error is: 5.6091
 epoch 14/20. Took 0.21239 seconds. Average reconstruction error is: 5.5332
 epoch 15/20. Took 0.19488 seconds. Average reconstruction error is: 5.4804
 epoch 16/20. Took 0.1948 seconds. Average reconstruction error is: 5.4488
 epoch 17/20. Took 0.211 seconds. Average reconstruction error is: 5.3747
 epoch 18/20. Took 0.22485 seconds. Average reconstruction error is: 5.3093
 epoch 19/20. Took 0.23826 seconds. Average reconstruction error is: 5.2533
 epoch 20/20. Took 0.21284 seconds. Average reconstruction error is: 5.2169
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.20774 seconds. Average reconstruction error is: 12.4769
 epoch 2/20. Took 0.2031 seconds. Average reconstruction error is: 7.2563
 epoch 3/20. Took 0.20631 seconds. Average reconstruction error is: 6.4305
 epoch 4/20. Took 0.2147 seconds. Average reconstruction error is: 6.019
 epoch 5/20. Took 0.24329 seconds. Average reconstruction error is: 5.8022
 epoch 6/20. Took 0.26235 seconds. Average reconstruction error is: 5.6277
 epoch 7/20. Took 0.20632 seconds. Average reconstruction error is: 5.4937
 epoch 8/20. Took 0.20368 seconds. Average reconstruction error is: 5.3994
 epoch 9/20. Took 0.20721 seconds. Average reconstruction error is: 5.3154
 epoch 10/20. Took 0.22775 seconds. Average reconstruction error is: 5.2569
 epoch 11/20. Took 0.25142 seconds. Average reconstruction error is: 5.1824
 epoch 12/20. Took 0.20657 seconds. Average reconstruction error is: 5.1187
 epoch 13/20. Took 0.21455 seconds. Average reconstruction error is: 5.0705
 epoch 14/20. Took 0.20044 seconds. Average reconstruction error is: 5.0114
 epoch 15/20. Took 0.19631 seconds. Average reconstruction error is: 4.9449
 epoch 16/20. Took 0.20534 seconds. Average reconstruction error is: 4.9046
 epoch 17/20. Took 0.21105 seconds. Average reconstruction error is: 4.872
 epoch 18/20. Took 0.20692 seconds. Average reconstruction error is: 4.8062
 epoch 19/20. Took 0.2294 seconds. Average reconstruction error is: 4.7718
 epoch 20/20. Took 0.21237 seconds. Average reconstruction error is: 4.7481
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.58086 seconds. 
  Full-batch training loss = 0.274535, test loss = 0.279816
  Training set accuracy = 0.926300, Test set accuracy = 0.925000
 epoch 2/100. Took 0.62494 seconds. 
  Full-batch training loss = 0.211370, test loss = 0.235662
  Training set accuracy = 0.942600, Test set accuracy = 0.932200
 epoch 3/100. Took 0.61717 seconds. 
  Full-batch training loss = 0.178156, test loss = 0.217415
  Training set accuracy = 0.950600, Test set accuracy = 0.938400
 epoch 4/100. Took 0.62224 seconds. 
  Full-batch training loss = 0.151914, test loss = 0.202552
  Training set accuracy = 0.958600, Test set accuracy = 0.940900
 epoch 5/100. Took 0.5846 seconds. 
  Full-batch training loss = 0.140523, test loss = 0.199110
  Training set accuracy = 0.962100, Test set accuracy = 0.942500
 epoch 6/100. Took 0.58388 seconds. 
  Full-batch training loss = 0.120371, test loss = 0.188112
  Training set accuracy = 0.968500, Test set accuracy = 0.945400
 epoch 7/100. Took 0.62842 seconds. 
  Full-batch training loss = 0.102811, test loss = 0.180409
  Training set accuracy = 0.973000, Test set accuracy = 0.947300
 epoch 8/100. Took 0.705 seconds. 
  Full-batch training loss = 0.088707, test loss = 0.172023
  Training set accuracy = 0.978000, Test set accuracy = 0.948700
 epoch 9/100. Took 0.65868 seconds. 
  Full-batch training loss = 0.084082, test loss = 0.176439
  Training set accuracy = 0.979400, Test set accuracy = 0.947100
 epoch 10/100. Took 0.63505 seconds. 
  Full-batch training loss = 0.071023, test loss = 0.167364
  Training set accuracy = 0.984000, Test set accuracy = 0.951300
 epoch 11/100. Took 0.65048 seconds. 
  Full-batch training loss = 0.063438, test loss = 0.163418
  Training set accuracy = 0.986100, Test set accuracy = 0.951500
 epoch 12/100. Took 0.60913 seconds. 
  Full-batch training loss = 0.056760, test loss = 0.163624
  Training set accuracy = 0.988700, Test set accuracy = 0.951100
 epoch 13/100. Took 0.68463 seconds. 
  Full-batch training loss = 0.052982, test loss = 0.165696
  Training set accuracy = 0.989400, Test set accuracy = 0.950600
 epoch 14/100. Took 0.58672 seconds. 
  Full-batch training loss = 0.046464, test loss = 0.160576
  Training set accuracy = 0.991500, Test set accuracy = 0.952500
 epoch 15/100. Took 0.58737 seconds. 
  Full-batch training loss = 0.042375, test loss = 0.160307
  Training set accuracy = 0.992500, Test set accuracy = 0.952900
 epoch 16/100. Took 0.60443 seconds. 
  Full-batch training loss = 0.038000, test loss = 0.159688
  Training set accuracy = 0.993800, Test set accuracy = 0.952900
 epoch 17/100. Took 0.58671 seconds. 
  Full-batch training loss = 0.035440, test loss = 0.157826
  Training set accuracy = 0.995300, Test set accuracy = 0.954600
 epoch 18/100. Took 0.58351 seconds. 
  Full-batch training loss = 0.035253, test loss = 0.163081
  Training set accuracy = 0.994400, Test set accuracy = 0.954200
 epoch 19/100. Took 0.57912 seconds. 
  Full-batch training loss = 0.029715, test loss = 0.159841
  Training set accuracy = 0.996300, Test set accuracy = 0.953500
 epoch 20/100. Took 0.60392 seconds. 
  Full-batch training loss = 0.027726, test loss = 0.159986
  Training set accuracy = 0.996500, Test set accuracy = 0.953900
 epoch 21/100. Took 0.61384 seconds. 
  Full-batch training loss = 0.024781, test loss = 0.157686
  Training set accuracy = 0.997700, Test set accuracy = 0.954900
 epoch 22/100. Took 0.59984 seconds. 
  Full-batch training loss = 0.023017, test loss = 0.158590
  Training set accuracy = 0.997800, Test set accuracy = 0.954800
 epoch 23/100. Took 0.72684 seconds. 
  Full-batch training loss = 0.021899, test loss = 0.159014
  Training set accuracy = 0.998000, Test set accuracy = 0.955400
 epoch 24/100. Took 0.58754 seconds. 
  Full-batch training loss = 0.019798, test loss = 0.159486
  Training set accuracy = 0.998600, Test set accuracy = 0.955500
 epoch 25/100. Took 0.59974 seconds. 
  Full-batch training loss = 0.018285, test loss = 0.159846
  Training set accuracy = 0.998700, Test set accuracy = 0.954900
 epoch 26/100. Took 0.58691 seconds. 
  Full-batch training loss = 0.017188, test loss = 0.159399
  Training set accuracy = 0.998800, Test set accuracy = 0.955500
 epoch 27/100. Took 0.7053 seconds. 
  Full-batch training loss = 0.016182, test loss = 0.160011
  Training set accuracy = 0.999000, Test set accuracy = 0.955600
 epoch 28/100. Took 0.61565 seconds. 
  Full-batch training loss = 0.015191, test loss = 0.160434
  Training set accuracy = 0.999200, Test set accuracy = 0.955700
 epoch 29/100. Took 0.59737 seconds. 
  Full-batch training loss = 0.013978, test loss = 0.160513
  Training set accuracy = 0.999200, Test set accuracy = 0.955500
 epoch 30/100. Took 0.59048 seconds. 
  Full-batch training loss = 0.013252, test loss = 0.160781
  Training set accuracy = 0.999400, Test set accuracy = 0.956100
 epoch 31/100. Took 0.5813 seconds. 
  Full-batch training loss = 0.012503, test loss = 0.160651
  Training set accuracy = 0.999500, Test set accuracy = 0.956400
 epoch 32/100. Took 0.59793 seconds. 
  Full-batch training loss = 0.011691, test loss = 0.161201
  Training set accuracy = 0.999400, Test set accuracy = 0.956000
 epoch 33/100. Took 0.58382 seconds. 
  Full-batch training loss = 0.011300, test loss = 0.161480
  Training set accuracy = 0.999600, Test set accuracy = 0.955700
 epoch 34/100. Took 0.58084 seconds. 
  Full-batch training loss = 0.010528, test loss = 0.162715
  Training set accuracy = 0.999800, Test set accuracy = 0.957300
 epoch 35/100. Took 0.60602 seconds. 
  Full-batch training loss = 0.009945, test loss = 0.162621
  Training set accuracy = 0.999700, Test set accuracy = 0.956400
 epoch 36/100. Took 0.60635 seconds. 
  Full-batch training loss = 0.009461, test loss = 0.162937
  Training set accuracy = 0.999900, Test set accuracy = 0.957100
 epoch 37/100. Took 0.57864 seconds. 
  Full-batch training loss = 0.008972, test loss = 0.163039
  Training set accuracy = 0.999900, Test set accuracy = 0.956600
 epoch 38/100. Took 0.57822 seconds. 
  Full-batch training loss = 0.008509, test loss = 0.163146
  Training set accuracy = 0.999900, Test set accuracy = 0.956800
 epoch 39/100. Took 0.61598 seconds. 
  Full-batch training loss = 0.008320, test loss = 0.164037
  Training set accuracy = 0.999900, Test set accuracy = 0.956700
 epoch 40/100. Took 0.60152 seconds. 
  Full-batch training loss = 0.007921, test loss = 0.164477
  Training set accuracy = 0.999900, Test set accuracy = 0.956700
 epoch 41/100. Took 0.58946 seconds. 
  Full-batch training loss = 0.007501, test loss = 0.164356
  Training set accuracy = 0.999900, Test set accuracy = 0.957000
 epoch 42/100. Took 0.62071 seconds. 
  Full-batch training loss = 0.007218, test loss = 0.165660
  Training set accuracy = 0.999900, Test set accuracy = 0.957100
 epoch 43/100. Took 0.608 seconds. 
  Full-batch training loss = 0.006967, test loss = 0.165519
  Training set accuracy = 0.999900, Test set accuracy = 0.956900
 epoch 44/100. Took 0.58622 seconds. 
  Full-batch training loss = 0.006645, test loss = 0.165743
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 45/100. Took 0.59059 seconds. 
  Full-batch training loss = 0.006488, test loss = 0.166478
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 46/100. Took 0.58898 seconds. 
  Full-batch training loss = 0.006197, test loss = 0.166645
  Training set accuracy = 1.000000, Test set accuracy = 0.957500
 epoch 47/100. Took 0.58579 seconds. 
  Full-batch training loss = 0.005916, test loss = 0.166874
  Training set accuracy = 1.000000, Test set accuracy = 0.956800
 epoch 48/100. Took 0.57666 seconds. 
  Full-batch training loss = 0.005736, test loss = 0.166927
  Training set accuracy = 1.000000, Test set accuracy = 0.957300
 epoch 49/100. Took 0.57855 seconds. 
  Full-batch training loss = 0.005540, test loss = 0.167804
  Training set accuracy = 1.000000, Test set accuracy = 0.957200
 epoch 50/100. Took 0.58151 seconds. 
  Full-batch training loss = 0.005378, test loss = 0.168487
  Training set accuracy = 1.000000, Test set accuracy = 0.957100
 epoch 51/100. Took 0.60479 seconds. 
  Full-batch training loss = 0.005167, test loss = 0.167926
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 52/100. Took 0.67477 seconds. 
  Full-batch training loss = 0.005001, test loss = 0.168681
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 53/100. Took 0.5797 seconds. 
  Full-batch training loss = 0.004860, test loss = 0.168889
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 54/100. Took 0.61393 seconds. 
  Full-batch training loss = 0.004717, test loss = 0.169172
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 55/100. Took 0.57973 seconds. 
  Full-batch training loss = 0.004578, test loss = 0.169253
  Training set accuracy = 1.000000, Test set accuracy = 0.957600
 epoch 56/100. Took 0.59647 seconds. 
  Full-batch training loss = 0.004443, test loss = 0.169583
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 57/100. Took 0.60142 seconds. 
  Full-batch training loss = 0.004335, test loss = 0.170176
  Training set accuracy = 1.000000, Test set accuracy = 0.957200
 epoch 58/100. Took 0.62594 seconds. 
  Full-batch training loss = 0.004203, test loss = 0.169766
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 59/100. Took 0.62776 seconds. 
  Full-batch training loss = 0.004088, test loss = 0.170345
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 60/100. Took 0.67204 seconds. 
  Full-batch training loss = 0.003982, test loss = 0.170514
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 61/100. Took 0.69405 seconds. 
  Full-batch training loss = 0.003880, test loss = 0.170762
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 62/100. Took 0.58494 seconds. 
  Full-batch training loss = 0.003779, test loss = 0.171173
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 63/100. Took 0.62398 seconds. 
  Full-batch training loss = 0.003682, test loss = 0.171466
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 64/100. Took 0.69101 seconds. 
  Full-batch training loss = 0.003601, test loss = 0.171514
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 65/100. Took 0.62133 seconds. 
  Full-batch training loss = 0.003527, test loss = 0.172170
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 66/100. Took 0.61677 seconds. 
  Full-batch training loss = 0.003440, test loss = 0.172563
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 67/100. Took 0.62283 seconds. 
  Full-batch training loss = 0.003349, test loss = 0.172281
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 68/100. Took 0.70627 seconds. 
  Full-batch training loss = 0.003283, test loss = 0.172837
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 69/100. Took 0.59345 seconds. 
  Full-batch training loss = 0.003207, test loss = 0.172950
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 70/100. Took 0.6312 seconds. 
  Full-batch training loss = 0.003137, test loss = 0.173074
  Training set accuracy = 1.000000, Test set accuracy = 0.957800
 epoch 71/100. Took 0.6019 seconds. 
  Full-batch training loss = 0.003067, test loss = 0.173495
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 72/100. Took 0.59071 seconds. 
  Full-batch training loss = 0.003005, test loss = 0.173427
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 73/100. Took 0.65275 seconds. 
  Full-batch training loss = 0.002937, test loss = 0.173884
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 74/100. Took 0.60623 seconds. 
  Full-batch training loss = 0.002879, test loss = 0.174241
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 75/100. Took 0.5815 seconds. 
  Full-batch training loss = 0.002825, test loss = 0.174423
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 76/100. Took 0.57069 seconds. 
  Full-batch training loss = 0.002770, test loss = 0.174677
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 77/100. Took 0.57928 seconds. 
  Full-batch training loss = 0.002712, test loss = 0.174698
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 78/100. Took 0.57168 seconds. 
  Full-batch training loss = 0.002662, test loss = 0.174820
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 79/100. Took 0.57314 seconds. 
  Full-batch training loss = 0.002612, test loss = 0.175249
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 80/100. Took 0.58529 seconds. 
  Full-batch training loss = 0.002563, test loss = 0.175162
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 81/100. Took 0.57166 seconds. 
  Full-batch training loss = 0.002522, test loss = 0.175610
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 82/100. Took 0.57244 seconds. 
  Full-batch training loss = 0.002471, test loss = 0.175775
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 83/100. Took 0.57542 seconds. 
  Full-batch training loss = 0.002431, test loss = 0.175970
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 84/100. Took 0.57204 seconds. 
  Full-batch training loss = 0.002388, test loss = 0.176279
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 85/100. Took 0.57154 seconds. 
  Full-batch training loss = 0.002346, test loss = 0.176445
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 86/100. Took 0.57043 seconds. 
  Full-batch training loss = 0.002307, test loss = 0.176676
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 87/100. Took 0.57714 seconds. 
  Full-batch training loss = 0.002268, test loss = 0.176773
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 88/100. Took 0.59577 seconds. 
  Full-batch training loss = 0.002231, test loss = 0.176856
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 89/100. Took 0.57158 seconds. 
  Full-batch training loss = 0.002194, test loss = 0.177351
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 90/100. Took 0.57575 seconds. 
  Full-batch training loss = 0.002158, test loss = 0.177408
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 91/100. Took 0.57511 seconds. 
  Full-batch training loss = 0.002124, test loss = 0.177428
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 92/100. Took 0.58011 seconds. 
  Full-batch training loss = 0.002092, test loss = 0.177737
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 93/100. Took 0.64252 seconds. 
  Full-batch training loss = 0.002060, test loss = 0.177841
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 94/100. Took 0.57035 seconds. 
  Full-batch training loss = 0.002027, test loss = 0.177900
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 95/100. Took 0.57513 seconds. 
  Full-batch training loss = 0.001995, test loss = 0.178213
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 96/100. Took 0.63853 seconds. 
  Full-batch training loss = 0.001966, test loss = 0.178502
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 97/100. Took 0.59073 seconds. 
  Full-batch training loss = 0.001937, test loss = 0.178493
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 98/100. Took 0.59598 seconds. 
  Full-batch training loss = 0.001909, test loss = 0.178666
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 99/100. Took 0.57551 seconds. 
  Full-batch training loss = 0.001882, test loss = 0.179133
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 100/100. Took 0.58209 seconds. 
  Full-batch training loss = 0.001856, test loss = 0.179078
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
Elapsed time is 140.750022 seconds.
End Training
