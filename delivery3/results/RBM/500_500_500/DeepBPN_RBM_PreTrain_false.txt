Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	false
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.53935 seconds. 
  Full-batch training loss = 2.307778, test loss = 2.306519
  Training set accuracy = 0.113000, Test set accuracy = 0.116200
 epoch 2/100. Took 0.54445 seconds. 
  Full-batch training loss = 1.230212, test loss = 1.216831
  Training set accuracy = 0.532600, Test set accuracy = 0.541100
 epoch 3/100. Took 0.54262 seconds. 
  Full-batch training loss = 0.664885, test loss = 0.664574
  Training set accuracy = 0.780100, Test set accuracy = 0.782600
 epoch 4/100. Took 0.54374 seconds. 
  Full-batch training loss = 0.479248, test loss = 0.494045
  Training set accuracy = 0.854200, Test set accuracy = 0.846100
 epoch 5/100. Took 0.54385 seconds. 
  Full-batch training loss = 0.385861, test loss = 0.405706
  Training set accuracy = 0.888700, Test set accuracy = 0.877500
 epoch 6/100. Took 0.54254 seconds. 
  Full-batch training loss = 0.389337, test loss = 0.413849
  Training set accuracy = 0.883000, Test set accuracy = 0.876300
 epoch 7/100. Took 0.54703 seconds. 
  Full-batch training loss = 0.304780, test loss = 0.339149
  Training set accuracy = 0.912700, Test set accuracy = 0.896700
 epoch 8/100. Took 0.54489 seconds. 
  Full-batch training loss = 0.267235, test loss = 0.304984
  Training set accuracy = 0.922800, Test set accuracy = 0.909000
 epoch 9/100. Took 0.54654 seconds. 
  Full-batch training loss = 0.254199, test loss = 0.297855
  Training set accuracy = 0.923200, Test set accuracy = 0.908400
 epoch 10/100. Took 0.54423 seconds. 
  Full-batch training loss = 0.213114, test loss = 0.258732
  Training set accuracy = 0.937900, Test set accuracy = 0.919800
 epoch 11/100. Took 0.54662 seconds. 
  Full-batch training loss = 0.211817, test loss = 0.267963
  Training set accuracy = 0.937900, Test set accuracy = 0.917500
 epoch 12/100. Took 0.54495 seconds. 
  Full-batch training loss = 0.184652, test loss = 0.244840
  Training set accuracy = 0.944900, Test set accuracy = 0.926100
 epoch 13/100. Took 0.54527 seconds. 
  Full-batch training loss = 0.157249, test loss = 0.229665
  Training set accuracy = 0.951700, Test set accuracy = 0.929500
 epoch 14/100. Took 0.54539 seconds. 
  Full-batch training loss = 0.127156, test loss = 0.209883
  Training set accuracy = 0.965400, Test set accuracy = 0.937700
 epoch 15/100. Took 0.53963 seconds. 
  Full-batch training loss = 0.149915, test loss = 0.250230
  Training set accuracy = 0.958000, Test set accuracy = 0.928500
 epoch 16/100. Took 0.54931 seconds. 
  Full-batch training loss = 0.111889, test loss = 0.216595
  Training set accuracy = 0.968300, Test set accuracy = 0.935600
 epoch 17/100. Took 0.54515 seconds. 
  Full-batch training loss = 0.105681, test loss = 0.213990
  Training set accuracy = 0.969200, Test set accuracy = 0.935500
 epoch 18/100. Took 0.54836 seconds. 
  Full-batch training loss = 0.089293, test loss = 0.205043
  Training set accuracy = 0.974400, Test set accuracy = 0.939000
 epoch 19/100. Took 0.54675 seconds. 
  Full-batch training loss = 0.119281, test loss = 0.237918
  Training set accuracy = 0.960400, Test set accuracy = 0.930700
 epoch 20/100. Took 0.5483 seconds. 
  Full-batch training loss = 0.073105, test loss = 0.196125
  Training set accuracy = 0.980300, Test set accuracy = 0.943300
 epoch 21/100. Took 0.54709 seconds. 
  Full-batch training loss = 0.062170, test loss = 0.187551
  Training set accuracy = 0.982600, Test set accuracy = 0.945000
 epoch 22/100. Took 0.54506 seconds. 
  Full-batch training loss = 0.058264, test loss = 0.195984
  Training set accuracy = 0.984600, Test set accuracy = 0.945900
 epoch 23/100. Took 0.54455 seconds. 
  Full-batch training loss = 0.064656, test loss = 0.199319
  Training set accuracy = 0.980000, Test set accuracy = 0.943900
 epoch 24/100. Took 0.54463 seconds. 
  Full-batch training loss = 0.046678, test loss = 0.191693
  Training set accuracy = 0.987000, Test set accuracy = 0.946400
 epoch 25/100. Took 0.54684 seconds. 
  Full-batch training loss = 0.041661, test loss = 0.184662
  Training set accuracy = 0.989000, Test set accuracy = 0.952100
 epoch 26/100. Took 0.54516 seconds. 
  Full-batch training loss = 0.030047, test loss = 0.181090
  Training set accuracy = 0.993500, Test set accuracy = 0.951100
 epoch 27/100. Took 0.54641 seconds. 
  Full-batch training loss = 0.035287, test loss = 0.193418
  Training set accuracy = 0.991000, Test set accuracy = 0.947700
 epoch 28/100. Took 0.54702 seconds. 
  Full-batch training loss = 0.024476, test loss = 0.189278
  Training set accuracy = 0.994900, Test set accuracy = 0.949500
 epoch 29/100. Took 0.54481 seconds. 
  Full-batch training loss = 0.021888, test loss = 0.187802
  Training set accuracy = 0.995900, Test set accuracy = 0.950800
 epoch 30/100. Took 0.54927 seconds. 
  Full-batch training loss = 0.021069, test loss = 0.189761
  Training set accuracy = 0.995600, Test set accuracy = 0.950400
 epoch 31/100. Took 0.54569 seconds. 
  Full-batch training loss = 0.019826, test loss = 0.195038
  Training set accuracy = 0.996400, Test set accuracy = 0.950000
 epoch 32/100. Took 0.54559 seconds. 
  Full-batch training loss = 0.012638, test loss = 0.185794
  Training set accuracy = 0.998100, Test set accuracy = 0.953700
 epoch 33/100. Took 0.54232 seconds. 
  Full-batch training loss = 0.011602, test loss = 0.186585
  Training set accuracy = 0.998300, Test set accuracy = 0.952900
 epoch 34/100. Took 0.54809 seconds. 
  Full-batch training loss = 0.010600, test loss = 0.185519
  Training set accuracy = 0.998900, Test set accuracy = 0.954800
 epoch 35/100. Took 0.54764 seconds. 
  Full-batch training loss = 0.010887, test loss = 0.195411
  Training set accuracy = 0.998600, Test set accuracy = 0.952100
 epoch 36/100. Took 0.54766 seconds. 
  Full-batch training loss = 0.006685, test loss = 0.185137
  Training set accuracy = 0.999600, Test set accuracy = 0.954700
 epoch 37/100. Took 0.5441 seconds. 
  Full-batch training loss = 0.007134, test loss = 0.199685
  Training set accuracy = 0.999700, Test set accuracy = 0.954200
 epoch 38/100. Took 0.54493 seconds. 
  Full-batch training loss = 0.005363, test loss = 0.191068
  Training set accuracy = 0.999900, Test set accuracy = 0.955700
 epoch 39/100. Took 0.54483 seconds. 
  Full-batch training loss = 0.004991, test loss = 0.194821
  Training set accuracy = 0.999900, Test set accuracy = 0.954200
 epoch 40/100. Took 0.54561 seconds. 
  Full-batch training loss = 0.004421, test loss = 0.195419
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 41/100. Took 0.54501 seconds. 
  Full-batch training loss = 0.004337, test loss = 0.198439
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 42/100. Took 0.54499 seconds. 
  Full-batch training loss = 0.003579, test loss = 0.194493
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 43/100. Took 0.54962 seconds. 
  Full-batch training loss = 0.003758, test loss = 0.198702
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 44/100. Took 0.54652 seconds. 
  Full-batch training loss = 0.003071, test loss = 0.200468
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 45/100. Took 0.5455 seconds. 
  Full-batch training loss = 0.002916, test loss = 0.198875
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 46/100. Took 0.54652 seconds. 
  Full-batch training loss = 0.002811, test loss = 0.199749
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 47/100. Took 0.54648 seconds. 
  Full-batch training loss = 0.002471, test loss = 0.201071
  Training set accuracy = 1.000000, Test set accuracy = 0.956100
 epoch 48/100. Took 0.54784 seconds. 
  Full-batch training loss = 0.002496, test loss = 0.202508
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 49/100. Took 0.5454 seconds. 
  Full-batch training loss = 0.002152, test loss = 0.202408
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 50/100. Took 0.5499 seconds. 
  Full-batch training loss = 0.002143, test loss = 0.202872
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 51/100. Took 0.54444 seconds. 
  Full-batch training loss = 0.001983, test loss = 0.203817
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 52/100. Took 0.5468 seconds. 
  Full-batch training loss = 0.001892, test loss = 0.205758
  Training set accuracy = 1.000000, Test set accuracy = 0.956800
 epoch 53/100. Took 0.54528 seconds. 
  Full-batch training loss = 0.001856, test loss = 0.204822
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 54/100. Took 0.54789 seconds. 
  Full-batch training loss = 0.001710, test loss = 0.206261
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 55/100. Took 0.54811 seconds. 
  Full-batch training loss = 0.001624, test loss = 0.205503
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 56/100. Took 0.54513 seconds. 
  Full-batch training loss = 0.001581, test loss = 0.207398
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 57/100. Took 0.54276 seconds. 
  Full-batch training loss = 0.001509, test loss = 0.207159
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 58/100. Took 0.54764 seconds. 
  Full-batch training loss = 0.001474, test loss = 0.208094
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 59/100. Took 0.54641 seconds. 
  Full-batch training loss = 0.001464, test loss = 0.210779
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 60/100. Took 0.54204 seconds. 
  Full-batch training loss = 0.001380, test loss = 0.209904
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 61/100. Took 0.54092 seconds. 
  Full-batch training loss = 0.001297, test loss = 0.210065
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 62/100. Took 0.54716 seconds. 
  Full-batch training loss = 0.001263, test loss = 0.211292
  Training set accuracy = 1.000000, Test set accuracy = 0.956100
 epoch 63/100. Took 0.54395 seconds. 
  Full-batch training loss = 0.001214, test loss = 0.211856
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 64/100. Took 0.54299 seconds. 
  Full-batch training loss = 0.001182, test loss = 0.212441
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 65/100. Took 0.6031 seconds. 
  Full-batch training loss = 0.001157, test loss = 0.212906
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 66/100. Took 0.54495 seconds. 
  Full-batch training loss = 0.001118, test loss = 0.213239
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 67/100. Took 0.54555 seconds. 
  Full-batch training loss = 0.001084, test loss = 0.214207
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 68/100. Took 0.54243 seconds. 
  Full-batch training loss = 0.001111, test loss = 0.216261
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 69/100. Took 0.54567 seconds. 
  Full-batch training loss = 0.001039, test loss = 0.214671
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 70/100. Took 0.54537 seconds. 
  Full-batch training loss = 0.001030, test loss = 0.215175
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 71/100. Took 0.54369 seconds. 
  Full-batch training loss = 0.000977, test loss = 0.215568
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 72/100. Took 0.54552 seconds. 
  Full-batch training loss = 0.000955, test loss = 0.216314
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 73/100. Took 0.54299 seconds. 
  Full-batch training loss = 0.000933, test loss = 0.217155
  Training set accuracy = 1.000000, Test set accuracy = 0.957000
 epoch 74/100. Took 0.54536 seconds. 
  Full-batch training loss = 0.000922, test loss = 0.217928
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 75/100. Took 0.55128 seconds. 
  Full-batch training loss = 0.000896, test loss = 0.219065
  Training set accuracy = 1.000000, Test set accuracy = 0.957000
 epoch 76/100. Took 0.54999 seconds. 
  Full-batch training loss = 0.000876, test loss = 0.218401
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 77/100. Took 0.54456 seconds. 
  Full-batch training loss = 0.000853, test loss = 0.219991
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 78/100. Took 0.5462 seconds. 
  Full-batch training loss = 0.000825, test loss = 0.219096
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 79/100. Took 0.54498 seconds. 
  Full-batch training loss = 0.000803, test loss = 0.219936
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 80/100. Took 0.54535 seconds. 
  Full-batch training loss = 0.000792, test loss = 0.220228
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 81/100. Took 0.5471 seconds. 
  Full-batch training loss = 0.000771, test loss = 0.220608
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 82/100. Took 0.54513 seconds. 
  Full-batch training loss = 0.000762, test loss = 0.221611
  Training set accuracy = 1.000000, Test set accuracy = 0.956900
 epoch 83/100. Took 0.54732 seconds. 
  Full-batch training loss = 0.000743, test loss = 0.220794
  Training set accuracy = 1.000000, Test set accuracy = 0.956800
 epoch 84/100. Took 0.54669 seconds. 
  Full-batch training loss = 0.000756, test loss = 0.220288
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 85/100. Took 0.54733 seconds. 
  Full-batch training loss = 0.000716, test loss = 0.222364
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 86/100. Took 0.54559 seconds. 
  Full-batch training loss = 0.000699, test loss = 0.222430
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 87/100. Took 0.54758 seconds. 
  Full-batch training loss = 0.000697, test loss = 0.221823
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 88/100. Took 0.54873 seconds. 
  Full-batch training loss = 0.000669, test loss = 0.222327
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 89/100. Took 0.54765 seconds. 
  Full-batch training loss = 0.000653, test loss = 0.222894
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 90/100. Took 0.54611 seconds. 
  Full-batch training loss = 0.000644, test loss = 0.223083
  Training set accuracy = 1.000000, Test set accuracy = 0.956200
 epoch 91/100. Took 0.54459 seconds. 
  Full-batch training loss = 0.000634, test loss = 0.223770
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 92/100. Took 0.55707 seconds. 
  Full-batch training loss = 0.000628, test loss = 0.223652
  Training set accuracy = 1.000000, Test set accuracy = 0.957000
 epoch 93/100. Took 0.54763 seconds. 
  Full-batch training loss = 0.000609, test loss = 0.224081
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 94/100. Took 0.54489 seconds. 
  Full-batch training loss = 0.000601, test loss = 0.225460
  Training set accuracy = 1.000000, Test set accuracy = 0.956500
 epoch 95/100. Took 0.54351 seconds. 
  Full-batch training loss = 0.000593, test loss = 0.225481
  Training set accuracy = 1.000000, Test set accuracy = 0.956600
 epoch 96/100. Took 0.54455 seconds. 
  Full-batch training loss = 0.000579, test loss = 0.225122
  Training set accuracy = 1.000000, Test set accuracy = 0.956100
 epoch 97/100. Took 0.54617 seconds. 
  Full-batch training loss = 0.000568, test loss = 0.225327
  Training set accuracy = 1.000000, Test set accuracy = 0.956700
 epoch 98/100. Took 0.5447 seconds. 
  Full-batch training loss = 0.000565, test loss = 0.225986
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
 epoch 99/100. Took 0.54328 seconds. 
  Full-batch training loss = 0.000550, test loss = 0.226046
  Training set accuracy = 1.000000, Test set accuracy = 0.956300
 epoch 100/100. Took 0.54694 seconds. 
  Full-batch training loss = 0.000548, test loss = 0.226424
  Training set accuracy = 1.000000, Test set accuracy = 0.956400
Elapsed time is 110.832014 seconds.
End Training
