Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.05
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.54346 seconds. Average reconstruction error is: 33.1543
 epoch 2/20. Took 0.5449 seconds. Average reconstruction error is: 18.9951
 epoch 3/20. Took 0.54596 seconds. Average reconstruction error is: 16.0771
 epoch 4/20. Took 0.54639 seconds. Average reconstruction error is: 14.5634
 epoch 5/20. Took 0.54929 seconds. Average reconstruction error is: 13.5471
 epoch 6/20. Took 0.54204 seconds. Average reconstruction error is: 12.8022
 epoch 7/20. Took 0.54807 seconds. Average reconstruction error is: 12.1929
 epoch 8/20. Took 0.54649 seconds. Average reconstruction error is: 11.6871
 epoch 9/20. Took 0.54491 seconds. Average reconstruction error is: 11.3012
 epoch 10/20. Took 0.55212 seconds. Average reconstruction error is: 10.9358
 epoch 11/20. Took 0.54429 seconds. Average reconstruction error is: 10.629
 epoch 12/20. Took 0.54528 seconds. Average reconstruction error is: 10.4185
 epoch 13/20. Took 0.55017 seconds. Average reconstruction error is: 10.1764
 epoch 14/20. Took 0.54681 seconds. Average reconstruction error is: 9.9695
 epoch 15/20. Took 0.54687 seconds. Average reconstruction error is: 9.791
 epoch 16/20. Took 0.55071 seconds. Average reconstruction error is: 9.6511
 epoch 17/20. Took 0.54533 seconds. Average reconstruction error is: 9.5272
 epoch 18/20. Took 0.54895 seconds. Average reconstruction error is: 9.3947
 epoch 19/20. Took 0.54731 seconds. Average reconstruction error is: 9.2948
 epoch 20/20. Took 0.54899 seconds. Average reconstruction error is: 9.19
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.19068 seconds. Average reconstruction error is: 19.278
 epoch 2/20. Took 0.19487 seconds. Average reconstruction error is: 11.1668
 epoch 3/20. Took 0.19318 seconds. Average reconstruction error is: 9.3742
 epoch 4/20. Took 0.19309 seconds. Average reconstruction error is: 8.4962
 epoch 5/20. Took 0.19454 seconds. Average reconstruction error is: 7.9663
 epoch 6/20. Took 0.1956 seconds. Average reconstruction error is: 7.6404
 epoch 7/20. Took 0.19477 seconds. Average reconstruction error is: 7.4036
 epoch 8/20. Took 0.19601 seconds. Average reconstruction error is: 7.1744
 epoch 9/20. Took 0.19485 seconds. Average reconstruction error is: 7.0313
 epoch 10/20. Took 0.1963 seconds. Average reconstruction error is: 6.8604
 epoch 11/20. Took 0.19269 seconds. Average reconstruction error is: 6.7945
 epoch 12/20. Took 0.19505 seconds. Average reconstruction error is: 6.6927
 epoch 13/20. Took 0.19565 seconds. Average reconstruction error is: 6.616
 epoch 14/20. Took 0.19573 seconds. Average reconstruction error is: 6.5423
 epoch 15/20. Took 0.19501 seconds. Average reconstruction error is: 6.4884
 epoch 16/20. Took 0.19407 seconds. Average reconstruction error is: 6.3975
 epoch 17/20. Took 0.19621 seconds. Average reconstruction error is: 6.3654
 epoch 18/20. Took 0.19468 seconds. Average reconstruction error is: 6.3004
 epoch 19/20. Took 0.19484 seconds. Average reconstruction error is: 6.2345
 epoch 20/20. Took 0.19385 seconds. Average reconstruction error is: 6.2035
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.19206 seconds. Average reconstruction error is: 15.9683
 epoch 2/20. Took 0.19225 seconds. Average reconstruction error is: 8.5391
 epoch 3/20. Took 0.19337 seconds. Average reconstruction error is: 7.1371
 epoch 4/20. Took 0.19252 seconds. Average reconstruction error is: 6.4711
 epoch 5/20. Took 0.19381 seconds. Average reconstruction error is: 6.1229
 epoch 6/20. Took 0.19195 seconds. Average reconstruction error is: 5.9104
 epoch 7/20. Took 0.19298 seconds. Average reconstruction error is: 5.7501
 epoch 8/20. Took 0.19499 seconds. Average reconstruction error is: 5.6273
 epoch 9/20. Took 0.19409 seconds. Average reconstruction error is: 5.5396
 epoch 10/20. Took 0.19428 seconds. Average reconstruction error is: 5.5101
 epoch 11/20. Took 0.19393 seconds. Average reconstruction error is: 5.4393
 epoch 12/20. Took 0.19546 seconds. Average reconstruction error is: 5.345
 epoch 13/20. Took 0.19216 seconds. Average reconstruction error is: 5.3387
 epoch 14/20. Took 0.19336 seconds. Average reconstruction error is: 5.3145
 epoch 15/20. Took 0.19495 seconds. Average reconstruction error is: 5.2997
 epoch 16/20. Took 0.19592 seconds. Average reconstruction error is: 5.2669
 epoch 17/20. Took 0.19421 seconds. Average reconstruction error is: 5.2555
 epoch 18/20. Took 0.19434 seconds. Average reconstruction error is: 5.2029
 epoch 19/20. Took 0.19326 seconds. Average reconstruction error is: 5.17
 epoch 20/20. Took 0.19502 seconds. Average reconstruction error is: 5.185
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.54293 seconds. 
  Full-batch training loss = 0.269315, test loss = 0.276850
  Training set accuracy = 0.921900, Test set accuracy = 0.917900
 epoch 2/100. Took 0.54598 seconds. 
  Full-batch training loss = 0.202813, test loss = 0.224592
  Training set accuracy = 0.943200, Test set accuracy = 0.934700
 epoch 3/100. Took 0.54576 seconds. 
  Full-batch training loss = 0.172462, test loss = 0.205402
  Training set accuracy = 0.951900, Test set accuracy = 0.940500
 epoch 4/100. Took 0.54567 seconds. 
  Full-batch training loss = 0.148529, test loss = 0.195687
  Training set accuracy = 0.956700, Test set accuracy = 0.943100
 epoch 5/100. Took 0.54559 seconds. 
  Full-batch training loss = 0.123137, test loss = 0.188492
  Training set accuracy = 0.965600, Test set accuracy = 0.943600
 epoch 6/100. Took 0.54828 seconds. 
  Full-batch training loss = 0.109496, test loss = 0.183181
  Training set accuracy = 0.971200, Test set accuracy = 0.946100
 epoch 7/100. Took 0.54748 seconds. 
  Full-batch training loss = 0.095687, test loss = 0.177330
  Training set accuracy = 0.976000, Test set accuracy = 0.947200
 epoch 8/100. Took 0.54835 seconds. 
  Full-batch training loss = 0.085097, test loss = 0.169563
  Training set accuracy = 0.979200, Test set accuracy = 0.949800
 epoch 9/100. Took 0.54828 seconds. 
  Full-batch training loss = 0.073593, test loss = 0.167784
  Training set accuracy = 0.983800, Test set accuracy = 0.949200
 epoch 10/100. Took 0.54952 seconds. 
  Full-batch training loss = 0.067305, test loss = 0.163010
  Training set accuracy = 0.984800, Test set accuracy = 0.951200
 epoch 11/100. Took 0.54876 seconds. 
  Full-batch training loss = 0.061615, test loss = 0.167168
  Training set accuracy = 0.986400, Test set accuracy = 0.950800
 epoch 12/100. Took 0.55009 seconds. 
  Full-batch training loss = 0.052109, test loss = 0.163876
  Training set accuracy = 0.989800, Test set accuracy = 0.951600
 epoch 13/100. Took 0.54793 seconds. 
  Full-batch training loss = 0.047620, test loss = 0.160499
  Training set accuracy = 0.991300, Test set accuracy = 0.953100
 epoch 14/100. Took 0.5473 seconds. 
  Full-batch training loss = 0.042737, test loss = 0.165809
  Training set accuracy = 0.992500, Test set accuracy = 0.952200
 epoch 15/100. Took 0.54661 seconds. 
  Full-batch training loss = 0.036890, test loss = 0.158529
  Training set accuracy = 0.995400, Test set accuracy = 0.953600
 epoch 16/100. Took 0.54563 seconds. 
  Full-batch training loss = 0.034246, test loss = 0.162498
  Training set accuracy = 0.994700, Test set accuracy = 0.953600
 epoch 17/100. Took 0.54572 seconds. 
  Full-batch training loss = 0.030681, test loss = 0.158106
  Training set accuracy = 0.996000, Test set accuracy = 0.954700
 epoch 18/100. Took 0.55063 seconds. 
  Full-batch training loss = 0.026825, test loss = 0.157005
  Training set accuracy = 0.996600, Test set accuracy = 0.956400
 epoch 19/100. Took 0.54701 seconds. 
  Full-batch training loss = 0.026734, test loss = 0.164249
  Training set accuracy = 0.997400, Test set accuracy = 0.953700
 epoch 20/100. Took 0.54848 seconds. 
  Full-batch training loss = 0.021837, test loss = 0.157677
  Training set accuracy = 0.997800, Test set accuracy = 0.955800
 epoch 21/100. Took 0.54608 seconds. 
  Full-batch training loss = 0.020286, test loss = 0.158388
  Training set accuracy = 0.997700, Test set accuracy = 0.956600
 epoch 22/100. Took 0.54852 seconds. 
  Full-batch training loss = 0.018571, test loss = 0.159779
  Training set accuracy = 0.998600, Test set accuracy = 0.955700
 epoch 23/100. Took 0.5476 seconds. 
  Full-batch training loss = 0.016775, test loss = 0.159350
  Training set accuracy = 0.999000, Test set accuracy = 0.956000
 epoch 24/100. Took 0.54721 seconds. 
  Full-batch training loss = 0.015852, test loss = 0.159826
  Training set accuracy = 0.999000, Test set accuracy = 0.956700
 epoch 25/100. Took 0.54697 seconds. 
  Full-batch training loss = 0.014273, test loss = 0.158674
  Training set accuracy = 0.999200, Test set accuracy = 0.956900
 epoch 26/100. Took 0.54598 seconds. 
  Full-batch training loss = 0.013688, test loss = 0.162315
  Training set accuracy = 0.999600, Test set accuracy = 0.955700
 epoch 27/100. Took 0.54752 seconds. 
  Full-batch training loss = 0.012333, test loss = 0.163399
  Training set accuracy = 0.999600, Test set accuracy = 0.956700
 epoch 28/100. Took 0.54923 seconds. 
  Full-batch training loss = 0.011398, test loss = 0.160321
  Training set accuracy = 0.999700, Test set accuracy = 0.957500
 epoch 29/100. Took 0.54867 seconds. 
  Full-batch training loss = 0.010595, test loss = 0.160502
  Training set accuracy = 0.999500, Test set accuracy = 0.957000
 epoch 30/100. Took 0.5482 seconds. 
  Full-batch training loss = 0.009916, test loss = 0.163693
  Training set accuracy = 0.999700, Test set accuracy = 0.956300
 epoch 31/100. Took 0.5477 seconds. 
  Full-batch training loss = 0.009088, test loss = 0.163288
  Training set accuracy = 0.999700, Test set accuracy = 0.957700
 epoch 32/100. Took 0.54751 seconds. 
  Full-batch training loss = 0.008739, test loss = 0.162960
  Training set accuracy = 0.999700, Test set accuracy = 0.958200
 epoch 33/100. Took 0.5456 seconds. 
  Full-batch training loss = 0.008157, test loss = 0.163660
  Training set accuracy = 0.999900, Test set accuracy = 0.957600
 epoch 34/100. Took 0.54712 seconds. 
  Full-batch training loss = 0.007497, test loss = 0.162717
  Training set accuracy = 0.999900, Test set accuracy = 0.958500
 epoch 35/100. Took 0.54807 seconds. 
  Full-batch training loss = 0.007158, test loss = 0.163839
  Training set accuracy = 0.999800, Test set accuracy = 0.957700
 epoch 36/100. Took 0.54663 seconds. 
  Full-batch training loss = 0.006807, test loss = 0.165080
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 37/100. Took 0.54831 seconds. 
  Full-batch training loss = 0.006552, test loss = 0.167067
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 38/100. Took 0.54422 seconds. 
  Full-batch training loss = 0.006097, test loss = 0.166633
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 39/100. Took 0.54571 seconds. 
  Full-batch training loss = 0.006022, test loss = 0.169428
  Training set accuracy = 1.000000, Test set accuracy = 0.957400
 epoch 40/100. Took 0.54845 seconds. 
  Full-batch training loss = 0.005505, test loss = 0.166932
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 41/100. Took 0.54838 seconds. 
  Full-batch training loss = 0.005317, test loss = 0.166679
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 42/100. Took 0.54913 seconds. 
  Full-batch training loss = 0.005098, test loss = 0.168260
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 43/100. Took 0.55098 seconds. 
  Full-batch training loss = 0.004869, test loss = 0.166890
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 44/100. Took 0.54875 seconds. 
  Full-batch training loss = 0.004664, test loss = 0.169053
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 45/100. Took 0.54869 seconds. 
  Full-batch training loss = 0.004512, test loss = 0.168365
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 46/100. Took 0.54813 seconds. 
  Full-batch training loss = 0.004333, test loss = 0.168918
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 47/100. Took 0.54829 seconds. 
  Full-batch training loss = 0.004204, test loss = 0.169682
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 48/100. Took 0.55288 seconds. 
  Full-batch training loss = 0.004009, test loss = 0.170228
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 49/100. Took 0.57593 seconds. 
  Full-batch training loss = 0.003893, test loss = 0.169808
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 50/100. Took 0.55027 seconds. 
  Full-batch training loss = 0.003759, test loss = 0.171101
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 51/100. Took 0.54656 seconds. 
  Full-batch training loss = 0.003631, test loss = 0.170623
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 52/100. Took 0.54779 seconds. 
  Full-batch training loss = 0.003516, test loss = 0.171680
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 53/100. Took 0.54679 seconds. 
  Full-batch training loss = 0.003405, test loss = 0.171792
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 54/100. Took 0.55043 seconds. 
  Full-batch training loss = 0.003295, test loss = 0.172549
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 55/100. Took 0.5463 seconds. 
  Full-batch training loss = 0.003197, test loss = 0.172383
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 56/100. Took 0.54664 seconds. 
  Full-batch training loss = 0.003106, test loss = 0.172928
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 57/100. Took 0.54462 seconds. 
  Full-batch training loss = 0.003020, test loss = 0.172978
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 58/100. Took 0.54745 seconds. 
  Full-batch training loss = 0.002954, test loss = 0.173097
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 59/100. Took 0.54755 seconds. 
  Full-batch training loss = 0.002873, test loss = 0.173207
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 60/100. Took 0.5489 seconds. 
  Full-batch training loss = 0.002792, test loss = 0.173736
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 61/100. Took 0.54869 seconds. 
  Full-batch training loss = 0.002706, test loss = 0.174322
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 62/100. Took 0.54883 seconds. 
  Full-batch training loss = 0.002641, test loss = 0.174702
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 63/100. Took 0.54842 seconds. 
  Full-batch training loss = 0.002583, test loss = 0.174918
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 64/100. Took 0.54654 seconds. 
  Full-batch training loss = 0.002513, test loss = 0.175508
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 65/100. Took 0.55299 seconds. 
  Full-batch training loss = 0.002453, test loss = 0.175390
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 66/100. Took 0.54818 seconds. 
  Full-batch training loss = 0.002403, test loss = 0.176206
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 67/100. Took 0.54798 seconds. 
  Full-batch training loss = 0.002344, test loss = 0.175660
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 68/100. Took 0.54604 seconds. 
  Full-batch training loss = 0.002286, test loss = 0.176188
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 69/100. Took 0.54806 seconds. 
  Full-batch training loss = 0.002251, test loss = 0.176721
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 70/100. Took 0.54835 seconds. 
  Full-batch training loss = 0.002192, test loss = 0.176795
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 71/100. Took 0.54829 seconds. 
  Full-batch training loss = 0.002147, test loss = 0.177279
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 72/100. Took 0.54715 seconds. 
  Full-batch training loss = 0.002101, test loss = 0.177251
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 73/100. Took 0.54848 seconds. 
  Full-batch training loss = 0.002053, test loss = 0.177604
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 74/100. Took 0.54801 seconds. 
  Full-batch training loss = 0.002012, test loss = 0.177670
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 75/100. Took 0.54638 seconds. 
  Full-batch training loss = 0.001982, test loss = 0.178223
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 76/100. Took 0.55085 seconds. 
  Full-batch training loss = 0.001939, test loss = 0.178174
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 77/100. Took 0.5485 seconds. 
  Full-batch training loss = 0.001897, test loss = 0.178073
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 78/100. Took 0.54616 seconds. 
  Full-batch training loss = 0.001861, test loss = 0.178509
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 79/100. Took 0.54684 seconds. 
  Full-batch training loss = 0.001830, test loss = 0.179025
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 80/100. Took 0.55203 seconds. 
  Full-batch training loss = 0.001799, test loss = 0.178838
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 81/100. Took 0.54812 seconds. 
  Full-batch training loss = 0.001764, test loss = 0.179306
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 82/100. Took 0.54729 seconds. 
  Full-batch training loss = 0.001735, test loss = 0.179138
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 83/100. Took 0.54644 seconds. 
  Full-batch training loss = 0.001701, test loss = 0.179560
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 84/100. Took 0.5458 seconds. 
  Full-batch training loss = 0.001671, test loss = 0.179885
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 85/100. Took 0.54616 seconds. 
  Full-batch training loss = 0.001641, test loss = 0.180078
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 86/100. Took 0.54943 seconds. 
  Full-batch training loss = 0.001616, test loss = 0.180121
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 87/100. Took 0.54723 seconds. 
  Full-batch training loss = 0.001589, test loss = 0.180430
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 88/100. Took 0.54639 seconds. 
  Full-batch training loss = 0.001567, test loss = 0.180728
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 89/100. Took 0.55111 seconds. 
  Full-batch training loss = 0.001536, test loss = 0.181024
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 90/100. Took 0.55104 seconds. 
  Full-batch training loss = 0.001512, test loss = 0.181555
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 91/100. Took 0.54932 seconds. 
  Full-batch training loss = 0.001487, test loss = 0.181684
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 92/100. Took 0.55116 seconds. 
  Full-batch training loss = 0.001466, test loss = 0.181532
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 93/100. Took 0.55502 seconds. 
  Full-batch training loss = 0.001441, test loss = 0.181517
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 94/100. Took 0.54987 seconds. 
  Full-batch training loss = 0.001421, test loss = 0.181961
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 95/100. Took 0.54842 seconds. 
  Full-batch training loss = 0.001399, test loss = 0.182297
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 96/100. Took 0.54815 seconds. 
  Full-batch training loss = 0.001379, test loss = 0.182022
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 97/100. Took 0.54841 seconds. 
  Full-batch training loss = 0.001357, test loss = 0.182451
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 98/100. Took 0.61989 seconds. 
  Full-batch training loss = 0.001337, test loss = 0.182665
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 99/100. Took 0.54812 seconds. 
  Full-batch training loss = 0.001323, test loss = 0.182548
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 100/100. Took 0.54708 seconds. 
  Full-batch training loss = 0.001300, test loss = 0.183061
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
Elapsed time is 130.981048 seconds.
End Training
