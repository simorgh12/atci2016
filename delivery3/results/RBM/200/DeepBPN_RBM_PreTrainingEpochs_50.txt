Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	50
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.57107 seconds. Average reconstruction error is: 31.0374
 epoch 2/50. Took 0.56683 seconds. Average reconstruction error is: 18.6099
 epoch 3/50. Took 0.5636 seconds. Average reconstruction error is: 16.0887
 epoch 4/50. Took 0.58002 seconds. Average reconstruction error is: 14.592
 epoch 5/50. Took 0.57921 seconds. Average reconstruction error is: 13.4846
 epoch 6/50. Took 0.57762 seconds. Average reconstruction error is: 12.6799
 epoch 7/50. Took 0.58124 seconds. Average reconstruction error is: 12.0616
 epoch 8/50. Took 0.57847 seconds. Average reconstruction error is: 11.5579
 epoch 9/50. Took 0.56841 seconds. Average reconstruction error is: 11.2495
 epoch 10/50. Took 0.5606 seconds. Average reconstruction error is: 10.9443
 epoch 11/50. Took 0.5762 seconds. Average reconstruction error is: 10.685
 epoch 12/50. Took 0.583 seconds. Average reconstruction error is: 10.5011
 epoch 13/50. Took 0.57849 seconds. Average reconstruction error is: 10.2836
 epoch 14/50. Took 0.5687 seconds. Average reconstruction error is: 10.1316
 epoch 15/50. Took 0.56146 seconds. Average reconstruction error is: 10.025
 epoch 16/50. Took 0.56418 seconds. Average reconstruction error is: 9.9239
 epoch 17/50. Took 0.55724 seconds. Average reconstruction error is: 9.8268
 epoch 18/50. Took 0.5598 seconds. Average reconstruction error is: 9.6941
 epoch 19/50. Took 0.55653 seconds. Average reconstruction error is: 9.6246
 epoch 20/50. Took 0.56456 seconds. Average reconstruction error is: 9.5299
 epoch 21/50. Took 0.57553 seconds. Average reconstruction error is: 9.4474
 epoch 22/50. Took 0.56988 seconds. Average reconstruction error is: 9.3965
 epoch 23/50. Took 0.57857 seconds. Average reconstruction error is: 9.313
 epoch 24/50. Took 0.5787 seconds. Average reconstruction error is: 9.2347
 epoch 25/50. Took 0.57765 seconds. Average reconstruction error is: 9.2022
 epoch 26/50. Took 0.57995 seconds. Average reconstruction error is: 9.1237
 epoch 27/50. Took 0.58085 seconds. Average reconstruction error is: 9.0899
 epoch 28/50. Took 0.575 seconds. Average reconstruction error is: 9.0613
 epoch 29/50. Took 0.58055 seconds. Average reconstruction error is: 9.0289
 epoch 30/50. Took 0.56599 seconds. Average reconstruction error is: 8.9659
 epoch 31/50. Took 0.55951 seconds. Average reconstruction error is: 8.9682
 epoch 32/50. Took 0.56767 seconds. Average reconstruction error is: 8.9456
 epoch 33/50. Took 0.55502 seconds. Average reconstruction error is: 8.8569
 epoch 34/50. Took 0.56218 seconds. Average reconstruction error is: 8.8515
 epoch 35/50. Took 0.55729 seconds. Average reconstruction error is: 8.8039
 epoch 36/50. Took 0.55897 seconds. Average reconstruction error is: 8.7484
 epoch 37/50. Took 0.56168 seconds. Average reconstruction error is: 8.7443
 epoch 38/50. Took 0.56123 seconds. Average reconstruction error is: 8.7162
 epoch 39/50. Took 0.56449 seconds. Average reconstruction error is: 8.683
 epoch 40/50. Took 0.57261 seconds. Average reconstruction error is: 8.6473
 epoch 41/50. Took 0.56055 seconds. Average reconstruction error is: 8.659
 epoch 42/50. Took 0.55817 seconds. Average reconstruction error is: 8.6135
 epoch 43/50. Took 0.5594 seconds. Average reconstruction error is: 8.5991
 epoch 44/50. Took 0.56134 seconds. Average reconstruction error is: 8.5827
 epoch 45/50. Took 0.55987 seconds. Average reconstruction error is: 8.5652
 epoch 46/50. Took 0.57817 seconds. Average reconstruction error is: 8.567
 epoch 47/50. Took 0.5778 seconds. Average reconstruction error is: 8.5061
 epoch 48/50. Took 0.58249 seconds. Average reconstruction error is: 8.5036
 epoch 49/50. Took 0.5802 seconds. Average reconstruction error is: 8.4906
 epoch 50/50. Took 0.57983 seconds. Average reconstruction error is: 8.4809
Training binary-binary RBM in layer 2 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.20083 seconds. Average reconstruction error is: 15.5721
 epoch 2/50. Took 0.20135 seconds. Average reconstruction error is: 9.6538
 epoch 3/50. Took 0.20397 seconds. Average reconstruction error is: 8.5837
 epoch 4/50. Took 0.20257 seconds. Average reconstruction error is: 8.0172
 epoch 5/50. Took 0.20297 seconds. Average reconstruction error is: 7.6837
 epoch 6/50. Took 0.20182 seconds. Average reconstruction error is: 7.3889
 epoch 7/50. Took 0.20345 seconds. Average reconstruction error is: 7.1702
 epoch 8/50. Took 0.20251 seconds. Average reconstruction error is: 7.0307
 epoch 9/50. Took 0.2039 seconds. Average reconstruction error is: 6.865
 epoch 10/50. Took 0.20337 seconds. Average reconstruction error is: 6.7429
 epoch 11/50. Took 0.20411 seconds. Average reconstruction error is: 6.5995
 epoch 12/50. Took 0.20289 seconds. Average reconstruction error is: 6.5297
 epoch 13/50. Took 0.20438 seconds. Average reconstruction error is: 6.423
 epoch 14/50. Took 0.20211 seconds. Average reconstruction error is: 6.3304
 epoch 15/50. Took 0.20309 seconds. Average reconstruction error is: 6.2447
 epoch 16/50. Took 0.20311 seconds. Average reconstruction error is: 6.1565
 epoch 17/50. Took 0.20355 seconds. Average reconstruction error is: 6.0892
 epoch 18/50. Took 0.20248 seconds. Average reconstruction error is: 6.0197
 epoch 19/50. Took 0.2013 seconds. Average reconstruction error is: 5.9621
 epoch 20/50. Took 0.20217 seconds. Average reconstruction error is: 5.8939
 epoch 21/50. Took 0.20263 seconds. Average reconstruction error is: 5.8296
 epoch 22/50. Took 0.20291 seconds. Average reconstruction error is: 5.7615
 epoch 23/50. Took 0.20278 seconds. Average reconstruction error is: 5.7022
 epoch 24/50. Took 0.20262 seconds. Average reconstruction error is: 5.6856
 epoch 25/50. Took 0.20346 seconds. Average reconstruction error is: 5.6177
 epoch 26/50. Took 0.20267 seconds. Average reconstruction error is: 5.5912
 epoch 27/50. Took 0.2048 seconds. Average reconstruction error is: 5.5351
 epoch 28/50. Took 0.20368 seconds. Average reconstruction error is: 5.4888
 epoch 29/50. Took 0.20364 seconds. Average reconstruction error is: 5.4804
 epoch 30/50. Took 0.20333 seconds. Average reconstruction error is: 5.4195
 epoch 31/50. Took 0.20397 seconds. Average reconstruction error is: 5.4227
 epoch 32/50. Took 0.20415 seconds. Average reconstruction error is: 5.3668
 epoch 33/50. Took 0.20418 seconds. Average reconstruction error is: 5.3321
 epoch 34/50. Took 0.20299 seconds. Average reconstruction error is: 5.3168
 epoch 35/50. Took 0.20317 seconds. Average reconstruction error is: 5.2587
 epoch 36/50. Took 0.20563 seconds. Average reconstruction error is: 5.2708
 epoch 37/50. Took 0.2047 seconds. Average reconstruction error is: 5.2541
 epoch 38/50. Took 0.20237 seconds. Average reconstruction error is: 5.1905
 epoch 39/50. Took 0.20632 seconds. Average reconstruction error is: 5.1546
 epoch 40/50. Took 0.2026 seconds. Average reconstruction error is: 5.1753
 epoch 41/50. Took 0.20449 seconds. Average reconstruction error is: 5.1284
 epoch 42/50. Took 0.20259 seconds. Average reconstruction error is: 5.105
 epoch 43/50. Took 0.20332 seconds. Average reconstruction error is: 5.0818
 epoch 44/50. Took 0.20317 seconds. Average reconstruction error is: 5.0734
 epoch 45/50. Took 0.20363 seconds. Average reconstruction error is: 5.0383
 epoch 46/50. Took 0.20397 seconds. Average reconstruction error is: 5.0312
 epoch 47/50. Took 0.20345 seconds. Average reconstruction error is: 5.0104
 epoch 48/50. Took 0.20371 seconds. Average reconstruction error is: 4.9882
 epoch 49/50. Took 0.20421 seconds. Average reconstruction error is: 4.9831
 epoch 50/50. Took 0.20269 seconds. Average reconstruction error is: 4.9686
Training binary-binary RBM in layer 3 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.20103 seconds. Average reconstruction error is: 12.4859
 epoch 2/50. Took 0.2018 seconds. Average reconstruction error is: 7.2685
 epoch 3/50. Took 0.20149 seconds. Average reconstruction error is: 6.4145
 epoch 4/50. Took 0.2019 seconds. Average reconstruction error is: 6.0009
 epoch 5/50. Took 0.20219 seconds. Average reconstruction error is: 5.7719
 epoch 6/50. Took 0.20256 seconds. Average reconstruction error is: 5.5839
 epoch 7/50. Took 0.20238 seconds. Average reconstruction error is: 5.4376
 epoch 8/50. Took 0.20109 seconds. Average reconstruction error is: 5.3127
 epoch 9/50. Took 0.20279 seconds. Average reconstruction error is: 5.2421
 epoch 10/50. Took 0.20359 seconds. Average reconstruction error is: 5.152
 epoch 11/50. Took 0.20724 seconds. Average reconstruction error is: 5.0768
 epoch 12/50. Took 0.20355 seconds. Average reconstruction error is: 4.9949
 epoch 13/50. Took 0.20226 seconds. Average reconstruction error is: 4.9538
 epoch 14/50. Took 0.2042 seconds. Average reconstruction error is: 4.9204
 epoch 15/50. Took 0.20332 seconds. Average reconstruction error is: 4.8449
 epoch 16/50. Took 0.20391 seconds. Average reconstruction error is: 4.8157
 epoch 17/50. Took 0.20259 seconds. Average reconstruction error is: 4.7272
 epoch 18/50. Took 0.20387 seconds. Average reconstruction error is: 4.7011
 epoch 19/50. Took 0.20265 seconds. Average reconstruction error is: 4.6687
 epoch 20/50. Took 0.20443 seconds. Average reconstruction error is: 4.6817
 epoch 21/50. Took 0.20321 seconds. Average reconstruction error is: 4.6226
 epoch 22/50. Took 0.20421 seconds. Average reconstruction error is: 4.5647
 epoch 23/50. Took 0.20169 seconds. Average reconstruction error is: 4.5398
 epoch 24/50. Took 0.20476 seconds. Average reconstruction error is: 4.5329
 epoch 25/50. Took 0.20378 seconds. Average reconstruction error is: 4.487
 epoch 26/50. Took 0.2026 seconds. Average reconstruction error is: 4.4591
 epoch 27/50. Took 0.2012 seconds. Average reconstruction error is: 4.4538
 epoch 28/50. Took 0.20271 seconds. Average reconstruction error is: 4.43
 epoch 29/50. Took 0.20407 seconds. Average reconstruction error is: 4.4078
 epoch 30/50. Took 0.20168 seconds. Average reconstruction error is: 4.3512
 epoch 31/50. Took 0.20228 seconds. Average reconstruction error is: 4.3425
 epoch 32/50. Took 0.20325 seconds. Average reconstruction error is: 4.3355
 epoch 33/50. Took 0.20474 seconds. Average reconstruction error is: 4.338
 epoch 34/50. Took 0.2051 seconds. Average reconstruction error is: 4.2955
 epoch 35/50. Took 0.20145 seconds. Average reconstruction error is: 4.2771
 epoch 36/50. Took 0.20199 seconds. Average reconstruction error is: 4.264
 epoch 37/50. Took 0.20324 seconds. Average reconstruction error is: 4.2443
 epoch 38/50. Took 0.20242 seconds. Average reconstruction error is: 4.2522
 epoch 39/50. Took 0.20447 seconds. Average reconstruction error is: 4.2268
 epoch 40/50. Took 0.20383 seconds. Average reconstruction error is: 4.2099
 epoch 41/50. Took 0.20364 seconds. Average reconstruction error is: 4.1987
 epoch 42/50. Took 0.20433 seconds. Average reconstruction error is: 4.2082
 epoch 43/50. Took 0.20443 seconds. Average reconstruction error is: 4.1836
 epoch 44/50. Took 0.20399 seconds. Average reconstruction error is: 4.1752
 epoch 45/50. Took 0.20325 seconds. Average reconstruction error is: 4.1243
 epoch 46/50. Took 0.20229 seconds. Average reconstruction error is: 4.1487
 epoch 47/50. Took 0.20202 seconds. Average reconstruction error is: 4.1294
 epoch 48/50. Took 0.20326 seconds. Average reconstruction error is: 4.1106
 epoch 49/50. Took 0.20324 seconds. Average reconstruction error is: 4.1124
 epoch 50/50. Took 0.20172 seconds. Average reconstruction error is: 4.1
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.54879 seconds. 
  Full-batch training loss = 0.300659, test loss = 0.292596
  Training set accuracy = 0.912800, Test set accuracy = 0.917800
 epoch 2/100. Took 0.55092 seconds. 
  Full-batch training loss = 0.237542, test loss = 0.245328
  Training set accuracy = 0.933900, Test set accuracy = 0.930400
 epoch 3/100. Took 0.55332 seconds. 
  Full-batch training loss = 0.195241, test loss = 0.216435
  Training set accuracy = 0.944300, Test set accuracy = 0.937700
 epoch 4/100. Took 0.55861 seconds. 
  Full-batch training loss = 0.170523, test loss = 0.205078
  Training set accuracy = 0.954500, Test set accuracy = 0.940200
 epoch 5/100. Took 0.55566 seconds. 
  Full-batch training loss = 0.143717, test loss = 0.192231
  Training set accuracy = 0.964400, Test set accuracy = 0.944500
 epoch 6/100. Took 0.55681 seconds. 
  Full-batch training loss = 0.127827, test loss = 0.183916
  Training set accuracy = 0.966600, Test set accuracy = 0.945200
 epoch 7/100. Took 0.55648 seconds. 
  Full-batch training loss = 0.110075, test loss = 0.175681
  Training set accuracy = 0.972200, Test set accuracy = 0.948900
 epoch 8/100. Took 0.55229 seconds. 
  Full-batch training loss = 0.098376, test loss = 0.168843
  Training set accuracy = 0.976600, Test set accuracy = 0.950500
 epoch 9/100. Took 0.55359 seconds. 
  Full-batch training loss = 0.086859, test loss = 0.165831
  Training set accuracy = 0.979700, Test set accuracy = 0.951200
 epoch 10/100. Took 0.55706 seconds. 
  Full-batch training loss = 0.081641, test loss = 0.168133
  Training set accuracy = 0.981900, Test set accuracy = 0.951800
 epoch 11/100. Took 0.55364 seconds. 
  Full-batch training loss = 0.070075, test loss = 0.160962
  Training set accuracy = 0.985600, Test set accuracy = 0.953900
 epoch 12/100. Took 0.55203 seconds. 
  Full-batch training loss = 0.065233, test loss = 0.161589
  Training set accuracy = 0.987200, Test set accuracy = 0.953400
 epoch 13/100. Took 0.55008 seconds. 
  Full-batch training loss = 0.058077, test loss = 0.158069
  Training set accuracy = 0.989300, Test set accuracy = 0.954100
 epoch 14/100. Took 0.54989 seconds. 
  Full-batch training loss = 0.051952, test loss = 0.155885
  Training set accuracy = 0.990900, Test set accuracy = 0.955700
 epoch 15/100. Took 0.54922 seconds. 
  Full-batch training loss = 0.048976, test loss = 0.157937
  Training set accuracy = 0.991600, Test set accuracy = 0.956600
 epoch 16/100. Took 0.56009 seconds. 
  Full-batch training loss = 0.043906, test loss = 0.155989
  Training set accuracy = 0.993200, Test set accuracy = 0.956000
 epoch 17/100. Took 0.55054 seconds. 
  Full-batch training loss = 0.039749, test loss = 0.152861
  Training set accuracy = 0.994000, Test set accuracy = 0.956500
 epoch 18/100. Took 0.55746 seconds. 
  Full-batch training loss = 0.037353, test loss = 0.153290
  Training set accuracy = 0.995000, Test set accuracy = 0.958000
 epoch 19/100. Took 0.55717 seconds. 
  Full-batch training loss = 0.034158, test loss = 0.152930
  Training set accuracy = 0.995200, Test set accuracy = 0.957400
 epoch 20/100. Took 0.557 seconds. 
  Full-batch training loss = 0.031770, test loss = 0.153756
  Training set accuracy = 0.996000, Test set accuracy = 0.958700
 epoch 21/100. Took 0.55012 seconds. 
  Full-batch training loss = 0.028639, test loss = 0.150085
  Training set accuracy = 0.996400, Test set accuracy = 0.958900
 epoch 22/100. Took 0.55699 seconds. 
  Full-batch training loss = 0.026787, test loss = 0.150668
  Training set accuracy = 0.997000, Test set accuracy = 0.959700
 epoch 23/100. Took 0.54997 seconds. 
  Full-batch training loss = 0.024953, test loss = 0.150615
  Training set accuracy = 0.997900, Test set accuracy = 0.960100
 epoch 24/100. Took 0.54839 seconds. 
  Full-batch training loss = 0.022988, test loss = 0.152526
  Training set accuracy = 0.998100, Test set accuracy = 0.958000
 epoch 25/100. Took 0.5541 seconds. 
  Full-batch training loss = 0.021508, test loss = 0.151283
  Training set accuracy = 0.998300, Test set accuracy = 0.958500
 epoch 26/100. Took 0.55118 seconds. 
  Full-batch training loss = 0.019832, test loss = 0.151411
  Training set accuracy = 0.998600, Test set accuracy = 0.959200
 epoch 27/100. Took 0.55339 seconds. 
  Full-batch training loss = 0.019570, test loss = 0.151845
  Training set accuracy = 0.998800, Test set accuracy = 0.958300
 epoch 28/100. Took 0.55365 seconds. 
  Full-batch training loss = 0.017538, test loss = 0.151731
  Training set accuracy = 0.998900, Test set accuracy = 0.960100
 epoch 29/100. Took 0.55728 seconds. 
  Full-batch training loss = 0.016647, test loss = 0.153247
  Training set accuracy = 0.999000, Test set accuracy = 0.959500
 epoch 30/100. Took 0.55124 seconds. 
  Full-batch training loss = 0.015562, test loss = 0.150966
  Training set accuracy = 0.999200, Test set accuracy = 0.959700
 epoch 31/100. Took 0.55245 seconds. 
  Full-batch training loss = 0.015126, test loss = 0.152244
  Training set accuracy = 0.999000, Test set accuracy = 0.960000
 epoch 32/100. Took 0.54986 seconds. 
  Full-batch training loss = 0.013923, test loss = 0.152671
  Training set accuracy = 0.999400, Test set accuracy = 0.959600
 epoch 33/100. Took 0.54969 seconds. 
  Full-batch training loss = 0.013159, test loss = 0.152312
  Training set accuracy = 0.999300, Test set accuracy = 0.959900
 epoch 34/100. Took 0.55245 seconds. 
  Full-batch training loss = 0.012685, test loss = 0.152566
  Training set accuracy = 0.999400, Test set accuracy = 0.960200
 epoch 35/100. Took 0.55148 seconds. 
  Full-batch training loss = 0.012159, test loss = 0.153787
  Training set accuracy = 0.999600, Test set accuracy = 0.959500
 epoch 36/100. Took 0.55894 seconds. 
  Full-batch training loss = 0.011381, test loss = 0.152580
  Training set accuracy = 0.999500, Test set accuracy = 0.961300
 epoch 37/100. Took 0.65087 seconds. 
  Full-batch training loss = 0.011082, test loss = 0.152675
  Training set accuracy = 0.999500, Test set accuracy = 0.959800
 epoch 38/100. Took 0.55446 seconds. 
  Full-batch training loss = 0.010381, test loss = 0.153992
  Training set accuracy = 0.999700, Test set accuracy = 0.961500
 epoch 39/100. Took 0.56178 seconds. 
  Full-batch training loss = 0.009868, test loss = 0.152978
  Training set accuracy = 0.999600, Test set accuracy = 0.961200
 epoch 40/100. Took 0.55401 seconds. 
  Full-batch training loss = 0.009344, test loss = 0.154584
  Training set accuracy = 0.999700, Test set accuracy = 0.960300
 epoch 41/100. Took 0.55044 seconds. 
  Full-batch training loss = 0.008977, test loss = 0.153311
  Training set accuracy = 0.999700, Test set accuracy = 0.961300
 epoch 42/100. Took 0.55396 seconds. 
  Full-batch training loss = 0.008777, test loss = 0.153454
  Training set accuracy = 0.999700, Test set accuracy = 0.962400
 epoch 43/100. Took 0.55505 seconds. 
  Full-batch training loss = 0.008261, test loss = 0.154424
  Training set accuracy = 0.999700, Test set accuracy = 0.961400
 epoch 44/100. Took 0.55298 seconds. 
  Full-batch training loss = 0.007857, test loss = 0.154732
  Training set accuracy = 0.999700, Test set accuracy = 0.960600
 epoch 45/100. Took 0.55259 seconds. 
  Full-batch training loss = 0.007627, test loss = 0.155388
  Training set accuracy = 0.999700, Test set accuracy = 0.961300
 epoch 46/100. Took 0.55712 seconds. 
  Full-batch training loss = 0.007292, test loss = 0.155075
  Training set accuracy = 0.999800, Test set accuracy = 0.961500
 epoch 47/100. Took 0.55366 seconds. 
  Full-batch training loss = 0.007053, test loss = 0.155304
  Training set accuracy = 0.999800, Test set accuracy = 0.961200
 epoch 48/100. Took 0.56492 seconds. 
  Full-batch training loss = 0.006771, test loss = 0.155762
  Training set accuracy = 0.999800, Test set accuracy = 0.962000
 epoch 49/100. Took 0.55574 seconds. 
  Full-batch training loss = 0.006493, test loss = 0.156280
  Training set accuracy = 0.999800, Test set accuracy = 0.961100
 epoch 50/100. Took 0.55519 seconds. 
  Full-batch training loss = 0.006307, test loss = 0.156875
  Training set accuracy = 0.999800, Test set accuracy = 0.960700
 epoch 51/100. Took 0.56081 seconds. 
  Full-batch training loss = 0.006064, test loss = 0.156338
  Training set accuracy = 0.999800, Test set accuracy = 0.961100
 epoch 52/100. Took 0.55733 seconds. 
  Full-batch training loss = 0.005843, test loss = 0.156547
  Training set accuracy = 0.999800, Test set accuracy = 0.961500
 epoch 53/100. Took 0.5526 seconds. 
  Full-batch training loss = 0.005701, test loss = 0.157406
  Training set accuracy = 0.999800, Test set accuracy = 0.961700
 epoch 54/100. Took 0.5529 seconds. 
  Full-batch training loss = 0.005538, test loss = 0.158097
  Training set accuracy = 1.000000, Test set accuracy = 0.961100
 epoch 55/100. Took 0.5569 seconds. 
  Full-batch training loss = 0.005364, test loss = 0.158051
  Training set accuracy = 0.999900, Test set accuracy = 0.961100
 epoch 56/100. Took 0.55515 seconds. 
  Full-batch training loss = 0.005179, test loss = 0.158548
  Training set accuracy = 1.000000, Test set accuracy = 0.961200
 epoch 57/100. Took 0.55666 seconds. 
  Full-batch training loss = 0.005020, test loss = 0.158231
  Training set accuracy = 1.000000, Test set accuracy = 0.961300
 epoch 58/100. Took 0.55489 seconds. 
  Full-batch training loss = 0.004851, test loss = 0.158177
  Training set accuracy = 1.000000, Test set accuracy = 0.961500
 epoch 59/100. Took 0.55377 seconds. 
  Full-batch training loss = 0.004729, test loss = 0.158544
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 60/100. Took 0.55433 seconds. 
  Full-batch training loss = 0.004599, test loss = 0.158824
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 61/100. Took 0.56381 seconds. 
  Full-batch training loss = 0.004474, test loss = 0.159448
  Training set accuracy = 1.000000, Test set accuracy = 0.961500
 epoch 62/100. Took 0.55953 seconds. 
  Full-batch training loss = 0.004338, test loss = 0.159076
  Training set accuracy = 1.000000, Test set accuracy = 0.962100
 epoch 63/100. Took 0.55215 seconds. 
  Full-batch training loss = 0.004233, test loss = 0.159350
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 64/100. Took 0.5505 seconds. 
  Full-batch training loss = 0.004159, test loss = 0.160181
  Training set accuracy = 1.000000, Test set accuracy = 0.961000
 epoch 65/100. Took 0.55139 seconds. 
  Full-batch training loss = 0.004032, test loss = 0.159326
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 66/100. Took 0.55939 seconds. 
  Full-batch training loss = 0.003931, test loss = 0.160014
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 67/100. Took 0.55977 seconds. 
  Full-batch training loss = 0.003851, test loss = 0.160595
  Training set accuracy = 1.000000, Test set accuracy = 0.962200
 epoch 68/100. Took 0.55027 seconds. 
  Full-batch training loss = 0.003752, test loss = 0.160523
  Training set accuracy = 1.000000, Test set accuracy = 0.961900
 epoch 69/100. Took 0.55205 seconds. 
  Full-batch training loss = 0.003663, test loss = 0.160589
  Training set accuracy = 1.000000, Test set accuracy = 0.962400
 epoch 70/100. Took 0.55578 seconds. 
  Full-batch training loss = 0.003594, test loss = 0.161496
  Training set accuracy = 1.000000, Test set accuracy = 0.961800
 epoch 71/100. Took 0.55903 seconds. 
  Full-batch training loss = 0.003504, test loss = 0.161030
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 72/100. Took 0.5507 seconds. 
  Full-batch training loss = 0.003426, test loss = 0.161654
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 73/100. Took 0.55558 seconds. 
  Full-batch training loss = 0.003351, test loss = 0.161735
  Training set accuracy = 1.000000, Test set accuracy = 0.962200
 epoch 74/100. Took 0.55447 seconds. 
  Full-batch training loss = 0.003286, test loss = 0.161428
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 75/100. Took 0.55164 seconds. 
  Full-batch training loss = 0.003229, test loss = 0.162241
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 76/100. Took 0.5547 seconds. 
  Full-batch training loss = 0.003149, test loss = 0.161936
  Training set accuracy = 1.000000, Test set accuracy = 0.962400
 epoch 77/100. Took 0.55467 seconds. 
  Full-batch training loss = 0.003085, test loss = 0.162193
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 78/100. Took 0.55215 seconds. 
  Full-batch training loss = 0.003026, test loss = 0.162799
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 79/100. Took 0.55332 seconds. 
  Full-batch training loss = 0.002967, test loss = 0.163031
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 80/100. Took 0.55001 seconds. 
  Full-batch training loss = 0.002912, test loss = 0.163012
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 81/100. Took 0.5524 seconds. 
  Full-batch training loss = 0.002861, test loss = 0.163305
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 82/100. Took 0.55456 seconds. 
  Full-batch training loss = 0.002809, test loss = 0.163276
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 83/100. Took 0.54929 seconds. 
  Full-batch training loss = 0.002755, test loss = 0.163467
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 84/100. Took 0.55413 seconds. 
  Full-batch training loss = 0.002708, test loss = 0.164071
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 85/100. Took 0.55122 seconds. 
  Full-batch training loss = 0.002661, test loss = 0.163730
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 86/100. Took 0.55134 seconds. 
  Full-batch training loss = 0.002617, test loss = 0.164291
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 87/100. Took 0.5514 seconds. 
  Full-batch training loss = 0.002567, test loss = 0.164307
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 88/100. Took 0.55302 seconds. 
  Full-batch training loss = 0.002524, test loss = 0.164525
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 89/100. Took 0.55278 seconds. 
  Full-batch training loss = 0.002482, test loss = 0.164501
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 90/100. Took 0.55191 seconds. 
  Full-batch training loss = 0.002443, test loss = 0.164666
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 91/100. Took 0.55762 seconds. 
  Full-batch training loss = 0.002403, test loss = 0.165157
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 92/100. Took 0.55395 seconds. 
  Full-batch training loss = 0.002362, test loss = 0.165140
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 93/100. Took 0.55435 seconds. 
  Full-batch training loss = 0.002328, test loss = 0.165276
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 94/100. Took 0.55088 seconds. 
  Full-batch training loss = 0.002291, test loss = 0.165459
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 95/100. Took 0.55403 seconds. 
  Full-batch training loss = 0.002254, test loss = 0.165687
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 96/100. Took 0.55317 seconds. 
  Full-batch training loss = 0.002221, test loss = 0.165572
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 97/100. Took 0.55636 seconds. 
  Full-batch training loss = 0.002191, test loss = 0.165971
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 98/100. Took 0.55272 seconds. 
  Full-batch training loss = 0.002155, test loss = 0.166053
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 99/100. Took 0.54952 seconds. 
  Full-batch training loss = 0.002124, test loss = 0.166104
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 100/100. Took 0.55042 seconds. 
  Full-batch training loss = 0.002093, test loss = 0.166289
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
Elapsed time is 162.705257 seconds.
End Training
