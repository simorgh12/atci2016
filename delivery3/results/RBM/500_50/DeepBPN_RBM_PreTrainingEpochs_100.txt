Loading data set: mnist_uint8.mat
{Error using <a href="matlab:helpUtils.errorDocCallback('load')" style="font-weight:bold">load</a>
Unable to read file 'mnist_uint8.mat': no such file or directory.

Error in <a href="matlab:helpUtils.errorDocCallback('LoadData_MNIST', 'C:\Users\Arkard\MasterUni\ATCI\PW3\DeepLearningToolbox-RasmusBergPalm\LoadData_MNIST.m', 8)" style="font-weight:bold">LoadData_MNIST</a> (<a href="matlab: opentoline('C:\Users\Arkard\MasterUni\ATCI\PW3\DeepLearningToolbox-RasmusBergPalm\LoadData_MNIST.m',8,0)">line 8</a>)
load(DataSet)
} 
clear all;

%%% Recursive addpath
ToolboxPath = './ToolboxDeepLearning/';
addpath(genpath(ToolboxPath));
DataPath = './Data/';
addpath(genpath(DataPath));

%%% Architecture
Arch = [500 50];
% Architecture string. Example: from [500 200 100] ----> '500_200_100'
archStr = '';
for n = 1:length(Arch) - 1
    archStr = strcat(archStr,num2str(Arch(n)),'_');
end
archStr = strcat(archStr,num2str(Arch(end)));

%%% Parameters and corresponding indexes
ReducedData_lst = [2000 5000 10000];
i = 1;
PreTrainRBM_lst = [true, false];
j = 1;
%% Fine-tuning parameters
FT_Epochs_lst = [100 200 500];
k = 1;
FT_LearningRate_lst = [0.2 0.1 0.05];
l = 1;
%% Pretraining parameters
PT_Epochs_lst = [20 50 100];
m = 1;
PT_LearningRate_lst = [0.2 0.1 0.05];
n = 1;
%% Changing Pretraining epochs
clc;
for m = 3:3
    diary(sprintf('./results/%s/DeepBPN_RBM_PreTrainingEpochs_%d.txt', archStr, PT_Epochs_lst(m)));
    diary on
    LoadData_MNIST;
    DeepBPN_RBM_set_parameters_MNIST;
    
    disp('==========================================================')
    fprintf('* ReducedData\t%d\n', ReducedData);
    fprintf('* PreTrainRBM\ttrue\n');
    fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
    fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
    disp('* Fine-tunning')
    fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
    fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
    disp('==========================================================')
    
    DeepBPN_RBM_train_models;
    diary off; % End the diary
end
Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	2000
* PreTrainRBM	true
	-> Epochs	100
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 2000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 2 (500   50)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 100 epochs
 epoch 1/100. Took 0.79917 seconds. Average reconstruction error is: 51.1848
 epoch 2/100. Took 0.71794 seconds. Average reconstruction error is: 26.529
 epoch 3/100. Took 0.747 seconds. Average reconstruction error is: 21.8388
 epoch 4/100. Took 0.90256 seconds. Average reconstruction error is: 19.4212
 epoch 5/100. Took 0.71386 seconds. Average reconstruction error is: 17.9443
 epoch 6/100. Took 0.80154 seconds. Average reconstruction error is: 16.8123
 epoch 7/100. Took 0.7765 seconds. Average reconstruction error is: 15.9058
 epoch 8/100. Took 0.74798 seconds. Average reconstruction error is: 15.2369
 epoch 9/100. Took 0.78405 seconds. Average reconstruction error is: 14.4503
 epoch 10/100. Took 0.81996 seconds. Average reconstruction error is: 13.9089
 epoch 11/100. Took 0.75086 seconds. Average reconstruction error is: 13.4495
 epoch 12/100. Took 0.85652 seconds. Average reconstruction error is: 13.0995
 epoch 13/100. Took 1.0149 seconds. Average reconstruction error is: 12.6681
 epoch 14/100. Took 0.77621 seconds. Average reconstruction error is: 12.3065
 epoch 15/100. Took 0.7844 seconds. Average reconstruction error is: 12.0831
 epoch 16/100. Took 0.77911 seconds. Average reconstruction error is: 11.6616
 epoch 17/100. Took 0.81929 seconds. Average reconstruction error is: 11.4231
 epoch 18/100. Took 0.74432 seconds. Average reconstruction error is: 11.1595
 epoch 19/100. Took 0.74695 seconds. Average reconstruction error is: 10.9834
 epoch 20/100. Took 0.75702 seconds. Average reconstruction error is: 10.6855
 epoch 21/100. Took 0.76086 seconds. Average reconstruction error is: 10.4559
 epoch 22/100. Took 0.74578 seconds. Average reconstruction error is: 10.308
 epoch 23/100. Took 0.71404 seconds. Average reconstruction error is: 10.1606
 epoch 24/100. Took 0.72887 seconds. Average reconstruction error is: 10.0376
 epoch 25/100. Took 0.7192 seconds. Average reconstruction error is: 9.8399
 epoch 26/100. Took 0.72432 seconds. Average reconstruction error is: 9.7621
 epoch 27/100. Took 0.75364 seconds. Average reconstruction error is: 9.5372
 epoch 28/100. Took 0.75718 seconds. Average reconstruction error is: 9.456
 epoch 29/100. Took 0.75833 seconds. Average reconstruction error is: 9.3277
 epoch 30/100. Took 0.73297 seconds. Average reconstruction error is: 9.2048
 epoch 31/100. Took 0.72776 seconds. Average reconstruction error is: 9.0197
 epoch 32/100. Took 0.75605 seconds. Average reconstruction error is: 8.9551
 epoch 33/100. Took 0.71268 seconds. Average reconstruction error is: 8.9269
 epoch 34/100. Took 0.72131 seconds. Average reconstruction error is: 8.7558
 epoch 35/100. Took 0.71924 seconds. Average reconstruction error is: 8.6994
 epoch 36/100. Took 0.70065 seconds. Average reconstruction error is: 8.5742
 epoch 37/100. Took 0.74728 seconds. Average reconstruction error is: 8.5596
 epoch 38/100. Took 0.72656 seconds. Average reconstruction error is: 8.4843
 epoch 39/100. Took 0.77146 seconds. Average reconstruction error is: 8.4151
 epoch 40/100. Took 0.73248 seconds. Average reconstruction error is: 8.3648
 epoch 41/100. Took 0.77384 seconds. Average reconstruction error is: 8.3205
 epoch 42/100. Took 0.75973 seconds. Average reconstruction error is: 8.1957
 epoch 43/100. Took 0.77776 seconds. Average reconstruction error is: 8.1069
 epoch 44/100. Took 0.75818 seconds. Average reconstruction error is: 8.0812
 epoch 45/100. Took 0.7062 seconds. Average reconstruction error is: 7.9875
 epoch 46/100. Took 0.73395 seconds. Average reconstruction error is: 7.9036
 epoch 47/100. Took 0.75274 seconds. Average reconstruction error is: 7.8899
 epoch 48/100. Took 0.73487 seconds. Average reconstruction error is: 7.8613
 epoch 49/100. Took 0.7447 seconds. Average reconstruction error is: 7.7986
 epoch 50/100. Took 0.72362 seconds. Average reconstruction error is: 7.7293
 epoch 51/100. Took 0.7488 seconds. Average reconstruction error is: 7.783
 epoch 52/100. Took 0.77291 seconds. Average reconstruction error is: 7.6438
 epoch 53/100. Took 0.74189 seconds. Average reconstruction error is: 7.6621
 epoch 54/100. Took 0.73716 seconds. Average reconstruction error is: 7.5631
 epoch 55/100. Took 0.73903 seconds. Average reconstruction error is: 7.5218
 epoch 56/100. Took 0.74671 seconds. Average reconstruction error is: 7.4689
 epoch 57/100. Took 0.78532 seconds. Average reconstruction error is: 7.4371
 epoch 58/100. Took 0.7722 seconds. Average reconstruction error is: 7.386
 epoch 59/100. Took 0.73913 seconds. Average reconstruction error is: 7.3931
 epoch 60/100. Took 0.74515 seconds. Average reconstruction error is: 7.4027
 epoch 61/100. Took 0.73069 seconds. Average reconstruction error is: 7.3097
 epoch 62/100. Took 0.74411 seconds. Average reconstruction error is: 7.2832
 epoch 63/100. Took 0.73666 seconds. Average reconstruction error is: 7.2566
 epoch 64/100. Took 0.72414 seconds. Average reconstruction error is: 7.2469
 epoch 65/100. Took 0.73363 seconds. Average reconstruction error is: 7.1534
 epoch 66/100. Took 0.74771 seconds. Average reconstruction error is: 7.12
 epoch 67/100. Took 0.73923 seconds. Average reconstruction error is: 7.1569
 epoch 68/100. Took 0.76383 seconds. Average reconstruction error is: 7.1236
 epoch 69/100. Took 0.68575 seconds. Average reconstruction error is: 7.122
 epoch 70/100. Took 0.79248 seconds. Average reconstruction error is: 7.0774
 epoch 71/100. Took 0.76015 seconds. Average reconstruction error is: 7.0145
 epoch 72/100. Took 0.76396 seconds. Average reconstruction error is: 7.0383
 epoch 73/100. Took 0.76674 seconds. Average reconstruction error is: 7.0203
 epoch 74/100. Took 0.75069 seconds. Average reconstruction error is: 6.9275
 epoch 75/100. Took 0.79232 seconds. Average reconstruction error is: 6.9083
 epoch 76/100. Took 0.74505 seconds. Average reconstruction error is: 6.8897
 epoch 77/100. Took 0.73167 seconds. Average reconstruction error is: 6.9078
 epoch 78/100. Took 0.76764 seconds. Average reconstruction error is: 6.8755
 epoch 79/100. Took 0.74937 seconds. Average reconstruction error is: 6.8889
 epoch 80/100. Took 0.73968 seconds. Average reconstruction error is: 6.8561
 epoch 81/100. Took 0.74604 seconds. Average reconstruction error is: 6.8626
 epoch 82/100. Took 0.76363 seconds. Average reconstruction error is: 6.8319
 epoch 83/100. Took 0.77122 seconds. Average reconstruction error is: 6.8039
 epoch 84/100. Took 0.74157 seconds. Average reconstruction error is: 6.7394
 epoch 85/100. Took 0.7799 seconds. Average reconstruction error is: 6.7913
 epoch 86/100. Took 0.75393 seconds. Average reconstruction error is: 6.693
 epoch 87/100. Took 0.77552 seconds. Average reconstruction error is: 6.6952
 epoch 88/100. Took 0.74681 seconds. Average reconstruction error is: 6.6848
 epoch 89/100. Took 0.72882 seconds. Average reconstruction error is: 6.6592
 epoch 90/100. Took 0.77802 seconds. Average reconstruction error is: 6.7155
 epoch 91/100. Took 0.74265 seconds. Average reconstruction error is: 6.5942
 epoch 92/100. Took 0.83153 seconds. Average reconstruction error is: 6.604
 epoch 93/100. Took 0.78017 seconds. Average reconstruction error is: 6.6438
 epoch 94/100. Took 0.74568 seconds. Average reconstruction error is: 6.6138
 epoch 95/100. Took 0.706 seconds. Average reconstruction error is: 6.5558
 epoch 96/100. Took 0.75348 seconds. Average reconstruction error is: 6.5446
 epoch 97/100. Took 0.75478 seconds. Average reconstruction error is: 6.5917
 epoch 98/100. Took 0.75559 seconds. Average reconstruction error is: 6.5024
 epoch 99/100. Took 0.75642 seconds. Average reconstruction error is: 6.4787
 epoch 100/100. Took 0.76245 seconds. Average reconstruction error is: 6.5
Training binary-binary RBM in layer 2 (500   50) with CD1 for 100 epochs
 epoch 1/100. Took 0.092193 seconds. Average reconstruction error is: 40.2259
 epoch 2/100. Took 0.096808 seconds. Average reconstruction error is: 25.0957
 epoch 3/100. Took 0.099527 seconds. Average reconstruction error is: 21.1535
 epoch 4/100. Took 0.099659 seconds. Average reconstruction error is: 19.293
 epoch 5/100. Took 0.1119 seconds. Average reconstruction error is: 18.2889
 epoch 6/100. Took 0.099562 seconds. Average reconstruction error is: 17.5182
 epoch 7/100. Took 0.098868 seconds. Average reconstruction error is: 16.9813
 epoch 8/100. Took 0.10046 seconds. Average reconstruction error is: 16.4804
 epoch 9/100. Took 0.096857 seconds. Average reconstruction error is: 16.0852
 epoch 10/100. Took 0.10242 seconds. Average reconstruction error is: 15.8191
 epoch 11/100. Took 0.094334 seconds. Average reconstruction error is: 15.516
 epoch 12/100. Took 0.10093 seconds. Average reconstruction error is: 15.3101
 epoch 13/100. Took 0.10119 seconds. Average reconstruction error is: 15.0743
 epoch 14/100. Took 0.10831 seconds. Average reconstruction error is: 14.9248
 epoch 15/100. Took 0.10969 seconds. Average reconstruction error is: 14.7619
 epoch 16/100. Took 0.10782 seconds. Average reconstruction error is: 14.6026
 epoch 17/100. Took 0.098366 seconds. Average reconstruction error is: 14.4839
 epoch 18/100. Took 0.10668 seconds. Average reconstruction error is: 14.3573
 epoch 19/100. Took 0.10184 seconds. Average reconstruction error is: 14.2306
 epoch 20/100. Took 0.097756 seconds. Average reconstruction error is: 14.1339
 epoch 21/100. Took 0.10029 seconds. Average reconstruction error is: 14.0845
 epoch 22/100. Took 0.098344 seconds. Average reconstruction error is: 13.9588
 epoch 23/100. Took 0.10144 seconds. Average reconstruction error is: 13.8871
 epoch 24/100. Took 0.10109 seconds. Average reconstruction error is: 13.807
 epoch 25/100. Took 0.10344 seconds. Average reconstruction error is: 13.7355
 epoch 26/100. Took 0.10202 seconds. Average reconstruction error is: 13.6452
 epoch 27/100. Took 0.10613 seconds. Average reconstruction error is: 13.5748
 epoch 28/100. Took 0.098722 seconds. Average reconstruction error is: 13.5469
 epoch 29/100. Took 0.10311 seconds. Average reconstruction error is: 13.4806
 epoch 30/100. Took 0.096452 seconds. Average reconstruction error is: 13.4071
 epoch 31/100. Took 0.10365 seconds. Average reconstruction error is: 13.4283
 epoch 32/100. Took 0.099349 seconds. Average reconstruction error is: 13.3883
 epoch 33/100. Took 0.090252 seconds. Average reconstruction error is: 13.2869
 epoch 34/100. Took 0.10315 seconds. Average reconstruction error is: 13.2776
 epoch 35/100. Took 0.099236 seconds. Average reconstruction error is: 13.1924
 epoch 36/100. Took 0.098752 seconds. Average reconstruction error is: 13.1697
 epoch 37/100. Took 0.11715 seconds. Average reconstruction error is: 13.1673
 epoch 38/100. Took 0.099309 seconds. Average reconstruction error is: 13.1162
 epoch 39/100. Took 0.092563 seconds. Average reconstruction error is: 13.1164
 epoch 40/100. Took 0.097878 seconds. Average reconstruction error is: 13.0597
 epoch 41/100. Took 0.1043 seconds. Average reconstruction error is: 13.0633
 epoch 42/100. Took 0.097189 seconds. Average reconstruction error is: 13.0776
 epoch 43/100. Took 0.10295 seconds. Average reconstruction error is: 13.0186
 epoch 44/100. Took 0.10392 seconds. Average reconstruction error is: 12.9725
 epoch 45/100. Took 0.11065 seconds. Average reconstruction error is: 12.9562
 epoch 46/100. Took 0.10467 seconds. Average reconstruction error is: 12.9342
 epoch 47/100. Took 0.094836 seconds. Average reconstruction error is: 12.8757
 epoch 48/100. Took 0.10139 seconds. Average reconstruction error is: 12.8449
 epoch 49/100. Took 0.10059 seconds. Average reconstruction error is: 12.8262
 epoch 50/100. Took 0.099949 seconds. Average reconstruction error is: 12.7869
 epoch 51/100. Took 0.099218 seconds. Average reconstruction error is: 12.8154
 epoch 52/100. Took 0.097463 seconds. Average reconstruction error is: 12.7873
 epoch 53/100. Took 0.096308 seconds. Average reconstruction error is: 12.7413
 epoch 54/100. Took 0.09964 seconds. Average reconstruction error is: 12.7256
 epoch 55/100. Took 0.099516 seconds. Average reconstruction error is: 12.7384
 epoch 56/100. Took 0.096516 seconds. Average reconstruction error is: 12.6915
 epoch 57/100. Took 0.098974 seconds. Average reconstruction error is: 12.6741
 epoch 58/100. Took 0.099994 seconds. Average reconstruction error is: 12.6886
 epoch 59/100. Took 0.096129 seconds. Average reconstruction error is: 12.6727
 epoch 60/100. Took 0.10397 seconds. Average reconstruction error is: 12.6549
 epoch 61/100. Took 0.10427 seconds. Average reconstruction error is: 12.6447
 epoch 62/100. Took 0.10801 seconds. Average reconstruction error is: 12.6292
 epoch 63/100. Took 0.095145 seconds. Average reconstruction error is: 12.5852
 epoch 64/100. Took 0.1064 seconds. Average reconstruction error is: 12.5906
 epoch 65/100. Took 0.10105 seconds. Average reconstruction error is: 12.5548
 epoch 66/100. Took 0.099753 seconds. Average reconstruction error is: 12.5486
 epoch 67/100. Took 0.103 seconds. Average reconstruction error is: 12.5537
 epoch 68/100. Took 0.10454 seconds. Average reconstruction error is: 12.5144
 epoch 69/100. Took 0.10376 seconds. Average reconstruction error is: 12.5726
 epoch 70/100. Took 0.098717 seconds. Average reconstruction error is: 12.5228
 epoch 71/100. Took 0.099779 seconds. Average reconstruction error is: 12.5193
 epoch 72/100. Took 0.10103 seconds. Average reconstruction error is: 12.4851
 epoch 73/100. Took 0.10266 seconds. Average reconstruction error is: 12.4958
 epoch 74/100. Took 0.10607 seconds. Average reconstruction error is: 12.5067
 epoch 75/100. Took 0.096935 seconds. Average reconstruction error is: 12.5036
 epoch 76/100. Took 0.10942 seconds. Average reconstruction error is: 12.4736
 epoch 77/100. Took 0.1008 seconds. Average reconstruction error is: 12.4547
 epoch 78/100. Took 0.096135 seconds. Average reconstruction error is: 12.41
 epoch 79/100. Took 0.096239 seconds. Average reconstruction error is: 12.4836
 epoch 80/100. Took 0.099007 seconds. Average reconstruction error is: 12.4508
 epoch 81/100. Took 0.099806 seconds. Average reconstruction error is: 12.3944
 epoch 82/100. Took 0.096733 seconds. Average reconstruction error is: 12.4481
 epoch 83/100. Took 0.11006 seconds. Average reconstruction error is: 12.4036
 epoch 84/100. Took 0.10763 seconds. Average reconstruction error is: 12.3822
 epoch 85/100. Took 0.099226 seconds. Average reconstruction error is: 12.3779
 epoch 86/100. Took 0.10783 seconds. Average reconstruction error is: 12.3639
 epoch 87/100. Took 0.094561 seconds. Average reconstruction error is: 12.3428
 epoch 88/100. Took 0.1076 seconds. Average reconstruction error is: 12.3652
 epoch 89/100. Took 0.10279 seconds. Average reconstruction error is: 12.3819
 epoch 90/100. Took 0.094732 seconds. Average reconstruction error is: 12.3806
 epoch 91/100. Took 0.10519 seconds. Average reconstruction error is: 12.3767
 epoch 92/100. Took 0.094427 seconds. Average reconstruction error is: 12.3278
 epoch 93/100. Took 0.096976 seconds. Average reconstruction error is: 12.2632
 epoch 94/100. Took 0.10368 seconds. Average reconstruction error is: 12.3035
 epoch 95/100. Took 0.098276 seconds. Average reconstruction error is: 12.3147
 epoch 96/100. Took 0.10217 seconds. Average reconstruction error is: 12.219
 epoch 97/100. Took 0.099243 seconds. Average reconstruction error is: 12.2835
 epoch 98/100. Took 0.099218 seconds. Average reconstruction error is: 12.2437
 epoch 99/100. Took 0.096738 seconds. Average reconstruction error is: 12.2153
 epoch 100/100. Took 0.10393 seconds. Average reconstruction error is: 12.2015
Training NN  (784  500   50   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.47211 seconds. 
  Full-batch training loss = 0.785942, test loss = 0.783710
  Training set accuracy = 0.818500, Test set accuracy = 0.815600
 epoch 2/100. Took 0.47622 seconds. 
  Full-batch training loss = 0.562290, test loss = 0.575698
  Training set accuracy = 0.860000, Test set accuracy = 0.848700
 epoch 3/100. Took 0.47036 seconds. 
  Full-batch training loss = 0.489694, test loss = 0.518603
  Training set accuracy = 0.869500, Test set accuracy = 0.852700
 epoch 4/100. Took 0.47738 seconds. 
  Full-batch training loss = 0.435297, test loss = 0.478631
  Training set accuracy = 0.885000, Test set accuracy = 0.861200
 epoch 5/100. Took 0.44867 seconds. 
  Full-batch training loss = 0.398394, test loss = 0.451356
  Training set accuracy = 0.898000, Test set accuracy = 0.868400
 epoch 6/100. Took 0.46037 seconds. 
  Full-batch training loss = 0.371823, test loss = 0.436150
  Training set accuracy = 0.908500, Test set accuracy = 0.873200
 epoch 7/100. Took 0.46318 seconds. 
  Full-batch training loss = 0.340728, test loss = 0.419102
  Training set accuracy = 0.914000, Test set accuracy = 0.874500
 epoch 8/100. Took 0.47211 seconds. 
  Full-batch training loss = 0.320016, test loss = 0.410654
  Training set accuracy = 0.920500, Test set accuracy = 0.877700
 epoch 9/100. Took 0.44072 seconds. 
  Full-batch training loss = 0.302466, test loss = 0.402453
  Training set accuracy = 0.924500, Test set accuracy = 0.878300
 epoch 10/100. Took 0.44696 seconds. 
  Full-batch training loss = 0.286741, test loss = 0.399162
  Training set accuracy = 0.929000, Test set accuracy = 0.879200
 epoch 11/100. Took 0.45858 seconds. 
  Full-batch training loss = 0.270205, test loss = 0.394246
  Training set accuracy = 0.931500, Test set accuracy = 0.883100
 epoch 12/100. Took 0.4729 seconds. 
  Full-batch training loss = 0.257545, test loss = 0.386077
  Training set accuracy = 0.937000, Test set accuracy = 0.884800
 epoch 13/100. Took 0.46374 seconds. 
  Full-batch training loss = 0.244523, test loss = 0.382132
  Training set accuracy = 0.941000, Test set accuracy = 0.885200
 epoch 14/100. Took 0.4646 seconds. 
  Full-batch training loss = 0.232887, test loss = 0.376771
  Training set accuracy = 0.944000, Test set accuracy = 0.887400
 epoch 15/100. Took 0.4499 seconds. 
  Full-batch training loss = 0.223942, test loss = 0.375007
  Training set accuracy = 0.946000, Test set accuracy = 0.886900
 epoch 16/100. Took 0.44768 seconds. 
  Full-batch training loss = 0.215855, test loss = 0.374864
  Training set accuracy = 0.945000, Test set accuracy = 0.889100
 epoch 17/100. Took 0.46229 seconds. 
  Full-batch training loss = 0.205184, test loss = 0.373060
  Training set accuracy = 0.950500, Test set accuracy = 0.888800
 epoch 18/100. Took 0.47411 seconds. 
  Full-batch training loss = 0.199268, test loss = 0.373245
  Training set accuracy = 0.953500, Test set accuracy = 0.889000
 epoch 19/100. Took 0.44784 seconds. 
  Full-batch training loss = 0.192863, test loss = 0.372268
  Training set accuracy = 0.955000, Test set accuracy = 0.889100
 epoch 20/100. Took 0.44221 seconds. 
  Full-batch training loss = 0.184293, test loss = 0.369979
  Training set accuracy = 0.958000, Test set accuracy = 0.891400
 epoch 21/100. Took 0.45273 seconds. 
  Full-batch training loss = 0.177830, test loss = 0.369693
  Training set accuracy = 0.960000, Test set accuracy = 0.890300
 epoch 22/100. Took 0.46055 seconds. 
  Full-batch training loss = 0.170024, test loss = 0.368463
  Training set accuracy = 0.962500, Test set accuracy = 0.891400
 epoch 23/100. Took 0.46707 seconds. 
  Full-batch training loss = 0.164532, test loss = 0.368725
  Training set accuracy = 0.965000, Test set accuracy = 0.890900
 epoch 24/100. Took 0.4886 seconds. 
  Full-batch training loss = 0.157403, test loss = 0.364722
  Training set accuracy = 0.965500, Test set accuracy = 0.892000
 epoch 25/100. Took 0.45872 seconds. 
  Full-batch training loss = 0.151326, test loss = 0.364491
  Training set accuracy = 0.970500, Test set accuracy = 0.892400
 epoch 26/100. Took 0.43208 seconds. 
  Full-batch training loss = 0.147272, test loss = 0.366504
  Training set accuracy = 0.970000, Test set accuracy = 0.892700
 epoch 27/100. Took 0.46795 seconds. 
  Full-batch training loss = 0.141660, test loss = 0.362835
  Training set accuracy = 0.973500, Test set accuracy = 0.894000
 epoch 28/100. Took 0.47663 seconds. 
  Full-batch training loss = 0.138252, test loss = 0.364481
  Training set accuracy = 0.973500, Test set accuracy = 0.892700
 epoch 29/100. Took 0.48694 seconds. 
  Full-batch training loss = 0.134318, test loss = 0.361574
  Training set accuracy = 0.972000, Test set accuracy = 0.894100
 epoch 30/100. Took 0.45599 seconds. 
  Full-batch training loss = 0.128351, test loss = 0.362263
  Training set accuracy = 0.977000, Test set accuracy = 0.894400
 epoch 31/100. Took 0.47643 seconds. 
  Full-batch training loss = 0.123534, test loss = 0.364756
  Training set accuracy = 0.978500, Test set accuracy = 0.894100
 epoch 32/100. Took 0.47852 seconds. 
  Full-batch training loss = 0.118665, test loss = 0.360876
  Training set accuracy = 0.979500, Test set accuracy = 0.896900
 epoch 33/100. Took 0.44139 seconds. 
  Full-batch training loss = 0.115387, test loss = 0.361030
  Training set accuracy = 0.980000, Test set accuracy = 0.896300
 epoch 34/100. Took 0.42594 seconds. 
  Full-batch training loss = 0.111313, test loss = 0.361004
  Training set accuracy = 0.980500, Test set accuracy = 0.897100
 epoch 35/100. Took 0.47489 seconds. 
  Full-batch training loss = 0.108180, test loss = 0.363198
  Training set accuracy = 0.980000, Test set accuracy = 0.893700
 epoch 36/100. Took 0.45147 seconds. 
  Full-batch training loss = 0.104375, test loss = 0.360918
  Training set accuracy = 0.981000, Test set accuracy = 0.896000
 epoch 37/100. Took 0.47484 seconds. 
  Full-batch training loss = 0.100778, test loss = 0.361157
  Training set accuracy = 0.982500, Test set accuracy = 0.897900
 epoch 38/100. Took 0.43604 seconds. 
  Full-batch training loss = 0.097320, test loss = 0.360773
  Training set accuracy = 0.982000, Test set accuracy = 0.898200
 epoch 39/100. Took 0.43993 seconds. 
  Full-batch training loss = 0.094335, test loss = 0.360287
  Training set accuracy = 0.983500, Test set accuracy = 0.897900
 epoch 40/100. Took 0.45569 seconds. 
  Full-batch training loss = 0.091587, test loss = 0.361257
  Training set accuracy = 0.983000, Test set accuracy = 0.897900
 epoch 41/100. Took 0.49613 seconds. 
  Full-batch training loss = 0.089132, test loss = 0.362151
  Training set accuracy = 0.984000, Test set accuracy = 0.896900
 epoch 42/100. Took 0.46169 seconds. 
  Full-batch training loss = 0.086274, test loss = 0.362154
  Training set accuracy = 0.983000, Test set accuracy = 0.897600
 epoch 43/100. Took 0.47423 seconds. 
  Full-batch training loss = 0.083136, test loss = 0.362261
  Training set accuracy = 0.985000, Test set accuracy = 0.897300
 epoch 44/100. Took 0.4439 seconds. 
  Full-batch training loss = 0.080361, test loss = 0.361191
  Training set accuracy = 0.985000, Test set accuracy = 0.898100
 epoch 45/100. Took 0.48885 seconds. 
  Full-batch training loss = 0.077886, test loss = 0.361469
  Training set accuracy = 0.986000, Test set accuracy = 0.899400
 epoch 46/100. Took 0.47787 seconds. 
  Full-batch training loss = 0.075659, test loss = 0.363134
  Training set accuracy = 0.986500, Test set accuracy = 0.898000
 epoch 47/100. Took 0.46087 seconds. 
  Full-batch training loss = 0.074160, test loss = 0.362596
  Training set accuracy = 0.986000, Test set accuracy = 0.899800
 epoch 48/100. Took 0.46608 seconds. 
  Full-batch training loss = 0.071171, test loss = 0.363958
  Training set accuracy = 0.987000, Test set accuracy = 0.897500
 epoch 49/100. Took 0.4496 seconds. 
  Full-batch training loss = 0.068823, test loss = 0.362714
  Training set accuracy = 0.987500, Test set accuracy = 0.898700
 epoch 50/100. Took 0.47699 seconds. 
  Full-batch training loss = 0.066734, test loss = 0.363360
  Training set accuracy = 0.988000, Test set accuracy = 0.899600
 epoch 51/100. Took 0.48213 seconds. 
  Full-batch training loss = 0.064703, test loss = 0.362997
  Training set accuracy = 0.988500, Test set accuracy = 0.899800
 epoch 52/100. Took 0.47671 seconds. 
  Full-batch training loss = 0.063150, test loss = 0.363325
  Training set accuracy = 0.989500, Test set accuracy = 0.900000
 epoch 53/100. Took 0.47651 seconds. 
  Full-batch training loss = 0.061141, test loss = 0.364529
  Training set accuracy = 0.989000, Test set accuracy = 0.899300
 epoch 54/100. Took 0.4439 seconds. 
  Full-batch training loss = 0.059521, test loss = 0.364338
  Training set accuracy = 0.990000, Test set accuracy = 0.900000
 epoch 55/100. Took 0.50141 seconds. 
  Full-batch training loss = 0.058276, test loss = 0.366219
  Training set accuracy = 0.990000, Test set accuracy = 0.898900
 epoch 56/100. Took 0.45553 seconds. 
  Full-batch training loss = 0.056404, test loss = 0.364835
  Training set accuracy = 0.991000, Test set accuracy = 0.900800
 epoch 57/100. Took 0.47402 seconds. 
  Full-batch training loss = 0.054992, test loss = 0.367135
  Training set accuracy = 0.991000, Test set accuracy = 0.899300
 epoch 58/100. Took 0.47002 seconds. 
  Full-batch training loss = 0.053348, test loss = 0.366521
  Training set accuracy = 0.991500, Test set accuracy = 0.899600
 epoch 59/100. Took 0.46386 seconds. 
  Full-batch training loss = 0.051950, test loss = 0.366577
  Training set accuracy = 0.992500, Test set accuracy = 0.901200
 epoch 60/100. Took 0.46728 seconds. 
  Full-batch training loss = 0.050697, test loss = 0.367115
  Training set accuracy = 0.991500, Test set accuracy = 0.900000
 epoch 61/100. Took 0.4475 seconds. 
  Full-batch training loss = 0.049427, test loss = 0.367668
  Training set accuracy = 0.993500, Test set accuracy = 0.899600
 epoch 62/100. Took 0.48355 seconds. 
  Full-batch training loss = 0.048289, test loss = 0.367655
  Training set accuracy = 0.993500, Test set accuracy = 0.900800
 epoch 63/100. Took 0.43833 seconds. 
  Full-batch training loss = 0.047171, test loss = 0.367880
  Training set accuracy = 0.994500, Test set accuracy = 0.900200
 epoch 64/100. Took 0.43067 seconds. 
  Full-batch training loss = 0.045954, test loss = 0.368518
  Training set accuracy = 0.994500, Test set accuracy = 0.901200
 epoch 65/100. Took 0.43863 seconds. 
  Full-batch training loss = 0.044820, test loss = 0.369003
  Training set accuracy = 0.995000, Test set accuracy = 0.900100
 epoch 66/100. Took 0.46681 seconds. 
  Full-batch training loss = 0.043712, test loss = 0.369630
  Training set accuracy = 0.995500, Test set accuracy = 0.899700
 epoch 67/100. Took 0.43185 seconds. 
  Full-batch training loss = 0.042938, test loss = 0.370208
  Training set accuracy = 0.995500, Test set accuracy = 0.901200
 epoch 68/100. Took 0.4914 seconds. 
  Full-batch training loss = 0.041786, test loss = 0.370469
  Training set accuracy = 0.995500, Test set accuracy = 0.899900
 epoch 69/100. Took 0.45867 seconds. 
  Full-batch training loss = 0.040876, test loss = 0.371836
  Training set accuracy = 0.996500, Test set accuracy = 0.899200
 epoch 70/100. Took 0.45705 seconds. 
  Full-batch training loss = 0.039873, test loss = 0.371491
  Training set accuracy = 0.996500, Test set accuracy = 0.900100
 epoch 71/100. Took 0.45658 seconds. 
  Full-batch training loss = 0.039003, test loss = 0.371359
  Training set accuracy = 0.997000, Test set accuracy = 0.900300
 epoch 72/100. Took 0.44136 seconds. 
  Full-batch training loss = 0.038004, test loss = 0.371816
  Training set accuracy = 0.997000, Test set accuracy = 0.900000
 epoch 73/100. Took 0.45868 seconds. 
  Full-batch training loss = 0.037174, test loss = 0.372880
  Training set accuracy = 0.997000, Test set accuracy = 0.899800
 epoch 74/100. Took 0.44584 seconds. 
  Full-batch training loss = 0.036405, test loss = 0.372863
  Training set accuracy = 0.997000, Test set accuracy = 0.900500
 epoch 75/100. Took 0.45823 seconds. 
  Full-batch training loss = 0.035700, test loss = 0.373377
  Training set accuracy = 0.996500, Test set accuracy = 0.901300
 epoch 76/100. Took 0.45049 seconds. 
  Full-batch training loss = 0.034817, test loss = 0.374250
  Training set accuracy = 0.997000, Test set accuracy = 0.900600
 epoch 77/100. Took 0.4387 seconds. 
  Full-batch training loss = 0.034065, test loss = 0.374177
  Training set accuracy = 0.997500, Test set accuracy = 0.901100
 epoch 78/100. Took 0.43448 seconds. 
  Full-batch training loss = 0.033355, test loss = 0.374353
  Training set accuracy = 0.997000, Test set accuracy = 0.901800
 epoch 79/100. Took 0.47441 seconds. 
  Full-batch training loss = 0.032685, test loss = 0.375037
  Training set accuracy = 0.997500, Test set accuracy = 0.901300
 epoch 80/100. Took 0.46062 seconds. 
  Full-batch training loss = 0.031886, test loss = 0.375055
  Training set accuracy = 0.997500, Test set accuracy = 0.901300
 epoch 81/100. Took 0.46455 seconds. 
  Full-batch training loss = 0.031099, test loss = 0.376253
  Training set accuracy = 0.997500, Test set accuracy = 0.901100
 epoch 82/100. Took 0.48044 seconds. 
  Full-batch training loss = 0.030401, test loss = 0.376333
  Training set accuracy = 0.998000, Test set accuracy = 0.901600
 epoch 83/100. Took 0.47111 seconds. 
  Full-batch training loss = 0.029759, test loss = 0.377640
  Training set accuracy = 0.998000, Test set accuracy = 0.901000
 epoch 84/100. Took 0.45775 seconds. 
  Full-batch training loss = 0.029227, test loss = 0.377874
  Training set accuracy = 0.999000, Test set accuracy = 0.900600
 epoch 85/100. Took 0.46232 seconds. 
  Full-batch training loss = 0.028576, test loss = 0.378638
  Training set accuracy = 0.998500, Test set accuracy = 0.901700
 epoch 86/100. Took 0.45958 seconds. 
  Full-batch training loss = 0.028048, test loss = 0.378136
  Training set accuracy = 0.999000, Test set accuracy = 0.901600
 epoch 87/100. Took 0.45956 seconds. 
  Full-batch training loss = 0.027479, test loss = 0.378887
  Training set accuracy = 0.999500, Test set accuracy = 0.901700
 epoch 88/100. Took 0.46559 seconds. 
  Full-batch training loss = 0.026982, test loss = 0.379167
  Training set accuracy = 0.999000, Test set accuracy = 0.901300
 epoch 89/100. Took 0.4752 seconds. 
  Full-batch training loss = 0.026416, test loss = 0.380220
  Training set accuracy = 0.999500, Test set accuracy = 0.901400
 epoch 90/100. Took 0.47254 seconds. 
  Full-batch training loss = 0.025961, test loss = 0.380341
  Training set accuracy = 0.999500, Test set accuracy = 0.901200
 epoch 91/100. Took 0.47407 seconds. 
  Full-batch training loss = 0.025488, test loss = 0.380988
  Training set accuracy = 0.999500, Test set accuracy = 0.901500
 epoch 92/100. Took 0.53307 seconds. 
  Full-batch training loss = 0.025053, test loss = 0.381901
  Training set accuracy = 0.999500, Test set accuracy = 0.901300
 epoch 93/100. Took 0.46606 seconds. 
  Full-batch training loss = 0.024608, test loss = 0.381894
  Training set accuracy = 0.999500, Test set accuracy = 0.901500
 epoch 94/100. Took 0.46379 seconds. 
  Full-batch training loss = 0.024198, test loss = 0.382711
  Training set accuracy = 0.999500, Test set accuracy = 0.900800
 epoch 95/100. Took 0.44284 seconds. 
  Full-batch training loss = 0.023779, test loss = 0.382663
  Training set accuracy = 0.999500, Test set accuracy = 0.901300
 epoch 96/100. Took 0.46621 seconds. 
  Full-batch training loss = 0.023378, test loss = 0.383208
  Training set accuracy = 0.999500, Test set accuracy = 0.901200
 epoch 97/100. Took 0.45457 seconds. 
  Full-batch training loss = 0.022987, test loss = 0.383899
  Training set accuracy = 0.999500, Test set accuracy = 0.901800
 epoch 98/100. Took 0.45761 seconds. 
  Full-batch training loss = 0.022611, test loss = 0.384130
  Training set accuracy = 0.999500, Test set accuracy = 0.901200
 epoch 99/100. Took 0.44198 seconds. 
  Full-batch training loss = 0.022272, test loss = 0.384175
  Training set accuracy = 0.999500, Test set accuracy = 0.901900
 epoch 100/100. Took 0.45088 seconds. 
  Full-batch training loss = 0.021889, test loss = 0.384834
  Training set accuracy = 0.999500, Test set accuracy = 0.902100
Elapsed time is 284.645704 seconds.
End Training
