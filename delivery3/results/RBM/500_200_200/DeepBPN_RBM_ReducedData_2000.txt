Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	2000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 2000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.11033 seconds. Average reconstruction error is: 52.0538
 epoch 2/20. Took 0.10747 seconds. Average reconstruction error is: 28.7498
 epoch 3/20. Took 0.10738 seconds. Average reconstruction error is: 24.2719
 epoch 4/20. Took 0.10543 seconds. Average reconstruction error is: 21.744
 epoch 5/20. Took 0.10871 seconds. Average reconstruction error is: 20.1072
 epoch 6/20. Took 0.11352 seconds. Average reconstruction error is: 18.8207
 epoch 7/20. Took 0.10678 seconds. Average reconstruction error is: 17.8533
 epoch 8/20. Took 0.10513 seconds. Average reconstruction error is: 16.9709
 epoch 9/20. Took 0.10623 seconds. Average reconstruction error is: 16.2402
 epoch 10/20. Took 0.10628 seconds. Average reconstruction error is: 15.7334
 epoch 11/20. Took 0.10707 seconds. Average reconstruction error is: 15.2109
 epoch 12/20. Took 0.10831 seconds. Average reconstruction error is: 14.7669
 epoch 13/20. Took 0.10604 seconds. Average reconstruction error is: 14.4032
 epoch 14/20. Took 0.1062 seconds. Average reconstruction error is: 14.031
 epoch 15/20. Took 0.10562 seconds. Average reconstruction error is: 13.7886
 epoch 16/20. Took 0.10691 seconds. Average reconstruction error is: 13.434
 epoch 17/20. Took 0.10552 seconds. Average reconstruction error is: 13.1421
 epoch 18/20. Took 0.10675 seconds. Average reconstruction error is: 12.8357
 epoch 19/20. Took 0.10512 seconds. Average reconstruction error is: 12.6227
 epoch 20/20. Took 0.10891 seconds. Average reconstruction error is: 12.3886
Training binary-binary RBM in layer 2 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.036346 seconds. Average reconstruction error is: 19.4571
 epoch 2/20. Took 0.035866 seconds. Average reconstruction error is: 11.1433
 epoch 3/20. Took 0.035765 seconds. Average reconstruction error is: 8.7675
 epoch 4/20. Took 0.035795 seconds. Average reconstruction error is: 7.5552
 epoch 5/20. Took 0.035805 seconds. Average reconstruction error is: 6.7927
 epoch 6/20. Took 0.036755 seconds. Average reconstruction error is: 6.2767
 epoch 7/20. Took 0.036081 seconds. Average reconstruction error is: 5.8857
 epoch 8/20. Took 0.036955 seconds. Average reconstruction error is: 5.6162
 epoch 9/20. Took 0.035762 seconds. Average reconstruction error is: 5.3665
 epoch 10/20. Took 0.036517 seconds. Average reconstruction error is: 5.1551
 epoch 11/20. Took 0.036176 seconds. Average reconstruction error is: 5.0265
 epoch 12/20. Took 0.036071 seconds. Average reconstruction error is: 4.8544
 epoch 13/20. Took 0.036048 seconds. Average reconstruction error is: 4.7447
 epoch 14/20. Took 0.0369 seconds. Average reconstruction error is: 4.6129
 epoch 15/20. Took 0.035543 seconds. Average reconstruction error is: 4.5611
 epoch 16/20. Took 0.037096 seconds. Average reconstruction error is: 4.4815
 epoch 17/20. Took 0.035836 seconds. Average reconstruction error is: 4.4308
 epoch 18/20. Took 0.036119 seconds. Average reconstruction error is: 4.3565
 epoch 19/20. Took 0.036464 seconds. Average reconstruction error is: 4.2938
 epoch 20/20. Took 0.035895 seconds. Average reconstruction error is: 4.1983
Training binary-binary RBM in layer 3 (200  200) with CD1 for 20 epochs
 epoch 1/20. Took 0.036242 seconds. Average reconstruction error is: 16.8847
 epoch 2/20. Took 0.036923 seconds. Average reconstruction error is: 8.8276
 epoch 3/20. Took 0.036745 seconds. Average reconstruction error is: 6.9802
 epoch 4/20. Took 0.036827 seconds. Average reconstruction error is: 6.0706
 epoch 5/20. Took 0.036349 seconds. Average reconstruction error is: 5.5466
 epoch 6/20. Took 0.036544 seconds. Average reconstruction error is: 5.2562
 epoch 7/20. Took 0.036077 seconds. Average reconstruction error is: 5.0388
 epoch 8/20. Took 0.036166 seconds. Average reconstruction error is: 4.8292
 epoch 9/20. Took 0.036882 seconds. Average reconstruction error is: 4.6699
 epoch 10/20. Took 0.035624 seconds. Average reconstruction error is: 4.5658
 epoch 11/20. Took 0.037019 seconds. Average reconstruction error is: 4.5242
 epoch 12/20. Took 0.036502 seconds. Average reconstruction error is: 4.4088
 epoch 13/20. Took 0.036204 seconds. Average reconstruction error is: 4.3522
 epoch 14/20. Took 0.035327 seconds. Average reconstruction error is: 4.3262
 epoch 15/20. Took 0.03656 seconds. Average reconstruction error is: 4.2649
 epoch 16/20. Took 0.036702 seconds. Average reconstruction error is: 4.2566
 epoch 17/20. Took 0.036377 seconds. Average reconstruction error is: 4.1414
 epoch 18/20. Took 0.036858 seconds. Average reconstruction error is: 4.1447
 epoch 19/20. Took 0.037407 seconds. Average reconstruction error is: 4.0973
 epoch 20/20. Took 0.036466 seconds. Average reconstruction error is: 4.0817
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.10655 seconds. 
  Full-batch training loss = 0.512789, test loss = 0.530180
  Training set accuracy = 0.850000, Test set accuracy = 0.846100
 epoch 2/100. Took 0.11215 seconds. 
  Full-batch training loss = 0.377107, test loss = 0.402343
  Training set accuracy = 0.896000, Test set accuracy = 0.887700
 epoch 3/100. Took 0.10885 seconds. 
  Full-batch training loss = 0.325471, test loss = 0.363783
  Training set accuracy = 0.910000, Test set accuracy = 0.897200
 epoch 4/100. Took 0.11062 seconds. 
  Full-batch training loss = 0.283756, test loss = 0.339456
  Training set accuracy = 0.925500, Test set accuracy = 0.903000
 epoch 5/100. Took 0.11035 seconds. 
  Full-batch training loss = 0.255596, test loss = 0.322903
  Training set accuracy = 0.932000, Test set accuracy = 0.907800
 epoch 6/100. Took 0.1098 seconds. 
  Full-batch training loss = 0.229363, test loss = 0.309286
  Training set accuracy = 0.943000, Test set accuracy = 0.911200
 epoch 7/100. Took 0.11019 seconds. 
  Full-batch training loss = 0.211255, test loss = 0.300506
  Training set accuracy = 0.944000, Test set accuracy = 0.915200
 epoch 8/100. Took 0.1091 seconds. 
  Full-batch training loss = 0.194795, test loss = 0.294749
  Training set accuracy = 0.950500, Test set accuracy = 0.913700
 epoch 9/100. Took 0.10939 seconds. 
  Full-batch training loss = 0.179396, test loss = 0.290874
  Training set accuracy = 0.953500, Test set accuracy = 0.915300
 epoch 10/100. Took 0.10902 seconds. 
  Full-batch training loss = 0.163902, test loss = 0.286524
  Training set accuracy = 0.959000, Test set accuracy = 0.916700
 epoch 11/100. Took 0.10855 seconds. 
  Full-batch training loss = 0.152631, test loss = 0.282675
  Training set accuracy = 0.963000, Test set accuracy = 0.916400
 epoch 12/100. Took 0.10875 seconds. 
  Full-batch training loss = 0.141786, test loss = 0.282004
  Training set accuracy = 0.965500, Test set accuracy = 0.917600
 epoch 13/100. Took 0.10935 seconds. 
  Full-batch training loss = 0.129003, test loss = 0.276286
  Training set accuracy = 0.969000, Test set accuracy = 0.920100
 epoch 14/100. Took 0.10989 seconds. 
  Full-batch training loss = 0.119436, test loss = 0.273551
  Training set accuracy = 0.971000, Test set accuracy = 0.920400
 epoch 15/100. Took 0.10933 seconds. 
  Full-batch training loss = 0.109924, test loss = 0.273119
  Training set accuracy = 0.975000, Test set accuracy = 0.920400
 epoch 16/100. Took 0.10906 seconds. 
  Full-batch training loss = 0.106836, test loss = 0.281014
  Training set accuracy = 0.979000, Test set accuracy = 0.916700
 epoch 17/100. Took 0.10853 seconds. 
  Full-batch training loss = 0.098862, test loss = 0.274545
  Training set accuracy = 0.980500, Test set accuracy = 0.921000
 epoch 18/100. Took 0.11048 seconds. 
  Full-batch training loss = 0.090455, test loss = 0.269712
  Training set accuracy = 0.984000, Test set accuracy = 0.922400
 epoch 19/100. Took 0.10882 seconds. 
  Full-batch training loss = 0.082593, test loss = 0.268361
  Training set accuracy = 0.985000, Test set accuracy = 0.922700
 epoch 20/100. Took 0.1075 seconds. 
  Full-batch training loss = 0.078397, test loss = 0.271639
  Training set accuracy = 0.986500, Test set accuracy = 0.920700
 epoch 21/100. Took 0.1078 seconds. 
  Full-batch training loss = 0.073738, test loss = 0.268332
  Training set accuracy = 0.990000, Test set accuracy = 0.922200
 epoch 22/100. Took 0.11038 seconds. 
  Full-batch training loss = 0.069171, test loss = 0.269391
  Training set accuracy = 0.990500, Test set accuracy = 0.922500
 epoch 23/100. Took 0.1093 seconds. 
  Full-batch training loss = 0.062548, test loss = 0.266532
  Training set accuracy = 0.993500, Test set accuracy = 0.923800
 epoch 24/100. Took 0.10809 seconds. 
  Full-batch training loss = 0.059118, test loss = 0.267742
  Training set accuracy = 0.995000, Test set accuracy = 0.923100
 epoch 25/100. Took 0.10958 seconds. 
  Full-batch training loss = 0.055605, test loss = 0.266484
  Training set accuracy = 0.995000, Test set accuracy = 0.923800
 epoch 26/100. Took 0.10877 seconds. 
  Full-batch training loss = 0.051952, test loss = 0.267718
  Training set accuracy = 0.995000, Test set accuracy = 0.923100
 epoch 27/100. Took 0.11287 seconds. 
  Full-batch training loss = 0.049175, test loss = 0.268859
  Training set accuracy = 0.996500, Test set accuracy = 0.923400
 epoch 28/100. Took 0.10811 seconds. 
  Full-batch training loss = 0.046330, test loss = 0.267698
  Training set accuracy = 0.997500, Test set accuracy = 0.924000
 epoch 29/100. Took 0.10909 seconds. 
  Full-batch training loss = 0.043830, test loss = 0.267679
  Training set accuracy = 0.998000, Test set accuracy = 0.924400
 epoch 30/100. Took 0.10911 seconds. 
  Full-batch training loss = 0.042555, test loss = 0.269216
  Training set accuracy = 0.997500, Test set accuracy = 0.924300
 epoch 31/100. Took 0.11203 seconds. 
  Full-batch training loss = 0.039246, test loss = 0.268766
  Training set accuracy = 0.998500, Test set accuracy = 0.924200
 epoch 32/100. Took 0.109 seconds. 
  Full-batch training loss = 0.037017, test loss = 0.268060
  Training set accuracy = 0.998500, Test set accuracy = 0.925100
 epoch 33/100. Took 0.10909 seconds. 
  Full-batch training loss = 0.035098, test loss = 0.269153
  Training set accuracy = 0.999000, Test set accuracy = 0.924700
 epoch 34/100. Took 0.10995 seconds. 
  Full-batch training loss = 0.033356, test loss = 0.270847
  Training set accuracy = 0.998500, Test set accuracy = 0.924100
 epoch 35/100. Took 0.11119 seconds. 
  Full-batch training loss = 0.031614, test loss = 0.271062
  Training set accuracy = 0.999000, Test set accuracy = 0.924400
 epoch 36/100. Took 0.10862 seconds. 
  Full-batch training loss = 0.030417, test loss = 0.271387
  Training set accuracy = 0.999000, Test set accuracy = 0.924900
 epoch 37/100. Took 0.11056 seconds. 
  Full-batch training loss = 0.028870, test loss = 0.271151
  Training set accuracy = 0.999000, Test set accuracy = 0.925500
 epoch 38/100. Took 0.1072 seconds. 
  Full-batch training loss = 0.027532, test loss = 0.272233
  Training set accuracy = 0.999000, Test set accuracy = 0.925400
 epoch 39/100. Took 0.10828 seconds. 
  Full-batch training loss = 0.026663, test loss = 0.274586
  Training set accuracy = 0.999500, Test set accuracy = 0.925400
 epoch 40/100. Took 0.10814 seconds. 
  Full-batch training loss = 0.025182, test loss = 0.273532
  Training set accuracy = 0.999500, Test set accuracy = 0.925400
 epoch 41/100. Took 0.10986 seconds. 
  Full-batch training loss = 0.024089, test loss = 0.274131
  Training set accuracy = 0.999500, Test set accuracy = 0.925700
 epoch 42/100. Took 0.11073 seconds. 
  Full-batch training loss = 0.023234, test loss = 0.274342
  Training set accuracy = 1.000000, Test set accuracy = 0.926000
 epoch 43/100. Took 0.10988 seconds. 
  Full-batch training loss = 0.022266, test loss = 0.274754
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 44/100. Took 0.10918 seconds. 
  Full-batch training loss = 0.021367, test loss = 0.275281
  Training set accuracy = 1.000000, Test set accuracy = 0.925800
 epoch 45/100. Took 0.10869 seconds. 
  Full-batch training loss = 0.020563, test loss = 0.276382
  Training set accuracy = 1.000000, Test set accuracy = 0.925100
 epoch 46/100. Took 0.10913 seconds. 
  Full-batch training loss = 0.019885, test loss = 0.276344
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 47/100. Took 0.11031 seconds. 
  Full-batch training loss = 0.019103, test loss = 0.276900
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 48/100. Took 0.10896 seconds. 
  Full-batch training loss = 0.018558, test loss = 0.278189
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 49/100. Took 0.11069 seconds. 
  Full-batch training loss = 0.017786, test loss = 0.278385
  Training set accuracy = 1.000000, Test set accuracy = 0.925800
 epoch 50/100. Took 0.10847 seconds. 
  Full-batch training loss = 0.017155, test loss = 0.278882
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 51/100. Took 0.12092 seconds. 
  Full-batch training loss = 0.016607, test loss = 0.279371
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 52/100. Took 0.11247 seconds. 
  Full-batch training loss = 0.016105, test loss = 0.279652
  Training set accuracy = 1.000000, Test set accuracy = 0.926400
 epoch 53/100. Took 0.10996 seconds. 
  Full-batch training loss = 0.015574, test loss = 0.280400
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 54/100. Took 0.10939 seconds. 
  Full-batch training loss = 0.015102, test loss = 0.280544
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 55/100. Took 0.10927 seconds. 
  Full-batch training loss = 0.014633, test loss = 0.281140
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 56/100. Took 0.10972 seconds. 
  Full-batch training loss = 0.014201, test loss = 0.282058
  Training set accuracy = 1.000000, Test set accuracy = 0.926400
 epoch 57/100. Took 0.11006 seconds. 
  Full-batch training loss = 0.013811, test loss = 0.282787
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 58/100. Took 0.10846 seconds. 
  Full-batch training loss = 0.013396, test loss = 0.282954
  Training set accuracy = 1.000000, Test set accuracy = 0.926400
 epoch 59/100. Took 0.11064 seconds. 
  Full-batch training loss = 0.013029, test loss = 0.283382
  Training set accuracy = 1.000000, Test set accuracy = 0.926200
 epoch 60/100. Took 0.10917 seconds. 
  Full-batch training loss = 0.012672, test loss = 0.284272
  Training set accuracy = 1.000000, Test set accuracy = 0.926000
 epoch 61/100. Took 0.11043 seconds. 
  Full-batch training loss = 0.012351, test loss = 0.284346
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 62/100. Took 0.10977 seconds. 
  Full-batch training loss = 0.012023, test loss = 0.284972
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 63/100. Took 0.10972 seconds. 
  Full-batch training loss = 0.011708, test loss = 0.285667
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 64/100. Took 0.11044 seconds. 
  Full-batch training loss = 0.011402, test loss = 0.285744
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 65/100. Took 0.10949 seconds. 
  Full-batch training loss = 0.011117, test loss = 0.286114
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 66/100. Took 0.10817 seconds. 
  Full-batch training loss = 0.010852, test loss = 0.286727
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 67/100. Took 0.10919 seconds. 
  Full-batch training loss = 0.010591, test loss = 0.287051
  Training set accuracy = 1.000000, Test set accuracy = 0.926800
 epoch 68/100. Took 0.10902 seconds. 
  Full-batch training loss = 0.010336, test loss = 0.287725
  Training set accuracy = 1.000000, Test set accuracy = 0.926200
 epoch 69/100. Took 0.11037 seconds. 
  Full-batch training loss = 0.010107, test loss = 0.288070
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 70/100. Took 0.10837 seconds. 
  Full-batch training loss = 0.009871, test loss = 0.288752
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 71/100. Took 0.10839 seconds. 
  Full-batch training loss = 0.009647, test loss = 0.288869
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 72/100. Took 0.10919 seconds. 
  Full-batch training loss = 0.009438, test loss = 0.289471
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 73/100. Took 0.10898 seconds. 
  Full-batch training loss = 0.009238, test loss = 0.289937
  Training set accuracy = 1.000000, Test set accuracy = 0.926800
 epoch 74/100. Took 0.10793 seconds. 
  Full-batch training loss = 0.009036, test loss = 0.290320
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 75/100. Took 0.111 seconds. 
  Full-batch training loss = 0.008857, test loss = 0.290640
  Training set accuracy = 1.000000, Test set accuracy = 0.926900
 epoch 76/100. Took 0.10934 seconds. 
  Full-batch training loss = 0.008664, test loss = 0.291212
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 77/100. Took 0.10874 seconds. 
  Full-batch training loss = 0.008481, test loss = 0.291645
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 78/100. Took 0.11108 seconds. 
  Full-batch training loss = 0.008321, test loss = 0.292259
  Training set accuracy = 1.000000, Test set accuracy = 0.926900
 epoch 79/100. Took 0.10828 seconds. 
  Full-batch training loss = 0.008162, test loss = 0.292264
  Training set accuracy = 1.000000, Test set accuracy = 0.927200
 epoch 80/100. Took 0.1101 seconds. 
  Full-batch training loss = 0.007995, test loss = 0.292966
  Training set accuracy = 1.000000, Test set accuracy = 0.927100
 epoch 81/100. Took 0.1095 seconds. 
  Full-batch training loss = 0.007842, test loss = 0.293223
  Training set accuracy = 1.000000, Test set accuracy = 0.927200
 epoch 82/100. Took 0.10797 seconds. 
  Full-batch training loss = 0.007692, test loss = 0.293489
  Training set accuracy = 1.000000, Test set accuracy = 0.926900
 epoch 83/100. Took 0.11128 seconds. 
  Full-batch training loss = 0.007552, test loss = 0.293892
  Training set accuracy = 1.000000, Test set accuracy = 0.927200
 epoch 84/100. Took 0.10852 seconds. 
  Full-batch training loss = 0.007413, test loss = 0.294501
  Training set accuracy = 1.000000, Test set accuracy = 0.927300
 epoch 85/100. Took 0.10873 seconds. 
  Full-batch training loss = 0.007278, test loss = 0.294667
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 86/100. Took 0.10995 seconds. 
  Full-batch training loss = 0.007151, test loss = 0.295107
  Training set accuracy = 1.000000, Test set accuracy = 0.927300
 epoch 87/100. Took 0.111 seconds. 
  Full-batch training loss = 0.007026, test loss = 0.295516
  Training set accuracy = 1.000000, Test set accuracy = 0.927300
 epoch 88/100. Took 0.1106 seconds. 
  Full-batch training loss = 0.006901, test loss = 0.295916
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 89/100. Took 0.10916 seconds. 
  Full-batch training loss = 0.006786, test loss = 0.296242
  Training set accuracy = 1.000000, Test set accuracy = 0.927100
 epoch 90/100. Took 0.10859 seconds. 
  Full-batch training loss = 0.006671, test loss = 0.296408
  Training set accuracy = 1.000000, Test set accuracy = 0.927200
 epoch 91/100. Took 0.10967 seconds. 
  Full-batch training loss = 0.006555, test loss = 0.296895
  Training set accuracy = 1.000000, Test set accuracy = 0.927200
 epoch 92/100. Took 0.11125 seconds. 
  Full-batch training loss = 0.006449, test loss = 0.297318
  Training set accuracy = 1.000000, Test set accuracy = 0.927500
 epoch 93/100. Took 0.10929 seconds. 
  Full-batch training loss = 0.006345, test loss = 0.297505
  Training set accuracy = 1.000000, Test set accuracy = 0.927400
 epoch 94/100. Took 0.11051 seconds. 
  Full-batch training loss = 0.006246, test loss = 0.298032
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 95/100. Took 0.11071 seconds. 
  Full-batch training loss = 0.006144, test loss = 0.298308
  Training set accuracy = 1.000000, Test set accuracy = 0.927300
 epoch 96/100. Took 0.11073 seconds. 
  Full-batch training loss = 0.006048, test loss = 0.298869
  Training set accuracy = 1.000000, Test set accuracy = 0.927600
 epoch 97/100. Took 0.10867 seconds. 
  Full-batch training loss = 0.005957, test loss = 0.299111
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 98/100. Took 0.1093 seconds. 
  Full-batch training loss = 0.005864, test loss = 0.299393
  Training set accuracy = 1.000000, Test set accuracy = 0.927100
 epoch 99/100. Took 0.10939 seconds. 
  Full-batch training loss = 0.005776, test loss = 0.299749
  Training set accuracy = 1.000000, Test set accuracy = 0.926900
 epoch 100/100. Took 0.10927 seconds. 
  Full-batch training loss = 0.005689, test loss = 0.300006
  Training set accuracy = 1.000000, Test set accuracy = 0.927200
Elapsed time is 48.741466 seconds.
End Training
