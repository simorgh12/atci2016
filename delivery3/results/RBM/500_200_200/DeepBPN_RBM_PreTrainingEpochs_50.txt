Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	50
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training binary-binary RBM in layer 1 (784  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.53881 seconds. Average reconstruction error is: 29.3767
 epoch 2/50. Took 0.54082 seconds. Average reconstruction error is: 17.898
 epoch 3/50. Took 0.53642 seconds. Average reconstruction error is: 15.6283
 epoch 4/50. Took 0.53414 seconds. Average reconstruction error is: 14.3624
 epoch 5/50. Took 0.53479 seconds. Average reconstruction error is: 13.3723
 epoch 6/50. Took 0.53547 seconds. Average reconstruction error is: 12.521
 epoch 7/50. Took 0.5414 seconds. Average reconstruction error is: 11.914
 epoch 8/50. Took 0.53552 seconds. Average reconstruction error is: 11.3815
 epoch 9/50. Took 0.54054 seconds. Average reconstruction error is: 11.0294
 epoch 10/50. Took 0.53998 seconds. Average reconstruction error is: 10.7038
 epoch 11/50. Took 0.54304 seconds. Average reconstruction error is: 10.5098
 epoch 12/50. Took 0.54019 seconds. Average reconstruction error is: 10.2975
 epoch 13/50. Took 0.53731 seconds. Average reconstruction error is: 10.1083
 epoch 14/50. Took 0.53823 seconds. Average reconstruction error is: 9.9696
 epoch 15/50. Took 0.53681 seconds. Average reconstruction error is: 9.849
 epoch 16/50. Took 0.53703 seconds. Average reconstruction error is: 9.7405
 epoch 17/50. Took 0.53591 seconds. Average reconstruction error is: 9.6187
 epoch 18/50. Took 0.53556 seconds. Average reconstruction error is: 9.5372
 epoch 19/50. Took 0.54101 seconds. Average reconstruction error is: 9.4702
 epoch 20/50. Took 0.53985 seconds. Average reconstruction error is: 9.3529
 epoch 21/50. Took 0.53947 seconds. Average reconstruction error is: 9.273
 epoch 22/50. Took 0.543 seconds. Average reconstruction error is: 9.2563
 epoch 23/50. Took 0.54178 seconds. Average reconstruction error is: 9.1663
 epoch 24/50. Took 0.53896 seconds. Average reconstruction error is: 9.1355
 epoch 25/50. Took 0.54021 seconds. Average reconstruction error is: 9.1141
 epoch 26/50. Took 0.53558 seconds. Average reconstruction error is: 8.9997
 epoch 27/50. Took 0.54082 seconds. Average reconstruction error is: 8.9987
 epoch 28/50. Took 0.54015 seconds. Average reconstruction error is: 8.9349
 epoch 29/50. Took 0.53985 seconds. Average reconstruction error is: 8.8837
 epoch 30/50. Took 0.54222 seconds. Average reconstruction error is: 8.853
 epoch 31/50. Took 0.54038 seconds. Average reconstruction error is: 8.8362
 epoch 32/50. Took 0.53798 seconds. Average reconstruction error is: 8.7824
 epoch 33/50. Took 0.54143 seconds. Average reconstruction error is: 8.7722
 epoch 34/50. Took 0.54122 seconds. Average reconstruction error is: 8.7342
 epoch 35/50. Took 0.53503 seconds. Average reconstruction error is: 8.6918
 epoch 36/50. Took 0.53636 seconds. Average reconstruction error is: 8.6718
 epoch 37/50. Took 0.53893 seconds. Average reconstruction error is: 8.6654
 epoch 38/50. Took 0.54145 seconds. Average reconstruction error is: 8.6229
 epoch 39/50. Took 0.53827 seconds. Average reconstruction error is: 8.5791
 epoch 40/50. Took 0.5381 seconds. Average reconstruction error is: 8.5837
 epoch 41/50. Took 0.5382 seconds. Average reconstruction error is: 8.571
 epoch 42/50. Took 0.53819 seconds. Average reconstruction error is: 8.5344
 epoch 43/50. Took 0.53983 seconds. Average reconstruction error is: 8.4945
 epoch 44/50. Took 0.53991 seconds. Average reconstruction error is: 8.4675
 epoch 45/50. Took 0.53592 seconds. Average reconstruction error is: 8.4169
 epoch 46/50. Took 0.53759 seconds. Average reconstruction error is: 8.4514
 epoch 47/50. Took 0.54061 seconds. Average reconstruction error is: 8.4185
 epoch 48/50. Took 0.53548 seconds. Average reconstruction error is: 8.3999
 epoch 49/50. Took 0.5337 seconds. Average reconstruction error is: 8.3871
 epoch 50/50. Took 0.54246 seconds. Average reconstruction error is: 8.373
Training binary-binary RBM in layer 2 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.19018 seconds. Average reconstruction error is: 15.0887
 epoch 2/50. Took 0.19275 seconds. Average reconstruction error is: 9.1491
 epoch 3/50. Took 0.19328 seconds. Average reconstruction error is: 8.0691
 epoch 4/50. Took 0.19429 seconds. Average reconstruction error is: 7.5203
 epoch 5/50. Took 0.19159 seconds. Average reconstruction error is: 7.167
 epoch 6/50. Took 0.19255 seconds. Average reconstruction error is: 6.9216
 epoch 7/50. Took 0.19345 seconds. Average reconstruction error is: 6.7071
 epoch 8/50. Took 0.19503 seconds. Average reconstruction error is: 6.5491
 epoch 9/50. Took 0.19305 seconds. Average reconstruction error is: 6.4128
 epoch 10/50. Took 0.19309 seconds. Average reconstruction error is: 6.3012
 epoch 11/50. Took 0.19534 seconds. Average reconstruction error is: 6.2149
 epoch 12/50. Took 0.19293 seconds. Average reconstruction error is: 6.0947
 epoch 13/50. Took 0.1939 seconds. Average reconstruction error is: 6.0407
 epoch 14/50. Took 0.19432 seconds. Average reconstruction error is: 5.9653
 epoch 15/50. Took 0.19255 seconds. Average reconstruction error is: 5.8948
 epoch 16/50. Took 0.19345 seconds. Average reconstruction error is: 5.8261
 epoch 17/50. Took 0.19303 seconds. Average reconstruction error is: 5.7579
 epoch 18/50. Took 0.19387 seconds. Average reconstruction error is: 5.69
 epoch 19/50. Took 0.19417 seconds. Average reconstruction error is: 5.6451
 epoch 20/50. Took 0.19154 seconds. Average reconstruction error is: 5.6021
 epoch 21/50. Took 0.19208 seconds. Average reconstruction error is: 5.5527
 epoch 22/50. Took 0.19478 seconds. Average reconstruction error is: 5.5095
 epoch 23/50. Took 0.19292 seconds. Average reconstruction error is: 5.4776
 epoch 24/50. Took 0.19442 seconds. Average reconstruction error is: 5.43
 epoch 25/50. Took 0.19333 seconds. Average reconstruction error is: 5.4015
 epoch 26/50. Took 0.19376 seconds. Average reconstruction error is: 5.3559
 epoch 27/50. Took 0.19116 seconds. Average reconstruction error is: 5.3135
 epoch 28/50. Took 0.19227 seconds. Average reconstruction error is: 5.2561
 epoch 29/50. Took 0.1937 seconds. Average reconstruction error is: 5.2144
 epoch 30/50. Took 0.19329 seconds. Average reconstruction error is: 5.1909
 epoch 31/50. Took 0.19423 seconds. Average reconstruction error is: 5.1632
 epoch 32/50. Took 0.195 seconds. Average reconstruction error is: 5.1381
 epoch 33/50. Took 0.19388 seconds. Average reconstruction error is: 5.0862
 epoch 34/50. Took 0.19359 seconds. Average reconstruction error is: 5.0591
 epoch 35/50. Took 0.19208 seconds. Average reconstruction error is: 5.056
 epoch 36/50. Took 0.195 seconds. Average reconstruction error is: 5.0125
 epoch 37/50. Took 0.19301 seconds. Average reconstruction error is: 4.9924
 epoch 38/50. Took 0.19447 seconds. Average reconstruction error is: 4.9699
 epoch 39/50. Took 0.19391 seconds. Average reconstruction error is: 4.9577
 epoch 40/50. Took 0.1926 seconds. Average reconstruction error is: 4.9196
 epoch 41/50. Took 0.19559 seconds. Average reconstruction error is: 4.9193
 epoch 42/50. Took 0.19144 seconds. Average reconstruction error is: 4.8704
 epoch 43/50. Took 0.19325 seconds. Average reconstruction error is: 4.8798
 epoch 44/50. Took 0.19331 seconds. Average reconstruction error is: 4.867
 epoch 45/50. Took 0.19233 seconds. Average reconstruction error is: 4.8399
 epoch 46/50. Took 0.19356 seconds. Average reconstruction error is: 4.8216
 epoch 47/50. Took 0.19281 seconds. Average reconstruction error is: 4.8016
 epoch 48/50. Took 0.19358 seconds. Average reconstruction error is: 4.7932
 epoch 49/50. Took 0.19275 seconds. Average reconstruction error is: 4.7736
 epoch 50/50. Took 0.19318 seconds. Average reconstruction error is: 4.7443
Training binary-binary RBM in layer 3 (200  200) with CD1 for 50 epochs
 epoch 1/50. Took 0.19017 seconds. Average reconstruction error is: 12.7479
 epoch 2/50. Took 0.19134 seconds. Average reconstruction error is: 7.5367
 epoch 3/50. Took 0.19054 seconds. Average reconstruction error is: 6.6564
 epoch 4/50. Took 0.19269 seconds. Average reconstruction error is: 6.2173
 epoch 5/50. Took 0.19326 seconds. Average reconstruction error is: 5.9261
 epoch 6/50. Took 0.1914 seconds. Average reconstruction error is: 5.7257
 epoch 7/50. Took 0.19125 seconds. Average reconstruction error is: 5.5732
 epoch 8/50. Took 0.192 seconds. Average reconstruction error is: 5.416
 epoch 9/50. Took 0.19048 seconds. Average reconstruction error is: 5.2979
 epoch 10/50. Took 0.19405 seconds. Average reconstruction error is: 5.1975
 epoch 11/50. Took 0.19266 seconds. Average reconstruction error is: 5.1266
 epoch 12/50. Took 0.19173 seconds. Average reconstruction error is: 5.0524
 epoch 13/50. Took 0.19355 seconds. Average reconstruction error is: 4.959
 epoch 14/50. Took 0.1913 seconds. Average reconstruction error is: 4.9349
 epoch 15/50. Took 0.19308 seconds. Average reconstruction error is: 4.8774
 epoch 16/50. Took 0.1918 seconds. Average reconstruction error is: 4.8087
 epoch 17/50. Took 0.19226 seconds. Average reconstruction error is: 4.762
 epoch 18/50. Took 0.19146 seconds. Average reconstruction error is: 4.7174
 epoch 19/50. Took 0.19271 seconds. Average reconstruction error is: 4.6644
 epoch 20/50. Took 0.19269 seconds. Average reconstruction error is: 4.6157
 epoch 21/50. Took 0.19154 seconds. Average reconstruction error is: 4.6053
 epoch 22/50. Took 0.19293 seconds. Average reconstruction error is: 4.573
 epoch 23/50. Took 0.1924 seconds. Average reconstruction error is: 4.5429
 epoch 24/50. Took 0.19212 seconds. Average reconstruction error is: 4.4872
 epoch 25/50. Took 0.19202 seconds. Average reconstruction error is: 4.4787
 epoch 26/50. Took 0.19182 seconds. Average reconstruction error is: 4.4198
 epoch 27/50. Took 0.19271 seconds. Average reconstruction error is: 4.3874
 epoch 28/50. Took 0.1915 seconds. Average reconstruction error is: 4.3569
 epoch 29/50. Took 0.19065 seconds. Average reconstruction error is: 4.3356
 epoch 30/50. Took 0.1913 seconds. Average reconstruction error is: 4.2928
 epoch 31/50. Took 0.19208 seconds. Average reconstruction error is: 4.2808
 epoch 32/50. Took 0.19322 seconds. Average reconstruction error is: 4.2713
 epoch 33/50. Took 0.19074 seconds. Average reconstruction error is: 4.2515
 epoch 34/50. Took 0.19335 seconds. Average reconstruction error is: 4.2201
 epoch 35/50. Took 0.19375 seconds. Average reconstruction error is: 4.2054
 epoch 36/50. Took 0.19258 seconds. Average reconstruction error is: 4.1699
 epoch 37/50. Took 0.19178 seconds. Average reconstruction error is: 4.1441
 epoch 38/50. Took 0.19233 seconds. Average reconstruction error is: 4.1319
 epoch 39/50. Took 0.19156 seconds. Average reconstruction error is: 4.1045
 epoch 40/50. Took 0.19291 seconds. Average reconstruction error is: 4.0699
 epoch 41/50. Took 0.1928 seconds. Average reconstruction error is: 4.0679
 epoch 42/50. Took 0.19188 seconds. Average reconstruction error is: 4.0671
 epoch 43/50. Took 0.19207 seconds. Average reconstruction error is: 4.05
 epoch 44/50. Took 0.19262 seconds. Average reconstruction error is: 4.0424
 epoch 45/50. Took 0.19284 seconds. Average reconstruction error is: 4.0463
 epoch 46/50. Took 0.1904 seconds. Average reconstruction error is: 4.027
 epoch 47/50. Took 0.19229 seconds. Average reconstruction error is: 3.997
 epoch 48/50. Took 0.19147 seconds. Average reconstruction error is: 3.9916
 epoch 49/50. Took 0.19068 seconds. Average reconstruction error is: 3.9705
 epoch 50/50. Took 0.19193 seconds. Average reconstruction error is: 3.9787
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.54368 seconds. 
  Full-batch training loss = 0.283852, test loss = 0.288708
  Training set accuracy = 0.919700, Test set accuracy = 0.919100
 epoch 2/100. Took 0.54831 seconds. 
  Full-batch training loss = 0.228695, test loss = 0.247192
  Training set accuracy = 0.936900, Test set accuracy = 0.931300
 epoch 3/100. Took 0.54651 seconds. 
  Full-batch training loss = 0.192573, test loss = 0.221145
  Training set accuracy = 0.946700, Test set accuracy = 0.937700
 epoch 4/100. Took 0.54884 seconds. 
  Full-batch training loss = 0.161278, test loss = 0.200130
  Training set accuracy = 0.956000, Test set accuracy = 0.942800
 epoch 5/100. Took 0.54704 seconds. 
  Full-batch training loss = 0.145443, test loss = 0.193087
  Training set accuracy = 0.960300, Test set accuracy = 0.944900
 epoch 6/100. Took 0.54631 seconds. 
  Full-batch training loss = 0.130355, test loss = 0.187071
  Training set accuracy = 0.966000, Test set accuracy = 0.945200
 epoch 7/100. Took 0.54728 seconds. 
  Full-batch training loss = 0.113945, test loss = 0.178897
  Training set accuracy = 0.970200, Test set accuracy = 0.948900
 epoch 8/100. Took 0.54629 seconds. 
  Full-batch training loss = 0.102404, test loss = 0.176926
  Training set accuracy = 0.974500, Test set accuracy = 0.948200
 epoch 9/100. Took 0.54817 seconds. 
  Full-batch training loss = 0.092527, test loss = 0.169626
  Training set accuracy = 0.977300, Test set accuracy = 0.949600
 epoch 10/100. Took 0.54671 seconds. 
  Full-batch training loss = 0.083399, test loss = 0.167171
  Training set accuracy = 0.981400, Test set accuracy = 0.950500
 epoch 11/100. Took 0.54875 seconds. 
  Full-batch training loss = 0.074292, test loss = 0.163253
  Training set accuracy = 0.983400, Test set accuracy = 0.951500
 epoch 12/100. Took 0.54843 seconds. 
  Full-batch training loss = 0.067900, test loss = 0.162821
  Training set accuracy = 0.985000, Test set accuracy = 0.952400
 epoch 13/100. Took 0.54832 seconds. 
  Full-batch training loss = 0.061935, test loss = 0.162074
  Training set accuracy = 0.986500, Test set accuracy = 0.952700
 epoch 14/100. Took 0.54586 seconds. 
  Full-batch training loss = 0.058962, test loss = 0.163584
  Training set accuracy = 0.987800, Test set accuracy = 0.951800
 epoch 15/100. Took 0.54833 seconds. 
  Full-batch training loss = 0.051567, test loss = 0.158436
  Training set accuracy = 0.989300, Test set accuracy = 0.953200
 epoch 16/100. Took 0.55148 seconds. 
  Full-batch training loss = 0.046006, test loss = 0.154987
  Training set accuracy = 0.991100, Test set accuracy = 0.954800
 epoch 17/100. Took 0.55192 seconds. 
  Full-batch training loss = 0.042303, test loss = 0.155533
  Training set accuracy = 0.992400, Test set accuracy = 0.954700
 epoch 18/100. Took 0.54789 seconds. 
  Full-batch training loss = 0.039396, test loss = 0.156638
  Training set accuracy = 0.993400, Test set accuracy = 0.955200
 epoch 19/100. Took 0.54674 seconds. 
  Full-batch training loss = 0.035414, test loss = 0.156716
  Training set accuracy = 0.994800, Test set accuracy = 0.955000
 epoch 20/100. Took 0.54646 seconds. 
  Full-batch training loss = 0.032227, test loss = 0.154850
  Training set accuracy = 0.995600, Test set accuracy = 0.956400
 epoch 21/100. Took 0.54819 seconds. 
  Full-batch training loss = 0.030392, test loss = 0.156307
  Training set accuracy = 0.995400, Test set accuracy = 0.955800
 epoch 22/100. Took 0.54833 seconds. 
  Full-batch training loss = 0.027505, test loss = 0.155279
  Training set accuracy = 0.997000, Test set accuracy = 0.954800
 epoch 23/100. Took 0.54836 seconds. 
  Full-batch training loss = 0.025165, test loss = 0.154595
  Training set accuracy = 0.997500, Test set accuracy = 0.957000
 epoch 24/100. Took 0.54828 seconds. 
  Full-batch training loss = 0.024348, test loss = 0.157922
  Training set accuracy = 0.997300, Test set accuracy = 0.955600
 epoch 25/100. Took 0.54776 seconds. 
  Full-batch training loss = 0.021493, test loss = 0.155358
  Training set accuracy = 0.998100, Test set accuracy = 0.956400
 epoch 26/100. Took 0.54814 seconds. 
  Full-batch training loss = 0.020238, test loss = 0.155901
  Training set accuracy = 0.998500, Test set accuracy = 0.955700
 epoch 27/100. Took 0.54922 seconds. 
  Full-batch training loss = 0.019020, test loss = 0.155142
  Training set accuracy = 0.998900, Test set accuracy = 0.956800
 epoch 28/100. Took 0.54754 seconds. 
  Full-batch training loss = 0.017516, test loss = 0.155364
  Training set accuracy = 0.998300, Test set accuracy = 0.956700
 epoch 29/100. Took 0.54844 seconds. 
  Full-batch training loss = 0.016772, test loss = 0.155529
  Training set accuracy = 0.999100, Test set accuracy = 0.956000
 epoch 30/100. Took 0.54939 seconds. 
  Full-batch training loss = 0.015381, test loss = 0.156817
  Training set accuracy = 0.999200, Test set accuracy = 0.956700
 epoch 31/100. Took 0.54712 seconds. 
  Full-batch training loss = 0.014743, test loss = 0.156046
  Training set accuracy = 0.999500, Test set accuracy = 0.956300
 epoch 32/100. Took 0.54754 seconds. 
  Full-batch training loss = 0.013626, test loss = 0.156108
  Training set accuracy = 0.999500, Test set accuracy = 0.957400
 epoch 33/100. Took 0.54728 seconds. 
  Full-batch training loss = 0.012928, test loss = 0.157147
  Training set accuracy = 0.999500, Test set accuracy = 0.957900
 epoch 34/100. Took 0.54565 seconds. 
  Full-batch training loss = 0.012139, test loss = 0.156938
  Training set accuracy = 0.999700, Test set accuracy = 0.957500
 epoch 35/100. Took 0.54558 seconds. 
  Full-batch training loss = 0.011345, test loss = 0.156086
  Training set accuracy = 0.999600, Test set accuracy = 0.957700
 epoch 36/100. Took 0.54677 seconds. 
  Full-batch training loss = 0.010746, test loss = 0.156540
  Training set accuracy = 0.999800, Test set accuracy = 0.957800
 epoch 37/100. Took 0.54484 seconds. 
  Full-batch training loss = 0.010201, test loss = 0.157599
  Training set accuracy = 0.999900, Test set accuracy = 0.957600
 epoch 38/100. Took 0.54558 seconds. 
  Full-batch training loss = 0.009830, test loss = 0.158319
  Training set accuracy = 0.999900, Test set accuracy = 0.957800
 epoch 39/100. Took 0.54497 seconds. 
  Full-batch training loss = 0.009351, test loss = 0.158935
  Training set accuracy = 0.999900, Test set accuracy = 0.958300
 epoch 40/100. Took 0.54699 seconds. 
  Full-batch training loss = 0.008842, test loss = 0.157778
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 41/100. Took 0.54945 seconds. 
  Full-batch training loss = 0.008419, test loss = 0.158291
  Training set accuracy = 0.999900, Test set accuracy = 0.958000
 epoch 42/100. Took 0.54541 seconds. 
  Full-batch training loss = 0.008126, test loss = 0.159049
  Training set accuracy = 0.999900, Test set accuracy = 0.957600
 epoch 43/100. Took 0.54576 seconds. 
  Full-batch training loss = 0.007711, test loss = 0.159340
  Training set accuracy = 0.999900, Test set accuracy = 0.957800
 epoch 44/100. Took 0.54567 seconds. 
  Full-batch training loss = 0.007392, test loss = 0.159134
  Training set accuracy = 0.999900, Test set accuracy = 0.958800
 epoch 45/100. Took 0.54558 seconds. 
  Full-batch training loss = 0.007151, test loss = 0.159998
  Training set accuracy = 0.999900, Test set accuracy = 0.958400
 epoch 46/100. Took 0.54797 seconds. 
  Full-batch training loss = 0.006851, test loss = 0.159412
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 47/100. Took 0.54998 seconds. 
  Full-batch training loss = 0.006564, test loss = 0.160205
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 48/100. Took 0.5463 seconds. 
  Full-batch training loss = 0.006369, test loss = 0.160130
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 49/100. Took 0.54719 seconds. 
  Full-batch training loss = 0.006105, test loss = 0.159797
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 50/100. Took 0.54588 seconds. 
  Full-batch training loss = 0.005896, test loss = 0.160630
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 51/100. Took 0.54931 seconds. 
  Full-batch training loss = 0.005662, test loss = 0.160347
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 52/100. Took 0.5505 seconds. 
  Full-batch training loss = 0.005489, test loss = 0.160683
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 53/100. Took 0.55312 seconds. 
  Full-batch training loss = 0.005325, test loss = 0.161467
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 54/100. Took 0.54667 seconds. 
  Full-batch training loss = 0.005152, test loss = 0.161542
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 55/100. Took 0.54474 seconds. 
  Full-batch training loss = 0.004982, test loss = 0.161149
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 56/100. Took 0.5469 seconds. 
  Full-batch training loss = 0.004830, test loss = 0.161646
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 57/100. Took 0.54794 seconds. 
  Full-batch training loss = 0.004702, test loss = 0.162045
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 58/100. Took 0.55203 seconds. 
  Full-batch training loss = 0.004573, test loss = 0.162250
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 59/100. Took 0.54655 seconds. 
  Full-batch training loss = 0.004445, test loss = 0.162124
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 60/100. Took 0.54486 seconds. 
  Full-batch training loss = 0.004327, test loss = 0.162497
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 61/100. Took 0.55019 seconds. 
  Full-batch training loss = 0.004216, test loss = 0.163074
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 62/100. Took 0.5477 seconds. 
  Full-batch training loss = 0.004110, test loss = 0.163144
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 63/100. Took 0.55045 seconds. 
  Full-batch training loss = 0.003995, test loss = 0.162952
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 64/100. Took 0.54571 seconds. 
  Full-batch training loss = 0.003901, test loss = 0.163531
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 65/100. Took 0.54438 seconds. 
  Full-batch training loss = 0.003834, test loss = 0.163522
  Training set accuracy = 1.000000, Test set accuracy = 0.959400
 epoch 66/100. Took 0.54762 seconds. 
  Full-batch training loss = 0.003730, test loss = 0.163950
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 67/100. Took 0.55095 seconds. 
  Full-batch training loss = 0.003641, test loss = 0.164162
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 68/100. Took 0.54953 seconds. 
  Full-batch training loss = 0.003549, test loss = 0.164183
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 69/100. Took 0.54998 seconds. 
  Full-batch training loss = 0.003474, test loss = 0.164489
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 70/100. Took 0.54868 seconds. 
  Full-batch training loss = 0.003391, test loss = 0.164559
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 71/100. Took 0.55116 seconds. 
  Full-batch training loss = 0.003318, test loss = 0.165069
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 72/100. Took 0.54828 seconds. 
  Full-batch training loss = 0.003245, test loss = 0.164889
  Training set accuracy = 1.000000, Test set accuracy = 0.959600
 epoch 73/100. Took 0.54792 seconds. 
  Full-batch training loss = 0.003180, test loss = 0.165240
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 74/100. Took 0.54573 seconds. 
  Full-batch training loss = 0.003115, test loss = 0.165340
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 75/100. Took 0.54795 seconds. 
  Full-batch training loss = 0.003051, test loss = 0.165480
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 76/100. Took 0.54886 seconds. 
  Full-batch training loss = 0.002995, test loss = 0.165899
  Training set accuracy = 1.000000, Test set accuracy = 0.959500
 epoch 77/100. Took 0.5493 seconds. 
  Full-batch training loss = 0.002933, test loss = 0.166033
  Training set accuracy = 1.000000, Test set accuracy = 0.959700
 epoch 78/100. Took 0.55047 seconds. 
  Full-batch training loss = 0.002879, test loss = 0.166276
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 79/100. Took 0.54911 seconds. 
  Full-batch training loss = 0.002822, test loss = 0.166258
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 80/100. Took 0.54732 seconds. 
  Full-batch training loss = 0.002768, test loss = 0.166535
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 81/100. Took 0.54947 seconds. 
  Full-batch training loss = 0.002723, test loss = 0.166835
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 82/100. Took 0.54841 seconds. 
  Full-batch training loss = 0.002666, test loss = 0.166877
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 83/100. Took 0.54802 seconds. 
  Full-batch training loss = 0.002622, test loss = 0.166948
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 84/100. Took 0.54556 seconds. 
  Full-batch training loss = 0.002576, test loss = 0.167278
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 85/100. Took 0.54664 seconds. 
  Full-batch training loss = 0.002533, test loss = 0.167309
  Training set accuracy = 1.000000, Test set accuracy = 0.959800
 epoch 86/100. Took 0.55054 seconds. 
  Full-batch training loss = 0.002488, test loss = 0.167531
  Training set accuracy = 1.000000, Test set accuracy = 0.959900
 epoch 87/100. Took 0.54954 seconds. 
  Full-batch training loss = 0.002444, test loss = 0.167400
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 88/100. Took 0.55043 seconds. 
  Full-batch training loss = 0.002405, test loss = 0.167500
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 89/100. Took 0.55153 seconds. 
  Full-batch training loss = 0.002364, test loss = 0.167746
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 90/100. Took 0.54769 seconds. 
  Full-batch training loss = 0.002327, test loss = 0.167861
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 91/100. Took 0.54885 seconds. 
  Full-batch training loss = 0.002289, test loss = 0.168214
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 92/100. Took 0.54619 seconds. 
  Full-batch training loss = 0.002251, test loss = 0.168333
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 93/100. Took 0.549 seconds. 
  Full-batch training loss = 0.002220, test loss = 0.168295
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 94/100. Took 0.54731 seconds. 
  Full-batch training loss = 0.002188, test loss = 0.168772
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 95/100. Took 0.5468 seconds. 
  Full-batch training loss = 0.002152, test loss = 0.168630
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
 epoch 96/100. Took 0.54441 seconds. 
  Full-batch training loss = 0.002120, test loss = 0.168935
  Training set accuracy = 1.000000, Test set accuracy = 0.960000
 epoch 97/100. Took 0.54545 seconds. 
  Full-batch training loss = 0.002087, test loss = 0.168965
  Training set accuracy = 1.000000, Test set accuracy = 0.960400
 epoch 98/100. Took 0.54843 seconds. 
  Full-batch training loss = 0.002057, test loss = 0.169176
  Training set accuracy = 1.000000, Test set accuracy = 0.960100
 epoch 99/100. Took 0.54542 seconds. 
  Full-batch training loss = 0.002026, test loss = 0.169224
  Training set accuracy = 1.000000, Test set accuracy = 0.960200
 epoch 100/100. Took 0.54688 seconds. 
  Full-batch training loss = 0.001998, test loss = 0.169458
  Training set accuracy = 1.000000, Test set accuracy = 0.960300
Elapsed time is 158.150791 seconds.
End Training
