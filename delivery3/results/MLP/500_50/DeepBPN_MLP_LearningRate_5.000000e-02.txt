Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.50793 seconds. 
  Full-batch training loss = 0.357267, test loss = 0.374412
  Training set accuracy = 0.893000, Test set accuracy = 0.887800
 epoch 2/100. Took 0.50933 seconds. 
  Full-batch training loss = 0.248959, test loss = 0.274511
  Training set accuracy = 0.924300, Test set accuracy = 0.917400
 epoch 3/100. Took 0.50672 seconds. 
  Full-batch training loss = 0.147139, test loss = 0.205558
  Training set accuracy = 0.955700, Test set accuracy = 0.938800
 epoch 4/100. Took 0.50664 seconds. 
  Full-batch training loss = 0.105998, test loss = 0.174534
  Training set accuracy = 0.969200, Test set accuracy = 0.945000
 epoch 5/100. Took 0.50714 seconds. 
  Full-batch training loss = 0.105712, test loss = 0.202374
  Training set accuracy = 0.966900, Test set accuracy = 0.938400
 epoch 6/100. Took 0.50797 seconds. 
  Full-batch training loss = 0.079499, test loss = 0.183159
  Training set accuracy = 0.976700, Test set accuracy = 0.942100
 epoch 7/100. Took 0.51252 seconds. 
  Full-batch training loss = 0.049315, test loss = 0.152170
  Training set accuracy = 0.987500, Test set accuracy = 0.952900
 epoch 8/100. Took 0.5096 seconds. 
  Full-batch training loss = 0.031305, test loss = 0.146256
  Training set accuracy = 0.992200, Test set accuracy = 0.957000
 epoch 9/100. Took 0.50677 seconds. 
  Full-batch training loss = 0.025692, test loss = 0.152443
  Training set accuracy = 0.994400, Test set accuracy = 0.957400
 epoch 10/100. Took 0.50831 seconds. 
  Full-batch training loss = 0.015063, test loss = 0.139626
  Training set accuracy = 0.997300, Test set accuracy = 0.960700
 epoch 11/100. Took 0.54154 seconds. 
  Full-batch training loss = 0.013766, test loss = 0.152885
  Training set accuracy = 0.997800, Test set accuracy = 0.960100
 epoch 12/100. Took 0.5066 seconds. 
  Full-batch training loss = 0.006438, test loss = 0.153667
  Training set accuracy = 0.999400, Test set accuracy = 0.961400
 epoch 13/100. Took 0.51503 seconds. 
  Full-batch training loss = 0.005285, test loss = 0.151264
  Training set accuracy = 0.999900, Test set accuracy = 0.962600
 epoch 14/100. Took 0.50599 seconds. 
  Full-batch training loss = 0.003571, test loss = 0.164593
  Training set accuracy = 0.999900, Test set accuracy = 0.960000
 epoch 15/100. Took 0.50654 seconds. 
  Full-batch training loss = 0.002390, test loss = 0.153310
  Training set accuracy = 0.999900, Test set accuracy = 0.962800
 epoch 16/100. Took 0.50478 seconds. 
  Full-batch training loss = 0.001845, test loss = 0.158309
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 17/100. Took 0.50714 seconds. 
  Full-batch training loss = 0.001549, test loss = 0.158752
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 18/100. Took 0.5071 seconds. 
  Full-batch training loss = 0.001436, test loss = 0.161438
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 19/100. Took 0.50562 seconds. 
  Full-batch training loss = 0.001223, test loss = 0.163689
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 20/100. Took 0.50729 seconds. 
  Full-batch training loss = 0.001120, test loss = 0.166775
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 21/100. Took 0.50698 seconds. 
  Full-batch training loss = 0.001015, test loss = 0.167139
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 22/100. Took 0.50735 seconds. 
  Full-batch training loss = 0.000923, test loss = 0.168224
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 23/100. Took 0.51057 seconds. 
  Full-batch training loss = 0.000847, test loss = 0.170626
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 24/100. Took 0.50589 seconds. 
  Full-batch training loss = 0.000805, test loss = 0.171120
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 25/100. Took 0.50632 seconds. 
  Full-batch training loss = 0.000760, test loss = 0.172914
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 26/100. Took 0.50891 seconds. 
  Full-batch training loss = 0.000698, test loss = 0.173974
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 27/100. Took 0.50553 seconds. 
  Full-batch training loss = 0.000652, test loss = 0.175068
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 28/100. Took 0.50855 seconds. 
  Full-batch training loss = 0.000620, test loss = 0.176675
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 29/100. Took 0.50704 seconds. 
  Full-batch training loss = 0.000587, test loss = 0.176680
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 30/100. Took 0.5092 seconds. 
  Full-batch training loss = 0.000561, test loss = 0.177978
  Training set accuracy = 1.000000, Test set accuracy = 0.964700
 epoch 31/100. Took 0.50844 seconds. 
  Full-batch training loss = 0.000531, test loss = 0.178067
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 32/100. Took 0.50855 seconds. 
  Full-batch training loss = 0.000504, test loss = 0.179773
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 33/100. Took 0.5053 seconds. 
  Full-batch training loss = 0.000484, test loss = 0.180289
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 34/100. Took 0.5077 seconds. 
  Full-batch training loss = 0.000464, test loss = 0.181406
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 35/100. Took 0.5158 seconds. 
  Full-batch training loss = 0.000446, test loss = 0.181879
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 36/100. Took 0.50716 seconds. 
  Full-batch training loss = 0.000431, test loss = 0.183456
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 37/100. Took 0.50634 seconds. 
  Full-batch training loss = 0.000411, test loss = 0.183430
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 38/100. Took 0.50742 seconds. 
  Full-batch training loss = 0.000394, test loss = 0.183935
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 39/100. Took 0.50679 seconds. 
  Full-batch training loss = 0.000378, test loss = 0.184885
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 40/100. Took 0.51008 seconds. 
  Full-batch training loss = 0.000369, test loss = 0.185732
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 41/100. Took 0.51166 seconds. 
  Full-batch training loss = 0.000353, test loss = 0.186382
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 42/100. Took 0.50991 seconds. 
  Full-batch training loss = 0.000342, test loss = 0.187131
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 43/100. Took 0.50795 seconds. 
  Full-batch training loss = 0.000330, test loss = 0.187336
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 44/100. Took 0.51497 seconds. 
  Full-batch training loss = 0.000320, test loss = 0.188205
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 45/100. Took 0.5092 seconds. 
  Full-batch training loss = 0.000310, test loss = 0.188288
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 46/100. Took 0.51162 seconds. 
  Full-batch training loss = 0.000300, test loss = 0.189126
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 47/100. Took 0.50875 seconds. 
  Full-batch training loss = 0.000292, test loss = 0.189217
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 48/100. Took 0.50798 seconds. 
  Full-batch training loss = 0.000282, test loss = 0.190350
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 49/100. Took 0.51115 seconds. 
  Full-batch training loss = 0.000275, test loss = 0.190733
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 50/100. Took 0.50668 seconds. 
  Full-batch training loss = 0.000268, test loss = 0.191015
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 51/100. Took 0.51111 seconds. 
  Full-batch training loss = 0.000260, test loss = 0.191758
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 52/100. Took 0.50854 seconds. 
  Full-batch training loss = 0.000254, test loss = 0.192458
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 53/100. Took 0.50747 seconds. 
  Full-batch training loss = 0.000249, test loss = 0.192963
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 54/100. Took 0.51067 seconds. 
  Full-batch training loss = 0.000240, test loss = 0.193436
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 55/100. Took 0.5074 seconds. 
  Full-batch training loss = 0.000235, test loss = 0.193409
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 56/100. Took 0.51071 seconds. 
  Full-batch training loss = 0.000229, test loss = 0.193888
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 57/100. Took 0.51232 seconds. 
  Full-batch training loss = 0.000224, test loss = 0.194391
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 58/100. Took 0.50872 seconds. 
  Full-batch training loss = 0.000219, test loss = 0.195069
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 59/100. Took 0.51066 seconds. 
  Full-batch training loss = 0.000214, test loss = 0.195672
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 60/100. Took 0.50866 seconds. 
  Full-batch training loss = 0.000209, test loss = 0.195823
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 61/100. Took 0.5098 seconds. 
  Full-batch training loss = 0.000204, test loss = 0.196003
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 62/100. Took 0.50919 seconds. 
  Full-batch training loss = 0.000200, test loss = 0.196781
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 63/100. Took 0.5137 seconds. 
  Full-batch training loss = 0.000196, test loss = 0.197073
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 64/100. Took 0.5113 seconds. 
  Full-batch training loss = 0.000192, test loss = 0.197462
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 65/100. Took 0.50821 seconds. 
  Full-batch training loss = 0.000188, test loss = 0.197604
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 66/100. Took 0.50967 seconds. 
  Full-batch training loss = 0.000185, test loss = 0.198532
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 67/100. Took 0.50838 seconds. 
  Full-batch training loss = 0.000180, test loss = 0.198220
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 68/100. Took 0.50855 seconds. 
  Full-batch training loss = 0.000177, test loss = 0.198414
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 69/100. Took 0.50856 seconds. 
  Full-batch training loss = 0.000174, test loss = 0.199428
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 70/100. Took 0.50983 seconds. 
  Full-batch training loss = 0.000170, test loss = 0.199288
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 71/100. Took 0.50795 seconds. 
  Full-batch training loss = 0.000167, test loss = 0.199980
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 72/100. Took 0.51194 seconds. 
  Full-batch training loss = 0.000164, test loss = 0.200127
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 73/100. Took 0.50999 seconds. 
  Full-batch training loss = 0.000161, test loss = 0.200568
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 74/100. Took 0.51155 seconds. 
  Full-batch training loss = 0.000158, test loss = 0.200985
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 75/100. Took 0.51498 seconds. 
  Full-batch training loss = 0.000156, test loss = 0.201005
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 76/100. Took 0.50798 seconds. 
  Full-batch training loss = 0.000153, test loss = 0.201677
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 77/100. Took 0.51022 seconds. 
  Full-batch training loss = 0.000150, test loss = 0.201738
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 78/100. Took 0.51014 seconds. 
  Full-batch training loss = 0.000148, test loss = 0.201996
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 79/100. Took 0.51267 seconds. 
  Full-batch training loss = 0.000146, test loss = 0.202585
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 80/100. Took 0.51377 seconds. 
  Full-batch training loss = 0.000143, test loss = 0.203105
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 81/100. Took 0.50787 seconds. 
  Full-batch training loss = 0.000141, test loss = 0.202962
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 82/100. Took 0.50818 seconds. 
  Full-batch training loss = 0.000138, test loss = 0.203236
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 83/100. Took 0.50881 seconds. 
  Full-batch training loss = 0.000136, test loss = 0.203676
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 84/100. Took 0.50777 seconds. 
  Full-batch training loss = 0.000134, test loss = 0.204034
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 85/100. Took 0.55232 seconds. 
  Full-batch training loss = 0.000132, test loss = 0.203983
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 86/100. Took 0.51437 seconds. 
  Full-batch training loss = 0.000130, test loss = 0.204180
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 87/100. Took 0.50724 seconds. 
  Full-batch training loss = 0.000128, test loss = 0.204942
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 88/100. Took 0.50632 seconds. 
  Full-batch training loss = 0.000126, test loss = 0.205223
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 89/100. Took 0.51104 seconds. 
  Full-batch training loss = 0.000124, test loss = 0.205318
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 90/100. Took 0.50908 seconds. 
  Full-batch training loss = 0.000123, test loss = 0.205550
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 91/100. Took 0.50842 seconds. 
  Full-batch training loss = 0.000121, test loss = 0.205864
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 92/100. Took 0.50803 seconds. 
  Full-batch training loss = 0.000119, test loss = 0.206153
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 93/100. Took 0.50903 seconds. 
  Full-batch training loss = 0.000118, test loss = 0.206326
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 94/100. Took 0.50729 seconds. 
  Full-batch training loss = 0.000116, test loss = 0.206827
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 95/100. Took 0.51606 seconds. 
  Full-batch training loss = 0.000115, test loss = 0.207022
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 96/100. Took 0.51261 seconds. 
  Full-batch training loss = 0.000113, test loss = 0.207188
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 97/100. Took 0.51349 seconds. 
  Full-batch training loss = 0.000111, test loss = 0.207361
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 98/100. Took 0.5107 seconds. 
  Full-batch training loss = 0.000110, test loss = 0.207663
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 99/100. Took 0.51101 seconds. 
  Full-batch training loss = 0.000109, test loss = 0.207979
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 100/100. Took 0.51182 seconds. 
  Full-batch training loss = 0.000107, test loss = 0.207883
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
Elapsed time is 95.716815 seconds.
End Training
