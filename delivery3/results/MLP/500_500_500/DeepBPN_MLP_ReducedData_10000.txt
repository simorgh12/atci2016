Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.51606 seconds. 
  Full-batch training loss = 0.266409, test loss = 0.281631
  Training set accuracy = 0.918300, Test set accuracy = 0.914400
 epoch 2/100. Took 0.51428 seconds. 
  Full-batch training loss = 0.163407, test loss = 0.222647
  Training set accuracy = 0.950300, Test set accuracy = 0.932000
 epoch 3/100. Took 0.53301 seconds. 
  Full-batch training loss = 0.096145, test loss = 0.202723
  Training set accuracy = 0.968600, Test set accuracy = 0.942300
 epoch 4/100. Took 0.52502 seconds. 
  Full-batch training loss = 0.086525, test loss = 0.193140
  Training set accuracy = 0.973400, Test set accuracy = 0.943400
 epoch 5/100. Took 0.5182 seconds. 
  Full-batch training loss = 0.053024, test loss = 0.175135
  Training set accuracy = 0.981900, Test set accuracy = 0.953600
 epoch 6/100. Took 0.52742 seconds. 
  Full-batch training loss = 0.047490, test loss = 0.178576
  Training set accuracy = 0.984400, Test set accuracy = 0.951900
 epoch 7/100. Took 0.51747 seconds. 
  Full-batch training loss = 0.046825, test loss = 0.194866
  Training set accuracy = 0.985200, Test set accuracy = 0.950100
 epoch 8/100. Took 0.52448 seconds. 
  Full-batch training loss = 0.028989, test loss = 0.166786
  Training set accuracy = 0.990200, Test set accuracy = 0.957800
 epoch 9/100. Took 0.52287 seconds. 
  Full-batch training loss = 0.009237, test loss = 0.158983
  Training set accuracy = 0.997600, Test set accuracy = 0.962400
 epoch 10/100. Took 0.52281 seconds. 
  Full-batch training loss = 0.037915, test loss = 0.200286
  Training set accuracy = 0.986300, Test set accuracy = 0.950500
 epoch 11/100. Took 0.51379 seconds. 
  Full-batch training loss = 0.029791, test loss = 0.211716
  Training set accuracy = 0.989100, Test set accuracy = 0.952500
 epoch 12/100. Took 0.52035 seconds. 
  Full-batch training loss = 0.022540, test loss = 0.227499
  Training set accuracy = 0.992900, Test set accuracy = 0.951800
 epoch 13/100. Took 0.52391 seconds. 
  Full-batch training loss = 0.047701, test loss = 0.253064
  Training set accuracy = 0.985700, Test set accuracy = 0.952200
 epoch 14/100. Took 0.5161 seconds. 
  Full-batch training loss = 0.031832, test loss = 0.193531
  Training set accuracy = 0.991200, Test set accuracy = 0.956000
 epoch 15/100. Took 0.54202 seconds. 
  Full-batch training loss = 0.056633, test loss = 0.267623
  Training set accuracy = 0.982500, Test set accuracy = 0.942800
 epoch 16/100. Took 0.52223 seconds. 
  Full-batch training loss = 0.018060, test loss = 0.179557
  Training set accuracy = 0.994400, Test set accuracy = 0.959800
 epoch 17/100. Took 0.52198 seconds. 
  Full-batch training loss = 0.004186, test loss = 0.165755
  Training set accuracy = 0.999000, Test set accuracy = 0.964600
 epoch 18/100. Took 0.51915 seconds. 
  Full-batch training loss = 0.001235, test loss = 0.171837
  Training set accuracy = 0.999700, Test set accuracy = 0.965200
 epoch 19/100. Took 0.51468 seconds. 
  Full-batch training loss = 0.014343, test loss = 0.192899
  Training set accuracy = 0.995900, Test set accuracy = 0.963000
 epoch 20/100. Took 0.51731 seconds. 
  Full-batch training loss = 0.000951, test loss = 0.172781
  Training set accuracy = 0.999900, Test set accuracy = 0.966500
 epoch 21/100. Took 0.51898 seconds. 
  Full-batch training loss = 0.000236, test loss = 0.167340
  Training set accuracy = 1.000000, Test set accuracy = 0.968400
 epoch 22/100. Took 0.5178 seconds. 
  Full-batch training loss = 0.000162, test loss = 0.168983
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 23/100. Took 0.53535 seconds. 
  Full-batch training loss = 0.000132, test loss = 0.171074
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 24/100. Took 0.51801 seconds. 
  Full-batch training loss = 0.000112, test loss = 0.172956
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 25/100. Took 0.51461 seconds. 
  Full-batch training loss = 0.000097, test loss = 0.174557
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 26/100. Took 0.51315 seconds. 
  Full-batch training loss = 0.000086, test loss = 0.175983
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 27/100. Took 0.5489 seconds. 
  Full-batch training loss = 0.000077, test loss = 0.177395
  Training set accuracy = 1.000000, Test set accuracy = 0.968700
 epoch 28/100. Took 0.52743 seconds. 
  Full-batch training loss = 0.000070, test loss = 0.178637
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 29/100. Took 0.52649 seconds. 
  Full-batch training loss = 0.000064, test loss = 0.179822
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 30/100. Took 0.53591 seconds. 
  Full-batch training loss = 0.000059, test loss = 0.180895
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 31/100. Took 0.53299 seconds. 
  Full-batch training loss = 0.000055, test loss = 0.181902
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 32/100. Took 0.53547 seconds. 
  Full-batch training loss = 0.000051, test loss = 0.182870
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 33/100. Took 0.5295 seconds. 
  Full-batch training loss = 0.000048, test loss = 0.183756
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 34/100. Took 0.57277 seconds. 
  Full-batch training loss = 0.000045, test loss = 0.184596
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 35/100. Took 0.5349 seconds. 
  Full-batch training loss = 0.000042, test loss = 0.185397
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 36/100. Took 0.53846 seconds. 
  Full-batch training loss = 0.000040, test loss = 0.186188
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 37/100. Took 0.55568 seconds. 
  Full-batch training loss = 0.000038, test loss = 0.186946
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 38/100. Took 0.52394 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.187634
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 39/100. Took 0.53872 seconds. 
  Full-batch training loss = 0.000035, test loss = 0.188316
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 40/100. Took 0.52641 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.188951
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 41/100. Took 0.55203 seconds. 
  Full-batch training loss = 0.000032, test loss = 0.189564
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 42/100. Took 0.54495 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.190168
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 43/100. Took 0.55904 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.190727
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 44/100. Took 0.53535 seconds. 
  Full-batch training loss = 0.000028, test loss = 0.191293
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 45/100. Took 0.54343 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.191830
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 46/100. Took 0.53374 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.192373
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 47/100. Took 0.5406 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.192886
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 48/100. Took 0.53728 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.193397
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 49/100. Took 0.5501 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.193868
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 50/100. Took 0.55117 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.194329
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 51/100. Took 0.53458 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.194792
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 52/100. Took 0.56935 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.195223
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 53/100. Took 0.55431 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.195660
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 54/100. Took 0.52759 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.196075
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 55/100. Took 0.57526 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.196457
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 56/100. Took 0.55394 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.196834
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 57/100. Took 0.56381 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.197252
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 58/100. Took 0.55566 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.197621
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 59/100. Took 0.56596 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.197989
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 60/100. Took 0.67397 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.198356
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 61/100. Took 0.5643 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.198704
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 62/100. Took 0.596 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.199025
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 63/100. Took 0.57606 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.199367
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 64/100. Took 0.54948 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.199687
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 65/100. Took 0.54692 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.200005
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 66/100. Took 0.55202 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.200333
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 67/100. Took 0.54646 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.200634
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 68/100. Took 0.55089 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.200934
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 69/100. Took 0.60826 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.201239
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 70/100. Took 0.71126 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.201529
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 71/100. Took 0.5689 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.201816
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 72/100. Took 0.57392 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.202115
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 73/100. Took 0.61252 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.202378
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 74/100. Took 0.58981 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.202655
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 75/100. Took 0.57023 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.202912
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 76/100. Took 0.55031 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.203183
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 77/100. Took 0.57192 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.203442
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 78/100. Took 0.58665 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.203699
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 79/100. Took 0.6325 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.203965
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 80/100. Took 0.58829 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.204196
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 81/100. Took 0.57116 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.204435
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 82/100. Took 0.56771 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.204681
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 83/100. Took 0.58727 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.204917
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 84/100. Took 0.57121 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.205158
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 85/100. Took 0.56058 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.205387
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 86/100. Took 0.54251 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.205612
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 87/100. Took 0.54714 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.205846
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 88/100. Took 0.56671 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206059
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 89/100. Took 0.70923 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206277
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 90/100. Took 0.57093 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206500
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 91/100. Took 0.54672 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206707
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 92/100. Took 0.5512 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206924
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 93/100. Took 0.59047 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.207135
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 94/100. Took 0.54657 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.207336
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 95/100. Took 0.5596 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207547
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 96/100. Took 0.55046 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207751
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 97/100. Took 0.55956 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207940
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 98/100. Took 0.64538 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.208134
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 99/100. Took 0.66958 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.208326
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 100/100. Took 0.58141 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.208531
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
Elapsed time is 102.363530 seconds.
End Training
