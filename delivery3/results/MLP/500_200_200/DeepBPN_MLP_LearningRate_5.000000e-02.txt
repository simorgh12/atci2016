Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.56994 seconds. 
  Full-batch training loss = 0.351239, test loss = 0.370563
  Training set accuracy = 0.889800, Test set accuracy = 0.886300
 epoch 2/100. Took 0.56527 seconds. 
  Full-batch training loss = 0.192973, test loss = 0.234441
  Training set accuracy = 0.945400, Test set accuracy = 0.930800
 epoch 3/100. Took 0.56922 seconds. 
  Full-batch training loss = 0.134173, test loss = 0.193150
  Training set accuracy = 0.960700, Test set accuracy = 0.942900
 epoch 4/100. Took 0.57821 seconds. 
  Full-batch training loss = 0.107376, test loss = 0.188776
  Training set accuracy = 0.966900, Test set accuracy = 0.943700
 epoch 5/100. Took 0.57197 seconds. 
  Full-batch training loss = 0.123213, test loss = 0.217252
  Training set accuracy = 0.960600, Test set accuracy = 0.934500
 epoch 6/100. Took 0.57489 seconds. 
  Full-batch training loss = 0.081635, test loss = 0.184539
  Training set accuracy = 0.974200, Test set accuracy = 0.943900
 epoch 7/100. Took 0.5684 seconds. 
  Full-batch training loss = 0.046614, test loss = 0.158677
  Training set accuracy = 0.986000, Test set accuracy = 0.953400
 epoch 8/100. Took 0.56532 seconds. 
  Full-batch training loss = 0.053434, test loss = 0.192363
  Training set accuracy = 0.982900, Test set accuracy = 0.945200
 epoch 9/100. Took 0.56459 seconds. 
  Full-batch training loss = 0.021037, test loss = 0.147244
  Training set accuracy = 0.995900, Test set accuracy = 0.960400
 epoch 10/100. Took 0.56755 seconds. 
  Full-batch training loss = 0.013121, test loss = 0.145575
  Training set accuracy = 0.997700, Test set accuracy = 0.961700
 epoch 11/100. Took 0.56027 seconds. 
  Full-batch training loss = 0.009000, test loss = 0.150097
  Training set accuracy = 0.999300, Test set accuracy = 0.961600
 epoch 12/100. Took 0.56955 seconds. 
  Full-batch training loss = 0.006446, test loss = 0.146708
  Training set accuracy = 0.999300, Test set accuracy = 0.963600
 epoch 13/100. Took 0.56218 seconds. 
  Full-batch training loss = 0.004962, test loss = 0.155637
  Training set accuracy = 0.999800, Test set accuracy = 0.961800
 epoch 14/100. Took 0.57026 seconds. 
  Full-batch training loss = 0.003382, test loss = 0.153126
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 15/100. Took 0.57034 seconds. 
  Full-batch training loss = 0.001953, test loss = 0.152677
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 16/100. Took 0.56722 seconds. 
  Full-batch training loss = 0.001601, test loss = 0.157184
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 17/100. Took 0.56989 seconds. 
  Full-batch training loss = 0.001395, test loss = 0.158921
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 18/100. Took 0.57063 seconds. 
  Full-batch training loss = 0.001255, test loss = 0.161887
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 19/100. Took 0.56439 seconds. 
  Full-batch training loss = 0.001138, test loss = 0.162898
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 20/100. Took 0.56783 seconds. 
  Full-batch training loss = 0.001034, test loss = 0.164567
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 21/100. Took 0.56508 seconds. 
  Full-batch training loss = 0.000933, test loss = 0.165731
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 22/100. Took 0.56682 seconds. 
  Full-batch training loss = 0.000853, test loss = 0.167812
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 23/100. Took 0.56392 seconds. 
  Full-batch training loss = 0.000797, test loss = 0.169563
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 24/100. Took 0.57149 seconds. 
  Full-batch training loss = 0.000737, test loss = 0.169637
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 25/100. Took 0.56986 seconds. 
  Full-batch training loss = 0.000692, test loss = 0.171397
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 26/100. Took 0.56886 seconds. 
  Full-batch training loss = 0.000655, test loss = 0.172565
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 27/100. Took 0.56589 seconds. 
  Full-batch training loss = 0.000616, test loss = 0.173369
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 28/100. Took 0.56839 seconds. 
  Full-batch training loss = 0.000579, test loss = 0.174661
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 29/100. Took 0.56864 seconds. 
  Full-batch training loss = 0.000549, test loss = 0.175678
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 30/100. Took 0.56453 seconds. 
  Full-batch training loss = 0.000522, test loss = 0.176318
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 31/100. Took 0.56245 seconds. 
  Full-batch training loss = 0.000498, test loss = 0.177512
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 32/100. Took 0.57549 seconds. 
  Full-batch training loss = 0.000471, test loss = 0.178131
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 33/100. Took 0.56664 seconds. 
  Full-batch training loss = 0.000451, test loss = 0.178995
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 34/100. Took 0.56359 seconds. 
  Full-batch training loss = 0.000432, test loss = 0.179509
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 35/100. Took 0.57275 seconds. 
  Full-batch training loss = 0.000414, test loss = 0.180850
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 36/100. Took 0.56763 seconds. 
  Full-batch training loss = 0.000397, test loss = 0.181493
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 37/100. Took 0.55992 seconds. 
  Full-batch training loss = 0.000383, test loss = 0.182243
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 38/100. Took 0.56917 seconds. 
  Full-batch training loss = 0.000368, test loss = 0.182420
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 39/100. Took 0.56669 seconds. 
  Full-batch training loss = 0.000355, test loss = 0.183547
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 40/100. Took 0.57039 seconds. 
  Full-batch training loss = 0.000340, test loss = 0.183873
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 41/100. Took 0.56325 seconds. 
  Full-batch training loss = 0.000330, test loss = 0.184771
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 42/100. Took 0.57013 seconds. 
  Full-batch training loss = 0.000321, test loss = 0.185198
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 43/100. Took 0.57849 seconds. 
  Full-batch training loss = 0.000308, test loss = 0.186106
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 44/100. Took 0.59227 seconds. 
  Full-batch training loss = 0.000300, test loss = 0.186700
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 45/100. Took 0.56974 seconds. 
  Full-batch training loss = 0.000290, test loss = 0.187085
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 46/100. Took 0.57892 seconds. 
  Full-batch training loss = 0.000281, test loss = 0.187518
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 47/100. Took 0.5743 seconds. 
  Full-batch training loss = 0.000273, test loss = 0.188484
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 48/100. Took 0.57222 seconds. 
  Full-batch training loss = 0.000266, test loss = 0.188879
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 49/100. Took 0.57536 seconds. 
  Full-batch training loss = 0.000257, test loss = 0.189014
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 50/100. Took 0.56616 seconds. 
  Full-batch training loss = 0.000251, test loss = 0.189701
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 51/100. Took 0.57004 seconds. 
  Full-batch training loss = 0.000244, test loss = 0.190125
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 52/100. Took 0.57501 seconds. 
  Full-batch training loss = 0.000238, test loss = 0.190954
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 53/100. Took 0.57497 seconds. 
  Full-batch training loss = 0.000232, test loss = 0.191153
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 54/100. Took 0.68854 seconds. 
  Full-batch training loss = 0.000227, test loss = 0.191714
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 55/100. Took 0.59153 seconds. 
  Full-batch training loss = 0.000221, test loss = 0.192080
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 56/100. Took 0.57443 seconds. 
  Full-batch training loss = 0.000216, test loss = 0.192644
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 57/100. Took 0.57212 seconds. 
  Full-batch training loss = 0.000210, test loss = 0.192856
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 58/100. Took 0.57338 seconds. 
  Full-batch training loss = 0.000206, test loss = 0.193588
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 59/100. Took 0.57276 seconds. 
  Full-batch training loss = 0.000201, test loss = 0.193691
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 60/100. Took 0.58467 seconds. 
  Full-batch training loss = 0.000197, test loss = 0.194166
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 61/100. Took 0.5758 seconds. 
  Full-batch training loss = 0.000192, test loss = 0.194701
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 62/100. Took 0.57435 seconds. 
  Full-batch training loss = 0.000188, test loss = 0.194947
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 63/100. Took 0.58328 seconds. 
  Full-batch training loss = 0.000184, test loss = 0.195532
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 64/100. Took 0.57546 seconds. 
  Full-batch training loss = 0.000181, test loss = 0.195768
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 65/100. Took 0.56572 seconds. 
  Full-batch training loss = 0.000177, test loss = 0.196341
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 66/100. Took 0.572 seconds. 
  Full-batch training loss = 0.000174, test loss = 0.196831
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 67/100. Took 0.58953 seconds. 
  Full-batch training loss = 0.000170, test loss = 0.196995
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 68/100. Took 0.56399 seconds. 
  Full-batch training loss = 0.000167, test loss = 0.197300
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 69/100. Took 0.57496 seconds. 
  Full-batch training loss = 0.000164, test loss = 0.197435
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 70/100. Took 0.56891 seconds. 
  Full-batch training loss = 0.000161, test loss = 0.198149
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 71/100. Took 0.57703 seconds. 
  Full-batch training loss = 0.000158, test loss = 0.198558
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 72/100. Took 0.57712 seconds. 
  Full-batch training loss = 0.000155, test loss = 0.198811
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 73/100. Took 0.5756 seconds. 
  Full-batch training loss = 0.000152, test loss = 0.199159
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 74/100. Took 0.56762 seconds. 
  Full-batch training loss = 0.000149, test loss = 0.199408
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 75/100. Took 0.58333 seconds. 
  Full-batch training loss = 0.000147, test loss = 0.199932
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 76/100. Took 0.58595 seconds. 
  Full-batch training loss = 0.000144, test loss = 0.200084
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 77/100. Took 0.56777 seconds. 
  Full-batch training loss = 0.000142, test loss = 0.200437
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 78/100. Took 0.56957 seconds. 
  Full-batch training loss = 0.000140, test loss = 0.200754
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 79/100. Took 0.57493 seconds. 
  Full-batch training loss = 0.000137, test loss = 0.200945
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 80/100. Took 0.57744 seconds. 
  Full-batch training loss = 0.000135, test loss = 0.201174
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 81/100. Took 0.59662 seconds. 
  Full-batch training loss = 0.000133, test loss = 0.201418
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 82/100. Took 0.59643 seconds. 
  Full-batch training loss = 0.000131, test loss = 0.201893
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 83/100. Took 0.57501 seconds. 
  Full-batch training loss = 0.000129, test loss = 0.202274
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 84/100. Took 0.56536 seconds. 
  Full-batch training loss = 0.000127, test loss = 0.202509
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 85/100. Took 0.5694 seconds. 
  Full-batch training loss = 0.000125, test loss = 0.202819
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 86/100. Took 0.5694 seconds. 
  Full-batch training loss = 0.000123, test loss = 0.203083
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 87/100. Took 0.59297 seconds. 
  Full-batch training loss = 0.000121, test loss = 0.203262
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 88/100. Took 0.58388 seconds. 
  Full-batch training loss = 0.000119, test loss = 0.203766
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 89/100. Took 0.56959 seconds. 
  Full-batch training loss = 0.000118, test loss = 0.204011
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 90/100. Took 0.57742 seconds. 
  Full-batch training loss = 0.000116, test loss = 0.204241
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 91/100. Took 0.58957 seconds. 
  Full-batch training loss = 0.000115, test loss = 0.204507
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 92/100. Took 0.57269 seconds. 
  Full-batch training loss = 0.000113, test loss = 0.204830
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 93/100. Took 0.57104 seconds. 
  Full-batch training loss = 0.000111, test loss = 0.205139
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 94/100. Took 0.57131 seconds. 
  Full-batch training loss = 0.000110, test loss = 0.205286
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 95/100. Took 0.56849 seconds. 
  Full-batch training loss = 0.000109, test loss = 0.205555
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 96/100. Took 0.58056 seconds. 
  Full-batch training loss = 0.000107, test loss = 0.205857
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 97/100. Took 0.57774 seconds. 
  Full-batch training loss = 0.000106, test loss = 0.206179
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 98/100. Took 0.58877 seconds. 
  Full-batch training loss = 0.000104, test loss = 0.206435
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 99/100. Took 0.58 seconds. 
  Full-batch training loss = 0.000103, test loss = 0.206452
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 100/100. Took 0.57547 seconds. 
  Full-batch training loss = 0.000102, test loss = 0.206815
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
Elapsed time is 106.405977 seconds.
End Training
