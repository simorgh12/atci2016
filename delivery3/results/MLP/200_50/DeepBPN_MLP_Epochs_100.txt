Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.50646 seconds. 
  Full-batch training loss = 0.236704, test loss = 0.274091
  Training set accuracy = 0.929600, Test set accuracy = 0.916200
 epoch 2/100. Took 0.50683 seconds. 
  Full-batch training loss = 0.188234, test loss = 0.242765
  Training set accuracy = 0.942300, Test set accuracy = 0.923200
 epoch 3/100. Took 0.50939 seconds. 
  Full-batch training loss = 0.127043, test loss = 0.209636
  Training set accuracy = 0.959100, Test set accuracy = 0.935600
 epoch 4/100. Took 0.50816 seconds. 
  Full-batch training loss = 0.079497, test loss = 0.178111
  Training set accuracy = 0.978700, Test set accuracy = 0.948400
 epoch 5/100. Took 0.50441 seconds. 
  Full-batch training loss = 0.053266, test loss = 0.178321
  Training set accuracy = 0.983900, Test set accuracy = 0.949600
 epoch 6/100. Took 0.50861 seconds. 
  Full-batch training loss = 0.060034, test loss = 0.219097
  Training set accuracy = 0.980600, Test set accuracy = 0.943100
 epoch 7/100. Took 0.50768 seconds. 
  Full-batch training loss = 0.029590, test loss = 0.179469
  Training set accuracy = 0.991100, Test set accuracy = 0.955700
 epoch 8/100. Took 0.51026 seconds. 
  Full-batch training loss = 0.035017, test loss = 0.199428
  Training set accuracy = 0.988400, Test set accuracy = 0.950900
 epoch 9/100. Took 0.5087 seconds. 
  Full-batch training loss = 0.017632, test loss = 0.185277
  Training set accuracy = 0.994300, Test set accuracy = 0.956800
 epoch 10/100. Took 0.51071 seconds. 
  Full-batch training loss = 0.006822, test loss = 0.162163
  Training set accuracy = 0.998200, Test set accuracy = 0.964300
 epoch 11/100. Took 0.50795 seconds. 
  Full-batch training loss = 0.068610, test loss = 0.321879
  Training set accuracy = 0.980500, Test set accuracy = 0.942700
 epoch 12/100. Took 0.50893 seconds. 
  Full-batch training loss = 0.038454, test loss = 0.239893
  Training set accuracy = 0.989000, Test set accuracy = 0.950000
 epoch 13/100. Took 0.50652 seconds. 
  Full-batch training loss = 0.056473, test loss = 0.234420
  Training set accuracy = 0.988400, Test set accuracy = 0.951800
 epoch 14/100. Took 0.50775 seconds. 
  Full-batch training loss = 0.017682, test loss = 0.215083
  Training set accuracy = 0.994000, Test set accuracy = 0.956000
 epoch 15/100. Took 0.51141 seconds. 
  Full-batch training loss = 0.005937, test loss = 0.165460
  Training set accuracy = 0.998600, Test set accuracy = 0.964300
 epoch 16/100. Took 0.5078 seconds. 
  Full-batch training loss = 0.016969, test loss = 0.217531
  Training set accuracy = 0.994500, Test set accuracy = 0.957400
 epoch 17/100. Took 0.50959 seconds. 
  Full-batch training loss = 0.009415, test loss = 0.185763
  Training set accuracy = 0.997700, Test set accuracy = 0.961600
 epoch 18/100. Took 0.50664 seconds. 
  Full-batch training loss = 0.007779, test loss = 0.195299
  Training set accuracy = 0.998000, Test set accuracy = 0.961900
 epoch 19/100. Took 0.50737 seconds. 
  Full-batch training loss = 0.010934, test loss = 0.214885
  Training set accuracy = 0.995800, Test set accuracy = 0.957800
 epoch 20/100. Took 0.50947 seconds. 
  Full-batch training loss = 0.001790, test loss = 0.193117
  Training set accuracy = 0.999500, Test set accuracy = 0.962800
 epoch 21/100. Took 0.5083 seconds. 
  Full-batch training loss = 0.000718, test loss = 0.191919
  Training set accuracy = 0.999900, Test set accuracy = 0.964200
 epoch 22/100. Took 0.50568 seconds. 
  Full-batch training loss = 0.000152, test loss = 0.185715
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 23/100. Took 0.50768 seconds. 
  Full-batch training loss = 0.000119, test loss = 0.187922
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 24/100. Took 0.50904 seconds. 
  Full-batch training loss = 0.000100, test loss = 0.190151
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 25/100. Took 0.50787 seconds. 
  Full-batch training loss = 0.000087, test loss = 0.191919
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 26/100. Took 0.51038 seconds. 
  Full-batch training loss = 0.000076, test loss = 0.193510
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 27/100. Took 0.51086 seconds. 
  Full-batch training loss = 0.000069, test loss = 0.194830
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 28/100. Took 0.509 seconds. 
  Full-batch training loss = 0.000063, test loss = 0.196146
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 29/100. Took 0.50921 seconds. 
  Full-batch training loss = 0.000057, test loss = 0.197361
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 30/100. Took 0.5084 seconds. 
  Full-batch training loss = 0.000053, test loss = 0.198451
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 31/100. Took 0.50787 seconds. 
  Full-batch training loss = 0.000049, test loss = 0.199465
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 32/100. Took 0.512 seconds. 
  Full-batch training loss = 0.000046, test loss = 0.200402
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 33/100. Took 0.51001 seconds. 
  Full-batch training loss = 0.000043, test loss = 0.201307
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 34/100. Took 0.51134 seconds. 
  Full-batch training loss = 0.000040, test loss = 0.202117
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 35/100. Took 0.51298 seconds. 
  Full-batch training loss = 0.000038, test loss = 0.202957
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 36/100. Took 0.51256 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.203714
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 37/100. Took 0.51282 seconds. 
  Full-batch training loss = 0.000034, test loss = 0.204444
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 38/100. Took 0.51487 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.205135
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 39/100. Took 0.51329 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.205789
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 40/100. Took 0.51064 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.206418
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 41/100. Took 0.51842 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.207044
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 42/100. Took 0.51265 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.207626
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 43/100. Took 0.51302 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.208195
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 44/100. Took 0.51503 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.208741
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 45/100. Took 0.51405 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.209275
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 46/100. Took 0.51492 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.209787
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 47/100. Took 0.51669 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.210299
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 48/100. Took 0.51751 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.210765
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 49/100. Took 0.51433 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.211258
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 50/100. Took 0.51607 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.211711
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 51/100. Took 0.51529 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.212162
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 52/100. Took 0.51562 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.212598
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 53/100. Took 0.51522 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.213021
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 54/100. Took 0.51738 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.213422
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 55/100. Took 0.51452 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.213841
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 56/100. Took 0.5155 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.214240
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 57/100. Took 0.51545 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.214619
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 58/100. Took 0.51531 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.214998
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 59/100. Took 0.51576 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.215369
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 60/100. Took 0.51744 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.215722
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 61/100. Took 0.51773 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.216068
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 62/100. Took 0.51788 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.216401
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 63/100. Took 0.51788 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.216742
  Training set accuracy = 1.000000, Test set accuracy = 0.966600
 epoch 64/100. Took 0.51459 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.217075
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 65/100. Took 0.51814 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.217400
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 66/100. Took 0.51793 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.217704
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 67/100. Took 0.51895 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.218014
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 68/100. Took 0.51609 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.218333
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 69/100. Took 0.5146 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.218635
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 70/100. Took 0.51599 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.218928
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 71/100. Took 0.51523 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.219205
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 72/100. Took 0.51818 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.219499
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 73/100. Took 0.51802 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.219780
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 74/100. Took 0.51598 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.220053
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 75/100. Took 0.51505 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.220326
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 76/100. Took 0.51625 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.220591
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 77/100. Took 0.51538 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.220862
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 78/100. Took 0.51774 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.221110
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 79/100. Took 0.51531 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.221371
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 80/100. Took 0.51708 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.221622
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 81/100. Took 0.51704 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.221864
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 82/100. Took 0.51799 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.222109
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 83/100. Took 0.51503 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.222350
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 84/100. Took 0.51383 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.222586
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 85/100. Took 0.51601 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.222824
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 86/100. Took 0.58218 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.223056
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 87/100. Took 0.52446 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.223286
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 88/100. Took 0.52142 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.223504
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 89/100. Took 0.51682 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.223731
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 90/100. Took 0.51598 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.223955
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 91/100. Took 0.51614 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.224166
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 92/100. Took 0.51594 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.224378
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 93/100. Took 0.51728 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.224599
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 94/100. Took 0.5162 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.224812
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 95/100. Took 0.51655 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.225016
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 96/100. Took 0.51641 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.225219
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 97/100. Took 0.51621 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.225427
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 98/100. Took 0.52126 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.225617
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 99/100. Took 0.51823 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.225818
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
 epoch 100/100. Took 0.51679 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.226020
  Training set accuracy = 1.000000, Test set accuracy = 0.966700
Elapsed time is 96.209735 seconds.
End Training
