Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.51143 seconds. 
  Full-batch training loss = 0.222923, test loss = 0.263597
  Training set accuracy = 0.928900, Test set accuracy = 0.915200
 epoch 2/100. Took 0.50716 seconds. 
  Full-batch training loss = 0.145611, test loss = 0.206942
  Training set accuracy = 0.956800, Test set accuracy = 0.937000
 epoch 3/100. Took 0.52018 seconds. 
  Full-batch training loss = 0.118431, test loss = 0.199060
  Training set accuracy = 0.964800, Test set accuracy = 0.941000
 epoch 4/100. Took 0.5132 seconds. 
  Full-batch training loss = 0.080781, test loss = 0.189600
  Training set accuracy = 0.973700, Test set accuracy = 0.943400
 epoch 5/100. Took 0.50569 seconds. 
  Full-batch training loss = 0.060173, test loss = 0.166531
  Training set accuracy = 0.981200, Test set accuracy = 0.952300
 epoch 6/100. Took 0.50496 seconds. 
  Full-batch training loss = 0.054999, test loss = 0.179238
  Training set accuracy = 0.981400, Test set accuracy = 0.950700
 epoch 7/100. Took 0.50495 seconds. 
  Full-batch training loss = 0.024694, test loss = 0.162087
  Training set accuracy = 0.991800, Test set accuracy = 0.958800
 epoch 8/100. Took 0.50898 seconds. 
  Full-batch training loss = 0.039484, test loss = 0.183588
  Training set accuracy = 0.988000, Test set accuracy = 0.951500
 epoch 9/100. Took 0.50966 seconds. 
  Full-batch training loss = 0.021373, test loss = 0.168262
  Training set accuracy = 0.992800, Test set accuracy = 0.956900
 epoch 10/100. Took 0.50843 seconds. 
  Full-batch training loss = 0.020601, test loss = 0.186959
  Training set accuracy = 0.994300, Test set accuracy = 0.957900
 epoch 11/100. Took 0.50837 seconds. 
  Full-batch training loss = 0.061869, test loss = 0.231483
  Training set accuracy = 0.982400, Test set accuracy = 0.946200
 epoch 12/100. Took 0.50917 seconds. 
  Full-batch training loss = 0.013631, test loss = 0.185561
  Training set accuracy = 0.996200, Test set accuracy = 0.958200
 epoch 13/100. Took 0.50708 seconds. 
  Full-batch training loss = 0.005149, test loss = 0.171009
  Training set accuracy = 0.998700, Test set accuracy = 0.966200
 epoch 14/100. Took 0.50843 seconds. 
  Full-batch training loss = 0.010360, test loss = 0.205414
  Training set accuracy = 0.996300, Test set accuracy = 0.959900
 epoch 15/100. Took 0.50699 seconds. 
  Full-batch training loss = 0.001576, test loss = 0.170378
  Training set accuracy = 0.999800, Test set accuracy = 0.967000
 epoch 16/100. Took 0.50693 seconds. 
  Full-batch training loss = 0.021763, test loss = 0.241284
  Training set accuracy = 0.993200, Test set accuracy = 0.954000
 epoch 17/100. Took 0.51056 seconds. 
  Full-batch training loss = 0.012620, test loss = 0.183937
  Training set accuracy = 0.996600, Test set accuracy = 0.961900
 epoch 18/100. Took 0.50575 seconds. 
  Full-batch training loss = 0.011313, test loss = 0.194151
  Training set accuracy = 0.996300, Test set accuracy = 0.958800
 epoch 19/100. Took 0.50725 seconds. 
  Full-batch training loss = 0.009872, test loss = 0.211594
  Training set accuracy = 0.996500, Test set accuracy = 0.962100
 epoch 20/100. Took 0.50747 seconds. 
  Full-batch training loss = 0.003137, test loss = 0.200366
  Training set accuracy = 0.999200, Test set accuracy = 0.963200
 epoch 21/100. Took 0.50612 seconds. 
  Full-batch training loss = 0.050847, test loss = 0.247095
  Training set accuracy = 0.986200, Test set accuracy = 0.952100
 epoch 22/100. Took 0.50921 seconds. 
  Full-batch training loss = 0.004366, test loss = 0.187407
  Training set accuracy = 0.998800, Test set accuracy = 0.962100
 epoch 23/100. Took 0.50691 seconds. 
  Full-batch training loss = 0.000416, test loss = 0.173368
  Training set accuracy = 0.999900, Test set accuracy = 0.968700
 epoch 24/100. Took 0.50669 seconds. 
  Full-batch training loss = 0.005452, test loss = 0.199556
  Training set accuracy = 0.998400, Test set accuracy = 0.964600
 epoch 25/100. Took 0.50899 seconds. 
  Full-batch training loss = 0.018974, test loss = 0.236880
  Training set accuracy = 0.995100, Test set accuracy = 0.957100
 epoch 26/100. Took 0.50833 seconds. 
  Full-batch training loss = 0.010479, test loss = 0.218067
  Training set accuracy = 0.996900, Test set accuracy = 0.960400
 epoch 27/100. Took 0.51017 seconds. 
  Full-batch training loss = 0.021891, test loss = 0.222908
  Training set accuracy = 0.993500, Test set accuracy = 0.956100
 epoch 28/100. Took 0.50773 seconds. 
  Full-batch training loss = 0.002247, test loss = 0.186850
  Training set accuracy = 0.999400, Test set accuracy = 0.965900
 epoch 29/100. Took 0.50935 seconds. 
  Full-batch training loss = 0.000268, test loss = 0.177816
  Training set accuracy = 1.000000, Test set accuracy = 0.967400
 epoch 30/100. Took 0.50897 seconds. 
  Full-batch training loss = 0.000134, test loss = 0.178553
  Training set accuracy = 1.000000, Test set accuracy = 0.968400
 epoch 31/100. Took 0.50903 seconds. 
  Full-batch training loss = 0.000107, test loss = 0.180095
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 32/100. Took 0.51044 seconds. 
  Full-batch training loss = 0.000090, test loss = 0.181392
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 33/100. Took 0.51007 seconds. 
  Full-batch training loss = 0.000078, test loss = 0.182558
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 34/100. Took 0.51099 seconds. 
  Full-batch training loss = 0.000069, test loss = 0.183602
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 35/100. Took 0.51062 seconds. 
  Full-batch training loss = 0.000062, test loss = 0.184659
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 36/100. Took 0.51043 seconds. 
  Full-batch training loss = 0.000056, test loss = 0.185661
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 37/100. Took 0.51273 seconds. 
  Full-batch training loss = 0.000051, test loss = 0.186475
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 38/100. Took 0.51193 seconds. 
  Full-batch training loss = 0.000047, test loss = 0.187358
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 39/100. Took 0.51138 seconds. 
  Full-batch training loss = 0.000044, test loss = 0.188190
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 40/100. Took 0.51189 seconds. 
  Full-batch training loss = 0.000041, test loss = 0.188974
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 41/100. Took 0.51149 seconds. 
  Full-batch training loss = 0.000038, test loss = 0.189648
  Training set accuracy = 1.000000, Test set accuracy = 0.970000
 epoch 42/100. Took 0.51411 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.190330
  Training set accuracy = 1.000000, Test set accuracy = 0.970000
 epoch 43/100. Took 0.51578 seconds. 
  Full-batch training loss = 0.000034, test loss = 0.190970
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 44/100. Took 0.51102 seconds. 
  Full-batch training loss = 0.000032, test loss = 0.191617
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 45/100. Took 0.51274 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.192193
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 46/100. Took 0.51221 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.192738
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 47/100. Took 0.50902 seconds. 
  Full-batch training loss = 0.000028, test loss = 0.193290
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 48/100. Took 0.51495 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.193807
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 49/100. Took 0.51233 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.194307
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 50/100. Took 0.51341 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.194783
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 51/100. Took 0.51242 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.195250
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 52/100. Took 0.51072 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.195690
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 53/100. Took 0.5118 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.196132
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 54/100. Took 0.51139 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.196566
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 55/100. Took 0.51516 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.196978
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 56/100. Took 0.51403 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.197340
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 57/100. Took 0.51502 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.197795
  Training set accuracy = 1.000000, Test set accuracy = 0.969700
 epoch 58/100. Took 0.53608 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.198170
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 59/100. Took 0.51959 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.198539
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 60/100. Took 0.52875 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.198907
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 61/100. Took 0.52087 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.199268
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 62/100. Took 0.55455 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.199612
  Training set accuracy = 1.000000, Test set accuracy = 0.970000
 epoch 63/100. Took 0.51796 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.199968
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 64/100. Took 0.51496 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.200292
  Training set accuracy = 1.000000, Test set accuracy = 0.970000
 epoch 65/100. Took 0.51455 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.200623
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 66/100. Took 0.51675 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.200955
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 67/100. Took 0.63646 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.201252
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 68/100. Took 0.51773 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.201556
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 69/100. Took 0.52071 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.201869
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 70/100. Took 0.54849 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.202186
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 71/100. Took 0.51588 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.202460
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 72/100. Took 0.51717 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.202737
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 73/100. Took 0.51846 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.203042
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 74/100. Took 0.51909 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.203290
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 75/100. Took 0.51559 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.203567
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 76/100. Took 0.51524 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.203814
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 77/100. Took 0.51638 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.204094
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 78/100. Took 0.51596 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.204338
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 79/100. Took 0.51678 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.204581
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 80/100. Took 0.51508 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.204843
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 81/100. Took 0.51533 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.205072
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 82/100. Took 0.51733 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.205312
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 83/100. Took 0.51569 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.205560
  Training set accuracy = 1.000000, Test set accuracy = 0.969900
 epoch 84/100. Took 0.51941 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.205789
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 85/100. Took 0.51635 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206009
  Training set accuracy = 1.000000, Test set accuracy = 0.969800
 epoch 86/100. Took 0.51527 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206238
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 87/100. Took 0.54198 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.206462
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 88/100. Took 0.51435 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.206677
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 89/100. Took 0.51714 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.206904
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 90/100. Took 0.51638 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207118
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 91/100. Took 0.51602 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207325
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 92/100. Took 0.51395 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207527
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 93/100. Took 0.51635 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207735
  Training set accuracy = 1.000000, Test set accuracy = 0.969600
 epoch 94/100. Took 0.51647 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.207940
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 95/100. Took 0.51386 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.208126
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 96/100. Took 0.51357 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.208330
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 97/100. Took 0.51704 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.208516
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 98/100. Took 0.51392 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.208717
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 99/100. Took 0.51532 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.208900
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
 epoch 100/100. Took 0.51548 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.209085
  Training set accuracy = 1.000000, Test set accuracy = 0.969500
Elapsed time is 96.307335 seconds.
End Training
