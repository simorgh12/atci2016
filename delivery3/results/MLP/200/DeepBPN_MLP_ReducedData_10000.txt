Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.51242 seconds. 
  Full-batch training loss = 0.235953, test loss = 0.264266
  Training set accuracy = 0.927400, Test set accuracy = 0.917500
 epoch 2/100. Took 0.51485 seconds. 
  Full-batch training loss = 0.195894, test loss = 0.264627
  Training set accuracy = 0.939600, Test set accuracy = 0.915900
 epoch 3/100. Took 0.51765 seconds. 
  Full-batch training loss = 0.217491, test loss = 0.325461
  Training set accuracy = 0.931900, Test set accuracy = 0.911300
 epoch 4/100. Took 0.51875 seconds. 
  Full-batch training loss = 0.074563, test loss = 0.189834
  Training set accuracy = 0.978800, Test set accuracy = 0.944500
 epoch 5/100. Took 0.51661 seconds. 
  Full-batch training loss = 0.061444, test loss = 0.181946
  Training set accuracy = 0.980700, Test set accuracy = 0.948500
 epoch 6/100. Took 0.51696 seconds. 
  Full-batch training loss = 0.059679, test loss = 0.195838
  Training set accuracy = 0.982800, Test set accuracy = 0.947500
 epoch 7/100. Took 0.5198 seconds. 
  Full-batch training loss = 0.035634, test loss = 0.183585
  Training set accuracy = 0.989400, Test set accuracy = 0.952200
 epoch 8/100. Took 0.51833 seconds. 
  Full-batch training loss = 0.042155, test loss = 0.187174
  Training set accuracy = 0.987500, Test set accuracy = 0.953000
 epoch 9/100. Took 0.51828 seconds. 
  Full-batch training loss = 0.028162, test loss = 0.191127
  Training set accuracy = 0.990100, Test set accuracy = 0.954000
 epoch 10/100. Took 0.51421 seconds. 
  Full-batch training loss = 0.065051, test loss = 0.270874
  Training set accuracy = 0.981300, Test set accuracy = 0.942600
 epoch 11/100. Took 0.51533 seconds. 
  Full-batch training loss = 0.046378, test loss = 0.260018
  Training set accuracy = 0.985900, Test set accuracy = 0.941700
 epoch 12/100. Took 0.51864 seconds. 
  Full-batch training loss = 0.014295, test loss = 0.188110
  Training set accuracy = 0.995900, Test set accuracy = 0.962800
 epoch 13/100. Took 0.51923 seconds. 
  Full-batch training loss = 0.017336, test loss = 0.204473
  Training set accuracy = 0.994700, Test set accuracy = 0.956600
 epoch 14/100. Took 0.51791 seconds. 
  Full-batch training loss = 0.008795, test loss = 0.200761
  Training set accuracy = 0.997900, Test set accuracy = 0.961400
 epoch 15/100. Took 0.52146 seconds. 
  Full-batch training loss = 0.008814, test loss = 0.186251
  Training set accuracy = 0.997800, Test set accuracy = 0.960400
 epoch 16/100. Took 0.53276 seconds. 
  Full-batch training loss = 0.020320, test loss = 0.230727
  Training set accuracy = 0.993200, Test set accuracy = 0.957200
 epoch 17/100. Took 0.53753 seconds. 
  Full-batch training loss = 0.021446, test loss = 0.233961
  Training set accuracy = 0.993500, Test set accuracy = 0.956700
 epoch 18/100. Took 0.51282 seconds. 
  Full-batch training loss = 0.004472, test loss = 0.205207
  Training set accuracy = 0.998400, Test set accuracy = 0.963300
 epoch 19/100. Took 0.55388 seconds. 
  Full-batch training loss = 0.000689, test loss = 0.195788
  Training set accuracy = 0.999900, Test set accuracy = 0.966500
 epoch 20/100. Took 0.59722 seconds. 
  Full-batch training loss = 0.000742, test loss = 0.187572
  Training set accuracy = 0.999800, Test set accuracy = 0.965900
 epoch 21/100. Took 0.55032 seconds. 
  Full-batch training loss = 0.000130, test loss = 0.184919
  Training set accuracy = 1.000000, Test set accuracy = 0.967900
 epoch 22/100. Took 0.5233 seconds. 
  Full-batch training loss = 0.000102, test loss = 0.185260
  Training set accuracy = 1.000000, Test set accuracy = 0.968500
 epoch 23/100. Took 0.52003 seconds. 
  Full-batch training loss = 0.000086, test loss = 0.186418
  Training set accuracy = 1.000000, Test set accuracy = 0.968500
 epoch 24/100. Took 0.51603 seconds. 
  Full-batch training loss = 0.000075, test loss = 0.187573
  Training set accuracy = 1.000000, Test set accuracy = 0.968600
 epoch 25/100. Took 0.52222 seconds. 
  Full-batch training loss = 0.000066, test loss = 0.188695
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 26/100. Took 0.54072 seconds. 
  Full-batch training loss = 0.000060, test loss = 0.189753
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 27/100. Took 0.53786 seconds. 
  Full-batch training loss = 0.000055, test loss = 0.190739
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 28/100. Took 0.53197 seconds. 
  Full-batch training loss = 0.000050, test loss = 0.191714
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 29/100. Took 0.5239 seconds. 
  Full-batch training loss = 0.000047, test loss = 0.192615
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 30/100. Took 0.53109 seconds. 
  Full-batch training loss = 0.000044, test loss = 0.193389
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 31/100. Took 0.54514 seconds. 
  Full-batch training loss = 0.000041, test loss = 0.194142
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 32/100. Took 0.54074 seconds. 
  Full-batch training loss = 0.000039, test loss = 0.194891
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 33/100. Took 0.55305 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.195609
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 34/100. Took 0.5423 seconds. 
  Full-batch training loss = 0.000035, test loss = 0.196205
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 35/100. Took 0.51696 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.196899
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 36/100. Took 0.57191 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.197466
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 37/100. Took 0.51586 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.198048
  Training set accuracy = 1.000000, Test set accuracy = 0.969400
 epoch 38/100. Took 0.51538 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.198641
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 39/100. Took 0.52376 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.199188
  Training set accuracy = 1.000000, Test set accuracy = 0.969300
 epoch 40/100. Took 0.51429 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.199691
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 41/100. Took 0.51618 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.200206
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 42/100. Took 0.52154 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.200668
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 43/100. Took 0.53669 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.201157
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 44/100. Took 0.53623 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.201635
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 45/100. Took 0.51661 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.202076
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 46/100. Took 0.51454 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.202485
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 47/100. Took 0.51629 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.202912
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 48/100. Took 0.5152 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.203310
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 49/100. Took 0.51495 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.203718
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 50/100. Took 0.51647 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.204108
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 51/100. Took 0.51631 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.204486
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 52/100. Took 0.51835 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.204849
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 53/100. Took 0.51788 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.205207
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 54/100. Took 0.51747 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.205579
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 55/100. Took 0.51869 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.205907
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 56/100. Took 0.51584 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.206256
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 57/100. Took 0.52137 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.206578
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 58/100. Took 0.51733 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.206897
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 59/100. Took 0.52105 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.207215
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 60/100. Took 0.5188 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.207529
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 61/100. Took 0.51717 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.207825
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 62/100. Took 0.51637 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.208122
  Training set accuracy = 1.000000, Test set accuracy = 0.968700
 epoch 63/100. Took 0.51811 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.208438
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 64/100. Took 0.51838 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.208740
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 65/100. Took 0.51541 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.209010
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 66/100. Took 0.51844 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.209300
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 67/100. Took 0.51774 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.209575
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 68/100. Took 0.51762 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.209847
  Training set accuracy = 1.000000, Test set accuracy = 0.968800
 epoch 69/100. Took 0.51653 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.210121
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 70/100. Took 0.51592 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.210378
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 71/100. Took 0.51821 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.210641
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 72/100. Took 0.51621 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.210896
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 73/100. Took 0.51588 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.211137
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 74/100. Took 0.51554 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.211401
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 75/100. Took 0.51792 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.211635
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 76/100. Took 0.51765 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.211874
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 77/100. Took 0.51654 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.212100
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 78/100. Took 0.51835 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.212334
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 79/100. Took 0.51601 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.212587
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 80/100. Took 0.51885 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.212795
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 81/100. Took 0.51566 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.213017
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 82/100. Took 0.51543 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.213249
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 83/100. Took 0.5155 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.213463
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 84/100. Took 0.51714 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.213680
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 85/100. Took 0.52275 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.213894
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 86/100. Took 0.51856 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.214095
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 87/100. Took 0.51594 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.214313
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 88/100. Took 0.5166 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.214514
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 89/100. Took 0.51879 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.214721
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 90/100. Took 0.51727 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.214912
  Training set accuracy = 1.000000, Test set accuracy = 0.968900
 epoch 91/100. Took 0.51785 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.215112
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 92/100. Took 0.5196 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.215309
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 93/100. Took 0.51759 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.215498
  Training set accuracy = 1.000000, Test set accuracy = 0.969000
 epoch 94/100. Took 0.5192 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.215687
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 95/100. Took 0.51957 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.215876
  Training set accuracy = 1.000000, Test set accuracy = 0.969100
 epoch 96/100. Took 0.5203 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.216065
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 97/100. Took 0.51887 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.216251
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 98/100. Took 0.52074 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.216432
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 99/100. Took 0.51776 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.216606
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
 epoch 100/100. Took 0.51958 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.216783
  Training set accuracy = 1.000000, Test set accuracy = 0.969200
Elapsed time is 97.287216 seconds.
End Training
