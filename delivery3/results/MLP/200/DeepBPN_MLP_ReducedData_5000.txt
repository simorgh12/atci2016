Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	5000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.25562 seconds. 
  Full-batch training loss = 0.371841, test loss = 0.421873
  Training set accuracy = 0.881600, Test set accuracy = 0.869800
 epoch 2/100. Took 0.25448 seconds. 
  Full-batch training loss = 0.375630, test loss = 0.482905
  Training set accuracy = 0.870600, Test set accuracy = 0.847000
 epoch 3/100. Took 0.2517 seconds. 
  Full-batch training loss = 0.193407, test loss = 0.318635
  Training set accuracy = 0.937800, Test set accuracy = 0.904600
 epoch 4/100. Took 0.25271 seconds. 
  Full-batch training loss = 0.073295, test loss = 0.226499
  Training set accuracy = 0.978200, Test set accuracy = 0.933000
 epoch 5/100. Took 0.2505 seconds. 
  Full-batch training loss = 0.051768, test loss = 0.224862
  Training set accuracy = 0.985600, Test set accuracy = 0.938900
 epoch 6/100. Took 0.25568 seconds. 
  Full-batch training loss = 0.089223, test loss = 0.295519
  Training set accuracy = 0.968400, Test set accuracy = 0.923000
 epoch 7/100. Took 0.25949 seconds. 
  Full-batch training loss = 0.043186, test loss = 0.223924
  Training set accuracy = 0.986000, Test set accuracy = 0.938600
 epoch 8/100. Took 0.25251 seconds. 
  Full-batch training loss = 0.066057, test loss = 0.280194
  Training set accuracy = 0.980400, Test set accuracy = 0.933500
 epoch 9/100. Took 0.26227 seconds. 
  Full-batch training loss = 0.010155, test loss = 0.216977
  Training set accuracy = 0.997400, Test set accuracy = 0.945500
 epoch 10/100. Took 0.25202 seconds. 
  Full-batch training loss = 0.014571, test loss = 0.249542
  Training set accuracy = 0.995200, Test set accuracy = 0.942200
 epoch 11/100. Took 0.25329 seconds. 
  Full-batch training loss = 0.014055, test loss = 0.236097
  Training set accuracy = 0.995800, Test set accuracy = 0.947000
 epoch 12/100. Took 0.25773 seconds. 
  Full-batch training loss = 0.035973, test loss = 0.333942
  Training set accuracy = 0.988600, Test set accuracy = 0.933700
 epoch 13/100. Took 0.25178 seconds. 
  Full-batch training loss = 0.026369, test loss = 0.268736
  Training set accuracy = 0.991600, Test set accuracy = 0.936800
 epoch 14/100. Took 0.25098 seconds. 
  Full-batch training loss = 0.004388, test loss = 0.237808
  Training set accuracy = 0.999400, Test set accuracy = 0.950500
 epoch 15/100. Took 0.2576 seconds. 
  Full-batch training loss = 0.001458, test loss = 0.235212
  Training set accuracy = 0.999800, Test set accuracy = 0.953000
 epoch 16/100. Took 0.25588 seconds. 
  Full-batch training loss = 0.020119, test loss = 0.283297
  Training set accuracy = 0.995200, Test set accuracy = 0.944900
 epoch 17/100. Took 0.25324 seconds. 
  Full-batch training loss = 0.009643, test loss = 0.263076
  Training set accuracy = 0.997600, Test set accuracy = 0.950300
 epoch 18/100. Took 0.25385 seconds. 
  Full-batch training loss = 0.013000, test loss = 0.249169
  Training set accuracy = 0.995000, Test set accuracy = 0.950400
 epoch 19/100. Took 0.2517 seconds. 
  Full-batch training loss = 0.002244, test loss = 0.268378
  Training set accuracy = 0.999200, Test set accuracy = 0.949100
 epoch 20/100. Took 0.25338 seconds. 
  Full-batch training loss = 0.001201, test loss = 0.264406
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 21/100. Took 0.25225 seconds. 
  Full-batch training loss = 0.000290, test loss = 0.253311
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 22/100. Took 0.25023 seconds. 
  Full-batch training loss = 0.000218, test loss = 0.255990
  Training set accuracy = 1.000000, Test set accuracy = 0.954200
 epoch 23/100. Took 0.25253 seconds. 
  Full-batch training loss = 0.000181, test loss = 0.258241
  Training set accuracy = 1.000000, Test set accuracy = 0.954200
 epoch 24/100. Took 0.25684 seconds. 
  Full-batch training loss = 0.000156, test loss = 0.260252
  Training set accuracy = 1.000000, Test set accuracy = 0.954300
 epoch 25/100. Took 0.25671 seconds. 
  Full-batch training loss = 0.000138, test loss = 0.261925
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 26/100. Took 0.25724 seconds. 
  Full-batch training loss = 0.000123, test loss = 0.263911
  Training set accuracy = 1.000000, Test set accuracy = 0.954600
 epoch 27/100. Took 0.25343 seconds. 
  Full-batch training loss = 0.000111, test loss = 0.265248
  Training set accuracy = 1.000000, Test set accuracy = 0.954700
 epoch 28/100. Took 0.26086 seconds. 
  Full-batch training loss = 0.000102, test loss = 0.267050
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 29/100. Took 0.26502 seconds. 
  Full-batch training loss = 0.000094, test loss = 0.268334
  Training set accuracy = 1.000000, Test set accuracy = 0.955000
 epoch 30/100. Took 0.25659 seconds. 
  Full-batch training loss = 0.000088, test loss = 0.269584
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 31/100. Took 0.25417 seconds. 
  Full-batch training loss = 0.000082, test loss = 0.270849
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 32/100. Took 0.25476 seconds. 
  Full-batch training loss = 0.000077, test loss = 0.271996
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 33/100. Took 0.25484 seconds. 
  Full-batch training loss = 0.000073, test loss = 0.273201
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 34/100. Took 0.26477 seconds. 
  Full-batch training loss = 0.000069, test loss = 0.274306
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 35/100. Took 0.25411 seconds. 
  Full-batch training loss = 0.000065, test loss = 0.275285
  Training set accuracy = 1.000000, Test set accuracy = 0.955100
 epoch 36/100. Took 0.25238 seconds. 
  Full-batch training loss = 0.000062, test loss = 0.276212
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 37/100. Took 0.26505 seconds. 
  Full-batch training loss = 0.000059, test loss = 0.277191
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 38/100. Took 0.25595 seconds. 
  Full-batch training loss = 0.000056, test loss = 0.278199
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 39/100. Took 0.25407 seconds. 
  Full-batch training loss = 0.000054, test loss = 0.278996
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 40/100. Took 0.26572 seconds. 
  Full-batch training loss = 0.000052, test loss = 0.279820
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 41/100. Took 0.25246 seconds. 
  Full-batch training loss = 0.000050, test loss = 0.280608
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 42/100. Took 0.25953 seconds. 
  Full-batch training loss = 0.000048, test loss = 0.281393
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 43/100. Took 0.25077 seconds. 
  Full-batch training loss = 0.000046, test loss = 0.282195
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 44/100. Took 0.25453 seconds. 
  Full-batch training loss = 0.000045, test loss = 0.282916
  Training set accuracy = 1.000000, Test set accuracy = 0.955200
 epoch 45/100. Took 0.25456 seconds. 
  Full-batch training loss = 0.000043, test loss = 0.283683
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 46/100. Took 0.2547 seconds. 
  Full-batch training loss = 0.000042, test loss = 0.284390
  Training set accuracy = 1.000000, Test set accuracy = 0.955300
 epoch 47/100. Took 0.25244 seconds. 
  Full-batch training loss = 0.000040, test loss = 0.285087
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 48/100. Took 0.25759 seconds. 
  Full-batch training loss = 0.000039, test loss = 0.285719
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 49/100. Took 0.2598 seconds. 
  Full-batch training loss = 0.000038, test loss = 0.286366
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 50/100. Took 0.25447 seconds. 
  Full-batch training loss = 0.000037, test loss = 0.286953
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 51/100. Took 0.25403 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.287569
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 52/100. Took 0.25469 seconds. 
  Full-batch training loss = 0.000035, test loss = 0.288179
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 53/100. Took 0.25156 seconds. 
  Full-batch training loss = 0.000034, test loss = 0.288743
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 54/100. Took 0.25253 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.289291
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 55/100. Took 0.25698 seconds. 
  Full-batch training loss = 0.000032, test loss = 0.289856
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 56/100. Took 0.25484 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.290429
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 57/100. Took 0.25582 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.290898
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 58/100. Took 0.25167 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.291417
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 59/100. Took 0.26068 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.291933
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 60/100. Took 0.25705 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.292423
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 61/100. Took 0.26115 seconds. 
  Full-batch training loss = 0.000028, test loss = 0.292879
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 62/100. Took 0.26564 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.293342
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 63/100. Took 0.25852 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.293799
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 64/100. Took 0.26928 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.294260
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 65/100. Took 0.2554 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.294688
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 66/100. Took 0.25491 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.295158
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 67/100. Took 0.25685 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.295566
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 68/100. Took 0.25733 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.295989
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 69/100. Took 0.26013 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.296400
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 70/100. Took 0.25731 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.296810
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 71/100. Took 0.26018 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.297233
  Training set accuracy = 1.000000, Test set accuracy = 0.955900
 epoch 72/100. Took 0.25699 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.297615
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 73/100. Took 0.25703 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.298006
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 74/100. Took 0.25907 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.298383
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 75/100. Took 0.25634 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.298772
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 76/100. Took 0.25924 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.299138
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 77/100. Took 0.2564 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.299515
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 78/100. Took 0.25711 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.299865
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 79/100. Took 0.2557 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.300208
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 80/100. Took 0.25795 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.300555
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 81/100. Took 0.26052 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.300922
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 82/100. Took 0.25962 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.301277
  Training set accuracy = 1.000000, Test set accuracy = 0.955800
 epoch 83/100. Took 0.25807 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.301610
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 84/100. Took 0.26081 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.301957
  Training set accuracy = 1.000000, Test set accuracy = 0.955700
 epoch 85/100. Took 0.26105 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.302314
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 86/100. Took 0.26232 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.302606
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 87/100. Took 0.26119 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.302936
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 88/100. Took 0.26185 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.303283
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 89/100. Took 0.26099 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.303587
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 90/100. Took 0.25992 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.303916
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 91/100. Took 0.25964 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.304195
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 92/100. Took 0.25546 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.304517
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 93/100. Took 0.25691 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.304832
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 94/100. Took 0.25501 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.305137
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 95/100. Took 0.25495 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.305427
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 96/100. Took 0.25729 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.305726
  Training set accuracy = 1.000000, Test set accuracy = 0.955600
 epoch 97/100. Took 0.26209 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.306014
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 98/100. Took 0.26251 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.306302
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
 epoch 99/100. Took 0.26782 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.306594
  Training set accuracy = 1.000000, Test set accuracy = 0.955500
 epoch 100/100. Took 0.25549 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.306868
  Training set accuracy = 1.000000, Test set accuracy = 0.955400
Elapsed time is 59.525606 seconds.
End Training
