Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	5000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.26775 seconds. 
  Full-batch training loss = 0.513088, test loss = 0.526878
  Training set accuracy = 0.855000, Test set accuracy = 0.848000
 epoch 2/100. Took 0.26159 seconds. 
  Full-batch training loss = 0.230937, test loss = 0.297723
  Training set accuracy = 0.932400, Test set accuracy = 0.913800
 epoch 3/100. Took 0.26651 seconds. 
  Full-batch training loss = 0.201702, test loss = 0.285055
  Training set accuracy = 0.939200, Test set accuracy = 0.913500
 epoch 4/100. Took 0.26671 seconds. 
  Full-batch training loss = 0.141052, test loss = 0.273995
  Training set accuracy = 0.955000, Test set accuracy = 0.916800
 epoch 5/100. Took 0.26441 seconds. 
  Full-batch training loss = 0.105678, test loss = 0.254118
  Training set accuracy = 0.966800, Test set accuracy = 0.928000
 epoch 6/100. Took 0.26311 seconds. 
  Full-batch training loss = 0.110971, test loss = 0.305582
  Training set accuracy = 0.966000, Test set accuracy = 0.918800
 epoch 7/100. Took 0.26485 seconds. 
  Full-batch training loss = 0.085074, test loss = 0.273921
  Training set accuracy = 0.973200, Test set accuracy = 0.928300
 epoch 8/100. Took 0.26566 seconds. 
  Full-batch training loss = 0.050051, test loss = 0.242199
  Training set accuracy = 0.984600, Test set accuracy = 0.937700
 epoch 9/100. Took 0.26573 seconds. 
  Full-batch training loss = 0.050618, test loss = 0.292774
  Training set accuracy = 0.983400, Test set accuracy = 0.934900
 epoch 10/100. Took 0.25925 seconds. 
  Full-batch training loss = 0.029185, test loss = 0.273181
  Training set accuracy = 0.989600, Test set accuracy = 0.937300
 epoch 11/100. Took 0.26485 seconds. 
  Full-batch training loss = 0.022470, test loss = 0.250888
  Training set accuracy = 0.993200, Test set accuracy = 0.942000
 epoch 12/100. Took 0.26133 seconds. 
  Full-batch training loss = 0.022694, test loss = 0.309489
  Training set accuracy = 0.992600, Test set accuracy = 0.939600
 epoch 13/100. Took 0.26515 seconds. 
  Full-batch training loss = 0.010058, test loss = 0.275910
  Training set accuracy = 0.997000, Test set accuracy = 0.942800
 epoch 14/100. Took 0.26301 seconds. 
  Full-batch training loss = 0.008381, test loss = 0.259173
  Training set accuracy = 0.998000, Test set accuracy = 0.946400
 epoch 15/100. Took 0.2591 seconds. 
  Full-batch training loss = 0.004305, test loss = 0.257755
  Training set accuracy = 0.998600, Test set accuracy = 0.950700
 epoch 16/100. Took 0.26459 seconds. 
  Full-batch training loss = 0.000970, test loss = 0.256427
  Training set accuracy = 1.000000, Test set accuracy = 0.951400
 epoch 17/100. Took 0.26195 seconds. 
  Full-batch training loss = 0.000421, test loss = 0.251861
  Training set accuracy = 1.000000, Test set accuracy = 0.952400
 epoch 18/100. Took 0.26073 seconds. 
  Full-batch training loss = 0.000321, test loss = 0.254042
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 19/100. Took 0.26343 seconds. 
  Full-batch training loss = 0.000265, test loss = 0.256460
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 20/100. Took 0.26288 seconds. 
  Full-batch training loss = 0.000227, test loss = 0.258741
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 21/100. Took 0.26373 seconds. 
  Full-batch training loss = 0.000198, test loss = 0.260884
  Training set accuracy = 1.000000, Test set accuracy = 0.953400
 epoch 22/100. Took 0.26558 seconds. 
  Full-batch training loss = 0.000177, test loss = 0.262994
  Training set accuracy = 1.000000, Test set accuracy = 0.953400
 epoch 23/100. Took 0.26281 seconds. 
  Full-batch training loss = 0.000159, test loss = 0.264913
  Training set accuracy = 1.000000, Test set accuracy = 0.953200
 epoch 24/100. Took 0.2615 seconds. 
  Full-batch training loss = 0.000145, test loss = 0.266509
  Training set accuracy = 1.000000, Test set accuracy = 0.953300
 epoch 25/100. Took 0.26405 seconds. 
  Full-batch training loss = 0.000133, test loss = 0.268016
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 26/100. Took 0.2695 seconds. 
  Full-batch training loss = 0.000123, test loss = 0.269631
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 27/100. Took 0.25863 seconds. 
  Full-batch training loss = 0.000114, test loss = 0.270934
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 28/100. Took 0.26057 seconds. 
  Full-batch training loss = 0.000107, test loss = 0.272282
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 29/100. Took 0.26109 seconds. 
  Full-batch training loss = 0.000100, test loss = 0.273523
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 30/100. Took 0.26398 seconds. 
  Full-batch training loss = 0.000094, test loss = 0.274821
  Training set accuracy = 1.000000, Test set accuracy = 0.953300
 epoch 31/100. Took 0.26389 seconds. 
  Full-batch training loss = 0.000089, test loss = 0.275990
  Training set accuracy = 1.000000, Test set accuracy = 0.953300
 epoch 32/100. Took 0.25795 seconds. 
  Full-batch training loss = 0.000084, test loss = 0.277193
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 33/100. Took 0.26732 seconds. 
  Full-batch training loss = 0.000080, test loss = 0.278075
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 34/100. Took 0.26447 seconds. 
  Full-batch training loss = 0.000076, test loss = 0.279148
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 35/100. Took 0.26588 seconds. 
  Full-batch training loss = 0.000073, test loss = 0.280093
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 36/100. Took 0.26423 seconds. 
  Full-batch training loss = 0.000070, test loss = 0.281030
  Training set accuracy = 1.000000, Test set accuracy = 0.953400
 epoch 37/100. Took 0.26718 seconds. 
  Full-batch training loss = 0.000067, test loss = 0.281966
  Training set accuracy = 1.000000, Test set accuracy = 0.953400
 epoch 38/100. Took 0.26997 seconds. 
  Full-batch training loss = 0.000064, test loss = 0.282783
  Training set accuracy = 1.000000, Test set accuracy = 0.953300
 epoch 39/100. Took 0.26458 seconds. 
  Full-batch training loss = 0.000062, test loss = 0.283656
  Training set accuracy = 1.000000, Test set accuracy = 0.953400
 epoch 40/100. Took 0.26121 seconds. 
  Full-batch training loss = 0.000059, test loss = 0.284467
  Training set accuracy = 1.000000, Test set accuracy = 0.953400
 epoch 41/100. Took 0.26572 seconds. 
  Full-batch training loss = 0.000057, test loss = 0.285272
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 42/100. Took 0.26005 seconds. 
  Full-batch training loss = 0.000055, test loss = 0.286040
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 43/100. Took 0.26099 seconds. 
  Full-batch training loss = 0.000053, test loss = 0.286774
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 44/100. Took 0.26313 seconds. 
  Full-batch training loss = 0.000052, test loss = 0.287509
  Training set accuracy = 1.000000, Test set accuracy = 0.953500
 epoch 45/100. Took 0.25586 seconds. 
  Full-batch training loss = 0.000050, test loss = 0.288231
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 46/100. Took 0.26057 seconds. 
  Full-batch training loss = 0.000048, test loss = 0.288912
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 47/100. Took 0.25654 seconds. 
  Full-batch training loss = 0.000047, test loss = 0.289649
  Training set accuracy = 1.000000, Test set accuracy = 0.953600
 epoch 48/100. Took 0.26145 seconds. 
  Full-batch training loss = 0.000046, test loss = 0.290267
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 49/100. Took 0.26439 seconds. 
  Full-batch training loss = 0.000044, test loss = 0.290909
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 50/100. Took 0.26337 seconds. 
  Full-batch training loss = 0.000043, test loss = 0.291568
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 51/100. Took 0.25965 seconds. 
  Full-batch training loss = 0.000042, test loss = 0.292133
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 52/100. Took 0.26305 seconds. 
  Full-batch training loss = 0.000041, test loss = 0.292783
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 53/100. Took 0.26637 seconds. 
  Full-batch training loss = 0.000040, test loss = 0.293351
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 54/100. Took 0.26368 seconds. 
  Full-batch training loss = 0.000039, test loss = 0.293916
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 55/100. Took 0.26167 seconds. 
  Full-batch training loss = 0.000038, test loss = 0.294459
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 56/100. Took 0.26373 seconds. 
  Full-batch training loss = 0.000037, test loss = 0.294990
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 57/100. Took 0.26408 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.295552
  Training set accuracy = 1.000000, Test set accuracy = 0.953700
 epoch 58/100. Took 0.2604 seconds. 
  Full-batch training loss = 0.000035, test loss = 0.296033
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 59/100. Took 0.26185 seconds. 
  Full-batch training loss = 0.000034, test loss = 0.296523
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 60/100. Took 0.25908 seconds. 
  Full-batch training loss = 0.000034, test loss = 0.297017
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 61/100. Took 0.25948 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.297497
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 62/100. Took 0.26163 seconds. 
  Full-batch training loss = 0.000032, test loss = 0.297966
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 63/100. Took 0.26196 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.298412
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 64/100. Took 0.28327 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.298893
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 65/100. Took 0.2903 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.299333
  Training set accuracy = 1.000000, Test set accuracy = 0.954100
 epoch 66/100. Took 0.28802 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.299746
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 67/100. Took 0.29179 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.300180
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 68/100. Took 0.28659 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.300611
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 69/100. Took 0.28797 seconds. 
  Full-batch training loss = 0.000028, test loss = 0.301050
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 70/100. Took 0.282 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.301437
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 71/100. Took 0.28755 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.301811
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 72/100. Took 0.29564 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.302249
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 73/100. Took 0.28366 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.302611
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 74/100. Took 0.28774 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.303030
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 75/100. Took 0.28643 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.303427
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 76/100. Took 0.29118 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.303779
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 77/100. Took 0.2914 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.304141
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 78/100. Took 0.29414 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.304530
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 79/100. Took 0.29082 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.304897
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 80/100. Took 0.2812 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.305215
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 81/100. Took 0.28143 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.305596
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 82/100. Took 0.2902 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.305869
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 83/100. Took 0.28917 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.306281
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 84/100. Took 0.28425 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.306626
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 85/100. Took 0.28671 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.306935
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 86/100. Took 0.28479 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.307263
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 87/100. Took 0.29103 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.307591
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 88/100. Took 0.2831 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.307938
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 89/100. Took 0.28581 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.308266
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 90/100. Took 0.28972 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.308523
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 91/100. Took 0.28859 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.308849
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 92/100. Took 0.28681 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.309180
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 93/100. Took 0.28597 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.309512
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 94/100. Took 0.28586 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.309787
  Training set accuracy = 1.000000, Test set accuracy = 0.954000
 epoch 95/100. Took 0.28831 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.310029
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 96/100. Took 0.28613 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.310374
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 97/100. Took 0.28651 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.310683
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 98/100. Took 0.29082 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.310956
  Training set accuracy = 1.000000, Test set accuracy = 0.953900
 epoch 99/100. Took 0.28682 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.311216
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
 epoch 100/100. Took 0.29143 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.311494
  Training set accuracy = 1.000000, Test set accuracy = 0.953800
Elapsed time is 60.913530 seconds.
End Training
