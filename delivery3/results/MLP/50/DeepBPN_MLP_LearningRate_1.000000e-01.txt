Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.10
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.51368 seconds. 
  Full-batch training loss = 0.244022, test loss = 0.260596
  Training set accuracy = 0.928500, Test set accuracy = 0.922100
 epoch 2/100. Took 0.51722 seconds. 
  Full-batch training loss = 0.173957, test loss = 0.231164
  Training set accuracy = 0.940900, Test set accuracy = 0.924700
 epoch 3/100. Took 0.50839 seconds. 
  Full-batch training loss = 0.099893, test loss = 0.178613
  Training set accuracy = 0.968600, Test set accuracy = 0.946100
 epoch 4/100. Took 0.51223 seconds. 
  Full-batch training loss = 0.088247, test loss = 0.183729
  Training set accuracy = 0.972000, Test set accuracy = 0.943600
 epoch 5/100. Took 0.51065 seconds. 
  Full-batch training loss = 0.056917, test loss = 0.174656
  Training set accuracy = 0.982600, Test set accuracy = 0.950300
 epoch 6/100. Took 0.51205 seconds. 
  Full-batch training loss = 0.046034, test loss = 0.171954
  Training set accuracy = 0.985100, Test set accuracy = 0.953000
 epoch 7/100. Took 0.5113 seconds. 
  Full-batch training loss = 0.090122, test loss = 0.241200
  Training set accuracy = 0.970100, Test set accuracy = 0.935800
 epoch 8/100. Took 0.50789 seconds. 
  Full-batch training loss = 0.041106, test loss = 0.188822
  Training set accuracy = 0.985500, Test set accuracy = 0.951800
 epoch 9/100. Took 0.52252 seconds. 
  Full-batch training loss = 0.015522, test loss = 0.169050
  Training set accuracy = 0.995000, Test set accuracy = 0.959500
 epoch 10/100. Took 0.50793 seconds. 
  Full-batch training loss = 0.033800, test loss = 0.223367
  Training set accuracy = 0.988200, Test set accuracy = 0.950000
 epoch 11/100. Took 0.50869 seconds. 
  Full-batch training loss = 0.012371, test loss = 0.173820
  Training set accuracy = 0.996700, Test set accuracy = 0.958500
 epoch 12/100. Took 0.51127 seconds. 
  Full-batch training loss = 0.007064, test loss = 0.173282
  Training set accuracy = 0.998500, Test set accuracy = 0.960200
 epoch 13/100. Took 0.51033 seconds. 
  Full-batch training loss = 0.005890, test loss = 0.180929
  Training set accuracy = 0.998500, Test set accuracy = 0.960100
 epoch 14/100. Took 0.51072 seconds. 
  Full-batch training loss = 0.001496, test loss = 0.169766
  Training set accuracy = 0.999800, Test set accuracy = 0.965100
 epoch 15/100. Took 0.51236 seconds. 
  Full-batch training loss = 0.000553, test loss = 0.163129
  Training set accuracy = 1.000000, Test set accuracy = 0.965700
 epoch 16/100. Took 0.51403 seconds. 
  Full-batch training loss = 0.000397, test loss = 0.165936
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 17/100. Took 0.51441 seconds. 
  Full-batch training loss = 0.000327, test loss = 0.167918
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 18/100. Took 0.51238 seconds. 
  Full-batch training loss = 0.000281, test loss = 0.170532
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 19/100. Took 0.5194 seconds. 
  Full-batch training loss = 0.000249, test loss = 0.172686
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 20/100. Took 0.51429 seconds. 
  Full-batch training loss = 0.000228, test loss = 0.173966
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 21/100. Took 0.54979 seconds. 
  Full-batch training loss = 0.000206, test loss = 0.175652
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 22/100. Took 0.51042 seconds. 
  Full-batch training loss = 0.000190, test loss = 0.176756
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 23/100. Took 0.51293 seconds. 
  Full-batch training loss = 0.000176, test loss = 0.178135
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 24/100. Took 0.5087 seconds. 
  Full-batch training loss = 0.000164, test loss = 0.179082
  Training set accuracy = 1.000000, Test set accuracy = 0.966900
 epoch 25/100. Took 0.56772 seconds. 
  Full-batch training loss = 0.000154, test loss = 0.180335
  Training set accuracy = 1.000000, Test set accuracy = 0.967200
 epoch 26/100. Took 0.51344 seconds. 
  Full-batch training loss = 0.000145, test loss = 0.181144
  Training set accuracy = 1.000000, Test set accuracy = 0.967100
 epoch 27/100. Took 0.51278 seconds. 
  Full-batch training loss = 0.000137, test loss = 0.182206
  Training set accuracy = 1.000000, Test set accuracy = 0.967100
 epoch 28/100. Took 0.51394 seconds. 
  Full-batch training loss = 0.000130, test loss = 0.182821
  Training set accuracy = 1.000000, Test set accuracy = 0.966800
 epoch 29/100. Took 0.53211 seconds. 
  Full-batch training loss = 0.000124, test loss = 0.183786
  Training set accuracy = 1.000000, Test set accuracy = 0.967100
 epoch 30/100. Took 0.54446 seconds. 
  Full-batch training loss = 0.000118, test loss = 0.184593
  Training set accuracy = 1.000000, Test set accuracy = 0.967100
 epoch 31/100. Took 0.51796 seconds. 
  Full-batch training loss = 0.000113, test loss = 0.185095
  Training set accuracy = 1.000000, Test set accuracy = 0.967100
 epoch 32/100. Took 0.52984 seconds. 
  Full-batch training loss = 0.000108, test loss = 0.185865
  Training set accuracy = 1.000000, Test set accuracy = 0.967200
 epoch 33/100. Took 0.5139 seconds. 
  Full-batch training loss = 0.000104, test loss = 0.186599
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 34/100. Took 0.50777 seconds. 
  Full-batch training loss = 0.000100, test loss = 0.187211
  Training set accuracy = 1.000000, Test set accuracy = 0.967300
 epoch 35/100. Took 0.50965 seconds. 
  Full-batch training loss = 0.000096, test loss = 0.187649
  Training set accuracy = 1.000000, Test set accuracy = 0.967000
 epoch 36/100. Took 0.51147 seconds. 
  Full-batch training loss = 0.000093, test loss = 0.188302
  Training set accuracy = 1.000000, Test set accuracy = 0.967200
 epoch 37/100. Took 0.50876 seconds. 
  Full-batch training loss = 0.000089, test loss = 0.188951
  Training set accuracy = 1.000000, Test set accuracy = 0.967300
 epoch 38/100. Took 0.54535 seconds. 
  Full-batch training loss = 0.000087, test loss = 0.189418
  Training set accuracy = 1.000000, Test set accuracy = 0.967400
 epoch 39/100. Took 0.54185 seconds. 
  Full-batch training loss = 0.000084, test loss = 0.189972
  Training set accuracy = 1.000000, Test set accuracy = 0.967300
 epoch 40/100. Took 0.53262 seconds. 
  Full-batch training loss = 0.000081, test loss = 0.190484
  Training set accuracy = 1.000000, Test set accuracy = 0.967400
 epoch 41/100. Took 0.51643 seconds. 
  Full-batch training loss = 0.000079, test loss = 0.191031
  Training set accuracy = 1.000000, Test set accuracy = 0.967400
 epoch 42/100. Took 0.61094 seconds. 
  Full-batch training loss = 0.000076, test loss = 0.191533
  Training set accuracy = 1.000000, Test set accuracy = 0.967300
 epoch 43/100. Took 0.55419 seconds. 
  Full-batch training loss = 0.000074, test loss = 0.192004
  Training set accuracy = 1.000000, Test set accuracy = 0.967400
 epoch 44/100. Took 0.51267 seconds. 
  Full-batch training loss = 0.000072, test loss = 0.192437
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 45/100. Took 0.59674 seconds. 
  Full-batch training loss = 0.000070, test loss = 0.192851
  Training set accuracy = 1.000000, Test set accuracy = 0.967500
 epoch 46/100. Took 0.58771 seconds. 
  Full-batch training loss = 0.000069, test loss = 0.193344
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 47/100. Took 0.50781 seconds. 
  Full-batch training loss = 0.000067, test loss = 0.193601
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 48/100. Took 0.51873 seconds. 
  Full-batch training loss = 0.000065, test loss = 0.194051
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 49/100. Took 0.52153 seconds. 
  Full-batch training loss = 0.000064, test loss = 0.194466
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 50/100. Took 0.55502 seconds. 
  Full-batch training loss = 0.000062, test loss = 0.194890
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 51/100. Took 0.53629 seconds. 
  Full-batch training loss = 0.000061, test loss = 0.195235
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 52/100. Took 0.51175 seconds. 
  Full-batch training loss = 0.000059, test loss = 0.195669
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 53/100. Took 0.51489 seconds. 
  Full-batch training loss = 0.000058, test loss = 0.195949
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 54/100. Took 0.54409 seconds. 
  Full-batch training loss = 0.000057, test loss = 0.196327
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 55/100. Took 0.522 seconds. 
  Full-batch training loss = 0.000056, test loss = 0.196699
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 56/100. Took 0.53536 seconds. 
  Full-batch training loss = 0.000054, test loss = 0.197084
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 57/100. Took 0.53834 seconds. 
  Full-batch training loss = 0.000053, test loss = 0.197418
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 58/100. Took 0.51757 seconds. 
  Full-batch training loss = 0.000052, test loss = 0.197726
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 59/100. Took 0.51432 seconds. 
  Full-batch training loss = 0.000051, test loss = 0.198067
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 60/100. Took 0.54048 seconds. 
  Full-batch training loss = 0.000050, test loss = 0.198412
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 61/100. Took 0.51474 seconds. 
  Full-batch training loss = 0.000049, test loss = 0.198764
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 62/100. Took 0.51687 seconds. 
  Full-batch training loss = 0.000048, test loss = 0.199078
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 63/100. Took 0.53167 seconds. 
  Full-batch training loss = 0.000047, test loss = 0.199401
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 64/100. Took 0.52082 seconds. 
  Full-batch training loss = 0.000047, test loss = 0.199646
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 65/100. Took 0.51458 seconds. 
  Full-batch training loss = 0.000046, test loss = 0.199919
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 66/100. Took 0.52461 seconds. 
  Full-batch training loss = 0.000045, test loss = 0.200206
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 67/100. Took 0.53304 seconds. 
  Full-batch training loss = 0.000044, test loss = 0.200476
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 68/100. Took 0.53133 seconds. 
  Full-batch training loss = 0.000044, test loss = 0.200806
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 69/100. Took 0.53623 seconds. 
  Full-batch training loss = 0.000043, test loss = 0.201115
  Training set accuracy = 1.000000, Test set accuracy = 0.967900
 epoch 70/100. Took 0.52323 seconds. 
  Full-batch training loss = 0.000042, test loss = 0.201366
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 71/100. Took 0.51708 seconds. 
  Full-batch training loss = 0.000041, test loss = 0.201619
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 72/100. Took 0.51161 seconds. 
  Full-batch training loss = 0.000041, test loss = 0.201848
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 73/100. Took 0.52078 seconds. 
  Full-batch training loss = 0.000040, test loss = 0.202193
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 74/100. Took 0.5071 seconds. 
  Full-batch training loss = 0.000040, test loss = 0.202418
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 75/100. Took 0.51649 seconds. 
  Full-batch training loss = 0.000039, test loss = 0.202640
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 76/100. Took 0.52402 seconds. 
  Full-batch training loss = 0.000038, test loss = 0.202879
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 77/100. Took 0.54768 seconds. 
  Full-batch training loss = 0.000038, test loss = 0.203175
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 78/100. Took 0.54515 seconds. 
  Full-batch training loss = 0.000037, test loss = 0.203444
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 79/100. Took 0.54405 seconds. 
  Full-batch training loss = 0.000037, test loss = 0.203699
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 80/100. Took 0.56273 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.203855
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 81/100. Took 0.51292 seconds. 
  Full-batch training loss = 0.000036, test loss = 0.204131
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 82/100. Took 0.53892 seconds. 
  Full-batch training loss = 0.000035, test loss = 0.204390
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 83/100. Took 0.51379 seconds. 
  Full-batch training loss = 0.000035, test loss = 0.204582
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 84/100. Took 0.52457 seconds. 
  Full-batch training loss = 0.000034, test loss = 0.204739
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 85/100. Took 0.53747 seconds. 
  Full-batch training loss = 0.000034, test loss = 0.205001
  Training set accuracy = 1.000000, Test set accuracy = 0.967900
 epoch 86/100. Took 0.52716 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.205267
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 87/100. Took 0.53177 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.205455
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 88/100. Took 0.5119 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.205668
  Training set accuracy = 1.000000, Test set accuracy = 0.967800
 epoch 89/100. Took 0.51303 seconds. 
  Full-batch training loss = 0.000032, test loss = 0.205859
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 90/100. Took 0.55264 seconds. 
  Full-batch training loss = 0.000032, test loss = 0.206095
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 91/100. Took 0.51777 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.206264
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 92/100. Took 0.51588 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.206438
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 93/100. Took 0.58376 seconds. 
  Full-batch training loss = 0.000031, test loss = 0.206686
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 94/100. Took 0.5188 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.206829
  Training set accuracy = 1.000000, Test set accuracy = 0.967600
 epoch 95/100. Took 0.50921 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.207084
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 96/100. Took 0.65916 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.207275
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 97/100. Took 0.52518 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.207487
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 98/100. Took 0.52815 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.207662
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 99/100. Took 0.52965 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.207882
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
 epoch 100/100. Took 0.51452 seconds. 
  Full-batch training loss = 0.000028, test loss = 0.208079
  Training set accuracy = 1.000000, Test set accuracy = 0.967700
Elapsed time is 98.353446 seconds.
End Training
