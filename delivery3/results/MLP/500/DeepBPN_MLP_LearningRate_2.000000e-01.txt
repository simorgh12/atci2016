Loading data set: mnist_uint8.mat
Setting parameters
==========================================================
* ReducedData	10000
* Training Parameters:
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (200  200  200)
Training NN  (784  200  200  200   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.50869 seconds. 
  Full-batch training loss = 0.226036, test loss = 0.258044
  Training set accuracy = 0.933900, Test set accuracy = 0.922900
 epoch 2/100. Took 0.50589 seconds. 
  Full-batch training loss = 0.148497, test loss = 0.228290
  Training set accuracy = 0.953000, Test set accuracy = 0.929800
 epoch 3/100. Took 0.50835 seconds. 
  Full-batch training loss = 0.145140, test loss = 0.234850
  Training set accuracy = 0.952600, Test set accuracy = 0.930700
 epoch 4/100. Took 0.50935 seconds. 
  Full-batch training loss = 0.082486, test loss = 0.180011
  Training set accuracy = 0.975900, Test set accuracy = 0.947300
 epoch 5/100. Took 0.50739 seconds. 
  Full-batch training loss = 0.046730, test loss = 0.176942
  Training set accuracy = 0.985300, Test set accuracy = 0.952300
 epoch 6/100. Took 0.50931 seconds. 
  Full-batch training loss = 0.152943, test loss = 0.315731
  Training set accuracy = 0.954200, Test set accuracy = 0.920700
 epoch 7/100. Took 0.50932 seconds. 
  Full-batch training loss = 0.061697, test loss = 0.224807
  Training set accuracy = 0.977700, Test set accuracy = 0.943600
 epoch 8/100. Took 0.50828 seconds. 
  Full-batch training loss = 0.019797, test loss = 0.188985
  Training set accuracy = 0.993000, Test set accuracy = 0.955300
 epoch 9/100. Took 0.50718 seconds. 
  Full-batch training loss = 0.037029, test loss = 0.237240
  Training set accuracy = 0.988100, Test set accuracy = 0.948500
 epoch 10/100. Took 0.50794 seconds. 
  Full-batch training loss = 0.014687, test loss = 0.184097
  Training set accuracy = 0.995400, Test set accuracy = 0.959000
 epoch 11/100. Took 0.50822 seconds. 
  Full-batch training loss = 0.024215, test loss = 0.231184
  Training set accuracy = 0.991000, Test set accuracy = 0.951500
 epoch 12/100. Took 0.50625 seconds. 
  Full-batch training loss = 0.012426, test loss = 0.194595
  Training set accuracy = 0.996500, Test set accuracy = 0.956500
 epoch 13/100. Took 0.50835 seconds. 
  Full-batch training loss = 0.028296, test loss = 0.220801
  Training set accuracy = 0.991000, Test set accuracy = 0.955500
 epoch 14/100. Took 0.50513 seconds. 
  Full-batch training loss = 0.005104, test loss = 0.212378
  Training set accuracy = 0.998600, Test set accuracy = 0.960300
 epoch 15/100. Took 0.50906 seconds. 
  Full-batch training loss = 0.014395, test loss = 0.226777
  Training set accuracy = 0.995700, Test set accuracy = 0.957100
 epoch 16/100. Took 0.50676 seconds. 
  Full-batch training loss = 0.009533, test loss = 0.209717
  Training set accuracy = 0.996700, Test set accuracy = 0.957600
 epoch 17/100. Took 0.50486 seconds. 
  Full-batch training loss = 0.013943, test loss = 0.229941
  Training set accuracy = 0.995300, Test set accuracy = 0.956800
 epoch 18/100. Took 0.50898 seconds. 
  Full-batch training loss = 0.003418, test loss = 0.205160
  Training set accuracy = 0.999000, Test set accuracy = 0.961500
 epoch 19/100. Took 0.51054 seconds. 
  Full-batch training loss = 0.000493, test loss = 0.195003
  Training set accuracy = 1.000000, Test set accuracy = 0.965300
 epoch 20/100. Took 0.50975 seconds. 
  Full-batch training loss = 0.000172, test loss = 0.194688
  Training set accuracy = 1.000000, Test set accuracy = 0.965600
 epoch 21/100. Took 0.50978 seconds. 
  Full-batch training loss = 0.000126, test loss = 0.197621
  Training set accuracy = 1.000000, Test set accuracy = 0.965700
 epoch 22/100. Took 0.51037 seconds. 
  Full-batch training loss = 0.000104, test loss = 0.200125
  Training set accuracy = 1.000000, Test set accuracy = 0.965800
 epoch 23/100. Took 0.5062 seconds. 
  Full-batch training loss = 0.000089, test loss = 0.202070
  Training set accuracy = 1.000000, Test set accuracy = 0.965900
 epoch 24/100. Took 0.50505 seconds. 
  Full-batch training loss = 0.000079, test loss = 0.203913
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 25/100. Took 0.5079 seconds. 
  Full-batch training loss = 0.000071, test loss = 0.205440
  Training set accuracy = 1.000000, Test set accuracy = 0.965800
 epoch 26/100. Took 0.5083 seconds. 
  Full-batch training loss = 0.000064, test loss = 0.206871
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 27/100. Took 0.50544 seconds. 
  Full-batch training loss = 0.000058, test loss = 0.208197
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 28/100. Took 0.50643 seconds. 
  Full-batch training loss = 0.000054, test loss = 0.209454
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 29/100. Took 0.5089 seconds. 
  Full-batch training loss = 0.000050, test loss = 0.210575
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 30/100. Took 0.51547 seconds. 
  Full-batch training loss = 0.000047, test loss = 0.211623
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 31/100. Took 0.51709 seconds. 
  Full-batch training loss = 0.000044, test loss = 0.212647
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 32/100. Took 0.50992 seconds. 
  Full-batch training loss = 0.000041, test loss = 0.213575
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 33/100. Took 0.50781 seconds. 
  Full-batch training loss = 0.000039, test loss = 0.214466
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 34/100. Took 0.51743 seconds. 
  Full-batch training loss = 0.000037, test loss = 0.215339
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 35/100. Took 0.50625 seconds. 
  Full-batch training loss = 0.000035, test loss = 0.216127
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 36/100. Took 0.51194 seconds. 
  Full-batch training loss = 0.000033, test loss = 0.216908
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 37/100. Took 0.513 seconds. 
  Full-batch training loss = 0.000032, test loss = 0.217653
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 38/100. Took 0.5088 seconds. 
  Full-batch training loss = 0.000030, test loss = 0.218357
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 39/100. Took 0.50698 seconds. 
  Full-batch training loss = 0.000029, test loss = 0.219051
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 40/100. Took 0.50926 seconds. 
  Full-batch training loss = 0.000028, test loss = 0.219711
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 41/100. Took 0.51182 seconds. 
  Full-batch training loss = 0.000027, test loss = 0.220328
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 42/100. Took 0.51214 seconds. 
  Full-batch training loss = 0.000026, test loss = 0.220957
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 43/100. Took 0.5104 seconds. 
  Full-batch training loss = 0.000025, test loss = 0.221559
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 44/100. Took 0.50821 seconds. 
  Full-batch training loss = 0.000024, test loss = 0.222107
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 45/100. Took 0.50751 seconds. 
  Full-batch training loss = 0.000023, test loss = 0.222667
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 46/100. Took 0.50799 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.223223
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 47/100. Took 0.51059 seconds. 
  Full-batch training loss = 0.000022, test loss = 0.223738
  Training set accuracy = 1.000000, Test set accuracy = 0.966500
 epoch 48/100. Took 0.50983 seconds. 
  Full-batch training loss = 0.000021, test loss = 0.224243
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 49/100. Took 0.5109 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.224738
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 50/100. Took 0.51079 seconds. 
  Full-batch training loss = 0.000020, test loss = 0.225214
  Training set accuracy = 1.000000, Test set accuracy = 0.966400
 epoch 51/100. Took 0.51012 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.225676
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 52/100. Took 0.50838 seconds. 
  Full-batch training loss = 0.000019, test loss = 0.226114
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 53/100. Took 0.51458 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.226573
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 54/100. Took 0.51491 seconds. 
  Full-batch training loss = 0.000018, test loss = 0.227018
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 55/100. Took 0.51444 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.227450
  Training set accuracy = 1.000000, Test set accuracy = 0.966300
 epoch 56/100. Took 0.51176 seconds. 
  Full-batch training loss = 0.000017, test loss = 0.227868
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 57/100. Took 0.51536 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.228254
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 58/100. Took 0.51259 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.228674
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 59/100. Took 0.51347 seconds. 
  Full-batch training loss = 0.000016, test loss = 0.229063
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 60/100. Took 0.51565 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.229437
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 61/100. Took 0.52031 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.229810
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 62/100. Took 0.51665 seconds. 
  Full-batch training loss = 0.000015, test loss = 0.230195
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 63/100. Took 0.52127 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.230564
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 64/100. Took 0.51305 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.230915
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 65/100. Took 0.50964 seconds. 
  Full-batch training loss = 0.000014, test loss = 0.231265
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 66/100. Took 0.52393 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.231600
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 67/100. Took 0.52071 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.231946
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 68/100. Took 0.52156 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.232269
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 69/100. Took 0.51218 seconds. 
  Full-batch training loss = 0.000013, test loss = 0.232582
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 70/100. Took 0.51253 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.232903
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 71/100. Took 0.51533 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.233214
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 72/100. Took 0.51353 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.233525
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 73/100. Took 0.51369 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.233812
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 74/100. Took 0.5344 seconds. 
  Full-batch training loss = 0.000012, test loss = 0.234116
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 75/100. Took 0.51326 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.234418
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 76/100. Took 0.53093 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.234706
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 77/100. Took 0.53417 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.234977
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 78/100. Took 0.54571 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.235262
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 79/100. Took 0.63959 seconds. 
  Full-batch training loss = 0.000011, test loss = 0.235537
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 80/100. Took 0.68147 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.235792
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 81/100. Took 0.74154 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.236072
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 82/100. Took 0.70627 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.236330
  Training set accuracy = 1.000000, Test set accuracy = 0.966200
 epoch 83/100. Took 0.6835 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.236585
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 84/100. Took 0.66013 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.236834
  Training set accuracy = 1.000000, Test set accuracy = 0.966100
 epoch 85/100. Took 0.66211 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.237096
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 86/100. Took 0.67742 seconds. 
  Full-batch training loss = 0.000010, test loss = 0.237344
  Training set accuracy = 1.000000, Test set accuracy = 0.965900
 epoch 87/100. Took 0.66073 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.237589
  Training set accuracy = 1.000000, Test set accuracy = 0.965900
 epoch 88/100. Took 0.65945 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.237824
  Training set accuracy = 1.000000, Test set accuracy = 0.965900
 epoch 89/100. Took 0.6747 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.238066
  Training set accuracy = 1.000000, Test set accuracy = 0.965900
 epoch 90/100. Took 0.58036 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.238307
  Training set accuracy = 1.000000, Test set accuracy = 0.965900
 epoch 91/100. Took 0.60023 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.238533
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 92/100. Took 0.56016 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.238764
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 93/100. Took 0.61263 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.238983
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 94/100. Took 0.59447 seconds. 
  Full-batch training loss = 0.000009, test loss = 0.239211
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 95/100. Took 0.54661 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.239428
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 96/100. Took 0.55327 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.239646
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 97/100. Took 0.53845 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.239861
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 98/100. Took 0.54245 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.240075
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 99/100. Took 0.56208 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.240281
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
 epoch 100/100. Took 0.59467 seconds. 
  Full-batch training loss = 0.000008, test loss = 0.240493
  Training set accuracy = 1.000000, Test set accuracy = 0.966000
Elapsed time is 99.911806 seconds.
End Training
