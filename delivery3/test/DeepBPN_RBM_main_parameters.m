clear all;
addpath('../DeepLearningToolbox-RasmusBergPalm');

%%% Recursive addpath
ToolboxPath = '../DeepLearningToolbox-RasmusBergPalm/ToolboxDeepLearning/';
addpath(genpath(ToolboxPath));
DataPath = '../DeepLearningToolbox-RasmusBergPalm/Data/';
addpath(genpath(DataPath));

%%% Architecture
%Arch_lst = [[500 200 50];[200 200 200];[500 200 200];[500 500 200];[500 500 500]];
Arch_lst = [50;200;500];
for x=1:size(Arch_lst,1) %% testing
    Arch = Arch_lst(x,:);

    % Architecture string. Example: from [500 200 100] ----> '500_200_100'
    archStr = '';
    for n = 1:length(Arch) - 1
        archStr = strcat(archStr,num2str(Arch(n)),'_');
    end
    archStr = strcat(archStr,num2str(Arch(end)));

    %%% Parameters and corresponding indexes
    ReducedData_lst = [2000 5000 10000];
    i = 1;
    PreTrainRBM_lst = [true, false];
    j = 1;
    %% Fine-tuning parameters
    FT_Epochs_lst = [100 200 500];
    k = 1;
    FT_LearningRate_lst = [0.2 0.1 0.05];
    l = 1;
    %% Pretraining parameters
    PT_Epochs_lst = [20 50 100];
    m = 1;
    PT_LearningRate_lst = [0.2 0.1 0.05];
    n = 1;
    path = '../results/RBM/';
    % create the folder if it doesn't exist already.
    if ~exist(strcat(path,archStr), 'dir')
      mkdir(strcat(path,archStr));
    end

    %% Changing reduced data
    clc;
    for i = 1:3
        diary(sprintf('%s%s/DeepBPN_RBM_ReducedData_%d.txt',path,archStr, ReducedData_lst(i)));
        diary on
        LoadData_MNIST;
        DeepBPN_RBM_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n', ReducedData);
        fprintf('* PreTrainRBM\ttrue\n');
        fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
        disp('* Fine-tunning')
        fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
        disp('==========================================================')

        DeepBPN_RBM_train_models;
        diary off; % End the diary
    end

    %% Changing pre-training
    clc;
    for j = 1:2
        if j == 1
            diary(sprintf('%s%s/DeepBPN_RBM_PreTrain_true.txt',path,archStr));
        else
            diary(sprintf('%s%s/DeepBPN_RBM_PreTrain_false.txt',path,archStr));
        end
        diary on

        LoadData_MNIST;
        DeepBPN_RBM_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n', ReducedData);
        if j == 1
            fprintf('* PreTrainRBM\ttrue\n');
            fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
            fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
            disp('* Fine-tunning')
            fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
            fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
        else
            fprintf('* PreTrainRBM\tfalse\n');
        end
        disp('==========================================================')

        DeepBPN_RBM_train_models;
        diary off; % End the diary
    end
    j = 1;

    %% Changing Fine-tuning epochs
    clc;
    for k = 1:3
        diary(sprintf('%s%s/DeepBPN_RBM_FineTuningEpochs_%d.txt',path, archStr, FT_Epochs_lst(k)));
        diary on
        LoadData_MNIST;
        DeepBPN_RBM_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n', ReducedData);
        fprintf('* PreTrainRBM\ttrue\n');
        fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
        disp('* Fine-tunning')
        fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
        disp('==========================================================')

        DeepBPN_RBM_train_models;
        diary off; % End the diary
    end
    k = 1;

    %% Changing Fine-tuning learning rate
    clc;
    for l = 1:3
        diary(sprintf('%s%s/DeepBPN_RBM_FineTuningLearningRate_%d.txt',path,archStr, FT_LearningRate_lst(l)));
        diary on
        LoadData_MNIST;
        DeepBPN_RBM_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n', ReducedData);
        fprintf('* PreTrainRBM\ttrue\n');
        fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
        disp('* Fine-tunning')
        fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
        disp('==========================================================')

        DeepBPN_RBM_train_models;
        diary off; % End the diary
    end
    l = 1;

    %% Changing Pretraining epochs
    clc;
    for m = 1:3
        diary(sprintf('%s%s/DeepBPN_RBM_PreTrainingEpochs_%d.txt',path,archStr, PT_Epochs_lst(m)));
        diary on
        LoadData_MNIST;
        DeepBPN_RBM_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n', ReducedData);
        fprintf('* PreTrainRBM\ttrue\n');
        fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
        disp('* Fine-tunning')
        fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
        disp('==========================================================')

        DeepBPN_RBM_train_models;
        diary off; % End the diary
    end
    m = 1;

    %% Changing Pretraining learning rate
    clc;
    for n = 1:3
        diary(sprintf('%s%s/DeepBPN_RBM_PreTrainingLearningRate_%d.txt',path, archStr, PT_LearningRate_lst(n)));
        diary on
        LoadData_MNIST;
        DeepBPN_RBM_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n', ReducedData);
        fprintf('* PreTrainRBM\ttrue\n');
        fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
        disp('* Fine-tunning')
        fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
        disp('==========================================================')

        DeepBPN_RBM_train_models;
        diary off; % End the diary
    end
    n = 1;


end %% testing