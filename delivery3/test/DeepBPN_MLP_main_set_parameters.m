%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Global parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Setting parameters']);

% Random seed
RandomSeed = sum(100*clock);             %%% sum(100*clock) / A fixed value allows to replicate exactly the same results
% 
rand('state',RandomSeed);
randn('state',RandomSeed);

% Data
ReducedData = ReducedData_lst(i);        %%% If ReducedData > 0, it uses 'ReducedData' traininig input vectors for training
BatchSize = 100;                         %%% Values in [10 100]

% Architecture
NHiddens = [200 200 200];                %%% [100] / [500 500] / etc
ActFunction = 'reclinear';               %%% 'sigm' (sigmoid), 'tanh_opt' (optimal tanh) or 'reclinear' (ReLU)

% Training parameters
TrainBP_IniWRange = 1.0;            %%% Initial weights in BP training from IniWRange * Gaussian(0,1) / sqrt(fanin)
TrainBP_NumEpochs = Epochs_lst(j);  %%% Number of epochs in BP training
TrainBP_LearningRate = LearningRate_lst(k);         %%% Learning rate in BP training
TrainBP_Scaling_learningRate = 1;   %%% Scaling factor for the learning rate (in each epoch) in BP training
TrainBP_Momentum = 0.8;             %%% Values in [0,1] / Momentum in BP training
TrainBP_WeightPenaltyL2 = 0;        %%% Weight decay penalty in BP training
TrainBP_DropoutFraction = 0;        %%% Values in [0,1] / Dropout level (http://www.cs.toronto.edu/~hinton/absps/dropout.pdf)
TrainBP_PlotError = 0;              %%% Plot the error curves (0/1)?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
