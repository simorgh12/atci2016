%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Global parameters
%%% see main.m
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(['Setting parameters']);

% Random seed
RandomSeed = sum(100*clock);             %%% sum(100*clock) / A fixed value allows to replicate exactly the same results
% 
rand('state',RandomSeed);
randn('state',RandomSeed);

% Data
ReducedData = ReducedData_lst(i);        %%% If ReducedData > 0, it uses 'ReducedData' traininig input vectors for training
BatchSize = 100;                         %%% Values in [10 100]

% Architecture
NHiddens = [200 200 200];                %%% [100] / [500 500] / etc

% Pre-training y/n
PreTrainRBM = PreTrainRBM_lst(j);        %%% true: pre-training with Stacked RBMs

% Pre-training parameters
PreTrainRBM_InputDataType = 'binary';    %%% Input type: binary/gaussian
PreTrainRBM_IniVHRange = 1.0;            %%% Initial weights in Stacked RBMs pre-training from IniVHRange * Gaussian(0,1) / sqrt(fanin)         
PreTrainRBM_NumEpochs = PT_Epochs_lst(m); %%% Number of epochs in Stacked RBMs pre-training
PreTrainRBM_LearningRate = PT_LearningRate_lst(n); %%% Learning rate in Stacked RBMs pre-training

PreTrainRBM_Momentum = 0.8;              %%% Momentum in Stacked RBMs pre-training
PreTrainRBM_WeightPenaltyL2 = 0.0002;    %%% Weight decay penalty in RBMs pre-training
PreTrainRBM_VisualizeWeights = false;    %%% Weight visualization in RBMs pre-training

% Fine-tuning parameters
FineTuningBP_IniWRange = 1.0;            %%% Initial NOT PRETRAINED weights in BP fine-tuning from IniWRange * Gaussian(0,1) / sqrt(fanin)
FineTuningBP_NumEpochs = FT_Epochs_lst(k);             %%% Number of epochs in BP fine-tuning
FineTuningBP_LearningRate = FT_LearningRate_lst(l);    %%% Learning rate in BP fine-tuning
FineTuningBP_Scaling_learningRate = 1;   %%% Scaling factor for the learning rate (in each epoch) in BP fine-tuning
FineTuningBP_Momentum = 0.8;             %%% Values in [0,1] / Momentum in BP fine-tuning
FineTuningBP_WeightPenaltyL2 = 0;        %%% Weight decay penalty in BP fine-tuning
FineTuningBP_DropoutFraction = 0;        %%% Values in [0,1] / Dropout level (http://www.cs.toronto.edu/~hinton/absps/dropout.pdf)
FineTuningBP_PlotError = 0;              %%% Plot the error curves (0/1)?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
