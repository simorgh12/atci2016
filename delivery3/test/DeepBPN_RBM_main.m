%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%  Training a Deep BackPropagation Network with (optionally) RBM
%  pretraining:
%  
%  Every layer is (optionally) pretrained as a Restricted Boltmann Machine 
%  in a greedy layer-wise unsupervised way with Contrastive Divergence.
%
%  Subsequently, the output layer is added and the whole network is trained
%  (finetuned) with supervised BackPropagation
%
% -------------------------------------------------------------------------
% Analysis
% -------------------------------------------------------------------------
% Topologies [50/200/500] x N,  N = Number of layers (1 - 3)
% Different topologies: 3 + 3^2 + 3^3 = 39
% We will consider those topologies consisting on "increasing" valued layers
% - i.e [50 200 500] - to be obviated (thus, not considered in training,
% neither in testing)
%
% -------------------------------------------------------------------------
% TestsToPerform
% -------------------------------------------------------------------------
% Different topologies = 19
% ReducedData: 2000, 5000, 10000
% PreTrainRBM: True or False
% Pretraining parameters:
%  - Epochs: 20, 50, 100
%  - LearningRate: 0.2, 0.1, 0.05
% Fine-tuning parameters:
%  - Epochs: 100, 200, 500
%  - LearningRate: 0.2, 0.1, 0.05
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;
clear all;
diary off % mandatory (see how the files are stored in this script)
%close all; % needed if we plot the weights

%% Recursive addpath
ToolboxPath = '../DeepLearningToolbox-RasmusBergPalm/ToolboxDeepLearning/';
addpath(genpath(ToolboxPath));
DataPath = '../DeepLearningToolbox-RasmusBergPalm/Data/';
addpath(genpath(DataPath));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Global settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Random seed
RandomSeed = sum(100*clock);             %%% sum(100*clock) / A fixed value allows to replicate exactly the same results
rand('state',RandomSeed);
randn('state',RandomSeed);
%% Data
BatchSize = 100;                         %%% Values in [10 100]
%% Architecture
NHiddens = [500 500 500];                %%% [100] / [500 500] / etc
%% Relative path
path = sprintf('../results/RBM/');
%% Folder name
folder = int2str(NHiddens(1));
for i=2:size(NHiddens,2)
    folder = strcat(folder,sprintf('-%d',NHiddens(i)));
end

% create the folder if it doesn't exist already.
if ~exist(strcat(path,folder), 'dir')
  mkdir(strcat(path,folder));
end


%% Pre-training parameters
PreTrainRBM_InputDataType = 'binary';    %%% Input type: binary/gaussian
PreTrainRBM_IniVHRange = 1.0;            %%% Initial weights in Stacked RBMs pre-training from IniVHRange * Gaussian(0,1) / sqrt(fanin)
PreTrainRBM_Momentum = 0.8;              %%% Momentum in Stacked RBMs pre-training
PreTrainRBM_WeightPenaltyL2 = 0.0002;    %%% Weight decay penalty in RBMs pre-training
PreTrainRBM_VisualizeWeights = false;    %%% Weight visualization in RBMs pre-training
%% Fine-tuning parameters
FineTuningBP_IniWRange = 1.0;            %%% Initial NOT PRETRAINED weights in BP fine-tuning from IniWRange * Gaussian(0,1) / sqrt(fanin)
FineTuningBP_Scaling_learningRate = 1;   %%% Scaling factor for the learning rate (in each epoch) in BP fine-tuning
FineTuningBP_Momentum = 0.8;             %%% Values in [0,1] / Momentum in BP fine-tuning
FineTuningBP_WeightPenaltyL2 = 0;        %%% Weight decay penalty in BP fine-tuning
FineTuningBP_DropoutFraction = 0;        %%% Values in [0,1] / Dropout level (http://www.cs.toronto.edu/~hinton/absps/dropout.pdf)
FineTuningBP_PlotError = 0;              %%% Plot the error curves (0/1)?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test on-loop settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ReducedData_lst = [2000 5000 10000];
PreTrainRBM_lst = [true, false];
%% Fine-tuning parameters
FT_Epochs_lst = [100 200 500];
FT_LearningRate_lst = [0.2 0.1 0.05];
%% Pretraining parameters
PT_Epochs_lst = [20 50 100];
PT_LearningRate_lst = [0.2 0.1 0.05];

%% Expected num. of iterations
it_noPT = size(ReducedData_lst,2) * size(FT_Epochs_lst,2) * size(FT_LearningRate_lst,2);
it_PT = it_noPT * size(PT_Epochs_lst,2) * size(PT_LearningRate_lst,2);
total_it = it_noPT + it_PT; %%% 3^5 * 3^3 = 270

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Use it in case of any execution problem happened.
% Delete the corrupt file and specify here its index.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
IT_CHECKPOINT = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Start autom. process
%%% Note that this will take a while, so be patient and get extra coffe ;)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
it = 1; % exect. counter
for i=1:size(ReducedData_lst,2)
    %% Load MNIST Data
    LoadData_MNIST;
    ReducedData = ReducedData_lst(i);   %%% If ReducedData > 0, it uses 'ReducedData' traininig input vectors for training
    
    %% FineTunning Settings
    for j=1:size(FT_Epochs_lst,2)
        FineTuningBP_NumEpochs = FT_Epochs_lst(j);  %%% Number of epochs in BP fine-tuning
        for k=1:size(FT_LearningRate_lst,2)
            FineTuningBP_LearningRate = FT_LearningRate_lst(k); %%% Learning rate in BP fine-tuning
            
            %% Pre-training Settings
            for l=1:size(PreTrainRBM_lst,2)
                % Pre-training y/n
                PreTrainRBM = PreTrainRBM_lst(l);   %%% true: pre-training with Stacked RBMs
                
                if(PreTrainRBM)
                    for m=1:size(PT_Epochs_lst,2)
                       PreTrainRBM_NumEpochs = PT_Epochs_lst(m);   %%% Number of epochs in Stacked RBMs pre-training
                       for n=1:size(PT_LearningRate_lst,2)
                           PreTrainRBM_LearningRate = PT_LearningRate_lst(n);	%%% Learning rate in Stacked RBMs pre-training
                           if it >= IT_CHECKPOINT
                               %% activate log output
                               clc;
                               diary(sprintf('%s%s/DeepBPN_RBM_%d.txt',path,folder,it));
                               diary on

                               disp('==========================================================')
                               fprintf('Architecture is %s\n', folder);
                               disp('----------------------------------------------------------')
                               fprintf('Execution %d/%d\n',it,total_it);
                               disp('----------------------------------------------------------')
                               fprintf('* ReducedData\t%d\n', ReducedData);
                               fprintf('* PreTrainRBM\ttrue\n');
                               fprintf('\t-> Epochs\t%d\n',PreTrainRBM_NumEpochs);
                               fprintf('\t-> LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
                               disp('* Fine-tunning')
                               fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
                               fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
                               disp('==========================================================')

                               %% TRAIN/TEST
                               DeepBPN_RBM_train_models;
                               diary off; % End the diary
                           end
                           it = it + 1;
                       end
                    end
                    % END Pretraining params
                else
                    %% Skip PreTraining Process
                    if it >= IT_CHECKPOINT
                        %% activate log output
                        clc;
                        diary(sprintf('%s%s/DeepBPN_RBM_%d.txt',path,folder,it));
                        diary on

                        disp('==========================================================')
                        fprintf('Architecture is %s\n',folder);
                        disp('----------------------------------------------------------')
                        fprintf('Execution %d/%d\n',it,total_it);
                        disp('----------------------------------------------------------')
                        fprintf('* ReducedData\t%d\n', ReducedData);
                        fprintf('* PreTrainRBM\tfalse\n');
                        fprintf('\t[x] Epochs\t%d\n',PreTrainRBM_NumEpochs);
                        fprintf('\t[x] LearningRate\t%0.2f\n',PreTrainRBM_LearningRate);
                        disp('* Fine-tunning')
                        fprintf('\t-> Epochs\t%d\n',FineTuningBP_NumEpochs);
                        fprintf('\t-> LearningRate\t%0.2f\n',FineTuningBP_LearningRate);
                        disp('==========================================================')
                    
                    
                        %% TRAIN/TEST
                        DeepBPN_RBM_train_models;
                        diary off; %%% End the diary
                    end
                    it = it + 1;
                end
            end
            % END Pretraining true/false
        end
    end
    % END finetuning
end
% END ReducedData