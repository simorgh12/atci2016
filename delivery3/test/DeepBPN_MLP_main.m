clear all;
addpath('../DeepLearningToolbox-RasmusBergPalm');

%%% Recursive addpath
ToolboxPath = '../DeepLearningToolbox-RasmusBergPalm/ToolboxDeepLearning/';
addpath(genpath(ToolboxPath));
DataPath = '../DeepLearningToolbox-RasmusBergPalm/Data/';
addpath(genpath(DataPath));

%%% Architectures
%Arch_lst = [[500 200 50];[200 200 200];[500 200 200];[500 500 200];[500 500 500]]; % 3 layers
%Arch_lst = [[50 50];[200 50];[500 50];[200 200];[500 200];[500 500]];              % 2 layers
Arch_lst = [50; 200; 500]; % 1 layer
for x=1:size(Arch_lst,1) %% testing
    Arch = Arch_lst(x,:);

    % Architecture string. Example: from [500 200 100] ----> '500_200_100'
    archStr = '';
    for n = 1:length(Arch) - 1
        archStr = strcat(archStr,num2str(Arch(n)),'_');
    end
    archStr = strcat(archStr,num2str(Arch(end)));

    %%% Parameters and corresponding indexes
    ReducedData_lst = [2000 5000 10000]; i = 1;
    Epochs_lst = [100 200 500]; j = 1;
    LearningRate_lst = [0.2 0.1 0.05]; k = 1;
    
    path = '../results/MLP/';
    % create the folder if it doesn't exist already.
    if ~exist(strcat(path,archStr), 'dir')
      mkdir(strcat(path,archStr));
    end

    %% Changing reduced data
    clc;
    for i = 1:size(ReducedData_lst,2)
        diary(sprintf('%s%s/DeepBPN_MLP_ReducedData_%d.txt',path,archStr, ReducedData_lst(i)));
        diary on
        LoadData_MNIST;
        DeepBPN_MLP_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n',ReducedData);
        disp('* Training Parameters:');
        fprintf('\t-> Epochs\t%d\n',TrainBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',TrainBP_LearningRate);
        disp('==========================================================')

        DeepBPN_MLP_train_models;
        diary off; % End the diary
    end

   
    %% Changing epochs
    clc;
    for j = 1:size(Epochs_lst,2)
        diary(sprintf('%s%s/DeepBPN_MLP_Epochs_%d.txt',path, archStr, Epochs_lst(j)));
        diary on
        LoadData_MNIST;
        DeepBPN_MLP_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n',ReducedData);
        disp('* Training Parameters:');
        fprintf('\t-> Epochs\t%d\n',TrainBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',TrainBP_LearningRate);
        disp('==========================================================')

        DeepBPN_MLP_train_models;
        diary off; % End the diary
    end
    j = 1;

    %% Changing learning rate
    clc;
    for k = 1:size(LearningRate_lst,2)
        diary(sprintf('%s%s/DeepBPN_MLP_LearningRate_%d.txt',path,archStr, LearningRate_lst(k)));
        diary on
        LoadData_MNIST;
        DeepBPN_MLP_main_set_parameters;

        disp('==========================================================')
        fprintf('* ReducedData\t%d\n',ReducedData);
        disp('* Training Parameters:');
        fprintf('\t-> Epochs\t%d\n',TrainBP_NumEpochs);
        fprintf('\t-> LearningRate\t%0.2f\n',TrainBP_LearningRate);
        disp('==========================================================')

        DeepBPN_MLP_train_models;
        diary off; % End the diary
    end
    k = 1;

end %% testing