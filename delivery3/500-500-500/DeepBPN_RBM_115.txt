==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 115/270
----------------------------------------------------------
* ReducedData	5000
* PreTrainRBM	true
	-> Epochs	50
	-> LearningRate	0.10
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.5501 seconds. Average reconstruction error is: 35.5113
 epoch 2/50. Took 0.54711 seconds. Average reconstruction error is: 18.8269
 epoch 3/50. Took 0.54341 seconds. Average reconstruction error is: 15.5815
 epoch 4/50. Took 0.54548 seconds. Average reconstruction error is: 13.882
 epoch 5/50. Took 0.53643 seconds. Average reconstruction error is: 12.7718
 epoch 6/50. Took 0.54759 seconds. Average reconstruction error is: 11.9946
 epoch 7/50. Took 0.54661 seconds. Average reconstruction error is: 11.364
 epoch 8/50. Took 0.54223 seconds. Average reconstruction error is: 10.828
 epoch 9/50. Took 0.55274 seconds. Average reconstruction error is: 10.417
 epoch 10/50. Took 0.54436 seconds. Average reconstruction error is: 10.0211
 epoch 11/50. Took 0.53823 seconds. Average reconstruction error is: 9.7707
 epoch 12/50. Took 0.54159 seconds. Average reconstruction error is: 9.4874
 epoch 13/50. Took 0.547 seconds. Average reconstruction error is: 9.2501
 epoch 14/50. Took 0.5525 seconds. Average reconstruction error is: 9.0727
 epoch 15/50. Took 0.53881 seconds. Average reconstruction error is: 8.9091
 epoch 16/50. Took 0.54431 seconds. Average reconstruction error is: 8.7672
 epoch 17/50. Took 0.53851 seconds. Average reconstruction error is: 8.6134
 epoch 18/50. Took 0.54406 seconds. Average reconstruction error is: 8.5033
 epoch 19/50. Took 0.5488 seconds. Average reconstruction error is: 8.4123
 epoch 20/50. Took 0.55376 seconds. Average reconstruction error is: 8.3593
 epoch 21/50. Took 0.54793 seconds. Average reconstruction error is: 8.2336
 epoch 22/50. Took 0.54607 seconds. Average reconstruction error is: 8.1488
 epoch 23/50. Took 0.54967 seconds. Average reconstruction error is: 8.0536
 epoch 24/50. Took 0.55098 seconds. Average reconstruction error is: 7.9934
 epoch 25/50. Took 0.54457 seconds. Average reconstruction error is: 7.9676
 epoch 26/50. Took 0.54783 seconds. Average reconstruction error is: 7.9312
 epoch 27/50. Took 0.54201 seconds. Average reconstruction error is: 7.8917
 epoch 28/50. Took 0.54661 seconds. Average reconstruction error is: 7.8176
 epoch 29/50. Took 0.54656 seconds. Average reconstruction error is: 7.7973
 epoch 30/50. Took 0.53538 seconds. Average reconstruction error is: 7.7315
 epoch 31/50. Took 0.55301 seconds. Average reconstruction error is: 7.7042
 epoch 32/50. Took 0.54169 seconds. Average reconstruction error is: 7.6287
 epoch 33/50. Took 0.54727 seconds. Average reconstruction error is: 7.5929
 epoch 34/50. Took 0.54522 seconds. Average reconstruction error is: 7.5785
 epoch 35/50. Took 0.54685 seconds. Average reconstruction error is: 7.575
 epoch 36/50. Took 0.55792 seconds. Average reconstruction error is: 7.5767
 epoch 37/50. Took 0.55081 seconds. Average reconstruction error is: 7.5133
 epoch 38/50. Took 0.54494 seconds. Average reconstruction error is: 7.4946
 epoch 39/50. Took 0.54659 seconds. Average reconstruction error is: 7.4801
 epoch 40/50. Took 0.54955 seconds. Average reconstruction error is: 7.4232
 epoch 41/50. Took 0.54453 seconds. Average reconstruction error is: 7.392
 epoch 42/50. Took 0.55172 seconds. Average reconstruction error is: 7.4059
 epoch 43/50. Took 0.55161 seconds. Average reconstruction error is: 7.3555
 epoch 44/50. Took 0.54326 seconds. Average reconstruction error is: 7.3321
 epoch 45/50. Took 0.54255 seconds. Average reconstruction error is: 7.2901
 epoch 46/50. Took 0.54983 seconds. Average reconstruction error is: 7.2974
 epoch 47/50. Took 0.54684 seconds. Average reconstruction error is: 7.2852
 epoch 48/50. Took 0.55098 seconds. Average reconstruction error is: 7.2725
 epoch 49/50. Took 0.55217 seconds. Average reconstruction error is: 7.2562
 epoch 50/50. Took 0.54273 seconds. Average reconstruction error is: 7.287
Training binary-binary RBM in layer 2 (500  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.38768 seconds. Average reconstruction error is: 33.7796
 epoch 2/50. Took 0.37694 seconds. Average reconstruction error is: 18.4843
 epoch 3/50. Took 0.37948 seconds. Average reconstruction error is: 15.1601
 epoch 4/50. Took 0.38009 seconds. Average reconstruction error is: 13.586
 epoch 5/50. Took 0.38475 seconds. Average reconstruction error is: 12.6423
 epoch 6/50. Took 0.37627 seconds. Average reconstruction error is: 11.9672
 epoch 7/50. Took 0.38369 seconds. Average reconstruction error is: 11.4854
 epoch 8/50. Took 0.38204 seconds. Average reconstruction error is: 11.1492
 epoch 9/50. Took 0.38253 seconds. Average reconstruction error is: 10.8557
 epoch 10/50. Took 0.38539 seconds. Average reconstruction error is: 10.6044
 epoch 11/50. Took 0.37806 seconds. Average reconstruction error is: 10.4388
 epoch 12/50. Took 0.38362 seconds. Average reconstruction error is: 10.2435
 epoch 13/50. Took 0.38345 seconds. Average reconstruction error is: 10.0234
 epoch 14/50. Took 0.38128 seconds. Average reconstruction error is: 9.9265
 epoch 15/50. Took 0.3824 seconds. Average reconstruction error is: 9.7898
 epoch 16/50. Took 0.38103 seconds. Average reconstruction error is: 9.6647
 epoch 17/50. Took 0.38273 seconds. Average reconstruction error is: 9.5765
 epoch 18/50. Took 0.38116 seconds. Average reconstruction error is: 9.4221
 epoch 19/50. Took 0.37798 seconds. Average reconstruction error is: 9.4158
 epoch 20/50. Took 0.37924 seconds. Average reconstruction error is: 9.2324
 epoch 21/50. Took 0.38246 seconds. Average reconstruction error is: 9.2025
 epoch 22/50. Took 0.37966 seconds. Average reconstruction error is: 9.1274
 epoch 23/50. Took 0.37757 seconds. Average reconstruction error is: 9.0763
 epoch 24/50. Took 0.3812 seconds. Average reconstruction error is: 8.9304
 epoch 25/50. Took 0.38245 seconds. Average reconstruction error is: 8.9172
 epoch 26/50. Took 0.38554 seconds. Average reconstruction error is: 8.8745
 epoch 27/50. Took 0.38157 seconds. Average reconstruction error is: 8.7902
 epoch 28/50. Took 0.3809 seconds. Average reconstruction error is: 8.7611
 epoch 29/50. Took 0.38098 seconds. Average reconstruction error is: 8.6833
 epoch 30/50. Took 0.37933 seconds. Average reconstruction error is: 8.6376
 epoch 31/50. Took 0.3787 seconds. Average reconstruction error is: 8.5717
 epoch 32/50. Took 0.38551 seconds. Average reconstruction error is: 8.4875
 epoch 33/50. Took 0.38335 seconds. Average reconstruction error is: 8.449
 epoch 34/50. Took 0.37886 seconds. Average reconstruction error is: 8.4157
 epoch 35/50. Took 0.38432 seconds. Average reconstruction error is: 8.3705
 epoch 36/50. Took 0.38008 seconds. Average reconstruction error is: 8.3023
 epoch 37/50. Took 0.38062 seconds. Average reconstruction error is: 8.2609
 epoch 38/50. Took 0.38364 seconds. Average reconstruction error is: 8.2584
 epoch 39/50. Took 0.37969 seconds. Average reconstruction error is: 8.2283
 epoch 40/50. Took 0.38058 seconds. Average reconstruction error is: 8.1798
 epoch 41/50. Took 0.38861 seconds. Average reconstruction error is: 8.1498
 epoch 42/50. Took 0.38563 seconds. Average reconstruction error is: 8.085
 epoch 43/50. Took 0.38301 seconds. Average reconstruction error is: 8.0492
 epoch 44/50. Took 0.38244 seconds. Average reconstruction error is: 8.0307
 epoch 45/50. Took 0.38598 seconds. Average reconstruction error is: 7.9707
 epoch 46/50. Took 0.3822 seconds. Average reconstruction error is: 7.9235
 epoch 47/50. Took 0.38159 seconds. Average reconstruction error is: 7.9178
 epoch 48/50. Took 0.38217 seconds. Average reconstruction error is: 7.8577
 epoch 49/50. Took 0.37588 seconds. Average reconstruction error is: 7.8028
 epoch 50/50. Took 0.37439 seconds. Average reconstruction error is: 7.8296
Training binary-binary RBM in layer 3 (500  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.37079 seconds. Average reconstruction error is: 24.6824
 epoch 2/50. Took 0.37353 seconds. Average reconstruction error is: 11.8415
 epoch 3/50. Took 0.37679 seconds. Average reconstruction error is: 9.4962
 epoch 4/50. Took 0.37985 seconds. Average reconstruction error is: 8.442
 epoch 5/50. Took 0.3768 seconds. Average reconstruction error is: 7.741
 epoch 6/50. Took 0.37786 seconds. Average reconstruction error is: 7.3707
 epoch 7/50. Took 0.3774 seconds. Average reconstruction error is: 7.0888
 epoch 8/50. Took 0.37707 seconds. Average reconstruction error is: 6.8552
 epoch 9/50. Took 0.37682 seconds. Average reconstruction error is: 6.6607
 epoch 10/50. Took 0.37528 seconds. Average reconstruction error is: 6.5382
 epoch 11/50. Took 0.37543 seconds. Average reconstruction error is: 6.4804
 epoch 12/50. Took 0.37531 seconds. Average reconstruction error is: 6.391
 epoch 13/50. Took 0.37587 seconds. Average reconstruction error is: 6.3011
 epoch 14/50. Took 0.38137 seconds. Average reconstruction error is: 6.1995
 epoch 15/50. Took 0.37462 seconds. Average reconstruction error is: 6.1487
 epoch 16/50. Took 0.37693 seconds. Average reconstruction error is: 6.0694
 epoch 17/50. Took 0.37915 seconds. Average reconstruction error is: 6.0405
 epoch 18/50. Took 0.37297 seconds. Average reconstruction error is: 5.9938
 epoch 19/50. Took 0.37667 seconds. Average reconstruction error is: 5.9635
 epoch 20/50. Took 0.37401 seconds. Average reconstruction error is: 5.9488
 epoch 21/50. Took 0.37606 seconds. Average reconstruction error is: 5.9174
 epoch 22/50. Took 0.3805 seconds. Average reconstruction error is: 5.8628
 epoch 23/50. Took 0.37917 seconds. Average reconstruction error is: 5.7932
 epoch 24/50. Took 0.37832 seconds. Average reconstruction error is: 5.7677
 epoch 25/50. Took 0.3776 seconds. Average reconstruction error is: 5.7679
 epoch 26/50. Took 0.37672 seconds. Average reconstruction error is: 5.7762
 epoch 27/50. Took 0.37399 seconds. Average reconstruction error is: 5.723
 epoch 28/50. Took 0.37608 seconds. Average reconstruction error is: 5.7604
 epoch 29/50. Took 0.37752 seconds. Average reconstruction error is: 5.6602
 epoch 30/50. Took 0.38001 seconds. Average reconstruction error is: 5.6643
 epoch 31/50. Took 0.37449 seconds. Average reconstruction error is: 5.6482
 epoch 32/50. Took 0.37678 seconds. Average reconstruction error is: 5.6161
 epoch 33/50. Took 0.37764 seconds. Average reconstruction error is: 5.6516
 epoch 34/50. Took 0.37568 seconds. Average reconstruction error is: 5.6197
 epoch 35/50. Took 0.37285 seconds. Average reconstruction error is: 5.5815
 epoch 36/50. Took 0.37594 seconds. Average reconstruction error is: 5.5861
 epoch 37/50. Took 0.37493 seconds. Average reconstruction error is: 5.5712
 epoch 38/50. Took 0.37733 seconds. Average reconstruction error is: 5.5814
 epoch 39/50. Took 0.37996 seconds. Average reconstruction error is: 5.5295
 epoch 40/50. Took 0.37839 seconds. Average reconstruction error is: 5.5605
 epoch 41/50. Took 0.39459 seconds. Average reconstruction error is: 5.5222
 epoch 42/50. Took 0.37565 seconds. Average reconstruction error is: 5.5314
 epoch 43/50. Took 0.37472 seconds. Average reconstruction error is: 5.4935
 epoch 44/50. Took 0.37619 seconds. Average reconstruction error is: 5.4723
 epoch 45/50. Took 0.37376 seconds. Average reconstruction error is: 5.4521
 epoch 46/50. Took 0.37849 seconds. Average reconstruction error is: 5.4735
 epoch 47/50. Took 0.37458 seconds. Average reconstruction error is: 5.4934
 epoch 48/50. Took 0.37528 seconds. Average reconstruction error is: 5.4556
 epoch 49/50. Took 0.37457 seconds. Average reconstruction error is: 5.4351
 epoch 50/50. Took 0.37498 seconds. Average reconstruction error is: 5.4263
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.87616 seconds. 
  Full-batch training loss = 0.471314, test loss = 0.478286
  Training set accuracy = 0.884600, Test set accuracy = 0.883000
 epoch 2/100. Took 0.87618 seconds. 
  Full-batch training loss = 0.357842, test loss = 0.370375
  Training set accuracy = 0.909000, Test set accuracy = 0.903900
 epoch 3/100. Took 0.86983 seconds. 
  Full-batch training loss = 0.312313, test loss = 0.330750
  Training set accuracy = 0.918200, Test set accuracy = 0.910900
 epoch 4/100. Took 0.86954 seconds. 
  Full-batch training loss = 0.279318, test loss = 0.301990
  Training set accuracy = 0.928200, Test set accuracy = 0.916100
 epoch 5/100. Took 0.86199 seconds. 
  Full-batch training loss = 0.257486, test loss = 0.286148
  Training set accuracy = 0.932000, Test set accuracy = 0.921500
 epoch 6/100. Took 0.91199 seconds. 
  Full-batch training loss = 0.241756, test loss = 0.270897
  Training set accuracy = 0.933800, Test set accuracy = 0.922600
 epoch 7/100. Took 0.87849 seconds. 
  Full-batch training loss = 0.226024, test loss = 0.260061
  Training set accuracy = 0.938400, Test set accuracy = 0.925800
 epoch 8/100. Took 0.89402 seconds. 
  Full-batch training loss = 0.211073, test loss = 0.250617
  Training set accuracy = 0.944200, Test set accuracy = 0.928800
 epoch 9/100. Took 0.87289 seconds. 
  Full-batch training loss = 0.200252, test loss = 0.242795
  Training set accuracy = 0.945800, Test set accuracy = 0.930000
 epoch 10/100. Took 0.87316 seconds. 
  Full-batch training loss = 0.190989, test loss = 0.239161
  Training set accuracy = 0.948600, Test set accuracy = 0.931100
 epoch 11/100. Took 0.87471 seconds. 
  Full-batch training loss = 0.181413, test loss = 0.235154
  Training set accuracy = 0.954000, Test set accuracy = 0.932400
 epoch 12/100. Took 0.87788 seconds. 
  Full-batch training loss = 0.175692, test loss = 0.229852
  Training set accuracy = 0.953200, Test set accuracy = 0.932600
 epoch 13/100. Took 0.86518 seconds. 
  Full-batch training loss = 0.166969, test loss = 0.225672
  Training set accuracy = 0.955800, Test set accuracy = 0.935100
 epoch 14/100. Took 0.87236 seconds. 
  Full-batch training loss = 0.159723, test loss = 0.222668
  Training set accuracy = 0.959600, Test set accuracy = 0.935100
 epoch 15/100. Took 0.884 seconds. 
  Full-batch training loss = 0.152986, test loss = 0.219714
  Training set accuracy = 0.958000, Test set accuracy = 0.935800
 epoch 16/100. Took 0.86964 seconds. 
  Full-batch training loss = 0.149410, test loss = 0.218309
  Training set accuracy = 0.961200, Test set accuracy = 0.937100
 epoch 17/100. Took 0.87975 seconds. 
  Full-batch training loss = 0.143386, test loss = 0.217016
  Training set accuracy = 0.963200, Test set accuracy = 0.936100
 epoch 18/100. Took 0.877 seconds. 
  Full-batch training loss = 0.135722, test loss = 0.210654
  Training set accuracy = 0.964800, Test set accuracy = 0.937800
 epoch 19/100. Took 0.87623 seconds. 
  Full-batch training loss = 0.131912, test loss = 0.211511
  Training set accuracy = 0.967600, Test set accuracy = 0.938300
 epoch 20/100. Took 0.88174 seconds. 
  Full-batch training loss = 0.126762, test loss = 0.207380
  Training set accuracy = 0.969400, Test set accuracy = 0.938300
 epoch 21/100. Took 0.86867 seconds. 
  Full-batch training loss = 0.122769, test loss = 0.206320
  Training set accuracy = 0.970000, Test set accuracy = 0.938300
 epoch 22/100. Took 0.86635 seconds. 
  Full-batch training loss = 0.119133, test loss = 0.206228
  Training set accuracy = 0.973000, Test set accuracy = 0.939200
 epoch 23/100. Took 0.87445 seconds. 
  Full-batch training loss = 0.113160, test loss = 0.201390
  Training set accuracy = 0.976000, Test set accuracy = 0.939300
 epoch 24/100. Took 0.86089 seconds. 
  Full-batch training loss = 0.111823, test loss = 0.202514
  Training set accuracy = 0.973400, Test set accuracy = 0.940800
 epoch 25/100. Took 0.87508 seconds. 
  Full-batch training loss = 0.106629, test loss = 0.198301
  Training set accuracy = 0.977000, Test set accuracy = 0.941100
 epoch 26/100. Took 0.87834 seconds. 
  Full-batch training loss = 0.104383, test loss = 0.202428
  Training set accuracy = 0.976400, Test set accuracy = 0.940600
 epoch 27/100. Took 0.8766 seconds. 
  Full-batch training loss = 0.100064, test loss = 0.198996
  Training set accuracy = 0.978800, Test set accuracy = 0.941500
 epoch 28/100. Took 0.87347 seconds. 
  Full-batch training loss = 0.096200, test loss = 0.195786
  Training set accuracy = 0.979000, Test set accuracy = 0.940900
 epoch 29/100. Took 0.88673 seconds. 
  Full-batch training loss = 0.093494, test loss = 0.194786
  Training set accuracy = 0.980000, Test set accuracy = 0.941900
 epoch 30/100. Took 0.86146 seconds. 
  Full-batch training loss = 0.090440, test loss = 0.194452
  Training set accuracy = 0.980600, Test set accuracy = 0.941200
 epoch 31/100. Took 0.87592 seconds. 
  Full-batch training loss = 0.087384, test loss = 0.193443
  Training set accuracy = 0.982200, Test set accuracy = 0.942400
 epoch 32/100. Took 0.8717 seconds. 
  Full-batch training loss = 0.084143, test loss = 0.192016
  Training set accuracy = 0.982600, Test set accuracy = 0.942700
 epoch 33/100. Took 0.87298 seconds. 
  Full-batch training loss = 0.081522, test loss = 0.191268
  Training set accuracy = 0.984000, Test set accuracy = 0.943100
 epoch 34/100. Took 0.87534 seconds. 
  Full-batch training loss = 0.079865, test loss = 0.191234
  Training set accuracy = 0.984200, Test set accuracy = 0.943600
 epoch 35/100. Took 0.8826 seconds. 
  Full-batch training loss = 0.078712, test loss = 0.192963
  Training set accuracy = 0.984200, Test set accuracy = 0.942000
 epoch 36/100. Took 0.86086 seconds. 
  Full-batch training loss = 0.074805, test loss = 0.189312
  Training set accuracy = 0.986400, Test set accuracy = 0.944400
 epoch 37/100. Took 0.87203 seconds. 
  Full-batch training loss = 0.072548, test loss = 0.190128
  Training set accuracy = 0.987200, Test set accuracy = 0.943400
 epoch 38/100. Took 0.87842 seconds. 
  Full-batch training loss = 0.070306, test loss = 0.189102
  Training set accuracy = 0.988000, Test set accuracy = 0.943800
 epoch 39/100. Took 0.87647 seconds. 
  Full-batch training loss = 0.068927, test loss = 0.188271
  Training set accuracy = 0.987200, Test set accuracy = 0.944200
 epoch 40/100. Took 0.86521 seconds. 
  Full-batch training loss = 0.066425, test loss = 0.187166
  Training set accuracy = 0.988200, Test set accuracy = 0.945900
 epoch 41/100. Took 0.88257 seconds. 
  Full-batch training loss = 0.064354, test loss = 0.187522
  Training set accuracy = 0.989000, Test set accuracy = 0.944000
 epoch 42/100. Took 0.87888 seconds. 
  Full-batch training loss = 0.063513, test loss = 0.189582
  Training set accuracy = 0.988800, Test set accuracy = 0.944100
 epoch 43/100. Took 0.87214 seconds. 
  Full-batch training loss = 0.060905, test loss = 0.186510
  Training set accuracy = 0.989400, Test set accuracy = 0.944500
 epoch 44/100. Took 0.87363 seconds. 
  Full-batch training loss = 0.059386, test loss = 0.185108
  Training set accuracy = 0.990200, Test set accuracy = 0.945500
 epoch 45/100. Took 0.88194 seconds. 
  Full-batch training loss = 0.057754, test loss = 0.185937
  Training set accuracy = 0.990000, Test set accuracy = 0.945000
 epoch 46/100. Took 0.87193 seconds. 
  Full-batch training loss = 0.055564, test loss = 0.184602
  Training set accuracy = 0.991000, Test set accuracy = 0.945400
 epoch 47/100. Took 0.88009 seconds. 
  Full-batch training loss = 0.054161, test loss = 0.185218
  Training set accuracy = 0.991400, Test set accuracy = 0.944600
 epoch 48/100. Took 0.86803 seconds. 
  Full-batch training loss = 0.052898, test loss = 0.185063
  Training set accuracy = 0.992200, Test set accuracy = 0.945000
 epoch 49/100. Took 0.86951 seconds. 
  Full-batch training loss = 0.051624, test loss = 0.185832
  Training set accuracy = 0.992200, Test set accuracy = 0.944600
 epoch 50/100. Took 0.8717 seconds. 
  Full-batch training loss = 0.050554, test loss = 0.185308
  Training set accuracy = 0.991000, Test set accuracy = 0.945900
 epoch 51/100. Took 0.87034 seconds. 
  Full-batch training loss = 0.048748, test loss = 0.184469
  Training set accuracy = 0.993600, Test set accuracy = 0.945900
 epoch 52/100. Took 0.86974 seconds. 
  Full-batch training loss = 0.047643, test loss = 0.184089
  Training set accuracy = 0.992600, Test set accuracy = 0.945900
 epoch 53/100. Took 0.87446 seconds. 
  Full-batch training loss = 0.046091, test loss = 0.183637
  Training set accuracy = 0.993000, Test set accuracy = 0.945500
 epoch 54/100. Took 0.86966 seconds. 
  Full-batch training loss = 0.045158, test loss = 0.184115
  Training set accuracy = 0.994000, Test set accuracy = 0.946200
 epoch 55/100. Took 0.87757 seconds. 
  Full-batch training loss = 0.043562, test loss = 0.184020
  Training set accuracy = 0.994200, Test set accuracy = 0.944800
 epoch 56/100. Took 0.86756 seconds. 
  Full-batch training loss = 0.042953, test loss = 0.184254
  Training set accuracy = 0.994600, Test set accuracy = 0.946500
 epoch 57/100. Took 0.86966 seconds. 
  Full-batch training loss = 0.041523, test loss = 0.183743
  Training set accuracy = 0.994800, Test set accuracy = 0.945800
 epoch 58/100. Took 0.86645 seconds. 
  Full-batch training loss = 0.040361, test loss = 0.183744
  Training set accuracy = 0.995000, Test set accuracy = 0.946300
 epoch 59/100. Took 0.86878 seconds. 
  Full-batch training loss = 0.039858, test loss = 0.184544
  Training set accuracy = 0.995400, Test set accuracy = 0.945800
 epoch 60/100. Took 0.86807 seconds. 
  Full-batch training loss = 0.039183, test loss = 0.186164
  Training set accuracy = 0.995600, Test set accuracy = 0.944600
 epoch 61/100. Took 0.8744 seconds. 
  Full-batch training loss = 0.037548, test loss = 0.184184
  Training set accuracy = 0.996000, Test set accuracy = 0.945900
 epoch 62/100. Took 0.87109 seconds. 
  Full-batch training loss = 0.036560, test loss = 0.183274
  Training set accuracy = 0.996600, Test set accuracy = 0.945800
 epoch 63/100. Took 0.86958 seconds. 
  Full-batch training loss = 0.035676, test loss = 0.183903
  Training set accuracy = 0.996200, Test set accuracy = 0.945900
 epoch 64/100. Took 0.87598 seconds. 
  Full-batch training loss = 0.034884, test loss = 0.183642
  Training set accuracy = 0.996200, Test set accuracy = 0.946200
 epoch 65/100. Took 0.86749 seconds. 
  Full-batch training loss = 0.034095, test loss = 0.183916
  Training set accuracy = 0.997600, Test set accuracy = 0.946300
 epoch 66/100. Took 0.87402 seconds. 
  Full-batch training loss = 0.033195, test loss = 0.183814
  Training set accuracy = 0.997200, Test set accuracy = 0.946200
 epoch 67/100. Took 0.87273 seconds. 
  Full-batch training loss = 0.032614, test loss = 0.184923
  Training set accuracy = 0.997200, Test set accuracy = 0.945900
 epoch 68/100. Took 0.87472 seconds. 
  Full-batch training loss = 0.031578, test loss = 0.184169
  Training set accuracy = 0.997200, Test set accuracy = 0.946400
 epoch 69/100. Took 0.87467 seconds. 
  Full-batch training loss = 0.031170, test loss = 0.183302
  Training set accuracy = 0.997600, Test set accuracy = 0.946700
 epoch 70/100. Took 0.87514 seconds. 
  Full-batch training loss = 0.030221, test loss = 0.183739
  Training set accuracy = 0.997400, Test set accuracy = 0.946400
 epoch 71/100. Took 0.88389 seconds. 
  Full-batch training loss = 0.029526, test loss = 0.183769
  Training set accuracy = 0.998000, Test set accuracy = 0.946300
 epoch 72/100. Took 0.87842 seconds. 
  Full-batch training loss = 0.028825, test loss = 0.184018
  Training set accuracy = 0.998200, Test set accuracy = 0.946600
 epoch 73/100. Took 0.87138 seconds. 
  Full-batch training loss = 0.028530, test loss = 0.185106
  Training set accuracy = 0.998200, Test set accuracy = 0.947400
 epoch 74/100. Took 0.87615 seconds. 
  Full-batch training loss = 0.027717, test loss = 0.184951
  Training set accuracy = 0.998400, Test set accuracy = 0.946300
 epoch 75/100. Took 0.87912 seconds. 
  Full-batch training loss = 0.027176, test loss = 0.183380
  Training set accuracy = 0.998800, Test set accuracy = 0.947500
 epoch 76/100. Took 0.87458 seconds. 
  Full-batch training loss = 0.026548, test loss = 0.183942
  Training set accuracy = 0.998600, Test set accuracy = 0.946600
 epoch 77/100. Took 0.86774 seconds. 
  Full-batch training loss = 0.025820, test loss = 0.183467
  Training set accuracy = 0.998800, Test set accuracy = 0.946800
 epoch 78/100. Took 0.87726 seconds. 
  Full-batch training loss = 0.025332, test loss = 0.184272
  Training set accuracy = 0.998800, Test set accuracy = 0.946900
 epoch 79/100. Took 0.86822 seconds. 
  Full-batch training loss = 0.024970, test loss = 0.184681
  Training set accuracy = 0.998600, Test set accuracy = 0.946600
 epoch 80/100. Took 0.87192 seconds. 
  Full-batch training loss = 0.024326, test loss = 0.184266
  Training set accuracy = 0.998800, Test set accuracy = 0.947100
 epoch 81/100. Took 0.89191 seconds. 
  Full-batch training loss = 0.024011, test loss = 0.184075
  Training set accuracy = 0.998800, Test set accuracy = 0.947700
 epoch 82/100. Took 0.87542 seconds. 
  Full-batch training loss = 0.023395, test loss = 0.184009
  Training set accuracy = 0.999200, Test set accuracy = 0.947200
 epoch 83/100. Took 0.86266 seconds. 
  Full-batch training loss = 0.022840, test loss = 0.184770
  Training set accuracy = 0.999400, Test set accuracy = 0.948300
 epoch 84/100. Took 0.87305 seconds. 
  Full-batch training loss = 0.022335, test loss = 0.184175
  Training set accuracy = 0.999400, Test set accuracy = 0.947000
 epoch 85/100. Took 0.86982 seconds. 
  Full-batch training loss = 0.022028, test loss = 0.184403
  Training set accuracy = 0.999400, Test set accuracy = 0.947400
 epoch 86/100. Took 0.86298 seconds. 
  Full-batch training loss = 0.021577, test loss = 0.184879
  Training set accuracy = 0.999400, Test set accuracy = 0.947800
 epoch 87/100. Took 0.88222 seconds. 
  Full-batch training loss = 0.021122, test loss = 0.184808
  Training set accuracy = 0.999600, Test set accuracy = 0.947200
 epoch 88/100. Took 0.86838 seconds. 
  Full-batch training loss = 0.020721, test loss = 0.184498
  Training set accuracy = 0.999400, Test set accuracy = 0.947400
 epoch 89/100. Took 0.87123 seconds. 
  Full-batch training loss = 0.020402, test loss = 0.185714
  Training set accuracy = 0.999400, Test set accuracy = 0.947300
 epoch 90/100. Took 0.87984 seconds. 
  Full-batch training loss = 0.020008, test loss = 0.184609
  Training set accuracy = 0.999600, Test set accuracy = 0.948300
 epoch 91/100. Took 0.86838 seconds. 
  Full-batch training loss = 0.019556, test loss = 0.184671
  Training set accuracy = 0.999600, Test set accuracy = 0.947600
 epoch 92/100. Took 0.8712 seconds. 
  Full-batch training loss = 0.019215, test loss = 0.185170
  Training set accuracy = 0.999600, Test set accuracy = 0.947500
 epoch 93/100. Took 0.87511 seconds. 
  Full-batch training loss = 0.018979, test loss = 0.185160
  Training set accuracy = 0.999600, Test set accuracy = 0.948400
 epoch 94/100. Took 0.87845 seconds. 
  Full-batch training loss = 0.018615, test loss = 0.185026
  Training set accuracy = 0.999600, Test set accuracy = 0.948400
 epoch 95/100. Took 0.88166 seconds. 
  Full-batch training loss = 0.018257, test loss = 0.185916
  Training set accuracy = 0.999600, Test set accuracy = 0.948100
 epoch 96/100. Took 0.87585 seconds. 
  Full-batch training loss = 0.017907, test loss = 0.185360
  Training set accuracy = 0.999600, Test set accuracy = 0.947800
 epoch 97/100. Took 0.87787 seconds. 
  Full-batch training loss = 0.017605, test loss = 0.185718
  Training set accuracy = 0.999600, Test set accuracy = 0.948300
 epoch 98/100. Took 0.87065 seconds. 
  Full-batch training loss = 0.017335, test loss = 0.185409
  Training set accuracy = 0.999600, Test set accuracy = 0.949100
 epoch 99/100. Took 0.87563 seconds. 
  Full-batch training loss = 0.016996, test loss = 0.185694
  Training set accuracy = 0.999600, Test set accuracy = 0.947800
 epoch 100/100. Took 0.86642 seconds. 
  Full-batch training loss = 0.016714, test loss = 0.185342
  Training set accuracy = 0.999600, Test set accuracy = 0.948500
Elapsed time is 261.960440 seconds.
End Training
