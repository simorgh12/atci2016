==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 201/270
----------------------------------------------------------
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 1.1107 seconds. Average reconstruction error is: 27.6326
 epoch 2/20. Took 1.1156 seconds. Average reconstruction error is: 16.2298
 epoch 3/20. Took 1.1158 seconds. Average reconstruction error is: 13.9287
 epoch 4/20. Took 1.117 seconds. Average reconstruction error is: 12.569
 epoch 5/20. Took 1.1057 seconds. Average reconstruction error is: 11.6512
 epoch 6/20. Took 1.1089 seconds. Average reconstruction error is: 10.9584
 epoch 7/20. Took 1.1084 seconds. Average reconstruction error is: 10.4671
 epoch 8/20. Took 1.102 seconds. Average reconstruction error is: 10.084
 epoch 9/20. Took 1.1059 seconds. Average reconstruction error is: 9.7791
 epoch 10/20. Took 1.1087 seconds. Average reconstruction error is: 9.5556
 epoch 11/20. Took 1.0968 seconds. Average reconstruction error is: 9.3799
 epoch 12/20. Took 1.1103 seconds. Average reconstruction error is: 9.2237
 epoch 13/20. Took 1.1054 seconds. Average reconstruction error is: 9.08
 epoch 14/20. Took 1.115 seconds. Average reconstruction error is: 9.0238
 epoch 15/20. Took 1.1066 seconds. Average reconstruction error is: 8.9068
 epoch 16/20. Took 1.1047 seconds. Average reconstruction error is: 8.7941
 epoch 17/20. Took 1.1108 seconds. Average reconstruction error is: 8.7287
 epoch 18/20. Took 1.1021 seconds. Average reconstruction error is: 8.684
 epoch 19/20. Took 1.1088 seconds. Average reconstruction error is: 8.6207
 epoch 20/20. Took 1.109 seconds. Average reconstruction error is: 8.5408
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.74381 seconds. Average reconstruction error is: 18.9766
 epoch 2/20. Took 0.74044 seconds. Average reconstruction error is: 10.7408
 epoch 3/20. Took 0.74246 seconds. Average reconstruction error is: 9.4541
 epoch 4/20. Took 0.7485 seconds. Average reconstruction error is: 8.8389
 epoch 5/20. Took 0.74765 seconds. Average reconstruction error is: 8.3795
 epoch 6/20. Took 0.74843 seconds. Average reconstruction error is: 8.1184
 epoch 7/20. Took 0.74433 seconds. Average reconstruction error is: 7.8824
 epoch 8/20. Took 0.75242 seconds. Average reconstruction error is: 7.6637
 epoch 9/20. Took 0.74702 seconds. Average reconstruction error is: 7.4872
 epoch 10/20. Took 0.74321 seconds. Average reconstruction error is: 7.3089
 epoch 11/20. Took 0.74208 seconds. Average reconstruction error is: 7.1515
 epoch 12/20. Took 0.7496 seconds. Average reconstruction error is: 7.0496
 epoch 13/20. Took 0.74734 seconds. Average reconstruction error is: 6.966
 epoch 14/20. Took 0.75042 seconds. Average reconstruction error is: 6.801
 epoch 15/20. Took 0.73909 seconds. Average reconstruction error is: 6.7123
 epoch 16/20. Took 0.74158 seconds. Average reconstruction error is: 6.6442
 epoch 17/20. Took 0.74282 seconds. Average reconstruction error is: 6.588
 epoch 18/20. Took 0.74883 seconds. Average reconstruction error is: 6.4769
 epoch 19/20. Took 0.74765 seconds. Average reconstruction error is: 6.4196
 epoch 20/20. Took 0.74992 seconds. Average reconstruction error is: 6.3419
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.74604 seconds. Average reconstruction error is: 14.8291
 epoch 2/20. Took 0.74745 seconds. Average reconstruction error is: 7.5194
 epoch 3/20. Took 0.74636 seconds. Average reconstruction error is: 6.5739
 epoch 4/20. Took 0.74969 seconds. Average reconstruction error is: 6.1281
 epoch 5/20. Took 0.74902 seconds. Average reconstruction error is: 5.8819
 epoch 6/20. Took 0.76135 seconds. Average reconstruction error is: 5.668
 epoch 7/20. Took 0.74878 seconds. Average reconstruction error is: 5.544
 epoch 8/20. Took 0.7447 seconds. Average reconstruction error is: 5.4227
 epoch 9/20. Took 0.74868 seconds. Average reconstruction error is: 5.3531
 epoch 10/20. Took 0.74786 seconds. Average reconstruction error is: 5.2954
 epoch 11/20. Took 0.75163 seconds. Average reconstruction error is: 5.2193
 epoch 12/20. Took 0.75257 seconds. Average reconstruction error is: 5.1667
 epoch 13/20. Took 0.74689 seconds. Average reconstruction error is: 5.1578
 epoch 14/20. Took 0.75277 seconds. Average reconstruction error is: 5.1083
 epoch 15/20. Took 0.76708 seconds. Average reconstruction error is: 5.0595
 epoch 16/20. Took 0.74341 seconds. Average reconstruction error is: 5.0116
 epoch 17/20. Took 0.74611 seconds. Average reconstruction error is: 5.0094
 epoch 18/20. Took 0.7508 seconds. Average reconstruction error is: 4.9849
 epoch 19/20. Took 0.75296 seconds. Average reconstruction error is: 4.9695
 epoch 20/20. Took 0.75005 seconds. Average reconstruction error is: 4.9116
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 1.7357 seconds. 
  Full-batch training loss = 0.416470, test loss = 0.415138
  Training set accuracy = 0.894900, Test set accuracy = 0.897500
 epoch 2/100. Took 1.7485 seconds. 
  Full-batch training loss = 0.323697, test loss = 0.330732
  Training set accuracy = 0.913200, Test set accuracy = 0.910600
 epoch 3/100. Took 1.7477 seconds. 
  Full-batch training loss = 0.277019, test loss = 0.288309
  Training set accuracy = 0.924100, Test set accuracy = 0.922300
 epoch 4/100. Took 1.7279 seconds. 
  Full-batch training loss = 0.251556, test loss = 0.267909
  Training set accuracy = 0.930500, Test set accuracy = 0.927500
 epoch 5/100. Took 1.7451 seconds. 
  Full-batch training loss = 0.232217, test loss = 0.253407
  Training set accuracy = 0.935600, Test set accuracy = 0.930600
 epoch 6/100. Took 1.7446 seconds. 
  Full-batch training loss = 0.216471, test loss = 0.241966
  Training set accuracy = 0.940400, Test set accuracy = 0.933700
 epoch 7/100. Took 1.7425 seconds. 
  Full-batch training loss = 0.204451, test loss = 0.234625
  Training set accuracy = 0.943100, Test set accuracy = 0.934400
 epoch 8/100. Took 1.7454 seconds. 
  Full-batch training loss = 0.192808, test loss = 0.226810
  Training set accuracy = 0.947000, Test set accuracy = 0.936300
 epoch 9/100. Took 1.7397 seconds. 
  Full-batch training loss = 0.182226, test loss = 0.219196
  Training set accuracy = 0.950800, Test set accuracy = 0.938600
 epoch 10/100. Took 1.7499 seconds. 
  Full-batch training loss = 0.173117, test loss = 0.215080
  Training set accuracy = 0.953100, Test set accuracy = 0.939300
 epoch 11/100. Took 1.7542 seconds. 
  Full-batch training loss = 0.167143, test loss = 0.213043
  Training set accuracy = 0.955200, Test set accuracy = 0.939700
 epoch 12/100. Took 1.7521 seconds. 
  Full-batch training loss = 0.158302, test loss = 0.206679
  Training set accuracy = 0.957700, Test set accuracy = 0.941300
 epoch 13/100. Took 1.7776 seconds. 
  Full-batch training loss = 0.152647, test loss = 0.203693
  Training set accuracy = 0.959200, Test set accuracy = 0.942000
 epoch 14/100. Took 1.7518 seconds. 
  Full-batch training loss = 0.144950, test loss = 0.198425
  Training set accuracy = 0.962400, Test set accuracy = 0.943100
 epoch 15/100. Took 1.7319 seconds. 
  Full-batch training loss = 0.138827, test loss = 0.195166
  Training set accuracy = 0.964100, Test set accuracy = 0.943500
 epoch 16/100. Took 1.7429 seconds. 
  Full-batch training loss = 0.133496, test loss = 0.193184
  Training set accuracy = 0.964500, Test set accuracy = 0.943400
 epoch 17/100. Took 1.7583 seconds. 
  Full-batch training loss = 0.128370, test loss = 0.190799
  Training set accuracy = 0.966700, Test set accuracy = 0.944400
 epoch 18/100. Took 1.7562 seconds. 
  Full-batch training loss = 0.123738, test loss = 0.187473
  Training set accuracy = 0.967100, Test set accuracy = 0.945200
 epoch 19/100. Took 1.7725 seconds. 
  Full-batch training loss = 0.119125, test loss = 0.185519
  Training set accuracy = 0.969200, Test set accuracy = 0.945300
 epoch 20/100. Took 1.7402 seconds. 
  Full-batch training loss = 0.115344, test loss = 0.184383
  Training set accuracy = 0.970900, Test set accuracy = 0.946000
 epoch 21/100. Took 1.7387 seconds. 
  Full-batch training loss = 0.111340, test loss = 0.181897
  Training set accuracy = 0.972200, Test set accuracy = 0.946000
 epoch 22/100. Took 1.7438 seconds. 
  Full-batch training loss = 0.107641, test loss = 0.181551
  Training set accuracy = 0.971400, Test set accuracy = 0.946500
 epoch 23/100. Took 1.7567 seconds. 
  Full-batch training loss = 0.103649, test loss = 0.178787
  Training set accuracy = 0.974000, Test set accuracy = 0.947400
 epoch 24/100. Took 1.7318 seconds. 
  Full-batch training loss = 0.100990, test loss = 0.178910
  Training set accuracy = 0.974900, Test set accuracy = 0.946800
 epoch 25/100. Took 1.7463 seconds. 
  Full-batch training loss = 0.097024, test loss = 0.176391
  Training set accuracy = 0.975500, Test set accuracy = 0.947700
 epoch 26/100. Took 1.7628 seconds. 
  Full-batch training loss = 0.094674, test loss = 0.175600
  Training set accuracy = 0.977500, Test set accuracy = 0.948400
 epoch 27/100. Took 1.7547 seconds. 
  Full-batch training loss = 0.091148, test loss = 0.174035
  Training set accuracy = 0.978000, Test set accuracy = 0.948400
 epoch 28/100. Took 1.7694 seconds. 
  Full-batch training loss = 0.087967, test loss = 0.172586
  Training set accuracy = 0.979400, Test set accuracy = 0.948300
 epoch 29/100. Took 1.7705 seconds. 
  Full-batch training loss = 0.085863, test loss = 0.171747
  Training set accuracy = 0.980000, Test set accuracy = 0.949100
 epoch 30/100. Took 1.7619 seconds. 
  Full-batch training loss = 0.082918, test loss = 0.172476
  Training set accuracy = 0.980700, Test set accuracy = 0.948700
 epoch 31/100. Took 1.7625 seconds. 
  Full-batch training loss = 0.079832, test loss = 0.169905
  Training set accuracy = 0.983100, Test set accuracy = 0.948500
 epoch 32/100. Took 1.7513 seconds. 
  Full-batch training loss = 0.077887, test loss = 0.169229
  Training set accuracy = 0.983000, Test set accuracy = 0.948500
 epoch 33/100. Took 1.7378 seconds. 
  Full-batch training loss = 0.074981, test loss = 0.167774
  Training set accuracy = 0.983800, Test set accuracy = 0.949500
 epoch 34/100. Took 1.7445 seconds. 
  Full-batch training loss = 0.072748, test loss = 0.166324
  Training set accuracy = 0.984600, Test set accuracy = 0.949700
 epoch 35/100. Took 1.7448 seconds. 
  Full-batch training loss = 0.070697, test loss = 0.166991
  Training set accuracy = 0.985000, Test set accuracy = 0.949900
 epoch 36/100. Took 1.7566 seconds. 
  Full-batch training loss = 0.068469, test loss = 0.165626
  Training set accuracy = 0.986400, Test set accuracy = 0.949900
 epoch 37/100. Took 1.7422 seconds. 
  Full-batch training loss = 0.066477, test loss = 0.165829
  Training set accuracy = 0.986700, Test set accuracy = 0.950000
 epoch 38/100. Took 1.7431 seconds. 
  Full-batch training loss = 0.064425, test loss = 0.163907
  Training set accuracy = 0.987300, Test set accuracy = 0.950500
 epoch 39/100. Took 1.7449 seconds. 
  Full-batch training loss = 0.062548, test loss = 0.163801
  Training set accuracy = 0.988000, Test set accuracy = 0.949900
 epoch 40/100. Took 1.7497 seconds. 
  Full-batch training loss = 0.061358, test loss = 0.163383
  Training set accuracy = 0.988100, Test set accuracy = 0.950700
 epoch 41/100. Took 1.7423 seconds. 
  Full-batch training loss = 0.059074, test loss = 0.162210
  Training set accuracy = 0.988700, Test set accuracy = 0.950700
 epoch 42/100. Took 1.7397 seconds. 
  Full-batch training loss = 0.057556, test loss = 0.162202
  Training set accuracy = 0.988800, Test set accuracy = 0.951900
 epoch 43/100. Took 1.7395 seconds. 
  Full-batch training loss = 0.056558, test loss = 0.163432
  Training set accuracy = 0.990400, Test set accuracy = 0.950500
 epoch 44/100. Took 1.7572 seconds. 
  Full-batch training loss = 0.054537, test loss = 0.161640
  Training set accuracy = 0.990000, Test set accuracy = 0.951300
 epoch 45/100. Took 1.7507 seconds. 
  Full-batch training loss = 0.052877, test loss = 0.161797
  Training set accuracy = 0.989800, Test set accuracy = 0.952000
 epoch 46/100. Took 1.7369 seconds. 
  Full-batch training loss = 0.051198, test loss = 0.160992
  Training set accuracy = 0.990500, Test set accuracy = 0.952000
 epoch 47/100. Took 1.7452 seconds. 
  Full-batch training loss = 0.049646, test loss = 0.160387
  Training set accuracy = 0.991300, Test set accuracy = 0.951400
 epoch 48/100. Took 1.7473 seconds. 
  Full-batch training loss = 0.048464, test loss = 0.160038
  Training set accuracy = 0.991600, Test set accuracy = 0.951900
 epoch 49/100. Took 1.7413 seconds. 
  Full-batch training loss = 0.047064, test loss = 0.159972
  Training set accuracy = 0.991600, Test set accuracy = 0.952600
 epoch 50/100. Took 1.7624 seconds. 
  Full-batch training loss = 0.045885, test loss = 0.160617
  Training set accuracy = 0.992300, Test set accuracy = 0.952700
 epoch 51/100. Took 1.7476 seconds. 
  Full-batch training loss = 0.044432, test loss = 0.159570
  Training set accuracy = 0.992500, Test set accuracy = 0.952300
 epoch 52/100. Took 1.7439 seconds. 
  Full-batch training loss = 0.043640, test loss = 0.160012
  Training set accuracy = 0.992700, Test set accuracy = 0.952700
 epoch 53/100. Took 1.7382 seconds. 
  Full-batch training loss = 0.042162, test loss = 0.158438
  Training set accuracy = 0.992800, Test set accuracy = 0.952400
 epoch 54/100. Took 1.7477 seconds. 
  Full-batch training loss = 0.041199, test loss = 0.158980
  Training set accuracy = 0.993000, Test set accuracy = 0.952300
 epoch 55/100. Took 1.7653 seconds. 
  Full-batch training loss = 0.039879, test loss = 0.158938
  Training set accuracy = 0.993500, Test set accuracy = 0.953100
 epoch 56/100. Took 1.7718 seconds. 
  Full-batch training loss = 0.038823, test loss = 0.158089
  Training set accuracy = 0.993600, Test set accuracy = 0.953200
 epoch 57/100. Took 1.7343 seconds. 
  Full-batch training loss = 0.037851, test loss = 0.158467
  Training set accuracy = 0.994000, Test set accuracy = 0.953100
 epoch 58/100. Took 1.7376 seconds. 
  Full-batch training loss = 0.037026, test loss = 0.158996
  Training set accuracy = 0.994400, Test set accuracy = 0.953100
 epoch 59/100. Took 1.7353 seconds. 
  Full-batch training loss = 0.036334, test loss = 0.158671
  Training set accuracy = 0.994300, Test set accuracy = 0.952700
 epoch 60/100. Took 1.7569 seconds. 
  Full-batch training loss = 0.035143, test loss = 0.157491
  Training set accuracy = 0.994500, Test set accuracy = 0.952800
 epoch 61/100. Took 1.7522 seconds. 
  Full-batch training loss = 0.034193, test loss = 0.157937
  Training set accuracy = 0.994700, Test set accuracy = 0.953500
 epoch 62/100. Took 1.7545 seconds. 
  Full-batch training loss = 0.033199, test loss = 0.158386
  Training set accuracy = 0.995400, Test set accuracy = 0.953100
 epoch 63/100. Took 1.7459 seconds. 
  Full-batch training loss = 0.032554, test loss = 0.157888
  Training set accuracy = 0.995200, Test set accuracy = 0.953400
 epoch 64/100. Took 1.7389 seconds. 
  Full-batch training loss = 0.031928, test loss = 0.157853
  Training set accuracy = 0.995400, Test set accuracy = 0.954500
 epoch 65/100. Took 1.7396 seconds. 
  Full-batch training loss = 0.030861, test loss = 0.158145
  Training set accuracy = 0.995900, Test set accuracy = 0.953500
 epoch 66/100. Took 1.7527 seconds. 
  Full-batch training loss = 0.030059, test loss = 0.157916
  Training set accuracy = 0.996000, Test set accuracy = 0.953700
 epoch 67/100. Took 1.7471 seconds. 
  Full-batch training loss = 0.029413, test loss = 0.157478
  Training set accuracy = 0.996200, Test set accuracy = 0.953600
 epoch 68/100. Took 1.7433 seconds. 
  Full-batch training loss = 0.028642, test loss = 0.157422
  Training set accuracy = 0.996600, Test set accuracy = 0.954100
 epoch 69/100. Took 1.739 seconds. 
  Full-batch training loss = 0.028214, test loss = 0.157176
  Training set accuracy = 0.996900, Test set accuracy = 0.953700
 epoch 70/100. Took 1.7482 seconds. 
  Full-batch training loss = 0.027352, test loss = 0.157705
  Training set accuracy = 0.997000, Test set accuracy = 0.953900
 epoch 71/100. Took 1.744 seconds. 
  Full-batch training loss = 0.026743, test loss = 0.157211
  Training set accuracy = 0.997400, Test set accuracy = 0.954300
 epoch 72/100. Took 1.7519 seconds. 
  Full-batch training loss = 0.026059, test loss = 0.157197
  Training set accuracy = 0.997500, Test set accuracy = 0.954300
 epoch 73/100. Took 1.7427 seconds. 
  Full-batch training loss = 0.025434, test loss = 0.157217
  Training set accuracy = 0.997700, Test set accuracy = 0.954100
 epoch 74/100. Took 1.7394 seconds. 
  Full-batch training loss = 0.024845, test loss = 0.157191
  Training set accuracy = 0.998000, Test set accuracy = 0.954200
 epoch 75/100. Took 1.751 seconds. 
  Full-batch training loss = 0.024471, test loss = 0.158652
  Training set accuracy = 0.997700, Test set accuracy = 0.953800
 epoch 76/100. Took 1.7372 seconds. 
  Full-batch training loss = 0.023730, test loss = 0.157529
  Training set accuracy = 0.998000, Test set accuracy = 0.954900
 epoch 77/100. Took 1.7386 seconds. 
  Full-batch training loss = 0.023290, test loss = 0.157722
  Training set accuracy = 0.998000, Test set accuracy = 0.954400
 epoch 78/100. Took 1.7399 seconds. 
  Full-batch training loss = 0.022789, test loss = 0.157252
  Training set accuracy = 0.998400, Test set accuracy = 0.954900
 epoch 79/100. Took 1.7464 seconds. 
  Full-batch training loss = 0.022281, test loss = 0.157134
  Training set accuracy = 0.998100, Test set accuracy = 0.954700
 epoch 80/100. Took 1.7548 seconds. 
  Full-batch training loss = 0.021724, test loss = 0.157158
  Training set accuracy = 0.998200, Test set accuracy = 0.954900
 epoch 81/100. Took 1.7387 seconds. 
  Full-batch training loss = 0.021310, test loss = 0.157426
  Training set accuracy = 0.998200, Test set accuracy = 0.954700
 epoch 82/100. Took 1.7363 seconds. 
  Full-batch training loss = 0.020813, test loss = 0.157688
  Training set accuracy = 0.998400, Test set accuracy = 0.955000
 epoch 83/100. Took 1.7512 seconds. 
  Full-batch training loss = 0.020471, test loss = 0.158126
  Training set accuracy = 0.998600, Test set accuracy = 0.954400
 epoch 84/100. Took 1.7473 seconds. 
  Full-batch training loss = 0.019952, test loss = 0.158094
  Training set accuracy = 0.998600, Test set accuracy = 0.955300
 epoch 85/100. Took 1.7487 seconds. 
  Full-batch training loss = 0.019537, test loss = 0.157604
  Training set accuracy = 0.998600, Test set accuracy = 0.955200
 epoch 86/100. Took 1.8605 seconds. 
  Full-batch training loss = 0.019282, test loss = 0.157815
  Training set accuracy = 0.998800, Test set accuracy = 0.955800
 epoch 87/100. Took 1.7464 seconds. 
  Full-batch training loss = 0.018833, test loss = 0.157623
  Training set accuracy = 0.998700, Test set accuracy = 0.954800
 epoch 88/100. Took 1.7649 seconds. 
  Full-batch training loss = 0.018376, test loss = 0.158119
  Training set accuracy = 0.999000, Test set accuracy = 0.955400
 epoch 89/100. Took 1.7256 seconds. 
  Full-batch training loss = 0.018069, test loss = 0.157839
  Training set accuracy = 0.998900, Test set accuracy = 0.956400
 epoch 90/100. Took 1.7762 seconds. 
  Full-batch training loss = 0.017662, test loss = 0.157937
  Training set accuracy = 0.999000, Test set accuracy = 0.955800
 epoch 91/100. Took 1.74 seconds. 
  Full-batch training loss = 0.017327, test loss = 0.158872
  Training set accuracy = 0.999000, Test set accuracy = 0.955500
 epoch 92/100. Took 1.7427 seconds. 
  Full-batch training loss = 0.017008, test loss = 0.158522
  Training set accuracy = 0.999100, Test set accuracy = 0.955000
 epoch 93/100. Took 1.7647 seconds. 
  Full-batch training loss = 0.016640, test loss = 0.158664
  Training set accuracy = 0.999200, Test set accuracy = 0.955600
 epoch 94/100. Took 1.7558 seconds. 
  Full-batch training loss = 0.016338, test loss = 0.158032
  Training set accuracy = 0.999200, Test set accuracy = 0.956300
 epoch 95/100. Took 1.7615 seconds. 
  Full-batch training loss = 0.016061, test loss = 0.158771
  Training set accuracy = 0.999000, Test set accuracy = 0.956100
 epoch 96/100. Took 1.7601 seconds. 
  Full-batch training loss = 0.015757, test loss = 0.158825
  Training set accuracy = 0.999100, Test set accuracy = 0.956200
 epoch 97/100. Took 1.7465 seconds. 
  Full-batch training loss = 0.015518, test loss = 0.159101
  Training set accuracy = 0.999100, Test set accuracy = 0.956200
 epoch 98/100. Took 1.7368 seconds. 
  Full-batch training loss = 0.015195, test loss = 0.158372
  Training set accuracy = 0.999300, Test set accuracy = 0.956800
 epoch 99/100. Took 1.7441 seconds. 
  Full-batch training loss = 0.014860, test loss = 0.159144
  Training set accuracy = 0.999300, Test set accuracy = 0.956100
 epoch 100/100. Took 1.7393 seconds. 
  Full-batch training loss = 0.014618, test loss = 0.158730
  Training set accuracy = 0.999400, Test set accuracy = 0.956300
Elapsed time is 365.603841 seconds.
End Training
