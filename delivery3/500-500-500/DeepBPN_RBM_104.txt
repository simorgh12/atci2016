==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 104/270
----------------------------------------------------------
* ReducedData	5000
* PreTrainRBM	true
	-> Epochs	50
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.10
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.54924 seconds. Average reconstruction error is: 35.0408
 epoch 2/50. Took 0.55063 seconds. Average reconstruction error is: 19.6961
 epoch 3/50. Took 0.55065 seconds. Average reconstruction error is: 16.8504
 epoch 4/50. Took 0.55414 seconds. Average reconstruction error is: 15.1626
 epoch 5/50. Took 0.55368 seconds. Average reconstruction error is: 14.0634
 epoch 6/50. Took 0.55117 seconds. Average reconstruction error is: 13.2451
 epoch 7/50. Took 0.54947 seconds. Average reconstruction error is: 12.525
 epoch 8/50. Took 0.55518 seconds. Average reconstruction error is: 11.9612
 epoch 9/50. Took 0.55142 seconds. Average reconstruction error is: 11.5048
 epoch 10/50. Took 0.54205 seconds. Average reconstruction error is: 11.0879
 epoch 11/50. Took 0.55042 seconds. Average reconstruction error is: 10.7328
 epoch 12/50. Took 0.54573 seconds. Average reconstruction error is: 10.4163
 epoch 13/50. Took 0.54955 seconds. Average reconstruction error is: 10.1744
 epoch 14/50. Took 0.54954 seconds. Average reconstruction error is: 9.9616
 epoch 15/50. Took 0.54629 seconds. Average reconstruction error is: 9.7865
 epoch 16/50. Took 0.547 seconds. Average reconstruction error is: 9.5832
 epoch 17/50. Took 0.54948 seconds. Average reconstruction error is: 9.4267
 epoch 18/50. Took 0.55431 seconds. Average reconstruction error is: 9.3112
 epoch 19/50. Took 0.54345 seconds. Average reconstruction error is: 9.1863
 epoch 20/50. Took 0.54441 seconds. Average reconstruction error is: 9.1645
 epoch 21/50. Took 0.54731 seconds. Average reconstruction error is: 9.0479
 epoch 22/50. Took 0.54745 seconds. Average reconstruction error is: 8.9284
 epoch 23/50. Took 0.54848 seconds. Average reconstruction error is: 8.8866
 epoch 24/50. Took 0.55174 seconds. Average reconstruction error is: 8.7563
 epoch 25/50. Took 0.55439 seconds. Average reconstruction error is: 8.6952
 epoch 26/50. Took 0.54736 seconds. Average reconstruction error is: 8.6288
 epoch 27/50. Took 0.54952 seconds. Average reconstruction error is: 8.5895
 epoch 28/50. Took 0.54502 seconds. Average reconstruction error is: 8.5422
 epoch 29/50. Took 0.54527 seconds. Average reconstruction error is: 8.4748
 epoch 30/50. Took 0.53849 seconds. Average reconstruction error is: 8.38
 epoch 31/50. Took 0.54756 seconds. Average reconstruction error is: 8.3254
 epoch 32/50. Took 0.54679 seconds. Average reconstruction error is: 8.3125
 epoch 33/50. Took 0.54329 seconds. Average reconstruction error is: 8.2275
 epoch 34/50. Took 0.54973 seconds. Average reconstruction error is: 8.2083
 epoch 35/50. Took 0.55087 seconds. Average reconstruction error is: 8.1759
 epoch 36/50. Took 0.5448 seconds. Average reconstruction error is: 8.1693
 epoch 37/50. Took 0.54904 seconds. Average reconstruction error is: 8.0785
 epoch 38/50. Took 0.55181 seconds. Average reconstruction error is: 8.1164
 epoch 39/50. Took 0.55452 seconds. Average reconstruction error is: 8.0266
 epoch 40/50. Took 0.54505 seconds. Average reconstruction error is: 8.0374
 epoch 41/50. Took 0.54824 seconds. Average reconstruction error is: 8.0408
 epoch 42/50. Took 0.54614 seconds. Average reconstruction error is: 7.9452
 epoch 43/50. Took 0.54437 seconds. Average reconstruction error is: 7.9359
 epoch 44/50. Took 0.55406 seconds. Average reconstruction error is: 7.8859
 epoch 45/50. Took 0.53864 seconds. Average reconstruction error is: 7.856
 epoch 46/50. Took 0.54108 seconds. Average reconstruction error is: 7.8458
 epoch 47/50. Took 0.54556 seconds. Average reconstruction error is: 7.8539
 epoch 48/50. Took 0.5478 seconds. Average reconstruction error is: 7.7849
 epoch 49/50. Took 0.54748 seconds. Average reconstruction error is: 7.788
 epoch 50/50. Took 0.54535 seconds. Average reconstruction error is: 7.7656
Training binary-binary RBM in layer 2 (500  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.37211 seconds. Average reconstruction error is: 26.0958
 epoch 2/50. Took 0.36654 seconds. Average reconstruction error is: 14.3423
 epoch 3/50. Took 0.37127 seconds. Average reconstruction error is: 12.1486
 epoch 4/50. Took 0.36564 seconds. Average reconstruction error is: 11.1151
 epoch 5/50. Took 0.37568 seconds. Average reconstruction error is: 10.4034
 epoch 6/50. Took 0.371 seconds. Average reconstruction error is: 9.9001
 epoch 7/50. Took 0.37269 seconds. Average reconstruction error is: 9.5881
 epoch 8/50. Took 0.37346 seconds. Average reconstruction error is: 9.2568
 epoch 9/50. Took 0.37068 seconds. Average reconstruction error is: 8.9751
 epoch 10/50. Took 0.36492 seconds. Average reconstruction error is: 8.795
 epoch 11/50. Took 0.37152 seconds. Average reconstruction error is: 8.6269
 epoch 12/50. Took 0.36909 seconds. Average reconstruction error is: 8.4864
 epoch 13/50. Took 0.36933 seconds. Average reconstruction error is: 8.3164
 epoch 14/50. Took 0.37343 seconds. Average reconstruction error is: 8.1877
 epoch 15/50. Took 0.37634 seconds. Average reconstruction error is: 8.0777
 epoch 16/50. Took 0.37161 seconds. Average reconstruction error is: 7.9303
 epoch 17/50. Took 0.37271 seconds. Average reconstruction error is: 7.8781
 epoch 18/50. Took 0.36739 seconds. Average reconstruction error is: 7.7735
 epoch 19/50. Took 0.36907 seconds. Average reconstruction error is: 7.6847
 epoch 20/50. Took 0.37138 seconds. Average reconstruction error is: 7.6251
 epoch 21/50. Took 0.36742 seconds. Average reconstruction error is: 7.5592
 epoch 22/50. Took 0.3716 seconds. Average reconstruction error is: 7.4443
 epoch 23/50. Took 0.37188 seconds. Average reconstruction error is: 7.3985
 epoch 24/50. Took 0.37443 seconds. Average reconstruction error is: 7.3375
 epoch 25/50. Took 0.37194 seconds. Average reconstruction error is: 7.2931
 epoch 26/50. Took 0.36993 seconds. Average reconstruction error is: 7.1913
 epoch 27/50. Took 0.36573 seconds. Average reconstruction error is: 7.1799
 epoch 28/50. Took 0.37108 seconds. Average reconstruction error is: 7.1222
 epoch 29/50. Took 0.37788 seconds. Average reconstruction error is: 7.0834
 epoch 30/50. Took 0.36947 seconds. Average reconstruction error is: 7.0391
 epoch 31/50. Took 0.3709 seconds. Average reconstruction error is: 7.0042
 epoch 32/50. Took 0.36683 seconds. Average reconstruction error is: 7.0039
 epoch 33/50. Took 0.37379 seconds. Average reconstruction error is: 6.8998
 epoch 34/50. Took 0.36798 seconds. Average reconstruction error is: 6.8855
 epoch 35/50. Took 0.37032 seconds. Average reconstruction error is: 6.8652
 epoch 36/50. Took 0.36928 seconds. Average reconstruction error is: 6.8116
 epoch 37/50. Took 0.36846 seconds. Average reconstruction error is: 6.7615
 epoch 38/50. Took 0.36912 seconds. Average reconstruction error is: 6.7206
 epoch 39/50. Took 0.36962 seconds. Average reconstruction error is: 6.6936
 epoch 40/50. Took 0.37051 seconds. Average reconstruction error is: 6.6724
 epoch 41/50. Took 0.37167 seconds. Average reconstruction error is: 6.6629
 epoch 42/50. Took 0.36926 seconds. Average reconstruction error is: 6.6225
 epoch 43/50. Took 0.37425 seconds. Average reconstruction error is: 6.5764
 epoch 44/50. Took 0.36959 seconds. Average reconstruction error is: 6.5389
 epoch 45/50. Took 0.36863 seconds. Average reconstruction error is: 6.5224
 epoch 46/50. Took 0.367 seconds. Average reconstruction error is: 6.4884
 epoch 47/50. Took 0.37511 seconds. Average reconstruction error is: 6.4406
 epoch 48/50. Took 0.371 seconds. Average reconstruction error is: 6.4465
 epoch 49/50. Took 0.37381 seconds. Average reconstruction error is: 6.3963
 epoch 50/50. Took 0.37132 seconds. Average reconstruction error is: 6.3588
Training binary-binary RBM in layer 3 (500  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.37219 seconds. Average reconstruction error is: 19.0309
 epoch 2/50. Took 0.36783 seconds. Average reconstruction error is: 8.8633
 epoch 3/50. Took 0.36529 seconds. Average reconstruction error is: 7.445
 epoch 4/50. Took 0.3702 seconds. Average reconstruction error is: 6.7722
 epoch 5/50. Took 0.37216 seconds. Average reconstruction error is: 6.4312
 epoch 6/50. Took 0.36495 seconds. Average reconstruction error is: 6.1353
 epoch 7/50. Took 0.37052 seconds. Average reconstruction error is: 5.9541
 epoch 8/50. Took 0.37112 seconds. Average reconstruction error is: 5.8244
 epoch 9/50. Took 0.37193 seconds. Average reconstruction error is: 5.7083
 epoch 10/50. Took 0.36964 seconds. Average reconstruction error is: 5.6507
 epoch 11/50. Took 0.36798 seconds. Average reconstruction error is: 5.5243
 epoch 12/50. Took 0.36413 seconds. Average reconstruction error is: 5.4574
 epoch 13/50. Took 0.37311 seconds. Average reconstruction error is: 5.4052
 epoch 14/50. Took 0.36837 seconds. Average reconstruction error is: 5.3802
 epoch 15/50. Took 0.36717 seconds. Average reconstruction error is: 5.287
 epoch 16/50. Took 0.37252 seconds. Average reconstruction error is: 5.2843
 epoch 17/50. Took 0.36847 seconds. Average reconstruction error is: 5.2345
 epoch 18/50. Took 0.3748 seconds. Average reconstruction error is: 5.2368
 epoch 19/50. Took 0.37145 seconds. Average reconstruction error is: 5.1758
 epoch 20/50. Took 0.37061 seconds. Average reconstruction error is: 5.1436
 epoch 21/50. Took 0.37055 seconds. Average reconstruction error is: 5.1083
 epoch 22/50. Took 0.37227 seconds. Average reconstruction error is: 5.136
 epoch 23/50. Took 0.36848 seconds. Average reconstruction error is: 5.1052
 epoch 24/50. Took 0.37279 seconds. Average reconstruction error is: 5.0363
 epoch 25/50. Took 0.36733 seconds. Average reconstruction error is: 5.0151
 epoch 26/50. Took 0.36811 seconds. Average reconstruction error is: 5.0085
 epoch 27/50. Took 0.36883 seconds. Average reconstruction error is: 4.9998
 epoch 28/50. Took 0.36899 seconds. Average reconstruction error is: 5.01
 epoch 29/50. Took 0.37448 seconds. Average reconstruction error is: 4.9445
 epoch 30/50. Took 0.37213 seconds. Average reconstruction error is: 4.9646
 epoch 31/50. Took 0.36866 seconds. Average reconstruction error is: 4.8876
 epoch 32/50. Took 0.37185 seconds. Average reconstruction error is: 4.9006
 epoch 33/50. Took 0.37099 seconds. Average reconstruction error is: 4.8527
 epoch 34/50. Took 0.36979 seconds. Average reconstruction error is: 4.8844
 epoch 35/50. Took 0.36662 seconds. Average reconstruction error is: 4.8577
 epoch 36/50. Took 0.36872 seconds. Average reconstruction error is: 4.8564
 epoch 37/50. Took 0.36998 seconds. Average reconstruction error is: 4.8123
 epoch 38/50. Took 0.37063 seconds. Average reconstruction error is: 4.82
 epoch 39/50. Took 0.37034 seconds. Average reconstruction error is: 4.8077
 epoch 40/50. Took 0.36821 seconds. Average reconstruction error is: 4.8259
 epoch 41/50. Took 0.37298 seconds. Average reconstruction error is: 4.7659
 epoch 42/50. Took 0.37016 seconds. Average reconstruction error is: 4.7783
 epoch 43/50. Took 0.36774 seconds. Average reconstruction error is: 4.7643
 epoch 44/50. Took 0.37078 seconds. Average reconstruction error is: 4.7414
 epoch 45/50. Took 0.36722 seconds. Average reconstruction error is: 4.7455
 epoch 46/50. Took 0.37055 seconds. Average reconstruction error is: 4.6735
 epoch 47/50. Took 0.36774 seconds. Average reconstruction error is: 4.6858
 epoch 48/50. Took 0.36988 seconds. Average reconstruction error is: 4.6828
 epoch 49/50. Took 0.37357 seconds. Average reconstruction error is: 4.6801
 epoch 50/50. Took 0.37282 seconds. Average reconstruction error is: 4.6626
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.85629 seconds. 
  Full-batch training loss = 0.385617, test loss = 0.393645
  Training set accuracy = 0.905000, Test set accuracy = 0.898500
 epoch 2/100. Took 0.86775 seconds. 
  Full-batch training loss = 0.298140, test loss = 0.313945
  Training set accuracy = 0.923800, Test set accuracy = 0.914600
 epoch 3/100. Took 0.87477 seconds. 
  Full-batch training loss = 0.255839, test loss = 0.282308
  Training set accuracy = 0.931200, Test set accuracy = 0.920600
 epoch 4/100. Took 0.86894 seconds. 
  Full-batch training loss = 0.227901, test loss = 0.261562
  Training set accuracy = 0.938000, Test set accuracy = 0.926600
 epoch 5/100. Took 0.86629 seconds. 
  Full-batch training loss = 0.206683, test loss = 0.246862
  Training set accuracy = 0.942800, Test set accuracy = 0.928500
 epoch 6/100. Took 0.86909 seconds. 
  Full-batch training loss = 0.189835, test loss = 0.238292
  Training set accuracy = 0.949000, Test set accuracy = 0.932400
 epoch 7/100. Took 0.87766 seconds. 
  Full-batch training loss = 0.176574, test loss = 0.228828
  Training set accuracy = 0.951000, Test set accuracy = 0.933800
 epoch 8/100. Took 0.86873 seconds. 
  Full-batch training loss = 0.162302, test loss = 0.219786
  Training set accuracy = 0.956000, Test set accuracy = 0.936300
 epoch 9/100. Took 0.87583 seconds. 
  Full-batch training loss = 0.153637, test loss = 0.215798
  Training set accuracy = 0.959200, Test set accuracy = 0.935800
 epoch 10/100. Took 0.87228 seconds. 
  Full-batch training loss = 0.143308, test loss = 0.211888
  Training set accuracy = 0.963000, Test set accuracy = 0.937500
 epoch 11/100. Took 0.86858 seconds. 
  Full-batch training loss = 0.133291, test loss = 0.206278
  Training set accuracy = 0.965800, Test set accuracy = 0.938800
 epoch 12/100. Took 0.86832 seconds. 
  Full-batch training loss = 0.124500, test loss = 0.203813
  Training set accuracy = 0.971600, Test set accuracy = 0.940600
 epoch 13/100. Took 0.87171 seconds. 
  Full-batch training loss = 0.119697, test loss = 0.201205
  Training set accuracy = 0.973000, Test set accuracy = 0.940000
 epoch 14/100. Took 0.86859 seconds. 
  Full-batch training loss = 0.110590, test loss = 0.195926
  Training set accuracy = 0.975000, Test set accuracy = 0.942900
 epoch 15/100. Took 0.87419 seconds. 
  Full-batch training loss = 0.104786, test loss = 0.193418
  Training set accuracy = 0.976800, Test set accuracy = 0.941400
 epoch 16/100. Took 0.87283 seconds. 
  Full-batch training loss = 0.098511, test loss = 0.192519
  Training set accuracy = 0.979200, Test set accuracy = 0.943000
 epoch 17/100. Took 0.86654 seconds. 
  Full-batch training loss = 0.095409, test loss = 0.193200
  Training set accuracy = 0.980400, Test set accuracy = 0.942700
 epoch 18/100. Took 0.87259 seconds. 
  Full-batch training loss = 0.089830, test loss = 0.190116
  Training set accuracy = 0.982600, Test set accuracy = 0.942400
 epoch 19/100. Took 0.86493 seconds. 
  Full-batch training loss = 0.084249, test loss = 0.188858
  Training set accuracy = 0.983600, Test set accuracy = 0.943100
 epoch 20/100. Took 0.87495 seconds. 
  Full-batch training loss = 0.080670, test loss = 0.187564
  Training set accuracy = 0.984800, Test set accuracy = 0.944600
 epoch 21/100. Took 0.85365 seconds. 
  Full-batch training loss = 0.076273, test loss = 0.185289
  Training set accuracy = 0.987000, Test set accuracy = 0.944400
 epoch 22/100. Took 0.86705 seconds. 
  Full-batch training loss = 0.073193, test loss = 0.186078
  Training set accuracy = 0.987400, Test set accuracy = 0.945200
 epoch 23/100. Took 0.86511 seconds. 
  Full-batch training loss = 0.070569, test loss = 0.184958
  Training set accuracy = 0.988600, Test set accuracy = 0.944500
 epoch 24/100. Took 0.87153 seconds. 
  Full-batch training loss = 0.066504, test loss = 0.183873
  Training set accuracy = 0.989400, Test set accuracy = 0.945800
 epoch 25/100. Took 0.86827 seconds. 
  Full-batch training loss = 0.062989, test loss = 0.182521
  Training set accuracy = 0.990600, Test set accuracy = 0.945800
 epoch 26/100. Took 0.86732 seconds. 
  Full-batch training loss = 0.060047, test loss = 0.182202
  Training set accuracy = 0.991000, Test set accuracy = 0.945600
 epoch 27/100. Took 0.86134 seconds. 
  Full-batch training loss = 0.057198, test loss = 0.181945
  Training set accuracy = 0.992200, Test set accuracy = 0.946000
 epoch 28/100. Took 0.8649 seconds. 
  Full-batch training loss = 0.055003, test loss = 0.181570
  Training set accuracy = 0.992600, Test set accuracy = 0.946300
 epoch 29/100. Took 0.86288 seconds. 
  Full-batch training loss = 0.053444, test loss = 0.182968
  Training set accuracy = 0.991400, Test set accuracy = 0.945400
 epoch 30/100. Took 0.86694 seconds. 
  Full-batch training loss = 0.050084, test loss = 0.180335
  Training set accuracy = 0.993600, Test set accuracy = 0.946600
 epoch 31/100. Took 0.88567 seconds. 
  Full-batch training loss = 0.048438, test loss = 0.180686
  Training set accuracy = 0.993600, Test set accuracy = 0.946600
 epoch 32/100. Took 0.8687 seconds. 
  Full-batch training loss = 0.046408, test loss = 0.181676
  Training set accuracy = 0.993400, Test set accuracy = 0.946200
 epoch 33/100. Took 0.86684 seconds. 
  Full-batch training loss = 0.044494, test loss = 0.179967
  Training set accuracy = 0.994600, Test set accuracy = 0.947800
 epoch 34/100. Took 0.87475 seconds. 
  Full-batch training loss = 0.042169, test loss = 0.180100
  Training set accuracy = 0.994600, Test set accuracy = 0.947700
 epoch 35/100. Took 0.87455 seconds. 
  Full-batch training loss = 0.040257, test loss = 0.178900
  Training set accuracy = 0.995400, Test set accuracy = 0.947600
 epoch 36/100. Took 0.86979 seconds. 
  Full-batch training loss = 0.038635, test loss = 0.179255
  Training set accuracy = 0.995000, Test set accuracy = 0.948000
 epoch 37/100. Took 0.86831 seconds. 
  Full-batch training loss = 0.037303, test loss = 0.180698
  Training set accuracy = 0.995400, Test set accuracy = 0.947500
 epoch 38/100. Took 0.85854 seconds. 
  Full-batch training loss = 0.035714, test loss = 0.179088
  Training set accuracy = 0.996200, Test set accuracy = 0.948200
 epoch 39/100. Took 0.86586 seconds. 
  Full-batch training loss = 0.034378, test loss = 0.179294
  Training set accuracy = 0.996800, Test set accuracy = 0.947800
 epoch 40/100. Took 0.85743 seconds. 
  Full-batch training loss = 0.032883, test loss = 0.179628
  Training set accuracy = 0.996600, Test set accuracy = 0.947900
 epoch 41/100. Took 0.85421 seconds. 
  Full-batch training loss = 0.031607, test loss = 0.179054
  Training set accuracy = 0.997200, Test set accuracy = 0.949000
 epoch 42/100. Took 0.86902 seconds. 
  Full-batch training loss = 0.030489, test loss = 0.178950
  Training set accuracy = 0.997000, Test set accuracy = 0.948400
 epoch 43/100. Took 0.86382 seconds. 
  Full-batch training loss = 0.029332, test loss = 0.179950
  Training set accuracy = 0.997800, Test set accuracy = 0.948300
 epoch 44/100. Took 0.88128 seconds. 
  Full-batch training loss = 0.028276, test loss = 0.179878
  Training set accuracy = 0.998200, Test set accuracy = 0.948500
 epoch 45/100. Took 0.87068 seconds. 
  Full-batch training loss = 0.027447, test loss = 0.180232
  Training set accuracy = 0.998200, Test set accuracy = 0.948000
 epoch 46/100. Took 0.87808 seconds. 
  Full-batch training loss = 0.026276, test loss = 0.179754
  Training set accuracy = 0.998400, Test set accuracy = 0.949300
 epoch 47/100. Took 0.86898 seconds. 
  Full-batch training loss = 0.025338, test loss = 0.180246
  Training set accuracy = 0.998400, Test set accuracy = 0.949100
 epoch 48/100. Took 0.85832 seconds. 
  Full-batch training loss = 0.024576, test loss = 0.179870
  Training set accuracy = 0.998600, Test set accuracy = 0.948300
 epoch 49/100. Took 0.87175 seconds. 
  Full-batch training loss = 0.023819, test loss = 0.180468
  Training set accuracy = 0.999000, Test set accuracy = 0.947900
 epoch 50/100. Took 0.86425 seconds. 
  Full-batch training loss = 0.022905, test loss = 0.180109
  Training set accuracy = 0.999000, Test set accuracy = 0.948900
 epoch 51/100. Took 0.86683 seconds. 
  Full-batch training loss = 0.021992, test loss = 0.179864
  Training set accuracy = 0.998800, Test set accuracy = 0.949100
 epoch 52/100. Took 0.87663 seconds. 
  Full-batch training loss = 0.021502, test loss = 0.180641
  Training set accuracy = 0.999000, Test set accuracy = 0.948700
 epoch 53/100. Took 0.86917 seconds. 
  Full-batch training loss = 0.020832, test loss = 0.181194
  Training set accuracy = 0.999400, Test set accuracy = 0.948600
 epoch 54/100. Took 0.86661 seconds. 
  Full-batch training loss = 0.020069, test loss = 0.180325
  Training set accuracy = 0.999400, Test set accuracy = 0.948600
 epoch 55/100. Took 0.87662 seconds. 
  Full-batch training loss = 0.019529, test loss = 0.181302
  Training set accuracy = 0.999600, Test set accuracy = 0.948600
 epoch 56/100. Took 0.87665 seconds. 
  Full-batch training loss = 0.018840, test loss = 0.180633
  Training set accuracy = 0.999600, Test set accuracy = 0.949100
 epoch 57/100. Took 0.85606 seconds. 
  Full-batch training loss = 0.018279, test loss = 0.180761
  Training set accuracy = 0.999600, Test set accuracy = 0.948700
 epoch 58/100. Took 0.87542 seconds. 
  Full-batch training loss = 0.017750, test loss = 0.181209
  Training set accuracy = 0.999600, Test set accuracy = 0.949000
 epoch 59/100. Took 0.86181 seconds. 
  Full-batch training loss = 0.017174, test loss = 0.181485
  Training set accuracy = 0.999600, Test set accuracy = 0.948700
 epoch 60/100. Took 0.87099 seconds. 
  Full-batch training loss = 0.016761, test loss = 0.181656
  Training set accuracy = 0.999600, Test set accuracy = 0.948700
 epoch 61/100. Took 0.96317 seconds. 
  Full-batch training loss = 0.016329, test loss = 0.181448
  Training set accuracy = 0.999800, Test set accuracy = 0.948900
 epoch 62/100. Took 0.87483 seconds. 
  Full-batch training loss = 0.015796, test loss = 0.181629
  Training set accuracy = 0.999600, Test set accuracy = 0.949000
 epoch 63/100. Took 0.86346 seconds. 
  Full-batch training loss = 0.015394, test loss = 0.182180
  Training set accuracy = 1.000000, Test set accuracy = 0.949000
 epoch 64/100. Took 0.87576 seconds. 
  Full-batch training loss = 0.014952, test loss = 0.181979
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 65/100. Took 0.88696 seconds. 
  Full-batch training loss = 0.014612, test loss = 0.182725
  Training set accuracy = 1.000000, Test set accuracy = 0.949100
 epoch 66/100. Took 0.87362 seconds. 
  Full-batch training loss = 0.014216, test loss = 0.182294
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 67/100. Took 0.86871 seconds. 
  Full-batch training loss = 0.013862, test loss = 0.182631
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 68/100. Took 0.85578 seconds. 
  Full-batch training loss = 0.013559, test loss = 0.183106
  Training set accuracy = 1.000000, Test set accuracy = 0.949200
 epoch 69/100. Took 0.87338 seconds. 
  Full-batch training loss = 0.013199, test loss = 0.183124
  Training set accuracy = 1.000000, Test set accuracy = 0.949200
 epoch 70/100. Took 0.87178 seconds. 
  Full-batch training loss = 0.012897, test loss = 0.183010
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 71/100. Took 0.87048 seconds. 
  Full-batch training loss = 0.012564, test loss = 0.183088
  Training set accuracy = 1.000000, Test set accuracy = 0.949300
 epoch 72/100. Took 0.87368 seconds. 
  Full-batch training loss = 0.012290, test loss = 0.183223
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 73/100. Took 0.88565 seconds. 
  Full-batch training loss = 0.012031, test loss = 0.183630
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 74/100. Took 0.87071 seconds. 
  Full-batch training loss = 0.011807, test loss = 0.184174
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 75/100. Took 0.87458 seconds. 
  Full-batch training loss = 0.011469, test loss = 0.183944
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 76/100. Took 0.86141 seconds. 
  Full-batch training loss = 0.011248, test loss = 0.184034
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 77/100. Took 0.87912 seconds. 
  Full-batch training loss = 0.011004, test loss = 0.184211
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 78/100. Took 0.86422 seconds. 
  Full-batch training loss = 0.010829, test loss = 0.184927
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 79/100. Took 0.86089 seconds. 
  Full-batch training loss = 0.010538, test loss = 0.184994
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 80/100. Took 0.87047 seconds. 
  Full-batch training loss = 0.010320, test loss = 0.185114
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 81/100. Took 0.87584 seconds. 
  Full-batch training loss = 0.010124, test loss = 0.184980
  Training set accuracy = 1.000000, Test set accuracy = 0.949700
 epoch 82/100. Took 0.87394 seconds. 
  Full-batch training loss = 0.009927, test loss = 0.185388
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 83/100. Took 0.85948 seconds. 
  Full-batch training loss = 0.009715, test loss = 0.185346
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 84/100. Took 0.85841 seconds. 
  Full-batch training loss = 0.009547, test loss = 0.185977
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 85/100. Took 0.86175 seconds. 
  Full-batch training loss = 0.009353, test loss = 0.185815
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 86/100. Took 0.86709 seconds. 
  Full-batch training loss = 0.009182, test loss = 0.186259
  Training set accuracy = 1.000000, Test set accuracy = 0.949700
 epoch 87/100. Took 0.86737 seconds. 
  Full-batch training loss = 0.009014, test loss = 0.186019
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 88/100. Took 0.8757 seconds. 
  Full-batch training loss = 0.008845, test loss = 0.186274
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 89/100. Took 0.86987 seconds. 
  Full-batch training loss = 0.008706, test loss = 0.186509
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 90/100. Took 0.86578 seconds. 
  Full-batch training loss = 0.008532, test loss = 0.186845
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 91/100. Took 0.86834 seconds. 
  Full-batch training loss = 0.008387, test loss = 0.186810
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 92/100. Took 0.87384 seconds. 
  Full-batch training loss = 0.008236, test loss = 0.186955
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 93/100. Took 0.86789 seconds. 
  Full-batch training loss = 0.008097, test loss = 0.186985
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 94/100. Took 0.87106 seconds. 
  Full-batch training loss = 0.007983, test loss = 0.187301
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 95/100. Took 0.87179 seconds. 
  Full-batch training loss = 0.007826, test loss = 0.187273
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 96/100. Took 0.86172 seconds. 
  Full-batch training loss = 0.007704, test loss = 0.187424
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 97/100. Took 0.85987 seconds. 
  Full-batch training loss = 0.007582, test loss = 0.187468
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 98/100. Took 0.87271 seconds. 
  Full-batch training loss = 0.007457, test loss = 0.187887
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 99/100. Took 0.85351 seconds. 
  Full-batch training loss = 0.007338, test loss = 0.188135
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 100/100. Took 0.86737 seconds. 
  Full-batch training loss = 0.007223, test loss = 0.188063
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
Elapsed time is 260.347442 seconds.
End Training
