==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 1/270
----------------------------------------------------------
* ReducedData	2000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 2000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.23583 seconds. Average reconstruction error is: 49.5178
 epoch 2/20. Took 0.21033 seconds. Average reconstruction error is: 26.6586
 epoch 3/20. Took 0.20883 seconds. Average reconstruction error is: 21.9766
 epoch 4/20. Took 0.20647 seconds. Average reconstruction error is: 19.6409
 epoch 5/20. Took 0.20893 seconds. Average reconstruction error is: 17.954
 epoch 6/20. Took 0.21091 seconds. Average reconstruction error is: 16.7577
 epoch 7/20. Took 0.22436 seconds. Average reconstruction error is: 15.851
 epoch 8/20. Took 0.20948 seconds. Average reconstruction error is: 15.1505
 epoch 9/20. Took 0.21169 seconds. Average reconstruction error is: 14.4214
 epoch 10/20. Took 0.21031 seconds. Average reconstruction error is: 13.9091
 epoch 11/20. Took 0.20653 seconds. Average reconstruction error is: 13.4493
 epoch 12/20. Took 0.19349 seconds. Average reconstruction error is: 12.9513
 epoch 13/20. Took 0.19035 seconds. Average reconstruction error is: 12.5677
 epoch 14/20. Took 0.19392 seconds. Average reconstruction error is: 12.2044
 epoch 15/20. Took 0.19192 seconds. Average reconstruction error is: 11.9415
 epoch 16/20. Took 0.19646 seconds. Average reconstruction error is: 11.6555
 epoch 17/20. Took 0.1951 seconds. Average reconstruction error is: 11.3587
 epoch 18/20. Took 0.1941 seconds. Average reconstruction error is: 11.1437
 epoch 19/20. Took 0.19325 seconds. Average reconstruction error is: 10.8458
 epoch 20/20. Took 0.19284 seconds. Average reconstruction error is: 10.6889
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.13742 seconds. Average reconstruction error is: 28.627
 epoch 2/20. Took 0.13091 seconds. Average reconstruction error is: 13.4183
 epoch 3/20. Took 0.12885 seconds. Average reconstruction error is: 10.3457
 epoch 4/20. Took 0.1319 seconds. Average reconstruction error is: 8.8178
 epoch 5/20. Took 0.1272 seconds. Average reconstruction error is: 7.8293
 epoch 6/20. Took 0.13092 seconds. Average reconstruction error is: 7.2687
 epoch 7/20. Took 0.12969 seconds. Average reconstruction error is: 6.8335
 epoch 8/20. Took 0.13064 seconds. Average reconstruction error is: 6.5135
 epoch 9/20. Took 0.12944 seconds. Average reconstruction error is: 6.2157
 epoch 10/20. Took 0.13099 seconds. Average reconstruction error is: 6.0336
 epoch 11/20. Took 0.13089 seconds. Average reconstruction error is: 5.839
 epoch 12/20. Took 0.13033 seconds. Average reconstruction error is: 5.6728
 epoch 13/20. Took 0.1285 seconds. Average reconstruction error is: 5.5137
 epoch 14/20. Took 0.12833 seconds. Average reconstruction error is: 5.4509
 epoch 15/20. Took 0.13109 seconds. Average reconstruction error is: 5.3927
 epoch 16/20. Took 0.13021 seconds. Average reconstruction error is: 5.2746
 epoch 17/20. Took 0.12906 seconds. Average reconstruction error is: 5.1808
 epoch 18/20. Took 0.13117 seconds. Average reconstruction error is: 5.1312
 epoch 19/20. Took 0.12832 seconds. Average reconstruction error is: 5.0558
 epoch 20/20. Took 0.1332 seconds. Average reconstruction error is: 5.0086
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.13435 seconds. Average reconstruction error is: 25.3238
 epoch 2/20. Took 0.13254 seconds. Average reconstruction error is: 10.3349
 epoch 3/20. Took 0.13312 seconds. Average reconstruction error is: 8.0459
 epoch 4/20. Took 0.13908 seconds. Average reconstruction error is: 6.9292
 epoch 5/20. Took 0.13718 seconds. Average reconstruction error is: 6.2975
 epoch 6/20. Took 0.14406 seconds. Average reconstruction error is: 5.8546
 epoch 7/20. Took 0.14186 seconds. Average reconstruction error is: 5.5342
 epoch 8/20. Took 0.13509 seconds. Average reconstruction error is: 5.3612
 epoch 9/20. Took 0.13617 seconds. Average reconstruction error is: 5.1546
 epoch 10/20. Took 0.13643 seconds. Average reconstruction error is: 5.0891
 epoch 11/20. Took 0.13482 seconds. Average reconstruction error is: 4.9488
 epoch 12/20. Took 0.13656 seconds. Average reconstruction error is: 4.9421
 epoch 13/20. Took 0.13451 seconds. Average reconstruction error is: 4.8498
 epoch 14/20. Took 0.13359 seconds. Average reconstruction error is: 4.8042
 epoch 15/20. Took 0.13574 seconds. Average reconstruction error is: 4.76
 epoch 16/20. Took 0.13337 seconds. Average reconstruction error is: 4.7037
 epoch 17/20. Took 0.13295 seconds. Average reconstruction error is: 4.6313
 epoch 18/20. Took 0.13096 seconds. Average reconstruction error is: 4.6521
 epoch 19/20. Took 0.13425 seconds. Average reconstruction error is: 4.5955
 epoch 20/20. Took 0.13289 seconds. Average reconstruction error is: 4.5754
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.31905 seconds. 
  Full-batch training loss = 0.460438, test loss = 0.493126
  Training set accuracy = 0.872500, Test set accuracy = 0.857000
 epoch 2/100. Took 0.30935 seconds. 
  Full-batch training loss = 0.344665, test loss = 0.394373
  Training set accuracy = 0.904500, Test set accuracy = 0.882500
 epoch 3/100. Took 0.30689 seconds. 
  Full-batch training loss = 0.285554, test loss = 0.346379
  Training set accuracy = 0.923000, Test set accuracy = 0.899900
 epoch 4/100. Took 0.3021 seconds. 
  Full-batch training loss = 0.245818, test loss = 0.325230
  Training set accuracy = 0.927500, Test set accuracy = 0.900500
 epoch 5/100. Took 0.31298 seconds. 
  Full-batch training loss = 0.212547, test loss = 0.301844
  Training set accuracy = 0.941500, Test set accuracy = 0.910600
 epoch 6/100. Took 0.30656 seconds. 
  Full-batch training loss = 0.200673, test loss = 0.300368
  Training set accuracy = 0.947500, Test set accuracy = 0.910700
 epoch 7/100. Took 0.30526 seconds. 
  Full-batch training loss = 0.171759, test loss = 0.288067
  Training set accuracy = 0.955500, Test set accuracy = 0.911900
 epoch 8/100. Took 0.30671 seconds. 
  Full-batch training loss = 0.161269, test loss = 0.285956
  Training set accuracy = 0.959500, Test set accuracy = 0.915000
 epoch 9/100. Took 0.30956 seconds. 
  Full-batch training loss = 0.140011, test loss = 0.280251
  Training set accuracy = 0.968500, Test set accuracy = 0.913000
 epoch 10/100. Took 0.30053 seconds. 
  Full-batch training loss = 0.125613, test loss = 0.271846
  Training set accuracy = 0.973000, Test set accuracy = 0.916900
 epoch 11/100. Took 0.3039 seconds. 
  Full-batch training loss = 0.113457, test loss = 0.269033
  Training set accuracy = 0.975500, Test set accuracy = 0.916300
 epoch 12/100. Took 0.29819 seconds. 
  Full-batch training loss = 0.105022, test loss = 0.269751
  Training set accuracy = 0.980500, Test set accuracy = 0.917200
 epoch 13/100. Took 0.3021 seconds. 
  Full-batch training loss = 0.096266, test loss = 0.264076
  Training set accuracy = 0.981000, Test set accuracy = 0.919300
 epoch 14/100. Took 0.3047 seconds. 
  Full-batch training loss = 0.087480, test loss = 0.263282
  Training set accuracy = 0.985000, Test set accuracy = 0.918500
 epoch 15/100. Took 0.32883 seconds. 
  Full-batch training loss = 0.080560, test loss = 0.261779
  Training set accuracy = 0.989000, Test set accuracy = 0.920600
 epoch 16/100. Took 0.30807 seconds. 
  Full-batch training loss = 0.072695, test loss = 0.260364
  Training set accuracy = 0.992000, Test set accuracy = 0.918700
 epoch 17/100. Took 0.30716 seconds. 
  Full-batch training loss = 0.066816, test loss = 0.259464
  Training set accuracy = 0.992500, Test set accuracy = 0.920300
 epoch 18/100. Took 0.30201 seconds. 
  Full-batch training loss = 0.063538, test loss = 0.258880
  Training set accuracy = 0.993500, Test set accuracy = 0.921500
 epoch 19/100. Took 0.30448 seconds. 
  Full-batch training loss = 0.060043, test loss = 0.264382
  Training set accuracy = 0.994000, Test set accuracy = 0.919700
 epoch 20/100. Took 0.30155 seconds. 
  Full-batch training loss = 0.053271, test loss = 0.258729
  Training set accuracy = 0.996500, Test set accuracy = 0.920700
 epoch 21/100. Took 0.30657 seconds. 
  Full-batch training loss = 0.049970, test loss = 0.259273
  Training set accuracy = 0.996500, Test set accuracy = 0.921500
 epoch 22/100. Took 0.30654 seconds. 
  Full-batch training loss = 0.046440, test loss = 0.259750
  Training set accuracy = 0.997500, Test set accuracy = 0.920800
 epoch 23/100. Took 0.30215 seconds. 
  Full-batch training loss = 0.043197, test loss = 0.260546
  Training set accuracy = 0.997500, Test set accuracy = 0.922200
 epoch 24/100. Took 0.30316 seconds. 
  Full-batch training loss = 0.040963, test loss = 0.260416
  Training set accuracy = 0.998000, Test set accuracy = 0.922100
 epoch 25/100. Took 0.30404 seconds. 
  Full-batch training loss = 0.037780, test loss = 0.261402
  Training set accuracy = 0.999000, Test set accuracy = 0.921500
 epoch 26/100. Took 0.30126 seconds. 
  Full-batch training loss = 0.035838, test loss = 0.260656
  Training set accuracy = 0.999500, Test set accuracy = 0.923100
 epoch 27/100. Took 0.30368 seconds. 
  Full-batch training loss = 0.033904, test loss = 0.260023
  Training set accuracy = 0.999000, Test set accuracy = 0.922600
 epoch 28/100. Took 0.30813 seconds. 
  Full-batch training loss = 0.032067, test loss = 0.259776
  Training set accuracy = 0.999000, Test set accuracy = 0.923500
 epoch 29/100. Took 0.30372 seconds. 
  Full-batch training loss = 0.029799, test loss = 0.261941
  Training set accuracy = 0.999500, Test set accuracy = 0.923100
 epoch 30/100. Took 0.30694 seconds. 
  Full-batch training loss = 0.028561, test loss = 0.264503
  Training set accuracy = 0.999500, Test set accuracy = 0.922300
 epoch 31/100. Took 0.30496 seconds. 
  Full-batch training loss = 0.026813, test loss = 0.262940
  Training set accuracy = 1.000000, Test set accuracy = 0.923200
 epoch 32/100. Took 0.30906 seconds. 
  Full-batch training loss = 0.025489, test loss = 0.263842
  Training set accuracy = 0.999500, Test set accuracy = 0.923700
 epoch 33/100. Took 0.32816 seconds. 
  Full-batch training loss = 0.024519, test loss = 0.262513
  Training set accuracy = 1.000000, Test set accuracy = 0.924400
 epoch 34/100. Took 0.3059 seconds. 
  Full-batch training loss = 0.023053, test loss = 0.264248
  Training set accuracy = 1.000000, Test set accuracy = 0.923300
 epoch 35/100. Took 0.30071 seconds. 
  Full-batch training loss = 0.022024, test loss = 0.264782
  Training set accuracy = 1.000000, Test set accuracy = 0.923600
 epoch 36/100. Took 0.3029 seconds. 
  Full-batch training loss = 0.021211, test loss = 0.267206
  Training set accuracy = 1.000000, Test set accuracy = 0.922900
 epoch 37/100. Took 0.3048 seconds. 
  Full-batch training loss = 0.020189, test loss = 0.264846
  Training set accuracy = 1.000000, Test set accuracy = 0.924200
 epoch 38/100. Took 0.30373 seconds. 
  Full-batch training loss = 0.019426, test loss = 0.265480
  Training set accuracy = 1.000000, Test set accuracy = 0.924200
 epoch 39/100. Took 0.30609 seconds. 
  Full-batch training loss = 0.018559, test loss = 0.266865
  Training set accuracy = 1.000000, Test set accuracy = 0.924500
 epoch 40/100. Took 0.30507 seconds. 
  Full-batch training loss = 0.017891, test loss = 0.265055
  Training set accuracy = 1.000000, Test set accuracy = 0.924700
 epoch 41/100. Took 0.30503 seconds. 
  Full-batch training loss = 0.017190, test loss = 0.266587
  Training set accuracy = 1.000000, Test set accuracy = 0.924200
 epoch 42/100. Took 0.30638 seconds. 
  Full-batch training loss = 0.016550, test loss = 0.269007
  Training set accuracy = 1.000000, Test set accuracy = 0.924300
 epoch 43/100. Took 0.30389 seconds. 
  Full-batch training loss = 0.015902, test loss = 0.267639
  Training set accuracy = 1.000000, Test set accuracy = 0.924500
 epoch 44/100. Took 0.30485 seconds. 
  Full-batch training loss = 0.015359, test loss = 0.268304
  Training set accuracy = 1.000000, Test set accuracy = 0.924600
 epoch 45/100. Took 0.30277 seconds. 
  Full-batch training loss = 0.014856, test loss = 0.268775
  Training set accuracy = 1.000000, Test set accuracy = 0.924400
 epoch 46/100. Took 0.30267 seconds. 
  Full-batch training loss = 0.014355, test loss = 0.270152
  Training set accuracy = 1.000000, Test set accuracy = 0.925000
 epoch 47/100. Took 0.30342 seconds. 
  Full-batch training loss = 0.013869, test loss = 0.269833
  Training set accuracy = 1.000000, Test set accuracy = 0.925000
 epoch 48/100. Took 0.30125 seconds. 
  Full-batch training loss = 0.013469, test loss = 0.270118
  Training set accuracy = 1.000000, Test set accuracy = 0.924200
 epoch 49/100. Took 0.29981 seconds. 
  Full-batch training loss = 0.013016, test loss = 0.271355
  Training set accuracy = 1.000000, Test set accuracy = 0.924900
 epoch 50/100. Took 0.30365 seconds. 
  Full-batch training loss = 0.012627, test loss = 0.271072
  Training set accuracy = 1.000000, Test set accuracy = 0.925800
 epoch 51/100. Took 0.30704 seconds. 
  Full-batch training loss = 0.012237, test loss = 0.271606
  Training set accuracy = 1.000000, Test set accuracy = 0.925100
 epoch 52/100. Took 0.30531 seconds. 
  Full-batch training loss = 0.011879, test loss = 0.272029
  Training set accuracy = 1.000000, Test set accuracy = 0.925500
 epoch 53/100. Took 0.3098 seconds. 
  Full-batch training loss = 0.011560, test loss = 0.272422
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 54/100. Took 0.30341 seconds. 
  Full-batch training loss = 0.011228, test loss = 0.273029
  Training set accuracy = 1.000000, Test set accuracy = 0.925800
 epoch 55/100. Took 0.32372 seconds. 
  Full-batch training loss = 0.010914, test loss = 0.273569
  Training set accuracy = 1.000000, Test set accuracy = 0.925700
 epoch 56/100. Took 0.30335 seconds. 
  Full-batch training loss = 0.010627, test loss = 0.273814
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 57/100. Took 0.30721 seconds. 
  Full-batch training loss = 0.010354, test loss = 0.273975
  Training set accuracy = 1.000000, Test set accuracy = 0.925600
 epoch 58/100. Took 0.3074 seconds. 
  Full-batch training loss = 0.010072, test loss = 0.274559
  Training set accuracy = 1.000000, Test set accuracy = 0.925700
 epoch 59/100. Took 0.30511 seconds. 
  Full-batch training loss = 0.009813, test loss = 0.274972
  Training set accuracy = 1.000000, Test set accuracy = 0.925700
 epoch 60/100. Took 0.30097 seconds. 
  Full-batch training loss = 0.009587, test loss = 0.275916
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 61/100. Took 0.30316 seconds. 
  Full-batch training loss = 0.009347, test loss = 0.276202
  Training set accuracy = 1.000000, Test set accuracy = 0.926000
 epoch 62/100. Took 0.30055 seconds. 
  Full-batch training loss = 0.009119, test loss = 0.276568
  Training set accuracy = 1.000000, Test set accuracy = 0.926000
 epoch 63/100. Took 0.30345 seconds. 
  Full-batch training loss = 0.008910, test loss = 0.276215
  Training set accuracy = 1.000000, Test set accuracy = 0.926200
 epoch 64/100. Took 0.2976 seconds. 
  Full-batch training loss = 0.008701, test loss = 0.277206
  Training set accuracy = 1.000000, Test set accuracy = 0.926200
 epoch 65/100. Took 0.30592 seconds. 
  Full-batch training loss = 0.008519, test loss = 0.277933
  Training set accuracy = 1.000000, Test set accuracy = 0.925400
 epoch 66/100. Took 0.30606 seconds. 
  Full-batch training loss = 0.008318, test loss = 0.277944
  Training set accuracy = 1.000000, Test set accuracy = 0.926200
 epoch 67/100. Took 0.30432 seconds. 
  Full-batch training loss = 0.008134, test loss = 0.278021
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 68/100. Took 0.33463 seconds. 
  Full-batch training loss = 0.007962, test loss = 0.278870
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 69/100. Took 0.30738 seconds. 
  Full-batch training loss = 0.007792, test loss = 0.278877
  Training set accuracy = 1.000000, Test set accuracy = 0.926400
 epoch 70/100. Took 0.30501 seconds. 
  Full-batch training loss = 0.007635, test loss = 0.278930
  Training set accuracy = 1.000000, Test set accuracy = 0.926100
 epoch 71/100. Took 0.30648 seconds. 
  Full-batch training loss = 0.007474, test loss = 0.279495
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 72/100. Took 0.30838 seconds. 
  Full-batch training loss = 0.007326, test loss = 0.280525
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 73/100. Took 0.30608 seconds. 
  Full-batch training loss = 0.007173, test loss = 0.280461
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 74/100. Took 0.30388 seconds. 
  Full-batch training loss = 0.007036, test loss = 0.280763
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 75/100. Took 0.30335 seconds. 
  Full-batch training loss = 0.006906, test loss = 0.281226
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 76/100. Took 0.2987 seconds. 
  Full-batch training loss = 0.006774, test loss = 0.281910
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 77/100. Took 0.30169 seconds. 
  Full-batch training loss = 0.006647, test loss = 0.281838
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 78/100. Took 0.30335 seconds. 
  Full-batch training loss = 0.006524, test loss = 0.282289
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 79/100. Took 0.3091 seconds. 
  Full-batch training loss = 0.006406, test loss = 0.282665
  Training set accuracy = 1.000000, Test set accuracy = 0.926000
 epoch 80/100. Took 0.30484 seconds. 
  Full-batch training loss = 0.006295, test loss = 0.283058
  Training set accuracy = 1.000000, Test set accuracy = 0.926800
 epoch 81/100. Took 0.30829 seconds. 
  Full-batch training loss = 0.006180, test loss = 0.282938
  Training set accuracy = 1.000000, Test set accuracy = 0.926400
 epoch 82/100. Took 0.30256 seconds. 
  Full-batch training loss = 0.006072, test loss = 0.283717
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 83/100. Took 0.3052 seconds. 
  Full-batch training loss = 0.005970, test loss = 0.283486
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 84/100. Took 0.32325 seconds. 
  Full-batch training loss = 0.005868, test loss = 0.284151
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 85/100. Took 0.30897 seconds. 
  Full-batch training loss = 0.005768, test loss = 0.284387
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 86/100. Took 0.3047 seconds. 
  Full-batch training loss = 0.005679, test loss = 0.284617
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 87/100. Took 0.30085 seconds. 
  Full-batch training loss = 0.005589, test loss = 0.285534
  Training set accuracy = 1.000000, Test set accuracy = 0.926800
 epoch 88/100. Took 0.30139 seconds. 
  Full-batch training loss = 0.005494, test loss = 0.285560
  Training set accuracy = 1.000000, Test set accuracy = 0.926200
 epoch 89/100. Took 0.30471 seconds. 
  Full-batch training loss = 0.005407, test loss = 0.285688
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 90/100. Took 0.30585 seconds. 
  Full-batch training loss = 0.005325, test loss = 0.285791
  Training set accuracy = 1.000000, Test set accuracy = 0.926500
 epoch 91/100. Took 0.30384 seconds. 
  Full-batch training loss = 0.005241, test loss = 0.286386
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 92/100. Took 0.41624 seconds. 
  Full-batch training loss = 0.005162, test loss = 0.286693
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 93/100. Took 0.30404 seconds. 
  Full-batch training loss = 0.005085, test loss = 0.286575
  Training set accuracy = 1.000000, Test set accuracy = 0.926300
 epoch 94/100. Took 0.30222 seconds. 
  Full-batch training loss = 0.005009, test loss = 0.287506
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 95/100. Took 0.30206 seconds. 
  Full-batch training loss = 0.004935, test loss = 0.287512
  Training set accuracy = 1.000000, Test set accuracy = 0.926600
 epoch 96/100. Took 0.30496 seconds. 
  Full-batch training loss = 0.004864, test loss = 0.287703
  Training set accuracy = 1.000000, Test set accuracy = 0.926700
 epoch 97/100. Took 0.30248 seconds. 
  Full-batch training loss = 0.004792, test loss = 0.287971
  Training set accuracy = 1.000000, Test set accuracy = 0.926900
 epoch 98/100. Took 0.29688 seconds. 
  Full-batch training loss = 0.004725, test loss = 0.288204
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 99/100. Took 0.30223 seconds. 
  Full-batch training loss = 0.004659, test loss = 0.288741
  Training set accuracy = 1.000000, Test set accuracy = 0.927000
 epoch 100/100. Took 0.30269 seconds. 
  Full-batch training loss = 0.004594, test loss = 0.288881
  Training set accuracy = 1.000000, Test set accuracy = 0.926900
Elapsed time is 118.733721 seconds.
End Training
