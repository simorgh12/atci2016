==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 213/270
----------------------------------------------------------
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.05
* Fine-tunning
	-> Epochs	200
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 1.0849 seconds. Average reconstruction error is: 29.6721
 epoch 2/20. Took 1.088 seconds. Average reconstruction error is: 16.0254
 epoch 3/20. Took 1.0813 seconds. Average reconstruction error is: 13.1826
 epoch 4/20. Took 1.0867 seconds. Average reconstruction error is: 11.7819
 epoch 5/20. Took 1.0786 seconds. Average reconstruction error is: 10.8553
 epoch 6/20. Took 1.0885 seconds. Average reconstruction error is: 10.2389
 epoch 7/20. Took 1.0839 seconds. Average reconstruction error is: 9.7412
 epoch 8/20. Took 1.0843 seconds. Average reconstruction error is: 9.3513
 epoch 9/20. Took 1.0935 seconds. Average reconstruction error is: 9.094
 epoch 10/20. Took 1.0934 seconds. Average reconstruction error is: 8.8399
 epoch 11/20. Took 1.0824 seconds. Average reconstruction error is: 8.6447
 epoch 12/20. Took 1.078 seconds. Average reconstruction error is: 8.4671
 epoch 13/20. Took 1.0909 seconds. Average reconstruction error is: 8.3515
 epoch 14/20. Took 1.0851 seconds. Average reconstruction error is: 8.1664
 epoch 15/20. Took 1.0839 seconds. Average reconstruction error is: 8.1077
 epoch 16/20. Took 1.0826 seconds. Average reconstruction error is: 8.0321
 epoch 17/20. Took 1.1094 seconds. Average reconstruction error is: 7.9082
 epoch 18/20. Took 1.1255 seconds. Average reconstruction error is: 7.8719
 epoch 19/20. Took 1.2162 seconds. Average reconstruction error is: 7.8162
 epoch 20/20. Took 1.2273 seconds. Average reconstruction error is: 7.7299
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.79522 seconds. Average reconstruction error is: 30.0377
 epoch 2/20. Took 0.78322 seconds. Average reconstruction error is: 16.068
 epoch 3/20. Took 0.76343 seconds. Average reconstruction error is: 13.2575
 epoch 4/20. Took 0.79934 seconds. Average reconstruction error is: 12.0649
 epoch 5/20. Took 0.86318 seconds. Average reconstruction error is: 11.405
 epoch 6/20. Took 0.79515 seconds. Average reconstruction error is: 10.9324
 epoch 7/20. Took 0.79346 seconds. Average reconstruction error is: 10.5757
 epoch 8/20. Took 0.9164 seconds. Average reconstruction error is: 10.3223
 epoch 9/20. Took 0.94185 seconds. Average reconstruction error is: 10.1187
 epoch 10/20. Took 0.85457 seconds. Average reconstruction error is: 9.9189
 epoch 11/20. Took 0.88437 seconds. Average reconstruction error is: 9.7433
 epoch 12/20. Took 0.90835 seconds. Average reconstruction error is: 9.5885
 epoch 13/20. Took 0.76717 seconds. Average reconstruction error is: 9.5022
 epoch 14/20. Took 0.77671 seconds. Average reconstruction error is: 9.3771
 epoch 15/20. Took 0.76543 seconds. Average reconstruction error is: 9.2931
 epoch 16/20. Took 0.76221 seconds. Average reconstruction error is: 9.2027
 epoch 17/20. Took 0.77045 seconds. Average reconstruction error is: 9.0941
 epoch 18/20. Took 0.77426 seconds. Average reconstruction error is: 8.9499
 epoch 19/20. Took 0.76874 seconds. Average reconstruction error is: 8.8238
 epoch 20/20. Took 0.77829 seconds. Average reconstruction error is: 8.8
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.76804 seconds. Average reconstruction error is: 22.7443
 epoch 2/20. Took 0.76526 seconds. Average reconstruction error is: 11.0193
 epoch 3/20. Took 0.80942 seconds. Average reconstruction error is: 8.9444
 epoch 4/20. Took 0.77992 seconds. Average reconstruction error is: 8.0212
 epoch 5/20. Took 0.77342 seconds. Average reconstruction error is: 7.5567
 epoch 6/20. Took 0.76997 seconds. Average reconstruction error is: 7.2825
 epoch 7/20. Took 0.76393 seconds. Average reconstruction error is: 7.1155
 epoch 8/20. Took 0.76883 seconds. Average reconstruction error is: 6.9475
 epoch 9/20. Took 0.7635 seconds. Average reconstruction error is: 6.8846
 epoch 10/20. Took 0.76441 seconds. Average reconstruction error is: 6.7886
 epoch 11/20. Took 0.75999 seconds. Average reconstruction error is: 6.7023
 epoch 12/20. Took 0.76631 seconds. Average reconstruction error is: 6.6722
 epoch 13/20. Took 0.76742 seconds. Average reconstruction error is: 6.569
 epoch 14/20. Took 0.76179 seconds. Average reconstruction error is: 6.54
 epoch 15/20. Took 0.77087 seconds. Average reconstruction error is: 6.5263
 epoch 16/20. Took 0.77284 seconds. Average reconstruction error is: 6.5188
 epoch 17/20. Took 0.76021 seconds. Average reconstruction error is: 6.4628
 epoch 18/20. Took 0.76847 seconds. Average reconstruction error is: 6.4315
 epoch 19/20. Took 0.76383 seconds. Average reconstruction error is: 6.419
 epoch 20/20. Took 0.76465 seconds. Average reconstruction error is: 6.3613
Training NN  (784  500  500  500   10) with BackPropagation for 200 epochs
 epoch 1/200. Took 1.7633 seconds. 
  Full-batch training loss = 0.243146, test loss = 0.271013
  Training set accuracy = 0.931200, Test set accuracy = 0.923100
 epoch 2/200. Took 1.7854 seconds. 
  Full-batch training loss = 0.163626, test loss = 0.210785
  Training set accuracy = 0.951100, Test set accuracy = 0.936300
 epoch 3/200. Took 1.7619 seconds. 
  Full-batch training loss = 0.145782, test loss = 0.194203
  Training set accuracy = 0.956900, Test set accuracy = 0.940100
 epoch 4/200. Took 1.7635 seconds. 
  Full-batch training loss = 0.106905, test loss = 0.168406
  Training set accuracy = 0.971300, Test set accuracy = 0.949600
 epoch 5/200. Took 1.7716 seconds. 
  Full-batch training loss = 0.103100, test loss = 0.175169
  Training set accuracy = 0.969800, Test set accuracy = 0.947000
 epoch 6/200. Took 1.8157 seconds. 
  Full-batch training loss = 0.081722, test loss = 0.166375
  Training set accuracy = 0.978100, Test set accuracy = 0.950300
 epoch 7/200. Took 1.7854 seconds. 
  Full-batch training loss = 0.065536, test loss = 0.151609
  Training set accuracy = 0.983300, Test set accuracy = 0.955000
 epoch 8/200. Took 1.8181 seconds. 
  Full-batch training loss = 0.052853, test loss = 0.145074
  Training set accuracy = 0.988000, Test set accuracy = 0.956800
 epoch 9/200. Took 1.8119 seconds. 
  Full-batch training loss = 0.046455, test loss = 0.147454
  Training set accuracy = 0.989700, Test set accuracy = 0.955800
 epoch 10/200. Took 1.8141 seconds. 
  Full-batch training loss = 0.038295, test loss = 0.138667
  Training set accuracy = 0.993200, Test set accuracy = 0.958700
 epoch 11/200. Took 1.7549 seconds. 
  Full-batch training loss = 0.038054, test loss = 0.146099
  Training set accuracy = 0.992400, Test set accuracy = 0.955800
 epoch 12/200. Took 1.7596 seconds. 
  Full-batch training loss = 0.030067, test loss = 0.142759
  Training set accuracy = 0.994500, Test set accuracy = 0.957400
 epoch 13/200. Took 1.749 seconds. 
  Full-batch training loss = 0.025847, test loss = 0.139633
  Training set accuracy = 0.996700, Test set accuracy = 0.959300
 epoch 14/200. Took 1.7649 seconds. 
  Full-batch training loss = 0.021375, test loss = 0.138878
  Training set accuracy = 0.997500, Test set accuracy = 0.958700
 epoch 15/200. Took 1.7559 seconds. 
  Full-batch training loss = 0.020356, test loss = 0.137613
  Training set accuracy = 0.997600, Test set accuracy = 0.960400
 epoch 16/200. Took 1.7493 seconds. 
  Full-batch training loss = 0.017237, test loss = 0.139482
  Training set accuracy = 0.998600, Test set accuracy = 0.959100
 epoch 17/200. Took 1.7736 seconds. 
  Full-batch training loss = 0.014956, test loss = 0.137181
  Training set accuracy = 0.998800, Test set accuracy = 0.960100
 epoch 18/200. Took 1.7588 seconds. 
  Full-batch training loss = 0.013766, test loss = 0.139611
  Training set accuracy = 0.999400, Test set accuracy = 0.959800
 epoch 19/200. Took 1.7615 seconds. 
  Full-batch training loss = 0.011999, test loss = 0.136683
  Training set accuracy = 0.999400, Test set accuracy = 0.960000
 epoch 20/200. Took 1.7573 seconds. 
  Full-batch training loss = 0.011789, test loss = 0.138816
  Training set accuracy = 0.999600, Test set accuracy = 0.961100
 epoch 21/200. Took 1.7641 seconds. 
  Full-batch training loss = 0.010107, test loss = 0.138320
  Training set accuracy = 0.999700, Test set accuracy = 0.960800
 epoch 22/200. Took 1.7419 seconds. 
  Full-batch training loss = 0.009567, test loss = 0.136799
  Training set accuracy = 0.999700, Test set accuracy = 0.960900
 epoch 23/200. Took 1.7521 seconds. 
  Full-batch training loss = 0.008977, test loss = 0.138343
  Training set accuracy = 0.999500, Test set accuracy = 0.961200
 epoch 24/200. Took 1.7627 seconds. 
  Full-batch training loss = 0.008172, test loss = 0.139224
  Training set accuracy = 0.999800, Test set accuracy = 0.960000
 epoch 25/200. Took 1.7568 seconds. 
  Full-batch training loss = 0.008072, test loss = 0.141784
  Training set accuracy = 0.999700, Test set accuracy = 0.960600
 epoch 26/200. Took 1.7441 seconds. 
  Full-batch training loss = 0.006926, test loss = 0.138204
  Training set accuracy = 1.000000, Test set accuracy = 0.960800
 epoch 27/200. Took 1.7492 seconds. 
  Full-batch training loss = 0.006503, test loss = 0.140644
  Training set accuracy = 0.999900, Test set accuracy = 0.960500
 epoch 28/200. Took 1.7936 seconds. 
  Full-batch training loss = 0.006140, test loss = 0.138307
  Training set accuracy = 0.999900, Test set accuracy = 0.961600
 epoch 29/200. Took 1.8277 seconds. 
  Full-batch training loss = 0.005986, test loss = 0.139066
  Training set accuracy = 0.999900, Test set accuracy = 0.962300
 epoch 30/200. Took 1.7756 seconds. 
  Full-batch training loss = 0.005371, test loss = 0.139260
  Training set accuracy = 1.000000, Test set accuracy = 0.961600
 epoch 31/200. Took 1.7605 seconds. 
  Full-batch training loss = 0.005213, test loss = 0.139457
  Training set accuracy = 1.000000, Test set accuracy = 0.962100
 epoch 32/200. Took 1.7549 seconds. 
  Full-batch training loss = 0.004880, test loss = 0.140497
  Training set accuracy = 1.000000, Test set accuracy = 0.961900
 epoch 33/200. Took 1.7782 seconds. 
  Full-batch training loss = 0.004604, test loss = 0.140896
  Training set accuracy = 1.000000, Test set accuracy = 0.962100
 epoch 34/200. Took 1.7925 seconds. 
  Full-batch training loss = 0.004454, test loss = 0.141383
  Training set accuracy = 1.000000, Test set accuracy = 0.962200
 epoch 35/200. Took 1.7941 seconds. 
  Full-batch training loss = 0.004203, test loss = 0.140931
  Training set accuracy = 1.000000, Test set accuracy = 0.962000
 epoch 36/200. Took 1.7698 seconds. 
  Full-batch training loss = 0.004064, test loss = 0.142508
  Training set accuracy = 1.000000, Test set accuracy = 0.962100
 epoch 37/200. Took 1.7646 seconds. 
  Full-batch training loss = 0.004061, test loss = 0.143579
  Training set accuracy = 0.999900, Test set accuracy = 0.962100
 epoch 38/200. Took 1.7606 seconds. 
  Full-batch training loss = 0.003720, test loss = 0.141842
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 39/200. Took 1.7703 seconds. 
  Full-batch training loss = 0.003548, test loss = 0.142491
  Training set accuracy = 1.000000, Test set accuracy = 0.961600
 epoch 40/200. Took 1.781 seconds. 
  Full-batch training loss = 0.003487, test loss = 0.142307
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 41/200. Took 1.7578 seconds. 
  Full-batch training loss = 0.003310, test loss = 0.142621
  Training set accuracy = 1.000000, Test set accuracy = 0.962200
 epoch 42/200. Took 1.7652 seconds. 
  Full-batch training loss = 0.003208, test loss = 0.143369
  Training set accuracy = 1.000000, Test set accuracy = 0.962400
 epoch 43/200. Took 1.743 seconds. 
  Full-batch training loss = 0.003119, test loss = 0.142676
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 44/200. Took 1.7774 seconds. 
  Full-batch training loss = 0.002974, test loss = 0.143949
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 45/200. Took 1.7836 seconds. 
  Full-batch training loss = 0.002883, test loss = 0.144300
  Training set accuracy = 1.000000, Test set accuracy = 0.962100
 epoch 46/200. Took 1.9503 seconds. 
  Full-batch training loss = 0.002790, test loss = 0.143773
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 47/200. Took 1.756 seconds. 
  Full-batch training loss = 0.002697, test loss = 0.144292
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 48/200. Took 1.7796 seconds. 
  Full-batch training loss = 0.002621, test loss = 0.144617
  Training set accuracy = 1.000000, Test set accuracy = 0.962400
 epoch 49/200. Took 1.7895 seconds. 
  Full-batch training loss = 0.002568, test loss = 0.145266
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 50/200. Took 1.9913 seconds. 
  Full-batch training loss = 0.002458, test loss = 0.145033
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 51/200. Took 1.8708 seconds. 
  Full-batch training loss = 0.002420, test loss = 0.146007
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 52/200. Took 1.8348 seconds. 
  Full-batch training loss = 0.002340, test loss = 0.146366
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 53/200. Took 1.8337 seconds. 
  Full-batch training loss = 0.002296, test loss = 0.145507
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 54/200. Took 1.8253 seconds. 
  Full-batch training loss = 0.002240, test loss = 0.146248
  Training set accuracy = 1.000000, Test set accuracy = 0.962400
 epoch 55/200. Took 1.9264 seconds. 
  Full-batch training loss = 0.002159, test loss = 0.146022
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 56/200. Took 1.8573 seconds. 
  Full-batch training loss = 0.002123, test loss = 0.146175
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 57/200. Took 1.8237 seconds. 
  Full-batch training loss = 0.002071, test loss = 0.147570
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 58/200. Took 1.7585 seconds. 
  Full-batch training loss = 0.001993, test loss = 0.146533
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 59/200. Took 1.7696 seconds. 
  Full-batch training loss = 0.001955, test loss = 0.147397
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 60/200. Took 1.7654 seconds. 
  Full-batch training loss = 0.001923, test loss = 0.147169
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 61/200. Took 1.7584 seconds. 
  Full-batch training loss = 0.001862, test loss = 0.147699
  Training set accuracy = 1.000000, Test set accuracy = 0.962300
 epoch 62/200. Took 1.7675 seconds. 
  Full-batch training loss = 0.001836, test loss = 0.148498
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 63/200. Took 1.7566 seconds. 
  Full-batch training loss = 0.001786, test loss = 0.147421
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 64/200. Took 1.8717 seconds. 
  Full-batch training loss = 0.001747, test loss = 0.147805
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 65/200. Took 1.7674 seconds. 
  Full-batch training loss = 0.001705, test loss = 0.148076
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 66/200. Took 1.7689 seconds. 
  Full-batch training loss = 0.001672, test loss = 0.147985
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 67/200. Took 1.7658 seconds. 
  Full-batch training loss = 0.001642, test loss = 0.148244
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 68/200. Took 1.9189 seconds. 
  Full-batch training loss = 0.001595, test loss = 0.148881
  Training set accuracy = 1.000000, Test set accuracy = 0.962500
 epoch 69/200. Took 1.8475 seconds. 
  Full-batch training loss = 0.001570, test loss = 0.149381
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 70/200. Took 1.7763 seconds. 
  Full-batch training loss = 0.001540, test loss = 0.149354
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 71/200. Took 1.7802 seconds. 
  Full-batch training loss = 0.001509, test loss = 0.149132
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 72/200. Took 1.7827 seconds. 
  Full-batch training loss = 0.001477, test loss = 0.149547
  Training set accuracy = 1.000000, Test set accuracy = 0.962600
 epoch 73/200. Took 1.7898 seconds. 
  Full-batch training loss = 0.001454, test loss = 0.149443
  Training set accuracy = 1.000000, Test set accuracy = 0.962900
 epoch 74/200. Took 1.7718 seconds. 
  Full-batch training loss = 0.001427, test loss = 0.149533
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 75/200. Took 1.816 seconds. 
  Full-batch training loss = 0.001398, test loss = 0.149924
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 76/200. Took 1.7742 seconds. 
  Full-batch training loss = 0.001377, test loss = 0.150159
  Training set accuracy = 1.000000, Test set accuracy = 0.962700
 epoch 77/200. Took 1.7511 seconds. 
  Full-batch training loss = 0.001351, test loss = 0.150479
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 78/200. Took 1.7516 seconds. 
  Full-batch training loss = 0.001328, test loss = 0.150623
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 79/200. Took 1.7659 seconds. 
  Full-batch training loss = 0.001305, test loss = 0.150678
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 80/200. Took 1.7535 seconds. 
  Full-batch training loss = 0.001289, test loss = 0.151196
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 81/200. Took 1.7563 seconds. 
  Full-batch training loss = 0.001260, test loss = 0.150655
  Training set accuracy = 1.000000, Test set accuracy = 0.962800
 epoch 82/200. Took 1.7517 seconds. 
  Full-batch training loss = 0.001245, test loss = 0.151732
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 83/200. Took 1.7278 seconds. 
  Full-batch training loss = 0.001219, test loss = 0.151473
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 84/200. Took 1.7366 seconds. 
  Full-batch training loss = 0.001204, test loss = 0.151129
  Training set accuracy = 1.000000, Test set accuracy = 0.963000
 epoch 85/200. Took 1.7527 seconds. 
  Full-batch training loss = 0.001185, test loss = 0.151695
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 86/200. Took 1.7528 seconds. 
  Full-batch training loss = 0.001168, test loss = 0.151415
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 87/200. Took 1.7711 seconds. 
  Full-batch training loss = 0.001147, test loss = 0.151515
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 88/200. Took 1.7481 seconds. 
  Full-batch training loss = 0.001128, test loss = 0.152293
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 89/200. Took 1.7417 seconds. 
  Full-batch training loss = 0.001114, test loss = 0.151772
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 90/200. Took 1.7582 seconds. 
  Full-batch training loss = 0.001097, test loss = 0.152292
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 91/200. Took 1.7483 seconds. 
  Full-batch training loss = 0.001082, test loss = 0.152579
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 92/200. Took 1.7569 seconds. 
  Full-batch training loss = 0.001067, test loss = 0.152511
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 93/200. Took 1.7492 seconds. 
  Full-batch training loss = 0.001048, test loss = 0.152714
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 94/200. Took 1.9086 seconds. 
  Full-batch training loss = 0.001036, test loss = 0.152807
  Training set accuracy = 1.000000, Test set accuracy = 0.963300
 epoch 95/200. Took 1.8453 seconds. 
  Full-batch training loss = 0.001022, test loss = 0.153324
  Training set accuracy = 1.000000, Test set accuracy = 0.963100
 epoch 96/200. Took 1.7564 seconds. 
  Full-batch training loss = 0.001009, test loss = 0.153295
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 97/200. Took 1.7543 seconds. 
  Full-batch training loss = 0.000993, test loss = 0.153795
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 98/200. Took 1.7466 seconds. 
  Full-batch training loss = 0.000979, test loss = 0.153585
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 99/200. Took 1.7375 seconds. 
  Full-batch training loss = 0.000966, test loss = 0.153631
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 100/200. Took 1.7683 seconds. 
  Full-batch training loss = 0.000957, test loss = 0.153994
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 101/200. Took 1.7577 seconds. 
  Full-batch training loss = 0.000941, test loss = 0.154126
  Training set accuracy = 1.000000, Test set accuracy = 0.963400
 epoch 102/200. Took 1.7529 seconds. 
  Full-batch training loss = 0.000928, test loss = 0.153946
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 103/200. Took 1.7659 seconds. 
  Full-batch training loss = 0.000918, test loss = 0.153819
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 104/200. Took 1.7469 seconds. 
  Full-batch training loss = 0.000905, test loss = 0.154271
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 105/200. Took 1.7709 seconds. 
  Full-batch training loss = 0.000894, test loss = 0.154418
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 106/200. Took 1.7637 seconds. 
  Full-batch training loss = 0.000884, test loss = 0.154853
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 107/200. Took 1.7532 seconds. 
  Full-batch training loss = 0.000875, test loss = 0.154612
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 108/200. Took 1.7677 seconds. 
  Full-batch training loss = 0.000864, test loss = 0.155085
  Training set accuracy = 1.000000, Test set accuracy = 0.963200
 epoch 109/200. Took 1.7565 seconds. 
  Full-batch training loss = 0.000851, test loss = 0.154995
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 110/200. Took 1.7701 seconds. 
  Full-batch training loss = 0.000843, test loss = 0.154493
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 111/200. Took 1.7716 seconds. 
  Full-batch training loss = 0.000836, test loss = 0.155159
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 112/200. Took 1.7744 seconds. 
  Full-batch training loss = 0.000822, test loss = 0.155058
  Training set accuracy = 1.000000, Test set accuracy = 0.963500
 epoch 113/200. Took 1.8215 seconds. 
  Full-batch training loss = 0.000812, test loss = 0.155161
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 114/200. Took 1.8901 seconds. 
  Full-batch training loss = 0.000803, test loss = 0.155157
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 115/200. Took 1.8316 seconds. 
  Full-batch training loss = 0.000795, test loss = 0.155431
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 116/200. Took 1.8299 seconds. 
  Full-batch training loss = 0.000786, test loss = 0.155229
  Training set accuracy = 1.000000, Test set accuracy = 0.963700
 epoch 117/200. Took 1.8898 seconds. 
  Full-batch training loss = 0.000777, test loss = 0.155548
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 118/200. Took 1.8465 seconds. 
  Full-batch training loss = 0.000769, test loss = 0.155728
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 119/200. Took 1.8872 seconds. 
  Full-batch training loss = 0.000760, test loss = 0.155726
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 120/200. Took 1.8249 seconds. 
  Full-batch training loss = 0.000751, test loss = 0.155919
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 121/200. Took 1.7656 seconds. 
  Full-batch training loss = 0.000744, test loss = 0.156199
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 122/200. Took 1.7624 seconds. 
  Full-batch training loss = 0.000736, test loss = 0.156171
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 123/200. Took 1.7743 seconds. 
  Full-batch training loss = 0.000729, test loss = 0.156422
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 124/200. Took 1.7855 seconds. 
  Full-batch training loss = 0.000721, test loss = 0.156457
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 125/200. Took 1.7825 seconds. 
  Full-batch training loss = 0.000713, test loss = 0.156583
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 126/200. Took 1.7811 seconds. 
  Full-batch training loss = 0.000705, test loss = 0.156693
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 127/200. Took 1.7835 seconds. 
  Full-batch training loss = 0.000698, test loss = 0.156526
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 128/200. Took 1.7838 seconds. 
  Full-batch training loss = 0.000691, test loss = 0.156713
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 129/200. Took 1.7609 seconds. 
  Full-batch training loss = 0.000685, test loss = 0.156931
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 130/200. Took 1.759 seconds. 
  Full-batch training loss = 0.000678, test loss = 0.156962
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 131/200. Took 1.7969 seconds. 
  Full-batch training loss = 0.000671, test loss = 0.157067
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 132/200. Took 1.8491 seconds. 
  Full-batch training loss = 0.000665, test loss = 0.157186
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 133/200. Took 1.8221 seconds. 
  Full-batch training loss = 0.000659, test loss = 0.157229
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 134/200. Took 1.7889 seconds. 
  Full-batch training loss = 0.000652, test loss = 0.157190
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 135/200. Took 1.7708 seconds. 
  Full-batch training loss = 0.000646, test loss = 0.157310
  Training set accuracy = 1.000000, Test set accuracy = 0.963800
 epoch 136/200. Took 1.7609 seconds. 
  Full-batch training loss = 0.000640, test loss = 0.157520
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 137/200. Took 1.7539 seconds. 
  Full-batch training loss = 0.000634, test loss = 0.157704
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 138/200. Took 1.7552 seconds. 
  Full-batch training loss = 0.000628, test loss = 0.157584
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 139/200. Took 1.7433 seconds. 
  Full-batch training loss = 0.000623, test loss = 0.157725
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 140/200. Took 1.8346 seconds. 
  Full-batch training loss = 0.000617, test loss = 0.158158
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 141/200. Took 1.8448 seconds. 
  Full-batch training loss = 0.000611, test loss = 0.158180
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 142/200. Took 1.8085 seconds. 
  Full-batch training loss = 0.000605, test loss = 0.158094
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 143/200. Took 1.7588 seconds. 
  Full-batch training loss = 0.000600, test loss = 0.158196
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 144/200. Took 1.7643 seconds. 
  Full-batch training loss = 0.000595, test loss = 0.158298
  Training set accuracy = 1.000000, Test set accuracy = 0.963600
 epoch 145/200. Took 1.7737 seconds. 
  Full-batch training loss = 0.000590, test loss = 0.158337
  Training set accuracy = 1.000000, Test set accuracy = 0.963900
 epoch 146/200. Took 1.7601 seconds. 
  Full-batch training loss = 0.000585, test loss = 0.158473
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 147/200. Took 1.7611 seconds. 
  Full-batch training loss = 0.000580, test loss = 0.158477
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 148/200. Took 1.7714 seconds. 
  Full-batch training loss = 0.000574, test loss = 0.158564
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 149/200. Took 1.7678 seconds. 
  Full-batch training loss = 0.000570, test loss = 0.158667
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 150/200. Took 1.7672 seconds. 
  Full-batch training loss = 0.000565, test loss = 0.158955
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 151/200. Took 1.774 seconds. 
  Full-batch training loss = 0.000560, test loss = 0.158857
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 152/200. Took 1.771 seconds. 
  Full-batch training loss = 0.000555, test loss = 0.159008
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 153/200. Took 1.757 seconds. 
  Full-batch training loss = 0.000551, test loss = 0.159128
  Training set accuracy = 1.000000, Test set accuracy = 0.964700
 epoch 154/200. Took 1.803 seconds. 
  Full-batch training loss = 0.000546, test loss = 0.159072
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 155/200. Took 1.7578 seconds. 
  Full-batch training loss = 0.000542, test loss = 0.159331
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 156/200. Took 1.7662 seconds. 
  Full-batch training loss = 0.000537, test loss = 0.159381
  Training set accuracy = 1.000000, Test set accuracy = 0.964700
 epoch 157/200. Took 1.7311 seconds. 
  Full-batch training loss = 0.000533, test loss = 0.159461
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 158/200. Took 1.7495 seconds. 
  Full-batch training loss = 0.000529, test loss = 0.159541
  Training set accuracy = 1.000000, Test set accuracy = 0.964200
 epoch 159/200. Took 1.8293 seconds. 
  Full-batch training loss = 0.000525, test loss = 0.159611
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 160/200. Took 1.7997 seconds. 
  Full-batch training loss = 0.000520, test loss = 0.159515
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 161/200. Took 1.8402 seconds. 
  Full-batch training loss = 0.000517, test loss = 0.159677
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 162/200. Took 1.8871 seconds. 
  Full-batch training loss = 0.000512, test loss = 0.159647
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 163/200. Took 1.796 seconds. 
  Full-batch training loss = 0.000508, test loss = 0.159899
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 164/200. Took 1.8366 seconds. 
  Full-batch training loss = 0.000505, test loss = 0.159672
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 165/200. Took 1.7771 seconds. 
  Full-batch training loss = 0.000501, test loss = 0.160095
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 166/200. Took 1.7473 seconds. 
  Full-batch training loss = 0.000497, test loss = 0.160217
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 167/200. Took 1.7933 seconds. 
  Full-batch training loss = 0.000493, test loss = 0.160151
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 168/200. Took 1.7573 seconds. 
  Full-batch training loss = 0.000489, test loss = 0.160380
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 169/200. Took 1.7805 seconds. 
  Full-batch training loss = 0.000486, test loss = 0.160378
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 170/200. Took 1.7598 seconds. 
  Full-batch training loss = 0.000482, test loss = 0.160448
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 171/200. Took 1.7842 seconds. 
  Full-batch training loss = 0.000479, test loss = 0.160495
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 172/200. Took 1.7999 seconds. 
  Full-batch training loss = 0.000475, test loss = 0.160697
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 173/200. Took 1.7597 seconds. 
  Full-batch training loss = 0.000472, test loss = 0.160802
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 174/200. Took 1.7736 seconds. 
  Full-batch training loss = 0.000468, test loss = 0.160724
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 175/200. Took 1.8203 seconds. 
  Full-batch training loss = 0.000465, test loss = 0.160730
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 176/200. Took 1.8186 seconds. 
  Full-batch training loss = 0.000462, test loss = 0.160810
  Training set accuracy = 1.000000, Test set accuracy = 0.964100
 epoch 177/200. Took 1.755 seconds. 
  Full-batch training loss = 0.000458, test loss = 0.160874
  Training set accuracy = 1.000000, Test set accuracy = 0.964000
 epoch 178/200. Took 1.7539 seconds. 
  Full-batch training loss = 0.000455, test loss = 0.160978
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 179/200. Took 1.7409 seconds. 
  Full-batch training loss = 0.000452, test loss = 0.161046
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 180/200. Took 1.7601 seconds. 
  Full-batch training loss = 0.000449, test loss = 0.161304
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 181/200. Took 1.7638 seconds. 
  Full-batch training loss = 0.000446, test loss = 0.161277
  Training set accuracy = 1.000000, Test set accuracy = 0.964700
 epoch 182/200. Took 1.7799 seconds. 
  Full-batch training loss = 0.000443, test loss = 0.161522
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 183/200. Took 1.7703 seconds. 
  Full-batch training loss = 0.000440, test loss = 0.161484
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 184/200. Took 1.752 seconds. 
  Full-batch training loss = 0.000437, test loss = 0.161462
  Training set accuracy = 1.000000, Test set accuracy = 0.964700
 epoch 185/200. Took 1.7718 seconds. 
  Full-batch training loss = 0.000433, test loss = 0.161503
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 186/200. Took 1.7685 seconds. 
  Full-batch training loss = 0.000431, test loss = 0.161672
  Training set accuracy = 1.000000, Test set accuracy = 0.964400
 epoch 187/200. Took 1.7642 seconds. 
  Full-batch training loss = 0.000428, test loss = 0.161499
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 188/200. Took 1.7774 seconds. 
  Full-batch training loss = 0.000425, test loss = 0.161755
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 189/200. Took 1.7621 seconds. 
  Full-batch training loss = 0.000422, test loss = 0.161758
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 190/200. Took 1.7723 seconds. 
  Full-batch training loss = 0.000419, test loss = 0.161797
  Training set accuracy = 1.000000, Test set accuracy = 0.964700
 epoch 191/200. Took 1.7828 seconds. 
  Full-batch training loss = 0.000417, test loss = 0.161969
  Training set accuracy = 1.000000, Test set accuracy = 0.964800
 epoch 192/200. Took 1.7617 seconds. 
  Full-batch training loss = 0.000414, test loss = 0.161951
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 193/200. Took 1.7594 seconds. 
  Full-batch training loss = 0.000411, test loss = 0.162021
  Training set accuracy = 1.000000, Test set accuracy = 0.964700
 epoch 194/200. Took 1.7581 seconds. 
  Full-batch training loss = 0.000409, test loss = 0.162100
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 195/200. Took 1.7664 seconds. 
  Full-batch training loss = 0.000406, test loss = 0.162234
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 196/200. Took 1.9293 seconds. 
  Full-batch training loss = 0.000403, test loss = 0.162165
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
 epoch 197/200. Took 1.7732 seconds. 
  Full-batch training loss = 0.000401, test loss = 0.162126
  Training set accuracy = 1.000000, Test set accuracy = 0.964300
 epoch 198/200. Took 1.7907 seconds. 
  Full-batch training loss = 0.000398, test loss = 0.162412
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 199/200. Took 1.7911 seconds. 
  Full-batch training loss = 0.000396, test loss = 0.162325
  Training set accuracy = 1.000000, Test set accuracy = 0.964600
 epoch 200/200. Took 1.837 seconds. 
  Full-batch training loss = 0.000394, test loss = 0.162531
  Training set accuracy = 1.000000, Test set accuracy = 0.964500
Elapsed time is 693.414409 seconds.
End Training
