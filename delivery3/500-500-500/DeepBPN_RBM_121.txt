==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 121/270
----------------------------------------------------------
* ReducedData	5000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	200
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.54287 seconds. Average reconstruction error is: 34.64
 epoch 2/20. Took 0.54274 seconds. Average reconstruction error is: 19.3992
 epoch 3/20. Took 0.54155 seconds. Average reconstruction error is: 16.591
 epoch 4/20. Took 0.54446 seconds. Average reconstruction error is: 15.0706
 epoch 5/20. Took 0.54725 seconds. Average reconstruction error is: 14.0043
 epoch 6/20. Took 0.53721 seconds. Average reconstruction error is: 13.2001
 epoch 7/20. Took 0.53858 seconds. Average reconstruction error is: 12.4572
 epoch 8/20. Took 0.53748 seconds. Average reconstruction error is: 11.9107
 epoch 9/20. Took 0.55186 seconds. Average reconstruction error is: 11.4231
 epoch 10/20. Took 0.54888 seconds. Average reconstruction error is: 11.0495
 epoch 11/20. Took 0.54463 seconds. Average reconstruction error is: 10.7498
 epoch 12/20. Took 0.54637 seconds. Average reconstruction error is: 10.4565
 epoch 13/20. Took 0.53775 seconds. Average reconstruction error is: 10.1832
 epoch 14/20. Took 0.55003 seconds. Average reconstruction error is: 9.9669
 epoch 15/20. Took 0.54822 seconds. Average reconstruction error is: 9.7215
 epoch 16/20. Took 0.53966 seconds. Average reconstruction error is: 9.5771
 epoch 17/20. Took 0.55246 seconds. Average reconstruction error is: 9.461
 epoch 18/20. Took 0.54902 seconds. Average reconstruction error is: 9.3252
 epoch 19/20. Took 0.54566 seconds. Average reconstruction error is: 9.1515
 epoch 20/20. Took 0.54798 seconds. Average reconstruction error is: 9.0882
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.37264 seconds. Average reconstruction error is: 22.7074
 epoch 2/20. Took 0.37728 seconds. Average reconstruction error is: 11.54
 epoch 3/20. Took 0.3774 seconds. Average reconstruction error is: 9.6567
 epoch 4/20. Took 0.36537 seconds. Average reconstruction error is: 8.8048
 epoch 5/20. Took 0.37147 seconds. Average reconstruction error is: 8.2845
 epoch 6/20. Took 0.37424 seconds. Average reconstruction error is: 7.9291
 epoch 7/20. Took 0.37333 seconds. Average reconstruction error is: 7.6743
 epoch 8/20. Took 0.37656 seconds. Average reconstruction error is: 7.4006
 epoch 9/20. Took 0.37043 seconds. Average reconstruction error is: 7.2704
 epoch 10/20. Took 0.37646 seconds. Average reconstruction error is: 7.1078
 epoch 11/20. Took 0.36912 seconds. Average reconstruction error is: 6.9949
 epoch 12/20. Took 0.37789 seconds. Average reconstruction error is: 6.8776
 epoch 13/20. Took 0.37169 seconds. Average reconstruction error is: 6.8093
 epoch 14/20. Took 0.38743 seconds. Average reconstruction error is: 6.7273
 epoch 15/20. Took 0.37805 seconds. Average reconstruction error is: 6.6013
 epoch 16/20. Took 0.3787 seconds. Average reconstruction error is: 6.5795
 epoch 17/20. Took 0.37206 seconds. Average reconstruction error is: 6.4993
 epoch 18/20. Took 0.37421 seconds. Average reconstruction error is: 6.4259
 epoch 19/20. Took 0.37553 seconds. Average reconstruction error is: 6.3673
 epoch 20/20. Took 0.37135 seconds. Average reconstruction error is: 6.2763
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.37196 seconds. Average reconstruction error is: 18.0983
 epoch 2/20. Took 0.37321 seconds. Average reconstruction error is: 8.2007
 epoch 3/20. Took 0.36867 seconds. Average reconstruction error is: 6.9336
 epoch 4/20. Took 0.37661 seconds. Average reconstruction error is: 6.3268
 epoch 5/20. Took 0.37527 seconds. Average reconstruction error is: 6.0112
 epoch 6/20. Took 0.37551 seconds. Average reconstruction error is: 5.8573
 epoch 7/20. Took 0.37245 seconds. Average reconstruction error is: 5.6496
 epoch 8/20. Took 0.37372 seconds. Average reconstruction error is: 5.5845
 epoch 9/20. Took 0.37631 seconds. Average reconstruction error is: 5.4915
 epoch 10/20. Took 0.37726 seconds. Average reconstruction error is: 5.3917
 epoch 11/20. Took 0.37344 seconds. Average reconstruction error is: 5.3271
 epoch 12/20. Took 0.37497 seconds. Average reconstruction error is: 5.3072
 epoch 13/20. Took 0.37387 seconds. Average reconstruction error is: 5.25
 epoch 14/20. Took 0.37601 seconds. Average reconstruction error is: 5.2226
 epoch 15/20. Took 0.37478 seconds. Average reconstruction error is: 5.168
 epoch 16/20. Took 0.37638 seconds. Average reconstruction error is: 5.1142
 epoch 17/20. Took 0.37125 seconds. Average reconstruction error is: 5.1253
 epoch 18/20. Took 0.37515 seconds. Average reconstruction error is: 5.0961
 epoch 19/20. Took 0.37823 seconds. Average reconstruction error is: 5.0578
 epoch 20/20. Took 0.37411 seconds. Average reconstruction error is: 5.0187
Training NN  (784  500  500  500   10) with BackPropagation for 200 epochs
 epoch 1/200. Took 0.86061 seconds. 
  Full-batch training loss = 0.305853, test loss = 0.326588
  Training set accuracy = 0.915400, Test set accuracy = 0.908900
 epoch 2/200. Took 0.86896 seconds. 
  Full-batch training loss = 0.250640, test loss = 0.282763
  Training set accuracy = 0.932400, Test set accuracy = 0.918100
 epoch 3/200. Took 0.87171 seconds. 
  Full-batch training loss = 0.209969, test loss = 0.253176
  Training set accuracy = 0.941000, Test set accuracy = 0.925800
 epoch 4/200. Took 0.86954 seconds. 
  Full-batch training loss = 0.179677, test loss = 0.235672
  Training set accuracy = 0.950200, Test set accuracy = 0.931300
 epoch 5/200. Took 0.88229 seconds. 
  Full-batch training loss = 0.157853, test loss = 0.223404
  Training set accuracy = 0.956400, Test set accuracy = 0.932800
 epoch 6/200. Took 0.87837 seconds. 
  Full-batch training loss = 0.136478, test loss = 0.215874
  Training set accuracy = 0.964600, Test set accuracy = 0.935800
 epoch 7/200. Took 0.87481 seconds. 
  Full-batch training loss = 0.126047, test loss = 0.212386
  Training set accuracy = 0.966200, Test set accuracy = 0.934800
 epoch 8/200. Took 0.8723 seconds. 
  Full-batch training loss = 0.109547, test loss = 0.204016
  Training set accuracy = 0.972000, Test set accuracy = 0.939300
 epoch 9/200. Took 0.87922 seconds. 
  Full-batch training loss = 0.099176, test loss = 0.201761
  Training set accuracy = 0.975800, Test set accuracy = 0.941100
 epoch 10/200. Took 0.8684 seconds. 
  Full-batch training loss = 0.088236, test loss = 0.195578
  Training set accuracy = 0.980400, Test set accuracy = 0.943800
 epoch 11/200. Took 0.87373 seconds. 
  Full-batch training loss = 0.078786, test loss = 0.192418
  Training set accuracy = 0.983200, Test set accuracy = 0.942900
 epoch 12/200. Took 0.87214 seconds. 
  Full-batch training loss = 0.070777, test loss = 0.192726
  Training set accuracy = 0.986000, Test set accuracy = 0.943900
 epoch 13/200. Took 0.86551 seconds. 
  Full-batch training loss = 0.064829, test loss = 0.188492
  Training set accuracy = 0.987600, Test set accuracy = 0.943400
 epoch 14/200. Took 0.87476 seconds. 
  Full-batch training loss = 0.061071, test loss = 0.190094
  Training set accuracy = 0.988000, Test set accuracy = 0.944000
 epoch 15/200. Took 0.87224 seconds. 
  Full-batch training loss = 0.055878, test loss = 0.191340
  Training set accuracy = 0.990800, Test set accuracy = 0.944800
 epoch 16/200. Took 0.87257 seconds. 
  Full-batch training loss = 0.048231, test loss = 0.187595
  Training set accuracy = 0.992800, Test set accuracy = 0.945600
 epoch 17/200. Took 0.87268 seconds. 
  Full-batch training loss = 0.045675, test loss = 0.190094
  Training set accuracy = 0.993800, Test set accuracy = 0.945600
 epoch 18/200. Took 0.87558 seconds. 
  Full-batch training loss = 0.039966, test loss = 0.184764
  Training set accuracy = 0.994800, Test set accuracy = 0.947000
 epoch 19/200. Took 0.87579 seconds. 
  Full-batch training loss = 0.038753, test loss = 0.189224
  Training set accuracy = 0.995600, Test set accuracy = 0.945800
 epoch 20/200. Took 0.86612 seconds. 
  Full-batch training loss = 0.034955, test loss = 0.186368
  Training set accuracy = 0.996600, Test set accuracy = 0.946700
 epoch 21/200. Took 0.87365 seconds. 
  Full-batch training loss = 0.033227, test loss = 0.185628
  Training set accuracy = 0.997200, Test set accuracy = 0.945900
 epoch 22/200. Took 0.87065 seconds. 
  Full-batch training loss = 0.029180, test loss = 0.185005
  Training set accuracy = 0.998000, Test set accuracy = 0.946700
 epoch 23/200. Took 0.86564 seconds. 
  Full-batch training loss = 0.027500, test loss = 0.184967
  Training set accuracy = 0.998600, Test set accuracy = 0.947900
 epoch 24/200. Took 0.88204 seconds. 
  Full-batch training loss = 0.025076, test loss = 0.185086
  Training set accuracy = 0.998600, Test set accuracy = 0.948100
 epoch 25/200. Took 0.87832 seconds. 
  Full-batch training loss = 0.023547, test loss = 0.185827
  Training set accuracy = 0.999200, Test set accuracy = 0.947400
 epoch 26/200. Took 0.87912 seconds. 
  Full-batch training loss = 0.021823, test loss = 0.184901
  Training set accuracy = 0.999400, Test set accuracy = 0.948700
 epoch 27/200. Took 0.87764 seconds. 
  Full-batch training loss = 0.020395, test loss = 0.184785
  Training set accuracy = 0.999400, Test set accuracy = 0.948200
 epoch 28/200. Took 0.87432 seconds. 
  Full-batch training loss = 0.019534, test loss = 0.187323
  Training set accuracy = 0.999400, Test set accuracy = 0.947700
 epoch 29/200. Took 0.87711 seconds. 
  Full-batch training loss = 0.017910, test loss = 0.186056
  Training set accuracy = 0.999400, Test set accuracy = 0.948100
 epoch 30/200. Took 0.88145 seconds. 
  Full-batch training loss = 0.016909, test loss = 0.185699
  Training set accuracy = 0.999600, Test set accuracy = 0.948300
 epoch 31/200. Took 0.87598 seconds. 
  Full-batch training loss = 0.015990, test loss = 0.186121
  Training set accuracy = 0.999600, Test set accuracy = 0.949100
 epoch 32/200. Took 0.88495 seconds. 
  Full-batch training loss = 0.015077, test loss = 0.185846
  Training set accuracy = 0.999600, Test set accuracy = 0.949000
 epoch 33/200. Took 0.87244 seconds. 
  Full-batch training loss = 0.014287, test loss = 0.186242
  Training set accuracy = 0.999600, Test set accuracy = 0.948400
 epoch 34/200. Took 0.87584 seconds. 
  Full-batch training loss = 0.013634, test loss = 0.187380
  Training set accuracy = 0.999600, Test set accuracy = 0.948800
 epoch 35/200. Took 0.87604 seconds. 
  Full-batch training loss = 0.012844, test loss = 0.187244
  Training set accuracy = 0.999600, Test set accuracy = 0.948800
 epoch 36/200. Took 0.88064 seconds. 
  Full-batch training loss = 0.012427, test loss = 0.187640
  Training set accuracy = 0.999600, Test set accuracy = 0.949600
 epoch 37/200. Took 0.87059 seconds. 
  Full-batch training loss = 0.011674, test loss = 0.188945
  Training set accuracy = 1.000000, Test set accuracy = 0.949000
 epoch 38/200. Took 0.87575 seconds. 
  Full-batch training loss = 0.011164, test loss = 0.188736
  Training set accuracy = 1.000000, Test set accuracy = 0.948500
 epoch 39/200. Took 0.88297 seconds. 
  Full-batch training loss = 0.010680, test loss = 0.189377
  Training set accuracy = 1.000000, Test set accuracy = 0.949300
 epoch 40/200. Took 0.90869 seconds. 
  Full-batch training loss = 0.010280, test loss = 0.189200
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 41/200. Took 0.86483 seconds. 
  Full-batch training loss = 0.009795, test loss = 0.188772
  Training set accuracy = 1.000000, Test set accuracy = 0.948500
 epoch 42/200. Took 0.87985 seconds. 
  Full-batch training loss = 0.009433, test loss = 0.189414
  Training set accuracy = 1.000000, Test set accuracy = 0.949100
 epoch 43/200. Took 0.87097 seconds. 
  Full-batch training loss = 0.009021, test loss = 0.189861
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 44/200. Took 0.87609 seconds. 
  Full-batch training loss = 0.008742, test loss = 0.190004
  Training set accuracy = 1.000000, Test set accuracy = 0.949100
 epoch 45/200. Took 0.87579 seconds. 
  Full-batch training loss = 0.008395, test loss = 0.190798
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 46/200. Took 0.87686 seconds. 
  Full-batch training loss = 0.008131, test loss = 0.190604
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 47/200. Took 0.86749 seconds. 
  Full-batch training loss = 0.007808, test loss = 0.190978
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 48/200. Took 0.87658 seconds. 
  Full-batch training loss = 0.007544, test loss = 0.191218
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 49/200. Took 0.87257 seconds. 
  Full-batch training loss = 0.007295, test loss = 0.191554
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 50/200. Took 0.8736 seconds. 
  Full-batch training loss = 0.007066, test loss = 0.192204
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 51/200. Took 0.88291 seconds. 
  Full-batch training loss = 0.006838, test loss = 0.192288
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 52/200. Took 0.87736 seconds. 
  Full-batch training loss = 0.006629, test loss = 0.192463
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 53/200. Took 0.87141 seconds. 
  Full-batch training loss = 0.006446, test loss = 0.192724
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 54/200. Took 0.86637 seconds. 
  Full-batch training loss = 0.006245, test loss = 0.192840
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 55/200. Took 0.8692 seconds. 
  Full-batch training loss = 0.006079, test loss = 0.193383
  Training set accuracy = 1.000000, Test set accuracy = 0.949700
 epoch 56/200. Took 0.86747 seconds. 
  Full-batch training loss = 0.005900, test loss = 0.193520
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 57/200. Took 0.87381 seconds. 
  Full-batch training loss = 0.005741, test loss = 0.193674
  Training set accuracy = 1.000000, Test set accuracy = 0.949100
 epoch 58/200. Took 0.87669 seconds. 
  Full-batch training loss = 0.005630, test loss = 0.195031
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 59/200. Took 0.8732 seconds. 
  Full-batch training loss = 0.005436, test loss = 0.194414
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 60/200. Took 0.87501 seconds. 
  Full-batch training loss = 0.005293, test loss = 0.194749
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 61/200. Took 0.87828 seconds. 
  Full-batch training loss = 0.005161, test loss = 0.194857
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 62/200. Took 0.87914 seconds. 
  Full-batch training loss = 0.005058, test loss = 0.195564
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 63/200. Took 0.86511 seconds. 
  Full-batch training loss = 0.004917, test loss = 0.195095
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 64/200. Took 0.87785 seconds. 
  Full-batch training loss = 0.004799, test loss = 0.195717
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 65/200. Took 0.87828 seconds. 
  Full-batch training loss = 0.004689, test loss = 0.195712
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 66/200. Took 0.86913 seconds. 
  Full-batch training loss = 0.004589, test loss = 0.196076
  Training set accuracy = 1.000000, Test set accuracy = 0.949700
 epoch 67/200. Took 0.87403 seconds. 
  Full-batch training loss = 0.004493, test loss = 0.196925
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 68/200. Took 0.90362 seconds. 
  Full-batch training loss = 0.004385, test loss = 0.196924
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 69/200. Took 0.87705 seconds. 
  Full-batch training loss = 0.004289, test loss = 0.197049
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 70/200. Took 0.94894 seconds. 
  Full-batch training loss = 0.004198, test loss = 0.197351
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 71/200. Took 0.87747 seconds. 
  Full-batch training loss = 0.004107, test loss = 0.197245
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 72/200. Took 0.88005 seconds. 
  Full-batch training loss = 0.004026, test loss = 0.197693
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 73/200. Took 0.87612 seconds. 
  Full-batch training loss = 0.003943, test loss = 0.197912
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 74/200. Took 0.87598 seconds. 
  Full-batch training loss = 0.003867, test loss = 0.198003
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 75/200. Took 0.8957 seconds. 
  Full-batch training loss = 0.003790, test loss = 0.198354
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 76/200. Took 0.87407 seconds. 
  Full-batch training loss = 0.003719, test loss = 0.198709
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 77/200. Took 0.87848 seconds. 
  Full-batch training loss = 0.003647, test loss = 0.198854
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 78/200. Took 0.86744 seconds. 
  Full-batch training loss = 0.003579, test loss = 0.199137
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 79/200. Took 0.86552 seconds. 
  Full-batch training loss = 0.003516, test loss = 0.199228
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 80/200. Took 0.87209 seconds. 
  Full-batch training loss = 0.003449, test loss = 0.199516
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 81/200. Took 0.87528 seconds. 
  Full-batch training loss = 0.003391, test loss = 0.199770
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 82/200. Took 0.87978 seconds. 
  Full-batch training loss = 0.003332, test loss = 0.199698
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 83/200. Took 0.86854 seconds. 
  Full-batch training loss = 0.003272, test loss = 0.199824
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 84/200. Took 0.8745 seconds. 
  Full-batch training loss = 0.003215, test loss = 0.200306
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 85/200. Took 0.87422 seconds. 
  Full-batch training loss = 0.003165, test loss = 0.200702
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 86/200. Took 0.86608 seconds. 
  Full-batch training loss = 0.003110, test loss = 0.200711
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 87/200. Took 0.87414 seconds. 
  Full-batch training loss = 0.003059, test loss = 0.201105
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 88/200. Took 0.87457 seconds. 
  Full-batch training loss = 0.003008, test loss = 0.201169
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 89/200. Took 0.866 seconds. 
  Full-batch training loss = 0.002962, test loss = 0.201450
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 90/200. Took 0.87081 seconds. 
  Full-batch training loss = 0.002914, test loss = 0.201424
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 91/200. Took 0.87854 seconds. 
  Full-batch training loss = 0.002869, test loss = 0.201659
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 92/200. Took 0.88131 seconds. 
  Full-batch training loss = 0.002827, test loss = 0.201814
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 93/200. Took 0.87917 seconds. 
  Full-batch training loss = 0.002783, test loss = 0.202308
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 94/200. Took 0.86978 seconds. 
  Full-batch training loss = 0.002742, test loss = 0.202397
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 95/200. Took 0.87021 seconds. 
  Full-batch training loss = 0.002700, test loss = 0.202421
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 96/200. Took 0.87114 seconds. 
  Full-batch training loss = 0.002661, test loss = 0.202516
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 97/200. Took 0.87901 seconds. 
  Full-batch training loss = 0.002621, test loss = 0.202819
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 98/200. Took 0.88071 seconds. 
  Full-batch training loss = 0.002585, test loss = 0.202982
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 99/200. Took 0.87588 seconds. 
  Full-batch training loss = 0.002548, test loss = 0.203063
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 100/200. Took 0.87103 seconds. 
  Full-batch training loss = 0.002513, test loss = 0.203068
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 101/200. Took 0.86997 seconds. 
  Full-batch training loss = 0.002478, test loss = 0.203418
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 102/200. Took 0.86519 seconds. 
  Full-batch training loss = 0.002444, test loss = 0.203595
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 103/200. Took 0.87309 seconds. 
  Full-batch training loss = 0.002411, test loss = 0.203795
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 104/200. Took 0.86999 seconds. 
  Full-batch training loss = 0.002380, test loss = 0.203844
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 105/200. Took 0.87467 seconds. 
  Full-batch training loss = 0.002348, test loss = 0.204155
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 106/200. Took 0.87828 seconds. 
  Full-batch training loss = 0.002317, test loss = 0.204268
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 107/200. Took 0.8696 seconds. 
  Full-batch training loss = 0.002287, test loss = 0.204506
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 108/200. Took 0.87522 seconds. 
  Full-batch training loss = 0.002259, test loss = 0.204476
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 109/200. Took 0.88209 seconds. 
  Full-batch training loss = 0.002229, test loss = 0.204758
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 110/200. Took 0.86999 seconds. 
  Full-batch training loss = 0.002201, test loss = 0.204937
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 111/200. Took 0.86499 seconds. 
  Full-batch training loss = 0.002174, test loss = 0.205130
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 112/200. Took 0.88363 seconds. 
  Full-batch training loss = 0.002148, test loss = 0.205334
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 113/200. Took 0.86911 seconds. 
  Full-batch training loss = 0.002122, test loss = 0.205319
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 114/200. Took 0.86081 seconds. 
  Full-batch training loss = 0.002096, test loss = 0.205626
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 115/200. Took 0.88247 seconds. 
  Full-batch training loss = 0.002071, test loss = 0.205633
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 116/200. Took 0.87734 seconds. 
  Full-batch training loss = 0.002047, test loss = 0.205933
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 117/200. Took 0.88045 seconds. 
  Full-batch training loss = 0.002023, test loss = 0.206045
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 118/200. Took 0.87741 seconds. 
  Full-batch training loss = 0.002000, test loss = 0.206300
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 119/200. Took 0.87654 seconds. 
  Full-batch training loss = 0.001976, test loss = 0.206319
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 120/200. Took 0.87355 seconds. 
  Full-batch training loss = 0.001954, test loss = 0.206464
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 121/200. Took 0.87625 seconds. 
  Full-batch training loss = 0.001932, test loss = 0.206600
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 122/200. Took 0.86413 seconds. 
  Full-batch training loss = 0.001910, test loss = 0.206680
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 123/200. Took 0.879 seconds. 
  Full-batch training loss = 0.001890, test loss = 0.206777
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 124/200. Took 0.87718 seconds. 
  Full-batch training loss = 0.001868, test loss = 0.206977
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 125/200. Took 0.87372 seconds. 
  Full-batch training loss = 0.001849, test loss = 0.207108
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 126/200. Took 0.87321 seconds. 
  Full-batch training loss = 0.001828, test loss = 0.207275
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 127/200. Took 0.87749 seconds. 
  Full-batch training loss = 0.001809, test loss = 0.207361
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 128/200. Took 0.86799 seconds. 
  Full-batch training loss = 0.001790, test loss = 0.207537
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 129/200. Took 0.87971 seconds. 
  Full-batch training loss = 0.001772, test loss = 0.207610
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 130/200. Took 0.86594 seconds. 
  Full-batch training loss = 0.001753, test loss = 0.207715
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 131/200. Took 0.86944 seconds. 
  Full-batch training loss = 0.001735, test loss = 0.207945
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 132/200. Took 0.86642 seconds. 
  Full-batch training loss = 0.001718, test loss = 0.208020
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 133/200. Took 0.88249 seconds. 
  Full-batch training loss = 0.001700, test loss = 0.208157
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 134/200. Took 0.86949 seconds. 
  Full-batch training loss = 0.001683, test loss = 0.208231
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 135/200. Took 0.87116 seconds. 
  Full-batch training loss = 0.001666, test loss = 0.208457
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 136/200. Took 0.87206 seconds. 
  Full-batch training loss = 0.001650, test loss = 0.208551
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 137/200. Took 0.8745 seconds. 
  Full-batch training loss = 0.001634, test loss = 0.208693
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 138/200. Took 0.87641 seconds. 
  Full-batch training loss = 0.001618, test loss = 0.208778
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 139/200. Took 0.86951 seconds. 
  Full-batch training loss = 0.001603, test loss = 0.209033
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 140/200. Took 0.8808 seconds. 
  Full-batch training loss = 0.001587, test loss = 0.209065
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 141/200. Took 0.87429 seconds. 
  Full-batch training loss = 0.001572, test loss = 0.209194
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 142/200. Took 0.86912 seconds. 
  Full-batch training loss = 0.001558, test loss = 0.209323
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 143/200. Took 0.87688 seconds. 
  Full-batch training loss = 0.001543, test loss = 0.209472
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 144/200. Took 0.88105 seconds. 
  Full-batch training loss = 0.001529, test loss = 0.209644
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 145/200. Took 0.87156 seconds. 
  Full-batch training loss = 0.001515, test loss = 0.209711
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 146/200. Took 0.87901 seconds. 
  Full-batch training loss = 0.001501, test loss = 0.209830
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 147/200. Took 0.87317 seconds. 
  Full-batch training loss = 0.001487, test loss = 0.210012
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 148/200. Took 0.87418 seconds. 
  Full-batch training loss = 0.001474, test loss = 0.209971
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 149/200. Took 0.87228 seconds. 
  Full-batch training loss = 0.001461, test loss = 0.210192
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 150/200. Took 0.86937 seconds. 
  Full-batch training loss = 0.001448, test loss = 0.210244
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 151/200. Took 0.87865 seconds. 
  Full-batch training loss = 0.001435, test loss = 0.210459
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 152/200. Took 0.8785 seconds. 
  Full-batch training loss = 0.001423, test loss = 0.210515
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 153/200. Took 0.86921 seconds. 
  Full-batch training loss = 0.001411, test loss = 0.210594
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 154/200. Took 0.86641 seconds. 
  Full-batch training loss = 0.001399, test loss = 0.210673
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 155/200. Took 0.87763 seconds. 
  Full-batch training loss = 0.001386, test loss = 0.210843
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 156/200. Took 0.86521 seconds. 
  Full-batch training loss = 0.001375, test loss = 0.211016
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 157/200. Took 0.87765 seconds. 
  Full-batch training loss = 0.001363, test loss = 0.211058
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 158/200. Took 0.87787 seconds. 
  Full-batch training loss = 0.001352, test loss = 0.211190
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 159/200. Took 0.87054 seconds. 
  Full-batch training loss = 0.001341, test loss = 0.211265
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 160/200. Took 0.87659 seconds. 
  Full-batch training loss = 0.001330, test loss = 0.211405
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 161/200. Took 0.87694 seconds. 
  Full-batch training loss = 0.001319, test loss = 0.211547
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 162/200. Took 0.87504 seconds. 
  Full-batch training loss = 0.001308, test loss = 0.211634
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 163/200. Took 0.87693 seconds. 
  Full-batch training loss = 0.001298, test loss = 0.211750
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 164/200. Took 0.88089 seconds. 
  Full-batch training loss = 0.001288, test loss = 0.211792
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 165/200. Took 0.8741 seconds. 
  Full-batch training loss = 0.001277, test loss = 0.211939
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 166/200. Took 0.87189 seconds. 
  Full-batch training loss = 0.001267, test loss = 0.212053
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 167/200. Took 0.87409 seconds. 
  Full-batch training loss = 0.001257, test loss = 0.212189
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 168/200. Took 0.87171 seconds. 
  Full-batch training loss = 0.001247, test loss = 0.212272
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 169/200. Took 0.86417 seconds. 
  Full-batch training loss = 0.001237, test loss = 0.212384
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 170/200. Took 0.88112 seconds. 
  Full-batch training loss = 0.001228, test loss = 0.212489
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 171/200. Took 0.87637 seconds. 
  Full-batch training loss = 0.001219, test loss = 0.212555
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 172/200. Took 0.87497 seconds. 
  Full-batch training loss = 0.001209, test loss = 0.212665
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 173/200. Took 0.87573 seconds. 
  Full-batch training loss = 0.001200, test loss = 0.212764
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 174/200. Took 0.87576 seconds. 
  Full-batch training loss = 0.001191, test loss = 0.212884
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 175/200. Took 0.86702 seconds. 
  Full-batch training loss = 0.001182, test loss = 0.212984
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 176/200. Took 0.87418 seconds. 
  Full-batch training loss = 0.001174, test loss = 0.213051
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 177/200. Took 0.86794 seconds. 
  Full-batch training loss = 0.001165, test loss = 0.213180
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 178/200. Took 0.86788 seconds. 
  Full-batch training loss = 0.001157, test loss = 0.213301
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 179/200. Took 0.88326 seconds. 
  Full-batch training loss = 0.001148, test loss = 0.213408
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 180/200. Took 0.86462 seconds. 
  Full-batch training loss = 0.001140, test loss = 0.213456
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 181/200. Took 0.8768 seconds. 
  Full-batch training loss = 0.001132, test loss = 0.213633
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 182/200. Took 0.875 seconds. 
  Full-batch training loss = 0.001124, test loss = 0.213595
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 183/200. Took 0.86656 seconds. 
  Full-batch training loss = 0.001116, test loss = 0.213697
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 184/200. Took 0.86548 seconds. 
  Full-batch training loss = 0.001108, test loss = 0.213784
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 185/200. Took 0.86283 seconds. 
  Full-batch training loss = 0.001100, test loss = 0.213886
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 186/200. Took 0.86764 seconds. 
  Full-batch training loss = 0.001092, test loss = 0.214037
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 187/200. Took 0.86812 seconds. 
  Full-batch training loss = 0.001085, test loss = 0.214127
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 188/200. Took 0.87717 seconds. 
  Full-batch training loss = 0.001077, test loss = 0.214152
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 189/200. Took 0.86816 seconds. 
  Full-batch training loss = 0.001070, test loss = 0.214314
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 190/200. Took 0.87559 seconds. 
  Full-batch training loss = 0.001063, test loss = 0.214478
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 191/200. Took 0.87343 seconds. 
  Full-batch training loss = 0.001055, test loss = 0.214555
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 192/200. Took 0.8723 seconds. 
  Full-batch training loss = 0.001048, test loss = 0.214537
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 193/200. Took 0.86216 seconds. 
  Full-batch training loss = 0.001041, test loss = 0.214711
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 194/200. Took 0.86601 seconds. 
  Full-batch training loss = 0.001034, test loss = 0.214827
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 195/200. Took 0.8681 seconds. 
  Full-batch training loss = 0.001027, test loss = 0.214859
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 196/200. Took 0.87017 seconds. 
  Full-batch training loss = 0.001021, test loss = 0.214876
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 197/200. Took 0.88077 seconds. 
  Full-batch training loss = 0.001014, test loss = 0.215071
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 198/200. Took 0.87722 seconds. 
  Full-batch training loss = 0.001007, test loss = 0.215138
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 199/200. Took 0.87366 seconds. 
  Full-batch training loss = 0.001001, test loss = 0.215204
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 200/200. Took 0.88631 seconds. 
  Full-batch training loss = 0.000994, test loss = 0.215262
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
Elapsed time is 419.122305 seconds.
End Training
