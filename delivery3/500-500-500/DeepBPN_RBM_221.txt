==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 221/270
----------------------------------------------------------
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	200
	-> LearningRate	0.10
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 1.0916 seconds. Average reconstruction error is: 27.4532
 epoch 2/20. Took 1.1561 seconds. Average reconstruction error is: 16.2565
 epoch 3/20. Took 1.1362 seconds. Average reconstruction error is: 13.9893
 epoch 4/20. Took 1.1351 seconds. Average reconstruction error is: 12.6357
 epoch 5/20. Took 1.1121 seconds. Average reconstruction error is: 11.6615
 epoch 6/20. Took 1.127 seconds. Average reconstruction error is: 11.0029
 epoch 7/20. Took 1.108 seconds. Average reconstruction error is: 10.4901
 epoch 8/20. Took 1.0953 seconds. Average reconstruction error is: 10.142
 epoch 9/20. Took 1.1591 seconds. Average reconstruction error is: 9.8749
 epoch 10/20. Took 1.1029 seconds. Average reconstruction error is: 9.5992
 epoch 11/20. Took 1.1318 seconds. Average reconstruction error is: 9.4655
 epoch 12/20. Took 1.122 seconds. Average reconstruction error is: 9.3263
 epoch 13/20. Took 1.1205 seconds. Average reconstruction error is: 9.151
 epoch 14/20. Took 1.087 seconds. Average reconstruction error is: 9.0197
 epoch 15/20. Took 1.1003 seconds. Average reconstruction error is: 8.9435
 epoch 16/20. Took 1.1037 seconds. Average reconstruction error is: 8.8549
 epoch 17/20. Took 1.1108 seconds. Average reconstruction error is: 8.762
 epoch 18/20. Took 1.1282 seconds. Average reconstruction error is: 8.7181
 epoch 19/20. Took 1.1995 seconds. Average reconstruction error is: 8.6222
 epoch 20/20. Took 1.1063 seconds. Average reconstruction error is: 8.5503
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.76304 seconds. Average reconstruction error is: 18.8943
 epoch 2/20. Took 0.8584 seconds. Average reconstruction error is: 10.6115
 epoch 3/20. Took 0.73657 seconds. Average reconstruction error is: 9.3452
 epoch 4/20. Took 0.80812 seconds. Average reconstruction error is: 8.6803
 epoch 5/20. Took 0.75999 seconds. Average reconstruction error is: 8.2646
 epoch 6/20. Took 0.77275 seconds. Average reconstruction error is: 7.9381
 epoch 7/20. Took 0.77003 seconds. Average reconstruction error is: 7.7055
 epoch 8/20. Took 0.80795 seconds. Average reconstruction error is: 7.4761
 epoch 9/20. Took 0.80593 seconds. Average reconstruction error is: 7.3263
 epoch 10/20. Took 0.74882 seconds. Average reconstruction error is: 7.1407
 epoch 11/20. Took 0.74468 seconds. Average reconstruction error is: 7.0152
 epoch 12/20. Took 0.74441 seconds. Average reconstruction error is: 6.8954
 epoch 13/20. Took 0.74828 seconds. Average reconstruction error is: 6.754
 epoch 14/20. Took 0.75884 seconds. Average reconstruction error is: 6.6842
 epoch 15/20. Took 0.74945 seconds. Average reconstruction error is: 6.5896
 epoch 16/20. Took 0.7574 seconds. Average reconstruction error is: 6.5062
 epoch 17/20. Took 0.74344 seconds. Average reconstruction error is: 6.4059
 epoch 18/20. Took 0.74051 seconds. Average reconstruction error is: 6.3435
 epoch 19/20. Took 0.76404 seconds. Average reconstruction error is: 6.2861
 epoch 20/20. Took 0.74371 seconds. Average reconstruction error is: 6.2525
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.76309 seconds. Average reconstruction error is: 14.6927
 epoch 2/20. Took 0.74244 seconds. Average reconstruction error is: 7.4327
 epoch 3/20. Took 0.74511 seconds. Average reconstruction error is: 6.5611
 epoch 4/20. Took 0.74816 seconds. Average reconstruction error is: 6.1155
 epoch 5/20. Took 0.83466 seconds. Average reconstruction error is: 5.8563
 epoch 6/20. Took 0.7594 seconds. Average reconstruction error is: 5.6719
 epoch 7/20. Took 0.78044 seconds. Average reconstruction error is: 5.5462
 epoch 8/20. Took 0.79991 seconds. Average reconstruction error is: 5.4581
 epoch 9/20. Took 0.748 seconds. Average reconstruction error is: 5.3825
 epoch 10/20. Took 0.74503 seconds. Average reconstruction error is: 5.2848
 epoch 11/20. Took 0.74657 seconds. Average reconstruction error is: 5.2481
 epoch 12/20. Took 0.75209 seconds. Average reconstruction error is: 5.1861
 epoch 13/20. Took 0.74961 seconds. Average reconstruction error is: 5.1909
 epoch 14/20. Took 0.7397 seconds. Average reconstruction error is: 5.0955
 epoch 15/20. Took 0.7488 seconds. Average reconstruction error is: 5.0504
 epoch 16/20. Took 0.74726 seconds. Average reconstruction error is: 5.0207
 epoch 17/20. Took 0.74076 seconds. Average reconstruction error is: 5.0188
 epoch 18/20. Took 0.73919 seconds. Average reconstruction error is: 4.9779
 epoch 19/20. Took 0.73721 seconds. Average reconstruction error is: 4.9172
 epoch 20/20. Took 0.74032 seconds. Average reconstruction error is: 4.8834
Training NN  (784  500  500  500   10) with BackPropagation for 200 epochs
 epoch 1/200. Took 1.7411 seconds. 
  Full-batch training loss = 0.321123, test loss = 0.326609
  Training set accuracy = 0.915300, Test set accuracy = 0.911900
 epoch 2/200. Took 1.8808 seconds. 
  Full-batch training loss = 0.249096, test loss = 0.259595
  Training set accuracy = 0.931400, Test set accuracy = 0.930400
 epoch 3/200. Took 1.8198 seconds. 
  Full-batch training loss = 0.216794, test loss = 0.239066
  Training set accuracy = 0.941300, Test set accuracy = 0.933100
 epoch 4/200. Took 1.7829 seconds. 
  Full-batch training loss = 0.187688, test loss = 0.215050
  Training set accuracy = 0.949800, Test set accuracy = 0.939900
 epoch 5/200. Took 1.782 seconds. 
  Full-batch training loss = 0.170008, test loss = 0.205981
  Training set accuracy = 0.953400, Test set accuracy = 0.941000
 epoch 6/200. Took 1.7526 seconds. 
  Full-batch training loss = 0.155381, test loss = 0.195994
  Training set accuracy = 0.957900, Test set accuracy = 0.943300
 epoch 7/200. Took 1.7495 seconds. 
  Full-batch training loss = 0.142590, test loss = 0.189268
  Training set accuracy = 0.961400, Test set accuracy = 0.945900
 epoch 8/200. Took 1.7462 seconds. 
  Full-batch training loss = 0.133777, test loss = 0.185459
  Training set accuracy = 0.964000, Test set accuracy = 0.943700
 epoch 9/200. Took 1.7605 seconds. 
  Full-batch training loss = 0.120939, test loss = 0.175527
  Training set accuracy = 0.968200, Test set accuracy = 0.950200
 epoch 10/200. Took 1.7427 seconds. 
  Full-batch training loss = 0.112941, test loss = 0.171701
  Training set accuracy = 0.971000, Test set accuracy = 0.951200
 epoch 11/200. Took 1.7397 seconds. 
  Full-batch training loss = 0.105112, test loss = 0.168284
  Training set accuracy = 0.973000, Test set accuracy = 0.951900
 epoch 12/200. Took 1.7801 seconds. 
  Full-batch training loss = 0.098139, test loss = 0.165982
  Training set accuracy = 0.976300, Test set accuracy = 0.951700
 epoch 13/200. Took 1.8089 seconds. 
  Full-batch training loss = 0.093239, test loss = 0.164160
  Training set accuracy = 0.977400, Test set accuracy = 0.953100
 epoch 14/200. Took 1.8004 seconds. 
  Full-batch training loss = 0.087332, test loss = 0.161573
  Training set accuracy = 0.979900, Test set accuracy = 0.953400
 epoch 15/200. Took 1.7676 seconds. 
  Full-batch training loss = 0.082798, test loss = 0.161431
  Training set accuracy = 0.981300, Test set accuracy = 0.953500
 epoch 16/200. Took 1.7599 seconds. 
  Full-batch training loss = 0.076381, test loss = 0.156798
  Training set accuracy = 0.984500, Test set accuracy = 0.954400
 epoch 17/200. Took 1.7577 seconds. 
  Full-batch training loss = 0.073302, test loss = 0.156981
  Training set accuracy = 0.984300, Test set accuracy = 0.954500
 epoch 18/200. Took 1.8488 seconds. 
  Full-batch training loss = 0.068720, test loss = 0.155902
  Training set accuracy = 0.985500, Test set accuracy = 0.953700
 epoch 19/200. Took 1.7629 seconds. 
  Full-batch training loss = 0.064363, test loss = 0.153788
  Training set accuracy = 0.986600, Test set accuracy = 0.954200
 epoch 20/200. Took 1.7705 seconds. 
  Full-batch training loss = 0.061377, test loss = 0.154542
  Training set accuracy = 0.988500, Test set accuracy = 0.954700
 epoch 21/200. Took 1.7815 seconds. 
  Full-batch training loss = 0.057371, test loss = 0.151747
  Training set accuracy = 0.988400, Test set accuracy = 0.955200
 epoch 22/200. Took 1.7496 seconds. 
  Full-batch training loss = 0.054651, test loss = 0.151362
  Training set accuracy = 0.989700, Test set accuracy = 0.954600
 epoch 23/200. Took 1.7584 seconds. 
  Full-batch training loss = 0.051109, test loss = 0.150652
  Training set accuracy = 0.990800, Test set accuracy = 0.955500
 epoch 24/200. Took 1.7535 seconds. 
  Full-batch training loss = 0.048412, test loss = 0.149895
  Training set accuracy = 0.991600, Test set accuracy = 0.954800
 epoch 25/200. Took 1.8011 seconds. 
  Full-batch training loss = 0.045842, test loss = 0.148810
  Training set accuracy = 0.992200, Test set accuracy = 0.955700
 epoch 26/200. Took 1.7671 seconds. 
  Full-batch training loss = 0.043716, test loss = 0.148617
  Training set accuracy = 0.992800, Test set accuracy = 0.956100
 epoch 27/200. Took 1.7948 seconds. 
  Full-batch training loss = 0.041497, test loss = 0.148050
  Training set accuracy = 0.993300, Test set accuracy = 0.956300
 epoch 28/200. Took 1.8174 seconds. 
  Full-batch training loss = 0.039011, test loss = 0.147001
  Training set accuracy = 0.993300, Test set accuracy = 0.956600
 epoch 29/200. Took 1.7596 seconds. 
  Full-batch training loss = 0.037375, test loss = 0.148220
  Training set accuracy = 0.994200, Test set accuracy = 0.955000
 epoch 30/200. Took 1.7755 seconds. 
  Full-batch training loss = 0.036452, test loss = 0.147685
  Training set accuracy = 0.993800, Test set accuracy = 0.956100
 epoch 31/200. Took 1.7747 seconds. 
  Full-batch training loss = 0.033593, test loss = 0.146989
  Training set accuracy = 0.995200, Test set accuracy = 0.956500
 epoch 32/200. Took 1.7433 seconds. 
  Full-batch training loss = 0.032372, test loss = 0.149013
  Training set accuracy = 0.995200, Test set accuracy = 0.955700
 epoch 33/200. Took 1.8038 seconds. 
  Full-batch training loss = 0.030330, test loss = 0.146602
  Training set accuracy = 0.995800, Test set accuracy = 0.956100
 epoch 34/200. Took 1.8218 seconds. 
  Full-batch training loss = 0.029557, test loss = 0.146451
  Training set accuracy = 0.996600, Test set accuracy = 0.955700
 epoch 35/200. Took 1.7641 seconds. 
  Full-batch training loss = 0.027761, test loss = 0.146696
  Training set accuracy = 0.996700, Test set accuracy = 0.956500
 epoch 36/200. Took 1.7563 seconds. 
  Full-batch training loss = 0.026681, test loss = 0.147323
  Training set accuracy = 0.996500, Test set accuracy = 0.955800
 epoch 37/200. Took 1.7594 seconds. 
  Full-batch training loss = 0.025523, test loss = 0.147876
  Training set accuracy = 0.997200, Test set accuracy = 0.956700
 epoch 38/200. Took 1.7718 seconds. 
  Full-batch training loss = 0.023949, test loss = 0.146428
  Training set accuracy = 0.997300, Test set accuracy = 0.956500
 epoch 39/200. Took 1.8351 seconds. 
  Full-batch training loss = 0.022851, test loss = 0.146978
  Training set accuracy = 0.997700, Test set accuracy = 0.956500
 epoch 40/200. Took 1.8256 seconds. 
  Full-batch training loss = 0.021871, test loss = 0.147545
  Training set accuracy = 0.998000, Test set accuracy = 0.956200
 epoch 41/200. Took 1.8665 seconds. 
  Full-batch training loss = 0.021331, test loss = 0.146601
  Training set accuracy = 0.997900, Test set accuracy = 0.956900
 epoch 42/200. Took 1.8142 seconds. 
  Full-batch training loss = 0.020125, test loss = 0.146990
  Training set accuracy = 0.997800, Test set accuracy = 0.956900
 epoch 43/200. Took 1.7522 seconds. 
  Full-batch training loss = 0.019405, test loss = 0.146904
  Training set accuracy = 0.998200, Test set accuracy = 0.957100
 epoch 44/200. Took 1.837 seconds. 
  Full-batch training loss = 0.018393, test loss = 0.147008
  Training set accuracy = 0.998300, Test set accuracy = 0.957300
 epoch 45/200. Took 1.748 seconds. 
  Full-batch training loss = 0.017688, test loss = 0.146986
  Training set accuracy = 0.998500, Test set accuracy = 0.956800
 epoch 46/200. Took 1.7462 seconds. 
  Full-batch training loss = 0.017080, test loss = 0.146759
  Training set accuracy = 0.998600, Test set accuracy = 0.956800
 epoch 47/200. Took 1.8237 seconds. 
  Full-batch training loss = 0.016504, test loss = 0.148433
  Training set accuracy = 0.998800, Test set accuracy = 0.956300
 epoch 48/200. Took 1.7434 seconds. 
  Full-batch training loss = 0.015803, test loss = 0.147745
  Training set accuracy = 0.998700, Test set accuracy = 0.957400
 epoch 49/200. Took 1.7377 seconds. 
  Full-batch training loss = 0.015291, test loss = 0.148758
  Training set accuracy = 0.999100, Test set accuracy = 0.957300
 epoch 50/200. Took 1.7548 seconds. 
  Full-batch training loss = 0.014736, test loss = 0.147987
  Training set accuracy = 0.999300, Test set accuracy = 0.957000
 epoch 51/200. Took 1.7335 seconds. 
  Full-batch training loss = 0.014195, test loss = 0.147997
  Training set accuracy = 0.999500, Test set accuracy = 0.957100
 epoch 52/200. Took 1.7533 seconds. 
  Full-batch training loss = 0.013607, test loss = 0.147919
  Training set accuracy = 0.999600, Test set accuracy = 0.957500
 epoch 53/200. Took 1.7522 seconds. 
  Full-batch training loss = 0.013190, test loss = 0.148292
  Training set accuracy = 0.999600, Test set accuracy = 0.956400
 epoch 54/200. Took 1.7846 seconds. 
  Full-batch training loss = 0.012758, test loss = 0.148359
  Training set accuracy = 0.999800, Test set accuracy = 0.957600
 epoch 55/200. Took 1.8042 seconds. 
  Full-batch training loss = 0.012448, test loss = 0.149391
  Training set accuracy = 0.999700, Test set accuracy = 0.957900
 epoch 56/200. Took 1.7628 seconds. 
  Full-batch training loss = 0.011992, test loss = 0.148902
  Training set accuracy = 0.999700, Test set accuracy = 0.957400
 epoch 57/200. Took 1.764 seconds. 
  Full-batch training loss = 0.011594, test loss = 0.149601
  Training set accuracy = 0.999700, Test set accuracy = 0.957400
 epoch 58/200. Took 1.748 seconds. 
  Full-batch training loss = 0.011268, test loss = 0.149240
  Training set accuracy = 0.999800, Test set accuracy = 0.957800
 epoch 59/200. Took 1.752 seconds. 
  Full-batch training loss = 0.010856, test loss = 0.148749
  Training set accuracy = 0.999800, Test set accuracy = 0.958300
 epoch 60/200. Took 1.767 seconds. 
  Full-batch training loss = 0.010488, test loss = 0.149646
  Training set accuracy = 0.999800, Test set accuracy = 0.957500
 epoch 61/200. Took 1.7669 seconds. 
  Full-batch training loss = 0.010230, test loss = 0.149376
  Training set accuracy = 0.999800, Test set accuracy = 0.958200
 epoch 62/200. Took 1.8174 seconds. 
  Full-batch training loss = 0.009936, test loss = 0.149681
  Training set accuracy = 0.999800, Test set accuracy = 0.957800
 epoch 63/200. Took 1.8226 seconds. 
  Full-batch training loss = 0.009620, test loss = 0.150199
  Training set accuracy = 0.999900, Test set accuracy = 0.957700
 epoch 64/200. Took 1.8585 seconds. 
  Full-batch training loss = 0.009418, test loss = 0.150506
  Training set accuracy = 0.999800, Test set accuracy = 0.957100
 epoch 65/200. Took 1.8228 seconds. 
  Full-batch training loss = 0.009167, test loss = 0.150479
  Training set accuracy = 0.999900, Test set accuracy = 0.957300
 epoch 66/200. Took 1.8487 seconds. 
  Full-batch training loss = 0.008873, test loss = 0.150273
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 67/200. Took 1.888 seconds. 
  Full-batch training loss = 0.008721, test loss = 0.149918
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 68/200. Took 1.8346 seconds. 
  Full-batch training loss = 0.008468, test loss = 0.150699
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 69/200. Took 1.8565 seconds. 
  Full-batch training loss = 0.008201, test loss = 0.150849
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 70/200. Took 1.7874 seconds. 
  Full-batch training loss = 0.008019, test loss = 0.150826
  Training set accuracy = 0.999900, Test set accuracy = 0.958500
 epoch 71/200. Took 1.7816 seconds. 
  Full-batch training loss = 0.007855, test loss = 0.151756
  Training set accuracy = 0.999900, Test set accuracy = 0.957300
 epoch 72/200. Took 1.8234 seconds. 
  Full-batch training loss = 0.007642, test loss = 0.151279
  Training set accuracy = 0.999900, Test set accuracy = 0.957600
 epoch 73/200. Took 1.7708 seconds. 
  Full-batch training loss = 0.007546, test loss = 0.151814
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 74/200. Took 1.8908 seconds. 
  Full-batch training loss = 0.007306, test loss = 0.151686
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 75/200. Took 1.8907 seconds. 
  Full-batch training loss = 0.007141, test loss = 0.152010
  Training set accuracy = 0.999900, Test set accuracy = 0.958100
 epoch 76/200. Took 1.8517 seconds. 
  Full-batch training loss = 0.006980, test loss = 0.152049
  Training set accuracy = 0.999900, Test set accuracy = 0.957700
 epoch 77/200. Took 1.8159 seconds. 
  Full-batch training loss = 0.006813, test loss = 0.152275
  Training set accuracy = 0.999900, Test set accuracy = 0.958100
 epoch 78/200. Took 1.8306 seconds. 
  Full-batch training loss = 0.006673, test loss = 0.151935
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 79/200. Took 1.7856 seconds. 
  Full-batch training loss = 0.006528, test loss = 0.152749
  Training set accuracy = 0.999900, Test set accuracy = 0.957900
 epoch 80/200. Took 1.7856 seconds. 
  Full-batch training loss = 0.006386, test loss = 0.152531
  Training set accuracy = 0.999900, Test set accuracy = 0.958200
 epoch 81/200. Took 1.9322 seconds. 
  Full-batch training loss = 0.006268, test loss = 0.152558
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 82/200. Took 1.9053 seconds. 
  Full-batch training loss = 0.006130, test loss = 0.152733
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 83/200. Took 1.802 seconds. 
  Full-batch training loss = 0.006014, test loss = 0.153131
  Training set accuracy = 0.999900, Test set accuracy = 0.958400
 epoch 84/200. Took 1.8282 seconds. 
  Full-batch training loss = 0.005892, test loss = 0.153328
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 85/200. Took 1.7708 seconds. 
  Full-batch training loss = 0.005797, test loss = 0.153120
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 86/200. Took 1.8572 seconds. 
  Full-batch training loss = 0.005693, test loss = 0.153485
  Training set accuracy = 1.000000, Test set accuracy = 0.957700
 epoch 87/200. Took 1.8053 seconds. 
  Full-batch training loss = 0.005572, test loss = 0.153902
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 88/200. Took 1.8306 seconds. 
  Full-batch training loss = 0.005464, test loss = 0.153932
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 89/200. Took 1.8164 seconds. 
  Full-batch training loss = 0.005358, test loss = 0.153854
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 90/200. Took 1.9336 seconds. 
  Full-batch training loss = 0.005287, test loss = 0.153697
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 91/200. Took 1.8587 seconds. 
  Full-batch training loss = 0.005173, test loss = 0.154300
  Training set accuracy = 1.000000, Test set accuracy = 0.957900
 epoch 92/200. Took 1.8141 seconds. 
  Full-batch training loss = 0.005077, test loss = 0.154435
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 93/200. Took 1.8059 seconds. 
  Full-batch training loss = 0.005010, test loss = 0.154219
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 94/200. Took 1.766 seconds. 
  Full-batch training loss = 0.004913, test loss = 0.154991
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 95/200. Took 1.7631 seconds. 
  Full-batch training loss = 0.004825, test loss = 0.154956
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 96/200. Took 1.7645 seconds. 
  Full-batch training loss = 0.004742, test loss = 0.154691
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 97/200. Took 1.7683 seconds. 
  Full-batch training loss = 0.004667, test loss = 0.154984
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 98/200. Took 1.7437 seconds. 
  Full-batch training loss = 0.004589, test loss = 0.155200
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 99/200. Took 1.7531 seconds. 
  Full-batch training loss = 0.004523, test loss = 0.155240
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 100/200. Took 1.7571 seconds. 
  Full-batch training loss = 0.004445, test loss = 0.155634
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 101/200. Took 1.7536 seconds. 
  Full-batch training loss = 0.004373, test loss = 0.155557
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 102/200. Took 1.76 seconds. 
  Full-batch training loss = 0.004310, test loss = 0.155764
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 103/200. Took 1.7757 seconds. 
  Full-batch training loss = 0.004246, test loss = 0.156064
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 104/200. Took 1.761 seconds. 
  Full-batch training loss = 0.004179, test loss = 0.156013
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 105/200. Took 1.7502 seconds. 
  Full-batch training loss = 0.004118, test loss = 0.156179
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 106/200. Took 1.7671 seconds. 
  Full-batch training loss = 0.004054, test loss = 0.156280
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 107/200. Took 1.8574 seconds. 
  Full-batch training loss = 0.004001, test loss = 0.156185
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 108/200. Took 1.9522 seconds. 
  Full-batch training loss = 0.003944, test loss = 0.156264
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 109/200. Took 1.8465 seconds. 
  Full-batch training loss = 0.003889, test loss = 0.156684
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 110/200. Took 1.7474 seconds. 
  Full-batch training loss = 0.003835, test loss = 0.156960
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 111/200. Took 1.7547 seconds. 
  Full-batch training loss = 0.003781, test loss = 0.156857
  Training set accuracy = 1.000000, Test set accuracy = 0.958100
 epoch 112/200. Took 1.7446 seconds. 
  Full-batch training loss = 0.003724, test loss = 0.157016
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 113/200. Took 1.7638 seconds. 
  Full-batch training loss = 0.003674, test loss = 0.157177
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 114/200. Took 1.7435 seconds. 
  Full-batch training loss = 0.003627, test loss = 0.157383
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 115/200. Took 1.7903 seconds. 
  Full-batch training loss = 0.003583, test loss = 0.157298
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 116/200. Took 1.7476 seconds. 
  Full-batch training loss = 0.003533, test loss = 0.157854
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 117/200. Took 1.7676 seconds. 
  Full-batch training loss = 0.003484, test loss = 0.157828
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 118/200. Took 1.7457 seconds. 
  Full-batch training loss = 0.003439, test loss = 0.157709
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 119/200. Took 1.7463 seconds. 
  Full-batch training loss = 0.003394, test loss = 0.157914
  Training set accuracy = 1.000000, Test set accuracy = 0.958000
 epoch 120/200. Took 1.7685 seconds. 
  Full-batch training loss = 0.003358, test loss = 0.157728
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 121/200. Took 1.7564 seconds. 
  Full-batch training loss = 0.003311, test loss = 0.158253
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 122/200. Took 1.7503 seconds. 
  Full-batch training loss = 0.003270, test loss = 0.158273
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 123/200. Took 1.7622 seconds. 
  Full-batch training loss = 0.003231, test loss = 0.158520
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 124/200. Took 1.7644 seconds. 
  Full-batch training loss = 0.003188, test loss = 0.158351
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 125/200. Took 1.7524 seconds. 
  Full-batch training loss = 0.003151, test loss = 0.158570
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 126/200. Took 1.7521 seconds. 
  Full-batch training loss = 0.003114, test loss = 0.158691
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 127/200. Took 1.7434 seconds. 
  Full-batch training loss = 0.003077, test loss = 0.158903
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 128/200. Took 1.7531 seconds. 
  Full-batch training loss = 0.003041, test loss = 0.159011
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 129/200. Took 1.7506 seconds. 
  Full-batch training loss = 0.003005, test loss = 0.159147
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 130/200. Took 1.7808 seconds. 
  Full-batch training loss = 0.002972, test loss = 0.159128
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 131/200. Took 1.7546 seconds. 
  Full-batch training loss = 0.002936, test loss = 0.159291
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 132/200. Took 1.7967 seconds. 
  Full-batch training loss = 0.002902, test loss = 0.159332
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 133/200. Took 1.7603 seconds. 
  Full-batch training loss = 0.002871, test loss = 0.159415
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 134/200. Took 1.7453 seconds. 
  Full-batch training loss = 0.002839, test loss = 0.159487
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 135/200. Took 1.751 seconds. 
  Full-batch training loss = 0.002812, test loss = 0.159364
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 136/200. Took 1.7523 seconds. 
  Full-batch training loss = 0.002777, test loss = 0.159892
  Training set accuracy = 1.000000, Test set accuracy = 0.958200
 epoch 137/200. Took 1.9913 seconds. 
  Full-batch training loss = 0.002752, test loss = 0.160113
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 138/200. Took 1.7924 seconds. 
  Full-batch training loss = 0.002719, test loss = 0.160157
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 139/200. Took 1.9317 seconds. 
  Full-batch training loss = 0.002689, test loss = 0.160205
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 140/200. Took 1.792 seconds. 
  Full-batch training loss = 0.002662, test loss = 0.160246
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 141/200. Took 1.7677 seconds. 
  Full-batch training loss = 0.002633, test loss = 0.160214
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 142/200. Took 1.8314 seconds. 
  Full-batch training loss = 0.002607, test loss = 0.160280
  Training set accuracy = 1.000000, Test set accuracy = 0.958400
 epoch 143/200. Took 1.7988 seconds. 
  Full-batch training loss = 0.002579, test loss = 0.160532
  Training set accuracy = 1.000000, Test set accuracy = 0.958300
 epoch 144/200. Took 1.7883 seconds. 
  Full-batch training loss = 0.002553, test loss = 0.160645
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 145/200. Took 1.7575 seconds. 
  Full-batch training loss = 0.002530, test loss = 0.160904
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 146/200. Took 1.9031 seconds. 
  Full-batch training loss = 0.002502, test loss = 0.160679
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 147/200. Took 1.8252 seconds. 
  Full-batch training loss = 0.002478, test loss = 0.160778
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 148/200. Took 1.8747 seconds. 
  Full-batch training loss = 0.002453, test loss = 0.161048
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 149/200. Took 1.9087 seconds. 
  Full-batch training loss = 0.002428, test loss = 0.161142
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 150/200. Took 1.7518 seconds. 
  Full-batch training loss = 0.002406, test loss = 0.161296
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 151/200. Took 1.7472 seconds. 
  Full-batch training loss = 0.002383, test loss = 0.161320
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 152/200. Took 1.7454 seconds. 
  Full-batch training loss = 0.002360, test loss = 0.161198
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 153/200. Took 1.7703 seconds. 
  Full-batch training loss = 0.002336, test loss = 0.161403
  Training set accuracy = 1.000000, Test set accuracy = 0.958500
 epoch 154/200. Took 1.7483 seconds. 
  Full-batch training loss = 0.002316, test loss = 0.161537
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 155/200. Took 1.7442 seconds. 
  Full-batch training loss = 0.002293, test loss = 0.161629
  Training set accuracy = 1.000000, Test set accuracy = 0.958700
 epoch 156/200. Took 1.7803 seconds. 
  Full-batch training loss = 0.002273, test loss = 0.161726
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 157/200. Took 1.7722 seconds. 
  Full-batch training loss = 0.002253, test loss = 0.161697
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 158/200. Took 1.7626 seconds. 
  Full-batch training loss = 0.002231, test loss = 0.161864
  Training set accuracy = 1.000000, Test set accuracy = 0.958600
 epoch 159/200. Took 1.7892 seconds. 
  Full-batch training loss = 0.002210, test loss = 0.162016
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 160/200. Took 1.7846 seconds. 
  Full-batch training loss = 0.002190, test loss = 0.162078
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 161/200. Took 1.7705 seconds. 
  Full-batch training loss = 0.002172, test loss = 0.162069
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 162/200. Took 1.7371 seconds. 
  Full-batch training loss = 0.002152, test loss = 0.162283
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 163/200. Took 1.8022 seconds. 
  Full-batch training loss = 0.002133, test loss = 0.162390
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 164/200. Took 1.7835 seconds. 
  Full-batch training loss = 0.002114, test loss = 0.162431
  Training set accuracy = 1.000000, Test set accuracy = 0.958800
 epoch 165/200. Took 1.7499 seconds. 
  Full-batch training loss = 0.002096, test loss = 0.162582
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 166/200. Took 1.7662 seconds. 
  Full-batch training loss = 0.002078, test loss = 0.162580
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 167/200. Took 1.7557 seconds. 
  Full-batch training loss = 0.002060, test loss = 0.162657
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 168/200. Took 1.7624 seconds. 
  Full-batch training loss = 0.002042, test loss = 0.162816
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 169/200. Took 1.7575 seconds. 
  Full-batch training loss = 0.002026, test loss = 0.162960
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 170/200. Took 1.7702 seconds. 
  Full-batch training loss = 0.002009, test loss = 0.162987
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 171/200. Took 1.781 seconds. 
  Full-batch training loss = 0.001992, test loss = 0.163078
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 172/200. Took 1.753 seconds. 
  Full-batch training loss = 0.001976, test loss = 0.163060
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 173/200. Took 1.7651 seconds. 
  Full-batch training loss = 0.001959, test loss = 0.163085
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 174/200. Took 1.7542 seconds. 
  Full-batch training loss = 0.001943, test loss = 0.163294
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 175/200. Took 1.7564 seconds. 
  Full-batch training loss = 0.001927, test loss = 0.163401
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 176/200. Took 1.7365 seconds. 
  Full-batch training loss = 0.001912, test loss = 0.163434
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 177/200. Took 1.7668 seconds. 
  Full-batch training loss = 0.001897, test loss = 0.163552
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 178/200. Took 1.7615 seconds. 
  Full-batch training loss = 0.001882, test loss = 0.163540
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 179/200. Took 1.7605 seconds. 
  Full-batch training loss = 0.001867, test loss = 0.163652
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 180/200. Took 1.7503 seconds. 
  Full-batch training loss = 0.001852, test loss = 0.163751
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 181/200. Took 1.7604 seconds. 
  Full-batch training loss = 0.001838, test loss = 0.163681
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 182/200. Took 1.7665 seconds. 
  Full-batch training loss = 0.001823, test loss = 0.163896
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 183/200. Took 1.7544 seconds. 
  Full-batch training loss = 0.001809, test loss = 0.164045
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 184/200. Took 1.7559 seconds. 
  Full-batch training loss = 0.001796, test loss = 0.164115
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 185/200. Took 1.7553 seconds. 
  Full-batch training loss = 0.001782, test loss = 0.164087
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 186/200. Took 1.7751 seconds. 
  Full-batch training loss = 0.001769, test loss = 0.164253
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 187/200. Took 1.766 seconds. 
  Full-batch training loss = 0.001756, test loss = 0.164364
  Training set accuracy = 1.000000, Test set accuracy = 0.959000
 epoch 188/200. Took 1.7373 seconds. 
  Full-batch training loss = 0.001743, test loss = 0.164479
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 189/200. Took 1.7538 seconds. 
  Full-batch training loss = 0.001729, test loss = 0.164492
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 190/200. Took 1.8144 seconds. 
  Full-batch training loss = 0.001716, test loss = 0.164599
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 191/200. Took 1.8425 seconds. 
  Full-batch training loss = 0.001704, test loss = 0.164591
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 192/200. Took 1.7517 seconds. 
  Full-batch training loss = 0.001691, test loss = 0.164681
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 193/200. Took 1.7589 seconds. 
  Full-batch training loss = 0.001679, test loss = 0.164777
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 194/200. Took 1.7521 seconds. 
  Full-batch training loss = 0.001668, test loss = 0.164807
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 195/200. Took 1.8355 seconds. 
  Full-batch training loss = 0.001656, test loss = 0.164960
  Training set accuracy = 1.000000, Test set accuracy = 0.959300
 epoch 196/200. Took 1.8421 seconds. 
  Full-batch training loss = 0.001643, test loss = 0.165001
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 197/200. Took 1.8777 seconds. 
  Full-batch training loss = 0.001632, test loss = 0.165128
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
 epoch 198/200. Took 1.8234 seconds. 
  Full-batch training loss = 0.001622, test loss = 0.165225
  Training set accuracy = 1.000000, Test set accuracy = 0.959200
 epoch 199/200. Took 1.784 seconds. 
  Full-batch training loss = 0.001609, test loss = 0.165166
  Training set accuracy = 1.000000, Test set accuracy = 0.958900
 epoch 200/200. Took 1.7611 seconds. 
  Full-batch training loss = 0.001598, test loss = 0.165282
  Training set accuracy = 1.000000, Test set accuracy = 0.959100
Elapsed time is 692.593442 seconds.
End Training
