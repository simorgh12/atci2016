==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 21/270
----------------------------------------------------------
* ReducedData	2000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.20
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 2000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.20154 seconds. Average reconstruction error is: 49.3908
 epoch 2/20. Took 0.19983 seconds. Average reconstruction error is: 26.3939
 epoch 3/20. Took 0.20011 seconds. Average reconstruction error is: 21.6993
 epoch 4/20. Took 0.20294 seconds. Average reconstruction error is: 19.1892
 epoch 5/20. Took 0.21498 seconds. Average reconstruction error is: 17.566
 epoch 6/20. Took 0.2058 seconds. Average reconstruction error is: 16.3917
 epoch 7/20. Took 0.20067 seconds. Average reconstruction error is: 15.5471
 epoch 8/20. Took 0.20282 seconds. Average reconstruction error is: 14.9023
 epoch 9/20. Took 0.20265 seconds. Average reconstruction error is: 14.3088
 epoch 10/20. Took 0.20042 seconds. Average reconstruction error is: 13.7109
 epoch 11/20. Took 0.20344 seconds. Average reconstruction error is: 13.3753
 epoch 12/20. Took 0.20393 seconds. Average reconstruction error is: 12.9064
 epoch 13/20. Took 0.20292 seconds. Average reconstruction error is: 12.6136
 epoch 14/20. Took 0.20395 seconds. Average reconstruction error is: 12.1953
 epoch 15/20. Took 0.20228 seconds. Average reconstruction error is: 11.7916
 epoch 16/20. Took 0.20085 seconds. Average reconstruction error is: 11.5984
 epoch 17/20. Took 0.1983 seconds. Average reconstruction error is: 11.2607
 epoch 18/20. Took 0.2055 seconds. Average reconstruction error is: 11.0653
 epoch 19/20. Took 0.2004 seconds. Average reconstruction error is: 10.8528
 epoch 20/20. Took 0.20003 seconds. Average reconstruction error is: 10.6265
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.13712 seconds. Average reconstruction error is: 29.2749
 epoch 2/20. Took 0.13707 seconds. Average reconstruction error is: 13.559
 epoch 3/20. Took 0.1375 seconds. Average reconstruction error is: 10.445
 epoch 4/20. Took 0.13677 seconds. Average reconstruction error is: 8.9768
 epoch 5/20. Took 0.13615 seconds. Average reconstruction error is: 8.1024
 epoch 6/20. Took 0.13599 seconds. Average reconstruction error is: 7.4963
 epoch 7/20. Took 0.13873 seconds. Average reconstruction error is: 7.0307
 epoch 8/20. Took 0.13832 seconds. Average reconstruction error is: 6.6788
 epoch 9/20. Took 0.13786 seconds. Average reconstruction error is: 6.3855
 epoch 10/20. Took 0.13758 seconds. Average reconstruction error is: 6.1752
 epoch 11/20. Took 0.13817 seconds. Average reconstruction error is: 5.9826
 epoch 12/20. Took 0.1391 seconds. Average reconstruction error is: 5.8714
 epoch 13/20. Took 0.13801 seconds. Average reconstruction error is: 5.7434
 epoch 14/20. Took 0.13852 seconds. Average reconstruction error is: 5.6234
 epoch 15/20. Took 0.14077 seconds. Average reconstruction error is: 5.4738
 epoch 16/20. Took 0.13769 seconds. Average reconstruction error is: 5.4822
 epoch 17/20. Took 0.13712 seconds. Average reconstruction error is: 5.3667
 epoch 18/20. Took 0.13822 seconds. Average reconstruction error is: 5.2404
 epoch 19/20. Took 0.13605 seconds. Average reconstruction error is: 5.2159
 epoch 20/20. Took 0.13736 seconds. Average reconstruction error is: 5.0832
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.13599 seconds. Average reconstruction error is: 25.8308
 epoch 2/20. Took 0.13954 seconds. Average reconstruction error is: 10.7648
 epoch 3/20. Took 0.13629 seconds. Average reconstruction error is: 8.191
 epoch 4/20. Took 0.1357 seconds. Average reconstruction error is: 7.0836
 epoch 5/20. Took 0.1361 seconds. Average reconstruction error is: 6.4229
 epoch 6/20. Took 0.13572 seconds. Average reconstruction error is: 5.9306
 epoch 7/20. Took 0.13433 seconds. Average reconstruction error is: 5.6693
 epoch 8/20. Took 0.1364 seconds. Average reconstruction error is: 5.4841
 epoch 9/20. Took 0.13709 seconds. Average reconstruction error is: 5.2868
 epoch 10/20. Took 0.13527 seconds. Average reconstruction error is: 5.0973
 epoch 11/20. Took 0.1362 seconds. Average reconstruction error is: 5.0615
 epoch 12/20. Took 0.13786 seconds. Average reconstruction error is: 5.0096
 epoch 13/20. Took 0.13592 seconds. Average reconstruction error is: 4.891
 epoch 14/20. Took 0.13698 seconds. Average reconstruction error is: 4.8684
 epoch 15/20. Took 0.13856 seconds. Average reconstruction error is: 4.7543
 epoch 16/20. Took 0.13622 seconds. Average reconstruction error is: 4.727
 epoch 17/20. Took 0.14032 seconds. Average reconstruction error is: 4.6701
 epoch 18/20. Took 0.14051 seconds. Average reconstruction error is: 4.6464
 epoch 19/20. Took 0.13782 seconds. Average reconstruction error is: 4.6012
 epoch 20/20. Took 0.13638 seconds. Average reconstruction error is: 4.6066
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.32715 seconds. 
  Full-batch training loss = 0.987324, test loss = 0.990565
  Training set accuracy = 0.819500, Test set accuracy = 0.824700
 epoch 2/100. Took 0.33629 seconds. 
  Full-batch training loss = 0.653059, test loss = 0.663236
  Training set accuracy = 0.856000, Test set accuracy = 0.857700
 epoch 3/100. Took 0.33768 seconds. 
  Full-batch training loss = 0.531909, test loss = 0.548696
  Training set accuracy = 0.877500, Test set accuracy = 0.875000
 epoch 4/100. Took 0.33277 seconds. 
  Full-batch training loss = 0.465521, test loss = 0.488771
  Training set accuracy = 0.889000, Test set accuracy = 0.881100
 epoch 5/100. Took 0.33247 seconds. 
  Full-batch training loss = 0.418997, test loss = 0.446969
  Training set accuracy = 0.895500, Test set accuracy = 0.887800
 epoch 6/100. Took 0.33761 seconds. 
  Full-batch training loss = 0.387269, test loss = 0.422606
  Training set accuracy = 0.902500, Test set accuracy = 0.890500
 epoch 7/100. Took 0.33775 seconds. 
  Full-batch training loss = 0.364568, test loss = 0.402239
  Training set accuracy = 0.906000, Test set accuracy = 0.893000
 epoch 8/100. Took 0.33726 seconds. 
  Full-batch training loss = 0.340487, test loss = 0.384348
  Training set accuracy = 0.916500, Test set accuracy = 0.895000
 epoch 9/100. Took 0.33103 seconds. 
  Full-batch training loss = 0.323029, test loss = 0.371396
  Training set accuracy = 0.919000, Test set accuracy = 0.897900
 epoch 10/100. Took 0.35023 seconds. 
  Full-batch training loss = 0.307664, test loss = 0.359729
  Training set accuracy = 0.921500, Test set accuracy = 0.899100
 epoch 11/100. Took 0.33318 seconds. 
  Full-batch training loss = 0.294894, test loss = 0.352856
  Training set accuracy = 0.924000, Test set accuracy = 0.901800
 epoch 12/100. Took 0.33306 seconds. 
  Full-batch training loss = 0.283000, test loss = 0.343851
  Training set accuracy = 0.924500, Test set accuracy = 0.902400
 epoch 13/100. Took 0.33123 seconds. 
  Full-batch training loss = 0.271406, test loss = 0.335907
  Training set accuracy = 0.929500, Test set accuracy = 0.904600
 epoch 14/100. Took 0.33153 seconds. 
  Full-batch training loss = 0.261705, test loss = 0.330164
  Training set accuracy = 0.930000, Test set accuracy = 0.905300
 epoch 15/100. Took 0.34201 seconds. 
  Full-batch training loss = 0.253159, test loss = 0.326353
  Training set accuracy = 0.931000, Test set accuracy = 0.905800
 epoch 16/100. Took 0.33109 seconds. 
  Full-batch training loss = 0.245197, test loss = 0.322118
  Training set accuracy = 0.933500, Test set accuracy = 0.906400
 epoch 17/100. Took 0.33137 seconds. 
  Full-batch training loss = 0.236941, test loss = 0.316550
  Training set accuracy = 0.939000, Test set accuracy = 0.908400
 epoch 18/100. Took 0.33575 seconds. 
  Full-batch training loss = 0.228935, test loss = 0.312465
  Training set accuracy = 0.938500, Test set accuracy = 0.908400
 epoch 19/100. Took 0.33043 seconds. 
  Full-batch training loss = 0.222009, test loss = 0.308187
  Training set accuracy = 0.942500, Test set accuracy = 0.910100
 epoch 20/100. Took 0.32672 seconds. 
  Full-batch training loss = 0.215222, test loss = 0.305271
  Training set accuracy = 0.941500, Test set accuracy = 0.910500
 epoch 21/100. Took 0.33068 seconds. 
  Full-batch training loss = 0.209002, test loss = 0.301508
  Training set accuracy = 0.944000, Test set accuracy = 0.911400
 epoch 22/100. Took 0.3291 seconds. 
  Full-batch training loss = 0.204688, test loss = 0.301332
  Training set accuracy = 0.943000, Test set accuracy = 0.911300
 epoch 23/100. Took 0.32768 seconds. 
  Full-batch training loss = 0.197604, test loss = 0.296641
  Training set accuracy = 0.948500, Test set accuracy = 0.912200
 epoch 24/100. Took 0.33416 seconds. 
  Full-batch training loss = 0.191996, test loss = 0.293907
  Training set accuracy = 0.950000, Test set accuracy = 0.912500
 epoch 25/100. Took 0.33623 seconds. 
  Full-batch training loss = 0.187155, test loss = 0.291914
  Training set accuracy = 0.951000, Test set accuracy = 0.912800
 epoch 26/100. Took 0.32897 seconds. 
  Full-batch training loss = 0.181885, test loss = 0.290831
  Training set accuracy = 0.953500, Test set accuracy = 0.913700
 epoch 27/100. Took 0.33186 seconds. 
  Full-batch training loss = 0.177173, test loss = 0.288189
  Training set accuracy = 0.954000, Test set accuracy = 0.913700
 epoch 28/100. Took 0.32905 seconds. 
  Full-batch training loss = 0.173048, test loss = 0.286402
  Training set accuracy = 0.957000, Test set accuracy = 0.914200
 epoch 29/100. Took 0.3303 seconds. 
  Full-batch training loss = 0.168234, test loss = 0.285207
  Training set accuracy = 0.958000, Test set accuracy = 0.913500
 epoch 30/100. Took 0.33019 seconds. 
  Full-batch training loss = 0.163992, test loss = 0.283897
  Training set accuracy = 0.961000, Test set accuracy = 0.915600
 epoch 31/100. Took 0.33187 seconds. 
  Full-batch training loss = 0.159793, test loss = 0.281899
  Training set accuracy = 0.962500, Test set accuracy = 0.914600
 epoch 32/100. Took 0.32566 seconds. 
  Full-batch training loss = 0.155738, test loss = 0.280283
  Training set accuracy = 0.963000, Test set accuracy = 0.916400
 epoch 33/100. Took 0.33419 seconds. 
  Full-batch training loss = 0.151932, test loss = 0.278881
  Training set accuracy = 0.966000, Test set accuracy = 0.916700
 epoch 34/100. Took 0.33866 seconds. 
  Full-batch training loss = 0.147974, test loss = 0.277907
  Training set accuracy = 0.967000, Test set accuracy = 0.916700
 epoch 35/100. Took 0.33636 seconds. 
  Full-batch training loss = 0.144924, test loss = 0.277493
  Training set accuracy = 0.967000, Test set accuracy = 0.915100
 epoch 36/100. Took 0.33051 seconds. 
  Full-batch training loss = 0.141471, test loss = 0.276179
  Training set accuracy = 0.970000, Test set accuracy = 0.916100
 epoch 37/100. Took 0.3359 seconds. 
  Full-batch training loss = 0.137618, test loss = 0.274794
  Training set accuracy = 0.971000, Test set accuracy = 0.916500
 epoch 38/100. Took 0.3296 seconds. 
  Full-batch training loss = 0.134420, test loss = 0.273913
  Training set accuracy = 0.971000, Test set accuracy = 0.917000
 epoch 39/100. Took 0.33989 seconds. 
  Full-batch training loss = 0.131480, test loss = 0.272951
  Training set accuracy = 0.972000, Test set accuracy = 0.916400
 epoch 40/100. Took 0.33528 seconds. 
  Full-batch training loss = 0.128091, test loss = 0.271923
  Training set accuracy = 0.974000, Test set accuracy = 0.917400
 epoch 41/100. Took 0.33622 seconds. 
  Full-batch training loss = 0.125632, test loss = 0.271867
  Training set accuracy = 0.973500, Test set accuracy = 0.917500
 epoch 42/100. Took 0.32663 seconds. 
  Full-batch training loss = 0.122473, test loss = 0.270640
  Training set accuracy = 0.974500, Test set accuracy = 0.917500
 epoch 43/100. Took 0.32903 seconds. 
  Full-batch training loss = 0.119544, test loss = 0.269731
  Training set accuracy = 0.977000, Test set accuracy = 0.916800
 epoch 44/100. Took 0.33292 seconds. 
  Full-batch training loss = 0.116836, test loss = 0.268955
  Training set accuracy = 0.977500, Test set accuracy = 0.917900
 epoch 45/100. Took 0.33579 seconds. 
  Full-batch training loss = 0.115172, test loss = 0.269488
  Training set accuracy = 0.978000, Test set accuracy = 0.917700
 epoch 46/100. Took 0.3324 seconds. 
  Full-batch training loss = 0.111865, test loss = 0.267798
  Training set accuracy = 0.978000, Test set accuracy = 0.917500
 epoch 47/100. Took 0.33232 seconds. 
  Full-batch training loss = 0.109225, test loss = 0.267222
  Training set accuracy = 0.979000, Test set accuracy = 0.918400
 epoch 48/100. Took 0.32726 seconds. 
  Full-batch training loss = 0.106658, test loss = 0.266729
  Training set accuracy = 0.980500, Test set accuracy = 0.918300
 epoch 49/100. Took 0.33083 seconds. 
  Full-batch training loss = 0.104335, test loss = 0.266116
  Training set accuracy = 0.980500, Test set accuracy = 0.917600
 epoch 50/100. Took 0.32969 seconds. 
  Full-batch training loss = 0.102839, test loss = 0.266839
  Training set accuracy = 0.981500, Test set accuracy = 0.917800
 epoch 51/100. Took 0.33213 seconds. 
  Full-batch training loss = 0.100125, test loss = 0.265369
  Training set accuracy = 0.983000, Test set accuracy = 0.918500
 epoch 52/100. Took 0.33274 seconds. 
  Full-batch training loss = 0.097756, test loss = 0.265436
  Training set accuracy = 0.984000, Test set accuracy = 0.918400
 epoch 53/100. Took 0.32664 seconds. 
  Full-batch training loss = 0.095708, test loss = 0.265185
  Training set accuracy = 0.984000, Test set accuracy = 0.918000
 epoch 54/100. Took 0.33333 seconds. 
  Full-batch training loss = 0.093735, test loss = 0.264004
  Training set accuracy = 0.986500, Test set accuracy = 0.919700
 epoch 55/100. Took 0.33267 seconds. 
  Full-batch training loss = 0.091548, test loss = 0.264134
  Training set accuracy = 0.986000, Test set accuracy = 0.919000
 epoch 56/100. Took 0.33447 seconds. 
  Full-batch training loss = 0.089546, test loss = 0.263677
  Training set accuracy = 0.988000, Test set accuracy = 0.918900
 epoch 57/100. Took 0.3366 seconds. 
  Full-batch training loss = 0.087672, test loss = 0.263434
  Training set accuracy = 0.988500, Test set accuracy = 0.918400
 epoch 58/100. Took 0.32903 seconds. 
  Full-batch training loss = 0.085876, test loss = 0.263047
  Training set accuracy = 0.988500, Test set accuracy = 0.918900
 epoch 59/100. Took 0.33356 seconds. 
  Full-batch training loss = 0.084205, test loss = 0.263132
  Training set accuracy = 0.989500, Test set accuracy = 0.919300
 epoch 60/100. Took 0.33554 seconds. 
  Full-batch training loss = 0.082345, test loss = 0.262844
  Training set accuracy = 0.990500, Test set accuracy = 0.919200
 epoch 61/100. Took 0.3298 seconds. 
  Full-batch training loss = 0.080698, test loss = 0.262586
  Training set accuracy = 0.991500, Test set accuracy = 0.919200
 epoch 62/100. Took 0.33249 seconds. 
  Full-batch training loss = 0.079053, test loss = 0.262583
  Training set accuracy = 0.991000, Test set accuracy = 0.919300
 epoch 63/100. Took 0.32974 seconds. 
  Full-batch training loss = 0.077411, test loss = 0.262085
  Training set accuracy = 0.992000, Test set accuracy = 0.919500
 epoch 64/100. Took 0.3302 seconds. 
  Full-batch training loss = 0.075840, test loss = 0.262081
  Training set accuracy = 0.992000, Test set accuracy = 0.919800
 epoch 65/100. Took 0.32913 seconds. 
  Full-batch training loss = 0.074351, test loss = 0.261779
  Training set accuracy = 0.992500, Test set accuracy = 0.919600
 epoch 66/100. Took 0.33172 seconds. 
  Full-batch training loss = 0.072880, test loss = 0.261943
  Training set accuracy = 0.992500, Test set accuracy = 0.919500
 epoch 67/100. Took 0.336 seconds. 
  Full-batch training loss = 0.071391, test loss = 0.261719
  Training set accuracy = 0.993000, Test set accuracy = 0.920300
 epoch 68/100. Took 0.35048 seconds. 
  Full-batch training loss = 0.070027, test loss = 0.261558
  Training set accuracy = 0.993000, Test set accuracy = 0.919800
 epoch 69/100. Took 0.33372 seconds. 
  Full-batch training loss = 0.068651, test loss = 0.261531
  Training set accuracy = 0.993000, Test set accuracy = 0.920100
 epoch 70/100. Took 0.33363 seconds. 
  Full-batch training loss = 0.067307, test loss = 0.261652
  Training set accuracy = 0.994000, Test set accuracy = 0.920000
 epoch 71/100. Took 0.34378 seconds. 
  Full-batch training loss = 0.065979, test loss = 0.261212
  Training set accuracy = 0.994000, Test set accuracy = 0.919800
 epoch 72/100. Took 0.32568 seconds. 
  Full-batch training loss = 0.064719, test loss = 0.261551
  Training set accuracy = 0.994500, Test set accuracy = 0.920300
 epoch 73/100. Took 0.32826 seconds. 
  Full-batch training loss = 0.063505, test loss = 0.261360
  Training set accuracy = 0.994500, Test set accuracy = 0.920800
 epoch 74/100. Took 0.33124 seconds. 
  Full-batch training loss = 0.062344, test loss = 0.261479
  Training set accuracy = 0.994500, Test set accuracy = 0.920500
 epoch 75/100. Took 0.32998 seconds. 
  Full-batch training loss = 0.061062, test loss = 0.261052
  Training set accuracy = 0.994500, Test set accuracy = 0.920800
 epoch 76/100. Took 0.33118 seconds. 
  Full-batch training loss = 0.059973, test loss = 0.261351
  Training set accuracy = 0.994500, Test set accuracy = 0.920500
 epoch 77/100. Took 0.3289 seconds. 
  Full-batch training loss = 0.058883, test loss = 0.261062
  Training set accuracy = 0.994500, Test set accuracy = 0.921000
 epoch 78/100. Took 0.3241 seconds. 
  Full-batch training loss = 0.057820, test loss = 0.260799
  Training set accuracy = 0.994500, Test set accuracy = 0.920200
 epoch 79/100. Took 0.32774 seconds. 
  Full-batch training loss = 0.056715, test loss = 0.261279
  Training set accuracy = 0.994500, Test set accuracy = 0.920700
 epoch 80/100. Took 0.33064 seconds. 
  Full-batch training loss = 0.055704, test loss = 0.260883
  Training set accuracy = 0.995500, Test set accuracy = 0.920800
 epoch 81/100. Took 0.32994 seconds. 
  Full-batch training loss = 0.054665, test loss = 0.261327
  Training set accuracy = 0.995500, Test set accuracy = 0.920700
 epoch 82/100. Took 0.33089 seconds. 
  Full-batch training loss = 0.053682, test loss = 0.261333
  Training set accuracy = 0.996000, Test set accuracy = 0.920100
 epoch 83/100. Took 0.33185 seconds. 
  Full-batch training loss = 0.052723, test loss = 0.261346
  Training set accuracy = 0.996000, Test set accuracy = 0.921300
 epoch 84/100. Took 0.32793 seconds. 
  Full-batch training loss = 0.051818, test loss = 0.261538
  Training set accuracy = 0.996000, Test set accuracy = 0.920600
 epoch 85/100. Took 0.33883 seconds. 
  Full-batch training loss = 0.050876, test loss = 0.261773
  Training set accuracy = 0.996000, Test set accuracy = 0.921100
 epoch 86/100. Took 0.331 seconds. 
  Full-batch training loss = 0.049925, test loss = 0.261470
  Training set accuracy = 0.996500, Test set accuracy = 0.921300
 epoch 87/100. Took 0.33262 seconds. 
  Full-batch training loss = 0.049056, test loss = 0.261562
  Training set accuracy = 0.996500, Test set accuracy = 0.921500
 epoch 88/100. Took 0.32673 seconds. 
  Full-batch training loss = 0.048215, test loss = 0.261251
  Training set accuracy = 0.997000, Test set accuracy = 0.921600
 epoch 89/100. Took 0.32953 seconds. 
  Full-batch training loss = 0.047403, test loss = 0.261834
  Training set accuracy = 0.997000, Test set accuracy = 0.921500
 epoch 90/100. Took 0.32674 seconds. 
  Full-batch training loss = 0.046626, test loss = 0.261573
  Training set accuracy = 0.997000, Test set accuracy = 0.921800
 epoch 91/100. Took 0.32981 seconds. 
  Full-batch training loss = 0.045802, test loss = 0.261714
  Training set accuracy = 0.997000, Test set accuracy = 0.921500
 epoch 92/100. Took 0.33042 seconds. 
  Full-batch training loss = 0.045036, test loss = 0.261622
  Training set accuracy = 0.997500, Test set accuracy = 0.921700
 epoch 93/100. Took 0.33434 seconds. 
  Full-batch training loss = 0.044296, test loss = 0.261903
  Training set accuracy = 0.997500, Test set accuracy = 0.921600
 epoch 94/100. Took 0.32504 seconds. 
  Full-batch training loss = 0.043555, test loss = 0.262119
  Training set accuracy = 0.997500, Test set accuracy = 0.921300
 epoch 95/100. Took 0.32997 seconds. 
  Full-batch training loss = 0.042814, test loss = 0.261889
  Training set accuracy = 0.998000, Test set accuracy = 0.921200
 epoch 96/100. Took 0.33103 seconds. 
  Full-batch training loss = 0.042107, test loss = 0.261876
  Training set accuracy = 0.998500, Test set accuracy = 0.921300
 epoch 97/100. Took 0.32882 seconds. 
  Full-batch training loss = 0.041400, test loss = 0.262367
  Training set accuracy = 0.998500, Test set accuracy = 0.921700
 epoch 98/100. Took 0.33843 seconds. 
  Full-batch training loss = 0.040748, test loss = 0.262414
  Training set accuracy = 0.998500, Test set accuracy = 0.921500
 epoch 99/100. Took 0.32948 seconds. 
  Full-batch training loss = 0.040091, test loss = 0.262603
  Training set accuracy = 0.998500, Test set accuracy = 0.921700
 epoch 100/100. Took 0.33389 seconds. 
  Full-batch training loss = 0.039472, test loss = 0.262498
  Training set accuracy = 0.998500, Test set accuracy = 0.921600
Elapsed time is 122.963248 seconds.
End Training
