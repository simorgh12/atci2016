==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 93/270
----------------------------------------------------------
* ReducedData	5000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.05
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.60897 seconds. Average reconstruction error is: 38.3846
 epoch 2/20. Took 0.61586 seconds. Average reconstruction error is: 20.7614
 epoch 3/20. Took 0.63651 seconds. Average reconstruction error is: 16.9516
 epoch 4/20. Took 0.63406 seconds. Average reconstruction error is: 14.8053
 epoch 5/20. Took 0.63369 seconds. Average reconstruction error is: 13.5339
 epoch 6/20. Took 0.62673 seconds. Average reconstruction error is: 12.5958
 epoch 7/20. Took 0.59493 seconds. Average reconstruction error is: 11.901
 epoch 8/20. Took 0.6095 seconds. Average reconstruction error is: 11.3711
 epoch 9/20. Took 0.6027 seconds. Average reconstruction error is: 10.8416
 epoch 10/20. Took 0.5998 seconds. Average reconstruction error is: 10.5018
 epoch 11/20. Took 0.58281 seconds. Average reconstruction error is: 10.1482
 epoch 12/20. Took 0.5824 seconds. Average reconstruction error is: 9.8037
 epoch 13/20. Took 0.59824 seconds. Average reconstruction error is: 9.5591
 epoch 14/20. Took 0.64086 seconds. Average reconstruction error is: 9.3726
 epoch 15/20. Took 0.6211 seconds. Average reconstruction error is: 9.1247
 epoch 16/20. Took 0.63834 seconds. Average reconstruction error is: 8.9407
 epoch 17/20. Took 0.63504 seconds. Average reconstruction error is: 8.763
 epoch 18/20. Took 0.60341 seconds. Average reconstruction error is: 8.6619
 epoch 19/20. Took 0.6109 seconds. Average reconstruction error is: 8.5441
 epoch 20/20. Took 0.57551 seconds. Average reconstruction error is: 8.4682
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.38691 seconds. Average reconstruction error is: 35.3351
 epoch 2/20. Took 0.40101 seconds. Average reconstruction error is: 19.0484
 epoch 3/20. Took 0.39464 seconds. Average reconstruction error is: 14.8348
 epoch 4/20. Took 0.39645 seconds. Average reconstruction error is: 12.7824
 epoch 5/20. Took 0.41571 seconds. Average reconstruction error is: 11.5845
 epoch 6/20. Took 0.3918 seconds. Average reconstruction error is: 10.8519
 epoch 7/20. Took 0.39666 seconds. Average reconstruction error is: 10.3205
 epoch 8/20. Took 0.41784 seconds. Average reconstruction error is: 9.9167
 epoch 9/20. Took 0.39787 seconds. Average reconstruction error is: 9.6764
 epoch 10/20. Took 0.4057 seconds. Average reconstruction error is: 9.4505
 epoch 11/20. Took 0.41509 seconds. Average reconstruction error is: 9.2232
 epoch 12/20. Took 0.44099 seconds. Average reconstruction error is: 9.1122
 epoch 13/20. Took 0.4204 seconds. Average reconstruction error is: 8.9927
 epoch 14/20. Took 0.44579 seconds. Average reconstruction error is: 8.8006
 epoch 15/20. Took 0.4357 seconds. Average reconstruction error is: 8.7814
 epoch 16/20. Took 0.43799 seconds. Average reconstruction error is: 8.7068
 epoch 17/20. Took 0.42562 seconds. Average reconstruction error is: 8.5724
 epoch 18/20. Took 0.4824 seconds. Average reconstruction error is: 8.5111
 epoch 19/20. Took 0.43027 seconds. Average reconstruction error is: 8.441
 epoch 20/20. Took 0.47793 seconds. Average reconstruction error is: 8.4117
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.40755 seconds. Average reconstruction error is: 29.2111
 epoch 2/20. Took 0.40968 seconds. Average reconstruction error is: 14.31
 epoch 3/20. Took 0.41135 seconds. Average reconstruction error is: 10.9384
 epoch 4/20. Took 0.43957 seconds. Average reconstruction error is: 9.3138
 epoch 5/20. Took 0.41733 seconds. Average reconstruction error is: 8.469
 epoch 6/20. Took 0.42345 seconds. Average reconstruction error is: 7.8917
 epoch 7/20. Took 0.418 seconds. Average reconstruction error is: 7.5544
 epoch 8/20. Took 0.40382 seconds. Average reconstruction error is: 7.261
 epoch 9/20. Took 0.42556 seconds. Average reconstruction error is: 7.1015
 epoch 10/20. Took 0.46283 seconds. Average reconstruction error is: 6.8723
 epoch 11/20. Took 0.42624 seconds. Average reconstruction error is: 6.7947
 epoch 12/20. Took 0.42101 seconds. Average reconstruction error is: 6.6797
 epoch 13/20. Took 0.42707 seconds. Average reconstruction error is: 6.6028
 epoch 14/20. Took 0.42351 seconds. Average reconstruction error is: 6.5243
 epoch 15/20. Took 0.4237 seconds. Average reconstruction error is: 6.4829
 epoch 16/20. Took 0.40306 seconds. Average reconstruction error is: 6.4446
 epoch 17/20. Took 0.39562 seconds. Average reconstruction error is: 6.3996
 epoch 18/20. Took 0.44439 seconds. Average reconstruction error is: 6.3682
 epoch 19/20. Took 0.41828 seconds. Average reconstruction error is: 6.3256
 epoch 20/20. Took 0.39487 seconds. Average reconstruction error is: 6.2919
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.8749 seconds. 
  Full-batch training loss = 0.276838, test loss = 0.316048
  Training set accuracy = 0.915400, Test set accuracy = 0.905300
 epoch 2/100. Took 0.90404 seconds. 
  Full-batch training loss = 0.217892, test loss = 0.276706
  Training set accuracy = 0.938600, Test set accuracy = 0.915900
 epoch 3/100. Took 1.1068 seconds. 
  Full-batch training loss = 0.196069, test loss = 0.267218
  Training set accuracy = 0.936800, Test set accuracy = 0.915200
 epoch 4/100. Took 1.1199 seconds. 
  Full-batch training loss = 0.143013, test loss = 0.225978
  Training set accuracy = 0.960400, Test set accuracy = 0.932900
 epoch 5/100. Took 0.89495 seconds. 
  Full-batch training loss = 0.117090, test loss = 0.207112
  Training set accuracy = 0.968200, Test set accuracy = 0.937200
 epoch 6/100. Took 0.97231 seconds. 
  Full-batch training loss = 0.100908, test loss = 0.198459
  Training set accuracy = 0.974800, Test set accuracy = 0.941300
 epoch 7/100. Took 0.93563 seconds. 
  Full-batch training loss = 0.091594, test loss = 0.197047
  Training set accuracy = 0.979200, Test set accuracy = 0.940700
 epoch 8/100. Took 0.88896 seconds. 
  Full-batch training loss = 0.076429, test loss = 0.190766
  Training set accuracy = 0.981200, Test set accuracy = 0.942700
 epoch 9/100. Took 0.92522 seconds. 
  Full-batch training loss = 0.071392, test loss = 0.192123
  Training set accuracy = 0.982200, Test set accuracy = 0.943100
 epoch 10/100. Took 0.87921 seconds. 
  Full-batch training loss = 0.056014, test loss = 0.182216
  Training set accuracy = 0.989400, Test set accuracy = 0.945900
 epoch 11/100. Took 0.87287 seconds. 
  Full-batch training loss = 0.048598, test loss = 0.181361
  Training set accuracy = 0.992600, Test set accuracy = 0.945600
 epoch 12/100. Took 0.86744 seconds. 
  Full-batch training loss = 0.043629, test loss = 0.183550
  Training set accuracy = 0.993600, Test set accuracy = 0.946100
 epoch 13/100. Took 0.87305 seconds. 
  Full-batch training loss = 0.037739, test loss = 0.179101
  Training set accuracy = 0.994600, Test set accuracy = 0.947100
 epoch 14/100. Took 0.88397 seconds. 
  Full-batch training loss = 0.033450, test loss = 0.180133
  Training set accuracy = 0.996200, Test set accuracy = 0.947200
 epoch 15/100. Took 0.88047 seconds. 
  Full-batch training loss = 0.032490, test loss = 0.184431
  Training set accuracy = 0.996400, Test set accuracy = 0.946000
 epoch 16/100. Took 0.92124 seconds. 
  Full-batch training loss = 0.026592, test loss = 0.177524
  Training set accuracy = 0.997800, Test set accuracy = 0.948400
 epoch 17/100. Took 0.9184 seconds. 
  Full-batch training loss = 0.024009, test loss = 0.177856
  Training set accuracy = 0.998200, Test set accuracy = 0.948300
 epoch 18/100. Took 0.87244 seconds. 
  Full-batch training loss = 0.022083, test loss = 0.180797
  Training set accuracy = 0.999000, Test set accuracy = 0.948900
 epoch 19/100. Took 0.90184 seconds. 
  Full-batch training loss = 0.021723, test loss = 0.181504
  Training set accuracy = 0.999000, Test set accuracy = 0.946800
 epoch 20/100. Took 0.93384 seconds. 
  Full-batch training loss = 0.018268, test loss = 0.182713
  Training set accuracy = 0.999400, Test set accuracy = 0.947900
 epoch 21/100. Took 0.87079 seconds. 
  Full-batch training loss = 0.016453, test loss = 0.176830
  Training set accuracy = 0.999400, Test set accuracy = 0.950100
 epoch 22/100. Took 0.87743 seconds. 
  Full-batch training loss = 0.014893, test loss = 0.178800
  Training set accuracy = 0.999600, Test set accuracy = 0.949000
 epoch 23/100. Took 1.174 seconds. 
  Full-batch training loss = 0.014357, test loss = 0.181023
  Training set accuracy = 0.999600, Test set accuracy = 0.948500
 epoch 24/100. Took 0.89033 seconds. 
  Full-batch training loss = 0.013518, test loss = 0.179528
  Training set accuracy = 0.999600, Test set accuracy = 0.949300
 epoch 25/100. Took 0.91868 seconds. 
  Full-batch training loss = 0.011922, test loss = 0.180954
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 26/100. Took 0.87636 seconds. 
  Full-batch training loss = 0.011059, test loss = 0.180186
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 27/100. Took 0.90481 seconds. 
  Full-batch training loss = 0.010639, test loss = 0.179753
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 28/100. Took 0.8798 seconds. 
  Full-batch training loss = 0.009887, test loss = 0.180813
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 29/100. Took 0.93554 seconds. 
  Full-batch training loss = 0.009411, test loss = 0.182068
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 30/100. Took 0.88742 seconds. 
  Full-batch training loss = 0.008654, test loss = 0.182103
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 31/100. Took 0.92809 seconds. 
  Full-batch training loss = 0.008358, test loss = 0.181711
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 32/100. Took 0.9038 seconds. 
  Full-batch training loss = 0.007992, test loss = 0.181402
  Training set accuracy = 1.000000, Test set accuracy = 0.951400
 epoch 33/100. Took 0.96057 seconds. 
  Full-batch training loss = 0.007617, test loss = 0.182176
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 34/100. Took 0.87261 seconds. 
  Full-batch training loss = 0.007160, test loss = 0.184536
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 35/100. Took 0.89777 seconds. 
  Full-batch training loss = 0.006873, test loss = 0.184685
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 36/100. Took 0.88868 seconds. 
  Full-batch training loss = 0.006619, test loss = 0.185449
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 37/100. Took 0.88612 seconds. 
  Full-batch training loss = 0.006265, test loss = 0.185635
  Training set accuracy = 1.000000, Test set accuracy = 0.951300
 epoch 38/100. Took 0.87028 seconds. 
  Full-batch training loss = 0.006226, test loss = 0.185282
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 39/100. Took 0.93244 seconds. 
  Full-batch training loss = 0.005780, test loss = 0.185232
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 40/100. Took 0.93283 seconds. 
  Full-batch training loss = 0.005555, test loss = 0.185734
  Training set accuracy = 1.000000, Test set accuracy = 0.951500
 epoch 41/100. Took 0.99526 seconds. 
  Full-batch training loss = 0.005372, test loss = 0.186851
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 42/100. Took 0.91301 seconds. 
  Full-batch training loss = 0.005184, test loss = 0.187072
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 43/100. Took 0.91965 seconds. 
  Full-batch training loss = 0.004963, test loss = 0.187145
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 44/100. Took 0.97772 seconds. 
  Full-batch training loss = 0.004806, test loss = 0.188224
  Training set accuracy = 1.000000, Test set accuracy = 0.951300
 epoch 45/100. Took 0.88333 seconds. 
  Full-batch training loss = 0.004726, test loss = 0.189411
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 46/100. Took 0.98553 seconds. 
  Full-batch training loss = 0.004479, test loss = 0.188415
  Training set accuracy = 1.000000, Test set accuracy = 0.952000
 epoch 47/100. Took 0.91121 seconds. 
  Full-batch training loss = 0.004370, test loss = 0.189903
  Training set accuracy = 1.000000, Test set accuracy = 0.951600
 epoch 48/100. Took 1.0026 seconds. 
  Full-batch training loss = 0.004222, test loss = 0.189023
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 49/100. Took 0.93269 seconds. 
  Full-batch training loss = 0.004100, test loss = 0.188646
  Training set accuracy = 1.000000, Test set accuracy = 0.951600
 epoch 50/100. Took 0.94869 seconds. 
  Full-batch training loss = 0.004026, test loss = 0.190803
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 51/100. Took 0.88034 seconds. 
  Full-batch training loss = 0.003861, test loss = 0.189313
  Training set accuracy = 1.000000, Test set accuracy = 0.952000
 epoch 52/100. Took 0.90288 seconds. 
  Full-batch training loss = 0.003764, test loss = 0.190236
  Training set accuracy = 1.000000, Test set accuracy = 0.951500
 epoch 53/100. Took 0.88351 seconds. 
  Full-batch training loss = 0.003693, test loss = 0.190600
  Training set accuracy = 1.000000, Test set accuracy = 0.952300
 epoch 54/100. Took 0.91735 seconds. 
  Full-batch training loss = 0.003568, test loss = 0.191177
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 55/100. Took 0.89151 seconds. 
  Full-batch training loss = 0.003457, test loss = 0.191736
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 56/100. Took 0.89361 seconds. 
  Full-batch training loss = 0.003395, test loss = 0.191414
  Training set accuracy = 1.000000, Test set accuracy = 0.951900
 epoch 57/100. Took 0.93827 seconds. 
  Full-batch training loss = 0.003283, test loss = 0.192266
  Training set accuracy = 1.000000, Test set accuracy = 0.952100
 epoch 58/100. Took 1.0017 seconds. 
  Full-batch training loss = 0.003211, test loss = 0.191730
  Training set accuracy = 1.000000, Test set accuracy = 0.952200
 epoch 59/100. Took 0.92368 seconds. 
  Full-batch training loss = 0.003160, test loss = 0.193436
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 60/100. Took 0.87414 seconds. 
  Full-batch training loss = 0.003050, test loss = 0.192465
  Training set accuracy = 1.000000, Test set accuracy = 0.952400
 epoch 61/100. Took 0.89598 seconds. 
  Full-batch training loss = 0.002989, test loss = 0.192513
  Training set accuracy = 1.000000, Test set accuracy = 0.951900
 epoch 62/100. Took 0.92382 seconds. 
  Full-batch training loss = 0.002926, test loss = 0.193733
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 63/100. Took 0.91015 seconds. 
  Full-batch training loss = 0.002852, test loss = 0.193110
  Training set accuracy = 1.000000, Test set accuracy = 0.951900
 epoch 64/100. Took 0.88133 seconds. 
  Full-batch training loss = 0.002805, test loss = 0.193340
  Training set accuracy = 1.000000, Test set accuracy = 0.952100
 epoch 65/100. Took 1.0148 seconds. 
  Full-batch training loss = 0.002744, test loss = 0.193494
  Training set accuracy = 1.000000, Test set accuracy = 0.952200
 epoch 66/100. Took 0.96661 seconds. 
  Full-batch training loss = 0.002671, test loss = 0.193817
  Training set accuracy = 1.000000, Test set accuracy = 0.952700
 epoch 67/100. Took 1.0726 seconds. 
  Full-batch training loss = 0.002615, test loss = 0.194513
  Training set accuracy = 1.000000, Test set accuracy = 0.952300
 epoch 68/100. Took 1.136 seconds. 
  Full-batch training loss = 0.002569, test loss = 0.195474
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 69/100. Took 1.0621 seconds. 
  Full-batch training loss = 0.002510, test loss = 0.194771
  Training set accuracy = 1.000000, Test set accuracy = 0.952500
 epoch 70/100. Took 1.0217 seconds. 
  Full-batch training loss = 0.002462, test loss = 0.195739
  Training set accuracy = 1.000000, Test set accuracy = 0.952400
 epoch 71/100. Took 1.0113 seconds. 
  Full-batch training loss = 0.002414, test loss = 0.195730
  Training set accuracy = 1.000000, Test set accuracy = 0.952500
 epoch 72/100. Took 1.028 seconds. 
  Full-batch training loss = 0.002368, test loss = 0.195825
  Training set accuracy = 1.000000, Test set accuracy = 0.952400
 epoch 73/100. Took 1.0572 seconds. 
  Full-batch training loss = 0.002317, test loss = 0.195887
  Training set accuracy = 1.000000, Test set accuracy = 0.952600
 epoch 74/100. Took 1.0844 seconds. 
  Full-batch training loss = 0.002282, test loss = 0.196886
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 75/100. Took 1.0626 seconds. 
  Full-batch training loss = 0.002237, test loss = 0.196294
  Training set accuracy = 1.000000, Test set accuracy = 0.952100
 epoch 76/100. Took 1.0466 seconds. 
  Full-batch training loss = 0.002189, test loss = 0.196408
  Training set accuracy = 1.000000, Test set accuracy = 0.952500
 epoch 77/100. Took 0.92323 seconds. 
  Full-batch training loss = 0.002154, test loss = 0.196369
  Training set accuracy = 1.000000, Test set accuracy = 0.953000
 epoch 78/100. Took 0.92065 seconds. 
  Full-batch training loss = 0.002115, test loss = 0.196784
  Training set accuracy = 1.000000, Test set accuracy = 0.952500
 epoch 79/100. Took 0.89811 seconds. 
  Full-batch training loss = 0.002079, test loss = 0.197319
  Training set accuracy = 1.000000, Test set accuracy = 0.952400
 epoch 80/100. Took 1.018 seconds. 
  Full-batch training loss = 0.002045, test loss = 0.197642
  Training set accuracy = 1.000000, Test set accuracy = 0.952900
 epoch 81/100. Took 0.96687 seconds. 
  Full-batch training loss = 0.002009, test loss = 0.197693
  Training set accuracy = 1.000000, Test set accuracy = 0.952500
 epoch 82/100. Took 0.92905 seconds. 
  Full-batch training loss = 0.001978, test loss = 0.197654
  Training set accuracy = 1.000000, Test set accuracy = 0.952500
 epoch 83/100. Took 0.90477 seconds. 
  Full-batch training loss = 0.001954, test loss = 0.197699
  Training set accuracy = 1.000000, Test set accuracy = 0.952600
 epoch 84/100. Took 0.91319 seconds. 
  Full-batch training loss = 0.001913, test loss = 0.198117
  Training set accuracy = 1.000000, Test set accuracy = 0.952600
 epoch 85/100. Took 0.9431 seconds. 
  Full-batch training loss = 0.001880, test loss = 0.198382
  Training set accuracy = 1.000000, Test set accuracy = 0.953000
 epoch 86/100. Took 0.88469 seconds. 
  Full-batch training loss = 0.001854, test loss = 0.198541
  Training set accuracy = 1.000000, Test set accuracy = 0.952600
 epoch 87/100. Took 0.92454 seconds. 
  Full-batch training loss = 0.001832, test loss = 0.199486
  Training set accuracy = 1.000000, Test set accuracy = 0.952900
 epoch 88/100. Took 0.91374 seconds. 
  Full-batch training loss = 0.001796, test loss = 0.198945
  Training set accuracy = 1.000000, Test set accuracy = 0.952700
 epoch 89/100. Took 1.0613 seconds. 
  Full-batch training loss = 0.001769, test loss = 0.199296
  Training set accuracy = 1.000000, Test set accuracy = 0.952600
 epoch 90/100. Took 0.90888 seconds. 
  Full-batch training loss = 0.001742, test loss = 0.199534
  Training set accuracy = 1.000000, Test set accuracy = 0.952800
 epoch 91/100. Took 0.91352 seconds. 
  Full-batch training loss = 0.001718, test loss = 0.199698
  Training set accuracy = 1.000000, Test set accuracy = 0.952900
 epoch 92/100. Took 0.91704 seconds. 
  Full-batch training loss = 0.001690, test loss = 0.199809
  Training set accuracy = 1.000000, Test set accuracy = 0.952900
 epoch 93/100. Took 0.95665 seconds. 
  Full-batch training loss = 0.001666, test loss = 0.200078
  Training set accuracy = 1.000000, Test set accuracy = 0.952600
 epoch 94/100. Took 1.0269 seconds. 
  Full-batch training loss = 0.001647, test loss = 0.200496
  Training set accuracy = 1.000000, Test set accuracy = 0.952900
 epoch 95/100. Took 0.90849 seconds. 
  Full-batch training loss = 0.001623, test loss = 0.200508
  Training set accuracy = 1.000000, Test set accuracy = 0.952700
 epoch 96/100. Took 0.88227 seconds. 
  Full-batch training loss = 0.001596, test loss = 0.200650
  Training set accuracy = 1.000000, Test set accuracy = 0.952600
 epoch 97/100. Took 1.1076 seconds. 
  Full-batch training loss = 0.001574, test loss = 0.200991
  Training set accuracy = 1.000000, Test set accuracy = 0.952400
 epoch 98/100. Took 0.89755 seconds. 
  Full-batch training loss = 0.001553, test loss = 0.201071
  Training set accuracy = 1.000000, Test set accuracy = 0.952700
 epoch 99/100. Took 0.87639 seconds. 
  Full-batch training loss = 0.001534, test loss = 0.201300
  Training set accuracy = 1.000000, Test set accuracy = 0.952900
 epoch 100/100. Took 0.92073 seconds. 
  Full-batch training loss = 0.001513, test loss = 0.201159
  Training set accuracy = 1.000000, Test set accuracy = 0.953100
Elapsed time is 237.341022 seconds.
End Training
