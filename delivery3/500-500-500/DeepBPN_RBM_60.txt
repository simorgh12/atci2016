==========================================================
   Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 60/270
----------------------------------------------------------
* ReducedData	2000
* PreTrainRBM	false
	[x] Epochs	100
	[x] LearningRate	0.05
* Fine-tunning
	-> Epochs	200
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 2000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training NN  (784  500  500  500   10) with BackPropagation for 200 epochs
 epoch 1/200. Took 0.46616 seconds. 
  Full-batch training loss = 2.415752, test loss = 2.411980
  Training set accuracy = 0.091500, Test set accuracy = 0.100900
 epoch 2/200. Took 0.52526 seconds. 
  Full-batch training loss = 2.400944, test loss = 2.403035
  Training set accuracy = 0.182000, Test set accuracy = 0.172200
 epoch 3/200. Took 0.53078 seconds. 
  Full-batch training loss = 2.378281, test loss = 2.379853
  Training set accuracy = 0.107500, Test set accuracy = 0.102800
 epoch 4/200. Took 0.52506 seconds. 
  Full-batch training loss = 2.368329, test loss = 2.372564
  Training set accuracy = 0.107500, Test set accuracy = 0.102800
 epoch 5/200. Took 0.52505 seconds. 
  Full-batch training loss = 2.309950, test loss = 2.310384
  Training set accuracy = 0.117500, Test set accuracy = 0.113500
 epoch 6/200. Took 0.52872 seconds. 
  Full-batch training loss = 2.295997, test loss = 2.293541
  Training set accuracy = 0.103000, Test set accuracy = 0.104400
 epoch 7/200. Took 0.54712 seconds. 
  Full-batch training loss = 2.319062, test loss = 2.322737
  Training set accuracy = 0.117500, Test set accuracy = 0.113500
 epoch 8/200. Took 0.56033 seconds. 
  Full-batch training loss = 2.334881, test loss = 2.329043
  Training set accuracy = 0.091500, Test set accuracy = 0.100900
 epoch 9/200. Took 0.54772 seconds. 
  Full-batch training loss = 2.304705, test loss = 2.302109
  Training set accuracy = 0.131500, Test set accuracy = 0.133000
 epoch 10/200. Took 0.54742 seconds. 
  Full-batch training loss = 2.265941, test loss = 2.271137
  Training set accuracy = 0.202000, Test set accuracy = 0.190000
 epoch 11/200. Took 0.54942 seconds. 
  Full-batch training loss = 2.288589, test loss = 2.300520
  Training set accuracy = 0.221500, Test set accuracy = 0.196300
 epoch 12/200. Took 0.55944 seconds. 
  Full-batch training loss = 2.256233, test loss = 2.258060
  Training set accuracy = 0.167000, Test set accuracy = 0.162300
 epoch 13/200. Took 0.55733 seconds. 
  Full-batch training loss = 2.254501, test loss = 2.257830
  Training set accuracy = 0.117500, Test set accuracy = 0.113500
 epoch 14/200. Took 0.55253 seconds. 
  Full-batch training loss = 2.236693, test loss = 2.240434
  Training set accuracy = 0.095000, Test set accuracy = 0.089200
 epoch 15/200. Took 0.56352 seconds. 
  Full-batch training loss = 2.202484, test loss = 2.203669
  Training set accuracy = 0.176000, Test set accuracy = 0.171600
 epoch 16/200. Took 0.57067 seconds. 
  Full-batch training loss = 2.135560, test loss = 2.136864
  Training set accuracy = 0.247000, Test set accuracy = 0.241500
 epoch 17/200. Took 0.54814 seconds. 
  Full-batch training loss = 2.094035, test loss = 2.097703
  Training set accuracy = 0.364000, Test set accuracy = 0.356300
 epoch 18/200. Took 0.55091 seconds. 
  Full-batch training loss = 1.974409, test loss = 1.973353
  Training set accuracy = 0.350500, Test set accuracy = 0.351400
 epoch 19/200. Took 0.54763 seconds. 
  Full-batch training loss = 1.792119, test loss = 1.794839
  Training set accuracy = 0.394500, Test set accuracy = 0.401500
 epoch 20/200. Took 0.54786 seconds. 
  Full-batch training loss = 1.613261, test loss = 1.612471
  Training set accuracy = 0.512000, Test set accuracy = 0.490800
 epoch 21/200. Took 0.55046 seconds. 
  Full-batch training loss = 1.462755, test loss = 1.459474
  Training set accuracy = 0.543000, Test set accuracy = 0.541800
 epoch 22/200. Took 0.55404 seconds. 
  Full-batch training loss = 1.385004, test loss = 1.394454
  Training set accuracy = 0.430000, Test set accuracy = 0.423900
 epoch 23/200. Took 0.55754 seconds. 
  Full-batch training loss = 1.218409, test loss = 1.223986
  Training set accuracy = 0.577000, Test set accuracy = 0.591600
 epoch 24/200. Took 0.55082 seconds. 
  Full-batch training loss = 1.125096, test loss = 1.142348
  Training set accuracy = 0.591000, Test set accuracy = 0.581500
 epoch 25/200. Took 0.55986 seconds. 
  Full-batch training loss = 1.049668, test loss = 1.072867
  Training set accuracy = 0.619000, Test set accuracy = 0.610200
 epoch 26/200. Took 0.55359 seconds. 
  Full-batch training loss = 0.964092, test loss = 0.983967
  Training set accuracy = 0.676500, Test set accuracy = 0.671600
 epoch 27/200. Took 0.54937 seconds. 
  Full-batch training loss = 0.936421, test loss = 0.966671
  Training set accuracy = 0.652000, Test set accuracy = 0.632200
 epoch 28/200. Took 0.55625 seconds. 
  Full-batch training loss = 0.851424, test loss = 0.880529
  Training set accuracy = 0.724500, Test set accuracy = 0.709700
 epoch 29/200. Took 0.55407 seconds. 
  Full-batch training loss = 0.862554, test loss = 0.892362
  Training set accuracy = 0.682000, Test set accuracy = 0.672200
 epoch 30/200. Took 0.5568 seconds. 
  Full-batch training loss = 0.797480, test loss = 0.827250
  Training set accuracy = 0.730000, Test set accuracy = 0.717200
 epoch 31/200. Took 0.55527 seconds. 
  Full-batch training loss = 0.781171, test loss = 0.823847
  Training set accuracy = 0.742500, Test set accuracy = 0.720700
 epoch 32/200. Took 0.55322 seconds. 
  Full-batch training loss = 0.739890, test loss = 0.776657
  Training set accuracy = 0.748000, Test set accuracy = 0.742000
 epoch 33/200. Took 0.55007 seconds. 
  Full-batch training loss = 0.748566, test loss = 0.787546
  Training set accuracy = 0.753000, Test set accuracy = 0.745800
 epoch 34/200. Took 0.55069 seconds. 
  Full-batch training loss = 0.696812, test loss = 0.742826
  Training set accuracy = 0.784000, Test set accuracy = 0.760400
 epoch 35/200. Took 0.55666 seconds. 
  Full-batch training loss = 0.666837, test loss = 0.715834
  Training set accuracy = 0.790000, Test set accuracy = 0.769900
 epoch 36/200. Took 0.56059 seconds. 
  Full-batch training loss = 0.701393, test loss = 0.759753
  Training set accuracy = 0.739500, Test set accuracy = 0.724000
 epoch 37/200. Took 0.55315 seconds. 
  Full-batch training loss = 0.638041, test loss = 0.695377
  Training set accuracy = 0.793500, Test set accuracy = 0.778000
 epoch 38/200. Took 0.55559 seconds. 
  Full-batch training loss = 0.614996, test loss = 0.684594
  Training set accuracy = 0.821000, Test set accuracy = 0.792700
 epoch 39/200. Took 0.55921 seconds. 
  Full-batch training loss = 0.594810, test loss = 0.658977
  Training set accuracy = 0.830000, Test set accuracy = 0.802300
 epoch 40/200. Took 0.55803 seconds. 
  Full-batch training loss = 0.570777, test loss = 0.641424
  Training set accuracy = 0.821500, Test set accuracy = 0.796700
 epoch 41/200. Took 0.55452 seconds. 
  Full-batch training loss = 0.548454, test loss = 0.620655
  Training set accuracy = 0.844000, Test set accuracy = 0.818200
 epoch 42/200. Took 0.56201 seconds. 
  Full-batch training loss = 0.543217, test loss = 0.632641
  Training set accuracy = 0.839500, Test set accuracy = 0.811300
 epoch 43/200. Took 0.56479 seconds. 
  Full-batch training loss = 0.517737, test loss = 0.606352
  Training set accuracy = 0.837000, Test set accuracy = 0.809200
 epoch 44/200. Took 0.55783 seconds. 
  Full-batch training loss = 0.517971, test loss = 0.608467
  Training set accuracy = 0.840500, Test set accuracy = 0.807000
 epoch 45/200. Took 0.5607 seconds. 
  Full-batch training loss = 0.498170, test loss = 0.590029
  Training set accuracy = 0.848500, Test set accuracy = 0.819500
 epoch 46/200. Took 0.5555 seconds. 
  Full-batch training loss = 0.468202, test loss = 0.560598
  Training set accuracy = 0.864500, Test set accuracy = 0.835600
 epoch 47/200. Took 0.55603 seconds. 
  Full-batch training loss = 0.454207, test loss = 0.554333
  Training set accuracy = 0.871000, Test set accuracy = 0.841300
 epoch 48/200. Took 0.55624 seconds. 
  Full-batch training loss = 0.445307, test loss = 0.544319
  Training set accuracy = 0.865500, Test set accuracy = 0.838100
 epoch 49/200. Took 0.61434 seconds. 
  Full-batch training loss = 0.443265, test loss = 0.551923
  Training set accuracy = 0.864500, Test set accuracy = 0.835100
 epoch 50/200. Took 0.55318 seconds. 
  Full-batch training loss = 0.416386, test loss = 0.527058
  Training set accuracy = 0.880000, Test set accuracy = 0.845300
 epoch 51/200. Took 0.554 seconds. 
  Full-batch training loss = 0.427352, test loss = 0.539725
  Training set accuracy = 0.866000, Test set accuracy = 0.835100
 epoch 52/200. Took 0.55499 seconds. 
  Full-batch training loss = 0.412863, test loss = 0.529126
  Training set accuracy = 0.870000, Test set accuracy = 0.841900
 epoch 53/200. Took 0.55925 seconds. 
  Full-batch training loss = 0.406037, test loss = 0.524009
  Training set accuracy = 0.873500, Test set accuracy = 0.839300
 epoch 54/200. Took 0.55928 seconds. 
  Full-batch training loss = 0.391481, test loss = 0.512261
  Training set accuracy = 0.889500, Test set accuracy = 0.852800
 epoch 55/200. Took 0.55368 seconds. 
  Full-batch training loss = 0.386756, test loss = 0.506879
  Training set accuracy = 0.887000, Test set accuracy = 0.849800
 epoch 56/200. Took 0.56094 seconds. 
  Full-batch training loss = 0.373282, test loss = 0.493118
  Training set accuracy = 0.888500, Test set accuracy = 0.853100
 epoch 57/200. Took 0.55636 seconds. 
  Full-batch training loss = 0.384636, test loss = 0.529639
  Training set accuracy = 0.879500, Test set accuracy = 0.845300
 epoch 58/200. Took 0.55993 seconds. 
  Full-batch training loss = 0.347930, test loss = 0.483872
  Training set accuracy = 0.894000, Test set accuracy = 0.856400
 epoch 59/200. Took 0.55384 seconds. 
  Full-batch training loss = 0.343885, test loss = 0.485174
  Training set accuracy = 0.898500, Test set accuracy = 0.859900
 epoch 60/200. Took 0.55573 seconds. 
  Full-batch training loss = 0.338566, test loss = 0.483397
  Training set accuracy = 0.901500, Test set accuracy = 0.859500
 epoch 61/200. Took 0.55973 seconds. 
  Full-batch training loss = 0.368858, test loss = 0.530041
  Training set accuracy = 0.893000, Test set accuracy = 0.846300
 epoch 62/200. Took 0.55342 seconds. 
  Full-batch training loss = 0.326792, test loss = 0.477050
  Training set accuracy = 0.896500, Test set accuracy = 0.861300
 epoch 63/200. Took 0.5787 seconds. 
  Full-batch training loss = 0.321089, test loss = 0.467888
  Training set accuracy = 0.903000, Test set accuracy = 0.861100
 epoch 64/200. Took 0.55283 seconds. 
  Full-batch training loss = 0.324561, test loss = 0.487120
  Training set accuracy = 0.903500, Test set accuracy = 0.861200
 epoch 65/200. Took 0.55434 seconds. 
  Full-batch training loss = 0.323902, test loss = 0.492451
  Training set accuracy = 0.902500, Test set accuracy = 0.858900
 epoch 66/200. Took 0.55407 seconds. 
  Full-batch training loss = 0.319207, test loss = 0.473623
  Training set accuracy = 0.904500, Test set accuracy = 0.859000
 epoch 67/200. Took 0.55286 seconds. 
  Full-batch training loss = 0.297661, test loss = 0.465841
  Training set accuracy = 0.908000, Test set accuracy = 0.865600
 epoch 68/200. Took 0.56256 seconds. 
  Full-batch training loss = 0.302211, test loss = 0.471658
  Training set accuracy = 0.907500, Test set accuracy = 0.860900
 epoch 69/200. Took 0.55818 seconds. 
  Full-batch training loss = 0.285802, test loss = 0.462884
  Training set accuracy = 0.912500, Test set accuracy = 0.865100
 epoch 70/200. Took 0.56036 seconds. 
  Full-batch training loss = 0.301275, test loss = 0.473010
  Training set accuracy = 0.909500, Test set accuracy = 0.863000
 epoch 71/200. Took 0.56059 seconds. 
  Full-batch training loss = 0.275096, test loss = 0.451132
  Training set accuracy = 0.917000, Test set accuracy = 0.867600
 epoch 72/200. Took 0.56304 seconds. 
  Full-batch training loss = 0.287378, test loss = 0.460980
  Training set accuracy = 0.911500, Test set accuracy = 0.865200
 epoch 73/200. Took 0.5606 seconds. 
  Full-batch training loss = 0.261999, test loss = 0.450852
  Training set accuracy = 0.925000, Test set accuracy = 0.872600
 epoch 74/200. Took 0.55338 seconds. 
  Full-batch training loss = 0.265245, test loss = 0.463739
  Training set accuracy = 0.920500, Test set accuracy = 0.867600
 epoch 75/200. Took 0.56813 seconds. 
  Full-batch training loss = 0.257340, test loss = 0.458382
  Training set accuracy = 0.921000, Test set accuracy = 0.868400
 epoch 76/200. Took 0.5535 seconds. 
  Full-batch training loss = 0.252849, test loss = 0.444282
  Training set accuracy = 0.919500, Test set accuracy = 0.872000
 epoch 77/200. Took 0.55472 seconds. 
  Full-batch training loss = 0.266423, test loss = 0.478132
  Training set accuracy = 0.917000, Test set accuracy = 0.862600
 epoch 78/200. Took 0.5491 seconds. 
  Full-batch training loss = 0.241326, test loss = 0.448204
  Training set accuracy = 0.925000, Test set accuracy = 0.872500
 epoch 79/200. Took 0.5578 seconds. 
  Full-batch training loss = 0.268531, test loss = 0.477984
  Training set accuracy = 0.922500, Test set accuracy = 0.863100
 epoch 80/200. Took 0.5545 seconds. 
  Full-batch training loss = 0.238509, test loss = 0.451947
  Training set accuracy = 0.930000, Test set accuracy = 0.872500
 epoch 81/200. Took 0.56358 seconds. 
  Full-batch training loss = 0.269993, test loss = 0.475701
  Training set accuracy = 0.906000, Test set accuracy = 0.863700
 epoch 82/200. Took 0.55592 seconds. 
  Full-batch training loss = 0.223530, test loss = 0.440470
  Training set accuracy = 0.932500, Test set accuracy = 0.877100
 epoch 83/200. Took 0.56344 seconds. 
  Full-batch training loss = 0.222490, test loss = 0.439180
  Training set accuracy = 0.933000, Test set accuracy = 0.877600
 epoch 84/200. Took 0.56002 seconds. 
  Full-batch training loss = 0.226411, test loss = 0.451186
  Training set accuracy = 0.931000, Test set accuracy = 0.871000
 epoch 85/200. Took 0.55724 seconds. 
  Full-batch training loss = 0.244341, test loss = 0.469069
  Training set accuracy = 0.925000, Test set accuracy = 0.865300
 epoch 86/200. Took 0.55445 seconds. 
  Full-batch training loss = 0.218646, test loss = 0.455202
  Training set accuracy = 0.937000, Test set accuracy = 0.873400
 epoch 87/200. Took 0.55321 seconds. 
  Full-batch training loss = 0.213341, test loss = 0.455122
  Training set accuracy = 0.937000, Test set accuracy = 0.872200
 epoch 88/200. Took 0.5599 seconds. 
  Full-batch training loss = 0.208699, test loss = 0.441110
  Training set accuracy = 0.938000, Test set accuracy = 0.876300
 epoch 89/200. Took 0.55082 seconds. 
  Full-batch training loss = 0.202531, test loss = 0.443982
  Training set accuracy = 0.935500, Test set accuracy = 0.876500
 epoch 90/200. Took 0.5593 seconds. 
  Full-batch training loss = 0.206470, test loss = 0.451401
  Training set accuracy = 0.937000, Test set accuracy = 0.874800
 epoch 91/200. Took 0.55052 seconds. 
  Full-batch training loss = 0.218906, test loss = 0.481488
  Training set accuracy = 0.929000, Test set accuracy = 0.866100
 epoch 92/200. Took 0.55364 seconds. 
  Full-batch training loss = 0.200914, test loss = 0.460399
  Training set accuracy = 0.936500, Test set accuracy = 0.872600
 epoch 93/200. Took 0.55378 seconds. 
  Full-batch training loss = 0.187857, test loss = 0.445656
  Training set accuracy = 0.943500, Test set accuracy = 0.878100
 epoch 94/200. Took 0.55717 seconds. 
  Full-batch training loss = 0.204570, test loss = 0.476625
  Training set accuracy = 0.938500, Test set accuracy = 0.871300
 epoch 95/200. Took 0.55645 seconds. 
  Full-batch training loss = 0.185697, test loss = 0.446571
  Training set accuracy = 0.947000, Test set accuracy = 0.876500
 epoch 96/200. Took 0.55787 seconds. 
  Full-batch training loss = 0.179051, test loss = 0.446477
  Training set accuracy = 0.948000, Test set accuracy = 0.879900
 epoch 97/200. Took 0.55083 seconds. 
  Full-batch training loss = 0.180080, test loss = 0.452283
  Training set accuracy = 0.950000, Test set accuracy = 0.875500
 epoch 98/200. Took 0.55403 seconds. 
  Full-batch training loss = 0.175993, test loss = 0.453673
  Training set accuracy = 0.949000, Test set accuracy = 0.879300
 epoch 99/200. Took 0.55879 seconds. 
  Full-batch training loss = 0.176342, test loss = 0.445709
  Training set accuracy = 0.948500, Test set accuracy = 0.879700
 epoch 100/200. Took 0.55778 seconds. 
  Full-batch training loss = 0.182183, test loss = 0.462294
  Training set accuracy = 0.942000, Test set accuracy = 0.874300
 epoch 101/200. Took 0.55307 seconds. 
  Full-batch training loss = 0.169867, test loss = 0.451255
  Training set accuracy = 0.949000, Test set accuracy = 0.877500
 epoch 102/200. Took 0.55423 seconds. 
  Full-batch training loss = 0.165417, test loss = 0.454631
  Training set accuracy = 0.953500, Test set accuracy = 0.880600
 epoch 103/200. Took 0.56125 seconds. 
  Full-batch training loss = 0.186903, test loss = 0.481790
  Training set accuracy = 0.946500, Test set accuracy = 0.868900
 epoch 104/200. Took 0.56024 seconds. 
  Full-batch training loss = 0.165005, test loss = 0.458052
  Training set accuracy = 0.948500, Test set accuracy = 0.875100
 epoch 105/200. Took 0.55505 seconds. 
  Full-batch training loss = 0.169065, test loss = 0.469892
  Training set accuracy = 0.950500, Test set accuracy = 0.875600
 epoch 106/200. Took 0.553 seconds. 
  Full-batch training loss = 0.168869, test loss = 0.471826
  Training set accuracy = 0.949000, Test set accuracy = 0.870800
 epoch 107/200. Took 0.57585 seconds. 
  Full-batch training loss = 0.151974, test loss = 0.451275
  Training set accuracy = 0.955500, Test set accuracy = 0.878500
 epoch 108/200. Took 0.5683 seconds. 
  Full-batch training loss = 0.149645, test loss = 0.460664
  Training set accuracy = 0.962000, Test set accuracy = 0.881100
 epoch 109/200. Took 0.55302 seconds. 
  Full-batch training loss = 0.148043, test loss = 0.459111
  Training set accuracy = 0.960500, Test set accuracy = 0.878500
 epoch 110/200. Took 0.56997 seconds. 
  Full-batch training loss = 0.146415, test loss = 0.459048
  Training set accuracy = 0.961000, Test set accuracy = 0.880600
 epoch 111/200. Took 0.55074 seconds. 
  Full-batch training loss = 0.164749, test loss = 0.496400
  Training set accuracy = 0.948500, Test set accuracy = 0.868700
 epoch 112/200. Took 0.5566 seconds. 
  Full-batch training loss = 0.144500, test loss = 0.460502
  Training set accuracy = 0.959500, Test set accuracy = 0.874700
 epoch 113/200. Took 0.5668 seconds. 
  Full-batch training loss = 0.149070, test loss = 0.473101
  Training set accuracy = 0.955500, Test set accuracy = 0.877400
 epoch 114/200. Took 0.55253 seconds. 
  Full-batch training loss = 0.134730, test loss = 0.461949
  Training set accuracy = 0.965000, Test set accuracy = 0.879500
 epoch 115/200. Took 0.55519 seconds. 
  Full-batch training loss = 0.139277, test loss = 0.468747
  Training set accuracy = 0.962500, Test set accuracy = 0.877700
 epoch 116/200. Took 0.55512 seconds. 
  Full-batch training loss = 0.144984, test loss = 0.469524
  Training set accuracy = 0.958500, Test set accuracy = 0.876700
 epoch 117/200. Took 0.56047 seconds. 
  Full-batch training loss = 0.151507, test loss = 0.500418
  Training set accuracy = 0.955000, Test set accuracy = 0.870300
 epoch 118/200. Took 0.55685 seconds. 
  Full-batch training loss = 0.127449, test loss = 0.472450
  Training set accuracy = 0.967000, Test set accuracy = 0.879700
 epoch 119/200. Took 0.55911 seconds. 
  Full-batch training loss = 0.128274, test loss = 0.470107
  Training set accuracy = 0.966500, Test set accuracy = 0.877800
 epoch 120/200. Took 0.56907 seconds. 
  Full-batch training loss = 0.126130, test loss = 0.474116
  Training set accuracy = 0.962500, Test set accuracy = 0.876600
 epoch 121/200. Took 0.55388 seconds. 
  Full-batch training loss = 0.130031, test loss = 0.487492
  Training set accuracy = 0.962500, Test set accuracy = 0.875300
 epoch 122/200. Took 0.55571 seconds. 
  Full-batch training loss = 0.127606, test loss = 0.490919
  Training set accuracy = 0.967000, Test set accuracy = 0.875400
 epoch 123/200. Took 0.55507 seconds. 
  Full-batch training loss = 0.116862, test loss = 0.475638
  Training set accuracy = 0.968000, Test set accuracy = 0.880000
 epoch 124/200. Took 0.55752 seconds. 
  Full-batch training loss = 0.142230, test loss = 0.514076
  Training set accuracy = 0.958500, Test set accuracy = 0.868200
 epoch 125/200. Took 0.56524 seconds. 
  Full-batch training loss = 0.131575, test loss = 0.484853
  Training set accuracy = 0.962000, Test set accuracy = 0.874000
 epoch 126/200. Took 0.56323 seconds. 
  Full-batch training loss = 0.117123, test loss = 0.490531
  Training set accuracy = 0.970500, Test set accuracy = 0.875300
 epoch 127/200. Took 0.55403 seconds. 
  Full-batch training loss = 0.113215, test loss = 0.490226
  Training set accuracy = 0.969000, Test set accuracy = 0.876500
 epoch 128/200. Took 0.55094 seconds. 
  Full-batch training loss = 0.111405, test loss = 0.487561
  Training set accuracy = 0.972000, Test set accuracy = 0.878000
 epoch 129/200. Took 0.55476 seconds. 
  Full-batch training loss = 0.112000, test loss = 0.490582
  Training set accuracy = 0.974500, Test set accuracy = 0.879100
 epoch 130/200. Took 0.55306 seconds. 
  Full-batch training loss = 0.115794, test loss = 0.491585
  Training set accuracy = 0.970000, Test set accuracy = 0.873800
 epoch 131/200. Took 0.56994 seconds. 
  Full-batch training loss = 0.112186, test loss = 0.490868
  Training set accuracy = 0.970500, Test set accuracy = 0.875900
 epoch 132/200. Took 0.55476 seconds. 
  Full-batch training loss = 0.100731, test loss = 0.490514
  Training set accuracy = 0.977000, Test set accuracy = 0.880700
 epoch 133/200. Took 0.5569 seconds. 
  Full-batch training loss = 0.117332, test loss = 0.499541
  Training set accuracy = 0.971000, Test set accuracy = 0.874000
 epoch 134/200. Took 0.55324 seconds. 
  Full-batch training loss = 0.097148, test loss = 0.488917
  Training set accuracy = 0.973500, Test set accuracy = 0.878100
 epoch 135/200. Took 0.56146 seconds. 
  Full-batch training loss = 0.093569, test loss = 0.489032
  Training set accuracy = 0.979000, Test set accuracy = 0.879600
 epoch 136/200. Took 0.55591 seconds. 
  Full-batch training loss = 0.102278, test loss = 0.498111
  Training set accuracy = 0.974000, Test set accuracy = 0.879300
 epoch 137/200. Took 0.55139 seconds. 
  Full-batch training loss = 0.109058, test loss = 0.505792
  Training set accuracy = 0.970500, Test set accuracy = 0.874500
 epoch 138/200. Took 0.55478 seconds. 
  Full-batch training loss = 0.095836, test loss = 0.499961
  Training set accuracy = 0.978500, Test set accuracy = 0.879100
 epoch 139/200. Took 0.55301 seconds. 
  Full-batch training loss = 0.090334, test loss = 0.499386
  Training set accuracy = 0.980000, Test set accuracy = 0.880000
 epoch 140/200. Took 0.55486 seconds. 
  Full-batch training loss = 0.090882, test loss = 0.506740
  Training set accuracy = 0.977000, Test set accuracy = 0.874700
 epoch 141/200. Took 0.55604 seconds. 
  Full-batch training loss = 0.087935, test loss = 0.499396
  Training set accuracy = 0.981500, Test set accuracy = 0.879400
 epoch 142/200. Took 0.56279 seconds. 
  Full-batch training loss = 0.089015, test loss = 0.504636
  Training set accuracy = 0.981000, Test set accuracy = 0.877000
 epoch 143/200. Took 0.5526 seconds. 
  Full-batch training loss = 0.089036, test loss = 0.510646
  Training set accuracy = 0.976500, Test set accuracy = 0.874200
 epoch 144/200. Took 0.5604 seconds. 
  Full-batch training loss = 0.080133, test loss = 0.506972
  Training set accuracy = 0.982500, Test set accuracy = 0.878000
 epoch 145/200. Took 0.54957 seconds. 
  Full-batch training loss = 0.079361, test loss = 0.505105
  Training set accuracy = 0.985000, Test set accuracy = 0.879700
 epoch 146/200. Took 0.55122 seconds. 
  Full-batch training loss = 0.083917, test loss = 0.514009
  Training set accuracy = 0.981500, Test set accuracy = 0.877200
 epoch 147/200. Took 0.55297 seconds. 
  Full-batch training loss = 0.080404, test loss = 0.512684
  Training set accuracy = 0.981000, Test set accuracy = 0.877900
 epoch 148/200. Took 0.55901 seconds. 
  Full-batch training loss = 0.076869, test loss = 0.512707
  Training set accuracy = 0.983000, Test set accuracy = 0.875900
 epoch 149/200. Took 0.55519 seconds. 
  Full-batch training loss = 0.076056, test loss = 0.512599
  Training set accuracy = 0.986500, Test set accuracy = 0.879800
 epoch 150/200. Took 0.55478 seconds. 
  Full-batch training loss = 0.077811, test loss = 0.524163
  Training set accuracy = 0.984000, Test set accuracy = 0.876500
 epoch 151/200. Took 0.55529 seconds. 
  Full-batch training loss = 0.075467, test loss = 0.524149
  Training set accuracy = 0.983500, Test set accuracy = 0.877200
 epoch 152/200. Took 0.55595 seconds. 
  Full-batch training loss = 0.072121, test loss = 0.525810
  Training set accuracy = 0.983000, Test set accuracy = 0.875800
 epoch 153/200. Took 0.55305 seconds. 
  Full-batch training loss = 0.075883, test loss = 0.521803
  Training set accuracy = 0.981500, Test set accuracy = 0.876100
 epoch 154/200. Took 0.5579 seconds. 
  Full-batch training loss = 0.073046, test loss = 0.527679
  Training set accuracy = 0.985500, Test set accuracy = 0.874800
 epoch 155/200. Took 0.56079 seconds. 
  Full-batch training loss = 0.094635, test loss = 0.555224
  Training set accuracy = 0.971500, Test set accuracy = 0.866800
 epoch 156/200. Took 0.55769 seconds. 
  Full-batch training loss = 0.071303, test loss = 0.538319
  Training set accuracy = 0.984500, Test set accuracy = 0.874900
 epoch 157/200. Took 0.5575 seconds. 
  Full-batch training loss = 0.063216, test loss = 0.522871
  Training set accuracy = 0.990000, Test set accuracy = 0.879200
 epoch 158/200. Took 0.56166 seconds. 
  Full-batch training loss = 0.066648, test loss = 0.537311
  Training set accuracy = 0.986000, Test set accuracy = 0.876500
 epoch 159/200. Took 0.55534 seconds. 
  Full-batch training loss = 0.062852, test loss = 0.531098
  Training set accuracy = 0.988000, Test set accuracy = 0.877800
 epoch 160/200. Took 0.56301 seconds. 
  Full-batch training loss = 0.060620, test loss = 0.532002
  Training set accuracy = 0.990500, Test set accuracy = 0.877600
 epoch 161/200. Took 0.57649 seconds. 
  Full-batch training loss = 0.059026, test loss = 0.532418
  Training set accuracy = 0.992500, Test set accuracy = 0.880000
 epoch 162/200. Took 0.55886 seconds. 
  Full-batch training loss = 0.058929, test loss = 0.535260
  Training set accuracy = 0.989500, Test set accuracy = 0.878000
 epoch 163/200. Took 0.56879 seconds. 
  Full-batch training loss = 0.065901, test loss = 0.539565
  Training set accuracy = 0.985000, Test set accuracy = 0.874000
 epoch 164/200. Took 0.55165 seconds. 
  Full-batch training loss = 0.056039, test loss = 0.538564
  Training set accuracy = 0.992500, Test set accuracy = 0.878600
 epoch 165/200. Took 0.55496 seconds. 
  Full-batch training loss = 0.054011, test loss = 0.535027
  Training set accuracy = 0.991500, Test set accuracy = 0.878900
 epoch 166/200. Took 0.58005 seconds. 
  Full-batch training loss = 0.055302, test loss = 0.542903
  Training set accuracy = 0.992000, Test set accuracy = 0.878300
 epoch 167/200. Took 0.61832 seconds. 
  Full-batch training loss = 0.050497, test loss = 0.536760
  Training set accuracy = 0.992500, Test set accuracy = 0.879500
 epoch 168/200. Took 0.56252 seconds. 
  Full-batch training loss = 0.049867, test loss = 0.538107
  Training set accuracy = 0.994000, Test set accuracy = 0.880000
 epoch 169/200. Took 0.55672 seconds. 
  Full-batch training loss = 0.054020, test loss = 0.551768
  Training set accuracy = 0.990500, Test set accuracy = 0.875300
 epoch 170/200. Took 0.55574 seconds. 
  Full-batch training loss = 0.049824, test loss = 0.546445
  Training set accuracy = 0.994000, Test set accuracy = 0.878500
 epoch 171/200. Took 0.55747 seconds. 
  Full-batch training loss = 0.051553, test loss = 0.550543
  Training set accuracy = 0.993000, Test set accuracy = 0.878700
 epoch 172/200. Took 0.55745 seconds. 
  Full-batch training loss = 0.051245, test loss = 0.553452
  Training set accuracy = 0.990500, Test set accuracy = 0.876500
 epoch 173/200. Took 0.55218 seconds. 
  Full-batch training loss = 0.046517, test loss = 0.543368
  Training set accuracy = 0.993000, Test set accuracy = 0.880200
 epoch 174/200. Took 0.56231 seconds. 
  Full-batch training loss = 0.048438, test loss = 0.554409
  Training set accuracy = 0.993000, Test set accuracy = 0.876200
 epoch 175/200. Took 0.55908 seconds. 
  Full-batch training loss = 0.049290, test loss = 0.559276
  Training set accuracy = 0.993500, Test set accuracy = 0.876000
 epoch 176/200. Took 0.56285 seconds. 
  Full-batch training loss = 0.052237, test loss = 0.562961
  Training set accuracy = 0.991000, Test set accuracy = 0.873900
 epoch 177/200. Took 0.55242 seconds. 
  Full-batch training loss = 0.047903, test loss = 0.565563
  Training set accuracy = 0.992500, Test set accuracy = 0.875400
 epoch 178/200. Took 0.55629 seconds. 
  Full-batch training loss = 0.042990, test loss = 0.552910
  Training set accuracy = 0.993500, Test set accuracy = 0.879700
 epoch 179/200. Took 0.55432 seconds. 
  Full-batch training loss = 0.041568, test loss = 0.558201
  Training set accuracy = 0.995000, Test set accuracy = 0.877300
 epoch 180/200. Took 0.56299 seconds. 
  Full-batch training loss = 0.042340, test loss = 0.560508
  Training set accuracy = 0.995000, Test set accuracy = 0.877700
 epoch 181/200. Took 0.5564 seconds. 
  Full-batch training loss = 0.043225, test loss = 0.567899
  Training set accuracy = 0.994000, Test set accuracy = 0.877000
 epoch 182/200. Took 0.55261 seconds. 
  Full-batch training loss = 0.042431, test loss = 0.566164
  Training set accuracy = 0.994500, Test set accuracy = 0.875400
 epoch 183/200. Took 0.55389 seconds. 
  Full-batch training loss = 0.038447, test loss = 0.561735
  Training set accuracy = 0.996500, Test set accuracy = 0.879400
 epoch 184/200. Took 0.56027 seconds. 
  Full-batch training loss = 0.047238, test loss = 0.570774
  Training set accuracy = 0.992500, Test set accuracy = 0.876400
 epoch 185/200. Took 0.5656 seconds. 
  Full-batch training loss = 0.040451, test loss = 0.568637
  Training set accuracy = 0.995500, Test set accuracy = 0.877000
 epoch 186/200. Took 0.57385 seconds. 
  Full-batch training loss = 0.037346, test loss = 0.566609
  Training set accuracy = 0.996000, Test set accuracy = 0.878000
 epoch 187/200. Took 0.55817 seconds. 
  Full-batch training loss = 0.035668, test loss = 0.567392
  Training set accuracy = 0.997500, Test set accuracy = 0.877900
 epoch 188/200. Took 0.56036 seconds. 
  Full-batch training loss = 0.035229, test loss = 0.568262
  Training set accuracy = 0.996500, Test set accuracy = 0.878300
 epoch 189/200. Took 0.55861 seconds. 
  Full-batch training loss = 0.035974, test loss = 0.573136
  Training set accuracy = 0.996500, Test set accuracy = 0.877800
 epoch 190/200. Took 0.55712 seconds. 
  Full-batch training loss = 0.034620, test loss = 0.570594
  Training set accuracy = 0.997000, Test set accuracy = 0.879100
 epoch 191/200. Took 0.56147 seconds. 
  Full-batch training loss = 0.035651, test loss = 0.577888
  Training set accuracy = 0.996000, Test set accuracy = 0.878200
 epoch 192/200. Took 0.56163 seconds. 
  Full-batch training loss = 0.034590, test loss = 0.572521
  Training set accuracy = 0.997000, Test set accuracy = 0.878700
 epoch 193/200. Took 0.5563 seconds. 
  Full-batch training loss = 0.032113, test loss = 0.572983
  Training set accuracy = 0.998000, Test set accuracy = 0.878500
 epoch 194/200. Took 0.55476 seconds. 
  Full-batch training loss = 0.032870, test loss = 0.575732
  Training set accuracy = 0.998500, Test set accuracy = 0.879900
 epoch 195/200. Took 0.55683 seconds. 
  Full-batch training loss = 0.032670, test loss = 0.577977
  Training set accuracy = 0.998500, Test set accuracy = 0.878300
 epoch 196/200. Took 0.5563 seconds. 
  Full-batch training loss = 0.033019, test loss = 0.579982
  Training set accuracy = 0.997500, Test set accuracy = 0.879100
 epoch 197/200. Took 0.56385 seconds. 
  Full-batch training loss = 0.032244, test loss = 0.584310
  Training set accuracy = 0.998000, Test set accuracy = 0.877600
 epoch 198/200. Took 0.56048 seconds. 
  Full-batch training loss = 0.030410, test loss = 0.582291
  Training set accuracy = 0.997000, Test set accuracy = 0.879500
 epoch 199/200. Took 0.55622 seconds. 
  Full-batch training loss = 0.030224, test loss = 0.582097
  Training set accuracy = 0.998500, Test set accuracy = 0.880400
 epoch 200/200. Took 0.56469 seconds. 
  Full-batch training loss = 0.030678, test loss = 0.584076
  Training set accuracy = 0.997500, Test set accuracy = 0.876700
Elapsed time is 392.095319 seconds.
End Training
