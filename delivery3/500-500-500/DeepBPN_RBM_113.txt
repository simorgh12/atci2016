==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 113/270
----------------------------------------------------------
* ReducedData	5000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.05
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.55542 seconds. Average reconstruction error is: 38.2829
 epoch 2/20. Took 0.55153 seconds. Average reconstruction error is: 20.7877
 epoch 3/20. Took 0.54761 seconds. Average reconstruction error is: 16.8212
 epoch 4/20. Took 0.55873 seconds. Average reconstruction error is: 14.74
 epoch 5/20. Took 0.55483 seconds. Average reconstruction error is: 13.4369
 epoch 6/20. Took 0.54485 seconds. Average reconstruction error is: 12.4744
 epoch 7/20. Took 0.55074 seconds. Average reconstruction error is: 11.7757
 epoch 8/20. Took 0.55996 seconds. Average reconstruction error is: 11.2247
 epoch 9/20. Took 0.55704 seconds. Average reconstruction error is: 10.8
 epoch 10/20. Took 0.55091 seconds. Average reconstruction error is: 10.4227
 epoch 11/20. Took 0.55319 seconds. Average reconstruction error is: 10.0635
 epoch 12/20. Took 0.55329 seconds. Average reconstruction error is: 9.7969
 epoch 13/20. Took 0.55257 seconds. Average reconstruction error is: 9.5187
 epoch 14/20. Took 0.5503 seconds. Average reconstruction error is: 9.273
 epoch 15/20. Took 0.55776 seconds. Average reconstruction error is: 9.1212
 epoch 16/20. Took 0.55649 seconds. Average reconstruction error is: 8.9312
 epoch 17/20. Took 0.5519 seconds. Average reconstruction error is: 8.7971
 epoch 18/20. Took 0.55285 seconds. Average reconstruction error is: 8.6443
 epoch 19/20. Took 0.56356 seconds. Average reconstruction error is: 8.5669
 epoch 20/20. Took 0.54856 seconds. Average reconstruction error is: 8.4526
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.37137 seconds. Average reconstruction error is: 34.8221
 epoch 2/20. Took 0.37523 seconds. Average reconstruction error is: 18.9822
 epoch 3/20. Took 0.37129 seconds. Average reconstruction error is: 14.7925
 epoch 4/20. Took 0.37609 seconds. Average reconstruction error is: 12.7313
 epoch 5/20. Took 0.37287 seconds. Average reconstruction error is: 11.578
 epoch 6/20. Took 0.37107 seconds. Average reconstruction error is: 10.8335
 epoch 7/20. Took 0.37333 seconds. Average reconstruction error is: 10.2883
 epoch 8/20. Took 0.37396 seconds. Average reconstruction error is: 9.9415
 epoch 9/20. Took 0.37056 seconds. Average reconstruction error is: 9.6662
 epoch 10/20. Took 0.37239 seconds. Average reconstruction error is: 9.4115
 epoch 11/20. Took 0.37533 seconds. Average reconstruction error is: 9.1484
 epoch 12/20. Took 0.37405 seconds. Average reconstruction error is: 9.0435
 epoch 13/20. Took 0.37217 seconds. Average reconstruction error is: 8.9065
 epoch 14/20. Took 0.37458 seconds. Average reconstruction error is: 8.8036
 epoch 15/20. Took 0.3748 seconds. Average reconstruction error is: 8.7221
 epoch 16/20. Took 0.37367 seconds. Average reconstruction error is: 8.5731
 epoch 17/20. Took 0.3753 seconds. Average reconstruction error is: 8.5504
 epoch 18/20. Took 0.37179 seconds. Average reconstruction error is: 8.5229
 epoch 19/20. Took 0.3747 seconds. Average reconstruction error is: 8.3692
 epoch 20/20. Took 0.37427 seconds. Average reconstruction error is: 8.3163
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.37888 seconds. Average reconstruction error is: 29.0097
 epoch 2/20. Took 0.38524 seconds. Average reconstruction error is: 14.16
 epoch 3/20. Took 0.38033 seconds. Average reconstruction error is: 10.8477
 epoch 4/20. Took 0.382 seconds. Average reconstruction error is: 9.2714
 epoch 5/20. Took 0.38294 seconds. Average reconstruction error is: 8.3461
 epoch 6/20. Took 0.38294 seconds. Average reconstruction error is: 7.7886
 epoch 7/20. Took 0.37954 seconds. Average reconstruction error is: 7.4066
 epoch 8/20. Took 0.38455 seconds. Average reconstruction error is: 7.1483
 epoch 9/20. Took 0.38478 seconds. Average reconstruction error is: 6.9454
 epoch 10/20. Took 0.38309 seconds. Average reconstruction error is: 6.8179
 epoch 11/20. Took 0.3826 seconds. Average reconstruction error is: 6.6731
 epoch 12/20. Took 0.38482 seconds. Average reconstruction error is: 6.584
 epoch 13/20. Took 0.37816 seconds. Average reconstruction error is: 6.5174
 epoch 14/20. Took 0.38512 seconds. Average reconstruction error is: 6.4626
 epoch 15/20. Took 0.38104 seconds. Average reconstruction error is: 6.3736
 epoch 16/20. Took 0.38443 seconds. Average reconstruction error is: 6.362
 epoch 17/20. Took 0.38168 seconds. Average reconstruction error is: 6.3152
 epoch 18/20. Took 0.38443 seconds. Average reconstruction error is: 6.327
 epoch 19/20. Took 0.38371 seconds. Average reconstruction error is: 6.2371
 epoch 20/20. Took 0.38197 seconds. Average reconstruction error is: 6.231
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.87859 seconds. 
  Full-batch training loss = 0.439477, test loss = 0.449760
  Training set accuracy = 0.891800, Test set accuracy = 0.889500
 epoch 2/100. Took 0.87865 seconds. 
  Full-batch training loss = 0.338146, test loss = 0.354503
  Training set accuracy = 0.911800, Test set accuracy = 0.905500
 epoch 3/100. Took 0.86623 seconds. 
  Full-batch training loss = 0.291285, test loss = 0.312441
  Training set accuracy = 0.919800, Test set accuracy = 0.914600
 epoch 4/100. Took 0.86605 seconds. 
  Full-batch training loss = 0.269125, test loss = 0.295302
  Training set accuracy = 0.927000, Test set accuracy = 0.916400
 epoch 5/100. Took 0.87972 seconds. 
  Full-batch training loss = 0.244715, test loss = 0.276511
  Training set accuracy = 0.931600, Test set accuracy = 0.919600
 epoch 6/100. Took 0.88069 seconds. 
  Full-batch training loss = 0.229697, test loss = 0.266410
  Training set accuracy = 0.933600, Test set accuracy = 0.920900
 epoch 7/100. Took 0.86752 seconds. 
  Full-batch training loss = 0.214946, test loss = 0.251750
  Training set accuracy = 0.938200, Test set accuracy = 0.928100
 epoch 8/100. Took 0.87738 seconds. 
  Full-batch training loss = 0.205385, test loss = 0.247748
  Training set accuracy = 0.938400, Test set accuracy = 0.927300
 epoch 9/100. Took 0.86815 seconds. 
  Full-batch training loss = 0.192365, test loss = 0.238179
  Training set accuracy = 0.944200, Test set accuracy = 0.931500
 epoch 10/100. Took 0.87035 seconds. 
  Full-batch training loss = 0.180006, test loss = 0.230344
  Training set accuracy = 0.948000, Test set accuracy = 0.934000
 epoch 11/100. Took 0.87119 seconds. 
  Full-batch training loss = 0.172394, test loss = 0.226887
  Training set accuracy = 0.951400, Test set accuracy = 0.934600
 epoch 12/100. Took 0.8723 seconds. 
  Full-batch training loss = 0.164582, test loss = 0.225513
  Training set accuracy = 0.956000, Test set accuracy = 0.933100
 epoch 13/100. Took 0.86332 seconds. 
  Full-batch training loss = 0.157632, test loss = 0.216522
  Training set accuracy = 0.954200, Test set accuracy = 0.936800
 epoch 14/100. Took 0.86392 seconds. 
  Full-batch training loss = 0.150149, test loss = 0.213850
  Training set accuracy = 0.958800, Test set accuracy = 0.938100
 epoch 15/100. Took 0.86821 seconds. 
  Full-batch training loss = 0.143837, test loss = 0.208705
  Training set accuracy = 0.959400, Test set accuracy = 0.938800
 epoch 16/100. Took 0.86017 seconds. 
  Full-batch training loss = 0.138244, test loss = 0.206297
  Training set accuracy = 0.962400, Test set accuracy = 0.938800
 epoch 17/100. Took 0.87702 seconds. 
  Full-batch training loss = 0.134590, test loss = 0.207751
  Training set accuracy = 0.963200, Test set accuracy = 0.939200
 epoch 18/100. Took 0.87616 seconds. 
  Full-batch training loss = 0.127087, test loss = 0.202445
  Training set accuracy = 0.964400, Test set accuracy = 0.941600
 epoch 19/100. Took 0.86806 seconds. 
  Full-batch training loss = 0.123093, test loss = 0.201099
  Training set accuracy = 0.967000, Test set accuracy = 0.940600
 epoch 20/100. Took 0.86835 seconds. 
  Full-batch training loss = 0.120280, test loss = 0.199891
  Training set accuracy = 0.966800, Test set accuracy = 0.941900
 epoch 21/100. Took 0.87095 seconds. 
  Full-batch training loss = 0.115962, test loss = 0.199280
  Training set accuracy = 0.970200, Test set accuracy = 0.940600
 epoch 22/100. Took 0.86317 seconds. 
  Full-batch training loss = 0.109669, test loss = 0.195993
  Training set accuracy = 0.972200, Test set accuracy = 0.942300
 epoch 23/100. Took 0.8687 seconds. 
  Full-batch training loss = 0.105710, test loss = 0.192701
  Training set accuracy = 0.973400, Test set accuracy = 0.943000
 epoch 24/100. Took 0.86464 seconds. 
  Full-batch training loss = 0.104313, test loss = 0.193889
  Training set accuracy = 0.975400, Test set accuracy = 0.943500
 epoch 25/100. Took 0.86277 seconds. 
  Full-batch training loss = 0.097819, test loss = 0.189434
  Training set accuracy = 0.975600, Test set accuracy = 0.943900
 epoch 26/100. Took 0.87424 seconds. 
  Full-batch training loss = 0.094929, test loss = 0.190270
  Training set accuracy = 0.978000, Test set accuracy = 0.943600
 epoch 27/100. Took 0.86684 seconds. 
  Full-batch training loss = 0.092886, test loss = 0.188324
  Training set accuracy = 0.979200, Test set accuracy = 0.943800
 epoch 28/100. Took 0.86245 seconds. 
  Full-batch training loss = 0.089067, test loss = 0.187733
  Training set accuracy = 0.979000, Test set accuracy = 0.944600
 epoch 29/100. Took 0.86725 seconds. 
  Full-batch training loss = 0.086623, test loss = 0.186821
  Training set accuracy = 0.979600, Test set accuracy = 0.945700
 epoch 30/100. Took 0.86456 seconds. 
  Full-batch training loss = 0.085000, test loss = 0.182962
  Training set accuracy = 0.980400, Test set accuracy = 0.945100
 epoch 31/100. Took 0.86198 seconds. 
  Full-batch training loss = 0.079989, test loss = 0.183726
  Training set accuracy = 0.982800, Test set accuracy = 0.945300
 epoch 32/100. Took 0.86247 seconds. 
  Full-batch training loss = 0.079549, test loss = 0.185667
  Training set accuracy = 0.981800, Test set accuracy = 0.946800
 epoch 33/100. Took 0.85976 seconds. 
  Full-batch training loss = 0.075969, test loss = 0.181682
  Training set accuracy = 0.983800, Test set accuracy = 0.946300
 epoch 34/100. Took 0.86716 seconds. 
  Full-batch training loss = 0.074741, test loss = 0.183453
  Training set accuracy = 0.984800, Test set accuracy = 0.944800
 epoch 35/100. Took 0.86937 seconds. 
  Full-batch training loss = 0.073847, test loss = 0.185395
  Training set accuracy = 0.984800, Test set accuracy = 0.944600
 epoch 36/100. Took 0.87233 seconds. 
  Full-batch training loss = 0.068370, test loss = 0.179304
  Training set accuracy = 0.986400, Test set accuracy = 0.946900
 epoch 37/100. Took 0.87365 seconds. 
  Full-batch training loss = 0.065604, test loss = 0.176967
  Training set accuracy = 0.987600, Test set accuracy = 0.947700
 epoch 38/100. Took 0.87025 seconds. 
  Full-batch training loss = 0.063503, test loss = 0.179164
  Training set accuracy = 0.989200, Test set accuracy = 0.947300
 epoch 39/100. Took 0.87563 seconds. 
  Full-batch training loss = 0.061689, test loss = 0.177068
  Training set accuracy = 0.987800, Test set accuracy = 0.947600
 epoch 40/100. Took 0.87156 seconds. 
  Full-batch training loss = 0.059658, test loss = 0.176558
  Training set accuracy = 0.989600, Test set accuracy = 0.947700
 epoch 41/100. Took 0.8686 seconds. 
  Full-batch training loss = 0.060247, test loss = 0.179357
  Training set accuracy = 0.989200, Test set accuracy = 0.946700
 epoch 42/100. Took 0.87872 seconds. 
  Full-batch training loss = 0.056924, test loss = 0.177047
  Training set accuracy = 0.990000, Test set accuracy = 0.947700
 epoch 43/100. Took 0.87092 seconds. 
  Full-batch training loss = 0.054511, test loss = 0.175631
  Training set accuracy = 0.991000, Test set accuracy = 0.948500
 epoch 44/100. Took 0.87712 seconds. 
  Full-batch training loss = 0.053924, test loss = 0.176774
  Training set accuracy = 0.991000, Test set accuracy = 0.948600
 epoch 45/100. Took 0.8701 seconds. 
  Full-batch training loss = 0.051407, test loss = 0.175407
  Training set accuracy = 0.991800, Test set accuracy = 0.948200
 epoch 46/100. Took 0.86828 seconds. 
  Full-batch training loss = 0.050831, test loss = 0.174965
  Training set accuracy = 0.991600, Test set accuracy = 0.947300
 epoch 47/100. Took 0.86592 seconds. 
  Full-batch training loss = 0.048324, test loss = 0.174739
  Training set accuracy = 0.993200, Test set accuracy = 0.948000
 epoch 48/100. Took 0.87478 seconds. 
  Full-batch training loss = 0.047599, test loss = 0.175788
  Training set accuracy = 0.992800, Test set accuracy = 0.948700
 epoch 49/100. Took 0.86447 seconds. 
  Full-batch training loss = 0.045303, test loss = 0.173321
  Training set accuracy = 0.993000, Test set accuracy = 0.949100
 epoch 50/100. Took 0.872 seconds. 
  Full-batch training loss = 0.044746, test loss = 0.173180
  Training set accuracy = 0.993400, Test set accuracy = 0.948500
 epoch 51/100. Took 0.88136 seconds. 
  Full-batch training loss = 0.043669, test loss = 0.174000
  Training set accuracy = 0.994400, Test set accuracy = 0.948400
 epoch 52/100. Took 0.86737 seconds. 
  Full-batch training loss = 0.041828, test loss = 0.172322
  Training set accuracy = 0.994200, Test set accuracy = 0.949100
 epoch 53/100. Took 0.87418 seconds. 
  Full-batch training loss = 0.040573, test loss = 0.172823
  Training set accuracy = 0.994600, Test set accuracy = 0.948400
 epoch 54/100. Took 0.87197 seconds. 
  Full-batch training loss = 0.039329, test loss = 0.172168
  Training set accuracy = 0.995200, Test set accuracy = 0.948900
 epoch 55/100. Took 0.87103 seconds. 
  Full-batch training loss = 0.038587, test loss = 0.172907
  Training set accuracy = 0.995800, Test set accuracy = 0.948300
 epoch 56/100. Took 0.86599 seconds. 
  Full-batch training loss = 0.037782, test loss = 0.174390
  Training set accuracy = 0.996400, Test set accuracy = 0.949500
 epoch 57/100. Took 0.86956 seconds. 
  Full-batch training loss = 0.036334, test loss = 0.172326
  Training set accuracy = 0.996200, Test set accuracy = 0.949400
 epoch 58/100. Took 0.87191 seconds. 
  Full-batch training loss = 0.036111, test loss = 0.175062
  Training set accuracy = 0.995400, Test set accuracy = 0.949500
 epoch 59/100. Took 0.87045 seconds. 
  Full-batch training loss = 0.034417, test loss = 0.171781
  Training set accuracy = 0.996200, Test set accuracy = 0.949100
 epoch 60/100. Took 0.87816 seconds. 
  Full-batch training loss = 0.033587, test loss = 0.173086
  Training set accuracy = 0.996800, Test set accuracy = 0.948900
 epoch 61/100. Took 0.86815 seconds. 
  Full-batch training loss = 0.032506, test loss = 0.171606
  Training set accuracy = 0.996800, Test set accuracy = 0.949700
 epoch 62/100. Took 0.87377 seconds. 
  Full-batch training loss = 0.032001, test loss = 0.171423
  Training set accuracy = 0.996800, Test set accuracy = 0.949400
 epoch 63/100. Took 0.87838 seconds. 
  Full-batch training loss = 0.031071, test loss = 0.173120
  Training set accuracy = 0.997400, Test set accuracy = 0.950300
 epoch 64/100. Took 0.85907 seconds. 
  Full-batch training loss = 0.030176, test loss = 0.171915
  Training set accuracy = 0.997600, Test set accuracy = 0.950300
 epoch 65/100. Took 0.86216 seconds. 
  Full-batch training loss = 0.030009, test loss = 0.171944
  Training set accuracy = 0.997200, Test set accuracy = 0.949300
 epoch 66/100. Took 0.88394 seconds. 
  Full-batch training loss = 0.028870, test loss = 0.171744
  Training set accuracy = 0.998000, Test set accuracy = 0.949600
 epoch 67/100. Took 0.8846 seconds. 
  Full-batch training loss = 0.028131, test loss = 0.172865
  Training set accuracy = 0.998400, Test set accuracy = 0.949300
 epoch 68/100. Took 0.86197 seconds. 
  Full-batch training loss = 0.027240, test loss = 0.171870
  Training set accuracy = 0.998600, Test set accuracy = 0.950200
 epoch 69/100. Took 0.87094 seconds. 
  Full-batch training loss = 0.026738, test loss = 0.172049
  Training set accuracy = 0.999000, Test set accuracy = 0.950700
 epoch 70/100. Took 0.87412 seconds. 
  Full-batch training loss = 0.025994, test loss = 0.171426
  Training set accuracy = 0.998800, Test set accuracy = 0.950200
 epoch 71/100. Took 0.87441 seconds. 
  Full-batch training loss = 0.025339, test loss = 0.172196
  Training set accuracy = 0.998800, Test set accuracy = 0.950400
 epoch 72/100. Took 0.87526 seconds. 
  Full-batch training loss = 0.025036, test loss = 0.171534
  Training set accuracy = 0.999000, Test set accuracy = 0.949900
 epoch 73/100. Took 0.87566 seconds. 
  Full-batch training loss = 0.024278, test loss = 0.172103
  Training set accuracy = 0.999000, Test set accuracy = 0.949900
 epoch 74/100. Took 0.88123 seconds. 
  Full-batch training loss = 0.023557, test loss = 0.172099
  Training set accuracy = 0.999200, Test set accuracy = 0.949800
 epoch 75/100. Took 0.87634 seconds. 
  Full-batch training loss = 0.023234, test loss = 0.172556
  Training set accuracy = 0.999200, Test set accuracy = 0.950500
 epoch 76/100. Took 0.86644 seconds. 
  Full-batch training loss = 0.022446, test loss = 0.172014
  Training set accuracy = 0.999200, Test set accuracy = 0.949800
 epoch 77/100. Took 0.87763 seconds. 
  Full-batch training loss = 0.022041, test loss = 0.172441
  Training set accuracy = 0.999200, Test set accuracy = 0.950200
 epoch 78/100. Took 0.87144 seconds. 
  Full-batch training loss = 0.021612, test loss = 0.172945
  Training set accuracy = 0.999200, Test set accuracy = 0.950800
 epoch 79/100. Took 0.86579 seconds. 
  Full-batch training loss = 0.021178, test loss = 0.172486
  Training set accuracy = 0.999000, Test set accuracy = 0.949900
 epoch 80/100. Took 0.86901 seconds. 
  Full-batch training loss = 0.020638, test loss = 0.172404
  Training set accuracy = 0.999200, Test set accuracy = 0.950500
 epoch 81/100. Took 0.86819 seconds. 
  Full-batch training loss = 0.020254, test loss = 0.173024
  Training set accuracy = 0.999200, Test set accuracy = 0.949900
 epoch 82/100. Took 0.88311 seconds. 
  Full-batch training loss = 0.019790, test loss = 0.172460
  Training set accuracy = 0.999400, Test set accuracy = 0.950300
 epoch 83/100. Took 0.87514 seconds. 
  Full-batch training loss = 0.019392, test loss = 0.172115
  Training set accuracy = 0.999400, Test set accuracy = 0.949700
 epoch 84/100. Took 0.8811 seconds. 
  Full-batch training loss = 0.019218, test loss = 0.171964
  Training set accuracy = 0.999600, Test set accuracy = 0.950200
 epoch 85/100. Took 0.87171 seconds. 
  Full-batch training loss = 0.018704, test loss = 0.173271
  Training set accuracy = 0.999200, Test set accuracy = 0.950800
 epoch 86/100. Took 0.86837 seconds. 
  Full-batch training loss = 0.018236, test loss = 0.173009
  Training set accuracy = 0.999400, Test set accuracy = 0.951400
 epoch 87/100. Took 0.87188 seconds. 
  Full-batch training loss = 0.017975, test loss = 0.172503
  Training set accuracy = 0.999600, Test set accuracy = 0.950700
 epoch 88/100. Took 0.88006 seconds. 
  Full-batch training loss = 0.017569, test loss = 0.173588
  Training set accuracy = 0.999600, Test set accuracy = 0.950900
 epoch 89/100. Took 0.87014 seconds. 
  Full-batch training loss = 0.017202, test loss = 0.173346
  Training set accuracy = 0.999400, Test set accuracy = 0.950900
 epoch 90/100. Took 0.87064 seconds. 
  Full-batch training loss = 0.016789, test loss = 0.173018
  Training set accuracy = 0.999600, Test set accuracy = 0.950800
 epoch 91/100. Took 0.87914 seconds. 
  Full-batch training loss = 0.016509, test loss = 0.173109
  Training set accuracy = 0.999400, Test set accuracy = 0.950300
 epoch 92/100. Took 0.87093 seconds. 
  Full-batch training loss = 0.016367, test loss = 0.174061
  Training set accuracy = 0.999400, Test set accuracy = 0.950800
 epoch 93/100. Took 0.87731 seconds. 
  Full-batch training loss = 0.015902, test loss = 0.173455
  Training set accuracy = 0.999600, Test set accuracy = 0.950500
 epoch 94/100. Took 0.87159 seconds. 
  Full-batch training loss = 0.015597, test loss = 0.172974
  Training set accuracy = 0.999600, Test set accuracy = 0.950100
 epoch 95/100. Took 0.88031 seconds. 
  Full-batch training loss = 0.015351, test loss = 0.173928
  Training set accuracy = 0.999800, Test set accuracy = 0.951000
 epoch 96/100. Took 0.85949 seconds. 
  Full-batch training loss = 0.015003, test loss = 0.173519
  Training set accuracy = 0.999800, Test set accuracy = 0.950900
 epoch 97/100. Took 0.87401 seconds. 
  Full-batch training loss = 0.014758, test loss = 0.173429
  Training set accuracy = 0.999800, Test set accuracy = 0.950600
 epoch 98/100. Took 0.88183 seconds. 
  Full-batch training loss = 0.014467, test loss = 0.174034
  Training set accuracy = 0.999800, Test set accuracy = 0.951400
 epoch 99/100. Took 0.87289 seconds. 
  Full-batch training loss = 0.014229, test loss = 0.173726
  Training set accuracy = 0.999800, Test set accuracy = 0.951000
 epoch 100/100. Took 0.87911 seconds. 
  Full-batch training loss = 0.014187, test loss = 0.173919
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
Elapsed time is 222.343491 seconds.
End Training
