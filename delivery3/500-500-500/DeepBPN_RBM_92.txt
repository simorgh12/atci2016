==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 92/270
----------------------------------------------------------
* ReducedData	5000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.10
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.20
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.56962 seconds. Average reconstruction error is: 35.5834
 epoch 2/20. Took 0.57316 seconds. Average reconstruction error is: 18.8317
 epoch 3/20. Took 0.57695 seconds. Average reconstruction error is: 15.6618
 epoch 4/20. Took 0.55631 seconds. Average reconstruction error is: 13.9451
 epoch 5/20. Took 0.57746 seconds. Average reconstruction error is: 12.7956
 epoch 6/20. Took 0.56175 seconds. Average reconstruction error is: 12.059
 epoch 7/20. Took 0.57765 seconds. Average reconstruction error is: 11.4185
 epoch 8/20. Took 0.56029 seconds. Average reconstruction error is: 10.8857
 epoch 9/20. Took 0.57567 seconds. Average reconstruction error is: 10.4572
 epoch 10/20. Took 0.57521 seconds. Average reconstruction error is: 10.1065
 epoch 11/20. Took 0.6382 seconds. Average reconstruction error is: 9.8478
 epoch 12/20. Took 0.60105 seconds. Average reconstruction error is: 9.5579
 epoch 13/20. Took 0.562 seconds. Average reconstruction error is: 9.3408
 epoch 14/20. Took 0.66722 seconds. Average reconstruction error is: 9.1302
 epoch 15/20. Took 0.59468 seconds. Average reconstruction error is: 8.9836
 epoch 16/20. Took 0.56565 seconds. Average reconstruction error is: 8.7713
 epoch 17/20. Took 0.5587 seconds. Average reconstruction error is: 8.6325
 epoch 18/20. Took 0.63891 seconds. Average reconstruction error is: 8.499
 epoch 19/20. Took 0.61782 seconds. Average reconstruction error is: 8.4493
 epoch 20/20. Took 0.56481 seconds. Average reconstruction error is: 8.3614
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.38716 seconds. Average reconstruction error is: 30.4249
 epoch 2/20. Took 0.4341 seconds. Average reconstruction error is: 16.2431
 epoch 3/20. Took 0.38429 seconds. Average reconstruction error is: 13.1079
 epoch 4/20. Took 0.37823 seconds. Average reconstruction error is: 11.6019
 epoch 5/20. Took 0.38737 seconds. Average reconstruction error is: 10.6971
 epoch 6/20. Took 0.41847 seconds. Average reconstruction error is: 10.1343
 epoch 7/20. Took 0.40645 seconds. Average reconstruction error is: 9.7078
 epoch 8/20. Took 0.38709 seconds. Average reconstruction error is: 9.407
 epoch 9/20. Took 0.38971 seconds. Average reconstruction error is: 9.1856
 epoch 10/20. Took 0.41163 seconds. Average reconstruction error is: 8.9582
 epoch 11/20. Took 0.40342 seconds. Average reconstruction error is: 8.755
 epoch 12/20. Took 0.40986 seconds. Average reconstruction error is: 8.6502
 epoch 13/20. Took 0.39859 seconds. Average reconstruction error is: 8.5517
 epoch 14/20. Took 0.37668 seconds. Average reconstruction error is: 8.3961
 epoch 15/20. Took 0.37321 seconds. Average reconstruction error is: 8.334
 epoch 16/20. Took 0.37628 seconds. Average reconstruction error is: 8.1578
 epoch 17/20. Took 0.38018 seconds. Average reconstruction error is: 8.1376
 epoch 18/20. Took 0.3817 seconds. Average reconstruction error is: 8.0736
 epoch 19/20. Took 0.37342 seconds. Average reconstruction error is: 7.9614
 epoch 20/20. Took 0.37803 seconds. Average reconstruction error is: 7.9022
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.37828 seconds. Average reconstruction error is: 22.6423
 epoch 2/20. Took 0.37542 seconds. Average reconstruction error is: 10.3686
 epoch 3/20. Took 0.37858 seconds. Average reconstruction error is: 8.2871
 epoch 4/20. Took 0.37727 seconds. Average reconstruction error is: 7.3571
 epoch 5/20. Took 0.3806 seconds. Average reconstruction error is: 6.8998
 epoch 6/20. Took 0.37795 seconds. Average reconstruction error is: 6.5578
 epoch 7/20. Took 0.37338 seconds. Average reconstruction error is: 6.3264
 epoch 8/20. Took 0.37478 seconds. Average reconstruction error is: 6.1606
 epoch 9/20. Took 0.37638 seconds. Average reconstruction error is: 6.0596
 epoch 10/20. Took 0.38026 seconds. Average reconstruction error is: 5.971
 epoch 11/20. Took 0.38866 seconds. Average reconstruction error is: 5.8766
 epoch 12/20. Took 0.38033 seconds. Average reconstruction error is: 5.8346
 epoch 13/20. Took 0.39952 seconds. Average reconstruction error is: 5.7429
 epoch 14/20. Took 0.42235 seconds. Average reconstruction error is: 5.6963
 epoch 15/20. Took 0.45883 seconds. Average reconstruction error is: 5.7001
 epoch 16/20. Took 0.41182 seconds. Average reconstruction error is: 5.6729
 epoch 17/20. Took 0.38208 seconds. Average reconstruction error is: 5.6282
 epoch 18/20. Took 0.39029 seconds. Average reconstruction error is: 5.6033
 epoch 19/20. Took 0.39785 seconds. Average reconstruction error is: 5.5497
 epoch 20/20. Took 0.38239 seconds. Average reconstruction error is: 5.5944
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 0.94252 seconds. 
  Full-batch training loss = 0.308409, test loss = 0.332876
  Training set accuracy = 0.905400, Test set accuracy = 0.900100
 epoch 2/100. Took 0.96727 seconds. 
  Full-batch training loss = 0.232456, test loss = 0.281592
  Training set accuracy = 0.930800, Test set accuracy = 0.913100
 epoch 3/100. Took 1.1107 seconds. 
  Full-batch training loss = 0.179954, test loss = 0.243794
  Training set accuracy = 0.946200, Test set accuracy = 0.925200
 epoch 4/100. Took 1.1356 seconds. 
  Full-batch training loss = 0.147846, test loss = 0.221357
  Training set accuracy = 0.957600, Test set accuracy = 0.931200
 epoch 5/100. Took 1.0172 seconds. 
  Full-batch training loss = 0.140022, test loss = 0.221582
  Training set accuracy = 0.960600, Test set accuracy = 0.932700
 epoch 6/100. Took 0.90482 seconds. 
  Full-batch training loss = 0.114370, test loss = 0.207609
  Training set accuracy = 0.970400, Test set accuracy = 0.936000
 epoch 7/100. Took 0.93436 seconds. 
  Full-batch training loss = 0.103110, test loss = 0.201127
  Training set accuracy = 0.974400, Test set accuracy = 0.937100
 epoch 8/100. Took 0.90058 seconds. 
  Full-batch training loss = 0.083727, test loss = 0.191789
  Training set accuracy = 0.982400, Test set accuracy = 0.940900
 epoch 9/100. Took 0.93378 seconds. 
  Full-batch training loss = 0.078641, test loss = 0.191683
  Training set accuracy = 0.983600, Test set accuracy = 0.940800
 epoch 10/100. Took 0.99542 seconds. 
  Full-batch training loss = 0.066538, test loss = 0.187764
  Training set accuracy = 0.986400, Test set accuracy = 0.940900
 epoch 11/100. Took 0.91668 seconds. 
  Full-batch training loss = 0.063656, test loss = 0.196126
  Training set accuracy = 0.987400, Test set accuracy = 0.941300
 epoch 12/100. Took 1.0207 seconds. 
  Full-batch training loss = 0.052664, test loss = 0.185893
  Training set accuracy = 0.991000, Test set accuracy = 0.943100
 epoch 13/100. Took 0.90952 seconds. 
  Full-batch training loss = 0.049021, test loss = 0.186716
  Training set accuracy = 0.991400, Test set accuracy = 0.943600
 epoch 14/100. Took 0.89108 seconds. 
  Full-batch training loss = 0.044550, test loss = 0.189012
  Training set accuracy = 0.992600, Test set accuracy = 0.942800
 epoch 15/100. Took 0.90969 seconds. 
  Full-batch training loss = 0.037515, test loss = 0.184893
  Training set accuracy = 0.993400, Test set accuracy = 0.945100
 epoch 16/100. Took 0.92737 seconds. 
  Full-batch training loss = 0.034914, test loss = 0.186257
  Training set accuracy = 0.995600, Test set accuracy = 0.944100
 epoch 17/100. Took 0.93382 seconds. 
  Full-batch training loss = 0.030850, test loss = 0.182984
  Training set accuracy = 0.996600, Test set accuracy = 0.944500
 epoch 18/100. Took 0.90456 seconds. 
  Full-batch training loss = 0.027941, test loss = 0.183715
  Training set accuracy = 0.997200, Test set accuracy = 0.945700
 epoch 19/100. Took 0.93699 seconds. 
  Full-batch training loss = 0.026395, test loss = 0.184881
  Training set accuracy = 0.997800, Test set accuracy = 0.946100
 epoch 20/100. Took 0.92517 seconds. 
  Full-batch training loss = 0.023030, test loss = 0.181805
  Training set accuracy = 0.999000, Test set accuracy = 0.946200
 epoch 21/100. Took 0.98086 seconds. 
  Full-batch training loss = 0.021673, test loss = 0.181668
  Training set accuracy = 0.999200, Test set accuracy = 0.945700
 epoch 22/100. Took 0.97493 seconds. 
  Full-batch training loss = 0.020311, test loss = 0.183710
  Training set accuracy = 0.998800, Test set accuracy = 0.946300
 epoch 23/100. Took 0.91396 seconds. 
  Full-batch training loss = 0.018507, test loss = 0.184839
  Training set accuracy = 0.999400, Test set accuracy = 0.947700
 epoch 24/100. Took 0.95442 seconds. 
  Full-batch training loss = 0.016817, test loss = 0.182440
  Training set accuracy = 0.999200, Test set accuracy = 0.948000
 epoch 25/100. Took 0.99391 seconds. 
  Full-batch training loss = 0.016281, test loss = 0.184141
  Training set accuracy = 0.999400, Test set accuracy = 0.946700
 epoch 26/100. Took 0.96214 seconds. 
  Full-batch training loss = 0.015123, test loss = 0.185057
  Training set accuracy = 0.999800, Test set accuracy = 0.947900
 epoch 27/100. Took 1.0159 seconds. 
  Full-batch training loss = 0.014266, test loss = 0.185840
  Training set accuracy = 0.999800, Test set accuracy = 0.946600
 epoch 28/100. Took 0.89649 seconds. 
  Full-batch training loss = 0.012940, test loss = 0.185018
  Training set accuracy = 0.999800, Test set accuracy = 0.948200
 epoch 29/100. Took 0.91813 seconds. 
  Full-batch training loss = 0.012149, test loss = 0.187123
  Training set accuracy = 1.000000, Test set accuracy = 0.948500
 epoch 30/100. Took 0.92997 seconds. 
  Full-batch training loss = 0.011498, test loss = 0.185438
  Training set accuracy = 1.000000, Test set accuracy = 0.948300
 epoch 31/100. Took 0.90149 seconds. 
  Full-batch training loss = 0.010901, test loss = 0.186570
  Training set accuracy = 1.000000, Test set accuracy = 0.949200
 epoch 32/100. Took 0.92197 seconds. 
  Full-batch training loss = 0.010338, test loss = 0.185340
  Training set accuracy = 1.000000, Test set accuracy = 0.948800
 epoch 33/100. Took 0.93919 seconds. 
  Full-batch training loss = 0.009997, test loss = 0.186897
  Training set accuracy = 1.000000, Test set accuracy = 0.948500
 epoch 34/100. Took 0.91932 seconds. 
  Full-batch training loss = 0.009408, test loss = 0.188056
  Training set accuracy = 1.000000, Test set accuracy = 0.949200
 epoch 35/100. Took 0.95157 seconds. 
  Full-batch training loss = 0.008926, test loss = 0.187667
  Training set accuracy = 1.000000, Test set accuracy = 0.949200
 epoch 36/100. Took 0.90302 seconds. 
  Full-batch training loss = 0.008655, test loss = 0.189300
  Training set accuracy = 1.000000, Test set accuracy = 0.948600
 epoch 37/100. Took 1.0911 seconds. 
  Full-batch training loss = 0.008249, test loss = 0.188621
  Training set accuracy = 1.000000, Test set accuracy = 0.948400
 epoch 38/100. Took 0.9188 seconds. 
  Full-batch training loss = 0.007731, test loss = 0.188533
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 39/100. Took 0.92648 seconds. 
  Full-batch training loss = 0.007451, test loss = 0.189264
  Training set accuracy = 1.000000, Test set accuracy = 0.949100
 epoch 40/100. Took 0.89911 seconds. 
  Full-batch training loss = 0.007101, test loss = 0.189165
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 41/100. Took 0.87694 seconds. 
  Full-batch training loss = 0.006883, test loss = 0.189218
  Training set accuracy = 1.000000, Test set accuracy = 0.948800
 epoch 42/100. Took 0.88285 seconds. 
  Full-batch training loss = 0.006605, test loss = 0.190072
  Training set accuracy = 1.000000, Test set accuracy = 0.949100
 epoch 43/100. Took 1.0966 seconds. 
  Full-batch training loss = 0.006423, test loss = 0.190088
  Training set accuracy = 1.000000, Test set accuracy = 0.949200
 epoch 44/100. Took 1.0081 seconds. 
  Full-batch training loss = 0.006199, test loss = 0.191197
  Training set accuracy = 1.000000, Test set accuracy = 0.949400
 epoch 45/100. Took 1.0574 seconds. 
  Full-batch training loss = 0.005939, test loss = 0.190979
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 46/100. Took 0.91075 seconds. 
  Full-batch training loss = 0.005741, test loss = 0.191758
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 47/100. Took 0.88393 seconds. 
  Full-batch training loss = 0.005589, test loss = 0.192924
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 48/100. Took 0.90307 seconds. 
  Full-batch training loss = 0.005347, test loss = 0.192524
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 49/100. Took 0.87857 seconds. 
  Full-batch training loss = 0.005194, test loss = 0.192958
  Training set accuracy = 1.000000, Test set accuracy = 0.949700
 epoch 50/100. Took 0.872 seconds. 
  Full-batch training loss = 0.005059, test loss = 0.193220
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 51/100. Took 0.91818 seconds. 
  Full-batch training loss = 0.004904, test loss = 0.193379
  Training set accuracy = 1.000000, Test set accuracy = 0.949200
 epoch 52/100. Took 0.87561 seconds. 
  Full-batch training loss = 0.004783, test loss = 0.193709
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 53/100. Took 0.8761 seconds. 
  Full-batch training loss = 0.004616, test loss = 0.194200
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 54/100. Took 0.90051 seconds. 
  Full-batch training loss = 0.004496, test loss = 0.194607
  Training set accuracy = 1.000000, Test set accuracy = 0.949500
 epoch 55/100. Took 0.89606 seconds. 
  Full-batch training loss = 0.004361, test loss = 0.194663
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 56/100. Took 0.87685 seconds. 
  Full-batch training loss = 0.004241, test loss = 0.194795
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 57/100. Took 0.9241 seconds. 
  Full-batch training loss = 0.004129, test loss = 0.195075
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 58/100. Took 0.90717 seconds. 
  Full-batch training loss = 0.004022, test loss = 0.195157
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 59/100. Took 0.948 seconds. 
  Full-batch training loss = 0.003927, test loss = 0.195405
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 60/100. Took 0.97016 seconds. 
  Full-batch training loss = 0.003828, test loss = 0.195978
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 61/100. Took 1.0776 seconds. 
  Full-batch training loss = 0.003762, test loss = 0.196581
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 62/100. Took 1.0298 seconds. 
  Full-batch training loss = 0.003646, test loss = 0.196513
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 63/100. Took 0.92251 seconds. 
  Full-batch training loss = 0.003563, test loss = 0.196694
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 64/100. Took 0.91927 seconds. 
  Full-batch training loss = 0.003474, test loss = 0.197197
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 65/100. Took 0.99955 seconds. 
  Full-batch training loss = 0.003413, test loss = 0.197550
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 66/100. Took 0.91299 seconds. 
  Full-batch training loss = 0.003332, test loss = 0.197684
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 67/100. Took 0.96524 seconds. 
  Full-batch training loss = 0.003269, test loss = 0.197910
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 68/100. Took 0.92499 seconds. 
  Full-batch training loss = 0.003190, test loss = 0.197829
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 69/100. Took 1.1201 seconds. 
  Full-batch training loss = 0.003119, test loss = 0.198160
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 70/100. Took 1.0003 seconds. 
  Full-batch training loss = 0.003055, test loss = 0.198682
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 71/100. Took 1.0087 seconds. 
  Full-batch training loss = 0.002994, test loss = 0.198764
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 72/100. Took 1.0532 seconds. 
  Full-batch training loss = 0.002944, test loss = 0.198832
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 73/100. Took 0.96076 seconds. 
  Full-batch training loss = 0.002881, test loss = 0.199201
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 74/100. Took 1.0254 seconds. 
  Full-batch training loss = 0.002822, test loss = 0.199868
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 75/100. Took 1.0498 seconds. 
  Full-batch training loss = 0.002777, test loss = 0.199791
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 76/100. Took 0.92411 seconds. 
  Full-batch training loss = 0.002726, test loss = 0.200319
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 77/100. Took 0.91492 seconds. 
  Full-batch training loss = 0.002668, test loss = 0.200121
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 78/100. Took 0.91981 seconds. 
  Full-batch training loss = 0.002624, test loss = 0.200297
  Training set accuracy = 1.000000, Test set accuracy = 0.949800
 epoch 79/100. Took 0.9448 seconds. 
  Full-batch training loss = 0.002570, test loss = 0.200841
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 80/100. Took 0.94229 seconds. 
  Full-batch training loss = 0.002528, test loss = 0.201103
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 81/100. Took 0.98002 seconds. 
  Full-batch training loss = 0.002486, test loss = 0.201560
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 82/100. Took 0.94148 seconds. 
  Full-batch training loss = 0.002441, test loss = 0.201227
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 83/100. Took 0.91411 seconds. 
  Full-batch training loss = 0.002403, test loss = 0.201815
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 84/100. Took 0.94197 seconds. 
  Full-batch training loss = 0.002360, test loss = 0.202034
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 85/100. Took 0.92176 seconds. 
  Full-batch training loss = 0.002324, test loss = 0.201838
  Training set accuracy = 1.000000, Test set accuracy = 0.949900
 epoch 86/100. Took 0.94909 seconds. 
  Full-batch training loss = 0.002288, test loss = 0.202524
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 87/100. Took 0.98226 seconds. 
  Full-batch training loss = 0.002250, test loss = 0.202169
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 88/100. Took 0.94002 seconds. 
  Full-batch training loss = 0.002211, test loss = 0.202482
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 89/100. Took 0.9175 seconds. 
  Full-batch training loss = 0.002182, test loss = 0.202618
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 90/100. Took 0.92466 seconds. 
  Full-batch training loss = 0.002145, test loss = 0.203191
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 91/100. Took 0.91588 seconds. 
  Full-batch training loss = 0.002114, test loss = 0.203045
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
 epoch 92/100. Took 0.95506 seconds. 
  Full-batch training loss = 0.002081, test loss = 0.203489
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 93/100. Took 0.91046 seconds. 
  Full-batch training loss = 0.002051, test loss = 0.203441
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 94/100. Took 0.92062 seconds. 
  Full-batch training loss = 0.002020, test loss = 0.203847
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 95/100. Took 0.95151 seconds. 
  Full-batch training loss = 0.001992, test loss = 0.204032
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 96/100. Took 1.0076 seconds. 
  Full-batch training loss = 0.001963, test loss = 0.204214
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 97/100. Took 1.0889 seconds. 
  Full-batch training loss = 0.001936, test loss = 0.204463
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 98/100. Took 1.0354 seconds. 
  Full-batch training loss = 0.001908, test loss = 0.204516
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 99/100. Took 0.90858 seconds. 
  Full-batch training loss = 0.001884, test loss = 0.204830
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 100/100. Took 1.007 seconds. 
  Full-batch training loss = 0.001857, test loss = 0.204840
  Training set accuracy = 1.000000, Test set accuracy = 0.950100
Elapsed time is 237.807460 seconds.
End Training
