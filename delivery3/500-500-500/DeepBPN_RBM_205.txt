==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 205/270
----------------------------------------------------------
* ReducedData	10000
* PreTrainRBM	true
	-> Epochs	50
	-> LearningRate	0.10
* Fine-tunning
	-> Epochs	100
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 10000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 50 epochs
 epoch 1/50. Took 1.2012 seconds. Average reconstruction error is: 27.2081
 epoch 2/50. Took 1.2335 seconds. Average reconstruction error is: 14.8985
 epoch 3/50. Took 1.1385 seconds. Average reconstruction error is: 12.5852
 epoch 4/50. Took 1.1313 seconds. Average reconstruction error is: 11.3461
 epoch 5/50. Took 1.156 seconds. Average reconstruction error is: 10.5641
 epoch 6/50. Took 1.1749 seconds. Average reconstruction error is: 9.9304
 epoch 7/50. Took 1.1554 seconds. Average reconstruction error is: 9.5103
 epoch 8/50. Took 1.1481 seconds. Average reconstruction error is: 9.1703
 epoch 9/50. Took 1.1708 seconds. Average reconstruction error is: 8.9647
 epoch 10/50. Took 1.2332 seconds. Average reconstruction error is: 8.7507
 epoch 11/50. Took 1.1894 seconds. Average reconstruction error is: 8.5759
 epoch 12/50. Took 1.1725 seconds. Average reconstruction error is: 8.4757
 epoch 13/50. Took 1.1713 seconds. Average reconstruction error is: 8.354
 epoch 14/50. Took 1.1514 seconds. Average reconstruction error is: 8.2461
 epoch 15/50. Took 1.1354 seconds. Average reconstruction error is: 8.1659
 epoch 16/50. Took 1.1935 seconds. Average reconstruction error is: 8.1178
 epoch 17/50. Took 1.1516 seconds. Average reconstruction error is: 8.0873
 epoch 18/50. Took 1.1598 seconds. Average reconstruction error is: 8.0327
 epoch 19/50. Took 1.1816 seconds. Average reconstruction error is: 7.992
 epoch 20/50. Took 1.1257 seconds. Average reconstruction error is: 7.945
 epoch 21/50. Took 1.2241 seconds. Average reconstruction error is: 7.8937
 epoch 22/50. Took 1.2167 seconds. Average reconstruction error is: 7.8493
 epoch 23/50. Took 1.1954 seconds. Average reconstruction error is: 7.8
 epoch 24/50. Took 1.1729 seconds. Average reconstruction error is: 7.7975
 epoch 25/50. Took 1.1319 seconds. Average reconstruction error is: 7.7732
 epoch 26/50. Took 1.092 seconds. Average reconstruction error is: 7.7155
 epoch 27/50. Took 1.0901 seconds. Average reconstruction error is: 7.7021
 epoch 28/50. Took 1.1313 seconds. Average reconstruction error is: 7.647
 epoch 29/50. Took 1.1112 seconds. Average reconstruction error is: 7.6628
 epoch 30/50. Took 1.119 seconds. Average reconstruction error is: 7.6393
 epoch 31/50. Took 1.0904 seconds. Average reconstruction error is: 7.6312
 epoch 32/50. Took 1.0824 seconds. Average reconstruction error is: 7.6032
 epoch 33/50. Took 1.0916 seconds. Average reconstruction error is: 7.5354
 epoch 34/50. Took 1.0849 seconds. Average reconstruction error is: 7.5558
 epoch 35/50. Took 1.0845 seconds. Average reconstruction error is: 7.4989
 epoch 36/50. Took 1.0922 seconds. Average reconstruction error is: 7.4931
 epoch 37/50. Took 1.0971 seconds. Average reconstruction error is: 7.4797
 epoch 38/50. Took 1.1274 seconds. Average reconstruction error is: 7.4914
 epoch 39/50. Took 1.1213 seconds. Average reconstruction error is: 7.4471
 epoch 40/50. Took 1.1017 seconds. Average reconstruction error is: 7.3938
 epoch 41/50. Took 1.1126 seconds. Average reconstruction error is: 7.3789
 epoch 42/50. Took 1.0977 seconds. Average reconstruction error is: 7.375
 epoch 43/50. Took 1.1066 seconds. Average reconstruction error is: 7.3861
 epoch 44/50. Took 1.0871 seconds. Average reconstruction error is: 7.3666
 epoch 45/50. Took 1.1164 seconds. Average reconstruction error is: 7.3736
 epoch 46/50. Took 1.1124 seconds. Average reconstruction error is: 7.3765
 epoch 47/50. Took 1.1308 seconds. Average reconstruction error is: 7.3109
 epoch 48/50. Took 1.1405 seconds. Average reconstruction error is: 7.3042
 epoch 49/50. Took 1.0916 seconds. Average reconstruction error is: 7.2678
 epoch 50/50. Took 1.1047 seconds. Average reconstruction error is: 7.3013
Training binary-binary RBM in layer 2 (500  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.76722 seconds. Average reconstruction error is: 27.2743
 epoch 2/50. Took 0.75977 seconds. Average reconstruction error is: 15.2199
 epoch 3/50. Took 0.75566 seconds. Average reconstruction error is: 13.1127
 epoch 4/50. Took 0.7587 seconds. Average reconstruction error is: 12.084
 epoch 5/50. Took 0.79976 seconds. Average reconstruction error is: 11.4389
 epoch 6/50. Took 0.75965 seconds. Average reconstruction error is: 11.0293
 epoch 7/50. Took 0.7741 seconds. Average reconstruction error is: 10.6725
 epoch 8/50. Took 0.76493 seconds. Average reconstruction error is: 10.4171
 epoch 9/50. Took 0.74992 seconds. Average reconstruction error is: 10.1766
 epoch 10/50. Took 0.76308 seconds. Average reconstruction error is: 9.9247
 epoch 11/50. Took 0.75737 seconds. Average reconstruction error is: 9.8043
 epoch 12/50. Took 0.76474 seconds. Average reconstruction error is: 9.6101
 epoch 13/50. Took 0.75873 seconds. Average reconstruction error is: 9.4584
 epoch 14/50. Took 0.76515 seconds. Average reconstruction error is: 9.3512
 epoch 15/50. Took 0.75106 seconds. Average reconstruction error is: 9.1844
 epoch 16/50. Took 0.76292 seconds. Average reconstruction error is: 9.1006
 epoch 17/50. Took 0.74483 seconds. Average reconstruction error is: 8.9814
 epoch 18/50. Took 0.76354 seconds. Average reconstruction error is: 8.8681
 epoch 19/50. Took 0.75744 seconds. Average reconstruction error is: 8.78
 epoch 20/50. Took 0.75732 seconds. Average reconstruction error is: 8.6645
 epoch 21/50. Took 0.7558 seconds. Average reconstruction error is: 8.5763
 epoch 22/50. Took 0.7624 seconds. Average reconstruction error is: 8.4462
 epoch 23/50. Took 0.75441 seconds. Average reconstruction error is: 8.4039
 epoch 24/50. Took 0.76133 seconds. Average reconstruction error is: 8.2786
 epoch 25/50. Took 0.75142 seconds. Average reconstruction error is: 8.2287
 epoch 26/50. Took 0.76083 seconds. Average reconstruction error is: 8.1735
 epoch 27/50. Took 0.7555 seconds. Average reconstruction error is: 8.1052
 epoch 28/50. Took 0.75497 seconds. Average reconstruction error is: 8.0045
 epoch 29/50. Took 0.75149 seconds. Average reconstruction error is: 7.9363
 epoch 30/50. Took 0.76011 seconds. Average reconstruction error is: 7.9003
 epoch 31/50. Took 0.75573 seconds. Average reconstruction error is: 7.8612
 epoch 32/50. Took 0.76079 seconds. Average reconstruction error is: 7.756
 epoch 33/50. Took 0.75832 seconds. Average reconstruction error is: 7.7045
 epoch 34/50. Took 0.76195 seconds. Average reconstruction error is: 7.6855
 epoch 35/50. Took 0.75555 seconds. Average reconstruction error is: 7.5954
 epoch 36/50. Took 0.76018 seconds. Average reconstruction error is: 7.5962
 epoch 37/50. Took 0.75446 seconds. Average reconstruction error is: 7.5106
 epoch 38/50. Took 0.7774 seconds. Average reconstruction error is: 7.4715
 epoch 39/50. Took 0.79091 seconds. Average reconstruction error is: 7.398
 epoch 40/50. Took 0.79669 seconds. Average reconstruction error is: 7.3977
 epoch 41/50. Took 0.77509 seconds. Average reconstruction error is: 7.2907
 epoch 42/50. Took 0.80207 seconds. Average reconstruction error is: 7.2681
 epoch 43/50. Took 0.77676 seconds. Average reconstruction error is: 7.2659
 epoch 44/50. Took 0.76589 seconds. Average reconstruction error is: 7.2413
 epoch 45/50. Took 0.75584 seconds. Average reconstruction error is: 7.1792
 epoch 46/50. Took 0.76434 seconds. Average reconstruction error is: 7.168
 epoch 47/50. Took 0.77793 seconds. Average reconstruction error is: 7.0898
 epoch 48/50. Took 0.76904 seconds. Average reconstruction error is: 7.0642
 epoch 49/50. Took 0.78611 seconds. Average reconstruction error is: 7.0142
 epoch 50/50. Took 0.77884 seconds. Average reconstruction error is: 7.0111
Training binary-binary RBM in layer 3 (500  500) with CD1 for 50 epochs
 epoch 1/50. Took 0.74906 seconds. Average reconstruction error is: 18.255
 epoch 2/50. Took 0.75375 seconds. Average reconstruction error is: 8.794
 epoch 3/50. Took 0.74462 seconds. Average reconstruction error is: 7.3436
 epoch 4/50. Took 0.75093 seconds. Average reconstruction error is: 6.7036
 epoch 5/50. Took 0.74481 seconds. Average reconstruction error is: 6.3594
 epoch 6/50. Took 0.74767 seconds. Average reconstruction error is: 6.1003
 epoch 7/50. Took 0.77897 seconds. Average reconstruction error is: 5.9034
 epoch 8/50. Took 0.77592 seconds. Average reconstruction error is: 5.7794
 epoch 9/50. Took 0.76403 seconds. Average reconstruction error is: 5.6754
 epoch 10/50. Took 0.79172 seconds. Average reconstruction error is: 5.626
 epoch 11/50. Took 0.76162 seconds. Average reconstruction error is: 5.5148
 epoch 12/50. Took 0.79076 seconds. Average reconstruction error is: 5.4242
 epoch 13/50. Took 0.76204 seconds. Average reconstruction error is: 5.3657
 epoch 14/50. Took 0.78096 seconds. Average reconstruction error is: 5.3227
 epoch 15/50. Took 0.77065 seconds. Average reconstruction error is: 5.2948
 epoch 16/50. Took 0.78689 seconds. Average reconstruction error is: 5.2568
 epoch 17/50. Took 0.75451 seconds. Average reconstruction error is: 5.209
 epoch 18/50. Took 0.76167 seconds. Average reconstruction error is: 5.1746
 epoch 19/50. Took 0.76096 seconds. Average reconstruction error is: 5.144
 epoch 20/50. Took 0.76479 seconds. Average reconstruction error is: 5.1142
 epoch 21/50. Took 0.76203 seconds. Average reconstruction error is: 5.1022
 epoch 22/50. Took 0.7613 seconds. Average reconstruction error is: 5.0674
 epoch 23/50. Took 0.75696 seconds. Average reconstruction error is: 5.0423
 epoch 24/50. Took 0.76394 seconds. Average reconstruction error is: 5.0546
 epoch 25/50. Took 0.76164 seconds. Average reconstruction error is: 5.0162
 epoch 26/50. Took 0.76546 seconds. Average reconstruction error is: 4.9828
 epoch 27/50. Took 0.78846 seconds. Average reconstruction error is: 4.934
 epoch 28/50. Took 0.77307 seconds. Average reconstruction error is: 4.9551
 epoch 29/50. Took 0.76248 seconds. Average reconstruction error is: 4.9125
 epoch 30/50. Took 0.77902 seconds. Average reconstruction error is: 4.8997
 epoch 31/50. Took 0.77905 seconds. Average reconstruction error is: 4.8911
 epoch 32/50. Took 0.77561 seconds. Average reconstruction error is: 4.881
 epoch 33/50. Took 0.78802 seconds. Average reconstruction error is: 4.8699
 epoch 34/50. Took 0.77173 seconds. Average reconstruction error is: 4.8548
 epoch 35/50. Took 0.76978 seconds. Average reconstruction error is: 4.8364
 epoch 36/50. Took 0.77228 seconds. Average reconstruction error is: 4.8285
 epoch 37/50. Took 0.75757 seconds. Average reconstruction error is: 4.8295
 epoch 38/50. Took 0.76228 seconds. Average reconstruction error is: 4.8118
 epoch 39/50. Took 0.76212 seconds. Average reconstruction error is: 4.8039
 epoch 40/50. Took 0.75824 seconds. Average reconstruction error is: 4.7758
 epoch 41/50. Took 0.74627 seconds. Average reconstruction error is: 4.7608
 epoch 42/50. Took 0.77407 seconds. Average reconstruction error is: 4.722
 epoch 43/50. Took 0.75396 seconds. Average reconstruction error is: 4.7513
 epoch 44/50. Took 0.76729 seconds. Average reconstruction error is: 4.7237
 epoch 45/50. Took 0.75222 seconds. Average reconstruction error is: 4.7123
 epoch 46/50. Took 0.753 seconds. Average reconstruction error is: 4.7136
 epoch 47/50. Took 0.76613 seconds. Average reconstruction error is: 4.6909
 epoch 48/50. Took 0.76309 seconds. Average reconstruction error is: 4.6797
 epoch 49/50. Took 0.75291 seconds. Average reconstruction error is: 4.6736
 epoch 50/50. Took 0.75066 seconds. Average reconstruction error is: 4.6386
Training NN  (784  500  500  500   10) with BackPropagation for 100 epochs
 epoch 1/100. Took 1.8102 seconds. 
  Full-batch training loss = 0.382978, test loss = 0.381143
  Training set accuracy = 0.902300, Test set accuracy = 0.901300
 epoch 2/100. Took 1.7715 seconds. 
  Full-batch training loss = 0.295905, test loss = 0.300122
  Training set accuracy = 0.919900, Test set accuracy = 0.920200
 epoch 3/100. Took 1.7908 seconds. 
  Full-batch training loss = 0.254791, test loss = 0.266541
  Training set accuracy = 0.929300, Test set accuracy = 0.924700
 epoch 4/100. Took 1.8133 seconds. 
  Full-batch training loss = 0.230643, test loss = 0.248406
  Training set accuracy = 0.936100, Test set accuracy = 0.930100
 epoch 5/100. Took 1.7977 seconds. 
  Full-batch training loss = 0.212054, test loss = 0.236451
  Training set accuracy = 0.943300, Test set accuracy = 0.935100
 epoch 6/100. Took 1.8628 seconds. 
  Full-batch training loss = 0.193265, test loss = 0.221906
  Training set accuracy = 0.947400, Test set accuracy = 0.937000
 epoch 7/100. Took 1.7409 seconds. 
  Full-batch training loss = 0.179674, test loss = 0.212501
  Training set accuracy = 0.952000, Test set accuracy = 0.940600
 epoch 8/100. Took 1.8026 seconds. 
  Full-batch training loss = 0.168238, test loss = 0.203996
  Training set accuracy = 0.955100, Test set accuracy = 0.942300
 epoch 9/100. Took 1.7655 seconds. 
  Full-batch training loss = 0.159562, test loss = 0.198679
  Training set accuracy = 0.958700, Test set accuracy = 0.943200
 epoch 10/100. Took 1.8209 seconds. 
  Full-batch training loss = 0.150327, test loss = 0.193652
  Training set accuracy = 0.961100, Test set accuracy = 0.945600
 epoch 11/100. Took 1.7685 seconds. 
  Full-batch training loss = 0.143060, test loss = 0.188250
  Training set accuracy = 0.962900, Test set accuracy = 0.946300
 epoch 12/100. Took 1.7556 seconds. 
  Full-batch training loss = 0.135469, test loss = 0.183374
  Training set accuracy = 0.965600, Test set accuracy = 0.948100
 epoch 13/100. Took 1.8103 seconds. 
  Full-batch training loss = 0.130321, test loss = 0.181286
  Training set accuracy = 0.966700, Test set accuracy = 0.947000
 epoch 14/100. Took 1.8617 seconds. 
  Full-batch training loss = 0.123304, test loss = 0.177273
  Training set accuracy = 0.968000, Test set accuracy = 0.949900
 epoch 15/100. Took 1.8353 seconds. 
  Full-batch training loss = 0.117409, test loss = 0.173548
  Training set accuracy = 0.970200, Test set accuracy = 0.951000
 epoch 16/100. Took 1.844 seconds. 
  Full-batch training loss = 0.113214, test loss = 0.171239
  Training set accuracy = 0.970100, Test set accuracy = 0.950800
 epoch 17/100. Took 1.9257 seconds. 
  Full-batch training loss = 0.110241, test loss = 0.169672
  Training set accuracy = 0.970100, Test set accuracy = 0.951000
 epoch 18/100. Took 1.8702 seconds. 
  Full-batch training loss = 0.103273, test loss = 0.166185
  Training set accuracy = 0.974700, Test set accuracy = 0.952300
 epoch 19/100. Took 1.7626 seconds. 
  Full-batch training loss = 0.100471, test loss = 0.165229
  Training set accuracy = 0.976000, Test set accuracy = 0.951900
 epoch 20/100. Took 1.7708 seconds. 
  Full-batch training loss = 0.094758, test loss = 0.161871
  Training set accuracy = 0.978300, Test set accuracy = 0.953000
 epoch 21/100. Took 1.7952 seconds. 
  Full-batch training loss = 0.091782, test loss = 0.161900
  Training set accuracy = 0.979300, Test set accuracy = 0.953400
 epoch 22/100. Took 1.763 seconds. 
  Full-batch training loss = 0.088511, test loss = 0.158834
  Training set accuracy = 0.979600, Test set accuracy = 0.954700
 epoch 23/100. Took 1.7624 seconds. 
  Full-batch training loss = 0.084910, test loss = 0.157300
  Training set accuracy = 0.980900, Test set accuracy = 0.955200
 epoch 24/100. Took 1.7696 seconds. 
  Full-batch training loss = 0.082232, test loss = 0.156264
  Training set accuracy = 0.982900, Test set accuracy = 0.955500
 epoch 25/100. Took 1.7827 seconds. 
  Full-batch training loss = 0.078617, test loss = 0.154805
  Training set accuracy = 0.983000, Test set accuracy = 0.956200
 epoch 26/100. Took 1.8547 seconds. 
  Full-batch training loss = 0.076387, test loss = 0.153280
  Training set accuracy = 0.982600, Test set accuracy = 0.955000
 epoch 27/100. Took 1.9741 seconds. 
  Full-batch training loss = 0.073720, test loss = 0.152467
  Training set accuracy = 0.984600, Test set accuracy = 0.957000
 epoch 28/100. Took 1.8397 seconds. 
  Full-batch training loss = 0.071441, test loss = 0.151810
  Training set accuracy = 0.985400, Test set accuracy = 0.956500
 epoch 29/100. Took 1.7517 seconds. 
  Full-batch training loss = 0.068005, test loss = 0.149930
  Training set accuracy = 0.986600, Test set accuracy = 0.957600
 epoch 30/100. Took 1.7464 seconds. 
  Full-batch training loss = 0.065427, test loss = 0.148602
  Training set accuracy = 0.987200, Test set accuracy = 0.958300
 epoch 31/100. Took 1.7437 seconds. 
  Full-batch training loss = 0.063465, test loss = 0.149366
  Training set accuracy = 0.988000, Test set accuracy = 0.958100
 epoch 32/100. Took 1.7617 seconds. 
  Full-batch training loss = 0.062259, test loss = 0.148804
  Training set accuracy = 0.988200, Test set accuracy = 0.958400
 epoch 33/100. Took 1.7519 seconds. 
  Full-batch training loss = 0.059625, test loss = 0.148842
  Training set accuracy = 0.989100, Test set accuracy = 0.958500
 epoch 34/100. Took 1.8184 seconds. 
  Full-batch training loss = 0.057465, test loss = 0.147562
  Training set accuracy = 0.989800, Test set accuracy = 0.958300
 epoch 35/100. Took 1.8375 seconds. 
  Full-batch training loss = 0.055305, test loss = 0.144554
  Training set accuracy = 0.990500, Test set accuracy = 0.959000
 epoch 36/100. Took 1.8181 seconds. 
  Full-batch training loss = 0.054286, test loss = 0.145525
  Training set accuracy = 0.990900, Test set accuracy = 0.959500
 epoch 37/100. Took 1.8221 seconds. 
  Full-batch training loss = 0.052003, test loss = 0.146572
  Training set accuracy = 0.991100, Test set accuracy = 0.958000
 epoch 38/100. Took 1.7783 seconds. 
  Full-batch training loss = 0.049877, test loss = 0.143510
  Training set accuracy = 0.992100, Test set accuracy = 0.959000
 epoch 39/100. Took 2.0238 seconds. 
  Full-batch training loss = 0.049059, test loss = 0.144340
  Training set accuracy = 0.992300, Test set accuracy = 0.959200
 epoch 40/100. Took 1.7529 seconds. 
  Full-batch training loss = 0.046891, test loss = 0.143033
  Training set accuracy = 0.992500, Test set accuracy = 0.958700
 epoch 41/100. Took 1.74 seconds. 
  Full-batch training loss = 0.045619, test loss = 0.142983
  Training set accuracy = 0.992900, Test set accuracy = 0.958900
 epoch 42/100. Took 1.7993 seconds. 
  Full-batch training loss = 0.044083, test loss = 0.142599
  Training set accuracy = 0.993600, Test set accuracy = 0.959300
 epoch 43/100. Took 1.7692 seconds. 
  Full-batch training loss = 0.043084, test loss = 0.142675
  Training set accuracy = 0.993800, Test set accuracy = 0.959200
 epoch 44/100. Took 1.79 seconds. 
  Full-batch training loss = 0.041612, test loss = 0.141970
  Training set accuracy = 0.994100, Test set accuracy = 0.959000
 epoch 45/100. Took 1.9952 seconds. 
  Full-batch training loss = 0.040446, test loss = 0.143657
  Training set accuracy = 0.993900, Test set accuracy = 0.959200
 epoch 46/100. Took 1.8886 seconds. 
  Full-batch training loss = 0.039153, test loss = 0.142367
  Training set accuracy = 0.994600, Test set accuracy = 0.958500
 epoch 47/100. Took 1.7772 seconds. 
  Full-batch training loss = 0.038258, test loss = 0.141147
  Training set accuracy = 0.995000, Test set accuracy = 0.959900
 epoch 48/100. Took 1.8738 seconds. 
  Full-batch training loss = 0.036805, test loss = 0.140946
  Training set accuracy = 0.995200, Test set accuracy = 0.959800
 epoch 49/100. Took 1.8384 seconds. 
  Full-batch training loss = 0.035474, test loss = 0.140630
  Training set accuracy = 0.995400, Test set accuracy = 0.959000
 epoch 50/100. Took 1.8827 seconds. 
  Full-batch training loss = 0.035071, test loss = 0.140847
  Training set accuracy = 0.995600, Test set accuracy = 0.959800
 epoch 51/100. Took 1.8003 seconds. 
  Full-batch training loss = 0.033704, test loss = 0.139890
  Training set accuracy = 0.996000, Test set accuracy = 0.960500
 epoch 52/100. Took 1.8827 seconds. 
  Full-batch training loss = 0.033268, test loss = 0.141192
  Training set accuracy = 0.995400, Test set accuracy = 0.959700
 epoch 53/100. Took 1.916 seconds. 
  Full-batch training loss = 0.032352, test loss = 0.140345
  Training set accuracy = 0.996800, Test set accuracy = 0.960900
 epoch 54/100. Took 1.8143 seconds. 
  Full-batch training loss = 0.030850, test loss = 0.140199
  Training set accuracy = 0.996600, Test set accuracy = 0.959700
 epoch 55/100. Took 1.7839 seconds. 
  Full-batch training loss = 0.030161, test loss = 0.140100
  Training set accuracy = 0.996800, Test set accuracy = 0.959900
 epoch 56/100. Took 1.7979 seconds. 
  Full-batch training loss = 0.029339, test loss = 0.139138
  Training set accuracy = 0.996900, Test set accuracy = 0.960200
 epoch 57/100. Took 1.8611 seconds. 
  Full-batch training loss = 0.028701, test loss = 0.141032
  Training set accuracy = 0.997300, Test set accuracy = 0.959900
 epoch 58/100. Took 1.8282 seconds. 
  Full-batch training loss = 0.027727, test loss = 0.139970
  Training set accuracy = 0.997500, Test set accuracy = 0.959700
 epoch 59/100. Took 1.8413 seconds. 
  Full-batch training loss = 0.027305, test loss = 0.139718
  Training set accuracy = 0.997400, Test set accuracy = 0.960100
 epoch 60/100. Took 1.7987 seconds. 
  Full-batch training loss = 0.026319, test loss = 0.138355
  Training set accuracy = 0.997600, Test set accuracy = 0.960400
 epoch 61/100. Took 1.7747 seconds. 
  Full-batch training loss = 0.025671, test loss = 0.139719
  Training set accuracy = 0.998000, Test set accuracy = 0.960800
 epoch 62/100. Took 1.8131 seconds. 
  Full-batch training loss = 0.024868, test loss = 0.138477
  Training set accuracy = 0.997800, Test set accuracy = 0.960400
 epoch 63/100. Took 1.8123 seconds. 
  Full-batch training loss = 0.024387, test loss = 0.138904
  Training set accuracy = 0.997900, Test set accuracy = 0.961300
 epoch 64/100. Took 1.811 seconds. 
  Full-batch training loss = 0.023726, test loss = 0.138538
  Training set accuracy = 0.998300, Test set accuracy = 0.961300
 epoch 65/100. Took 1.7889 seconds. 
  Full-batch training loss = 0.023454, test loss = 0.138168
  Training set accuracy = 0.998200, Test set accuracy = 0.961600
 epoch 66/100. Took 1.8026 seconds. 
  Full-batch training loss = 0.022369, test loss = 0.138589
  Training set accuracy = 0.998200, Test set accuracy = 0.961200
 epoch 67/100. Took 1.9698 seconds. 
  Full-batch training loss = 0.021942, test loss = 0.138825
  Training set accuracy = 0.998200, Test set accuracy = 0.961200
 epoch 68/100. Took 1.8851 seconds. 
  Full-batch training loss = 0.021524, test loss = 0.140179
  Training set accuracy = 0.998400, Test set accuracy = 0.961400
 epoch 69/100. Took 1.8248 seconds. 
  Full-batch training loss = 0.020911, test loss = 0.138943
  Training set accuracy = 0.998300, Test set accuracy = 0.961700
 epoch 70/100. Took 1.7688 seconds. 
  Full-batch training loss = 0.020407, test loss = 0.138982
  Training set accuracy = 0.998300, Test set accuracy = 0.961300
 epoch 71/100. Took 1.9119 seconds. 
  Full-batch training loss = 0.020025, test loss = 0.138437
  Training set accuracy = 0.998400, Test set accuracy = 0.961200
 epoch 72/100. Took 1.9127 seconds. 
  Full-batch training loss = 0.019684, test loss = 0.140450
  Training set accuracy = 0.998400, Test set accuracy = 0.960400
 epoch 73/100. Took 1.835 seconds. 
  Full-batch training loss = 0.019096, test loss = 0.138572
  Training set accuracy = 0.998400, Test set accuracy = 0.961600
 epoch 74/100. Took 1.7967 seconds. 
  Full-batch training loss = 0.018570, test loss = 0.138256
  Training set accuracy = 0.998400, Test set accuracy = 0.961600
 epoch 75/100. Took 1.7533 seconds. 
  Full-batch training loss = 0.018324, test loss = 0.139445
  Training set accuracy = 0.998700, Test set accuracy = 0.961000
 epoch 76/100. Took 1.8302 seconds. 
  Full-batch training loss = 0.017971, test loss = 0.139483
  Training set accuracy = 0.998600, Test set accuracy = 0.961500
 epoch 77/100. Took 1.9551 seconds. 
  Full-batch training loss = 0.017412, test loss = 0.138999
  Training set accuracy = 0.998600, Test set accuracy = 0.962000
 epoch 78/100. Took 2.0446 seconds. 
  Full-batch training loss = 0.017065, test loss = 0.138368
  Training set accuracy = 0.998700, Test set accuracy = 0.961500
 epoch 79/100. Took 1.7573 seconds. 
  Full-batch training loss = 0.016507, test loss = 0.138600
  Training set accuracy = 0.998800, Test set accuracy = 0.961300
 epoch 80/100. Took 1.9106 seconds. 
  Full-batch training loss = 0.016201, test loss = 0.139081
  Training set accuracy = 0.998800, Test set accuracy = 0.961900
 epoch 81/100. Took 1.8525 seconds. 
  Full-batch training loss = 0.015843, test loss = 0.139170
  Training set accuracy = 0.998800, Test set accuracy = 0.961200
 epoch 82/100. Took 1.8993 seconds. 
  Full-batch training loss = 0.015567, test loss = 0.138644
  Training set accuracy = 0.998700, Test set accuracy = 0.961800
 epoch 83/100. Took 1.8566 seconds. 
  Full-batch training loss = 0.015297, test loss = 0.139943
  Training set accuracy = 0.999200, Test set accuracy = 0.961200
 epoch 84/100. Took 1.9197 seconds. 
  Full-batch training loss = 0.014933, test loss = 0.138837
  Training set accuracy = 0.999100, Test set accuracy = 0.962200
 epoch 85/100. Took 1.8861 seconds. 
  Full-batch training loss = 0.014616, test loss = 0.139115
  Training set accuracy = 0.999200, Test set accuracy = 0.961700
 epoch 86/100. Took 2.039 seconds. 
  Full-batch training loss = 0.014347, test loss = 0.139119
  Training set accuracy = 0.999300, Test set accuracy = 0.961700
 epoch 87/100. Took 1.7604 seconds. 
  Full-batch training loss = 0.014059, test loss = 0.138226
  Training set accuracy = 0.999400, Test set accuracy = 0.962300
 epoch 88/100. Took 1.7693 seconds. 
  Full-batch training loss = 0.013752, test loss = 0.138682
  Training set accuracy = 0.999400, Test set accuracy = 0.961800
 epoch 89/100. Took 1.7623 seconds. 
  Full-batch training loss = 0.013468, test loss = 0.138764
  Training set accuracy = 0.999400, Test set accuracy = 0.961800
 epoch 90/100. Took 1.7865 seconds. 
  Full-batch training loss = 0.013253, test loss = 0.138952
  Training set accuracy = 0.999400, Test set accuracy = 0.962100
 epoch 91/100. Took 1.8223 seconds. 
  Full-batch training loss = 0.013037, test loss = 0.138608
  Training set accuracy = 0.999400, Test set accuracy = 0.962000
 epoch 92/100. Took 1.8143 seconds. 
  Full-batch training loss = 0.012811, test loss = 0.139266
  Training set accuracy = 0.999400, Test set accuracy = 0.962400
 epoch 93/100. Took 1.9985 seconds. 
  Full-batch training loss = 0.012555, test loss = 0.139787
  Training set accuracy = 0.999400, Test set accuracy = 0.962100
 epoch 94/100. Took 1.8106 seconds. 
  Full-batch training loss = 0.012335, test loss = 0.140038
  Training set accuracy = 0.999400, Test set accuracy = 0.961000
 epoch 95/100. Took 1.9344 seconds. 
  Full-batch training loss = 0.012187, test loss = 0.140224
  Training set accuracy = 0.999400, Test set accuracy = 0.962200
 epoch 96/100. Took 1.893 seconds. 
  Full-batch training loss = 0.011860, test loss = 0.139016
  Training set accuracy = 0.999400, Test set accuracy = 0.962700
 epoch 97/100. Took 1.7647 seconds. 
  Full-batch training loss = 0.011625, test loss = 0.139029
  Training set accuracy = 0.999400, Test set accuracy = 0.962300
 epoch 98/100. Took 1.7932 seconds. 
  Full-batch training loss = 0.011529, test loss = 0.140372
  Training set accuracy = 0.999600, Test set accuracy = 0.962100
 epoch 99/100. Took 1.7408 seconds. 
  Full-batch training loss = 0.011395, test loss = 0.139585
  Training set accuracy = 0.999600, Test set accuracy = 0.962700
 epoch 100/100. Took 1.7555 seconds. 
  Full-batch training loss = 0.011051, test loss = 0.139514
  Training set accuracy = 0.999600, Test set accuracy = 0.961700
Elapsed time is 460.805218 seconds.
End Training
