==========================================================
  Architecture is 500 500 500 // 3 layer x 500 neuron
----------------------------------------------------------
Execution 143/270
----------------------------------------------------------
* ReducedData	5000
* PreTrainRBM	true
	-> Epochs	20
	-> LearningRate	0.05
* Fine-tunning
	-> Epochs	200
	-> LearningRate	0.05
==========================================================
Start Training
Number of training examples: 5000  BatchSize: 100
Number of inputs: 784  Number of outputs: 10
Number of hidden layers: 3 (500  500  500)
Training binary-binary RBM in layer 1 (784  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.54715 seconds. Average reconstruction error is: 37.7364
 epoch 2/20. Took 0.53744 seconds. Average reconstruction error is: 20.5316
 epoch 3/20. Took 0.54211 seconds. Average reconstruction error is: 16.6031
 epoch 4/20. Took 0.53578 seconds. Average reconstruction error is: 14.5327
 epoch 5/20. Took 0.54185 seconds. Average reconstruction error is: 13.2711
 epoch 6/20. Took 0.54385 seconds. Average reconstruction error is: 12.3479
 epoch 7/20. Took 0.53585 seconds. Average reconstruction error is: 11.6882
 epoch 8/20. Took 0.5383 seconds. Average reconstruction error is: 11.1712
 epoch 9/20. Took 0.53679 seconds. Average reconstruction error is: 10.7132
 epoch 10/20. Took 0.53929 seconds. Average reconstruction error is: 10.3373
 epoch 11/20. Took 0.53739 seconds. Average reconstruction error is: 10.0027
 epoch 12/20. Took 0.53928 seconds. Average reconstruction error is: 9.6805
 epoch 13/20. Took 0.54507 seconds. Average reconstruction error is: 9.4292
 epoch 14/20. Took 0.54581 seconds. Average reconstruction error is: 9.2432
 epoch 15/20. Took 0.55036 seconds. Average reconstruction error is: 9.0455
 epoch 16/20. Took 0.53923 seconds. Average reconstruction error is: 8.8513
 epoch 17/20. Took 0.55361 seconds. Average reconstruction error is: 8.7321
 epoch 18/20. Took 0.5453 seconds. Average reconstruction error is: 8.611
 epoch 19/20. Took 0.53626 seconds. Average reconstruction error is: 8.4868
 epoch 20/20. Took 0.54147 seconds. Average reconstruction error is: 8.3486
Training binary-binary RBM in layer 2 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.37434 seconds. Average reconstruction error is: 36.0848
 epoch 2/20. Took 0.37213 seconds. Average reconstruction error is: 19.72
 epoch 3/20. Took 0.37494 seconds. Average reconstruction error is: 15.3378
 epoch 4/20. Took 0.37634 seconds. Average reconstruction error is: 13.1493
 epoch 5/20. Took 0.37386 seconds. Average reconstruction error is: 11.8625
 epoch 6/20. Took 0.3753 seconds. Average reconstruction error is: 11.0789
 epoch 7/20. Took 0.37665 seconds. Average reconstruction error is: 10.5007
 epoch 8/20. Took 0.37278 seconds. Average reconstruction error is: 10.1472
 epoch 9/20. Took 0.37769 seconds. Average reconstruction error is: 9.7246
 epoch 10/20. Took 0.37719 seconds. Average reconstruction error is: 9.4936
 epoch 11/20. Took 0.3787 seconds. Average reconstruction error is: 9.316
 epoch 12/20. Took 0.37565 seconds. Average reconstruction error is: 9.1578
 epoch 13/20. Took 0.37274 seconds. Average reconstruction error is: 8.9612
 epoch 14/20. Took 0.37608 seconds. Average reconstruction error is: 8.9013
 epoch 15/20. Took 0.37909 seconds. Average reconstruction error is: 8.8209
 epoch 16/20. Took 0.37343 seconds. Average reconstruction error is: 8.6725
 epoch 17/20. Took 0.37873 seconds. Average reconstruction error is: 8.5786
 epoch 18/20. Took 0.38721 seconds. Average reconstruction error is: 8.5581
 epoch 19/20. Took 0.37342 seconds. Average reconstruction error is: 8.4217
 epoch 20/20. Took 0.3725 seconds. Average reconstruction error is: 8.3362
Training binary-binary RBM in layer 3 (500  500) with CD1 for 20 epochs
 epoch 1/20. Took 0.37285 seconds. Average reconstruction error is: 28.028
 epoch 2/20. Took 0.37945 seconds. Average reconstruction error is: 13.6712
 epoch 3/20. Took 0.3752 seconds. Average reconstruction error is: 10.4331
 epoch 4/20. Took 0.37684 seconds. Average reconstruction error is: 8.9504
 epoch 5/20. Took 0.37755 seconds. Average reconstruction error is: 8.1004
 epoch 6/20. Took 0.37742 seconds. Average reconstruction error is: 7.5524
 epoch 7/20. Took 0.37687 seconds. Average reconstruction error is: 7.248
 epoch 8/20. Took 0.37464 seconds. Average reconstruction error is: 7.0304
 epoch 9/20. Took 0.37831 seconds. Average reconstruction error is: 6.7992
 epoch 10/20. Took 0.37909 seconds. Average reconstruction error is: 6.6396
 epoch 11/20. Took 0.37393 seconds. Average reconstruction error is: 6.5689
 epoch 12/20. Took 0.3757 seconds. Average reconstruction error is: 6.4385
 epoch 13/20. Took 0.37662 seconds. Average reconstruction error is: 6.3643
 epoch 14/20. Took 0.37712 seconds. Average reconstruction error is: 6.3048
 epoch 15/20. Took 0.37691 seconds. Average reconstruction error is: 6.2046
 epoch 16/20. Took 0.37445 seconds. Average reconstruction error is: 6.234
 epoch 17/20. Took 0.37488 seconds. Average reconstruction error is: 6.1678
 epoch 18/20. Took 0.3762 seconds. Average reconstruction error is: 6.12
 epoch 19/20. Took 0.37524 seconds. Average reconstruction error is: 6.0965
 epoch 20/20. Took 0.37509 seconds. Average reconstruction error is: 6.103
Training NN  (784  500  500  500   10) with BackPropagation for 200 epochs
 epoch 1/200. Took 0.87098 seconds. 
  Full-batch training loss = 0.431276, test loss = 0.443068
  Training set accuracy = 0.892000, Test set accuracy = 0.892600
 epoch 2/200. Took 0.87308 seconds. 
  Full-batch training loss = 0.334990, test loss = 0.350216
  Training set accuracy = 0.909600, Test set accuracy = 0.908300
 epoch 3/200. Took 0.87582 seconds. 
  Full-batch training loss = 0.292389, test loss = 0.315695
  Training set accuracy = 0.916400, Test set accuracy = 0.913000
 epoch 4/200. Took 0.8894 seconds. 
  Full-batch training loss = 0.262697, test loss = 0.291923
  Training set accuracy = 0.928600, Test set accuracy = 0.919600
 epoch 5/200. Took 0.87635 seconds. 
  Full-batch training loss = 0.240767, test loss = 0.274081
  Training set accuracy = 0.933400, Test set accuracy = 0.923800
 epoch 6/200. Took 0.8796 seconds. 
  Full-batch training loss = 0.225090, test loss = 0.263743
  Training set accuracy = 0.938800, Test set accuracy = 0.924800
 epoch 7/200. Took 0.88579 seconds. 
  Full-batch training loss = 0.211394, test loss = 0.254863
  Training set accuracy = 0.942600, Test set accuracy = 0.926000
 epoch 8/200. Took 0.87107 seconds. 
  Full-batch training loss = 0.201594, test loss = 0.249098
  Training set accuracy = 0.942600, Test set accuracy = 0.927400
 epoch 9/200. Took 0.87479 seconds. 
  Full-batch training loss = 0.194091, test loss = 0.248995
  Training set accuracy = 0.947600, Test set accuracy = 0.927200
 epoch 10/200. Took 0.87845 seconds. 
  Full-batch training loss = 0.176800, test loss = 0.231178
  Training set accuracy = 0.952400, Test set accuracy = 0.931900
 epoch 11/200. Took 0.87288 seconds. 
  Full-batch training loss = 0.173082, test loss = 0.229819
  Training set accuracy = 0.949800, Test set accuracy = 0.933000
 epoch 12/200. Took 0.87301 seconds. 
  Full-batch training loss = 0.162508, test loss = 0.224909
  Training set accuracy = 0.954200, Test set accuracy = 0.935800
 epoch 13/200. Took 0.87435 seconds. 
  Full-batch training loss = 0.155478, test loss = 0.220814
  Training set accuracy = 0.957800, Test set accuracy = 0.934900
 epoch 14/200. Took 0.87616 seconds. 
  Full-batch training loss = 0.148561, test loss = 0.216523
  Training set accuracy = 0.958800, Test set accuracy = 0.936800
 epoch 15/200. Took 0.87501 seconds. 
  Full-batch training loss = 0.141844, test loss = 0.213869
  Training set accuracy = 0.964000, Test set accuracy = 0.936900
 epoch 16/200. Took 0.87074 seconds. 
  Full-batch training loss = 0.138052, test loss = 0.212661
  Training set accuracy = 0.963600, Test set accuracy = 0.938100
 epoch 17/200. Took 0.87133 seconds. 
  Full-batch training loss = 0.132669, test loss = 0.211755
  Training set accuracy = 0.965000, Test set accuracy = 0.937000
 epoch 18/200. Took 0.87042 seconds. 
  Full-batch training loss = 0.125851, test loss = 0.207166
  Training set accuracy = 0.966000, Test set accuracy = 0.938500
 epoch 19/200. Took 0.87964 seconds. 
  Full-batch training loss = 0.120768, test loss = 0.206088
  Training set accuracy = 0.968800, Test set accuracy = 0.938300
 epoch 20/200. Took 0.87957 seconds. 
  Full-batch training loss = 0.116934, test loss = 0.203464
  Training set accuracy = 0.970600, Test set accuracy = 0.939400
 epoch 21/200. Took 0.87426 seconds. 
  Full-batch training loss = 0.111664, test loss = 0.200380
  Training set accuracy = 0.972400, Test set accuracy = 0.940500
 epoch 22/200. Took 0.885 seconds. 
  Full-batch training loss = 0.107497, test loss = 0.199039
  Training set accuracy = 0.972200, Test set accuracy = 0.941000
 epoch 23/200. Took 0.87724 seconds. 
  Full-batch training loss = 0.103491, test loss = 0.198378
  Training set accuracy = 0.975400, Test set accuracy = 0.941300
 epoch 24/200. Took 0.87374 seconds. 
  Full-batch training loss = 0.099976, test loss = 0.196845
  Training set accuracy = 0.975800, Test set accuracy = 0.942000
 epoch 25/200. Took 0.87953 seconds. 
  Full-batch training loss = 0.097968, test loss = 0.195752
  Training set accuracy = 0.974800, Test set accuracy = 0.942000
 epoch 26/200. Took 0.87035 seconds. 
  Full-batch training loss = 0.094222, test loss = 0.195264
  Training set accuracy = 0.978600, Test set accuracy = 0.941800
 epoch 27/200. Took 0.86985 seconds. 
  Full-batch training loss = 0.089555, test loss = 0.194018
  Training set accuracy = 0.980800, Test set accuracy = 0.942800
 epoch 28/200. Took 0.87014 seconds. 
  Full-batch training loss = 0.087633, test loss = 0.193646
  Training set accuracy = 0.980400, Test set accuracy = 0.942200
 epoch 29/200. Took 0.87278 seconds. 
  Full-batch training loss = 0.085340, test loss = 0.193008
  Training set accuracy = 0.980800, Test set accuracy = 0.943400
 epoch 30/200. Took 0.87316 seconds. 
  Full-batch training loss = 0.082343, test loss = 0.192234
  Training set accuracy = 0.982000, Test set accuracy = 0.942600
 epoch 31/200. Took 0.88194 seconds. 
  Full-batch training loss = 0.077743, test loss = 0.188488
  Training set accuracy = 0.983600, Test set accuracy = 0.944000
 epoch 32/200. Took 0.87536 seconds. 
  Full-batch training loss = 0.076426, test loss = 0.189365
  Training set accuracy = 0.984000, Test set accuracy = 0.943800
 epoch 33/200. Took 0.87276 seconds. 
  Full-batch training loss = 0.072195, test loss = 0.186675
  Training set accuracy = 0.985800, Test set accuracy = 0.945700
 epoch 34/200. Took 0.87456 seconds. 
  Full-batch training loss = 0.070005, test loss = 0.185724
  Training set accuracy = 0.986800, Test set accuracy = 0.945900
 epoch 35/200. Took 0.8697 seconds. 
  Full-batch training loss = 0.068516, test loss = 0.186786
  Training set accuracy = 0.987200, Test set accuracy = 0.945000
 epoch 36/200. Took 0.87264 seconds. 
  Full-batch training loss = 0.065984, test loss = 0.185167
  Training set accuracy = 0.988200, Test set accuracy = 0.945400
 epoch 37/200. Took 0.88169 seconds. 
  Full-batch training loss = 0.064092, test loss = 0.184780
  Training set accuracy = 0.987800, Test set accuracy = 0.945700
 epoch 38/200. Took 0.88664 seconds. 
  Full-batch training loss = 0.061976, test loss = 0.183901
  Training set accuracy = 0.988400, Test set accuracy = 0.945500
 epoch 39/200. Took 0.87599 seconds. 
  Full-batch training loss = 0.060499, test loss = 0.184618
  Training set accuracy = 0.989600, Test set accuracy = 0.947300
 epoch 40/200. Took 0.87685 seconds. 
  Full-batch training loss = 0.058444, test loss = 0.183911
  Training set accuracy = 0.990200, Test set accuracy = 0.946700
 epoch 41/200. Took 0.88031 seconds. 
  Full-batch training loss = 0.056823, test loss = 0.183996
  Training set accuracy = 0.989800, Test set accuracy = 0.946900
 epoch 42/200. Took 0.87207 seconds. 
  Full-batch training loss = 0.054652, test loss = 0.182268
  Training set accuracy = 0.989800, Test set accuracy = 0.947100
 epoch 43/200. Took 0.87477 seconds. 
  Full-batch training loss = 0.053118, test loss = 0.182565
  Training set accuracy = 0.990400, Test set accuracy = 0.947100
 epoch 44/200. Took 0.87832 seconds. 
  Full-batch training loss = 0.050884, test loss = 0.181192
  Training set accuracy = 0.991800, Test set accuracy = 0.947700
 epoch 45/200. Took 0.87176 seconds. 
  Full-batch training loss = 0.049799, test loss = 0.181821
  Training set accuracy = 0.992400, Test set accuracy = 0.947800
 epoch 46/200. Took 0.87428 seconds. 
  Full-batch training loss = 0.048344, test loss = 0.180026
  Training set accuracy = 0.992400, Test set accuracy = 0.948300
 epoch 47/200. Took 0.87121 seconds. 
  Full-batch training loss = 0.046485, test loss = 0.180765
  Training set accuracy = 0.992800, Test set accuracy = 0.949000
 epoch 48/200. Took 0.87154 seconds. 
  Full-batch training loss = 0.045181, test loss = 0.181823
  Training set accuracy = 0.993200, Test set accuracy = 0.947200
 epoch 49/200. Took 0.87421 seconds. 
  Full-batch training loss = 0.044394, test loss = 0.180182
  Training set accuracy = 0.992800, Test set accuracy = 0.947700
 epoch 50/200. Took 0.8949 seconds. 
  Full-batch training loss = 0.043290, test loss = 0.181292
  Training set accuracy = 0.993400, Test set accuracy = 0.948700
 epoch 51/200. Took 0.87505 seconds. 
  Full-batch training loss = 0.041786, test loss = 0.181227
  Training set accuracy = 0.994000, Test set accuracy = 0.948400
 epoch 52/200. Took 0.8813 seconds. 
  Full-batch training loss = 0.040433, test loss = 0.180228
  Training set accuracy = 0.994600, Test set accuracy = 0.948800
 epoch 53/200. Took 0.8738 seconds. 
  Full-batch training loss = 0.039389, test loss = 0.180374
  Training set accuracy = 0.994800, Test set accuracy = 0.948600
 epoch 54/200. Took 0.88087 seconds. 
  Full-batch training loss = 0.038496, test loss = 0.181568
  Training set accuracy = 0.995200, Test set accuracy = 0.948100
 epoch 55/200. Took 0.87758 seconds. 
  Full-batch training loss = 0.037061, test loss = 0.180513
  Training set accuracy = 0.994800, Test set accuracy = 0.948000
 epoch 56/200. Took 0.8761 seconds. 
  Full-batch training loss = 0.036045, test loss = 0.179506
  Training set accuracy = 0.995400, Test set accuracy = 0.948900
 epoch 57/200. Took 0.87578 seconds. 
  Full-batch training loss = 0.034926, test loss = 0.179041
  Training set accuracy = 0.996200, Test set accuracy = 0.948800
 epoch 58/200. Took 0.87193 seconds. 
  Full-batch training loss = 0.033767, test loss = 0.179117
  Training set accuracy = 0.996600, Test set accuracy = 0.948700
 epoch 59/200. Took 0.87423 seconds. 
  Full-batch training loss = 0.032990, test loss = 0.178820
  Training set accuracy = 0.996400, Test set accuracy = 0.948500
 epoch 60/200. Took 0.86949 seconds. 
  Full-batch training loss = 0.032151, test loss = 0.178579
  Training set accuracy = 0.996600, Test set accuracy = 0.949000
 epoch 61/200. Took 0.87149 seconds. 
  Full-batch training loss = 0.031355, test loss = 0.179493
  Training set accuracy = 0.997000, Test set accuracy = 0.949500
 epoch 62/200. Took 0.88174 seconds. 
  Full-batch training loss = 0.030911, test loss = 0.180489
  Training set accuracy = 0.997200, Test set accuracy = 0.949300
 epoch 63/200. Took 0.86822 seconds. 
  Full-batch training loss = 0.029581, test loss = 0.179428
  Training set accuracy = 0.997200, Test set accuracy = 0.949300
 epoch 64/200. Took 0.88201 seconds. 
  Full-batch training loss = 0.029074, test loss = 0.179946
  Training set accuracy = 0.997000, Test set accuracy = 0.948800
 epoch 65/200. Took 0.8816 seconds. 
  Full-batch training loss = 0.028064, test loss = 0.179213
  Training set accuracy = 0.997400, Test set accuracy = 0.949300
 epoch 66/200. Took 0.86488 seconds. 
  Full-batch training loss = 0.027516, test loss = 0.179305
  Training set accuracy = 0.997600, Test set accuracy = 0.949800
 epoch 67/200. Took 0.87267 seconds. 
  Full-batch training loss = 0.026778, test loss = 0.179325
  Training set accuracy = 0.998600, Test set accuracy = 0.949200
 epoch 68/200. Took 0.89276 seconds. 
  Full-batch training loss = 0.026267, test loss = 0.179460
  Training set accuracy = 0.997600, Test set accuracy = 0.950400
 epoch 69/200. Took 0.87358 seconds. 
  Full-batch training loss = 0.025465, test loss = 0.179231
  Training set accuracy = 0.998200, Test set accuracy = 0.949800
 epoch 70/200. Took 0.87809 seconds. 
  Full-batch training loss = 0.024824, test loss = 0.179432
  Training set accuracy = 0.998400, Test set accuracy = 0.950000
 epoch 71/200. Took 0.88263 seconds. 
  Full-batch training loss = 0.024218, test loss = 0.179569
  Training set accuracy = 0.998400, Test set accuracy = 0.949500
 epoch 72/200. Took 0.88644 seconds. 
  Full-batch training loss = 0.023822, test loss = 0.179687
  Training set accuracy = 0.998400, Test set accuracy = 0.949300
 epoch 73/200. Took 0.86792 seconds. 
  Full-batch training loss = 0.023332, test loss = 0.179441
  Training set accuracy = 0.998200, Test set accuracy = 0.950100
 epoch 74/200. Took 0.88466 seconds. 
  Full-batch training loss = 0.022845, test loss = 0.179880
  Training set accuracy = 0.998400, Test set accuracy = 0.949800
 epoch 75/200. Took 0.867 seconds. 
  Full-batch training loss = 0.022075, test loss = 0.179701
  Training set accuracy = 0.999200, Test set accuracy = 0.949900
 epoch 76/200. Took 0.87236 seconds. 
  Full-batch training loss = 0.021649, test loss = 0.179985
  Training set accuracy = 0.999000, Test set accuracy = 0.950100
 epoch 77/200. Took 0.89147 seconds. 
  Full-batch training loss = 0.021148, test loss = 0.180170
  Training set accuracy = 0.999400, Test set accuracy = 0.949400
 epoch 78/200. Took 0.86329 seconds. 
  Full-batch training loss = 0.020675, test loss = 0.180004
  Training set accuracy = 0.999400, Test set accuracy = 0.950600
 epoch 79/200. Took 0.88375 seconds. 
  Full-batch training loss = 0.020089, test loss = 0.179861
  Training set accuracy = 0.999400, Test set accuracy = 0.949600
 epoch 80/200. Took 0.88387 seconds. 
  Full-batch training loss = 0.019836, test loss = 0.179432
  Training set accuracy = 0.999400, Test set accuracy = 0.950300
 epoch 81/200. Took 0.865 seconds. 
  Full-batch training loss = 0.019268, test loss = 0.180048
  Training set accuracy = 0.999400, Test set accuracy = 0.950100
 epoch 82/200. Took 0.87302 seconds. 
  Full-batch training loss = 0.018939, test loss = 0.179947
  Training set accuracy = 0.999600, Test set accuracy = 0.950100
 epoch 83/200. Took 0.87035 seconds. 
  Full-batch training loss = 0.018588, test loss = 0.180256
  Training set accuracy = 0.999600, Test set accuracy = 0.950200
 epoch 84/200. Took 0.86829 seconds. 
  Full-batch training loss = 0.018092, test loss = 0.180046
  Training set accuracy = 0.999600, Test set accuracy = 0.950200
 epoch 85/200. Took 0.87442 seconds. 
  Full-batch training loss = 0.017768, test loss = 0.180661
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 86/200. Took 0.86591 seconds. 
  Full-batch training loss = 0.017339, test loss = 0.180328
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 87/200. Took 0.87248 seconds. 
  Full-batch training loss = 0.016993, test loss = 0.180093
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 88/200. Took 0.86594 seconds. 
  Full-batch training loss = 0.016805, test loss = 0.181009
  Training set accuracy = 1.000000, Test set accuracy = 0.949600
 epoch 89/200. Took 0.87327 seconds. 
  Full-batch training loss = 0.016304, test loss = 0.180526
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 90/200. Took 0.87245 seconds. 
  Full-batch training loss = 0.016128, test loss = 0.180878
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 91/200. Took 0.87153 seconds. 
  Full-batch training loss = 0.015885, test loss = 0.180721
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 92/200. Took 0.87243 seconds. 
  Full-batch training loss = 0.015393, test loss = 0.180853
  Training set accuracy = 1.000000, Test set accuracy = 0.950000
 epoch 93/200. Took 0.88376 seconds. 
  Full-batch training loss = 0.015149, test loss = 0.181127
  Training set accuracy = 0.999800, Test set accuracy = 0.950800
 epoch 94/200. Took 0.87674 seconds. 
  Full-batch training loss = 0.014822, test loss = 0.180956
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 95/200. Took 0.87058 seconds. 
  Full-batch training loss = 0.014574, test loss = 0.181571
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 96/200. Took 0.88276 seconds. 
  Full-batch training loss = 0.014317, test loss = 0.181100
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 97/200. Took 0.87532 seconds. 
  Full-batch training loss = 0.014037, test loss = 0.181560
  Training set accuracy = 1.000000, Test set accuracy = 0.950200
 epoch 98/200. Took 0.87052 seconds. 
  Full-batch training loss = 0.013781, test loss = 0.181364
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 99/200. Took 0.877 seconds. 
  Full-batch training loss = 0.013790, test loss = 0.181953
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 100/200. Took 0.87015 seconds. 
  Full-batch training loss = 0.013355, test loss = 0.182018
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 101/200. Took 0.87256 seconds. 
  Full-batch training loss = 0.013151, test loss = 0.182115
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 102/200. Took 0.88045 seconds. 
  Full-batch training loss = 0.012910, test loss = 0.182312
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 103/200. Took 0.86885 seconds. 
  Full-batch training loss = 0.012680, test loss = 0.181841
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 104/200. Took 0.867 seconds. 
  Full-batch training loss = 0.012448, test loss = 0.181862
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 105/200. Took 0.90774 seconds. 
  Full-batch training loss = 0.012261, test loss = 0.182361
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 106/200. Took 0.86958 seconds. 
  Full-batch training loss = 0.012068, test loss = 0.182411
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 107/200. Took 0.87151 seconds. 
  Full-batch training loss = 0.011960, test loss = 0.182335
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 108/200. Took 0.88436 seconds. 
  Full-batch training loss = 0.011697, test loss = 0.182620
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 109/200. Took 0.87266 seconds. 
  Full-batch training loss = 0.011508, test loss = 0.182774
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 110/200. Took 0.8856 seconds. 
  Full-batch training loss = 0.011321, test loss = 0.182738
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 111/200. Took 0.8811 seconds. 
  Full-batch training loss = 0.011126, test loss = 0.182764
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 112/200. Took 0.87622 seconds. 
  Full-batch training loss = 0.010951, test loss = 0.183055
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 113/200. Took 0.87465 seconds. 
  Full-batch training loss = 0.010793, test loss = 0.183191
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 114/200. Took 0.88208 seconds. 
  Full-batch training loss = 0.010692, test loss = 0.183550
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 115/200. Took 0.87054 seconds. 
  Full-batch training loss = 0.010476, test loss = 0.183618
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 116/200. Took 0.87456 seconds. 
  Full-batch training loss = 0.010329, test loss = 0.183155
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 117/200. Took 0.88218 seconds. 
  Full-batch training loss = 0.010208, test loss = 0.183246
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 118/200. Took 0.87671 seconds. 
  Full-batch training loss = 0.010055, test loss = 0.183947
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 119/200. Took 0.87045 seconds. 
  Full-batch training loss = 0.009892, test loss = 0.183949
  Training set accuracy = 1.000000, Test set accuracy = 0.950300
 epoch 120/200. Took 0.87702 seconds. 
  Full-batch training loss = 0.009753, test loss = 0.184071
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 121/200. Took 0.87402 seconds. 
  Full-batch training loss = 0.009638, test loss = 0.184118
  Training set accuracy = 1.000000, Test set accuracy = 0.950400
 epoch 122/200. Took 0.87211 seconds. 
  Full-batch training loss = 0.009495, test loss = 0.184195
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 123/200. Took 0.8757 seconds. 
  Full-batch training loss = 0.009366, test loss = 0.184215
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 124/200. Took 0.88325 seconds. 
  Full-batch training loss = 0.009251, test loss = 0.184303
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 125/200. Took 0.88434 seconds. 
  Full-batch training loss = 0.009103, test loss = 0.184431
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 126/200. Took 0.89134 seconds. 
  Full-batch training loss = 0.009016, test loss = 0.184841
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 127/200. Took 0.87516 seconds. 
  Full-batch training loss = 0.008873, test loss = 0.184958
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 128/200. Took 0.87152 seconds. 
  Full-batch training loss = 0.008761, test loss = 0.185007
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 129/200. Took 0.88638 seconds. 
  Full-batch training loss = 0.008643, test loss = 0.185115
  Training set accuracy = 1.000000, Test set accuracy = 0.950500
 epoch 130/200. Took 0.87923 seconds. 
  Full-batch training loss = 0.008557, test loss = 0.185223
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 131/200. Took 0.87489 seconds. 
  Full-batch training loss = 0.008434, test loss = 0.185510
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 132/200. Took 0.8793 seconds. 
  Full-batch training loss = 0.008318, test loss = 0.185350
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 133/200. Took 0.87508 seconds. 
  Full-batch training loss = 0.008219, test loss = 0.185452
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 134/200. Took 0.87663 seconds. 
  Full-batch training loss = 0.008118, test loss = 0.185754
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 135/200. Took 0.8911 seconds. 
  Full-batch training loss = 0.008018, test loss = 0.185754
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 136/200. Took 0.87473 seconds. 
  Full-batch training loss = 0.007929, test loss = 0.186062
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 137/200. Took 0.91323 seconds. 
  Full-batch training loss = 0.007825, test loss = 0.186050
  Training set accuracy = 1.000000, Test set accuracy = 0.951400
 epoch 138/200. Took 0.87402 seconds. 
  Full-batch training loss = 0.007731, test loss = 0.186076
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 139/200. Took 0.86961 seconds. 
  Full-batch training loss = 0.007637, test loss = 0.186191
  Training set accuracy = 1.000000, Test set accuracy = 0.950600
 epoch 140/200. Took 0.87772 seconds. 
  Full-batch training loss = 0.007546, test loss = 0.186230
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 141/200. Took 0.90695 seconds. 
  Full-batch training loss = 0.007467, test loss = 0.186537
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 142/200. Took 0.87495 seconds. 
  Full-batch training loss = 0.007377, test loss = 0.186638
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 143/200. Took 0.8711 seconds. 
  Full-batch training loss = 0.007300, test loss = 0.186596
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 144/200. Took 0.87538 seconds. 
  Full-batch training loss = 0.007220, test loss = 0.186778
  Training set accuracy = 1.000000, Test set accuracy = 0.951400
 epoch 145/200. Took 0.87768 seconds. 
  Full-batch training loss = 0.007136, test loss = 0.186992
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 146/200. Took 0.87272 seconds. 
  Full-batch training loss = 0.007052, test loss = 0.186964
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 147/200. Took 0.87109 seconds. 
  Full-batch training loss = 0.006978, test loss = 0.187307
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 148/200. Took 0.86705 seconds. 
  Full-batch training loss = 0.006903, test loss = 0.187301
  Training set accuracy = 1.000000, Test set accuracy = 0.950800
 epoch 149/200. Took 0.87195 seconds. 
  Full-batch training loss = 0.006830, test loss = 0.187152
  Training set accuracy = 1.000000, Test set accuracy = 0.951500
 epoch 150/200. Took 0.86889 seconds. 
  Full-batch training loss = 0.006758, test loss = 0.187366
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 151/200. Took 0.87864 seconds. 
  Full-batch training loss = 0.006675, test loss = 0.187472
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 152/200. Took 0.86988 seconds. 
  Full-batch training loss = 0.006612, test loss = 0.187557
  Training set accuracy = 1.000000, Test set accuracy = 0.950700
 epoch 153/200. Took 0.87208 seconds. 
  Full-batch training loss = 0.006538, test loss = 0.187686
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 154/200. Took 0.88015 seconds. 
  Full-batch training loss = 0.006472, test loss = 0.187875
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 155/200. Took 0.86673 seconds. 
  Full-batch training loss = 0.006409, test loss = 0.188013
  Training set accuracy = 1.000000, Test set accuracy = 0.951600
 epoch 156/200. Took 0.87393 seconds. 
  Full-batch training loss = 0.006340, test loss = 0.188044
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 157/200. Took 0.87243 seconds. 
  Full-batch training loss = 0.006276, test loss = 0.188155
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 158/200. Took 0.8809 seconds. 
  Full-batch training loss = 0.006211, test loss = 0.188416
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 159/200. Took 0.8678 seconds. 
  Full-batch training loss = 0.006147, test loss = 0.188281
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 160/200. Took 0.87187 seconds. 
  Full-batch training loss = 0.006083, test loss = 0.188547
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 161/200. Took 0.87596 seconds. 
  Full-batch training loss = 0.006026, test loss = 0.188441
  Training set accuracy = 1.000000, Test set accuracy = 0.951300
 epoch 162/200. Took 0.87511 seconds. 
  Full-batch training loss = 0.005961, test loss = 0.188543
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 163/200. Took 0.88665 seconds. 
  Full-batch training loss = 0.005905, test loss = 0.188684
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 164/200. Took 0.86437 seconds. 
  Full-batch training loss = 0.005850, test loss = 0.188890
  Training set accuracy = 1.000000, Test set accuracy = 0.950900
 epoch 165/200. Took 0.87605 seconds. 
  Full-batch training loss = 0.005797, test loss = 0.188877
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 166/200. Took 0.88098 seconds. 
  Full-batch training loss = 0.005736, test loss = 0.189010
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 167/200. Took 0.88146 seconds. 
  Full-batch training loss = 0.005687, test loss = 0.189027
  Training set accuracy = 1.000000, Test set accuracy = 0.951500
 epoch 168/200. Took 0.87326 seconds. 
  Full-batch training loss = 0.005635, test loss = 0.189286
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 169/200. Took 0.87021 seconds. 
  Full-batch training loss = 0.005582, test loss = 0.189313
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 170/200. Took 0.8717 seconds. 
  Full-batch training loss = 0.005527, test loss = 0.189450
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 171/200. Took 0.87783 seconds. 
  Full-batch training loss = 0.005475, test loss = 0.189468
  Training set accuracy = 1.000000, Test set accuracy = 0.951600
 epoch 172/200. Took 0.87144 seconds. 
  Full-batch training loss = 0.005423, test loss = 0.189690
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 173/200. Took 0.87485 seconds. 
  Full-batch training loss = 0.005375, test loss = 0.189750
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 174/200. Took 0.86568 seconds. 
  Full-batch training loss = 0.005324, test loss = 0.189828
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 175/200. Took 0.8767 seconds. 
  Full-batch training loss = 0.005279, test loss = 0.190070
  Training set accuracy = 1.000000, Test set accuracy = 0.951400
 epoch 176/200. Took 0.87385 seconds. 
  Full-batch training loss = 0.005231, test loss = 0.189976
  Training set accuracy = 1.000000, Test set accuracy = 0.951300
 epoch 177/200. Took 0.89739 seconds. 
  Full-batch training loss = 0.005188, test loss = 0.190114
  Training set accuracy = 1.000000, Test set accuracy = 0.951000
 epoch 178/200. Took 0.87446 seconds. 
  Full-batch training loss = 0.005146, test loss = 0.190305
  Training set accuracy = 1.000000, Test set accuracy = 0.952000
 epoch 179/200. Took 0.87062 seconds. 
  Full-batch training loss = 0.005099, test loss = 0.190375
  Training set accuracy = 1.000000, Test set accuracy = 0.951200
 epoch 180/200. Took 0.87962 seconds. 
  Full-batch training loss = 0.005054, test loss = 0.190449
  Training set accuracy = 1.000000, Test set accuracy = 0.951400
 epoch 181/200. Took 0.88078 seconds. 
  Full-batch training loss = 0.005009, test loss = 0.190485
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 182/200. Took 0.87308 seconds. 
  Full-batch training loss = 0.004962, test loss = 0.190620
  Training set accuracy = 1.000000, Test set accuracy = 0.951600
 epoch 183/200. Took 0.87923 seconds. 
  Full-batch training loss = 0.004923, test loss = 0.190699
  Training set accuracy = 1.000000, Test set accuracy = 0.951500
 epoch 184/200. Took 0.87981 seconds. 
  Full-batch training loss = 0.004884, test loss = 0.190782
  Training set accuracy = 1.000000, Test set accuracy = 0.952000
 epoch 185/200. Took 0.88178 seconds. 
  Full-batch training loss = 0.004842, test loss = 0.190921
  Training set accuracy = 1.000000, Test set accuracy = 0.952000
 epoch 186/200. Took 0.86831 seconds. 
  Full-batch training loss = 0.004797, test loss = 0.190894
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 187/200. Took 0.8816 seconds. 
  Full-batch training loss = 0.004762, test loss = 0.191022
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 188/200. Took 0.88631 seconds. 
  Full-batch training loss = 0.004719, test loss = 0.191185
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 189/200. Took 0.87324 seconds. 
  Full-batch training loss = 0.004681, test loss = 0.191280
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 190/200. Took 0.88643 seconds. 
  Full-batch training loss = 0.004643, test loss = 0.191309
  Training set accuracy = 1.000000, Test set accuracy = 0.951100
 epoch 191/200. Took 0.88467 seconds. 
  Full-batch training loss = 0.004606, test loss = 0.191471
  Training set accuracy = 1.000000, Test set accuracy = 0.951400
 epoch 192/200. Took 0.86605 seconds. 
  Full-batch training loss = 0.004569, test loss = 0.191535
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 193/200. Took 0.88853 seconds. 
  Full-batch training loss = 0.004532, test loss = 0.191611
  Training set accuracy = 1.000000, Test set accuracy = 0.951500
 epoch 194/200. Took 0.86938 seconds. 
  Full-batch training loss = 0.004496, test loss = 0.191712
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 195/200. Took 0.87248 seconds. 
  Full-batch training loss = 0.004461, test loss = 0.191864
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 196/200. Took 0.86976 seconds. 
  Full-batch training loss = 0.004427, test loss = 0.191927
  Training set accuracy = 1.000000, Test set accuracy = 0.951700
 epoch 197/200. Took 0.8793 seconds. 
  Full-batch training loss = 0.004390, test loss = 0.191983
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
 epoch 198/200. Took 0.87186 seconds. 
  Full-batch training loss = 0.004358, test loss = 0.192106
  Training set accuracy = 1.000000, Test set accuracy = 0.951300
 epoch 199/200. Took 0.88889 seconds. 
  Full-batch training loss = 0.004329, test loss = 0.192223
  Training set accuracy = 1.000000, Test set accuracy = 0.951300
 epoch 200/200. Took 0.87349 seconds. 
  Full-batch training loss = 0.004290, test loss = 0.192287
  Training set accuracy = 1.000000, Test set accuracy = 0.951800
Elapsed time is 419.536201 seconds.
End Training
