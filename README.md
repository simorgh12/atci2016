ATCI - course 2015-16 – Master branch
=================================================

# Advanced Topics in Computational Intelligence ( ATCI ) from Master in Artificial Intelligence degree at UPC.

### Contributors:
* Andrés Flores (andres.flores@est.fib.upc.edu)
* Hugo Bertiche (hugo.bertiche@est.fib.upc.edu)
* Vicent Roig (vicent.roig@est.fib.upc.edu)